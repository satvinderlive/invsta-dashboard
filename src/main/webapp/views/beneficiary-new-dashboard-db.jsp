<%-- 
    Document   : dashboard
    Created on : Jul 25, 2019, 2:38:52 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Beneficiary Dashboard</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <!--<link href="resources/images/mint.png" rel="shortcut icon"/>-->
        <link rel="icon" href="./resources/images/invsta-color-logo.png" type="image/gif" sizes="16x16">
        <link href="https://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <link href="https://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>
        <!--<link href="https://backoffice.invsta.io/pocv/resources/css/main.css?version=3.5.1" rel="stylesheet"/>-->
        <link href="https://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link href="./resources/css/main.css" rel="stylesheet"/>

        <link href="./resources/css/sweetalert.css" rel="stylesheet"/>
        <link href="./resources/css/custom.css" rel="stylesheet"/>
        <link href="./resources/css/newcss/new-main.css" rel="stylesheet"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">       
        <style>
            .el-chart-w {
                overflow: visible!important;
            }
            .el-tablo:hover {
                box-shadow: none;
                transform: none;
            }
            .founder-icon-color {
                display: flex;
                margin-left: 10px;
            }
            .value.sumWalletBalance {
                float: right;
                display: flex;
                align-items: center;
            }

            .emp-profile{
                padding: 3%;
                margin-top: 3%;
                margin-bottom: 3%;
                border-radius: 0.5rem;
                background: #fff;
            }
            .profile-img{
                text-align: center;
            }
            .profile-img img{
                width: 70%;
                height: 100%;
            }
            .profile-img .file {
                position: relative;
                overflow: hidden;
                margin-top: -20%;
                width: 70%;
                border: none;
                border-radius: 0;
                font-size: 15px;
                background: #212529b8;
            }
            .profile-img .file input {
                position: absolute;
                opacity: 0;
                right: 0;
                top: 0;
            }
            .profile-head h5{
                color: #333;
            }
            .profile-head h6{
                color: #0062cc;
            }
            .profile-edit-btn{
                border: none;
                border-radius: 1.5rem;
                width: 70%;
                padding: 2%;
                font-weight: 600;
                color: #6c757d;
                cursor: pointer;
            }
            /*            .proile-rating{
                            font-size: 12px;
                            color: #818182;
                            margin-top: 5%;
                        }
                        .proile-rating span{
                            color: #495057;
                            font-size: 15px;
                            font-weight: 600;
                        }*/
            .profile-head .nav-tabs{
                margin-bottom:5%;
            }
            .profile-head .nav-tabs .nav-link{
                font-weight:600;
                border: none;
            }
            .profile-head .nav-tabs .nav-link.active{
                border: none;
                border-bottom:2px solid #0062cc;
            }
            .profile-work{
                padding: 14%;
                margin-top: -15%;
            }
            .profile-work p{
                font-size: 12px;
                color: #818182;
                font-weight: 600;
                margin-top: 10%;
            }
            .profile-work a{
                text-decoration: none;
                color: #495057;
                font-weight: 600;
                font-size: 14px;
            }
            .profile-work ul{
                list-style: none;
            }
            .profile-tab label{
                font-weight: 600;
            }
            .profile-tab p {
                font-weight: 600;
                color: #54555a;
            }
            .el-chart-w {
                overflow: visible!important;
            }
            .portfolio_overlay {
                height: 75%;
                display: block;
            }
            .ilumony-box:hover .portfolio_overlay {
                display: block;
            }
            .funds-deatil {
                background: #fff;
                padding: 1rem;
                border-radius: 4px;
                margin-bottom: 10px;
            }
            .show_more_btn_new {
                position: absolute;
                top: 0;
                right: 0px;
                margin: 11px 27px;
            }
            .funds-deatil h4 {
                max-width: 80%;
                margin-bottom: 15px;
            }
            .collapse {
                display: none;
            }
            .collapse.show {
                display: block;
            }
            .profile-img img {
                max-width: 102px;
                width: 70%;
                height: 100%;
                /* border-radius: 34%; */
            }
            .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {

                border: none;
            }
            .nav.smaller.nav-tabs .nav-link {
                padding: 0.7em .6em;
            }
            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('./resources/images/Preloader_3.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
        </style>
    </head>
    <body>
        <!--        <div class="loader"></div>-->
        <!--        <div id="overlay">
                    <img src="./resources/images/Preloader_3.gif" alt="Loading" />
                    Loading...
                </div>-->
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"></jsp:include>
                    <div class="content-w">
                        <!--------------------
                        START - Breadcrumbs
                -------------------->
                        <ul class="breadcrumb">
                            <li class="">
                                <span><a href="./home">Home</a></span>
                            </li>
                            <li class="">
                                <span class="right-mark"><i class="fa fa-caret-right"></i></span>
                            </li>
                            <li class="">
                                <span><a href="">KiwiSaver Overview</a></span>
                            </li>

                        </ul>
                        <!--------------------
                        END - Breadcrumbs
                          -------------------->
                        <div class="content-panel-toggler">
                            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                        </div>
                        <div class="content-i">
                            <div class="content-box">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="element-wrapper">
                                            <!--<p  class="wlcm-heading">Hi <span class="logged-user-name"></span>,  welcome to Mint,</p>-->
                                            <h6>Hi John, here’s an overview of your KiwiSaver account.  <h6><br>
                                                    <a href="javascript:void(0)"class="nav-link">
                                                        Investment Overview
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <div class="element-box">
                                                                <div class="os-tabs-w">
                                                                    <div class="tab-content">
                                                                        <div class="start-amnt">
                                                                            <div class="tab-pane active custom-add-box" id="tab_overview">
                                                                                <div class="" style="">
                                                                                    Amount Invested 
                                                                                    <!--                                                                <div class="pulsating-circle pulsating-around" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom"> 
                                                                                                                                                        <span class="tooltiptext">The Live Balance chart provides an indication of the real-time balance of your  portfolio, and is reliant on external data sources which may be different to what is reported on exchanges. The actual daily closing value and withdrawal value of your portfolio will be different. This balance does not include any cash that is available in your account.</span></div>-->

                                                                                </div>
                                                                                <div class="value add-value currentBalance">$952,649 

                                                                                </div>

                                                                            </div>
                                                                            <div class="custom-add-box-1" style="">
                                                                                <div class=" current-label add-label" >
                                                                                    Current Value
                                                                                    <!--                                                                <div class="pulsating-circle2 pulsating-around2" style="vertical-align:text-bottom;   " data-toggle="tooltip" title="">
                                                                                                                                                        <span class="tooltiptext">(previous day close, incl cash in wallet) This represents the actual value of your  portfolio investments based on the previous day closing value, and includes any cash you may have available in your cash account.</span>
                                                                                                                                                    </div>-->
                                                                                </div>

                                                                                <div class="value add-value sumWalletBalance clr-add" style="">
                                                                                    <div class="sumWalletBal" >  $1,280,302 </div>
                                                                                    <!--<span class="founder-icon-color"><div class="percentage" > 16.23% </div> <img class="income-high" src="./resources/images/ins.gif"></img></span>-->
                                                                                </div>
                                                                            </div>
                                                                            <div class="year-drop">
                                                                                Year: <select><option>2019</option>
                                                                                    <option>2018</option>
                                                                                    <option>2017</option>
                                                                                    <option>2016</option></select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="tab-pane active" id="tab_all_days">

                                                                            <div id="stockbalance" style="min-width: 100%; height: 303px; margin: 0 auto;"></div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="element-box">
                                                                <div class="fund-contri-btn">
                                                                    <a href="#" class="tabl-btn kiwi-btn switch-space">
                                                                        Switch Fund
                                                                    </a>
                                                                    <a href="#" class="tabl-btn kiwi-btn kiwi-grey">
                                                                        Make Contribution
                                                                    </a>
                                                                </div>

                                                                <div class="groth-box">
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <div id="pie-1" style="width:150px; height: 120px; margin: 0 auto"></div>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <div class="pie-text">
                                                                                <p>Your current KiwiSaver Fund is:<br>Growth Fund(100%)</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="risk-indicator">
                                                                            <div class="element-wrapper">
                                                                                <h6 class="element-heade">Risk Indicator 
                                                                                    <!--<div class="pulsating-circle circle-2" data-toggle="tooltip" title="Managed funds in New Zealand must have a standard risk indicator. The risk indicator is designed to help investors understand the uncertainties both for loss and growth that may affect their investment. You can compare funds using the risk indicator. The risk indicator is rated from 1 (low) to 7 (high).The rating reflects how much the value of the fund's assets goes up and down (volatility). A higher risk generally means higher potential returns over time, but more ups and downs along the way. To help you clarify your own attitude to risk, you can seek financial advice or work out your risk profile at www.sorted.org.nz/tools/investor-kickstarter. Note that even the lowest category does not mean a risk-free investment, and there are other risks(described under the heading 'Other specific risks') that are not captured by this rating. This risk indicator is not a guarantee of a fund's future performance. The risk indicator is based on the returns data for the five years to 30 June 2019. While risk indicators are usually relatively stable, they do shift from time to time. You can see the most recent risk indicator in the latest fund update for each Fund." data-placement="right" style="vertical-align:text-bottom"></div>-->
                                                                                </h6>

                                                                            </div>
                                                                            <div class="Higher">
                                                                                <span class="risk-set">Lower risk</span>
                                                                                <span>Higher risk</span>
                                                                            </div>
                                                                            <span class="indicator-box">
                                                                                <span class="risk-btns ">1</span>
                                                                                <span class="risk-btns addactive-cls">2</span>
                                                                                <span class="risk-btns">3</span>
                                                                                <span class="risk-btns active">4</span>
                                                                                <span class="risk-btns">5</span>
                                                                                <span class="risk-btns">6</span>
                                                                                <span class="risk-btns">7</span>
                                                                            </span>
                                                                            <div class="Higher">
                                                                                <span  class="risk-set">Potentially <br>lower returns</span>
                                                                                <span>Potentially <br>higher returns</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="element-box">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div id="pie-2" style="width:120px; height: 120px; margin: 0 auto"></div>
                                                                    </div>
                                                                    <div class="col-md-8">
                                                                        <div class="pie-text ml-3">
                                                                            <p>Fund Performance over the last<br>3 months: 2.07%</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="element-box">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div id="pie-3" style="width:120px; height: 120px; margin: 0 auto"></div>
                                                                    </div>
                                                                    <div class="col-md-8">
                                                                        <div class="pie-text ml-3">
                                                                            <p>Fund Performance over the last<br>1 Year: 15.29%</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="element-box">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div id="pie-4" style="width:120px; height: 120px; margin: 0 auto"></div>
                                                                    </div>
                                                                    <div class="col-md-8">
                                                                        <div class="pie-text ml-3">
                                                                            <p>Fund Performance over the last<br>5 Years: 34%</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="element-box">
                                                                <h6 class="element-header"> Projected balance of your KiwiSaver account at retirement age 65:</h6>
                                                                </br>
                                                                <h5 class="stock-value">$2,631,804</h5>
                                                                <div id="stockbalance1" style="min-width: 100%; height: 315px; margin: 0 auto;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="element-box">
                                                                <h6 class="element-header">Here's a quick look at how your retirement is currently shaping up:</h6>
                                                                <br>
                                                                <p class="column-pera">Current weekly income(based on your salary) <span class="stock-value">$961,54</span></p>
                                                                <p class="column-pera">Forecast weekly income at retirement <span class="stock-value">$1,052,00</span></p>
                                                                <br>
                                                                <div id="bar-column" style="min-width: 100%; height: 280px; margin: 0 "></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="tabs-control">
                                                                <a class="nav-link active" id="tabhome224-tab" data-toggle="tab" href="#tabhome224" role="tab" aria-controls="tabhome224" aria-selected="false">
                                                                    Fund Details 
                                                                </a>
                                                            </div>
                                                            <div class="element-box">
                                                                <div class="element-wrapper">
                                                                    <h6 class="element-header"> Month in review  <a href="" class="fund-detail-btn">Download Full Update</a></h6>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12" >
                                                                        <h6><span>What is Lorem Ipsum?</span></h6>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                                                            when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                                                                            It has survived not only five centuries, but also the leap into electronic typesetting, 
                                                                            remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
                                                                            sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like 
                                                                            Aldus PageMaker including versions of Lorem Ipsum. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="tabs-control">
                                                                <a class="nav-link active" id="tabhome224-tab" data-toggle="tab" href="#tabhome224" role="tab" aria-controls="tabhome224" aria-selected="false">
                                                                    Features of the fund
                                                                </a>
                                                            </div>
                                                            <div class="element-box">
                                                                <!--                                                                <div class="element-wrapper">
                                                                                                                                    <h6 class="element-header"> Month in review  <a href="" class="fund-detail-btn">Download Full Update</a></h6>
                                                                                                                                </div>-->
                                                                <div class="row">
                                                                    <!--                                                                    <div class="col-md-12" >
                                                                                                                                            <h6><span>Our portfolio returned 5.01% for the month.</span></h6>
                                                                                                                                        </div>-->
                                                                    <div class="col-md-12">
                                                                        <div class="fund-content">
                                                                            <p>Provides investors with long-term capital growth.</p>
                                                                            <p>Has a risk profile between that of the property and equities asset classes</p>
                                                                            <p>Targeting sustainable returns above the CPI index +4.5% per annum, with moderate to high volatility.</p>
                                                                            <p>Will hold a combination of International and Australasian equities, fixed interest, listed property and cash</p>
                                                                            <p>Suitable for Investors with a minimum 5 year investment horizon.</p>
                                                                            <p>1.09% Management Fee (GST exclusive, no performance fee).</p>
                                                                            <p>Currency risk hedged at manager's discretion.</p>
                                                                        </div>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    </div>

                                                    </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    <div class="display-type"></div>
                                                    </div>
                                                    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" defer></script>
                                                    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" defer></script>
                                                    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" defer></script>
                                                    <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" defer></script>
                                                    <script src="https://backoffice.invsta.io/ui-fund/resources/bower_components/bootstrap/js/dist/util.js" defer></script>
                                                    <script src="https://backoffice.invsta.io/ui-fund/resources/bower_components/bootstrap/js/dist/tab.js" defer></script>
                                                    <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" defer></script>
                                                    <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
                                                    <script src="https://code.highcharts.com/highcharts.js" ></script> 
                                                    <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
                                                    <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
                                                    <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
                                                    <script src="https://code.highcharts.com/modules/cylinder.js"></script>
                                                    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
                                                    <script type="text/javascript">
                                                        $(window).load(function () {
                                                            $(".loader").fadeOut("fast");
                                                        });
                                                    </script>
                                                    <script>
                                                        function executeBEDB() {
                                                            $.ajax({
                                                                type: 'GET',
                                                                url: './rest/groot/db/api/beneficiary-dashboard?id=${id}',
                                                                headers: {"Content-Type": 'application/json'},
                                                                success: function (data, textStatus, jqXHR) {
                                                                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                    if (obj.investmentHoldings.length > 0
                                                                            && obj.investmentPerformances.length > 0
                                                                            && obj.fmcaInvestmentMix.length > 0) {
                                                                        dashboard(obj);
                                                                        $(".loader").hide();
                                                                    } else {
                                                                        $.ajax({
                                                                            type: 'GET',
                                                                            url: './rest/groot/db/api/investment-holdings?id=${id}',
                                                                            headers: {"Content-Type": 'application/json'},
                                                                            success: function (data, textStatus, jqXHR) {
                                                                                $(".loader").hide();
                                                                                var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                if (obj.investmentHoldings.length > 0) {
                                                                                    investmentHoldings(obj);
                                                                                    $.ajax({
                                                                                        type: 'GET',
                                                                                        url: './rest/groot/db/api/investment-performances?id=${id}',
                                                                                        headers: {"Content-Type": 'application/json'},
                                                                                        success: function (data, textStatus, jqXHR) {
                                                                                            console.log('success' + data);
                                                                                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                            if (obj.investmentPerformances.length > 0) {
                                                                                                investmentPerformances(obj);
                                                                                            }
                                                                                        },
                                                                                        error: function (jqXHR, textStatus, errorThrown) {
                                                                                            console.log('error');
                                                                                        }
                                                                                    });
                                                                                    $.ajax({
                                                                                        type: 'GET',
                                                                                        url: './rest/groot/db/api/fmca-investment-mix?id=${id}',
                                                                                        headers: {"Content-Type": 'application/json'},
                                                                                        success: function (data, textStatus, jqXHR) {
                                                                                            console.log('success' + data);
                                                                                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                            if (obj.fmcaInvestmentMix.length > 0) {
                                                                                                fmcaInvestmentMix(obj);
                                                                                            }
                                                                                        },
                                                                                        error: function (jqXHR, textStatus, errorThrown) {
                                                                                            console.log('error');
                                                                                        }
                                                                                    });
                                                                                } else {
//                                                                                    swal({
//                                                                                        title: "Info",
//                                                                                        text: "You have no investment yet",
//                                                                                        timer: 2000,
//                                                                                        showConfirmButton: false
//                                                                                    });
                                                                                }
                                                                            },
                                                                            error: function (jqXHR, textStatus, errorThrown) {
                                                                                console.log('error');
                                                                            }
                                                                        });
                                                                    }
                                                                },
                                                                error: function (jqXHR, textStatus, errorThrown) {
                                                                    console.log('error');
                                                                }
                                                            });
                                                        }

                                                        var investmentAmount = 0;
                                                        var CurrentValue = 0;
                                                        var portfolio = document.getElementById("portfolio");
                                                        var investments = document.getElementById("investments");
                                                        function investmentHoldings(obj) {
                                                            var investmentHoldings = obj.investmentHoldings;
                                                            var arr = [];
                                                            var fundMap = new Map();
                                                            for (var i = 0; i < investmentHoldings.length; i++) {
                                                                var performances = investmentHoldings[i];
                                                                var clone = portfolio.cloneNode(true);
                                                                var h6 = clone.getElementsByClassName("pcName")[0];
                                                                h6.innerHTML = performances.InvestmentCode + " :: " + performances.PortfolioName;
                                                                var cont = clone.getElementsByClassName("contribution")[0];
                                                                cont.innerHTML = '$' + performances.Contributions.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                                                investmentAmount = investmentAmount + performances.Contributions;
                                                                var marVal = clone.getElementsByClassName("marketValue")[0];
                                                                marVal.innerHTML = '$' + performances.MarketValue.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                                                ;
                                                                CurrentValue = CurrentValue + performances.MarketValue;
                                                                clone.style.display = 'block';
                                                                var href = './show-me-more-db-' + performances.InvestmentCode + '?pc=' + performances.PortfolioCode;
                                                                clone.getElementsByClassName("showmemore")[0].setAttribute('href', href);
                                                                arr.push([performances.PortfolioName, performances.MarketValue]);
                                                                fundMap[performances.PortfolioCode] = performances.PortfolioName;
                                                                investments.appendChild(clone);
                                                            }
                                                            document.getElementsByClassName("currentBalance")[0].innerHTML = "$" + investmentAmount.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                                            document.getElementsByClassName("sumWalletBal")[0].innerHTML = "$" + CurrentValue.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                                            pieChart('investmentBreakdown', arr, true);
                                                        }

                                                        function investmentPerformances(obj) {
                                                            var investmentPerformances = obj.investmentPerformances;
                                                            var performanceMap = new Map();
                                                            var fundMap = new Map();
                                                            var keys = new Array();
                                                            for (var i = 0; i < investmentPerformances.length; i++) {
                                                                var performance = investmentPerformances[i];
                                                                var PortfolioArray = performanceMap[performance.Portfolio];
                                                                if (typeof PortfolioArray === 'undefined') {
                                                                    PortfolioArray = new Array();
                                                                    keys.push(performance.Portfolio);
                                                                }
                                                                PortfolioArray.push(performance);
                                                                performanceMap[performance.Portfolio] = PortfolioArray;
                                                            }
                                                            var avgSeries = [],
                                                                    ago1YearSeries = [],
                                                                    ago2YearSeries = [],
                                                                    ago3YearSeries = [];
                                                            for (var key of keys) {//3 Keys
                                                                var PortfolioArray = performanceMap[key]; var fundName = fundMap[key];
                                                                if (typeof fundName === 'undefined') {
                                                                    fundName = key;
                                                                }
                                                                var avgPeriod = PortfolioArray[PortfolioArray.length - 1];
                                                                var avgRR = avgPeriod.XIRRReturnRate * 100;
                                                                avgSeries.push({
                                                                    name: fundName,
                                                                    data: [avgRR]
                                                                });
                                                                var ago1YearPeriod = PortfolioArray[PortfolioArray.length - 2];
                                                                var ago1YearRR = ago1YearPeriod.XIRRReturnRate * 100;
                                                                ago1YearSeries.push({
                                                                    name: fundName,
                                                                    data: [ago1YearRR]
                                                                });
                                                                var ago2YearPeriod = PortfolioArray[PortfolioArray.length - 3];
                                                                var ago2YearRR = ago2YearPeriod.XIRRReturnRate * 100;
                                                                ago2YearSeries.push({
                                                                    name: fundName,
                                                                    data: [ago2YearRR]
                                                                });
                                                                var ago3YearPeriod = PortfolioArray[PortfolioArray.length - 4];
                                                                var ago3YearRR = ago3YearPeriod.XIRRReturnRate * 100;
                                                                ago3YearSeries.push({
                                                                    name: fundName,
                                                                    data: [ago3YearRR]
                                                                });
                                                            }
                                                            barChart('avgBarChart', avgSeries);
                                                            barChart('ago1YearsBarChart', ago1YearSeries);
                                                            barChart('ago2YearsBarChart', ago2YearSeries);
                                                            barChart('ago3YearsBarChart', ago3YearSeries);
                                                        }

                                                        function fmcaInvestmentMix(obj) {
                                                            var assetAllocation = [];
                                                            var fmcaInvestmentMix = obj.fmcaInvestmentMix;
                                                            //                    var fmcaInvestmentMixByTable = obj.fmcaInvestmentMixByTable;
                                                            //                    $.each(fmcaInvestmentMix, function (idx, val) {
                                                            //                        assetAllocation.push([val.FMCAAssetClass, val.SectorValueBase]);
                                                            //                    });
                                                            $.each(fmcaInvestmentMix, function (idx, val) {
                                                                assetAllocation.push([val.FMCAAssetClass, val.SectorValueBase]);
                                                            });
                                                            pieChart('assetAllocation', assetAllocation, false);
                                                        }

                                                        function dashboard(obj) {
                                                            investmentHoldings(obj);
                                                            investmentPerformances(obj);
                                                            fmcaInvestmentMix(obj);
                                                        }

                                                        executeBEDB();
                                                </script>
                                                <script>
                                                    function pieChart(idc, arr, mktval) {
                                                        Highcharts.setOptions({
                                                            colors: ['#54565a', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
                                                        });
                                                        var ptfmt = '';
                                                        if (mktval) {
                                                            ptfmt = '{series.name} <br>{point.percentage:.2f} % <br>Market Value: {point.y}';
                                                        } else {
                                                            ptfmt = '{series.name} <br>{point.percentage:.2f} %';
                                                        }
                                                        Highcharts.chart(idc, {
                                                            chart: {
                                                                type: 'pie',
                                                                options3d: {
                                                                    enabled: true,
                                                                    alpha: 0
                                                                }
                                                            },
                                                            credits: {
                                                                enabled: false,
                                                            },
                                                            exporting: {
                                                                enabled: false,
                                                            },
                                                            legend: {
                                                                enabled: true,
                                                            },
                                                            title: {
                                                                text: ''
                                                            },
                                                            subtitle: {
                                                                text: ''
                                                            },
                                                            tooltip: {
                                                                pointFormat: ptfmt
                                                            },
                                                            plotOptions: {
                                                                pie: {
                                                                    innerSize: 160,
                                                                    depth: 45,
                                                                    dataLabels: {enabled: false,
                                                                        format: '<b>{point.name}</b>: {point.percentage:.1f} %<br>Value: {point.y}',
                                                                        style: {
                                                                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                                        }
                                                                    },
                                                                },
                                                            },
                                                            series: [{
                                                                    showInLegend: true,
                                                                    name: '',
                                                                    data: arr
                                                                }]
                                                        });
                                                    }

                                                    function barChart(idc, series) {
                                                        Highcharts.setOptions({
                                                            colors: ['#54565a', '#ad4d3c', '#b9bbbe']
                                                        });
                                                        Highcharts.chart(idc, {
                                                            chart: {
                                                                type: 'column'
                                                            },
                                                            title: {
                                                                text: ''
                                                            },
                                                            subtitle: {
                                                                text: ''},
                                                            xAxis: {
                                                                categories: [''],
                                                                crosshair: true
                                                            },
                                                            yAxis: {
                                                                min: 0,
                                                                max: 30,
                                                                tickInterval: 5,
                                                                labels: {
                                                                    format: '{value}%'
                                                                },
                                                                title: {
                                                                    text: ''
                                                                }
                                                            },
                                                            tooltip: {
                                                                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                                        '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                                                                footerFormat: '</table>',
                                                                shared: true,
                                                                useHTML: true
                                                            },
                                                            credits: {
                                                                enabled: false
                                                            },
                                                            navigation: {
                                                                buttonOptions: {
                                                                    enabled: false
                                                                }
                                                            },
                                                            plotOptions: {
                                                                column: {
                                                                    pointPadding: 0,
                                                                    borderWidth: 0
                                                                },
                                                                series: {
                                                                    borderWidth: 0,
                                                                    stroke: 0,
                                                                }
                                                            },
                                                            series: series
                                                        });
                                                    }
                                                </script>
                                                <script>
                                                    Highcharts.chart('stockbalance', {
                                                        chart: {
                                                            type: 'spline',
                                                        },
                                                        title: {
                                                            text: ''
                                                        },
                                                        subtitle: {

                                                        },
                                                        xAxis: {
                                                            categories: [
                                                                '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019'],
                                                            tickInterval: 1,
                                                            labels: {
                                                                style: {
                                                                    color: '#333',
                                                                    fontSize: '12px',
                                                                    textTransform: 'uppercase'
                                                                },
                                                                y: 20,
                                                                x: 10
                                                            },
                                                            lineColor: '#dadada'
                                                        },
                                                        yAxis: {
                                                            labels: {
                                                                formatter: function () {
                                                                    if (this.value >= 1E6) {
                                                                        return '$' + this.value / 1000000 + 'M';
                                                                    }
                                                                    return '$' + this.value / 1000 + 'k';
                                                                }
                                                            },

                                                            min: 5000, tickInterval: 500000, max: 1300000,
                                                            title: {
                                                                text: ''
                                                            }
                                                        },
                                                        credits: {
                                                            enabled: false
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        legend: {
                                                            layout: 'vertical',
                                                            align: 'center',
                                                            verticalAlign: 'bottom',
                                                            enabled: false

                                                        },
                                                        plotOptions: {
                                                            series: {
                                                                color: '#2c94ec',
                                                                shadow: true,
                                                                lineWidth: 3,
                                                                marker: {
                                                                    enabled: false
                                                                }
                                                            }
                                                        },
                                                        tooltip: {
                                                            pointFormatter: function () {
                                                                var isNegative = this.y < 0 ? '-' : '';
                                                                return  isNegative + '$' + Math.abs(this.y.toFixed(0));
                                                            }
                                                        },
                                                        series: [{
                                                                name: '',
                                                                data: [50000,
                                                                    90688 + 5000,
                                                                    100000,
                                                                    300000,
                                                                    350000,
                                                                    500000,
                                                                    700000,
                                                                    800000,
                                                                    1100000,
                                                                    1300000]
                                                            }],
                                                        responsive: {
                                                            rules: [{
                                                                    condition: {
                                                                        maxWidth: 500
                                                                    }
                                                                }]
                                                        }

                                                    });
                                                </script>        

                                                <script>
                                                    Highcharts.setOptions({
                                                        colors: ['#65b9ac']});
                                                    Highcharts.chart('lg-latestweek1', {
                                                        title: {
                                                            text: ''
                                                        },
                                                        legend: {
                                                            enabled: false
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        xAxis: {
                                                            categories: [''],
                                                            title: {
                                                                text: null
                                                            },
                                                            labels: {enabled: false, }

                                                        },
                                                        yAxis: {
                                                            gridLineWidth: 0,
                                                            labels: {enabled: false, }, title: {
                                                                text: ''
                                                            },
                                                        },
                                                        credits: {
                                                            enabled: false
                                                        },
                                                        series: [{
                                                                name: 'Investment Balance',
                                                                data: [10000, 9000, 9999, 8000, 9600, 13365.55]
                                                            }],
                                                    });
                                                </script>
                                                <script>
                                                    Highcharts.chart('lg-latestweek2', {
                                                        title: {
                                                            text: ''
                                                        },
                                                        legend: {
                                                            enabled: false
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        xAxis: {
                                                            categories: [''],
                                                            title: {
                                                                text: null
                                                            },
                                                            labels: {enabled: false, }

                                                        },
                                                        yAxis: {
                                                            gridLineWidth: 0,
                                                            labels: {enabled: false, }, title: {
                                                                text: ''
                                                            },
                                                        },
                                                        credits: {
                                                            enabled: false
                                                        },
                                                        series: [{
                                                                name: 'Investment Balance',
                                                                data: [10000, 9700, 8700, 9280, 8900, 9758.43]
                                                            }],
                                                    });
                                                </script>
                                                <script>
                                                    Highcharts.chart('lg-latestweek3', {
                                                        title: {
                                                            text: ''
                                                        },
                                                        legend: {
                                                            enabled: false
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        xAxis: {
                                                            categories: [''],
                                                            title: {
                                                                text: null
                                                            },
                                                            labels: {enabled: false, }

                                                        },
                                                        yAxis: {
                                                            gridLineWidth: 0,
                                                            labels: {enabled: false, }, title: {
                                                                text: ''
                                                            },
                                                        },
                                                        credits: {
                                                            enabled: false
                                                        },
                                                        series: [{
                                                                name: 'Investment Balance',
                                                                data: [10000, 9700, 8500, 9280, 9100, 10898.47]
                                                            }],
                                                    });
                                                </script>
                                                <script>
                                                    Highcharts.chart('lg-latestweek4', {
                                                        title: {
                                                            text: ''
                                                        },
                                                        legend: {
                                                            enabled: false
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        xAxis: {
                                                            categories: [''],
                                                            title: {
                                                                text: null
                                                            },
                                                            labels: {enabled: false, }

                                                        },
                                                        yAxis: {
                                                            gridLineWidth: 0,
                                                            labels: {enabled: false, }, title: {
                                                                text: ''
                                                            },
                                                        },
                                                        credits: {
                                                            enabled: false
                                                        },
                                                        series: [{
                                                                name: 'Investment Balance',
                                                                data: [10000, 8700, 9000, 9280, 8900, 10258.28]
                                                            }],
                                                    });
                                                </script>
                                                <script>
                                                    Highcharts.chart('lg-latestweek5', {
                                                        title: {
                                                            text: ''
                                                        },
                                                        legend: {
                                                            enabled: false
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        xAxis: {
                                                            categories: [''],
                                                            title: {
                                                                text: null
                                                            },
                                                            labels: {enabled: false, }

                                                        },
                                                        yAxis: {
                                                            gridLineWidth: 0,
                                                            labels: {enabled: false, }, title: {
                                                                text: ''
                                                            },
                                                        },
                                                        credits: {
                                                            enabled: false
                                                        },
                                                        series: [{
                                                                name: 'Investment Balance',
                                                                data: [10000, 9700, 8700, 9280, 8900, 11540.38]
                                                            }],
                                                    });
                                                </script>
                                                <script>
                                                    Highcharts.chart('lg-latestweek6', {
                                                        title: {
                                                            text: ''
                                                        },
                                                        legend: {
                                                            enabled: false
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        xAxis: {
                                                            categories: [''],
                                                            title: {
                                                                text: null
                                                            },
                                                            labels: {enabled: false, }

                                                        },
                                                        yAxis: {
                                                            gridLineWidth: 0,
                                                            labels: {enabled: false, }, title: {
                                                                text: ''
                                                            },
                                                        },
                                                        credits: {
                                                            enabled: false
                                                        },
                                                        series: [{
                                                                name: 'Investment Balance',
                                                                data: [10000, 9700, 8700, 9280, 8900, 11629.15]
                                                            }],
                                                    });
                                                </script>
                                                <script>
                                                    Highcharts.chart('lg-latestweek7', {
                                                        title: {
                                                            text: ''
                                                        },
                                                        legend: {
                                                            enabled: false
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        xAxis: {
                                                            categories: [''],
                                                            title: {
                                                                text: null
                                                            },
                                                            labels: {enabled: false, }

                                                        },
                                                        yAxis: {
                                                            gridLineWidth: 0,
                                                            labels: {enabled: false, }, title: {
                                                                text: ''
                                                            },
                                                        },
                                                        credits: {
                                                            enabled: false
                                                        },
                                                        series: [{
                                                                name: 'Investment Balance',
                                                                data: [10000, 9700, 8596, 9280, 10890, 12545.00]
                                                            }],
                                                    });
                                                </script>
                                                <script>
                                                    Highcharts.chart('composition-chart', {
                                                        chart: {
                                                            type: 'spline',
                                                        },
                                                        credits: {
                                                            enabled: false,
                                                        },
                                                        title: {
                                                            text: ''
                                                        },
                                                        subtitle: {
                                                            text: ''
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        yAxis: {
                                                            title: {
                                                                text: 'Performance'
                                                            }
                                                        },
                                                        series: [{
                                                                name: 'Magellan Global Equities Fund (MGE)',
                                                                data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
                                                            }, {
                                                                name: 'Magellan Global Equities Fund (Currency Hedged) (MHG)',
                                                                data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
                                                            }, {
                                                                name: 'Magellan Global Trust (MGG)',
                                                                data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
                                                            }, {
                                                                name: 'Magellan Global Fund',
                                                                data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
                                                            }, {
                                                                name: 'Magellan Global Fund (Hedged)',
                                                                data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]

                                                            }]
                                                    });
                                                </script>
                                                <script>
                                                    // Build the chart
                                                    Highcharts.setOptions({
                                                        colors: ['#93dbd0', '#2d2a26', '#799fc3', '#c64b38', '#5ca695', '#1be9b9']
                                                    });
                                                    Highcharts.chart('container19', {
                                                        chart: {
                                                            plotBackgroundColor: null,
                                                            plotBorderWidth: null,
                                                            plotShadow: false,
                                                            type: 'pie'
                                                        },
                                                        title: {
                                                            text: ''
                                                        },
                                                        tooltip: {
                                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                        },
                                                        credits: {
                                                            enabled: false,
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        plotOptions: {
                                                            pie: {
                                                                size: 200,
                                                                innerSize: 150,
                                                                depth: 45,
                                                                allowPointSelect: true,
                                                                cursor: 'pointer',
                                                                dataLabels: {
                                                                    distance: 2,
                                                                    connectorWidth: 0,
                                                                    enabled: false,
                                                                    format: '{point.percentage:.1f} %'
                                                                },
                                                                allowPointSelect: true,
                                                                cursor: 'pointer',
                                                                dataLabels: {
                                                                    enabled: false
                                                                },
                                                                showInLegend: false
                                                            }
                                                        },
                                                        series: [{
                                                                name: 'Brands',
                                                                colorByPoint: true,
                                                                data: [{
                                                                        name: 'International Equities ',
                                                                        y: 70
                                                                    }]
                                                            }]
                                                    });
                                                </script>
                                                <script>
                                                    Highcharts.chart('stockbalance1', {
                                                        chart: {
                                                            type: 'spline',
                                                        },
                                                        title: {
                                                            text: ''
                                                        },
                                                        subtitle: {

                                                        },
                                                        xAxis: {
                                                            categories: [
                                                                '35', '40', '45', '50', '55', '60', '65'],
                                                            tickInterval: 1,
                                                            labels: {
                                                                style: {
                                                                    color: '#333',
                                                                    fontSize: '12px',
                                                                    textTransform: 'uppercase'
                                                                },
                                                                y: 20,
                                                                x: 10
                                                            },
                                                            lineColor: '#dadada'
                                                        },
//                                                        yAxis: {
//                                                            title: {
//                                                                text: 'KiwiSaver current balance'
//                                                            }
//                                                        },
                                                        yAxis: {
                                                            labels: {
                                                                formatter: function () {
                                                                    if (this.value >= 1E6) {
                                                                        return '$' + this.value / 1000000 + 'M';
                                                                    }
                                                                    return '$' + this.value / 1000 + 'M';
                                                                }
                                                            },

                                                            min: 0, tickInterval: 1000000, max: 3000000,
                                                            title: {
                                                                text: ''
                                                            }
                                                        },
                                                        credits: {
                                                            enabled: false
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        legend: {
                                                            layout: 'vertical',
                                                            align: 'center',
                                                            verticalAlign: 'bottom',
                                                            enabled: false

                                                        },
                                                        plotOptions: {
                                                            series: {
                                                                color: '#2c94ec',
                                                                shadow: true,
                                                                lineWidth: 3,
                                                                marker: {
                                                                    enabled: false
                                                                }
                                                            }
                                                        },
                                                        tooltip: {
                                                            pointFormatter: function () {
                                                                var isNegative = this.y < 0 ? '-' : '';
                                                                return  isNegative + '$' + Math.abs(this.y.toFixed(0));
                                                            }
                                                        },
                                                        series: [{
                                                                name: 'KiwiSaver current balance',
                                                                data: [300000,
                                                                   750000,
                                                                   950000,
                                                                   1100000,
                                                                    1500000,
                                                                    2100000,
                                                                    3000000]
                                                            }],
                                                        responsive: {
                                                            rules: [{
                                                                    condition: {
                                                                        maxWidth: 500
                                                                    }
                                                                }]
                                                        }

                                                    });
                                                </script> 
                                                <script>
                                                    Highcharts.setOptions({
                                                        colors: ['#2c94ec', '#2d373e', '#575f65']
                                                    });
                                                    Highcharts.chart('bar-column1', {
                                                        chart: {
                                                            type: 'column'
                                                        },
                                                        title: {
                                                            text: ''
                                                        },
                                                        subtitle: {
                                                            text: ''
                                                        },
                                                        xAxis: {
                                                            categories: [
                                                                ''
                                                            ],
                                                        },
                                                        yAxis: {
                                                            labels: {
                                                                formatter: function () {
                                                                    return '$' + this.axis.defaultLabelFormatter.call(this);
                                                                }
                                                            },
                                                            min: 0, max: 1500,
                                                            title: {
                                                                text: 'Weekly income ($)'
                                                            }
                                                        },
                                                        credits: {
                                                            enabled: false
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        tooltip: {
                                                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                                    '<td style="padding:0"><b>{point.y:.1f} $</b></td></tr>',
                                                            footerFormat: '</table>',
                                                            shared: true,
                                                            useHTML: true
                                                        },
                                                        plotOptions: {
                                                            column: {
                                                                pointPadding: 0.2,
                                                                borderWidth: 0,
                                                            }
                                                        },
                                                        series: [{
                                                                name: 'Current weekly income',
                                                                data: [1000],
                                                            }, {
                                                                name: 'Kiwisaver top up',
                                                                data: [250]

                                                            }, {
                                                                name: 'NZ Super Weekly',
                                                                data: [1000]

                                                            }]
                                                    });
                                                </script> 
                                                <script>
                                                    Highcharts.setOptions({
                                                        colors: ['#2c94ec', '#2d373e', '#575f65']
                                                    });
                                                    Highcharts.chart('bar-column', {
                                                        chart: {
                                                            type: 'column'
                                                        },
                                                        title: {
                                                            text: ''
                                                        },
                                                        xAxis: {
                                                            labels: {
                                                                enabled: false
                                                            },
                                                            categories: [
                                                                ''
                                                            ]
                                                        },
                                                        yAxis: {
                                                            labels: {
                                                                formatter: function () {
                                                                    return '$' + this.axis.defaultLabelFormatter.call(this);
                                                                }
                                                            },
                                                            min: 0, max: 1500,
                                                            title: {
                                                                text: 'Weekly income ($)'
                                                            }
                                                        },
                                                        credits: {
                                                            enabled: false
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        tooltip: {
                                                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                                    '<td style="padding:0"><b>{point.y:.1f} $</b></td></tr>',
                                                            footerFormat: '</table>',
                                                            shared: true,
                                                            useHTML: true
                                                        },
                                                        plotOptions: {
                                                            column: {
                                                                stacking: '$',

                                                            }
                                                        },
                                                        series: [{
                                                                name: 'Current income',
                                                                data: [1400, 0]
                                                            }, {
                                                                name: 'KiwiSaver top up',
                                                                data: [0, 450]
                                                            }, {
                                                                name: 'NZ Super Weekly',
                                                                data: [0, 602]
                                                            }]
                                                    });
                                                </script> 
                                                <script>
                                                    Highcharts.setOptions({
                                                        colors: ['#2c94ec', '#2d373e', '#575f65']
                                                    });
                                                    // Build the chart
                                                    Highcharts.chart('pie-1', {
                                                        chart: {
                                                            plotBackgroundColor: null,
                                                            plotBorderWidth: null,
                                                            plotShadow: false,
                                                            type: 'pie',
                                                            margin: [0, 0, 0, 0],
                                                            spacingTop: 0,
                                                            spacingBottom: 0,
                                                            spacingLeft: 0,
                                                            spacingRight: 0
                                                        },
                                                        title: {
                                                            text: '100%',
                                                            verticalAlign: 'middle',
                                                            floating: true,
                                                            y: 20,
                                                        },
                                                        tooltip: {
                                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                                                            enabled: false,
                                                        },
                                                        legend: {enabled: false},
                                                        credits: {
                                                            enabled: false
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        plotOptions: {
                                                            pie: {
                                                                innerSize: 70,
                                                                depth: 0,
                                                                allowPointSelect: true,
                                                                cursor: 'pointer',
                                                                dataLabels: {
                                                                    enabled: false
                                                                },
                                                                showInLegend: true
                                                            }
                                                        },
                                                        series: [{
                                                                name: 'Fund Performance',
                                                                colorByPoint: true,
                                                                data: [{
                                                                        name: 'Current weekly income',
                                                                        y: 7
                                                                    }]
                                                            }]
                                                    });
                                                </script> 
                                                <script>
                                                    Highcharts.setOptions({
                                                        colors: ['#2c94ec', '#2d373e', '#575f65']
                                                    });
                                                    // Build the chart
                                                    Highcharts.chart('pie-2', {
                                                        chart: {
                                                            plotBackgroundColor: null,
                                                            plotBorderWidth: null,
                                                            plotShadow: false,
                                                            type: 'pie',
                                                            margin: [0, 0, 0, 0],
                                                            spacingTop: 0,
                                                            spacingBottom: 0,
                                                            spacingLeft: 0,
                                                            spacingRight: 0
                                                        },
                                                        title: {
                                                            text: '2.07%',
                                                            verticalAlign: 'middle',
                                                            floating: true,
                                                            y: 20,
                                                        },
                                                        tooltip: {
                                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                                                            enabled: false,
                                                        },
                                                        legend: {enabled: false},
                                                        credits: {
                                                            enabled: false
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        plotOptions: {
                                                            pie: {
                                                                innerSize: 70,
                                                                depth: 0,
                                                                allowPointSelect: true,
                                                                cursor: 'pointer',
                                                                dataLabels: {
                                                                    enabled: false
                                                                },
                                                                showInLegend: true
                                                            }
                                                        },
                                                        series: [{
                                                                name: 'Fund Performance',
                                                                colorByPoint: true,
                                                                data: [{
                                                                        name: 'Current weekly income',
                                                                        y: 7
                                                                    }, {
                                                                        name: 'KiwiSaver top up',
                                                                        y: 3
                                                                    }]
                                                            }]
                                                    });
                                                </script> 
                                                <script>
                                                    Highcharts.setOptions({
                                                        colors: ['#2c94ec', '#2d373e', '#575f65']
                                                    });
                                                    // Build the chart
                                                    Highcharts.chart('pie-3', {
                                                        chart: {
                                                            plotBackgroundColor: null,
                                                            plotBorderWidth: null,
                                                            plotShadow: false,
                                                            type: 'pie',
                                                            margin: [0, 0, 0, 0],
                                                            spacingTop: 0,
                                                            spacingBottom: 0,
                                                            spacingLeft: 0,
                                                            spacingRight: 0
                                                        },
                                                        title: {
                                                            text: '15.29%',
                                                            verticalAlign: 'middle',
                                                            floating: true,
                                                            y: 20,
                                                        },
                                                        tooltip: {
                                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                                                            enabled: false,
                                                        },
                                                        legend: {enabled: false},
                                                        credits: {
                                                            enabled: false
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        plotOptions: {
                                                            pie: {
                                                                innerSize: 70,
                                                                depth: 0,
                                                                allowPointSelect: true,
                                                                cursor: 'pointer',
                                                                dataLabels: {
                                                                    enabled: false
                                                                },
                                                                showInLegend: true
                                                            }
                                                        },
                                                        series: [{
                                                                name: 'Fund Performance',
                                                                colorByPoint: true,
                                                                data: [{
                                                                        name: 'Current weekly income',
                                                                        y: 7
                                                                    }, {
                                                                        name: 'KiwiSaver top up',
                                                                        y: 3
                                                                    }]
                                                            }]
                                                    });
                                                </script> 
                                                <script>
                                                    Highcharts.setOptions({
                                                        colors: ['#2c94ec', '#2d373e', '#575f65']
                                                    });
                                                    // Build the chart
                                                    Highcharts.chart('pie-4', {
                                                        chart: {
                                                            plotBackgroundColor: null,
                                                            plotBorderWidth: null,
                                                            plotShadow: false,
                                                            type: 'pie',
                                                            margin: [0, 0, 0, 0],
                                                            spacingTop: 0,
                                                            spacingBottom: 0,
                                                            spacingLeft: 0,
                                                            spacingRight: 0
                                                        },
                                                        title: {
                                                            text: '34%',
                                                            verticalAlign: 'middle',
                                                            floating: true,
                                                            y: 20,
                                                        },
                                                        tooltip: {
                                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                                                            enabled: false,
                                                        },
                                                        legend: {enabled: false},
                                                        credits: {
                                                            enabled: false
                                                        },
                                                        navigation: {
                                                            buttonOptions: {
                                                                enabled: false
                                                            }
                                                        },
                                                        plotOptions: {
                                                            pie: {
                                                                innerSize: 70,
                                                                depth: 0,
                                                                allowPointSelect: true,
                                                                cursor: 'pointer',
                                                                dataLabels: {
                                                                    enabled: false
                                                                },
                                                                showInLegend: true
                                                            }
                                                        },
                                                        series: [{
                                                                name: 'Fund Performance',
                                                                colorByPoint: true,
                                                                data: [{
                                                                        name: 'Current weekly income',
                                                                        y: 7
                                                                    }, {
                                                                        name: 'KiwiSaver top up',
                                                                        y: 3
                                                                    }]
                                                            }]
                                                    });
                                                </script> 
                                                </body>
                                                </html>