
<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<html>
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link rel="icon" href="./resources/images/invsta-color-logo.png" type="image/gif" sizes="16x16">
        <link href="http://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <!--<link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>-->
        <link href="http://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
        <!--<link href="http://backoffice.invsta.io/pocv/resources/css/main.css?version=3.5.1" rel="stylesheet"/>-->
        <link href="http://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>
        <!-- <link href="resources/css/osfonts.css" rel="stylesheet"> -->
        <link href="http://backoffice.invsta.io/pocv/resources/css/custom.css" rel="stylesheet"/>
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">-->
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link href="./resources/css/newcss/new-main.css" rel="stylesheet"/>
        <link href="./resources/css/sweetalert.css" rel="stylesheet" />

    </head>
    <body>
        <!--        <div class="loader"></div>-->
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <span><a href="./home">Home</a></span>
                        </li>
                        <li class="breadcrumb-item">
                            <span class="right-mark"><i class="fa fa-caret-right"></i></span>
                        </li>
                        <li class="breadcrumb-item">
                            <span><a href="javascript:void(0)">Investment Fund</a></span>
                        </li>

                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box" style="display: block">
                            <div class="Portfolio-page">


                                <div class="modal fade" id="add-investment-fund-modal-lg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" ria-hiddena="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">${portfolioDetail.fundName} </h5>

                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Beneficiary Name :</label><br>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <select name="beneficiaryId" id="beneficiaryId"  class="form-group">
                                                                <option >-select-</option>

                                                                <option value="${category.getId()}">${category.getName()}</option>

                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Investment Name :</label><br>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" id="investmentName" name ="investmentName" pattern="Enter Investment Name"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Investment Amount :</label><br>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="number" id="investmentAmount" name ="investmentAmount" pattern="Enter Investment Amount" min="0"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 bankaccounthide">
                                                        <div class="form-group">
                                                            <label>Bank Account Details:</label><br>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div  class="bankaccountdiv">
                                                    <input type="hidden" name="id" class="bankid" value="${bank.getId()}">
                                                    <input type="hidden" name="id" class="beneficiarid" value="${bank.getBeneficiaryId()}">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group model-name">
                                                                Account Name
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group accountname">
                                                                ${bank.getAccountName()}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group model-name">
                                                                Bank
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group bankname">
                                                                ${bank.getBank()}  
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group model-name">
                                                                Branch
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group branchname">
                                                                ${bank.getBranch()} 
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group model-name">
                                                                Account 
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group account">
                                                                ${bank.getAccount()} 
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="btn-pay">
                                                                <button type="button" class="btn btn-primary saveInvestmentbtn pay-model">pay</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>


                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary close-model" data-dismiss="modal">Close</button>
                                                <button type="button" id="saveInvestmentbtn" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="container2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="element-box funds-radius">
                                                <div class="funds-line">
                                                    <div class="funds-number">
                                                        <span class="fund-box"></span> <span class="fund-name-data">Enhanced Cash Fund</span> 
                                                    </div>
                                                    <div class="funds-number">
                                                        <span class="fund-box fund-box-2"></span> <span class="fund-name-data">Corporate Bond Fund</span> 
                                                    </div>
                                                    <div class="funds-number">
                                                        <span class="fund-box fund-box-3"></span> <span class="fund-name-data">Australasian Diversified Share Fund</span> 
                                                    </div>
                                                    <div class="funds-number">
                                                        <span class="fund-box fund-box-4"></span> <span class="fund-name-data">Property Fund</span> 
                                                    </div>
                                                </div>
                                                <div class="new-invest-btn">
                                                    <a href="" class="invest-btn-style">Make Investments</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="invest-fund-header">
                                                <h2 class="fund-size">Property Fund</h2>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="fund-live-value">
                                                <div class="price-value">
                                                    <span class="price-name">Unit Price</span> <span class="value-name"><i class="fa fa-arrow-up" aria-hidden="true"></i> $10</span>
                                                </div>
                                                <div class="price-value">
                                                    <span class="price-name">Fund Size</span> <span class="value-name"><i class="fa fa-arrow-up" aria-hidden="true"></i> $145,7m</span>
                                                </div>
                                                <div class="price-value">
                                                    <span class="price-name">1 Year Return</span> <span class="value-name"><i class="fa fa-arrow-up" aria-hidden="true"></i> 10.8%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row Portfolio-page-text">
                                        <div class="col-sm-12 col-md-12">
                                            <div class="element-wrapper">
                                                <h6 class="new-design-hearder">Performance Since Inception </h6>

                                            </div>

                                            <div class="char_section1">
                                                <div class="element-box">
                                                    <div class="new-invest-chart">
                                                        <div class="inception-section">
                                                            <h5>Invested Since Inception</h5>
                                                            <h4>$75,790</h4>
                                                        </div>
                                                        <div class="month-section">
                                                            <div class="month-section">
                                                                <span class="mont-drop">Year </span> 
                                                                <select class="date-option">
                                                                    <option>2019</option>
                                                                    <option>2018</option>
                                                                    <option>2017</option>
                                                                    <option>2016</option>
                                                                </select>
                                                            </div>
                                                            <div class="month-section">
                                                                <span class="mont-drop">Month </span> 
                                                                <select class="date-option">
                                                                    <option>Feb</option>
                                                                    <option>Jan</option>
                                                                    <option>Dec</option>
                                                                    <option>Nov</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--<div id="fundPerformanceChart1" style="height: 400px; overflow: hidden;" data-highcharts-chart="1"></div>-->
                                                    <div id="stockbalance" style="min-width: 100%; height: 400px; margin: 0 auto;"></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 mt-3">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Fund Performance </h6>
                                                        </div>
                                                        <div id="elementboxcontent" class="new-perform">
                                                            <div class="row">
                                                                <div class="col-md-3 col-sm-3 mb-4">
                                                                    <div class="element-box el-tablo centered trend-in-corner smaller performance-sec">
                                                                        <div class="label" >
                                                                            1 Month
                                                                        </div>
                                                                        <div class="value">
                                                                            <div class="down-data" id="1-month" >
                                                                                -1.03%
                                                                            </div>
                                                                            <!--<p class="date_upd"> as at 31 mar 2019</p>-->
                                                                        </div>

                                                                        <div class="trending trending-up 1-monthTrending" hidden>
                                                                            <span>11.95</span>
                                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                                        </div> 


                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 mb-4">
                                                                    <div class="element-box el-tablo centered trend-in-corner smaller performance-sec">
                                                                        <div class="label">
                                                                            3 Months
                                                                        </div>
                                                                        <div class="value">
                                                                            <div class="down-data" id="3-month" >
                                                                                7.63
                                                                            </div>
                                                                            <!--<p class="date_upd"> as at 31 mar 2019</p>-->
                                                                        </div>



                                                                        <div class="trending trending-up 3-monthTrending" hidden>
                                                                            <span>10.78</span>
                                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                                        </div> 

                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 mb-4">
                                                                    <div class="element-box el-tablo centered trend-in-corner smaller performance-sec">
                                                                        <div class="label">
                                                                            1 Year
                                                                        </div>
                                                                        <div class="value">
                                                                            <div class="down-data" id="1-year" >
                                                                                14.08
                                                                            </div>
                                                                            <!--<p class="date_upd"> as at 31 mar 2019</p>-->
                                                                        </div>


                                                                        <div class="trending trending-up 1-yearTrending" hidden>
                                                                            <span>24.04</span>
                                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                                        </div> 


                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 mb-4">
                                                                    <div class="element-box el-tablo centered trend-in-corner smaller performance-sec">
                                                                        <div class="label">
                                                                            5 Years
                                                                        </div>
                                                                        <div class="value">
                                                                            <div class="down-data" id="5-year" >
                                                                                14.61
                                                                            </div>
                                                                            <!--<p class="date_upd"> as at 31 mar 2019</p>--> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Fund Overview </h6>
                                                        </div>
                                                        <div id="elementboxcontent" class="element-box">
                                                            <div class="row">
                                                                <div class="col-md-12 update-fund-pera" id="paragraph">
                                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>                                                                   
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Risk Indicator </h6>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="element-box risk-radius">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <!--                                                                    <div class="risk-indicator">
                                                                                                                                           <div class="element-wrapper">
                                                                                                                                                <h6 class="element-header">Risk Indicator   <div class="pulsating-circle circle-2" data-toggle="tooltip" title="Managed funds in New Zealand must have a standard risk indicator. The risk indicator is designed to help investors understand the uncertainties both for loss and growth that may affect their investment. You can compare funds using the risk indicator. The risk indicator is rated from 1 (low) to 7 (high).The rating reflects how much the value of the fund's assets goes up and down (volatility). A higher risk generally means higher potential returns over time, but more ups and downs along the way. To help you clarify your own attitude to risk, you can seek financial advice or work out your risk profile at www.sorted.org.nz/tools/investor-kickstarter. Note that even the lowest category does not mean a risk-free investment, and there are other risks(described under the heading 'Other specific risks') that are not captured by this rating. This risk indicator is not a guarantee of a fund's future performance. The risk indicator is based on the returns data for the five years to 30 June 2019. While risk indicators are usually relatively stable, they do shift from time to time. You can see the most recent risk indicator in the latest fund update for each Fund." data-placement="right" style="vertical-align:text-bottom"></div></h6>
                                                                                                                                            </div>
                                                                                                                                            <div class="Higher">
                                                                                                                                                <span class="risk-set">Lower risk</span>
                                                                                                                                                <span>Higher risk</span>
                                                                                                                                            </div>
                                                                                                                                            <span class="indicator-box">
                                                                                                                                                <span class="risk-btns ">1</span>
                                                                                                                                                <span class="risk-btns addactive-cls">2</span>
                                                                                                                                                <span class="risk-btns">3</span>
                                                                                                                                                <span class="risk-btns">4</span>
                                                                                                                                                <span class="risk-btns">5</span>
                                                                                                                                                <span class="risk-btns">6</span>
                                                                                                                                                <span class="risk-btns">7</span>
                                                                                                                                            </span>
                                                                                                                                            <div class="Higher">
                                                                                                                                                <span  class="risk-set">Potentially <br>lower returns</span>
                                                                                                                                                <span>Potentially <br>higher returns</span>
                                                                                                                                            </div>
                                                                                                                                        </div>-->
                                                                    <div class="new-risk-section">
                                                                        <div class="low-risk">
                                                                            <h6>Lower risk</h6>
                                                                        </div>
                                                                        <div class="low-risk risk-width">
                                                                            <div class="risk-point">
                                                                                <span>1</span>
                                                                                <span>2</span>
                                                                                <span>3</span>
                                                                                <span class="risk-point-radius">4</span>
                                                                                <span>5</span>
                                                                                <span>6</span>
                                                                                <span>7</span>
                                                                                <span>8</span>
                                                                                <span>9</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="low-risk">
                                                                            <h6>Higher risk</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="risk-data">
                                                            <h6>Potentially lower returns</h6>
                                                            <h6 class="risk-color">Potentially higher returns</h6>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Target Investment Mix</h6>
                                                        </div>
                                                        <div class="element-box">
                                                            <div class="el-chart-w" id="container19" style="height:437px"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Sector Allocation</h6>
                                                        </div>
                                                        <div class="element-box">
                                                            <div class="el-chart-w" id="container-221" style="height:437px"></div>
                                                        </div>
                                                    </div>  
                                                    <div class="col-md-4">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Performance</h6>
                                                        </div>
                                                        <div class="element-box22">
                                                            <div class="card tab-card">
                                                                <div class="card-header tab-card-header">
                                                                    <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                                                        <li class="nav-item active">
                                                                            <a class="nav-link" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Last Year</a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">2 Years ago</a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">3 Years ago</a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" id="four-tab" data-toggle="tab" href="#four" role="tab" aria-controls="Four" aria-selected="false">Return Average</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>

                                                                <div class="tab-content" id="myTabContent">
                                                                    <div class="tab-pane fade show active p-3" id="one" role="tabpanel" aria-labelledby="one-tab">
                                                                        <div id="invest-bar-1" style="height: 400px; margin: 0 auto"></div>              
                                                                    </div>
                                                                    <div class="tab-pane fade p-3" id="two" role="tabpanel" aria-labelledby="two-tab">
                                                                        <div id="invest-bar-2" style="height: 400px; margin: 0 auto"></div>                
                                                                    </div>
                                                                    <div class="tab-pane fade p-3" id="three" role="tabpanel" aria-labelledby="three-tab">
                                                                        <div id="invest-bar-3" style="height: 400px; margin: 0 auto"></div>                
                                                                    </div>
                                                                    <div class="tab-pane fade p-3" id="four" role="tabpanel" aria-labelledby="four-tab">
                                                                        <div id="invest-bar-4" style="height: 400px; margin: 0 auto"></div>             
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>  

                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Month in review</h6>
                                                        </div>
                                                        <div id="elementboxcontent" class="element-box">
                                                            <div class="element-wrapper download-btn-right">
                                                                <a href="" class="fund-detail-btn">Download Full Update</a>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12" >
                                                                    <h6><span id="portheading">Our portfolio returned -2.23% for the month.</span></h6>
                                                                </div><br>
                                                                <div class="col-md-12" id="portfoliooverview">
                                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                                                </div>
                                                            </div>                                                           
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Features of the fund</h6>
                                                        </div>
                                                        <div class="element-box">
                                                            <div class="new-feature-text">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="fund-design fund-border-right">
                                                                            <div class="fund-dot">

                                                                            </div>
                                                                            <div class="fund-pera">
                                                                                <p>Provides investors with a combination of income and capital growth.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="fund-design">
                                                                            <div class="fund-dot">

                                                                            </div>
                                                                            <div class="fund-pera">
                                                                                <p>Suitable for investors with a minimum 5 year investment horion.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="fund-design fund-border-right">
                                                                            <div class="fund-dot">

                                                                            </div>
                                                                            <div class="fund-pera">
                                                                                <p>Will hold combination of both domestic and global fixed interest and equity securities.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="fund-design">
                                                                            <div class="fund-dot">

                                                                            </div>
                                                                            <div class="fund-pera">
                                                                                <p>Targeting sustainable returns above the CPI index +3.0% per annum, with low volatility.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="fund-design fund-border-right">
                                                                            <div class="fund-dot">

                                                                            </div>
                                                                            <div class="fund-pera">
                                                                                <p>Currency risk hedged at manager's discretion.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="fund-design">
                                                                            <div class="fund-dot">

                                                                            </div>
                                                                            <div class="fund-pera">
                                                                                <p>Has a lower risk profile than funds investing only in equities.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="fund-design fund-border-right fund-border-bottom">
                                                                            <div class="fund-dot">

                                                                            </div>
                                                                            <div class="fund-pera">
                                                                                <p>0.86% Management fee (GST inclusive, no performance fee).</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
<!--                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Portfolio Manager</h6>
                                                        </div>
                                                        <div class="element-box manager-style">
                                                                                                                                                                            <div class="element-wrapper">
                                                                                                                                                                                <h6 class="element-header">Portfolio Manager</h6>
                                                                                                                                                                            </div> 
                                                            <div class="invest-team">
                                                                <div class="row">

                                                                    <div class="col-md-12 col-sm-12 five-div invest-team-images manager-section">
                                                                        <div class="row">
                                                                            <div class="col-md-2">
                                                                                <img src="resources/images/man.jpg" alt="image">
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <div class="manager-content">
                                                                                    <h6>Portfolio Manager</h6>
                                                                                    <p>Mr Simon Haworth</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-md-12 col-sm-12 five-div invest-team-images">

                                                                        <div class="bio-input">
                                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>                                                                 </div>

                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>-->
                                                    <div class="col-md-12 resource-pdf">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Resources</h6>
                                                        </div>
                                                        <div class="element-box-new">

                                                            <div class="resources-pdf element-box risk-radius pdf-area">
                                                                <p> Product Disclosure Statement 2019</p>
                                                                <a href="javascript:void(0)">
                                                                    <span class="download-icon">
                                                                        <img src="resources/images/download-down.png">
                                                                    </span>
                                                                </a>
                                                            </div>
                                                            <div class="resources-pdf element-box risk-radius pdf-area">
                                                                <p> Statement of Investment Policy Objectives 2019</p>
                                                                <a href="javascript:void(0)">
                                                                    <span class="download-icon">
                                                                        <img src="resources/images/download-down.png">
                                                                    </span>
                                                                </a>
                                                            </div>
                                                            <div class="resources-pdf element-box risk-radius pdf-area">
                                                                <p> Asset Management Fund Financial Statements FY19</p>
                                                                <a href="javascript:void(0)">
                                                                    <span class="download-icon">
                                                                        <img src="resources/images/download-down.png">
                                                                    </span>
                                                                </a>
                                                            </div>
                                                            <div class="resources-pdf element-box risk-radius pdf-area">
                                                                <p> SRI Policy</p>
                                                                <a href="javascript:void(0)">
                                                                    <span class="download-icon">
                                                                        <img src="resources/images/download-down.png">
                                                                    </span>
                                                                </a>
                                                            </div>
                                                            <div class="resources-pdf element-box risk-radius pdf-area">
                                                                <p> Report</p>
                                                                <a href="javascript:void(0)">
                                                                    <span class="download-icon">
                                                                        <img src="resources/images/download-down.png">
                                                                    </span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 resource-pdf">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Quarterly Fund Updates</h6>
                                                        </div>
                                                        <div class="element-box-new">

                                                            <div class="resources-pdf element-box risk-radius pdf-area">
                                                                <p>Property Q2 2019 - June 2019</p>
                                                                <a href="javascript:void(0)">
                                                                    <span class="download-icon">
                                                                        <img src="resources/images/download-down.png">
                                                                    </span>
                                                                </a>
                                                            </div>
                                                            <div class="resources-pdf element-box risk-radius pdf-area">
                                                                <p>Property Q1 2019 - March 2019</p>
                                                                <a href="javascript:void(0)">
                                                                    <span class="download-icon">
                                                                        <img src="resources/images/download-down.png">
                                                                    </span>
                                                                </a>
                                                            </div>
                                                            <div class="resources-pdf element-box risk-radius pdf-area">
                                                                <p>Property Q4 2018 - December 2018</p>
                                                                <a href="javascript:void(0)">
                                                                    <span class="download-icon">
                                                                        <img src="resources/images/download-down.png">
                                                                    </span>
                                                                </a>
                                                            </div>
                                                            <div class="resources-pdf element-box risk-radius pdf-area">
                                                                <p>Property Q3 2018 - September 2018</p>
                                                                <a href="javascript:void(0)">
                                                                    <span class="download-icon">
                                                                        <img src="resources/images/download-down.png">
                                                                    </span>
                                                                </a>
                                                            </div>
                                                            <div class="resources-pdf element-box risk-radius pdf-area">
                                                                <p>Property Q2 2018 Fund Update - June 2018</p>
                                                                <a href="javascript:void(0)">
                                                                    <span class="download-icon">
                                                                        <img src="resources/images/download-down.png">
                                                                    </span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--                                        <div class="col-sm-6 col-md-2">
                                                                                    <div class="side_barr">
                                                                                        <a class="button_inv btn-style" data-target="#add-investment-fund-modal-lg" data-toggle="modal" style="cursor: pointer">Make Investment</a>
                                                                                        <h3>Other Funds</h3>
                                                                                        <div class="other_port">
                                        
                                        <c:forEach items="${otherPortfolioDetail}" var="port">
                                            <a href="javascript:void(0)" id="fundlink1">
                                                <div class="other_row">
                                                    <div class="other_row-img">
                                                        <img src="./resources/images/globe-plane.jpg">
                                                    </div>
                                                    <div class="other_row-text">
                                                        <p><b id="other-fund1">${port.fundName}</b></p>
                                                    </div>
                                                </div>
                                            </a>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="display-type"></div>
    </div>
    <script src="http://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" defer></script>
    <script src="http://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" defer></script>
    <script src="http://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" defer></script>
    <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
    <script src="https://code.highcharts.com/highcharts.js" ></script> 
    <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
    <script src="https://code.highcharts.com/modules/cylinder.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="./resources/js/sweetalert.min.js"></script>
    <script type="text/javascript">
        $(window).load(function () {
            $(".loader").fadeOut("slow");
        });
    </script>
    <script>

        var fmcaTopAssets = [];
        var fmcaTopAssetsColorCode = [];
        var totalFmcaInvestmentMix = [];
        var totalFmcaInvestmentMixcolorCode = [];
        var portfolioCode;
        var performanceSinceinceptionDate = [];
        var performanceSinceinceptionData = [];
        $(document).ready(function () {

            $.each(${fmcaTopAssets}, function (idx, val) {
                //                alert(JSON.stringify(val.investmentName));
                fmcaTopAssets.push([val.investmentName, val.price]);
                fmcaTopAssetsColorCode.push(val.colorCode);
            });
            $.each(${totalFmcaInvestmentMix}, function (idx, val2) {
                //                alert(JSON.stringify(val2.InvestmentName));
                totalFmcaInvestmentMix.push([val2.InvestmentName, val2.Price]);
                totalFmcaInvestmentMixcolorCode.push(val2.colorCode);
            });
            //            piechart1();
            //            alert(JSON.stringify(totalFmcaInvestmentMix));
            $('.bankaccountdiv').hide();
            $('.bankaccounthide').hide();
            var pc = '${pc}';
            portfolioCode = pc;
            var fundName = '${portfolioDetail.fundName}';



            piechart1();


            $.each(${performanceSinceinception}, function (idx, val) {
                performanceSinceinceptionDate.push(val.date);
                performanceSinceinceptionData.push(parseInt(val.data));
            });
            areachart1(fundName);
            //            $.ajax({
            //                type: 'GET',
            //                url: './rest/groot/db/api/all-funds',
            //                headers: {"Content-Type": 'application/json'},
            //                success: function (data, textStatus, jqXHR) {
            //                    $(".loader").hide();
            //                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
            //                    $.each(obj, function (idx, val) {
            //                        if (val.Code === pc) {
            //                            if (val.Name.includes("Mint")) {
            //                                fundName = val.Name;
            //                            } else {
            //                                fundName = 'Mint ' + val.Name;
            //                            }
            //                            fundName = fundName.replace('Wholesale', '');
            //                            $('#fund-name').text(fundName);
            //                            $('#fund-name-modal').text(fundName);
            //                        }
            //                        if (val.Code === "290002") {
            //                            $('#other-fund1').text(val.Name);
            //                            if ($('#other-fund1').text() === fundName) {
            //                                $('#fundlink1').hide();
            //                            }
            //                            var id1 = './investment-fund-' + val.Code;
            //                            $('#fundlink1').attr('href', id1);
            //                        }
            //                        if (val.Code === "290004") {
            //                            $('#other-fund2').text('Mint ' + val.Name);
            //                            if ($('#other-fund2').text() === fundName) {
            //                                $('#fundlink2').hide();
            //                            }
            //                            var id1 = './investment-fund-' + val.Code;
            //                            $('#fundlink2').attr('href', id1);
            //                        }
            //                        if (val.Code === "290006") {
            //                            $('#other-fund3').text('Mint ' + val.Name.replace('Wholesale', ''));
            //                            if ($('#other-fund3').text() === fundName) {
            //                                $('#fundlink3').hide();
            //                            }
            //                            var id1 = './investment-fund-' + val.Code;
            //                            $('#fundlink3').attr('href', id1);
            //                        }

            //                        if (val.Code === "290002") {
            //                            $('#other-fund4').text('Mint ' + val.Name);
            //                            if ($('#other-fund4').text() === fundName) {
            //                                $('#fundlink4').hide();
            //                            }
            //                            var id1 = './investment-fund-' + val.Code;
            //                            $('#fundlink4').attr('href', id1);
            //                        }
            //                    });
            //                    if (pc === "290002") {
            //                        $('#paragraph').html("<p>This is a single asset class Fund, investing predominantly in Australasian equities, and targeting medium to long-term growth. Investors should expect returns and risk commensurate with the New Zealand and Australian share markets.</p><p>Our objective is to outperform the S&P/NZX50 Gross Index (which is the Fund`s relevant market index) by 3% per annum, before fees, over the medium to long-term.</p>");
            //                                                                                            $('#marketoverview').html("<p>A trade ceasefire between the United States and China towards the end of Juneprompted hope that the trade war would not escalate further (for now).While the trade war impact on the United States economy is yet to fully play out,there is an expectation that the US Federal Reserve will cut rates earlier rather than later. The Reserve Bank of Australia cut their cash rate by 0.25% in early June(and again by the same amount thus far in July). This leaves their cash rate at a record low of 1%. The Reserve Bank of New Zealand held rates at their latest review (at 1.5%), but made it clear that a lower rate may be needed. In summary ? interest rates are lower for longer, reigniting the global demand for higher yielding assets.</p><p>The NZ listed property sector had another very strong month with a return of 6.0%(+12% for the quarter, and +22% for the year to date). Top performers for the month were Augusta and Precinct. The NZ share market too was strong (+3.8%, +7% for the quarter, +19% ytd), with June?s top performers being Mercury Energy, Gentrack, and Auckland Airport. In Australia, Gold was the best performing sector, and bond proxies like REITs and infrastructure were also strong. Global equities were very strong, led by the USA, France and Germany ? regaining their losses from May.</p>");
            //                        $('#portfoliooverview').html("<p>The main positive contributions came from the interest ratesensitive holdings in the portfolio, with Auckland Airport,Contact Energy and Meridian Energy leading the way for us.The main negative contributors were the a2 Milk Company,Fletcher Building and Link Administration Holdings.</p><p>During the month, we increased holdings in Afterpay TouchGroup and Lend Lease Group, and we added Arvida Group.We exited Link Administration Holdings (having lost convictionin the name), and eased back a little on Mercury andMainfreight into very strong share prices.</p><p>Arvida raised new equity during the month to acquire three villages, two in Tauranga and one in Queenstown, for $180m. The acquisition adds 326 independent living units to Arvida&apos;s existing portfolio of 3,677 units and beds. The acquisition alsoprovides Arvida with additional brownfield developmentopportunities.</p>");
            //                        $('#1-month').text('3.21%');
            //                        $('#3-month').text('5.63%');
            //                        $('#1-year').text('14.08%');
            //                        $('#5-year').text('14.61%');
            //                        $('#fund-size').text('$157.7$');
            //                        $('#fund-return').text('14.08%');
            //                        $('#portheading').text('Our portfolio returned 3.21% for the month.');
            //                        $('#fundnamespan').text(fundName + ': ');
            //                        $('.feature-text').html("<div class='row'><div class='col-md-6'><p>Actively managed 'high conviction' unit trust investing in securities listed on the stock exchanges of Australia and New Zealand.</p><p>Targeting returns above the NZX 50 Gross Index + 3.0% per annum, before fees over the medium to long-term.</p><p>No fixed allocation between countries, regions or sectors.</p><p>Consistent returns via Total Return approach + Active Management (GARP style - Growth At the Right Price).</p><p>1.15% Management Fee (GST inclusive, no performance fee).</p></div><div class='col-md-6'><p>Cash - where attractive opportunities do not exist the Trust will hold cash/yielding securities.</p><p>Suitable for investors with a minimum 5 year investment horizon.</p><p>Target 25 - 40 holdings.</p><p>Currency risk hedged at manager`s discretion.</p></div></div>");

            //                    }
            //                    if (pc === "290004") {
            //                        $('#paragraph').html("<p>This is a single asset class Fund, investing predominantly in Australasian listed property securities. Investors should expect returns and risk commensurate with the listed property sector of the New Zealand and Australian share markets.</p><p>Our objective is to outperform the S&P/NZX All Real Estate (Industry Group) Gross Index (which is the Fund`s relevant market index) by 1% per annum, before fees, over the medium to long-term.</p>");
            //                                                                                            $('#marketoverview').html("<p>A trade ceasefire between the United States and China towards the end of June prompted hope that the trade war would not escalate further (for now).While the trade war impact on the United States economy is yet to fully play out, there is an expectation that the US Federal Reserve will cut rates earlier rather than later. The Reserve Bank of Australia cut their cash rate by 0.25% in early June (and again by the same amount thus far in July). This leaves their cash rate at a record low of 1%. The Reserve Bank of New Zealand held rates at their latest review (at 1.5%), but made it clear that a lower rate may be needed. In summary ? interest rates are lower for longer, reigniting the global demand for higher yielding assets.</p><p>The NZ listed property sector had another very strong month with a return of 6.0% (+12% for the quarter, and +22% for the year to date). Top performers for the month were Augusta and Precinct. The NZ share market too was strong (+3.8%, +7% for the quarter, +19% ytd), with June?s top performers being Mercury Energy, Gentrack, and Auckland Airport. In Australia, Gold was the best performing sector, and bond proxies like REITs and infrastructure were also strong. Global equities were very strong, led by the USA, France and Germany ? regaining their losses from May.</p>");
            //                        $('#portfoliooverview').html("<p>The top positive contributors were holdings in Precinct Property and Kiwi Property Group. The key detractors were holdings in Aveo Group and Lend Lease Group.</p> \n\
            //                                                      <p>Over the month, there was a flurry of capital raisings and we participated in the Arvida, Charter Hall Long WALE REIT and GPT issues. We also increased exposure to Stride and Lend Lease. We reduced exposure to Kiwi Property Group, Mirvac and Investore as they reached price targets. Arvida (ARV) announced during June that it was acquiring three villages, two in Tauranga and one in Queenstown, for $180m - partly funded by new equity. The acquisition also provides ARV with additional development opportunities.</p> \n\
            //                                                      <p>Charter Hall and Abacus together launched a takeover offer for Australian Unity Office (AOF) after acquiring a 20% stake on market. GPT raised $0.8bn to acquire two Sydney office assets. With retail REITs still trading below NTA, Scentre Group sold a large asset and launched a share buyback.</p>");
            //                        piechart2();
            //                        areachart2(fundName);
            //                        $('#1-month').text('5.01%');
            //                        $('#3-month').text('10.19%');
            //                        $('#1-year').text('25.00%');
            //                        $('#5-year').text('12.24%');
            //                        $('#fund-size').text('$76.1$');
            //                        $('#fund-return').text('25.00%');
            //                        $('#portheading').text('Our portfolio returned 5.01% for the month.');
            //                        $('#fundnamespan').text(fundName + ': ');
            //                        $('.feature-text').html("<div class='row'><div class='col-md-6'><p>Open-ended 'high conviction' Australia and New Zealand actively managed listed property and property related equity trust.</p><p>Can hold up to 60% of listed property and property related investments in Australia.</p><p>Targeting returns above the S&P/NZX All Real Estate Gross Index + 1.0% p.a, before fees.</p><p>Consistent returns via Total Return approach + Active Management (GARP style).</p><p>Suitable for Investors with a minimum 5 year investment horizon.</p></div><div class='col-md-6'><p>Cash where attractive opportunities do not exist the Trust will hold cash/yielding securities.</p><p>0.86% Management Fee (GST exclusive, no performance fee).</p><p>Target 15 - 30 holdings.</p><p>Currency risk hedged at Manager's discretion.</p></div></div> ");
            //                    }
            //                    if (pc === "290006") {
            //                        $('#paragraph').html("<p>This is a multi-asset class Fund that invests across a range of asset types which includes: cash, fixed interest (including credit products), listed property and equities.</p><p>The objective of the Fund is to deliver moderate capital growth in excess of the Consumer Price Index (CPI) by 3% per annum, before fees, over the medium to long-term, while also providing income on a quarterly basis. This Fund is Mint`s lowest risk strategy and investors should expect returns and risk to sit between the risk profiles of the fixed interest and property asset classes.</p>");
            //                                                                                            $('#marketoverview').html("<p>A trade ceasefire between the United States and China towards the end of June prompted hope that the trade war would not escalate further (for now). While the trade war impact on the United States economy is yet to fully play out, there is an expectation that the US Federal Reserve will cut rates earlier rather than later. The Reserve Bank of Australia cut their cash rate by 0.25% in early June (and again by the same amount thus far in July). This leaves their cash rate at a record low of 1%. The Reserve Bank of New Zealand held rates at their latest review (at 1.5%), but made it clear that a lower rate may be needed. In summary ? interest rates are lower for longer, reigniting the global demand for higher yielding assets.</p> <p>The NZ listed property sector had another very strong month with a return of 6.0% (+12% for the quarter, and +22% for the year to date). Top performers for the month were Augusta and Precinct. The NZ share market too was strong (+3.8%, +7% for the quarter, +19% ytd), with June?s top performers being Mercury Energy, Gentrack, and Auckland Airport. In Australia, Gold was the best performing sector, and bond proxies like REITs and infrastructure were also strong. Global equities were very strong, led by the USA, France and Germany ? regaining their losses from May.</p>");
            //                        $('#portfoliooverview').html("<p>The main positive contributions came from the interest rate sensitive holdings in the portfolio, with Auckland Airport, Contact Energy and Meridian Energy leading the way for us. The main negative contributors were the a2 Milk Company, Fletcher Building and Link Administration Holdings.</p> <p>During the month, we increased holdings in Afterpay Touch Group and Lend Lease Group, and we added Arvida Group. We exited Link Administration Holdings (having lost conviction in the name), and eased back a little on Mercury and Mainfreight into very strong share prices.</p> <p>Arvida raised new equity during the month to acquire three villages, two in Tauranga and one in Queenstown, for $180m. The acquisition adds 326 independent living units to Arvida&apos;s existing portfolio of 3,677 units and beds. The acquisition alsoprovides Arvida with additional brownfield development opportunities.</p>");
            //                        piechart3();
            //                        areachart3(fundName);
            //                        $('#1-month').text('1.45%');
            //                        $('#3-month').text('2.87%');
            //                        $('#1-year').text('7.01%');
            //                        $('#5-year').text('-');
            //                        $('#fund-size').text('$114.1$');
            //                        $('#fund-return').text('7.01%');
            //                        $('#portheading').text('Our portfolio returned 1.45% for the month.');
            //                        $('#fundnamespan').text(fundName + ': ');
            //                        $('.feature-text').html("<div class='row'><div class='col-md-6'><p>Provides investors with a combination of income and capital growth.</p><p>Has a lower risk profile than funds investing only in equities.</p><p>Targeting sustainable returns above the CPI index + 3.0% per annum, with low volatility.</p><p>Will hold combination of both domestic and global fixed interest and equity securities.</p></div><div class='col-md-6'><p>Suitable for investors with a minimum 5 year investment horizon.</p><p>0.86% Management fee (GST inclusive, no performance fee).</p><p>Currency risk hedged at manager's discretion.</p></div></div>");
            //                    }
            //                    if (pc === "290012") {
            //                        $('#paragraph').html("<p>This is a multi-asset class Fund that invests across a range of asset types which includes: cash, fixed interest (including credit products), listed property and equities. </p><p>The objective of the Fund is to deliver returns in excess of the Consumer Price Index (CPI) by 4.5% per annum, before fees, over the medium to long-term. Investors should expect returns and risk to sit between the risk profiles of the property and equities asset classes.</p>");
            //                                                                                            $('#marketoverview').html("<p>A trade ceasefire between the United States and China towards the end of June prompted hope that the trade war would not escalate further (for now).While the trade war impact on the United States economy is yet to fully play out, there is an expectation that the US Federal Reserve will cut rates earlier rather than later. The Reserve Bank of Australia cut their cash rate by 0.25% in early June (and again by the same amount thus far in July). This leaves their cash rate at a record low of 1%. The Reserve Bank of New Zealand held rates at their latest review (at 1.5%), but made it clear that a lower rate may be needed. In summary ? interest rates are lower for longer, reigniting the global demand for higher yielding assets.</p><p>The NZ listed property sector had another very strong month with a return of 6.0% (+12% for the quarter, and +22% for the year to date). Top performers for the month were Augusta and Precinct. The NZ share market too was strong (+3.8%, +7% for the quarter, +19% ytd), with June?s top performers being Mercury Energy, Gentrack, and Auckland Airport. In Australia, Gold was the best performing sector, and bond proxies like REITs and infrastructure were also strong. Global equities were very strong, led by the USA, France and Germany ? regaining their losses from May.</p>");
            //                        $('#portfoliooverview').html("<p>The main positive contributions came from the interest rate sensitive holdings in the portfolio, with Auckland Airport,Contact Energy and Meridian Energy leading the way for us.The main negative contributors were the a2 Milk Company,Fletcher Building and Link Administration Holdings.</p><p>During the month, we increased holdings in Afterpay TouchGroup and Lend Lease Group, and we added Arvida Group.We exited Link Administration Holdings (having lost convictionin the name), and eased back a little on Mercury andMainfreight into very strong share prices.</p><p>Arvida raised new equity during the month to acquire threevillages, two in Tauranga and one in Queenstown, for $180m. The acquisition adds 326 independent living units to Arvida&apos;s existing portfolio of 3,677 units and beds. The acquisition alsoprovides Arvida with additional brownfield development opportunities.</p>");
            //                        piechart4();
            //                        areachart4(fundName);
            //                        $('#1-month').text('3.66%');
            //                        $('#3-month').text('4.01%');
            //                        $('#1-year').text('-');
            //                        $('#5-year').text('-');
            //                        $('#fund-size').text('$5.8$');
            //                        $('#fund-return').text('-%');
            //                                                $('#portheading').text('Our portfolio returned 3.66% for the month.');
            //                        $('#fundnamespan').text(fundName + ': ');
            //                                                $('.feature-text').html("<div class='row'><div class='col-md-6'><p>Provides investors with long-term capital growth.</p><p>Has a risk profile between that of the property and equities asset classes</p><p>Targeting sustainable returns above the CPI Index + 4.5% per annum, with moderate to high volatility.</p><p>Will hold a combination of International and Australasian equities, fixed interest, listed property and cash.</p></div><div class='col-md-6'><p>Suitable for investors with a minimum 5 year investment horizon.</p><p>1.09% Management fee (GST inclusive, no performance fee).</p><p>Currency risk hedged at manager's discretion.</p></div></div>");
            //                    }
            //                },
            //                error: function (jqXHR, textStatus, errorThrown) {
            //                }
            //            });
        });

        //        $('#paragraph').html('');
        //        $('#portfoliooverview').html('');
        //        $('#portheading').html('');
        //        $('.feature-text').html('');
        //                $.ajax({
        //                type: 'GET',
        //                url: './rest/3rd/party/api/fund-details-' + pc,
        //                headers: {"Content-Type": 'application/json'},
        //                success: function (data, textStatus, jqXHR) {
        //                    //                        alert(data);
        //                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
        //                    $.each(obj.pricePortfolioUnitPrices, function (idx, val) {
        //                        priceArr.push(val.Prices);
        //
        //                    });
        //                    price = priceArr[priceArr.length - 1];
        //                    $('#fund-price').text(price);
        //                },
        //                error: function (jqXHR, textStatus, errorThrown) {
        //                }
        //            });
        //            $.ajax({
        //                type: 'GET',
        //                url: './rest/groot/db/api/investment-fund-details-'+pc,
        //                headers: {"Content-Type": 'application/json'},
        //                success: function (data, textStatus, jqXHR) {
        //                    $(".loader").hide();
        //                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
        //                    console.log(JSON.stringify(obj));
        //                    $.each(obj, function (idx, val) {
        //                        console.log(JSON.stringify(val[0].Portfolio));
        //                        $('#paragraph').html(val[0].FundOverview);
        //                        $('#portfoliooverview').html(val[0].MonthlyFundUpdate);
        //                        $('#portheading').html(val[0].monthlyReturnValue);
        //                        $('.feature-text').html(val[0].FeaturesOftheFund);
        //                    });
        //                },
        //                error: function (jqXHR, textStatus, errorThrown) {
        //                    alert(textStatus);
        //                }
        //            });
        //      });
        //        price = priceArr[priceArr.length - 1];
        //        $('#fund-price').text(price);
        //        },
        //                error: function (jqXHR, textStatus, errorThrown) {
        //                }
        $('#beneficiaryId').change(function () {
            $('.bankaccountdiv').hide();
            $('.bankaccounthide').hide();
            var beneficiaryval = $('#beneficiaryId option:selected').text();
            $('#investmentName').val(beneficiaryval);
            var beneficiaryid = $(this).val();
            var beneficiarvals = document.getElementsByClassName('beneficiarid');
            for (var i = 0; i < beneficiarvals.length; i++) {
                var beneficiarval = beneficiarvals[i];
                var root = $(beneficiarval).closest('.bankaccountdiv');
                if (beneficiarval.value === beneficiaryid) {
                    root.show();
                    $('.bankaccounthide').show();
                }
            }

        });
        $('#saveInvestmentbtn').click(function () {
            var root = $(this).closest(".bankaccountdiv");
            var bankAccountId = root.find('.bankid').val();
            var investmentName = $('#investmentName').val();
            var investmentAmount = $('#investmentAmount').val();
            var beneficiaryId = $('#beneficiaryId option:selected').val();
            var applicationId = '${applicationId}';
            var fundCode = '${pc}';
            var fundName = '${portfolioDetail.fundName}';
            var obj = {portfolioName: fundName, investmentName: investmentName, investedAmount: investmentAmount, portfolioCode: fundCode, applicationId: applicationId, beneficiaryId: beneficiaryId, bankAccountId: bankAccountId};
            swal({
                title: "Proceed",
                text: "Investment is progress.",
                type: "info",
                timer: 2500,
                showConfirmButton: true
            });
            $.ajax({
                type: 'POST',
                url: './rest/groot/db/api/admin/pending-investment',
                data: JSON.stringify(obj),
                headers: {"Content-Type": 'application/json'},
                success: function (data, textStatus, jqXHR) {

                    swal({
                        title: "Success",
                        text: "Investment is successfully.",
                        type: "success",
                        timer: 2500,
                        showConfirmButton: true
                    });
                    location.reload(true);

                },
                error: function (jqXHR, textStatus, errorThrown) {
//                    alert("error");
                }
            });
        });
    </script>
    <script>
        Highcharts.chart('livebalance1', {
            chart: {
                type: 'spline',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {

                        // set up the updating of the chart each second
                        var series = this.series[0];
                        setInterval(function () {
                            var x = (new Date()).getTime(), // current time
                                    y = Math.random();
                            series.addPoint([x, y], true, true);
                        }, 1000);
                    }
                }
            },
            time: {
                useUTC: false
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    color: '#03A9F4',
                    shadow: true,
                    lineWidth: 3,
                    marker: {
                        enabled: false
                    }
                }
            },
            title: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {

                title: {
                    text: 'Portfolio Investments ($)'
                },
                plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br/>',
                pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                    name: 'Random data',
                    data: (function () {
                        // generate an array of random data
                        var data = [],
                                time = (new Date()).getTime(),
                                i;
                        for (i = -19; i <= 0; i += 1) {
                            data.push({
                                x: time + i * 1000,
                                y: Math.random()
                            });
                        }
                        return data;
                    }())
                }]
        });
    </script>

    <script>
        Highcharts.chart('stockbalance1', {
            chart: {
                type: 'area'
            },
            accessibility: {
                description: ''
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            legend: {
                enabled: false
            },
            xAxis: [{
                    categories: ['21 Oct', '28 Oct', '4 Nov', '11 Nov', '18 Nov', '25 Nov',
                        '1 Dec', '8 Dec']

                }],
            yAxis: {
                title: {
                    text: ''
                },
                labels: {
                    formatter: function () {
                        return this.value / 1000 + 'k';
                    }
                }
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name} had stockpiled <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
            },
            plotOptions: {
                area: {
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                }
            },
            series: [{
                    name: '',
                    data: [
                        0, 110, 250, 300, 410, 535,
                        709
                    ]
                }, ]
        });
    </script>
    <!--    <script>
            $.getJSON('https://www.highcharts.com/samples/data/aapl-c.json', function (data) {
    
                // Create the chart
                Highcharts.stockChart('container', {
    
                    rangeSelector: {
                        selected: 1
                    },
                    title: {
                        text: 'AAPL Stock Price'
                    },
                    navigator: {
                        enabled: false
                    },
                    series: [{
                            name: 'AAPL Stock Price',
                            data: data,
                            tooltip: {
                                valueDecimals: 2
                            }
                        }]
                });
            });
        </script>-->

    <script>
        $(document).ready(function () {
            $("#view_transaction").hide();
            $("#view_transaction_status").click(function () {
                $("#view_transaction").toggle();
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $(".company-show1").hide();
            $(".company-show2").hide();
            $(".company-show3").hide();
            $(".company-show4").hide();
            $(".company-show5").hide();
            $(".company-show6").hide();
            $(".company-show7").hide();
            $(".company-show8").hide();
            $(".company-show9").hide();
            $(".companyshow1").click(function () {
                $(".company-show1").toggle();
            });
            $(".companyshow2").click(function () {
                $(".company-show2").toggle();
            });
            $(".companyshow3").click(function () {
                $(".company-show3").toggle();
            });
            $(".companyshow4").click(function () {
                $(".company-show4").toggle();
            });
            $(".companyshow5").click(function () {
                $(".company-show5").toggle();
            });
            $(".companyshow6").click(function () {
                $(".company-show6").toggle();
            });
            $(".companyshow7").click(function () {
                $(".company-show7").toggle();
            });
            $(".companyshow8").click(function () {
                $(".company-show8").toggle();
            });
            $(".companyshow9").click(function () {
                $(".company-show9").toggle();
            });
        });
    </script>
    <script>
        Highcharts.chart('fund-container22', {
            chart: {
                type: 'pie'
            },
            credits: {
                enabled: false,
            },
            exporting: {
                enabled: false,
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            plotOptions: {
                pie: {
                    innerSize: 100,
                    depth: 45,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}</b>: {point.y:,.2f}',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    },
                },
            },
            series: [{
                    showInLegend: true,
                    name: 'Percentage',
                    data: [
                        ['Australia 24.2%', 24.2],
                        ['China 17.3%', 17.3],
                        ['Korea 15.6%', 15.6],
                        ['Hong Kong 9.2%', 9.2],
                        ['India 7.8%', 7.8],
                        ['Taiwan 7.0%', 7.0],
                        ['Thailand 4.7%', 4.7],
                        ['Singapore 4.5%', 4.5],
                        ['Indonesia 4.2%', 4.2],
                        ['Other 5.5%', 5.5],
                    ]
                }]
        });
    </script>
    <script>
        function piechart1() {
            Highcharts.setOptions({
                colors: ['#54565a', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
                        // colors: fmcaTopAssetsColorCode
            });
            Highcharts.chart('container-221', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                legend: {
                    maxHeight: 70,
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 150,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: fmcaTopAssets
                    }]
            });
            Highcharts.setOptions({
                // colors: totalFmcaInvestmentMixcolorCode
                colors: ['#54565a', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
            });
            Highcharts.chart('container19', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 150,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            distance: 2,
                            connectorWidth: 0,
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: totalFmcaInvestmentMix
                    }]
            });
        }
    </script>

    <script>
        function piechart3() {

            Highcharts.setOptions({
                colors: ['#7ba0c4', '#3b888a', '#8f8d8c', '#c54b38', '#93dacf']
            });
            Highcharts.chart('container19', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                legend: {
                    maxHeight: 70,
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 0,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            distance: 2,
                            connectorWidth: 0,
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                                name: 'Cash and Cash Equivalents',
                                y: 5
                            }, {
                                name: 'Listed property',
                                y: 15
                            }, {
                                name: 'Fixed Interest',
                                y: 65
                            }, {
                                name: 'International Equities',
                                y: 10
                            }, {
                                name: 'Australasian equities',
                                y: 5
                            }]
                    }]
            });
            $('.risk-btns').removeClass('active');
            $('.addactive-cls').addClass('active');
        }
    </script>
    <script>
        $(document).ready(function () {
            $.ajax({
                type: 'GET',
                url: './rest/groot/db/api/get-FundPerformance-${pc}',
                headers: {"Content-Type": 'application/json'},
                success: function (data, textStatus, jqXHR) {
                    console.log("data-->" + data);
                    var obj = JSON.parse(data);
                    $(".loader").hide();
                    console.log(typeof obj);
                    $('#1-month').text(obj.onemonth);
                    $('.1-monthTrending').find('span').text(obj.onemonthTrending);
                    $('#3-month').text(obj.threemonth);
                    $('.3-monthTrending').find('span').text(obj.threemonthTrending);
                    $('#1-year').text(obj.oneyear);
                    $('.1-yearTrending').find('span').text(obj.oneyeartrending);
                    $('#5-year').text(obj.fiveyear);
                    $('#fund-price').text(obj.unit_price);
                    $('#fund-size').text(obj.fund_size);
                    $('#fund-return').text(obj.one_year_return);
                    $('.risk-btns').each(function (i) {
                        var statsValue = $(this).text();
                        if (statsValue === obj.riskIndicator) {
                            $(this).addClass("active");
                        }
                    });
                    console.log('here is end');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //                    alert(${pc});
//                    alert("error");
                }
            });
        });
    </script>
    <script>
        Highcharts.chart('stockbalance', {
            chart: {
                type: 'spline',
            },
            title: {
                text: ''
            },
            subtitle: {

            },
            xAxis: {
                categories: [
                    '2017 Jan', '2017 Feb', '2017 Mar', '2017 Apr', '2017 May', '2017 Jun', '2017 Jul', '2017 Aug', '2017 Sep', '2017 Oct', '2017 Nov', '2017 Dec',
                    '2018 Jan', '2018 Feb', '2018 Mar', '2018 Apr', '2018 May', '2018 Jun', '2018 Jul', '2018 Aug', '2018 Sep', '2018 Oct', '2018 Nov', '2018 Dec',
                    '2019 Jan', '2019 Feb', '2019 Mar', '2019 Apr', '2019 May', '2019 Jun', '2019 Jul'],
                tickInterval: 1,
                labels: {
                    style: {
                        color: '#333',
                        fontSize: '12px',
                        textTransform: 'uppercase'
                    },
                    y: 20,
                    x: 10
                },
                lineColor: '#dadada'
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            legend: {
                layout: 'vertical',
                align: 'center',
                verticalAlign: 'bottom',
                enabled: false

            },
            plotOptions: {
                series: {
                    color: '#2c94ec',
                    shadow: true,
                    lineWidth: 3,
                    marker: {
                        enabled: false
                    }
                }
            },
            tooltip: {
                pointFormatter: function () {
                    var isNegative = this.y < 0 ? '-' : '';
                    return  isNegative + '$' + Math.abs(this.y.toFixed(0));
                }
            },
            series: [{
                    name: '',
                    data: [90688,
                        90688 + 970.00,
                        90688 + 888.80,
                        90688 + 1009.60,
                        90688 + 1202.45,
                        90688 + 1460.47,
                        90688 + 1800.66,
                        90688 + 2092.99,
                        90688 + 2207.57,
                        90688 + 2654.50,
                        90688 + 3037.57,
                        90688 + 4883.94,
                        90688 + 5762.66,
                        90688 + 6864.18,
                        90688 + 7903.55,
                        90688 + 9018.43,
                        90688 + 10095.45,
                        90688 + 13109.44,
                        90688 + 19630.21,
                        90688 + 22010.11,
                        113081.42,
                        114035.62,
                        115055.92,
                        117057.41,
                        120343.07,
                        123068.20,
                        125022.82,
                        127051.06,
                        128001.64,
                        130004.75,
                        134644.35]
                }],
            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        }
                    }]
            }

        });
    </script>  
    <script>
        Highcharts.setOptions({
            colors: ['#2c94ec', '#2d373e', '#575f65']
        });
        Highcharts.chart('invest-bar-1', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                    name: '290002',
                    data: [15]

                }, {
                    name: '290004',
                    data: [70]

                }, {
                    name: '290007',
                    data: [50]

                }, {
                    name: 'All',
                    data: [30]

                }]
        });
    </script> 
    <script>
        Highcharts.setOptions({
            colors: ['#2c94ec', '#2d373e', '#575f65']
        });
        Highcharts.chart('invest-bar-2', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                    name: '290002',
                    data: [60]

                }, {
                    name: '290004',
                    data: [29]

                }, {
                    name: '290007',
                    data: [27]

                }, {
                    name: 'All',
                    data: [30]

                }]
        });
    </script> 
    <script>
        Highcharts.setOptions({
            colors: ['#2c94ec', '#2d373e', '#575f65']
        });
        Highcharts.chart('invest-bar-3', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                    name: '290002',
                    data: [40]

                }, {
                    name: '290004',
                    data: [50]

                }, {
                    name: '290007',
                    data: [20]

                }, {
                    name: 'All',
                    data: [30]

                }]
        });
    </script> 
    <script>
        Highcharts.setOptions({
            colors: ['#2c94ec', '#2d373e', '#575f65']
        });
        Highcharts.chart('invest-bar-4', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                    name: '290002',
                    data: [40]

                }, {
                    name: '290004',
                    data: [45]

                }, {
                    name: '290007',
                    data: [65]

                }, {
                    name: 'All',
                    data: [20]

                }]
        });
    </script> 
    <script>
        Highcharts.setOptions({
            colors: ['#2c94ec', '#2d373e', '#575f65']
        });
        Highcharts.chart('container19', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            credits: {
                enabled: false,
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            plotOptions: {
                pie: {
                    innerSize: 190,
                    depth: 45,
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                    name: '',
                    colorByPoint: true,
                    data: [{
                            name: 'Cash and cash equivalents',
                            y: 70
                        }, {
                            name: 'Listed property',
                            y: 30
                        }]
                }]
        });
    </script> 
    <script>
        Highcharts.setOptions({
            colors: ['#2c94ec', '#2d373e', '#575f65']
        });
        Highcharts.chart('container-221', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            credits: {
                enabled: false,
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            plotOptions: {
                pie: {
                    innerSize: 190,
                    depth: 45,
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                    name: '',
                    colorByPoint: true,
                    data: [{
                            name: 'Diversified REITs',
                            y: 10
                        }, {
                            name: 'Real Estate Operating Companies',
                            y: 15
                        }, {
                            name: 'Industrial REITs',
                            y: 45
                        }, {
                            name: 'Cash and cash equivalents',
                            y: 30
                        }]
                }]
        });
    </script> 
</body>
</html>