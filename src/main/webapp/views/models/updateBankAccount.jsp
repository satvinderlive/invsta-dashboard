<%-- 
    Document   : updateBankAccount
    Created on : Sep 27, 2019, 11:36:06 AM
    Author     : ADMIN
--%>
<style>
    .new-model-text label.label_input {
        color: #2d2a26;
    }
    .new-model-text .form-control:focus {
        color: #464a4c;
        background-color: #fff;
        border-color: #999!important;
        outline: none;
    }
</style>
<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="bankModel" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Bank Account Detail</h4>
                </div>
                <div class="modal-body new-model-text">
                    <input type="hidden" id="investIdBank" value="">  
                    <input type="hidden" id="beneIdBank"  value="">  
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="label_input">
                                Bank name 
                            </label>
                        </div>
                        <div class="col-sm-6 details-pos">
                            <div class="profile-form-style">
                            <select class="selectoption  bank_name form-group aml-select" id="bank_name" value="">
                                <option value="No Bank Selected">-Select-</option>
                                <option value="Bank of New Zealand">Bank of New Zealand</option>
                                <option value="ANZ Bank New Zealand">ANZ Bank New Zealand</option>
                                <option value="ASB Bank">ASB Bank</option>
                                <option value="Westpac">Westpac</option>
                                <option value="Heartland Bank">Heartland Bank</option>
                                <option value="Kiwibank">Kiwibank</option>
                                <option value="SBS Bank">SBS Bank</option>
                                <option value="TSB Bank">TSB Bank</option>
                                <option value="The Co-operative Bank">The Co-operative Bank</option>
                                <option value="NZCU">NZCU</option>
                                <option value="Rabobank New Zealand">Rabobank New Zealand</option>
                                <option value="National Bank of New Zealand">National Bank of New Zealand</option>
                                <option value="National Australia Bank">National Australia Bank</option>
                                <option value="Industrial and Commercial Bank of China">Industrial and Commercial Bank of China</option>
                                <option value="PostBank">PostBank</option>
                                <option value="Trust Bank Southland">Trust Bank Southland</option>
                                <option value="Trust Bank Otago">Trust Bank Otago</option>
                                <option value="Trust Bank Canterbury">Trust Bank Canterbury</option>
                                <option value="Trust Bank Waikato">Trust Bank Waikato</option>
                                <option value="Trust Bank Bay of Plenty">Trust Bank Bay of Plenty</option>
                                <option value="Trust Bank South Canterbury">Trust Bank South Canterbury</option>
                                <option value="Trust Bank Auckland">Trust Bank Auckland</option>
                                <option value="Trust Bank Central">Trust Bank Central</option>
                                <option value="Trust Bank Wanganui">Trust Bank Wanganui</option>
                                <option value="Westland Bank">Westland Bank</option>
                                <option value="Trust Bank Wellington">Trust Bank Wellington</option>
                                <option value="Countrywide">Countrywide</option>
                                <option value="United Bank">United Bank</option>
                                <option value="HSBC">HSBC</option>
                                <option value="Citibank">Citibank</option>
                                <option value="Other" class="otherBank">Other</option>
                            </select>
                            <span class="error" id="error_bank_name"></span>
                        </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input" style="text-align:left">
                                Account name
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <div class="profile-form-style">
                            <input type="text" class="acount_holder_name form-control input-field" name="acount_holder_name" id="acount_holder_name" value="" required="required" placeholder="Enter your account name" onkeypress="return ((event.charCode >= 65 & amp; & amp; event.charCode <= 90) || (event.charCode >= 97 & amp; & amp; event.charCode <= 122) || (event.charCode == 32))">
                            <span class="error" id="error_acount_holder_name"></span>
                        </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input" style="text-align:left">
                                Account number
                            </label>
                        </div>
                        <div class="col-sm-6 ">
                            <div class="profile-form-style">
                            <input type="tel" class="account_number form-control input-field" id="acount_holder_number" name="acount_holder_number" value="" required="required" placeholder="xx-xxxx-xxxxxxx-xxx" onkeydown="checkAccountNO(this)">
                            <span class="error" id="error_acount_holder_number"></span>
                        </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input" style="text-align:left">
                                Branch
                            </label>
                        </div>
                        <div class="col-sm-6 ">
                            <div class="profile-form-style">
                            <input type="text" class="account_number form-control input-field" id="Branch" name="Branch" value="" required="required" placeholder="Enter Bank Branch " onkeydown="checkAccountNO(this)">
                            <span class="error" id="error_acount_holder_number"></span>
                        </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input" style="text-align:left">
                                Suffix
                            </label>
                        </div>
                        <div class="col-sm-6 ">
                            <div class="profile-form-style">
                            <input type="tel" class="account_number form-control input-field" id="Suffix" name="Suffix" value="" required="required" placeholder="Enter Suffix" onkeydown="checkAccountNO(this)">
                            <span class="error" id="error_acount_holder_number"></span>
                        </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input" style="text-align:left">
                                Currency
                            </label>
                        </div>
                        <div class="col-sm-6 ">
                            <div class="profile-form-style">
                            <input type="tel" class="account_number form-control input-field" id="Currency" name="Currency" value="" required="required" placeholder="Enter  Currency" onkeydown="checkAccountNO(this)">
                            <span class="error" id="error_acount_holder_number"></span>
                        </div>
                        </div>
                        <!-- <div class="col-sm-129">
                                <input type="file" name="myFile" class="attach-btn2">
                        </div> -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-info bank-save" >Submit</button>
                    <button type="button" class="btn btn-default btn-info" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

</div>
<script>
    $('.bank-save').click(function () {
        var investId = $("#investIdBank").val();
        var beneId = $("#beneIdBank").val();
        var bankName = $("#bank_name").val();
        var accountName = $("#acount_holder_name").val();
        var accountNumber = $("#acount_holder_number").val();
        var branch = $("#Branch").val();
        var suffix = $("#Suffix").val();
        var currency = $("#Currency").val();
        var obj = {bank: bankName, accountName: accountName, account: accountNumber, branch: branch,
            suffix: suffix, currency: currency, beneficiaryId: beneId, InvestmentCode:investId};
        console.log(JSON.stringify(obj));
        $.ajax({
            type: 'POST',
            url: './rest/groot/db/api/bankAccount-registeration',
            headers: {"Content-Type": 'application/json'},
            data: JSON.stringify(obj),
            success: function (data, textStatus, jqXHR) {
                var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                console.log(data);
            }, error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus);
            }
        });
    });

</script>
