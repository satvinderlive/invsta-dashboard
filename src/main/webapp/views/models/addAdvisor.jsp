<%-- 
    Document   : addAdvisor
    Created on : Sep 27, 2019, 11:16:46 AM
    Author     : ADMIN
--%>

<style>
    .new-model-text label.label_input {
        color: #2d2a26;
    }
    .new-model-text .form-control:focus {
        color: #464a4c;
        background-color: #fff;
        border-color: #999!important;
        outline: none;
    }
</style>
<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="advisorModel" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Add Advisor</h4>
                </div>
                <div class="modal-body new-model-text">
                    <input type="hidden" id="advisorId" value="">
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="label_input" style="text-align:left">
                                Advisor Name
                            </label>
                        </div>
                        <div class="col-sm-6 ">
                            <input type="text" id="advisor_name" class="form-control input-field" placeholder="Enter Advisor Name" value="" autocomplete="off">
                            <span class="error" id="error_advisor_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input" style="text-align:left">
                                Company Name
                            </label>
                        </div>
                        <div class="col-sm-6 ">
                            <input type="text" id="company_name" class="  form-control input-field" placeholder="Enter Company Name" value="" autocomplete="off">
                            <span class="error" id="error_company_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input" style="text-align:left">
                                Date of Birth
                            </label>
                        </div>
                        <div class="col-sm-6 ">
                            <input type="text" id="dob" class="  form-control input-field dob" placeholder="Enter Date of Birth " value="" autocomplete="off">
                            <span class="error" id="error_dob"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input" style="text-align:left">
                                Advisor Email
                            </label>
                        </div>
                        <div class="col-sm-6 ">
                            <input type="text" id="username" class="  form-control input-field" placeholder="Enter Email " value="" autocomplete="off">
                            <span class="error" id="error_username"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input" style="text-align:left">
                                Advisor Password
                            </label>
                        </div>
                        <div class="col-sm-6 ">
                            <input type="text" id="password" class="  form-control input-field" placeholder="Enter Password " value="" autocomplete="off">
                            <span class="error" id="error_password"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-info advisor-save" >Submit</button>
                    <button type="button" class="btn btn-default btn-info" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.advisor-save').click(function () {
        var emailExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        //                var passwordExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/;
        var passwordExpression = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*[\s]).{8,16}$/;
        var passwordTitle = "Password must contain at least one number, one uppercase and lowercase letter, one special character  and be between 8-16 characters";

        var advisor_name = $('#advisor_name').val();
        var company_name = $('#company_name').val();
        var dob = $('#dob').val();
        var username = $('#username').val();
        var password = $('#password').val();
        if (advisor_name.trim() === "") {
            $('#error_advisor_name').text("This field is required");
        } else if (company_name.trim() === "") {
            $('#error_company_name').text("This field is required");
        } else if (dob === "") {
            $('#error_dob').text("This field is required");
        } else if (username === "" || !emailExpression.test(username)) {
            $("#error_username").text("Please enter a valid email address, such as example@email.com ");
        } else if (password === "" || !passwordExpression.test(password)) {
            $("#error_password").text(passwordTitle);
        } else {
            var obj = {advisorName: advisor_name, companyName: company_name, advisorDob: dob, username: username, password: password};
            swal({
                title: "Progress",
                text: "Advisor is created progress.",
                type: "info",
                timer: 2500,
                showConfirmButton: true
            });
            $.ajax({
                type: 'POST',
                url: './rest/groot/db/admin/api/add-advisor',
                headers: {"Content-Type": 'application/json'},
                data: JSON.stringify(obj),
                success: function (data, textStatus, jqXHR) {
                    swal({
                        title: "Success",
                        text: "Advisor is created successfully.",
                        type: "success",
                        timer: 3500,
                        showConfirmButton: true
                    });
                    location.reload(true);
                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                    console.log(data);
                }, error: function (jqXHR, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        }
    });
    $('.form-control').keyup(function () {
        $('.error').text('');
    });

    $(document).ready(function () {
        var adultDOB = '2001-07-22';
        var arr = adultDOB.split("-");
        var year = parseInt(arr[0]);
        $(".dob").datepicker({
            yearRange: (year - 80) + ':' + year,
            changeMonth: true,
            changeYear: true,
            'maxDate': new Date(adultDOB),
            dateFormat: 'yy/mm/dd'
        }).datepicker( ).attr('readonly', 'readonly');


    });



</script>
