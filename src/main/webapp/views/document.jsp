<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>All Beneficiaries</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>

        <link href="http://backoffice.invsta.io/mint/images/mint.png" rel="shortcut icon"/>
        <link href="http://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
         <link href="./resources/css/main.css" rel="stylesheet"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <!--<link href="http://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>-->
        <!--<link href="http://backoffice.invsta.io/pocv/resources/css/main.css?version=3.5.1" rel="stylesheet"/>-->
        
        <link href="http://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.css">
        <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.css">
            
              
        <style>
            
            .emp-profile{
                padding: 3%;
                margin-top: 3%;
                margin-bottom: 3%;
                border-radius: 0.5rem;
                background: #fff;
            }
            .profile-img{
                text-align: center;
            }
            .profile-img img{
                width: 70%;
                height: 100%;
            }
            .profile-img .file {
                position: relative;
                overflow: hidden;
                margin-top: -20%;
                width: 70%;
                border: none;
                border-radius: 0;
                font-size: 15px;
                background: #212529b8;
            }
            .profile-img .file input {
                position: absolute;
                opacity: 0;
                right: 0;
                top: 0;
            }
            .profile-head h5{
                color: #333;
            }
            .profile-head h6{
                color: #0062cc;
            }
            .profile-edit-btn{
                border: none;
                border-radius: 1.5rem;
                width: 70%;
                padding: 2%;
                font-weight: 600;
                color: #6c757d;
                cursor: pointer;
            }
            .proile-rating{
                font-size: 12px;
                color: #818182;
                margin-top: 5%;
            }
            .proile-rating span{
                color: #495057;
                font-size: 15px;
                font-weight: 600;
            }
            .profile-head .nav-tabs{
                margin-bottom:5%;
            }
            .profile-head .nav-tabs .nav-link{
                font-weight:600;
                border: none;
            }
            .profile-head .nav-tabs .nav-link.active{
                border: none;
                border-bottom:2px solid #0062cc;
            }
            .profile-work{
                padding: 14%;
                margin-top: -15%;
            }
            .profile-work p{
                font-size: 12px;
                color: #818182;
                font-weight: 600;
                margin-top: 10%;
            }
   
        </style>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                         <a href="./home">Home</a>
                                        </li>
                                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                                        </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                  <div class="col-sm-12 advisor-btn">
                                                    <button type="button" class="btn btn-info btn-lg DocumentModel " data-toggle="modal" data-target="#documentmodel" onclick="documentmodel(this);">Add Advisor </button>
                                    </div>
                                <div class="col-md-12">
                                    <div class="container emp-profile">
                                        <table id="benificary-table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                               data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                            <!--<th data-checkbox="true"></th>-->
                                            <!--<th data-field="title" data-formatter=""><b>Title</b></th>-->
                                            <th data-field="Id" ><span >Date</span></th>
                                            <th data-field="IrdNumber" ><span >Name</span></th>
                                            <th data-field="Name" ><span>file</span></th>

                                            </tr>

                                            </thead>
                                            <tbody>
                                           
                                            </tbody>
                                        </table> 
                                    <tr>
                                        <div class="row">
                                                <c:forEach items="${document}" var="list">
                                         <form action="./save-document" method="POST">

<!--                                                    <div class="col-md-4 ">
                                                        <a href="./portfolio-">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background: #54565a">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">${list.upload_path}</h5>
                                                                </div>
                                                            </div>
                                                    </div>-->
                                                                 <div class="col-md-4 ">
                                                        <!--<a href="./portfolio-">-->
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background: #54565a">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">${list.name}</h5>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="col-md-4 ">
                                                          <div class="ilumony-box text-center" style="cursor: pointer; background: #54565a">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">${list.document_file}</h5>
                                                                </div>
                                                            </div>
<!--                                                            <div class="parent-row-action">
                                                                <div class="row-actions"> 
                                                                    <a href="javascript:void(0)" style="color:white"><i class="fa fa-file" style="font-size:20px; margin-right: 10px"></i></a>
                                                                    <a href="./home-portreport" style="color:white"><i class="fa fa-line-chart" style="font-size:20px"></i></a>
                                                                </div>
                                                            </div>-->
                                                        <!--</a>-->    
                                                    </div>
                                                    <div class="col-md-4 ">
                                                      <div class="ilumony-box text-center" style="cursor: pointer; background: #54565a">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">${list.name}</h5>
                                                                </div>
                                                            </div>
                                                    </div>
                                                                </form>
                                                                <div class="col-md-4 ">
                                                        <!--<a href="./portfolio-">-->
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background: #54565a">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">${list.upload_path}</h5>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </c:forEach>
                                            </div>
                                </tr>
                             
                        </div>
                    </div>
                </div>
            </div>
                            </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
                <jsp:include page = "../views/models/documentmodel.jsp"></jsp:include>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.js"></script> 
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="http://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>


                          

                                 
        </section>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script src="./resources/plugins/jquery/jquery.min.js"></script>
        <script src="./resources/plugins/bootstrap/js/bootstrap.js"></script>
        <script src="./resources/plugins/bootstrap-select/js/bootstrap-select.js"></script>
        <script src="./resources/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="./resources/plugins/jquery-validation/jquery.validate.js"></script>
        <script src="./resources/plugins/jquery-steps/jquery.steps.js"></script>
        <script src="./resources/plugins/sweetalert/sweetalert.min.js"></script>
        <script src="./resources/plugins/node-waves/waves.js"></script>
        <script src="./resources/js/admin.js"></script>
        <script src="./resources/js/pages/forms/form-validation.js"></script>
        <script src="./resources/js/demo.js"></script>
        <script src="./resources/js/chosen.jquery.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

        <script>

                            function openCity(evt, cityName) {
                                var i, tabcontent, tablinks;
                                tabcontent = document.getElementsByClassName("tabcontent");
                                for (i = 0; i < tabcontent.length; i++) {
                                    tabcontent[i].style.display = "none";
                                }
                                tablinks = document.getElementsByClassName("tablinks");
                                for (i = 0; i < tablinks.length; i++) {
                                    tablinks[i].className = tablinks[i].className.replace(" active", "");
                                }
                                document.getElementById(cityName).style.display = "block";
                                evt.currentTarget.className += " active";
                            }
                            $(document).ready(function(){
                               $('.i-name').hide();
                               $('.i-description').hide();
                               $('.Update').hide();
                               $('.Edit').click(function(){
                                   var row=$(this).closest('tr');
                                  
                                   row.find('.s-name').hide();
                                        row.find('.i-name').show();
                                        row.find('.s-description').hide();
                                        row.find('.i-description').show();
                                        row.find('.Edit').hide();
                                        row.find('.Update').show();
                               } );
                                             
                                              
                            });
//                            function showdata() {
//                               $('.s-name').hide();
//                               $('.i-name').show();
//                               $('.s-description').hide();
//                               $('.i-description').show();
//                               $('.Edit').hide();
//                               $('.Update').show();
//                                
//                            }
        </script>



    </body>
</html>
