<%-- 
    Document   : more-director-view
    Created on : 23 Oct, 2019, 10:56:34 AM
    Author     : IESL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<c:forEach items="${joint.moreInvestorList}" var="moreInvestor" varStatus="loop"></c:forEach>--%>
<c:forEach items="${company.moreInvestorList}" var="moreInvestor" varStatus="loop">
    <fieldset class="morestep1 more-director-fs more-director-info" id="step7${loop.index}1">
        <div class="content-section">
            <div class="element-wrapper">
                <h5 class="element-header">
                    <div class="require-data"> 
                        <span>We require further information about each Director/Shareholder.</span>
                    </div>
                    You can provide this information now, alternatively we?ll send an email to them requesting they provide this information.
                </h5>
            </div>
            <div class="input-content">
                <div class="row">
                    <div class="col-sm-12">
                        <p><span class="director-name curr-director"></span></p>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Email address
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="email" class="form-control input-field removedata more-director-email" name="email" required="required" placeholder="Enter email address" value="${moreInvestor.email}" onkeyup="removetinspan();"/>
                        <span class="error more-director-email-error"></span>
                    </div>
                    <div class="col-sm-12 new-box">
                        <input type="radio" id="radio05${loop.index}" value="1" data-id="send-now" class="s-options checkradio radio1" name="year${loop.index}" checked>
                        <label for="radio05${loop.index}" class="forlabel1"><span class="enter-btn-small small-padd">I will enter their details now</span> </label>
                        <input type="radio" id="radio06${loop.index}" value="2"  data-id="send-email" class="s-options checkradio radio2"  name="year${loop.index}">
                        <label for="radio06${loop.index}" class="forlabel2"><span class="enter-btn-small small-padd1">Send them an email requesting info</span> </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_form">
            <!--<img src="./resources/images/red.png">-->	
        </div>
        <input type="button" name="previous" class="previous7 action-button-previous" onclick="prev7(this)" value="Previous" />
        <input type="button" name="next" class="next8 action-button" onclick="next8(this)" value="Continue" />
    </fieldset>
    <fieldset class="morestep2 more-director-fs more-director-info" id="step7${loop.index}2">
        <div class="content-section">
            <div class="element-wrapper">
                <h5 class="element-header">
                    Please enter the details for: <span class="director-name curr-director">${moreInvestor.fname}</span>
                </h5>
            </div>
            <div class="input-content">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="label_input">
                            Home address
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" id="address1" class="form-control input-field removedata more-director-address"  placeholder="Enter home address" value="${moreInvestor.address}" onkeyup="removetinspan();"/>
                        <span class="error more-director-address-error"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Mobile phone number
                        </label>
                    </div>
                    <div class="col-sm-6 mobile_number first-mobile mobile-index1">
                        <input type="text" class="form-control codenumber more-director-countrycode countryCode" name="countryCode" required="required" placeholder="Enter mobile number" readonly="readonly">
                        <input type="tel" class="form-control removedata codename more-director-mobile" name="mobileNo" required="required" placeholder="Enter mobile number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="${moreInvestor.mobileNo}" onkeyup="removetinspan();">
                        <span class="error more-director-mobile-error"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Date of birth
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" name="dob" class="input-field more-director-dob" placeholder="dd/mm/yyyy" data-format="dd/mm/yyyy" data-lang="en" required value="${moreInvestor.dateOfBirth}" onchange="removetinspan();"/>
                        <span class="error more-director-dob-error"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Occupation
                        </label>
                    </div>
                    <div class="col-sm-6" >
                        <select name="occupation" class="Occupation selectoption selectOcc  form-group OccupationOption more-director-select-occu" onchange="chnageoccupation(this);">
                            <option value="-Select-">-Select-</option>
                                                    
                                                        <option value="5">Arts and Media Professionals</option>
                                                    
                                                        <option value="13">Automotive and Engineering Trades Workers</option>
                                                    
                                                        <option value="6">Business, Human Resource and Marketing Professionals</option>
                                                    
                                                        <option value="20">Carers and Aides</option>
                                                    
                                                        <option value="1">Chief Executives, General Managers and Legislators</option>
                                                    
                                                        <option value="38">Cleaners and Laundry Workers</option>
                                                    
                                                        <option value="29">Clerical and Office Support Workers</option>
                                                    
                                                        <option value="39">Construction and Mining Labourers</option>
                                                    
                                                        <option value="14">Construction Trades Workers</option>
                                                    
                                                        <option value="7">Design, Engineering, Science and Transport Professionals</option>
                                                    
                                                    <option value="0">Other</option>
                        </select>
                        <input type="text" name="Occupation" placeholder="Enter occupation" value="${moreInvestor.occupation}" class="form-control otherOcc  input-field removedata more-director-input-occu" style="display: none" onkeyup="removetinspan();"/>
                        <span class="error spanOccupation"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Position in Company 
                        </label>
                    </div>
                    <div class="col-sm-6 ">
                        <select class="selectoption form-group more-director-positionInCompany" onchange="removetinspan();">
                            <option value="0">-Select-</option>
                            <option value="Director" <c:if test = "${moreInvestor.positionInCompany eq 'Director'}">selected</c:if>>Director</option>
                            <option value="Beneficial owner" <c:if test = "${moreInvestor.positionInCompany eq 'Beneficial owner'}">selected</c:if>>Beneficial owner</option>
                            <option value="Authorised" <c:if test = "${moreInvestor.positionInCompany eq 'Authorised'}">selected</c:if>>Authorised Person</option>
                            </select>
                            <span class="error more-director-positionInCompany-error"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_form">
                <!--<img src="./resources/images/red.png">-->	
            </div>
            <input type="button" name="previous" class="previous8 action-button-previous" onclick="prev8(this)" value="Previous" />
            <input type="button" name="next" class="next9 action-button" onclick="next9(this)" value="Continue" />
        </fieldset>
        <fieldset class="morestep3 more-director-fs more-director-info"  id="step7${loop.index}3">
        <div class="content-section">
            <div class="element-wrapper">
                <h5 class="element-header">
                    Please enter the identification details for: <span class="director-name curr-director">${moreInvestor.fname}</span>
                </h5>
            </div>
            <input type="hidden" class="which_container abc" value="#more-director-dob"/>
            <div class="input-content">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="label_input detail-2">
                            Which type of ID are you providing 
                        </label>
                    </div>
                    <div class="col-sm-6 ">
                        <select class="selectoption form-group Id_Type src_of_fund2" onchange="changeFund(this)">
                            <option value="1">NZ Driver Licence</option>
                            <option value="2">NZ Passport</option>
                            <option value="3">Other</option>

                        </select>
                    </div>
                    <div class="row drivery-licence1">

                        <div class="col-sm-6">
                            <label class="label_input">
                                First Name 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="first_name more_licence_first_name" name="licence_first_name" placeholder="Enter first name" value="${moreInvestor.fname}" onkeyup="removetinspan();"/>
                            <span class="error more_error_licence_first_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Middle Name 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="middle_name more_licence_middle_name" name="licence_middle_name" placeholder="Enter middle name" onkeyup="removetinspan();"/>
                            <span class="error error_licence_middle_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Last Name 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="last_name more_licence_last_name" name="licence_last_name" placeholder="Enter last name" id="" onkeyup="removetinspan();"/>
                            <span class="error more_error_licence_last_name"></span>
                        </div>

                        <div class="col-sm-6">
                            <label class="label_input">
                                Licence number 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control input-field removedata more-director-licenseNumber" name="license_number" value="${moreInvestor.licenseNumber}" required="required" placeholder="Enter licence number " onkeyup="removetinspan();" />
                            <span class="error more-director-licenseNumber-error"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Expiry date 
                            </label>
                        </div>
                        <div class="col-sm-6 form-group">
                            <input type="text" name="expiryDate" placeholder="dd/mm/yyyy" class="input-field more-director-exp lic_expiry_Date more-director-licenseExpiryDate" value="${moreInvestor.licenseExpiryDate}" data-format="DD/MM/YY" data-lang="en" required onchange="changeFund(this)"/>                                 
                            <span class="error more-director-licenseExpiryDate-error"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Version number 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="versionNumber" value="${moreInvestor.versionNumber}" placeholder="Enter version number" class="input-field removedata lic_verson_number more-director-versionNumber" onkeypress="return event.charCode >= 48 && event.charCode <= 57" onkeyup="removetinspan();"/>
                            <span class="error more-director-versionNumber-error"></span>
                        </div>
                    </div>
                    <div class="row passport-select1">
                        <div class="col-sm-6">
                            <label class="label_input">
                                First Name 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="first_name more_passport_first_name" name="passport_first_name" placeholder="Enter first name" value="${moreInvestor.fname}" onkeyup="removetinspan();"/>
                            <span class="error more_error_passport_first_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Middle Name 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="middle_name more_passport_middle_name" name="passport_middle_name" placeholder="Enter middle name" onkeyup="removetinspan();"/>
                            <span class="error more_error_passport_middle_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Last Name 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="last_name more_passport_last_name" name="passport_last_name" placeholder="Enter last name" onkeyup="removetinspan();"/>
                            <span class="error more_error_passport_last_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Passport number 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control input-field removedata more-director-passportNumber" name="passport_number" value="${moreInvestor.passportNumber}" required="required" placeholder="Enter passport number" onkeyup="removetinspan();"/>
                            <span class="error more-director-passportNumber-error"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Expiry date 
                            </label>
                        </div>
                        <div class="col-sm-6 form-group">
                            <input type="text" name="passportExpiryDate"  placeholder="dd/mm/yyyy" class="input-field more-director-exp pass_expiry more-director-passportExpiryDate" data-format="DD/MM/YY" data-lang="en" required value="${moreInvestor.passportExpiryDate}"/>                                 
                            <span class="error more-director-passportExpiryDate-error"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Country of issue  
                            </label>
                        </div>
                        <div class="col-sm-6 flag-drop">
                            <input type="text" class="form-control  more-director-passportCountryOfIssue more-director-countryname countryname" value="${moreInvestor.countryOfIssue}" name="countryOfIssue" required="required" placeholder="Enter Country Code" readonly="readonly">    
                            <span class="error more-director-passportCountryOfIssue-error"></span>
                        </div>
                    </div>
                    <div class="row other-select1">
                        <div class="col-sm-6">
                            <label class="label_input">
                                Type of ID   
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control input-field removedata more-director-typeOfId" name="typeOfID" value="${moreInvestor.typeOfID}" required="required" placeholder="Enter ID type" onkeyup="removetinspan();" />
                            <span class="error more-director-typeOfId-error"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                First name 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="first_name" id="other_first_name" name="other_first_name" placeholder="Enter first name"/>
                            <span class="error" id="error_other_first_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Middle name 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="middle_name" name="other_middle_name" placeholder="Enter middle name"/>
                            <span class="error" id="error_other_middle_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Last name 
                            </label>
                        </div>                                                    
                        <div class="col-sm-6">
                            <input type="text" class="last_name" id="other_last_name" name="other_last_name" placeholder="Enter last name"/>
                            <span class="error" id="error_other_last_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Expiry date 
                            </label>
                        </div>
                        <div class="col-sm-6 form-group">
                            <input type="text" name="dob" placeholder="dd/mm/yyyy" class="input-field dob1 more-director-typeOfIdExpiryDate" data-format="DD/MM/YY" data-lang="en" value="${moreInvestor.typeOfID}"/>                                 
                            <span class="error more-director-typeOfIdExpiryDate-error"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Country of issue  
                            </label>
                        </div>
                        <div class="col-sm-6 flag-drop">
                            <input type="text" class="form-control  more-director-typeOfIdCountryOfIssue more-director-countryname countryname" name="Country of issue"  required="required" placeholder="Enter Country Code" readonly="readonly" value="${moreInvestor.typeCountryOfIssue}">    
                            <span class="error more-director-typeOfIdCountryOfIssue-error"></span>

                        </div>
                        <div class="col-sm-129 closestcls">
                            <input type="file" name="myFile" class="attach-btn checkname myFile" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf">
                            <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile"><i class="far fa-times-circle"></i></a>
                        </div>
                    </div>
                    <input type="hidden" name="more_director_verify" id="more_director_verify" value="false">
                </div>
            </div>
        </div>
        <div class="footer_form">
            <!--<img src="./resources/images/red.png">-->	
        </div>
        <input type="button" name="previous" class="previous9 action-button-previous" onclick="prev9(this)" value="Previous" />
        <input type="button" name="next" class="next10 action-button" onclick="next10(this)" value="Continue" />
    </fieldset>
    <fieldset class="morestep4 more-director-fs more-director-info" id="step7${loop.index}4">
        <div class="content-section">
            <div class="element-wrapper">
                <h5 class="element-header">
                    Please enter the tax details for: <span class="director-name curr-director">${moreInvestor.fname}</span>
                </h5>
            </div>
            <div class="input-content">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="label_input">
                            Country of residence 
                        </label>
                    </div>
                    <div class="col-sm-6 form-group country-set flag-drop">
                        <input type="text" class="form-control  countryOfResidence more-director-countryname countryname" name="countryOfResidence"  required="required" placeholder="Enter Country Code" readonly="readonly" value="${moreInvestor.holderCountryOfResidence}">
                        <span class="error more-director-countryname-error"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            IRD Number 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control input-field removedata more-director-irdNumber" name="IRDNumber"  required="required" placeholder="XXX-XXX-XXX" onkeydown="checkird(this)" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="${moreInvestor.irdNumber}" />
                        <span class="error more-director-irdNumber-error"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input" style="text-align:left">
                            Are you a US citizen or US tax resident, or a tax resident in any other country?
                        </label>
                    </div>
                    <div class="col-sm-6 ">
                        <select class="selectoption selectoption2 form-group more-director-usCitizen" onchange="changeCountry(this)">
                            <option value="1">No</option>
                            <option value="2">Yes</option>
                        </select>
                    </div>

                    <div class="row yes-option1">
                        <div class="row yes-new3 checktin3data">
                            <div class="col-sm-12">
                                <h5 class="element-header aml-text">
                                    Please enter all of the countries (excluding NZ) of which you are a tax resident)
                                </h5>
                            </div>
                            <div class="col-sm-6">
                                <label class="label_input" style="text-align:left">
                                    Country of tax residence:
                                </label>
                            </div>
                            <div class="col-sm-6 details-pos flag-drop">
                                <input type="text" class="form-control excludenz more-director-tex_residence_Country countrynameexcludenz"  name="countryOfTaxResidence" required="required" placeholder="Enter Country Code" readonly="readonly" value="${moreInvestor.typeCountryOfIssue}">    
                                <span class="error excludenz-error" ></span>
                            </div>
                            <div class="col-sm-6">
                                <label class="label_input">
                                    Tax Identification Number (TIN) 
                                </label>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control input-field removedata more-director-TIN" name="TIN"  required="required" placeholder="Enter TIN " onkeyup="myFunction()" />
                                <span class="error more-director-TIN-error" ></span>
                            </div>
                            <div class="col-sm-6">
                                <label class="label_input">
                                    Reason if TIN not available  
                                </label>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control input-field removedata more-director-resn_tin_unavailable" name="reasonTIN" value="${company.countryOfResidence}" required="required" placeholder=""/>
                                <span class="error more-director-resn_tin_unavailable-error" ></span>
                            </div> 
                        </div>
                        <div class="col-sm-12 add-another">
                            <a class="add-another2 all-btn-color add-country-another3" href="javascript:void(0)" onclick="addAnotherCountry(this)">Add another Country</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_form">
            <!--<img src="./resources/images/red.png">-->	
        </div>
        <input type="button" name="previous" class="previous10 action-button-previous" onclick="prev10(this)" value="Previous" />
        <input type="button" name="next" class="next11 action-button" onclick="next11(this)" value="Continue" />
    </fieldset> 
</c:forEach>
<fieldset class="morestep1" id="morestep1" style="display: none">
    <div class="content-section">
        <div class="element-wrapper">
            <h5 class="element-header">
                <div class="require-data"> 
                    <span>We require further information about each Director/Shareholder.</span>
                </div>
                You can provide this information now, alternatively we?ll send an email to them requesting they provide this information.
            </h5>
        </div>
        <div class="input-content">
            <div class="row">
                <div class="col-sm-12">
                    <p><span class="director-name curr-director"></span></p>
                </div>
                <div class="col-sm-6">
                    <label class="label_input">
                        Email address
                    </label>
                </div>
                <div class="col-sm-6">
                    <input type="email" class="form-control input-field removedata more-director-email" name="email" required="required" placeholder="Enter email address" onkeyup="removetinspan();" />
                    <span class="error more-director-email-error"></span>
                </div>
                <div class="col-sm-12 new-box">
                    <input type="radio" id="radio05" value="1" data-id="send-now" class="s-options checkradio radio1" name="year" checked>
                    <label for="radio05" class="forlabel1"><span class="enter-btn-small small-padd">I will enter their details now</span> </label>
                    <input type="radio" id="radio06" value="2"  data-id="send-email" class="s-options checkradio radio2"  name="year">
                    <label for="radio06" class="forlabel2"><span class="enter-btn-small small-padd1">Send them an email requesting info</span> </label>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_form">
        <!--<img src="./resources/images/red.png">-->	
    </div>
    <input type="button" name="previous" class="previous7 action-button-previous" onclick="prev7(this)" value="Previous" />
    <input type="button" name="next" class="next8 action-button" onclick="next8(this)" value="Continue" />
</fieldset>
<fieldset class="morestep2" id="morestep2" style="display: none">
    <div class="content-section">
        <div class="element-wrapper">
            <h5 class="element-header">
                Please enter the details for: <span class="director-name curr-director"></span>
            </h5>
        </div>
        <div class="input-content">
            <div class="row">
                <div class="col-sm-6">
                    <label class="label_input">
                        Home address
                    </label>
                </div>
                <div class="col-sm-6">
                    <input type="text" id="address1" class="form-control input-field removedata more-director-address"  placeholder="Enter home address" onkeyup="removetinspan();"/>
                    <span class="error more-director-address-error"></span>
                </div>
                <div class="col-sm-6">
                    <label class="label_input">
                        Mobile phone number
                    </label>
                </div>
                <div class="col-sm-6 mobile_number first-mobile mobile-index1">
                    <input type="text" class="form-control codenumber more-director-countrycode" name="countryCode" required="required" placeholder="Enter mobile number" readonly="readonly">
                    <input type="tel" class="form-control codename removedata more-director-mobile" name="mobileNo" required="required" placeholder="Enter mobile number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" onkeyup="removetinspan();">
                    <span class="error more-director-mobile-error"></span>
                </div>
                <div class="col-sm-6">
                    <label class="label_input">
                        Date of birth
                    </label>
                </div>
                <div class="col-sm-6">
                    <input type="text" name="dob" class="input-field more-director-dob" placeholder="dd/mm/yyyy" data-format="dd/mm/yyyy" data-lang="en" required onchange="removetinspan();"/>
                    <span class="error more-director-dob-error"></span>
                </div>
                <div class="col-sm-6">
                    <label class="label_input">
                        Occupation
                    </label>
                </div>
                <div class="col-sm-6" >
                    <select name="occupation" class="Occupation selectoption selectOcc  form-group OccupationOption more-director-select-occu" onchange="chnageoccupation(this);">
                        <option value="-Select-">-Select-</option>
                                                    
                                                        <option value="5">Arts and Media Professionals</option>
                                                    
                                                        <option value="13">Automotive and Engineering Trades Workers</option>
                                                    
                                                        <option value="6">Business, Human Resource and Marketing Professionals</option>
                                                    
                                                        <option value="20">Carers and Aides</option>
                                                    
                                                        <option value="1">Chief Executives, General Managers and Legislators</option>
                                                    
                                                        <option value="38">Cleaners and Laundry Workers</option>
                                                    
                                                        <option value="29">Clerical and Office Support Workers</option>
                                                    
                                                        <option value="39">Construction and Mining Labourers</option>
                                                    
                                                        <option value="14">Construction Trades Workers</option>
                                                    
                                                        <option value="7">Design, Engineering, Science and Transport Professionals</option>
                                                    
                                                    <option value="0">Other</option>
                    </select>
                    <input type="text" name="Occupation" placeholder="Enter occupation"  class="form-control otherOcc input-field removedata more-director-input-occu" style="display: none" onkeyup="removetinspan();"/>
                    <span class="error spanOccupation"></span>
                </div>
                <div class="col-sm-6">
                    <label class="label_input">
                        Position in Company 
                    </label>
                </div>
                <div class="col-sm-6 ">
                    <select class="selectoption form-group more-director-positionInCompany" onchange="removetinspan();">
                        <option value="0">-Select-</option>
                        <option value="Appointment">Director</option>
                        <option value="Interview">Beneficial owner</option>
                        <option value="4">Authorised Person</option>
                    </select>
                    <span class="error more-director-positionInCompany-error"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_form">
        <!--<img src="./resources/images/red.png">-->	
    </div>
    <input type="button" name="previous" class="previous8 action-button-previous" onclick="prev8(this)" value="Previous" />
    <input type="button" name="next" class="next9 action-button" onclick="next9(this)" value="Continue" />
</fieldset>
<fieldset class="morestep3" id="morestep3" style="display: none">
    <div class="content-section">
        <div class="element-wrapper">
            <h5 class="element-header">
                Please enter the identification details for: <span class="director-name curr-director"></span>
            </h5>
        </div>
        <input type="hidden" class="which_container abc" value="#more-director-dob"/>
        <div class="input-content">
            <div class="row">
                <div class="col-sm-6">
                    <label class="label_input detail-2">
                        Which type of ID are you providing 
                    </label>
                </div>
                <div class="col-sm-6 ">
                    <select class="selectoption form-group Id_Type src_of_fund2" onchange="changeFund(this)">
                        <option value="1">NZ Driver Licence</option>
                        <option value="2">NZ Passport</option>
                        <option value="3">Other</option>

                    </select>
                </div>
                <div class="row drivery-licence1">

                    <div class="col-sm-6">
                        <label class="label_input">
                            First Name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="first_name more_licence_first_name" name="licence_first_name" placeholder="Enter first name" id="" onkeyup="removetinspan();"/>
                        <span class="error more_error_licence_first_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Middle Name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="middle_name more_licence_middle_name" name="licence_middle_name" placeholder="Enter middle name" onkeyup="removetinspan();"/>
                        <span class="error error_licence_middle_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Last Name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="last_name more_licence_last_name" name="licence_last_name" placeholder="Enter last name" id="" onkeyup="removetinspan();"/>
                        <span class="error more_error_licence_last_name"></span>
                    </div>

                    <div class="col-sm-6">
                        <label class="label_input">
                            Licence number 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control input-field removedata more-director-licenseNumber" name="license_number" value="${company.licenseNumber}" required="required" placeholder="Enter licence number " onkeyup="removetinspan();"/>
                        <span class="error more-director-licenseNumber-error"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Expiry date 
                        </label>
                    </div>
                    <div class="col-sm-6 form-group">
                        <input type="text" name="expiryDate" placeholder="dd/mm/yyyy" class="input-field more-director-exp lic_expiry_Date more-director-licenseExpiryDate" value="${company.licenseExpiryDate}" data-format="DD/MM/YY" data-lang="en" required onchange="removetinspan();"/>                                 
                        <span class="error more-director-licenseExpiryDate-error"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Version number 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" name="versionNumber" value="${company.versionNumber}" placeholder="Enter version number" class="input-field removedata lic_verson_number more-director-versionNumber" onkeypress="return event.charCode >= 48 && event.charCode <= 57" onkeyup="removetinspan();"/>
                        <span class="error more-director-versionNumber-error"></span>
                    </div>
                </div>
                <div class="row passport-select1">
                    <div class="col-sm-6">
                        <label class="label_input">
                            First Name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="first_name more_passport_first_name" name="passport_first_name" placeholder="Enter first name" onkeyup="removetinspan();"/>
                        <span class="error more_error_passport_first_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Middle Name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="middle_name more_passport_middle_name" name="passport_middle_name" placeholder="Enter middle name" onkeyup="removetinspan();"/>
                        <span class="error more_error_passport_middle_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Last Name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="last_name more_passport_last_name" name="passport_last_name" placeholder="Enter last name" onkeyup="removetinspan();"/>
                        <span class="error more_error_passport_last_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Passport number 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control input-field removedata more-director-passportNumber" name="passport_number" value="${company.passportNumber}" required="required" placeholder="Enter passport number" onkeyup="removetinspan();" />
                        <span class="error more-director-passportNumber-error"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Expiry date 
                        </label>
                    </div>
                    <div class="col-sm-6 form-group">
                        <input type="text" name="passportExpiryDate" value="${company.passportExpiryDate}" placeholder="dd/mm/yyyy" class="input-field more-director-exp pass_expiry more-director-passportExpiryDate" data-format="DD/MM/YY" data-lang="en" required/>                                 
                        <span class="error more-director-passportExpiryDate-error"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Country of issue  
                        </label>
                    </div>
                    <div class="col-sm-6 flag-drop">
                        <input type="text" class="form-control  more-director-passportCountryOfIssue more-director-countryname" value="${company.countryOfIssue}" name="countryOfIssue" required="required" placeholder="Enter Country Code" readonly="readonly">    
                        <span class="error more-director-passportCountryOfIssue-error"></span>
                    </div>
                </div>
                <div class="row other-select1">
                    <div class="col-sm-6">
                        <label class="label_input">
                            Type of ID   
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control input-field removedata more-director-typeOfId" name="typeOfID" value="${company.typeOfID}" required="required" placeholder="Enter ID type" onkeyup="removetinspan();"/>
                        <span class="error more-director-typeOfId-error"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            First name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="first_name" id="other_first_name" name="other_first_name" placeholder="Enter first name"/>
                        <span class="error" id="error_other_first_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Middle name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="middle_name" name="other_middle_name" placeholder="Enter middle name"/>
                        <span class="error" id="error_other_middle_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Last name 
                        </label>
                    </div>                                                    
                    <div class="col-sm-6">
                        <input type="text" class="last_name" id="other_last_name" name="other_last_name" placeholder="Enter last name"/>
                        <span class="error" id="error_other_last_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Expiry date 
                        </label>
                    </div>
                    <div class="col-sm-6 form-group">
                        <input type="text" name="dob" placeholder="dd/mm/yyyy" class="input-field more-director-exp more-director-typeOfIdExpiryDate" value="${company.typeCountryOfIssue}" data-format="DD/MM/YY" data-lang="en" required onchange="removetinspan();"/>                                 
                        <span class="error more-director-typeOfIdExpiryDate-error"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Country of issue  
                        </label>
                    </div>
                    <div class="col-sm-6 flag-drop">
                        <input type="text" class="form-control  more-director-typeOfIdCountryOfIssue more-director-countryname" name="Country of issue" value="${company.typeCountryOfIssue}" required="required" placeholder="Enter Country Code" readonly="readonly">    
                        <span class="error more-director-typeOfIdCountryOfIssue-error"></span>

                    </div>
                    <div class="col-sm-129 closestcls">
                        <input type="file" name="myFile" class="attach-btn checkname myFile" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf">
                        <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile"><i class="far fa-times-circle"></i></a>
                    </div>
                </div>
                <input type="hidden" name="more_director_verify" id="more_director_verify" value="false">
            </div>
        </div>
    </div>
    <div class="footer_form">
        <!--<img src="./resources/images/red.png">-->	
    </div>
    <input type="button" name="previous" class="previous9 action-button-previous" onclick="prev9(this)" value="Previous" />
    <input type="button" name="next" class="next10 action-button" onclick="next10(this)" value="Continue" />
</fieldset>
<fieldset class="morestep4" id="morestep4" style="display: none">
    <div class="content-section">
        <div class="element-wrapper">
            <h5 class="element-header">
                Please enter the tax details for: <span class="director-name curr-director"></span>
            </h5>
        </div>
        <div class="input-content">
            <div class="row">
                <div class="col-sm-6">
                    <label class="label_input">
                        Country of residence 
                    </label>
                </div>
                <div class="col-sm-6 form-group country-set flag-drop">
                    <input type="text" class="form-control  countryOfResidence more-director-countryname" name="countryOfResidence"  required="required" placeholder="Enter Country Code" readonly="readonly">
                    <span class="error more-director-countryname-error"></span>
                </div>
                <div class="col-sm-6">
                    <label class="label_input">
                        IRD Number 
                    </label>
                </div>
                <div class="col-sm-6">
                    <input type="text" class="form-control input-field removedata more-director-irdNumber" name="IRDNumber"  required="required" placeholder="XXX-XXX-XXX" onkeydown="checkird(this)" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  />
                    <span class="error more-director-irdNumber-error"></span>
                </div>
                <div class="col-sm-6">
                    <label class="label_input" style="text-align:left">
                        Are you a US citizen or US tax resident, or a tax resident in any other country?
                    </label>
                </div>
                <div class="col-sm-6 ">
                    <select class="selectoption selectoption2 form-group more-director-usCitizen" onchange="changeCountry(this)">
                        <option value="1">No</option>
                        <option value="2">Yes</option>
                    </select>
                </div>

                <div class="row yes-option1">
                    <div class="row yes-new3 checktin3data">
                        <div class="col-sm-12">
                            <h5 class="element-header aml-text">
                                Please enter all of the countries (excluding NZ) of which you are a tax resident)
                            </h5>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input" style="text-align:left">
                                Country of tax residence:
                            </label>
                        </div>
                        <div class="col-sm-6 details-pos flag-drop">
                            <input type="text" class="form-control excludenz more-director-tex_residence_Country "  name="countryOfTaxResidence" required="required" placeholder="Enter Country Code" readonly="readonly">    
                            <span class="error excludenz-error" ></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Tax Identification Number (TIN) 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control input-field removedata more-director-TIN" name="TIN"  required="required" placeholder="Enter TIN " onkeyup="removetinspan();" />
                            <span class="error more-director-TIN-error" ></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Reason if TIN not available  
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control input-field removedata more-director-resn_tin_unavailable" name="reasonTIN" value="${company.countryOfResidence}" required="required" placeholder="" onkeyup="removetinspan();"/>
                            <span class="error more-director-resn_tin_unavailable-error" ></span>
                        </div> 
                    </div>
                    <div class="col-sm-12 add-another">
                        <a class="add-another2 all-btn-color add-country-another3" href="javascript:void(0)" onclick="addAnotherCountry(this)">Add another Country</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_form">
        <!--<img src="./resources/images/red.png">-->	
    </div>
    <input type="button" name="previous" class="previous10 action-button-previous" onclick="prev10(this)" value="Previous" />
    <input type="button" name="next" class="next11 action-button" onclick="next11(this)" value="Continue" />
</fieldset> 