<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Advisor Dashboard</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="https://backoffice.invsta.io/mint/images/mint.png" rel="shortcut icon"/>
        <link href="https://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <link href="https://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>
        <link href="https://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>
        <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.css">
        <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.css">
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link href="./resources/css/custom.css" rel="stylesheet"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css'>
        <style>

            .emp-profile{
                padding: 3%;
                margin-top: 3%;
                margin-bottom: 3%;
                border-radius: 0.5rem;
                background: #fff;
            }
            .profile-img{
                text-align: center;
            }
            .profile-img img{
                width: 70%;
                height: 100%;
            }
            .profile-img .file {
                position: relative;
                overflow: hidden;
                margin-top: -20%;
                width: 70%;
                border: none;
                border-radius: 0;
                font-size: 15px;
                background: #212529b8;
            }
            .profile-img .file input {
                position: absolute;
                opacity: 0;
                right: 0;
                top: 0;
            }
            .profile-head h5{
                color: #333;
            }
            .profile-head h6{
                color: #0062cc;
            }
            .profile-edit-btn{
                border: none;
                border-radius: 1.5rem;
                width: 70%;
                padding: 2%;
                font-weight: 600;
                color: #6c757d;
                cursor: pointer;
            }
            .proile-rating{
                font-size: 12px;
                color: #818182;
                margin-top: 5%;
            }
            .proile-rating span{
                color: #495057;
                font-size: 15px;
                font-weight: 600;
            }
            .profile-head .nav-tabs{
                margin-bottom:5%;
            }
            .profile-head .nav-tabs .nav-link{
                font-weight:600;
                border: none;
            }
            .profile-head .nav-tabs .nav-link.active{
                border: none;
                border-bottom:2px solid #0062cc;
            }
            .profile-work{
                padding: 14%;
                margin-top: -15%;
            }
            .profile-work p{
                font-size: 12px;
                color: #818182;
                font-weight: 600;
                margin-top: 10%;
            }
            .profile-work a{
                text-decoration: none;
                color: #495057;
                font-weight: 600;
                font-size: 14px;
            }
            .profile-work ul{
                list-style: none;
            }
            .profile-tab label{
                font-weight: 600;
            }
            .profile-tab p{
                font-weight: 600;
                color: #0062cc;
            }
            .el-chart-w {
                overflow: visible!important;
            }
            .portfolio_overlay {
                height: 75%;
                display: block;
            }
            .ilumony-box:hover .portfolio_overlay {
                display: block;
            }
            .funds-deatil {
                background: #fff;
                padding: 1rem;
                border-radius: 4px;
                margin-bottom: 10px;
            }
            .show_more_btn_new {
                position: absolute;
                top: 0;
                right: 0px;
                margin: 11px 27px;
            }
            .funds-deatil h4 {
                max-width: 80%;
                margin-bottom: 15px;
            }
            .collapse {
                display: none;
            }
            .collapse.show {
                display: block;
            }
  .pulsating-circle2 {
    position: relative;
    width: 10px;
    height: 10px;
    bottom: 0;
    top: 6px;
    letter-spacing: 1px;
    color: #a6a7a8;
    font-weight: normal;
    right: -6px;
    font-size: 12px;
}

        </style>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"></jsp:include>  
                    <div class="content-w">
                        <!--------------------
                        START - Breadcrumbs
                -------------------->
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item ">
                                <a href="./home">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span>Investment</span>
                            </li>
                            <li class="breadcrumb-item">
                                <span></span>
                            </li>
                        </ul>
                        <!--------------------
                        END - Breadcrumbs
                -------------------->
                        <div class="content-panel-toggler">
                            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                        </div>
                        <div class="content-i">
                            <div class="content-box">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="element-wrapper">
                                            <p>Hi <span class="logged-user-name">${user.name}</span>, welcome to Mint. Here’s an overview of your investments with us:</p><br>
                                            <h6 class="element-header">
                                                Investment Overview
                                            </h6>
                                            <div class="element-box">
                                                <div class="os-tabs-w">
                                                    <div class="os-tabs-controls">
                                                        <ul class="nav nav-tabs smaller">

                                                            <li class="nav-item">
                                                                <a class="nav-link active" data-toggle="tab" href="#tab_all_days">Historical Performance</a>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab_overview">
                                                            <div class="label" style="display:inline-block;">
                                                                Amount Invested
<!--                                                                <div class="pulsating-circle pulsating-around" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom"> 
                                                                    <span class="tooltiptext">The Live Balance chart provides an indication of the real-time balance of your  portfolio, and is reliant on external data sources which may be different to what is reported on exchanges. The actual daily closing value and withdrawal value of your portfolio will be different. This balance does not include any cash that is available in your account.</span></div>-->
                                                                    
                                                            </div>
                                                            <div class="label current-label" style="float:right; display:inline-block; ">
                                                                CURRENT Value
<!--                                                                <div class="pulsating-circle2 pulsating-around2" style="vertical-align:text-bottom;   " data-toggle="tooltip" title="">
                                                                    <span class="tooltiptext">(previous day close, incl cash in wallet) This represents the actual value of your  portfolio investments based on the previous day closing value, and includes any cash you may have available in your cash account.</span>
                                                                </div>-->
                                                            </div>
                                                            <div class="" style="padding:0 0 10px;width: 100%;float: left;">
                                                                <div class="value currentBalance">$0.00 

                                                                </div>
                                                                <div class="value sumWalletBalance" style="float:right;">
                                                                    <div class="sumWalletBal" >  $0.00 </div>
                                                                    <span class="founder-icon-color"><div class="percentage" > 16.23% </div> 
                                                                        <!--<img class="income-high" src="./resources/images/ins.gif"></img>-->
                                                                    <div class="pulsating-circle2 pulsating-around2" style="vertical-align:text-bottom;   " data-toggle="tooltip" title="">
                                                                    <span class="tooltiptext">This is an annualised1 money weighted2 return after fees and tax, since you have been invested in the fund.1  The return weights how long each amount has been invested for by how much has been invested to work out the average years each dollar has been invested for. If you have been invested for less than one year, then the return is not annualised.2  The return takes into account when your cash flows occur and how big they are.Past performance is not a reliable indicator of future returns.</span>
                                                                </div>
                                                                    </span>
                                                                </div>

                                                            </div>

                                                        </div>

                                                        <div class="tab-pane active" id="tab_all_days">

                                                            <div id="stockbalance" style="min-width: 100%; height: 400px; margin: 0 auto;"></div>
                                                        </div>
                                                        <div class="tab-pane" id="investment-composition">
                                                            <div class="label">
                                                                BALANCE  
                                                                <img src="./resources/img/info-icon.png" style="vertical-align:text-bottom">
                                                            </div>
                                                            <div class="el-tablo">
                                                                <div class="value prev-actual-balance">$ 0.00

                                                                </div>
                                                            </div>
                                                            <div id="composition-chart" style="min-width: 100%; height: 400px; margin: 0 auto;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="element-wrapper">
                                            <div class="element-box max-box">
                                                <div class="os-tabs-w">
                                                    <div class="os-tabs-controls">


                                                        <ul class="nav nav-tabs justify-content-center" role="tablist">

                                                            <li class="nav-item">
                                                                <a class="nav-link active" id="tabhome224-tab" data-toggle="tab" href="#tabhome224" role="tab" aria-controls="tabhome224" aria-selected="false">
                                                                    Investment Breakdown 
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-content">

                                                        <div class="tab-pane active" id="tab_latest_week-cons-invs-pieChart" style="" aria-expanded="false">
                                                            <div class=" el-tablo">

                                                                <div class="el-chart-w" id="investmentBreakdown" style="height:434px"></div>
                                                                <div id="legend-2"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="element-wrapper">
                                            <div class="element-box max-box">
                                                <ul class="nav nav-tabs justify-content-center" role="tablist">

                                                    <li class="nav-item">
                                                        <a class="nav-link active" id="tabhome224-tab" data-toggle="tab" href="#tabhome224" role="tab" aria-controls="tabhome224" aria-selected="false">
                                                            Performance    
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div class="os-tabs-w">
                                                    <div class="os-tabs-controls">
                                                        <ul class="nav nav-tabs smaller">
                                                            <li class="nav-item">
                                                                <a class="nav-link active" data-toggle="tab" href="#tab_overview-cons-invs-pieChart" aria-expanded="true">Last Year
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="tab" href="#tab_overview-cons-invs-pieChart2" aria-expanded="true">2 Years ago
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="tab" href="#tab_overview-cons-invs-pieChart3" aria-expanded="true">3 Years ago
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="tab" href="#tab_overview-cons-invs-pieChart4" aria-expanded="true">Average Return
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab_overview-cons-invs-pieChart" aria-expanded="true" style="">
                                                            <div class=" el-tablo">
                                                                <div id="ago1YearsBarChart" style="height: 383px;"></div>
                                                                <div id="legend-1"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_overview-cons-invs-pieChart2" aria-expanded="true" style="">
                                                            <div class=" el-tablo">
                                                                <div id="ago2YearsBarChart" style="height: 383px;"></div>
                                                                <div id="legend-1"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_overview-cons-invs-pieChart3" aria-expanded="true" style="">
                                                            <div class=" el-tablo">
                                                                <div id="ago3YearsBarChart" style="height: 383px;"></div>
                                                                <div id="legend-1"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_overview-cons-invs-pieChart4" aria-expanded="true" style="">
                                                            <div class=" el-tablo">
                                                                <div id="avgBarChart" style="height: 383px;"></div>
                                                                <div id="legend-1"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="element-wrapper">
                                            <div class="element-box max-box">
                                                <div class="os-tabs-w">
                                                    <div class="os-tabs-controls">

                                                        <ul class="nav nav-tabs justify-content-center" role="tablist">

                                                            <li class="nav-item">
                                                                <a class="nav-link active" id="tabhome224-tab" data-toggle="tab" href="#tabhome224" role="tab" aria-controls="tabhome224" aria-selected="false">
                                                                    Asset Allocation  
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab_overview-cons-invs-pieChart" aria-expanded="true" style="">
                                                            <div class=" el-tablo">
                                                                <div id="assetAllocation" style="height:434px""></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                            <div class="das-board" id="investments" hidden>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="element-wrapper">
                                                <h6 class="element-header">Investments </h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="portfolio" style="display: none;">
                                        <div class="col-md-12">
                                            <div class="element-box ele-overlay" style="background:url(https://backoffice.invsta.io/pocv/resources/img/) center center no-repeat ;background-size:cover">
                                                <h6 style="margin-bottom:20px; display: inline-block; margin-right: 5px;" class="pcName">Australasian Equity Fund</h6>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                            <div class="label">
                                                                Contribution
                                                            </div>
                                                            <div class="value contribution">
                                                                NZ$ 0.00 
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                            <div class="label ">
                                                                Current Balance
                                                            </div>
                                                            <div class="value marketValue" id="currentInvSqr">
                                                                NZ$ 0.00
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                            <div class="label">
                                                                Performance
                                                            </div>
                                                            <div class="arrow-text">
                                                                <div class="value per- per-color- percentage">
                                                                    33.66 %
                                                                </div>

                                                                <i class=" os-icon os-icon-arrow-up6"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                            <div id="lg-latestweek1" style="height:70px; width:100%"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="show_more_btn">
                                                    <a class="btn btn-primary btn-style showmemore" href="javascript:void(0)">Show Me More</a>
                                                </div>
                                            </div>
                                        </div>                                                                             
                                    </div>
                                </div>
                                <div class="row">                                  
                                    <div class="col-sm-12">
                                        <div class="element-wrapper">
                                            <!--                                        <div class="element-actions">
                                                                                        <button type="button" class="btn btn-info btn-lg advisorModel" data-toggle="modal" data-target="#advisorModel" onclick="advisorModel(this);">Add Advisor </button>
                                                                                    </div>-->
                                            <h6 class="element-header">
                                                Advisor Dashboard <img src="https://s3-ap-southeast-2.amazonaws.com/invsta.com/public_resources/img/info-icon.png" style="vertical-align:text-top">
                                            </h6>
                                            <div class="element-content">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-7">
                                                                <div class="element-box">
                                                                    <h5>ASSET ALLOCATION</h5>
                                                                    <div id="advisor-pie" style="min-width: 310px; height: 250px; max-width: 600px; margin: 0 auto"></div>

                                                                </div>
                                                            </div> 
                                                            <div class="col-md-5">
                                                                <div class="element-box">
                                                                    <div class="advisor-pie-data">
                                                                        <div class="advisor-pie-section">
                                                                            <span>Contribution</span> 
                                                                            <span class="contributionspan">$0.0</span> 
                                                                        </div>
                                                                        <div class="advisor-pie-section">
                                                                            <span>Reinvested Distns</span> 
                                                                            <span>$0.0</span> 
                                                                        </div>
                                                                        <div class="advisor-pie-section">
                                                                            <span>Distns</span> 
                                                                            <span>-$0.0</span> 
                                                                        </div>
                                                                        <div class="advisor-pie-section">
                                                                            <span>Withdrawals</span> 
                                                                            <span class="withdrawalspan">-$0.0</span> 
                                                                        </div>
                                                                        <div class="advisor-pie-section">
                                                                            <span>Tax/Paid</span> 
                                                                            <span>-$0.0</span> 
                                                                        </div>
                                                                        <div class="advisor-pie-section">
                                                                            <span>Gains/Losses</span> 
                                                                            <span>$0.0</span> 
                                                                        </div>
                                                                        <div class="advisor-pie-section advisor-border">
                                                                            <span>Value</span> 
                                                                            <span class="totalvalspan">$0.0</span> 
                                                                        </div>
                                                                    </div>
                                                                </div> 
                                                            </div> 
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="element-box el-tablo">
                                                                    <div class="label">
                                                                        Total Customers
                                                                    </div>
                                                                    <div class="value totaluser">
                                                                        4

                                                                    </div>
                                                                    <div class="trending trending-up">
                                                                        <!--<span>12%</span><i class="os-icon os-icon-arrow-up2"></i>-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="element-box el-tablo">
                                                                    <div class="label">
                                                                        Total Investments
                                                                    </div>
                                                                    <div class="value totalinvestment">
                                                                        425

                                                                    </div>
                                                                    <div class="trending trending-down-basic">
                                                                        <!--<span>12%</span><i class="os-icon os-icon-arrow-2-down"></i>-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="element-box el-tablo">
                                                                    <div class="label">
                                                                        Current Balance
                                                                    </div>
                                                                    <div class="value currentBalance">
                                                                        152,963.02

                                                                    </div>
                                                                    <div class="trending trending-down-basic">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="advisor-crousel">
                                                                    <div class="carousel-wrap">
                                                                        <div class="owl-carousel" id="owl-items">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="element-wrapper">
                                            <h6 class="element-header">
                                                <!--                                                            Crypto Traded Portfolio (CTP) Investment Options -->
                                                Portfolio Investment
                                            </h6>
                                            <h6 id="error-message">

                                            </h6>
                                            <div class="element-content">
                                                <div class="row">

                                                <c:forEach items="${allPortfolios}" var="portfolio">
                                                    <div class="col-md-3 col-sm-4">
                                                        <a href="./portfolio-${portfolio.getCode()}">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background: #54565a">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">${portfolio.getName()}</h5>
                                                                </div>
                                                            </div>
                                                        </a> 
                                                        <div class="parent-row-action">
                                                            <div class="row-actions"> 
                                                                <!--<a href="javascript:void(0)" style="color:white"><i class="fa fa-file" style="font-size:20px; margin-right: 10px"></i></a>-->
                                                                <a href="./home-portreport" style="color:white"><i class="fa fa-line-chart" style="font-size:20px"></i></a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </c:forEach> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="container emp-profile">
                                        <div class="pendingstate"><h5>Pending Investments:</h5></div>
                                        <table id="pending-invetsment" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                               data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <th data-field="name" ><span >Name</span></th>
                                                    <th data-field="beneficiaryName" ><span >Beneficiary Name</span></th>
                                                    <th data-field="investmentName" ><span >Investment Name</span></th>
                                                    <th data-field="investedAmount" ><span >Invested Amount</span></th>
                                                    <th data-field="portfolioCode" ><span>Portfolio Code</span></th>
                                                    <th data-field="status" ><span>status</span></th>
                                                    <!--<th data-formatter="viewButtonFormatter">Action</th>-->

                                                </tr>
                                            </thead>
                                        </table>
                                    </div>

                                    <div class="container emp-profile">
                                        <div class="pendingstate"><h5>Pending Transactions:</h5></div>
                                        <table id="pending-transaction" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                               data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <th data-field="name" ><span>Name</span></th>
                                                    <th data-field="beneficiaryName" ><span >Beneficiary Name</span></th>
                                                    <th data-field="investmentcode" ><span >Investment Code</span></th>
                                                    <th data-field="amount" ><span>Amount</span></th>
                                                    <th data-field="type" ><span>Type</span></th>
                                                    <th data-field="portfolioCode" ><span>Portfolio Code</span></th>
                                                    <!--<th data-formatter="viewButtonFormatter1">Action</th>-->
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>

                                    <div class="container emp-profile">
                                        <div class="pendingstate"><h5>Pending Registrations:</h5></div>
                                        <table id="pending-registration" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                               data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <!--<th data-field="id" ><span>Id</span></th>-->
                                                    <th data-field="username"><span>Username</span></th>
                                                    <th data-field="name"><span>Full Name</span></th>
                                                    <th data-field="dob"><span>D.O.B</span></th>
                                                    <th data-field="reg_type"><span>Reg. Type</span></th>
                                                    <th data-field="step" ><span>Step</span></th>
                                                    <th data-formatter="showDetailsButtonFormatter">Show Details</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
            </div>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
            <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
            <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
            <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
            <script src="https://code.highcharts.com/highcharts.js"></script>
            <script src="https://code.highcharts.com/modules/exporting.js"></script>
            <script src="https://code.highcharts.com/modules/export-data.js"></script>
            <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
            <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js'></script>
            <script src='https://use.fontawesome.com/826a7e3dce.js'></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.js"></script> 

            <script>
                function viewButtonFormatter(value, row, index) {
                    return '<a href="./investment-' + row.id + '" class="btn btn-info a-btn-slide-text "><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Accept</strong></span></a>';
                }
                function viewButtonFormatter1(value, row, index) {
                    return '<a href="./transection-' + row.id + '" class="btn btn-info a-btn-slide-text "><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Accept</strong></span></a>';
                }
                function showDetailsButtonFormatter(value, row, index) {
                    return '<a href="./reg-details-db-' + row.token + '" class="btn btn-info a-btn-slide-text show-details"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Show Details</strong></span></a>';
                }
                function deleteapplicationformatter(value, row, index) {
                    return '<a href="./delete-application-' + row.ApplicationId + '" class="btn btn-info a-btn-slide-text show-details"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Delete</strong></span></a>';
                }
                function BeneficiariesLenght(value, row, index) {
                    var length = row.Beneficiaries.length;
                    return length;
                }
            </script>
            <script>
                $(document).ready(function () {
                    var userInvestments = ${userInvestments};
                    var uniqueArr = [];
                    var totalsum = [];
                    var countribution = 0;
                    var withdwarl = 0;
                    var totalval = 0;
                    $.each(userInvestments, function (idx, inv) {
                        if (uniqueArr.indexOf(inv) === -1) {
                            uniqueArr.push(inv);
                        }
                    });
                    var invMap = new Map();
                    var benMap = new Map();
                    for (var i = 0; i < userInvestments.length; i++) {
                        var inv = userInvestments[i];
                        var invArr = invMap[inv.investmentCode];
                        if (typeof invArr === 'undefined') {
                            invArr = new Array();
                        }
                        invArr.push(inv);
                        invMap[inv.investmentCode] = invArr;

                        var ben = benMap[inv.beneficiairyId];
                        if (typeof ben === 'undefined') {
                            benMap[inv.beneficiairyId] = inv.beneficiairyId;
                        }
                    }
                    var keys = Object.keys(invMap);
                    $('.totalinvestment').text(keys.length);
                    var bens = Object.keys(benMap);
                    $('.totaluser').text(bens.length);
                    for (var i = 0; i < keys.length; i++) {
                        var key = keys[i];
                        console.log(key);
                        var sum = 0;
                        var inv_code = '';
                        var invArr = invMap[key];
                        for (var j = 0; j < invArr.length; j++) {
                            var inv = invArr[j];
                            inv_code = inv.investmentCode;
                            sum += parseFloat(inv.att_value);
                            countribution += parseFloat(inv.att_value);
                            withdwarl += parseFloat(inv.wdw_value);
                        }
                        totalsum.push([inv_code, sum]);
                    }
                    totalval = countribution + withdwarl;
                    $('.contributionspan').text("$" + countribution);
                    $('.withdrawalspan').text("$" + withdwarl);
                    $('.totalvalspan').text("$" + countribution);
                    advisorpiechart(totalsum);
                    totalsum = [];
                    var countribution = 0;
                    var withdwarl = 0;
                    var totalval = 0;
                    var ic;
                    var sum = 0;
                    var userId;
                    for (var i = 0; i < keys.length; i++) {
                        var key = keys[i];
                        var sum = 0;
                        var inv_code = '';
                        var invArr = invMap[key];
                        for (var j = 0; j < invArr.length; j++) {
                            var inv = invArr[j];
                            inv_code = inv.name + ' [' + inv.investmentCode + ']';
                            ic=inv.investmentCode;
                            var port_code = inv.portfolioName;
                            sum = parseFloat(inv.att_value);
                            countribution = parseFloat(inv.att_value);
                            withdwarl = parseInt(inv.wdw_value);
                            totalsum.push([port_code, sum]);
                            userId = inv.userId;
                        }
                        totalval = countribution + withdwarl;
                        var id = "advisor-pie1" + i;
//                        $('#owl-items').append("<div class='item'><div class='element-box'><div id='" + id + "' style=' height: 300px; max-width: 300px; margin: 0 auto'></div><div class='advisor-pie-data'><div class='advisor-pie-section'><span>Contribution</span><span>$" + countribution + "</span></div><div class='advisor-pie-section'><span>Reinvested Distns</span><span>$0.0</span></div><div class='advisor-pie-section'><span>Distns</span><span>-$0.0</span></div><div class='advisor-pie-section'><span>Withdrawals</span><span>-$" + withdwarl + "</span> </div><div class='advisor-pie-section'><span>Tax/Paid</span><span>-$0.0</span></div> <div class='advisor-pie-section'><span>Gains/Losses</span> <span>$0.0</span> </div><div class='advisor-pie-section advisor-border'> <span>Value</span><span>$" + countribution + "</span> </div></div></div></div>");
                        $('#owl-items').append("<div class='item'><div class='element-box'><a href='./home-beneficiary-dashboard-db?id=" + userId + "&ic="+ic+"'><div id='" + id + "' style=' height: 370px; max-width: 300px; margin: 0 auto'></div></a><div class='advisor-pie-data'><div class='advisor-pie-section'><span>Contribution</span><span>$" + countribution + "</span></div><div class='advisor-pie-section'><span>Reinvested Distns</span><span>$0.0</span></div><div class='advisor-pie-section'><span>Distns</span><span>-$0.0</span></div><div class='advisor-pie-section'><span>Withdrawals</span><span>-$" + withdwarl + "</span> </div><div class='advisor-pie-section'><span>Tax/Paid</span><span>-$0.0</span></div> <div class='advisor-pie-section'><span>Gains/Losses</span> <span>$0.0</span> </div><div class='advisor-pie-section advisor-border'> <span>Value</span><span>$" + countribution + "</span> </div></div></div></div>");

                        userpiechart(id, totalsum, inv_code);
                        totalsum = [];
                        var countribution = 0;
                        var withdwarl = 0;
                        var totalval = 0;
                        sum = 0;
                    }
                    $.ajax({
                        type: 'GET',
                        url: './rest/groot/db/api/admin/get-pending-transaction',
                        headers: {"Content-Type": 'application/json'},
                        success: function (data, textStatus, jqXHR) {
                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                            console.log(obj);
                            $('#pending-transaction').bootstrapTable('load', obj);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR);
                        }
                    });
                    $.ajax({
                        type: 'GET',
                        url: './rest/groot/db/api/admin/get-pending-investment',
                        headers: {"Content-Type": 'application/json'},
                        success: function (data, textStatus, jqXHR) {
                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                            console.log(JSON.stringify(obj));
                            $('#pending-invetsment').bootstrapTable('load', obj);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR);
                        }
                    });
                    $.ajax({
                        type: 'GET',
                        url: './rest/groot/db/api/get-pending-registration',
                        headers: {"Content-Type": 'application/json'},
                        success: function (data, textStatus, jqXHR) {
                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                            console.log(JSON.stringify(obj));
                            $('#pending-registration').bootstrapTable('load', obj);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR);
                        }
                    });
                    $.ajax({
                        type: 'GET',
                        url: './get-user-information',
                        headers: {"Content-Type": 'application/json'},
                        success: function (data, textStatus, jqXHR) {
                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                            console.log(JSON.stringify(obj));
                            $('.currentBalance').text(obj.amount);

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR);
                        }
                    });
                    $('.owl-carousel').owlCarousel({
                        loop: true,
                        margin: 10,
                        nav: true,
                        navText: [
                            "<i class='fa fa-caret-left'></i>",
                            "<i class='fa fa-caret-right'></i>"
                        ],
                        autoplay: true,
                        autoplayHoverPause: true,
                        responsive: {
                            0: {
                                items: 1
                            },
                            600: {
                                items: 3
                            },
                            1000: {
                                items: 3
                            }
                        }
                    });

                });
        </script>
        <script>
            function advisorpiechart(totalsum) {
                Highcharts.setOptions({
                    colors: ['#54565a', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
                });
                Highcharts.chart('advisor-pie', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Summary',
                        margin: -5
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'middle',
                        layout: 'vertical',
                        title: {
//                            text:[["rony"],["neeraj"]]
                        }

                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    navigation: {
                        buttonOptions: {
                            enabled: false
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                distance: 5,
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                            name: 'Brands',
                            innerSize: '50%',
                            colorByPoint: true,
                            data: totalsum
                        }]
                });
            }
        </script>
        <script>
            function userpiechart(id, totalsum, title) {
                Highcharts.chart(id, {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: title
                    },

                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    navigation: {
                        buttonOptions: {
                            enabled: false
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                            name: 'Brands',
                            innerSize: '50%',
                            colorByPoint: true,
                            data: totalsum
                        }]
                });

            }
        </script>
         <script>
                function executeBEDB() {
                    $.ajax({
                        type: 'GET',
                        url: './rest/groot/db/api/beneficiary-dashboard?id=${id}',
                        headers: {"Content-Type": 'application/json'},
                        success: function (data, textStatus, jqXHR) {
                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                            if (obj.investmentHoldings.length > 0
                                    && obj.investmentPerformances.length > 0
                                    && obj.fmcaInvestmentMix.length > 0) {
                                dashboard(obj);
                                $(".loader").hide();
                            } else {
                                $.ajax({
                                    type: 'GET',
                                    url: './rest/groot/db/api/investment-holdings?id=${id}',
                                    headers: {"Content-Type": 'application/json'},
                                    success: function (data, textStatus, jqXHR) {
                                        $(".loader").hide();
                                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                        if (obj.investmentHoldings.length > 0) {
                                            investmentHoldings(obj);
                                            $.ajax({
                                                type: 'GET',
                                                url: './rest/groot/db/api/investment-performances?id=${id}',
                                                headers: {"Content-Type": 'application/json'},
                                                success: function (data, textStatus, jqXHR) {
                                                    console.log('success' + data);
                                                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                    if (obj.investmentPerformances.length > 0) {
                                                        investmentPerformances(obj);
                                                    }
                                                },
                                                error: function (jqXHR, textStatus, errorThrown) {
                                                    console.log('error');
                                                }
                                            });
                                            $.ajax({
                                                type: 'GET',
                                                url: './rest/groot/db/api/fmca-investment-mix?id=${id}',
                                                headers: {"Content-Type": 'application/json'},
                                                success: function (data, textStatus, jqXHR) {
                                                    console.log('success' + data);
                                                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                    if (obj.fmcaInvestmentMix.length > 0) {
                                                        fmcaInvestmentMix(obj);
                                                    }
                                                },
                                                error: function (jqXHR, textStatus, errorThrown) {
                                                    console.log('error');
                                                }
                                            });
                                        } else {
                                            swal({
                                                title: "Info",
                                                text: "You have no investment yet",
                                                timer: 2000,
                                                showConfirmButton: false
                                            });
                                        }
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        console.log('error');
                                    }
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log('error');
                        }
                    });
                }

                var investmentAmount = 0;
                var CurrentValue = 0;
                var portfolio = document.getElementById("portfolio");
                var investments = document.getElementById("investments");
                function investmentHoldings(obj) {
                    var investmentHoldings = obj.investmentHoldings;
                    var arr = [];
                    var fundMap = new Map();
                    for (var i = 0; i < investmentHoldings.length; i++) {
                        var performances = investmentHoldings[i];
                        var clone = portfolio.cloneNode(true);
                        var h6 = clone.getElementsByClassName("pcName")[0];
                        h6.innerHTML = performances.InvestmentCode+" :: "+performances.PortfolioName;
                        var cont = clone.getElementsByClassName("contribution")[0];
                        cont.innerHTML = '$' + performances.Contributions.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                        investmentAmount = investmentAmount + performances.Contributions;
                        var marVal = clone.getElementsByClassName("marketValue")[0];
                        marVal.innerHTML = '$' + performances.MarketValue.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');;
                        CurrentValue = CurrentValue + performances.MarketValue;
                        clone.style.display = 'block';
                        var href = './show-me-more-db-' + performances.InvestmentCode + '?pc=' + performances.PortfolioCode;
                        clone.getElementsByClassName("showmemore")[0].setAttribute('href', href);
                        arr.push([performances.PortfolioName, performances.MarketValue]);
                        fundMap[performances.PortfolioCode] = performances.PortfolioName;
                        investments.appendChild(clone);
                    }
                    document.getElementsByClassName("currentBalance")[0].innerHTML = "$" + investmentAmount.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                    document.getElementsByClassName("sumWalletBal")[0].innerHTML = "$" + CurrentValue.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                    pieChart('investmentBreakdown', arr, true);
                }

                function investmentPerformances(obj) {
                    var investmentPerformances = obj.investmentPerformances;
                    var performanceMap = new Map();
                    var fundMap = new Map();
                    var keys = new Array();
                    for (var i = 0; i < investmentPerformances.length; i++) {
                        var performance = investmentPerformances[i];
                        var PortfolioArray = performanceMap[performance.Portfolio];
                        if (typeof PortfolioArray === 'undefined') {
                            PortfolioArray = new Array();
                            keys.push(performance.Portfolio);
                        }
                        PortfolioArray.push(performance);
                        performanceMap[performance.Portfolio] = PortfolioArray;
                    }
                    var avgSeries = [],
                            ago1YearSeries = [],
                            ago2YearSeries = [],
                            ago3YearSeries = [];
                    for (var key of keys) {//3 Keys
                        var PortfolioArray = performanceMap[key]; var fundName = fundMap[key];
                        if (typeof fundName === 'undefined') {
                            fundName = key;
                        }
                        var avgPeriod = PortfolioArray[PortfolioArray.length - 1];
                        var avgRR = avgPeriod.XIRRReturnRate * 100;
                        avgSeries.push({
                            name: fundName,
                            data: [avgRR]
                        });
                        var ago1YearPeriod = PortfolioArray[PortfolioArray.length - 2];
                        var ago1YearRR = ago1YearPeriod.XIRRReturnRate * 100;
                        ago1YearSeries.push({
                            name: fundName,
                            data: [ago1YearRR]
                        });

                        var ago2YearPeriod = PortfolioArray[PortfolioArray.length - 3];
                        var ago2YearRR = ago2YearPeriod.XIRRReturnRate * 100;
                        ago2YearSeries.push({
                            name: fundName,
                            data: [ago2YearRR]
                        });

                        var ago3YearPeriod = PortfolioArray[PortfolioArray.length - 4];
                        var ago3YearRR = ago3YearPeriod.XIRRReturnRate * 100;
                        ago3YearSeries.push({
                            name: fundName,
                            data: [ago3YearRR]
                        });
                    }
                    barChart('avgBarChart', avgSeries);
                    barChart('ago1YearsBarChart', ago1YearSeries);
                    barChart('ago2YearsBarChart', ago2YearSeries);
                    barChart('ago3YearsBarChart', ago3YearSeries);
                }

                function fmcaInvestmentMix(obj) {
                    var assetAllocation = [];
                    var fmcaInvestmentMix = obj.fmcaInvestmentMix;
//                    var fmcaInvestmentMixByTable = obj.fmcaInvestmentMixByTable;
//                    $.each(fmcaInvestmentMix, function (idx, val) {
//                        assetAllocation.push([val.FMCAAssetClass, val.SectorValueBase]);
//                    });
                    $.each(fmcaInvestmentMix, function (idx, val) {
                        assetAllocation.push([val.FMCAAssetClass, val.SectorValueBase]);
                    });
                    pieChart('assetAllocation', assetAllocation, false);
                }

                function dashboard(obj) {
                    investmentHoldings(obj);
                    investmentPerformances(obj);
                    fmcaInvestmentMix(obj);
                }

                executeBEDB();
        </script>
        <script>
            function pieChart(idc, arr, mktval) {
                Highcharts.setOptions({
                    colors: ['#91d9cf', '#54565a', '#879fc2', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
                });
                var ptfmt = '';
                if (mktval) {
                    ptfmt = '{series.name} <br>{point.percentage:.2f} % <br>Market Value: {point.y}';
                } else {
                    ptfmt = '{series.name} <br>{point.percentage:.2f} %';
                }
                Highcharts.chart(idc, {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 0
                        }
                    },
                    credits: {
                        enabled: false,
                    },
                    exporting: {
                        enabled: false,
                    },
                    legend: {
                        enabled: true,
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: ptfmt
                    },
                    plotOptions: {
                        pie: {
                            innerSize: 100,
                            depth: 45,
                            dataLabels: {enabled: false,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %<br>Value: {point.y}',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            },
                        },
                    },
                    series: [{
                            showInLegend: true,
                            name: '',
                            data: arr
                        }]
                });
            }

            function barChart(idc, series) {
                Highcharts.setOptions({
                    colors: ['#606065', '#c64b38', '#2d2a26']
                });
                Highcharts.chart(idc, {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''},
                    xAxis: {
                        categories: [''],
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        max: 30,
                        tickInterval: 5,
                        labels: {
                            format: '{value}%'
                        },
                        title: {
                            text: ''
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    credits: {
                        enabled: false
                    },
                    navigation: {
                        buttonOptions: {
                            enabled: false
                        }
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0,
                            borderWidth: 0
                        }
                    },
                    series: series
                });
            }
        </script>
         <script>
            Highcharts.chart('stockbalance', {
                chart: {
                    type: 'spline',
                },
                title: {
                    text: ''
                },
                subtitle: {

                },
                xAxis: {
                    categories: [
                        '2017 Jan', '2017 Feb', '2017 Mar', '2017 Apr', '2017 May', '2017 Jun', '2017 Jul', '2017 Aug', '2017 Sep', '2017 Oct', '2017 Nov', '2017 Dec',
                        '2018 Jan', '2018 Feb', '2018 Mar', '2018 Apr', '2018 May', '2018 Jun', '2018 Jul', '2018 Aug', '2018 Sep', '2018 Oct', '2018 Nov', '2018 Dec',
                        '2019 Jan', '2019 Feb', '2019 Mar', '2019 Apr', '2019 May', '2019 Jun', '2019 Jul'],
                    tickInterval: 1,
                    labels: {
                        style: {
                            color: '#333',
                            fontSize: '12px',
                            textTransform: 'uppercase'
                        },
                        y: 20,
                        x: 10
                    },
                    lineColor: '#dadada'
                },
                yAxis: {
                    title: {
                        text: ''
                    }
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'center',
                    verticalAlign: 'bottom',
                    enabled: false

                },
                plotOptions: {
                    series: {
                        color: '#93dbd0',
                        shadow: true,
                        lineWidth: 3,
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    pointFormatter: function () {
                        var isNegative = this.y < 0 ? '-' : '';
                        return  isNegative + '$' + Math.abs(this.y.toFixed(0));
                    }
                },
                series: [{
                        name: '',
                        data: [90688,
                            90688 + 970.00,
                            90688 + 888.80,
                            90688 + 1009.60,
                            90688 + 1202.45,
                            90688 + 1460.47,
                            90688 + 1800.66,
                            90688 + 2092.99,
                            90688 + 2207.57,
                            90688 + 2654.50,
                            90688 + 3037.57,
                            90688 + 4883.94,
                            90688 + 5762.66,
                            90688 + 6864.18,
                            90688 + 7903.55,
                            90688 + 9018.43,
                            90688 + 10095.45,
                            90688 + 13109.44,
                            90688 + 19630.21,
                            90688 + 22010.11,
                            113081.42,
                            114035.62,
                            115055.92,
                            117057.41,
                            120343.07,
                            123068.20,
                            125022.82,
                            127051.06,
                            128001.64,
                            130004.75,
                            134644.35]
                    }],
                responsive: {
                    rules: [{
                            condition: {
                                maxWidth: 500
                            }
                        }]
                }

            });
        </script>  
    </body>
</html>