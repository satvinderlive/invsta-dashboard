<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Advisor Dashboard</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <!--<link href="resources/images/mint.png" rel="shortcut icon"/>-->
        <link rel="icon" href="./resources/images/invsta-color-logo.png" type="image/gif" sizes="16x16">
        <link href="https://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <link href="https://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>
        <link href="https://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>
        <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.css">
        <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.css">
        <link href="./resources/css/main.css" rel="stylesheet"/>
        
        <link href="./resources/css/custom.css" rel="stylesheet"/>
        <link href="./resources/css/newcss/new-custom.css" rel="stylesheet"/>
        <link href="./resources/css/newcss/new-main.css" rel="stylesheet"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css'>
        <style>

            .profile-img{
                text-align: center;
            }
            .profile-img img{
                width: 70%;
                height: 100%;
            }
            .profile-img .file {
                position: relative;
                overflow: hidden;
                margin-top: -20%;
                width: 70%;
                border: none;
                border-radius: 0;
                font-size: 15px;
                background: #212529b8;
            }
            .profile-img .file input {
                position: absolute;
                opacity: 0;
                right: 0;
                top: 0;
            }
            .profile-head h5{
                color: #333;
            }
            .profile-head h6{
                color: #0062cc;
            }
            .profile-edit-btn{
                border: none;
                border-radius: 1.5rem;
                width: 70%;
                padding: 2%;
                font-weight: 600;
                color: #6c757d;
                cursor: pointer;
            }
            .proile-rating{
                font-size: 12px;
                color: #818182;
                margin-top: 5%;
            }
            .proile-rating span{
                color: #495057;
                font-size: 15px;
                font-weight: 600;
            }
            .profile-head .nav-tabs{
                margin-bottom:5%;
            }
            .profile-head .nav-tabs .nav-link{
                font-weight:600;
                border: none;
            }
            .profile-head .nav-tabs .nav-link.active{
                border: none;
                border-bottom:2px solid #0062cc;
            }
            .profile-work{
                padding: 14%;
                margin-top: -15%;
            }
            .profile-work p{
                font-size: 12px;
                color: #818182;
                font-weight: 600;
                margin-top: 10%;
            }
            .profile-work a{
                text-decoration: none;
                color: #495057;
                font-weight: 600;
                font-size: 14px;
            }
            .profile-work ul{
                list-style: none;
            }
            .profile-tab label{
                font-weight: 600;
            }
            .profile-tab p{
                font-weight: 600;
                color: #0062cc;
            }
            .el-chart-w {
                overflow: visible!important;
            }
            .portfolio_overlay {
                height: 75%;
                display: block;
            }
            .ilumony-box:hover .portfolio_overlay {
                display: block;
            }
            .funds-deatil {
                background: #fff;
                padding: 1rem;
                border-radius: 4px;
                margin-bottom: 10px;
            }
            .show_more_btn_new {
                position: absolute;
                top: 0;
                right: 0px;
                margin: 11px 27px;
            }
            .funds-deatil h4 {
                max-width: 80%;
                margin-bottom: 15px;
            }
            .collapse {
                display: none;
            }
            .collapse.show {
                display: block;
            }
            .advisor-pie-section {
                padding: 0;
                border-radius: 39px;
            }



            .advisor-pie-section span:nth-child(2) {
                background: #e5f5f3;
                padding: 6px;
                width: 129px;
                text-align: center;
                border-radius: 40px;
            }

            .advisor-pie-section span {
                padding: 7px 14px;
            }

            .content-i {
                background: #f5f9f8;
            }



            .advisor-pie-data.owl_add .advisor-pie-section {
                background: #8c9296;
                padding: 0;
                border-radius: 39px;
            }



            .advisor-pie-data.owl_add .advisor-pie-section span:nth-child(2) {
                background: #ffff;
                padding: 6px;
                width: 150px;
                text-align: center;
                border-radius: 40px;
                    position: relative;
                     left: 3px;
            }

            .advisor-pie-data.owl_add .advisor-pie-section span {
                padding: 7px 14px;
            }
            .element-box {
                background: white;
            }
            .clr-cng {
                background: #b6000b;
            }


        </style>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"></jsp:include>  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <span><a href="./home">Home</a></span>
                        </li>
                        <li class="breadcrumb-item">
                            <span class="right-mark"><i class="fa fa-caret-right"></i></span>
                        </li>
                        <li class="breadcrumb-item">
                            <span><a href="javascript:void(0)">Wealth Management Dashboard</a></span>
                        </li>

                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="das-board" id="investments" hidden>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="element-wrapper">
                                            <h6 class="element-header">Investments </h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="portfolio" style="display: none;">
                                    <div class="col-md-12">
                                        <div class="element-box ele-overlay" style="background:url(https://backoffice.invsta.io/pocv/resources/img/) center center no-repeat ;background-size:cover">
                                            <h6 style="margin-bottom:20px; display: inline-block; margin-right: 5px;" class="pcName">Australasian Equity Fund</h6>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="element-box el-tablo centered trend-in-corner smaller">
                                                        <div class="label">
                                                            Contribution
                                                        </div>
                                                        <div class="value contribution">
                                                            NZ$ 0.00 
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="element-box el-tablo centered trend-in-corner smaller">
                                                        <div class="label ">
                                                            Current Balance
                                                        </div>
                                                        <div class="value marketValue" id="currentInvSqr">
                                                            NZ$ 0.00
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="element-box el-tablo centered trend-in-corner smaller">
                                                        <div class="label">
                                                            Performance
                                                        </div>
                                                        <div class="arrow-text">
                                                            <div class="value per- per-color- percentage">
                                                                33.66 %
                                                            </div>

                                                            <i class=" os-icon os-icon-arrow-up6"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="element-box el-tablo centered trend-in-corner smaller">
                                                        <div id="lg-latestweek1" style="height:70px; width:100%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="show_more_btn">
                                                <a class="btn btn-primary btn-style showmemore" href="javascript:void(0)">Show Me More</a>
                                            </div>
                                        </div>
                                    </div>                                                                             
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-md-12">
                                    <div class="head-option">
                                        <h6>Hi John, welcome to Invsta. Here’s an overview of your investments with us. </h6><br>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <!--                                        <div class="element-actions">
                                                                                    <button type="button" class="btn btn-info btn-lg advisorModel" data-toggle="modal" data-target="#advisorModel" onclick="advisorModel(this);">Add Advisor </button>
                                                                                </div>-->

                                        <div class="element-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <div class="advi-add">
                                                                <!--                                                                <h6 class="element-header">
                                                                                                                                    ASSET
                                                                                                                                </h6>-->
                                                                <div class="element-box">
                                                                    <div id="advisor-pie" style="height: 250px; max-width: 100%; margin: 0 auto"></div>

                                                                </div>
                                                            </div>
                                                        </div> 
                                                        <div class="col-md-5">
                                                            <div class="advisor-pie-data  owl_add">
                                                                <div class="advisor-pie-section ">
                                                                    <span class="section-color">Private Portfolio Value</span> 
                                                                    <span class="contributionspan">$4,500,376</span> 
                                                                </div>
                                                                <div class="advisor-pie-section">
                                                                    <span class="section-color">Private Portfolio Performance</span> 
                                                                    <span>6.32%</span> 
                                                                </div>
                                                                <div class="advisor-pie-section">
                                                                    <span class="section-color">Private Portfolio Risk Profile</span> 
                                                                    <span>Growth</span> 
                                                                </div>
                                                                <div class="advisor-pie-section active-color">
                                                                    <span class="section-color">KiwiSaver Value</span> 
                                                                    <span class="withdrawalspan">$1,280,302</span> 
                                                                </div>
                                                                <div class="advisor-pie-section active-color">
                                                                    <span class="section-color">KiwiSaver Performance</span> 
                                                                    <span>8.15%</span> 
                                                                </div>
                                                                <div class="advisor-pie-section active-color">
                                                                    <span class="section-color">KiwiSaver Risk Profile</span> 
                                                                    <span>Growth</span> 
                                                                </div>

                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6 col-sm-4">
                                                            <div class="element-box el-tablo clr-cng">
                                                                <div class="label">
                                                                    Wealth Investment Types 
                                                                </div>
                                                                <div class="value totaluser">
                                                                    2

                                                                </div>
                                                                <div class="trending trending-up">
                                                                    <!--<span>12%</span><i class="os-icon os-icon-arrow-up2"></i>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-6 col-sm-4">
                                                            <div class="element-box el-tablo clr-cng ">
                                                                <div class="label">
                                                                    Overall Performance
                                                                </div>
                                                                <div class="value totalinvestment">
                                                                    7.02%

                                                                </div>
                                                                <div class="trending trending-down-basic">
                                                                    <!--<span>12%</span><i class="os-icon os-icon-arrow-2-down"></i>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="element-box el-tablo clr-cng">
                                                                <div class="label">
                                                                    Total Current Value
                                                                </div>
                                                                <div class="value currentBalance">
                                                                    $5,780,678

                                                                </div>
                                                                <div class="trending trending-down-basic">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-4">

                                    <div class="element-box">
                                        <h6>KiwiSaver Growth Fund </h6>
                                        <p class="fund-part">Asset allocation</p>
                                        <div class="el-chart-w" id="add-chart-1" style="height:448px"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="element-box">
                                        <h6>Private Portfolio </h6>
                                        <p class="fund-part">Asset allocation</p>
                                        <div class="el-chart-w" id="add-chart-2" style="height:448px"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="element-box">
                                        <h6>Consolidated </h6>
                                        <p class="fund-part">Asset allocation</p>
                                        <div class="el-chart-w" id="add-chart-3" style="height:448px"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="hlodings-box">
                                        <h6>Geographical Allocation</h6>
                                        <div class="element-box" style="max-width: 100%">
                                            <div id="countryhighchart"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="hlodings-box">
                                        <h6>Top 5 Holdings</h6>
                                        <div class="element-box">
                                            <div class="top-holdings">
                                                <div class="first-holding">
                                                    <span>Magellan Global Fund</span>
                                                    <span>7%</span>
                                                </div>
                                                <div class="first-holding">
                                                    <span>Diversified Bond Fund</span>
                                                    <span>5%</span>
                                                </div>
                                                <div class="first-holding">
                                                    <span>Australian Diversified Share Fund</span>
                                                    <span>4%</span>
                                                </div>
                                                <div class="first-holding">
                                                    <span>Property Fund</span>
                                                    <span>2.5%</span>
                                                </div>
                                                <div class="first-holding">
                                                    <span>Corporate Bond Fund</span>
                                                    <span>6%</span>
                                                </div>
                                                <div class="first-holding">
                                                    <span><h6>Total</h6></span>
                                                    <span><h6>24.5%</h6></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h6 class="hearder-int">Investment Accounts </h6>
                            <div class="das-board new-das" id="investments">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="element-wrapper">

                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="portfolio" style="display: none;">
                                    <div class="col-md-12">
                                        <div class="element-box ele-overlay" style="background:url(https://backoffice.invsta.io/pocv/resources/img/) center center no-repeat ;background-size:cover">
                                            <h6 style="margin-bottom:20px; display: inline-block; margin-right: 5px;" class="pcName">Australasian Equity Fund</h6>
                                            <div class="row">
                                                <div class="col-md-3 add-cls-box">
                                                    <div class="element-box el-tablo centered trend-in-corner smaller">
                                                        <div class="label">
                                                            Contribution
                                                        </div>
                                                        <div class="value contribution">
                                                            NZ$ 0.00 
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-3 add-cls-box">
                                                    <div class="element-box el-tablo centered trend-in-corner smaller">
                                                        <div class="label ">
                                                            Current Balance
                                                        </div>
                                                        <div class="value marketValue" id="currentInvSqr">
                                                            NZ$ 0.00
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-3 add-cls-box">
                                                    <div class="element-box el-tablo centered trend-in-corner smaller">
                                                        <div class="label">
                                                            Performance
                                                        </div>
                                                        <div class="arrow-text">
                                                            <div class="value per- per-color- percentage">
                                                                33.66 %
                                                            </div>

                                                            <i class=" os-icon os-icon-arrow-up6"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 add-cls-box">
                                                    <div class="element-box el-tablo centered trend-in-corner smaller">
                                                        <div class="label">
                                                            <div class="show_btn lable">
                                                                <a class="btn btn-primary btn-style showmemore" href="javascript:void(0)">Show Me More</a>
                                                            </div>
                                                        </div>


                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>                                                                             
                                </div>
                                <div class="row" id="portfolio" style="display: block;">
                                    <div class="col-md-12">
                                        <div class="element-box ele-overlay" style="background:url(https://backoffice.invsta.io/pocv/resources/img/) center center no-repeat ;background-size:cover">
                                            <h6 style="margin-bottom:10px; display: inline-block; margin-right: 5px;" class="pcName">Growth KiwiSaver Fund</h6>
                                            <div class="row">

                                                <div class="col-6 col-sm-3">
                                                    <div class="element-box el-tablo clr-cng">
                                                        <div class="label font-capital">
                                                            Contribution
                                                        </div>
                                                        <div class="value contribution">$952,649</div>
<!--                                                        <div class="trending trending-up">
                                                            <span>12%</span><i class="os-icon os-icon-arrow-up2"></i>
                                                        </div>-->
                                                    </div>
                                                </div>
                                                <div class="col-6 col-sm-3">
                                                    <div class="element-box el-tablo clr-cng">
                                                        <div class="label font-capital">
                                                            Current Balance
                                                        </div>
                                                        <div class="value contribution">$1,280,302</div>
<!--                                                        <div class="trending trending-up">
                                                            <span>12%</span><i class="os-icon os-icon-arrow-up2"></i>
                                                        </div>-->
                                                    </div>
                                                </div>
                                                <div class="col-6 col-sm-3">
                                                    <div class="element-box el-tablo clr-cng">
                                                        <div class="label font-capital">
                                                            Performance
                                                        </div>
                                                        <div class="value contribution">34%</div>
<!--                                                        <div class="trending trending-up">
                                                            <span>12%</span><i class="os-icon os-icon-arrow-up2"></i>
                                                        </div>-->
                                                    </div>
                                                </div>
                                                <div class="col-6 col-sm-3">
                                                    <div class="element-box el-tablo clr-cng admin-show">
                                                        <div class="label font-capital">
                                                            <div class="show_btn lable">
                                                                <a class="btn btn-primary btn-style showmemore" href="./home-beneficiary-new-dashboard-db">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>                                                                             
                                </div><div class="row" id="portfolio" style="display: block;">
                                    <div class="col-md-12">
                                        <div class="element-box ele-overlay" style="background:url(https://backoffice.invsta.io/pocv/resources/img/) center center no-repeat ;background-size:cover">
                                            <h6 style="margin-bottom:10px; display: inline-block; margin-right: 5px;" class="pcName">Private Portfolio </h6>
                                            <div class="row">
                                                <div class="col-6 col-sm-3">
                                                    <div class="element-box el-tablo clr-cng">
                                                        <div class="label font-capital">
                                                            Contribution
                                                        </div>
                                                        <div class="value contribution">$3,800,000</div>
<!--                                                        <div class="trending trending-up">
                                                            <span>12%</span><i class="os-icon os-icon-arrow-up2"></i>
                                                        </div>-->
                                                    </div>
                                                </div>
                                                <div class="col-6 col-sm-3">
                                                    <div class="element-box el-tablo clr-cng">
                                                        <div class="label font-capital">
                                                            Current Balance
                                                        </div>
                                                        <div class="value contribution">$4,500,376</div>
<!--                                                        <div class="trending trending-up">
                                                            <span>12%</span><i class="os-icon os-icon-arrow-up2"></i>
                                                        </div>-->
                                                    </div>
                                                </div>
                                                <div class="col-6 col-sm-3">
                                                    <div class="element-box el-tablo clr-cng">
                                                        <div class="label font-capital">
                                                            Performance
                                                        </div>
                                                        <div class="value contribution">18.4%</div>
<!--                                                        <div class="trending trending-up">
                                                            <span>12%</span><i class="os-icon os-icon-arrow-up2"></i>
                                                        </div>-->
                                                    </div>
                                                </div>
                                                <div class="col-6 col-sm-3">
                                                    <div class="element-box el-tablo clr-cng admin-show">
                                                        <div class="label font-capital">
                                                            <div class="show_btn lable">
                                                                <a class="btn btn-primary btn-style showmemore" href="./home-show-me-more-db">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>                                                                             
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script src="https://code.highcharts.com/maps/highmaps.js"></script>
        <script src="https://code.highcharts.com/maps/modules/data.js"></script>
        <script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/maps/modules/offline-exporting.js"></script>
        <script src="https://code.highcharts.com/mapdata/custom/world.js"></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js'></script>
        <script src='https://use.fontawesome.com/826a7e3dce.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.js"></script> 

        <script>
            Highcharts.setOptions({
                colors: ['#54565a', '#2c94ec']
            });
            Highcharts.chart('advisor-pie', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Summary',
                    margin: -5,
                    align: 'left'
                },
                legend: {
                    align: 'right',
                    verticalAlign: 'middle',
                    layout: 'vertical',
                    title: {
//                            text:[["rony"],["neeraj"]]
                    }

                },
                responsive: {
                    rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    enabled: false
                                }
                            }
                        }]
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    pie: {
                        innerSize: 110,
                        depth: 0,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: 20,
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Allocation',
                        colorByPoint: true,
                        data: [{
                                name: 'Private Portfolio',
                                y: 61.41,
                                sliced: true,
                                selected: true
                            }, {
                                name: 'KiwiSaver',
                                y: 11.84
                            }]
                    }]
            });
        </script>


        <script>
            Highcharts.setOptions({
                colors: ['#2d373e', '#575f65', '#2c94ec']
            });
            Highcharts.chart('add-chart-1', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                credits: {
                    enabled: false,
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                plotOptions: {
                    pie: {
                        innerSize: 200,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            distance: 2,
                            connectorWidth: 0,
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Allocation',
                        colorByPoint: true,
                        data: [{
                                name: 'Australian Equities',
                                y: 70
                            }, {
                                name: 'New Zealand Equities ',
                                y: 10
                            }, {
                                name: 'Emerging Economies',
                                y: 10
                            }]
                    }]
            });
        </script>
        <script>
            Highcharts.setOptions({
                colors: ['#575f65', '#d7d2cb', '#2c94ec']
            });
            Highcharts.chart('add-chart-2', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                credits: {
                    enabled: false,
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                plotOptions: {
                    pie: {
                        innerSize: 200,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            distance: 2,
                            connectorWidth: 0,
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Allocation',
                        colorByPoint: true,
                        data: [{
                                name: 'Bonds',
                                y: 70
                            }, {
                                name: 'New Zealand Equities',
                                y: 10
                            }, {
                                name: 'Emerging Economies',
                                y: 10
                            }]
                    }]
            });
        </script>
        <script>
            Highcharts.setOptions({
                colors: ['#2c94ec', '#2d373e', '#575f65']
            });
            Highcharts.chart('add-chart-3', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                credits: {
                    enabled: false,
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                plotOptions: {
                    pie: {
                        innerSize: 200,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            distance: 2,
                            connectorWidth: 0,
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Allocation',
                        colorByPoint: true,
                        data: [{
                                name: 'New Zealand Equities',
                                y: 70
                            }, {
                                name: 'Australian Equities ',
                                y: 10
                            }, {
                                name: 'Cash',
                                y: 10
                            }]
                    }]
            });
        </script>

        <script>
            Highcharts.getJSON('https://cdn.jsdelivr.net/gh/highcharts/highcharts@v7.0.0/samples/data/world-population-density.json', function (data) {
                var total = 94579.83999999998;
                // Prevent logarithmic errors in color calulcation
//                 data.forEach(function (p) {
//                    total = total+ (p.value < 1 ? 1 : p.value);
//                });
//                 console.log(total);
                data.forEach(function (p) {
                    p.value = (p.value < 1 ? 1 : p.value);
                    console.log(p.value);
                });
                // Initiate the chart
                Highcharts.mapChart('countryhighchart', {
                    chart: {
                        map: 'custom/world'
                    },
                    title: {
                        text: ''
                    },
                    mapNavigation: {
                        enabled: false,
                        enableDoubleClickZoomTo: true
                    },
                    colorAxis: {
                        min: 1,
                        max: 1000,
                        type: 'logarithmic',
                        stops: [
                            [0, '#2c94ec'],
                            [0.5, Highcharts.getOptions().colors[1]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[1]).brighten(-0.5).get()]
                        ]
                    },
                    credits: {
                        enabled: false,
                    },
                    navigation: {
                        buttonOptions: {
                            enabled: false
                        }
                    },
                    legend: {
                        enabled: false
                    },
//                    tooltip: {
//                        pointFormat: '{series.name}: <b>{value.percentage:.1f}%</b>'
//                    },

                    series: [{
                            data: data,
                            joinBy: ['iso-a3', 'code3'],
                            name: 'Investment Allocation',
                            states: {
                                hover: {
                                    color: '#304bb7'
                                }
                            },
                            tooltip: {
                                valueSuffix: '%'
                            }
                        }]
                });
            });

        </script>

    </body>
</html>