<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>westpac</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="http://backoffice.invsta.io/mint/images/mint.png" rel="shortcut icon"/>
        <link href="http://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <!--<link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>-->
        <link href="http://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>
        <!--<link href="http://backoffice.invsta.io/pocv/resources/css/main.css?version=3.5.1" rel="stylesheet"/>-->
        <link href="http://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link href="./resources/css/custom.css" rel="stylesheet"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
        <!--  <link href="resources/css/osfonts.css" rel="stylesheet"> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

        <link rel="stylesheet" href="./resources/css/acc-style.css">


        <style>

            img.logo {
                width: 40%;
            }

            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('./resources/images/Preloader_3.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
            .content_body {
               max-width: 100%;
                margin: auto;
                margin-top: 10px;
                background: #e0e0df9e;
                border-radius: 10px;
            }

            .content-section {
                padding: 40px;
            }
            .btns {
                display: flex;
                justify-content: space-between;
            }
            #msform fieldset:not(:first-of-type) {
                display: none;
            }
        </style>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Payment</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
<!--                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">

                                <div class="col-md-12 invst-option">
                                    <div class="card mt-3 tab-card">
                                         <div class="card-header tab-card-header">
                                                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                                        <li class="nav-item">
                                                                <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Funds</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Direct Companies</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">New Offers</a>
                                                        </li>
                                                </ul>
                                        </div> 

                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active p-31" id="one" role="tabpanel" aria-labelledby="one-tab">
                                                <div class="row1">
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="element-header">Payment</h6>
                                                        </div>

                                                        <div class="form-data">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12  box-payment-space">
                                                                        <div class="content-body payment-box-space payment-overflow">
                                                                            <div class="line-progress">
                                                                                <div class="line-bar2">
                                                                                    <div class="line-dot">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="line-data">
                                                                                    <span>Amount</span>
                                                                                    <span>You</span>
                                                                                    <span>Recipient</span>
                                                                                    <span>Review</span>
                                                                                    <span>Pay</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="header-border">
                                                                            </div>
                                                                            <div class="input-content-text">
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <h5 class="text-center">How much would you like to transfer?</h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="currency-page">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div class="body-section">

                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">

                                                                                            <div class="calculator-page">
                                                                                                <div class="calculator-input">
                                                                                                    <p class="input-text">You Send</p>
                                                                                                    <input type="text" class="flag-input" placeholder="0" >
                                                                                                </div>
                                                                                                <div class="calculator-dropdown">
                                                                                                    <ul class="list-unstyled">
                                                                                                        <li class="init"><img class="flag-images" src="./resources/images/nzd.png"> NZD<i class="fa fa-angle-down dropdown-flag"></i></li>
                                                                                                        <li data-value="value 1"><img class="flag-images" src="./resources/images/nzd.png"> NZD </li>
                                                                                                        <li data-value="value 2"><img class="flag-images" src="./resources/images/usd.png"> USD</li>
                                                                                                        <li data-value="value 3"><img class="flag-images" src="./resources/images/inr.png"> INR</li>
                                                                                                        <li data-value="value 4"><img class="flag-images" src="./resources/images/gbp.png"> GBP</li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class="calculate-data see-space">
                                                                                                <a href="javascript:void(0)" class="see-toggle">See calculation</a>
                                                                                            </div>
                                                                                            <div class="calculate-data data-toggle">
                                                                                                <span class="six-value six-new">0 INR</span>
                                                                                                <select class="select-space">
                                                                                                    <option value="1">Bank Transfer</option>
                                                                                                </select>
                                                                                                <span>fee</span>
                                                                                            </div>
                                                                                            <div class="calculate-data data-toggle">
                                                                                                <span class="six-value six-new">9.10 NZD Our fees</span>
                                                                                            </div>
                                                                                            <div class="calculate-data">
                                                                                                <span class="six-value">9.10 NZD Total fees</span>
                                                                                            </div>
                                                                                            <div class="calculate-data data-toggle">
                                                                                                <span class="six-value six-new">99,198.78 INR Amount weâ€™ll convert</span>
                                                                                            </div>
                                                                                            <div class="calculate-data cal-result">
                                                                                                <span class="six-value">0.660900</span> <span class="rate-value">Guaranteed rate (48hrs)</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">

                                                                                            <div class="calculator-page">
                                                                                                <div class="calculator-input">
                                                                                                    <p class="input-text">You Send</p>
                                                                                                    <input type="text" class="flag-input" placeholder="0">
                                                                                                </div>
                                                                                                <div class="calculator-dropdown2">
                                                                                                    <ul class="list-unstyled">
                                                                                                        <li class="init2"><img class="flag-images" src="./resources/images/usd.png"> USD<i class="fa fa-angle-down dropdown-flag "></i></li>
                                                                                                        <li data-value="value 1"><img class="flag-images" src="./resources/images/nzd.png"> NZD</li>
                                                                                                        <li data-value="value 2"><img class="flag-images" src="./resources/images/usd.png"> USD</li>
                                                                                                        <li data-value="value 3"><img class="flag-images" src="./resources/images/inr.png"> INR</li>
                                                                                                        <li data-value="value 4"><img class="flag-images" src="./resources/images/gbp.png"> GBP</li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class="shoul-data">
                                                                                                <p>Should arrive <span class="six-value">by Aug 2</span></p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class="result-btn">
                                                                                                <a href="javascript:void(0)" class="btn btn-primary btn-styl">Compare Price</a>
                                                                                                <a href="javascript:void(0)" class="btn btn-primary btn-styl" id="continue">Continue</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <a href="#" class="btn btn-primary" id="viewmore1">View More</a>               
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>-->



                    </div>
                
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="content_body">
                                    <form id="msform">
                                        <fieldset id="step1">
                                            <div class="content-section">
                                                <div class="row">
                                                    <div class="col-md-12 thanku">
                                                   <div class="form-data">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12  box-payment-space">
                                                                        <div class="content-body payment-box-space payment-overflow">
                                                                            <div class="line-progress">
                                                                                <div class="line-bar2">
                                                                                    <div class="line-dot">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="line-data">
                                                                                    <span>Amount</span>
                                                                                    <span>You</span>
                                                                                    <span>Recipient</span>
                                                                                    <span>Review</span>
                                                                                    <span>Pay</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="header-border">
                                                                            </div>
                                                                            <div class="input-content-text">
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <h5 class="text-center">How much would you like to transfer?</h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="currency-page">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div class="body-section">

                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">

                                                                                            <div class="calculator-page">
                                                                                                <div class="calculator-input">
                                                                                                    <p class="input-text">You Send</p>
                                                                                                    <input type="text" class="flag-input" placeholder="0">
                                                                                                </div>
                                                                                                <div class="calculator-dropdown">
                                                                                                    <ul class="list-unstyled">
                                                                                                        <li class="init"><img class="flag-images" src="./resources/images/nzd.png"> NZD<i class="fa fa-angle-down dropdown-flag"></i></li>
                                                                                                        <li data-value="value 1"><img class="flag-images" src="./resources/images/nzd.png"> NZD </li>
                                                                                                        <li data-value="value 2"><img class="flag-images" src="./resources/images/usd.png"> USD</li>
                                                                                                        <li data-value="value 3"><img class="flag-images" src="./resources/images/inr.png"> INR</li>
                                                                                                        <li data-value="value 4"><img class="flag-images" src="./resources/images/gbp.png"> GBP</li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class="calculate-data see-space">
                                                                                                <a href="javascript:void(0)" class="see-toggle">See calculation</a>
                                                                                            </div>
                                                                                            <div class="calculate-data data-toggle" style="display: none;">
                                                                                                <span class="six-value six-new">0 INR</span>
                                                                                                <select class="select-space">
                                                                                                    <option value="1">Bank Transfer</option>
                                                                                                </select>
                                                                                                <span>fee</span>
                                                                                            </div>
                                                                                            <div class="calculate-data data-toggle" style="display: none;">
                                                                                                <span class="six-value six-new">9.10 NZD Our fees</span>
                                                                                            </div>
                                                                                            <div class="calculate-data">
                                                                                                <span class="six-value">9.10 NZD Total fees</span>
                                                                                            </div>
                                                                                            <div class="calculate-data data-toggle" style="display: none;">
                                                                                                <span class="six-value six-new">99,198.78 INR Amount weâ€™ll convert</span>
                                                                                            </div>
                                                                                            <div class="calculate-data cal-result">
                                                                                                <span class="six-value">0.660900</span> <span class="rate-value">Guaranteed rate (48hrs)</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">

                                                                                            <div class="calculator-page">
                                                                                                <div class="calculator-input">
                                                                                                    <p class="input-text">You Send</p>
                                                                                                    <input type="text" class="flag-input" placeholder="0">
                                                                                                </div>
                                                                                                <div class="calculator-dropdown2">
                                                                                                    <ul class="list-unstyled">
                                                                                                        <li class="init2"><img class="flag-images" src="./resources/images/usd.png"> USD<i class="fa fa-angle-down dropdown-flag "></i></li>
                                                                                                        <li data-value="value 1"><img class="flag-images" src="./resources/images/nzd.png"> NZD</li>
                                                                                                        <li data-value="value 2"><img class="flag-images" src="./resources/images/usd.png"> USD</li>
                                                                                                        <li data-value="value 3"><img class="flag-images" src="./resources/images/inr.png"> INR</li>
                                                                                                        <li data-value="value 4"><img class="flag-images" src="./resources/images/gbp.png"> GBP</li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class="shoul-data">
                                                                                                <p>Should arrive <span class="six-value">by Aug 2</span></p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class="result-btn">
                                                                                                <a href="javascript:void(0)" class="btn btn-primary btn-styl">Compare Price</a>
                                                                                                <a href="javascript:void(0)" class="btn btn-primary btn-styl" id="continue">Continue</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="btns">
                                                            <input type="button" name="previous" value="Previous" class="previous1 ">
                                                            <input type="button" name="previous" value="Next" class="next1">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset id="step2">
                                            <div class="content-section">
                                                <div class="row">
                                                    <div class="col-md-12 thanku">
                                                       <div class="form-data">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12  box-payment-space">
                                                                        <div class="content-body payment-box-space">
                                                                            <div class="line-progress">
                                                                                <div class="line-bar5">
                                                                                    <div class="line-dot">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="line-data">
                                                                                    <span>Amount</span>
                                                                                    <span>You</span>
                                                                                    <span>Recipient</span>
                                                                                    <span>Review</span>
                                                                                    <span>Pay</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="header-border">
                                                                            </div>
                                                                            <div class="input-content-text">
<!--                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <h5 class="text-center">How will you make your bank transfer?</h5>
                                                                                        <p class="text-center">You"ll need to transfer <span class="bold-text-input">exactly 90NZD</span> from your own bank account to<br> TransferWise's NZD account.</p>
                                                                                        <a href="" class="payment-link">Or,pay another way.</a>
                                                                                    </div>
                                                                                    <div class="col-md-6 transfer-body">
                                                                                        <div class="header-paymet">
                                                                                            <h6>What kind of bank account will you use? </h6>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                        <div class="col-md-12">
                                                                                        <div class="personal-radio">
                                                                                        <div class="personal-new-radio">
                                                                                            
                                                                                            <label class="new-container">Personal account
                                                                                                <input type="radio" checked="checked" name="radio">
                                                                                                <span class="checkmark"></span>
                                                                                            </label>
                                                                                            <label class="new-container">Joint account
                                                                                                <input type="radio" name="radio">
                                                                                                <span class="checkmark"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                            <input class="transfer-text" type="text" placeholder="Where will you make your bank transfer from?">
                                                                                        </div>
                                                                                        </div>
                                                                                        </div>
                                                                                        <div class="body-bank transfer-bank">
                                                                                            <label class="bank-radio intro">
                                                                                                <i class="fa fa-globe" aria-hidden="true"></i>
                                                                                                <span class="bank-card">Online banking</span>
                                                                                                <input type="radio" checked="checked" name="radio2">
                                                                                                <span class="checkmark"></span>
                                                                                            </label>
                                                                                            <label class="bank-radio">
                                                                                                <i class="fa fa-phone" aria-hidden="true"></i>
                                                                                                <span class="bank-card">Telephone banking</span>
                                                                                                <input type="radio" name="radio2">
                                                                                                <span class="checkmark"></span>
                                                                                            </label>
                                                                                            <label class="bank-radio regular">
                                                                                                <i class="fa fa-university" aria-hidden="true"></i>
                                                                                                <span class="bank-card">In branch</span>
                                                                                                <input type="radio" name="radio2">
                                                                                                <span class="checkmark"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                        <div class="bottom-heading">
                                                                                            <p>
                                                                                                To get your guaranteed rate, we need to receive the full amount 
                                                                                                <span class="bold-text-input">by Friday 8:55pm.</span>
                                                                                            </p>
                                                                                        </div>
                                                                                        <div class="new1">
                                                                                            <a class="btn-bottom continue" href="javascript:void(0)">Continue</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>-->
<div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <h5 class="text-center" style="text-align:left!important">How would you like to pay?</h5>
                                                                                            </div>
                                                                                            <div class="col-md-12">
                                                                                                <div class="body-bank radoi-set">
                                                                                                    <label class="bank-radio  ">
                                                                                                        <i class="fa fa-university" aria-hidden="true"></i>
                                                                                                        <span class="bank-card">Poli</span>
                                                                                                        <p class="bank-card-text">4.71 NZD in total fees.</p>
                                                                                                        <input type="radio" checked="checked" name="radio">
                                                                                                        <span class="checkmark"></span>
                                                                                                    </label>
                                                                                                    <label class="bank-radio ">
                                                                                                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                                                                                                        <span class="bank-card">Debit Card</span>
                                                                                                        <p class="bank-card-text">5.20 NZD in total fees.</p>
                                                                                                        <input type="radio" name="radio">
                                                                                                        <span class="checkmark"></span>
                                                                                                    </label>
                                                                                                    <label class="bank-radio">
                                                                                                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                                                                                                        <span class="bank-card">Credit Card</span>
                                                                                                        <p class="bank-card-text">5.20 NZD in total fees.</p>
                                                                                                        <input type="radio" name="radio">
                                                                                                        <span class="checkmark"></span>
                                                                                                    </label>
                                                                                                    <label class="bank-radio continue">
                                                                                                        <i class="fa fa-university" aria-hidden="true"></i>
                                                                                                        <span class="bank-card">Bank Transfer</span>
                                                                                                        <p class="bank-card-text">3.44 NZD in total fees.</p>
                                                                                                        <input type="radio" name="radio">
                                                                                                        <span class="checkmark"></span>
                                                                                                    </label>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-12">
                                                                                                <div class="trans-bank trans-space">
                                                                                                    <h6 class="body-data">Transfer details</h6>
                                                                                                </div>
                                                                                                <div class="body-trans-space">
                                                                                                    <label class="balance-result">
                                                                                                        <i class="fa fa-bolt" aria-hidden="true"></i>
                                                                                                        <span class="bank-card">Your balance</span>
                                                                                                        <p class="bank-card-text">Open a borderless account and add funds to instantly pay for your transfers.</p>
                                                                                                    </label>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
<!--                                                                                    <div class="col-md-4">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="trans-detail">
                                                                                                    <div class="trans-bank">
                                                                                                        <h6 class="body-data">Transfer details</h6>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">You send</span> 
                                                                                                        <span class="second-span">100 NZD</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Fee (included):</span> 
                                                                                                        <span class="second-span">-4.71 NZD</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Amount we'll convert</span> 
                                                                                                        <span class="second-span">95.29 NZD</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Guaranteed rate (48 hours)</span> 
                                                                                                        <span class="second-span">0.66075</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">You get</span> 
                                                                                                        <span class="second-span">62.96 NZD</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Should arrive</span> 
                                                                                                        <span class="second-span">by Aug 1</span>
                                                                                                    </div>
                                                                                                    <div class="trans-bank trans-space">
                                                                                                        <h6 class="body-data">Recipient details</h6>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Name</span> 
                                                                                                        <span class="second-span">Kayla White</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Email</span> 
                                                                                                        <span class="second-span">white.kaylaj@ gmail.com</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Routing number</span> 
                                                                                                        <span class="second-span">031176110</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Account number</span> 
                                                                                                        <span class="second-span">157040704</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Account type</span> 
                                                                                                        <span class="second-span">Checking</span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>-->
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="btns">
                                                            <input type="button" name="previous" value="Previous" class="previous2 ">
                                                            <input type="button" name="previous" value="Next" class="next2">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset id="step3">
                                            <div class="content-section">
                                                <div class="row">
                                                    <div class="col-md-12 thanku">
                                                        <div class="form-data">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12 box-payment-space">
                                                                        <div class="content-body payment-box-space">
                                                                            <div class="line-progress">
                                                                                <div class="line-bar">
                                                                                    <div class="line-dot">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="line-data">
                                                                                    <span>Amount</span>
                                                                                    <span>You</span>
                                                                                    <span>Recipient</span>
                                                                                    <span>Review</span>
                                                                                    <span>Pay</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="header-border">

                                                                            </div>
                                                                            <div class="input-content-text">
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <h5 class="text-center">Who are you sending money to?</h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="body-section">
                                                                                            <h6 class="body-data">Existing recipients</h6>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-4 continue">
                                                                                        <div class="body-box">
                                                                                            <div class="body-icon">
                                                                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                                                            </div>
                                                                                            <h6 class="icon-name">Myself</h6>
                                                                                            <img src="./resources/images/nz-flag.png"> <span class="flag-data">USD Account</span>
                                                                                            <p>ending with 0704</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 continue">
                                                                                        <div class="body-box">
                                                                                            <div class="body-icon">
                                                                                                <i class="fa fa-user" aria-hidden="false"></i>
                                                                                            </div>
                                                                                            <h6 class="icon-name">David</h6>
                                                                                            <img src="./resources/images/nz-flag.png"> <span class="flag-data">USD Account</span>
                                                                                            <p>ending with 9524</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="body-section">
                                                                                            <h6 class="body-data">New recipient</h6>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-4 continue">
                                                                                        <div class="body-box">
                                                                                            <div class="body-icon">
                                                                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                                                            </div>
                                                                                            <h6 class="icon-name">Myself</h6>
                                                                                            <img src="./resources/images/nz-flag.png"> <span class="flag-data">USD Account</span>
                                                                                            <p>ending with 0704</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 continue">
                                                                                        <div class="body-box">
                                                                                            <div class="body-icon">
                                                                                                <i class="fa fa-user" aria-hidden="false"></i>
                                                                                            </div>
                                                                                            <h6 class="icon-name">Someone Else</h6>
                                                                                            <img src="./resources/images/nz-flag.png"> <span class="flag-data">USD Account</span>
                                                                                            <p>ending with 9524</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 continue">
                                                                                        <div class="body-box">
                                                                                            <div class="body-icon">
                                                                                                <i class="fa fa-user" aria-hidden="false"></i>
                                                                                            </div>
                                                                                            <h6 class="icon-name">Company Account</h6>
                                                                                            <img src="./resources/images/nz-flag.png"> <span class="flag-data">USD Account</span>
                                                                                            <p>ending with 8354</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="btns">
                                                            <input type="button" name="previous" value="Previous" class="previous3 ">
                                                            <input type="button" name="previous" value="Next" class="next3">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset id="step4">
                                            <div class="content-section">
                                                <div class="row">
                                                    <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="content-body">
                                                                            <div class="line-progress">
                                                                                <div class="line-bar6">
                                                                                    <div class="line-dot line-adjust">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="line-data">
                                                                                    <span>Amount</span>
                                                                                    <span>You</span>
                                                                                    <span>Recipient</span>
                                                                                    <span>Review</span>
                                                                                    <span>Pay</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="header-border">
                                                                            </div>
                                                                            <div class="input-content-text">
                                                                                <div class="main-header">
                                                                                    <h2>Review details of your transfer</h2>
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div class="trans-detail-2">
                                                                                                <div class="trans-bank-2">
                                                                                                    <h6 class="body-data">Transfer details</h6>
                                                                                                    <a href="">Edit</a>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">You send</span> 
                                                                                                    <span class="second-span">100 NZD</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Fee (included):</span> 
                                                                                                    <span class="second-span">-4.71 NZD</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Amount we'll convert</span> 
                                                                                                    <span class="second-span">95.29 NZD</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Guaranteed rate (48 hours)</span> 
                                                                                                    <span class="second-span">0.66075</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">You get</span> 
                                                                                                    <span class="second-span">62.96 NZD</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Should arrive</span> 
                                                                                                    <span class="second-span">by Aug 1</span>
                                                                                                </div>
                                                                                                <div class="heading-middle"></div>
                                                                                                <div class="trans-bank-2 trans-space">
                                                                                                    <h6 class="body-data">Recipient details</h6>
                                                                                                    <a href="">Changes</a>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Name</span> 
                                                                                                    <span class="second-span">Kayla White</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Email</span> 
                                                                                                    <span class="second-span">white.kaylaj@ gmail.com</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Routing number</span> 
                                                                                                    <span class="second-span">031176110</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Account number</span> 
                                                                                                    <span class="second-span">157040704</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Account type</span> 
                                                                                                    <span class="second-span">Checking</span>
                                                                                                </div>
                                                                                                <div class="count-btn">
                                                                                                    <a href="./maketransfer">Continue</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="btns">
                                                            <input type="button" name="previous" value="Previous" class="previous4 ">
                                                            <input type="button" name="previous" value="Next" class="next4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                         <fieldset id="step5">
                                            <div class="content-section">
                                                <div class="row">
                                                    <div class="col-md-12 thanku">
                                                  <div class="form-data">
                                                          <div class="form-data">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12 box-payment-space">
                                                                        <div class="content-body payment-box-space">
                                                                            <div class="line-progress">
                                                                                <div class="line-bar1 ">
                                                                                    <div class="line-dot ">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="line-data">
                                                                                    <span>Amount</span>
                                                                                    <span>You</span>
                                                                                    <span>Recipient</span>
                                                                                    <span>Review</span>
                                                                                    <span>Pay</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="header-border">

                                                                            </div>
                                                                            <div class="input-content-text">
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <h5 class="text-center">Bank transfer Instructions?</h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="currency-page1">
                                                                                    <div class="trans-detail">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="body-section">

                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="instruction-body">
                                                                                                    <h5>Payee name</h5>
                                                                                                    <!--<a href="" data-toggle="tooltip" data-placement="auto" title=""></a>-->
                                                                                                    <div class="tooltip">TransferWise Limited <span class="tooltiptext">Don't forget your reference This code id unique to you and hels us process your bank tranfer fater. When you make your bank transfer to us, please make sure you write it in the reference field.</span></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="instruction-body">
                                                                                                    <h5>Use the reference</h5>
                                                                                                    <!--<a href="" data-toggle="tooltip" data-placement="auto" title="Don't forget your reference This code id unique to you and hels us process your bank tranfer fater. When you make your bank transfer to us, please make sure you write it in the reference field.">P2980490</a>-->
                                                                                                    <div class="tooltip">P2980490 <span class="tooltiptext">Don't forget your reference This code id unique to you and hels us process your bank tranfer fater. When you make your bank transfer to us, please make sure you write it in the reference field.</span></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="instruction-body">
                                                                                                    <h5>Amount to send</h5>
                                                                                                    <!--<a href="" data-toggle="tooltip" data-placement="auto" title="Don't forget your reference This code id unique to you and hels us process your bank tranfer fater. When you make your bank transfer to us, please make sure you write it in the reference field.">90 NZD</a>-->
                                                                                                    <div class="tooltip">90 NZD <span class="tooltiptext">Don't forget your reference This code id unique to you and hels us process your bank tranfer fater. When you make your bank transfer to us, please make sure you write it in the reference field.</span></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="instruction-body">
                                                                                                    <h5>Account number</h5>
                                                                                                    <!--<a href="" data-toggle="tooltip" data-placement="auto" title="Don't forget your reference This code id unique to you and hels us process your bank tranfer fater. When you make your bank transfer to us, please make sure you write it in the reference field.">02-0108-0344040-000</a>-->
                                                                                                    <div class="tooltip">02-0108-0344040-000<span class="tooltiptext">Don't forget your reference This code id unique to you and hels us process your bank tranfer fater. When you make your bank transfer to us, please make sure you write it in the reference field.</span></div>
                                                                                                   
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-12">
                                                                                                <div class="instruction-body">
                                                                                                    <p>TransferWise never takes money automatically from your account.</p>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-12">
                                                                                                <div class="instruction-body">
                                                                                                    <p>You can use your bank's online banking or mobile app to make your bank transfer to TransferWise.</p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="instruction-btn">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="tree-btn">
                                                                                                    <a href="javascript:void(0)" class="btn btn-primary btn-styl">I've made my bank transfer</a>
                                                                                                    <a href="javascript:void(0)" class="btn btn-primary btn-styl transparent-style">I'll transfer my money later</a>
                                                                                                    <a href="javascript:void(0)" class="btn btn-primary btn-styl transparent-style">Cancel this transfer</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="btns">
                                                            <input type="button" name="previous" value="Previous" class="previous4 ">
                                                            <input type="button" name="previous" value="Next" class="next5">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <!-- <script src="resources/bower_components/moment/moment.js" ></script>
        
        <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
        <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
        <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="http://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>
        <script type="text/javascript">
            $(window).load(function () {

                $(".loader").fadeOut("slow");
            });
        </script>

        <!--        <script>
                    //        $('#viewmore1').click(function () {
                    //            $('.hidediv1').show();
                    //            $('#viewmore1').hide();
                    //        });
                    //        $('#viewmore2').click(function () {
                    //            $('.hidediv2').show();
                    //            $('#viewmore2').hide();
                    //        });
                    //        $('#viewmore3').click(function () {
                    //            $('.hidediv3').show();
                    //            $('#viewmore3').hide();
                    //        });
                    $('.clickinput').on("click", function () {
                        var recemt = $(this).closest('.funds-deatil');
                        recemt.find(".offer-input").toggle();
                    });
                    $(document).ready(function () {
                        $(".loader").fadeOut("slow");
                        $('.hidediv1').hide();
                        $('.hidediv2').hide();
                        $('.hidediv3').hide();
                    });
                    $('.democlick').click(function () {
                        var data = $(this).data("target");
                        $('.showclick').hide();
                        $(data).show();
                    });
                </script>-->
        <script>
            $(".calculator-dropdown ul").on("click", ".init", function () {
                $(this).closest(".calculator-dropdown ul").children('li:not(.init)').toggle();
            });

            var allOptions = $(".calculator-dropdown ul").children('li:not(.init)');
            $(".calculator-dropdown ul").on("click", "li:not(.init)", function () {
                allOptions.removeClass('selected');
                $(this).addClass('selected');
                $(".calculator-dropdown ul").children('.init').html($(this).html());
                allOptions.toggle();
            });
        </script>
        <script>
            $(".calculator-dropdown2 ul").on("click", ".init2", function () {
                $(this).closest(".calculator-dropdown2 ul").children('li:not(.init2)').toggle();
            });

            var allOption = $(".calculator-dropdown2 ul").children('li:not(.init2)');
            $(".calculator-dropdown2 ul").on("click", "li:not(.init2)", function () {
                allOption.removeClass('selected');
                $(this).addClass('selected');
                $(".calculator-dropdown2 ul").children('.init2').html($(this).html());
                allOption.toggle();
            });
        </script>
        <script>
            $(document).ready(function () {
                $(".data-toggle").hide();
                $(".loader").fadeOut("slow");
                $(".see-toggle").click(function () {
                    $(".data-toggle").toggle();
                });
                $("#continue").click(function () {
                    window.location.href = './regularmethod';
                });
            });
        </script>
        <script>
            $(".previous2").click(function () {
                $("#step2").hide();
                $("#step1").show();
                $("html, body").animate({scrollTop: 0}, "fast");
            });
            $(".previous3").click(function () {
                $("#step3").hide();
                $("#step2").show();
                $("html, body").animate({scrollTop: 0}, "fast");
            });
            $(".previous4").click(function () {
                $("#step4").hide();
                $("#step3").show();
                $("html, body").animate({scrollTop: 0}, "fast");
            });
                $(".previous5").click(function () {
                $("#step5").hide();
                $("#step4").show();
                $("html, body").animate({scrollTop: 0}, "fast");
            });
            $(".next1").click(function () {
                $("#step2").show();
                $("#step1").hide();
                $("html, body").animate({scrollTop: 0}, "fast");
            });
            $(".next2").click(function () {
                $("#step3").show();
                $("#step2").hide();
                $("html, body").animate({scrollTop: 0}, "fast");
            });
            $(".next3").click(function () {
                $("#step4").show();
                $("#step3").hide();
                $("html, body").animate({scrollTop: 0}, "fast");
            });
             $(".next4").click(function () {
                $("#step5").show();
                $("#step4").hide();
                $("html, body").animate({scrollTop: 0}, "fast");
            });
        </script>
    </body>
</html>
