<style>
    @media (min-width:320px) and (max-width:576px){
        header {
            padding: 10px !important;
        }
        header a.logo {
            width: 70px;
        }
        .breadcrumb {
            margin-top: 0.1rem!important;
        }
        .content-w, .menu-side .content-w {
            padding-top: 3rem!important;
        }
        .bar1, .bar2, .bar3 {
            width: 25px!important;
            height: 4px!important;
            margin: 4px 0!important;
        }
        .change .bar1 {
            transform: rotate(-45deg) translate(-4px, 3px)!important;
        }
    }
</style>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!--<div class="onloader">
<div class="infinityChrome">
    <div></div>
    <div></div>
    <div></div>
</div>
    </div>-->
<div class="header-bar">
    <div class="container">
        <header>
            <div class="mobile-logo">
                <a class="logo" href="./home-new-advisor-dashboard">
                    <img class="main-logo" src="resources/images/logo.png">
                </a>
                <div class="hamburger">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                </div>
            </div>
            <nav class="nav-responsive">
                <div class="logged-user-w">
                    <div class="avatar-w">
                        <img alt="" src="resources/images/man.jpg">
                    </div>
                    <div class="logged-user-info-w">
                        <div class="logged-user-name">                 
                            John Smith
                        </div>
                        <div class="logged-user-role">
                            Private Wealth Member
                        </div>
                        <div class="logged-user-role coustmer-Id">
                            <!--MIN123-->
                        </div>
                    </div>
                    <div class="logged-user-menu">
                        <ul>
                            <li>
                                <a href="./home-beneficary-db"><i class="fa fa-user-o" aria-hidden="true"></i><span>Profile Details</span></a>
                            </li>
                            <li>
                                <a href="./login?logout"><i class="fa fa-sign-in" aria-hidden="true"></i><span>Logout</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <ul>                    
                    <sec:authorize access="hasAuthority('ADMIN')">
                        <li>
                            <a href="./admin-dashboard">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <!--<div class="os-icon os-icon-window-content"></div>-->
                                        <img class="menu-icon" src="./resources/images/wealth-icon.png">
                                    </div>
                                    <div class="icon-name">
                                        <span>Wealth Dashboard</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="./home-show-me-more-db">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <!--                                        <div class="os-icon os-icon-window-content"></div>-->
                                        <img class="menu-icon" src="./resources/images/private-icon.png">
                                    </div>
                                    <div class="icon-name">
                                        <span>Private Portfolio</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="./home-beneficiary-new-dashboard-db">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <!--<i class="fa fa-file-text-o" aria-hidden="true"></i>-->
                                        <img class="menu-icon" src="./resources/images/kivi-icon.png">
                                    </div>
                                    <div class="icon-name">
                                        <span>KiwiSaver</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="./home-investment">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <!--<i class="fa fa-file-text-o" aria-hidden="true"></i>-->
                                        <img class="menu-icon" src="./resources/images/serch-icon.png">
                                    </div>
                                    <div class="icon-name">
                                        <span>Investment Options</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="https://backoffice.invsta.io/invsta-ocr-demo/verifyid.html">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <!--<i class="fa fa-file-text-o" aria-hidden="true"></i>-->
                                        <img class="menu-icon" src="./resources/images/serch-icon.png">
                                    </div>
                                    <div class="icon-name">
                                        <span>Facial Recognition Result</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        
                        <li>
                            <a href="javascript:void(0)">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <!--<i class="fa fa-file-text-o" aria-hidden="true"></i>-->
                                        <img class="menu-icon" src="./resources/images/msg-icon.png">
                                    </div>
                                    <div class="icon-name">
                                        <span>Transactions</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <!--<i class="fa fa-file-text-o" aria-hidden="true"></i>-->
                                        <img class="menu-icon" src="./resources/images/serch-icon.png">
                                    </div>
                                    <div class="icon-name">
                                        <span>News & Research</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <!--<i class="fa fa-file-text-o" aria-hidden="true"></i>-->
                                        <img class="menu-icon" src="./resources/images/msg-icon.png">
                                    </div>
                                    <div class="icon-name">
                                        <span>Documents</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <!--<i class="fa fa-file-text-o" aria-hidden="true"></i>-->
                                        <img class="menu-icon" src="./resources/images/wealth-icon.png">
                                    </div>
                                    <div class="icon-name">
                                        <span>Contact & Support</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </sec:authorize>

                </ul>
            </nav>
        </header>
    </div>
</div>
<div id="menys" class="desktop-menu menu-side-w menu-activated-on-click">
    <div class="logo-w">
        <a class="logo" href="./home-new-advisor-dashboard">
            <img class="main-logo" src="resources/images/logo.png"> 
        </a>
    </div>
    <div class="menu-and-user">
        <div class="logged-user-w">
            <div class="logged-user-i">
                <div class="avatar-w">
                    <img alt="" src="resources/images/man.jpg">                                  
                </div>
                <div class="logged-user-info-w">
                    <div class="logged-user-name">
                        John Smith
                    </div>
                    <div class="logged-user-role">
                        Private Wealth Member
                    </div>
                    <div class="logged-user-role coustmer-Id">
                        <!--MIN123-->
                    </div>
                </div>
                <div class="logged-user-menu">
                    <div class="logged-user-avatar-info">
                        <div class="logged-user-info-w">
                            <div class="logged-user-name">
                                John Smith
                            </div>
                            <div class="logged-user-role">
                                Private Wealth Member
                            </div>
                        </div>
                    </div>
                    <div class="bg-icon">
                        <i class="os-icon os-icon-wallet-loaded"></i>
                    </div>
                    <ul>
                        <li>
                            <a href="./home-beneficary-db" ><i class="fa fa-user-o" aria-hidden="true"></i><span>Profile Details</span></a>
                        </li>
                        <li>
                            <a href="./login?logout"><i class="fa fa-sign-in" aria-hidden="true"></i><span>Logout</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="main-menu">            
            <sec:authorize access="hasAuthority('ADMIN')">
                <li class="sub-menu">
                    <a href="./admin-dashboard" id="menu-dash">
                        <div class="icon-w">
                            <!--<div class="os-icon os-icon-window-content"></div>-->
                            <img class="menu-icon" src="./resources/images/wealth-icon.png">
                        </div>
                        <span>Wealth Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./home-show-me-more-db" >
                        <div class="icon-w">
                            <!--<div class="os-icon os-icon-window-content"></div>-->
                            <img class="menu-icon" src="./resources/images/private-icon.png">
                        </div>
                        <span>Private Portfolio</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./home-beneficiary-new-dashboard-db" class="menu-notes">
                        <div class="icon-w">
                            <!--<i class="fa fa-file-text-o" aria-hidden="true"></i>-->
                            <img class="menu-icon" src="./resources/images/kivi-icon.png">
                        </div>
                        <span>KiwiSaver</span>
                    </a> 
                </li>
                <li class="sub-menu drop1">
                    <a href="./home-investment">
                        <div class="icon-w">
                            <!--<i class="fa fa-user-o"></i>-->    
                            <img class="menu-icon" src="./resources/images/serch-icon.png">
                        </div>
                        <span>Investment Options</span>
                    </a> 
                </li>
                <li class="sub-menu drop1">
                    <a href="https://backoffice.invsta.io/invsta-ocr-demo/verifyid.html">
                        <div class="icon-w">
                            <!--<i class="fa fa-user-o"></i>-->    
                            <img class="menu-icon" src="./resources/images/serch-icon.png">
                        </div>
                        <span>Facial Recognition Result</span>
                    </a> 
                </li>
                <li class="sub-menu drop1">
                    <a href="javaScript:Void(0)">
                        <div class="icon-w">
                            <!--<i class="fa fa-user-o"></i>--> 
                            <img class="menu-icon" src="./resources/images/msg-icon.png">
                        </div>
                        <span>Transactions</span>
                    </a> 
                </li>
                <li class="sub-menu drop1">
                    <a href="javaScript:Void(0)">
                        <div class="icon-w">
                            <!--<i class="fa fa-user-o"></i>-->  
                            <img class="menu-icon" src="./resources/images/serch-icon.png">
                        </div>
                        <span>News & Research</span>
                    </a> 
                </li>
                <li class="sub-menu drop1">
                    <a href="javaScript:Void(0)">
                        <div class="icon-w">
                            <!--<i class="fa fa-user-o"></i>-->  
                            <img class="menu-icon" src="./resources/images/msg-icon.png">
                        </div>
                        <span>Documents</span>
                    </a> 
                </li>
                <li class="sub-menu drop1">
                    <a href="javaScript:Void(0)">
                        <div class="icon-w">
                            <!--                            <i class="fa fa-user-o"></i>                           -->
                            <img class="menu-icon" src="./resources/images/wealth-icon.png">
                        </div>
                        <span>Contact & Support</span>
                    </a> 
                </li>
            </sec:authorize>
        </ul>
    </div>

</div>
<link href="./resources/css/main1.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
<script>
    $(document).ready(function () {
        $('.menu-dash').attr('href', './home-beneficiary-dashboard-db?id=${id}');
        $('.menu-dash-new').attr('href', './home-beneficiary-new-dashboard-db?id=${id}');
        $('.menu-txns').attr('href', './home-beneficary-transactions-db?id=${id}');
        $('.menu-txns-new').attr('href', './home-beneficary-new-transactions-db?id=${id}');
        $('.menu-profile').attr('href', './profile-db?id=${id}');
        $('.menu-payment').attr('href', './home-payment-enterpayment?id=${id}');

        $(".hamburger").click(function () {
            $(this).toggleClass("change");
            $("nav").toggle("1000");
        });
    });
</script>