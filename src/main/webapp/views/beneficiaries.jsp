<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>All Beneficiaries</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
      <link href="http://backoffice.invsta.io/mint/images/mint.png" rel="shortcut icon"/>
        <link href="http://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <link href="http://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>
        <link href="http://backoffice.invsta.io/pocv/resources/css/main.css?version=3.5.1" rel="stylesheet"/>
        <link href="http://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.css">
        <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.css">
        <style>
            
            .emp-profile{
                padding: 3%;
                margin-top: 3%;
                margin-bottom: 3%;
                border-radius: 0.5rem;
                background: #fff;
            }
            .profile-img{
                text-align: center;
            }
            .profile-img img{
                width: 70%;
                height: 100%;
            }
            .profile-img .file {
                position: relative;
                overflow: hidden;
                margin-top: -20%;
                width: 70%;
                border: none;
                border-radius: 0;
                font-size: 15px;
                background: #212529b8;
            }
            .profile-img .file input {
                position: absolute;
                opacity: 0;
                right: 0;
                top: 0;
            }
            .profile-head h5{
                color: #333;
            }
            .profile-head h6{
                color: #0062cc;
            }
            .profile-edit-btn{
                border: none;
                border-radius: 1.5rem;
                width: 70%;
                padding: 2%;
                font-weight: 600;
                color: #6c757d;
                cursor: pointer;
            }
            .proile-rating{
                font-size: 12px;
                color: #818182;
                margin-top: 5%;
            }
            .proile-rating span{
                color: #495057;
                font-size: 15px;
                font-weight: 600;
            }
            .profile-head .nav-tabs{
                margin-bottom:5%;
            }
            .profile-head .nav-tabs .nav-link{
                font-weight:600;
                border: none;
            }
            .profile-head .nav-tabs .nav-link.active{
                border: none;
                border-bottom:2px solid #0062cc;
            }
            .profile-work{
                padding: 14%;
                margin-top: -15%;
            }
            .profile-work p{
                font-size: 12px;
                color: #818182;
                font-weight: 600;
                margin-top: 10%;
            }
            .profile-work a{
                text-decoration: none;
                color: #495057;
                font-weight: 600;
                font-size: 14px;
            }
            .profile-work ul{
                list-style: none;
            }
            .profile-tab label{
                font-weight: 600;
            }
            .profile-tab p{
                font-weight: 600;
                color: #0062cc;
            }
            .el-chart-w {
                overflow: visible!important;
            }
            .portfolio_overlay {
                height: 75%;
                display: block;
            }
            .ilumony-box:hover .portfolio_overlay {
                display: block;
            }
            .funds-deatil {
                background: #fff;
                padding: 1rem;
                border-radius: 4px;
                margin-bottom: 10px;
            }
            .show_more_btn_new {
                position: absolute;
                top: 0;
                right: 0px;
                margin: 11px 27px;
            }
            .funds-deatil h4 {
                max-width: 80%;
                margin-bottom: 15px;
            }
            .collapse {
                display: none;
            }
            .collapse.show {
                display: block;
            }
body:before {
background: #f2f2f2!important;
}

        </style>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                           <a href="./home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Investment</span>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="container emp-profile">
                                        <table id="benificary-table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                               data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <!--<th data-checkbox="true"></th>-->
                                                    <!--<th data-field="title" data-formatter=""><b>Title</b></th>-->
                                                    <th data-field="Id" ><span >Id</span></th>
                                                    <th data-field="IrdNumber" ><span >Ird Number</span></th>
                                                    <th data-field="Name" ><span>Name</span></th>
                                                    <th data-field="AMLEntityType" ><span>AML Entity Type</span></th>
                                                    <th data-field="Status" ><span>Status</span></th>
                                                    <th data-formatter="viewButtonFormatter">Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.js"></script> 
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="http://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>






<!--        <script>
            $('#viewmore1').click(function () {
                $('.hidediv1').show();
                $('#viewmore1').hide();
            });
            $('#viewmore2').click(function () {
                $('.hidediv2').show();
                $('#viewmore2').hide();
            });
            $('#viewmore3').click(function () {
                $('.hidediv3').show();
                $('#viewmore3').hide();
            });
            $('.clickinput').on("click", function () {
                var recemt = $(this).closest('.funds-deatil');
                recemt.find(".offer-input").toggle();
            });
            $(document).ready(function () {

                $('.hidediv1').hide();
                $('.hidediv2').hide();
                $('.hidediv3').hide();

            });
        </script>-->
        <script>
            function viewButtonFormatter(value, row, index) {
                return '<a href="./beneficiary?id=' + row.Id + '" class="btn  a-btn-slide-text "><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>View</strong></span></a> <a href="./home-beneficiary-dashboard?id=' + row.Id + '" class="btn  a-btn-slide-text "><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Dashboard</strong></span></a>';
            }
        </script>
        <script>
            $(document).ready(function () {
                $.ajax({
                    type: 'GET',
                    url: './rest/3rd/party/api/beneficiaries',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
//                        alert(data);
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        $('#benificary-table').bootstrapTable('load', obj);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    }
                });
            });
        </script>

    </body>
</html>