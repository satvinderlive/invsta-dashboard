<%-- 
    Document   : dashboard
    Created on : Jul 25, 2019, 2:38:52 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Beneficiary Dashboard</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="http://backoffice.invsta.io/mint/images/mint.png" rel="shortcut icon"/>
        <link href="http://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <link href="http://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>
        <!--<link href="http://backoffice.invsta.io/pocv/resources/css/main.css?version=3.5.1" rel="stylesheet"/>-->
        <link href="http://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link href="./resources/css/custom.css" rel="stylesheet"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">       
        <style>
            .el-chart-w {
                overflow: visible!important;
            }
            .el-tablo:hover {
                box-shadow: none;
                transform: none;
            }
            .founder-icon-color {
                display: flex;
                margin-left: 10px;
            }
            .value.sumWalletBalance {
                float: right;
                display: flex;
                align-items: center;
            }

            .element-box.ele-overlay:before {
                background: #93dbd0!important;
                opacity: 0.6!important;
            }


            .emp-profile{
                padding: 3%;
                margin-top: 3%;
                margin-bottom: 3%;
                border-radius: 0.5rem;
                background: #fff;
            }
            .profile-img{
                text-align: center;
            }
            .profile-img img{
                width: 70%;
                height: 100%;
            }
            .profile-img .file {
                position: relative;
                overflow: hidden;
                margin-top: -20%;
                width: 70%;
                border: none;
                border-radius: 0;
                font-size: 15px;
                background: #212529b8;
            }
            .profile-img .file input {
                position: absolute;
                opacity: 0;
                right: 0;
                top: 0;
            }
            .profile-head h5{
                color: #333;
            }
            .profile-head h6{
                color: #0062cc;
            }
            .profile-edit-btn{
                border: none;
                border-radius: 1.5rem;
                width: 70%;
                padding: 2%;
                font-weight: 600;
                color: #6c757d;
                cursor: pointer;
            }
            /*            .proile-rating{
                            font-size: 12px;
                            color: #818182;
                            margin-top: 5%;
                        }
                        .proile-rating span{
                            color: #495057;
                            font-size: 15px;
                            font-weight: 600;
                        }*/
            .profile-head .nav-tabs{
                margin-bottom:5%;
            }
            .profile-head .nav-tabs .nav-link{
                font-weight:600;
                border: none;
            }
            .profile-head .nav-tabs .nav-link.active{
                border: none;
                border-bottom:2px solid #0062cc;
            }
            .profile-work{
                padding: 14%;
                margin-top: -15%;
            }
            .profile-work p{
                font-size: 12px;
                color: #818182;
                font-weight: 600;
                margin-top: 10%;
            }
            .profile-work a{
                text-decoration: none;
                color: #495057;
                font-weight: 600;
                font-size: 14px;
            }
            .profile-work ul{
                list-style: none;
            }
            .profile-tab label{
                font-weight: 600;
            }
            .profile-tab p {
                font-weight: 600;
                color: #54555a;
            }
            .el-chart-w {
                overflow: visible!important;
            }
            .portfolio_overlay {
                height: 75%;
                display: block;
            }
            .ilumony-box:hover .portfolio_overlay {
                display: block;
            }
            .funds-deatil {
                background: #fff;
                padding: 1rem;
                border-radius: 4px;
                margin-bottom: 10px;
            }
            .show_more_btn_new {
                position: absolute;
                top: 0;
                right: 0px;
                margin: 11px 27px;
            }
            .funds-deatil h4 {
                max-width: 80%;
                margin-bottom: 15px;
            }
            .collapse {
                display: none;
            }
            .collapse.show {
                display: block;
            }
            .profile-img img {
                max-width: 102px;
                width: 70%;
                height: 100%;
                /* border-radius: 34%; */
            }
            .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {

                border: none;
            }
            .nav.smaller.nav-tabs .nav-link {
                padding: 0.7em .6em;
            }

        </style>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"></jsp:include>
                    <div class="content-w">
                        <!--------------------
                        START - Breadcrumbs
                -------------------->
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item ">
                                <a href="./home">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span>Welcome</span>
                            </li>
                            <li class="breadcrumb-item">
                                <span></span>
                            </li>
                        </ul>
                        <!--------------------
                        END - Breadcrumbs
                -------------------->
                        <div class="content-panel-toggler">
                            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                        </div>
                        <div class="content-i">
                            <div class="content-box">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="element-wrapper">
                                            <p>Hi <span class="logged-user-name">Claudia</span>, welcome to Mint. Here’s an overview of your investments with us:</p><br>
                                            <h6 class="element-header">
                                                Investment Overview
                                            </h6>
                                            <div class="element-box">
                                                <div class="os-tabs-w">
                                                    <div class="os-tabs-controls">
                                                        <ul class="nav nav-tabs smaller">

                                                            <li class="nav-item">
                                                                <a class="nav-link active" data-toggle="tab" href="#tab_all_days">Historical Performance</a>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab_overview">
                                                            <div class="label" style="display:inline-block">
                                                                Amount Invested <img src="http://backoffice.invsta.io/pocv/resources/img/info-icon.png" data-toggle="tooltip" title="The Live Balance chart provides an indication of the real-time balance of your  portfolio, and is reliant on external data sources which may be different to what is reported on exchanges. The actual daily closing value and withdrawal value of your portfolio will be different. This balance does not include any cash that is available in your Invsta cash account." data-placement="right" style="vertical-align:text-bottom">
                                                            </div>
                                                            <div class="label" style="float:right;">
                                                                CURRENT Value

                                                                <img src="http://backoffice.invsta.io/pocv/resources/img/info-icon.png" style="vertical-align:text-bottom;   margin-left: 5px;" data-toggle="tooltip" title="(previous day close, incl cash in wallet) This represents the actual value of your  portfolio investments based on the previous day closing value, and includes any cash you may have available in your Invsta cash account.">
                                                            </div>
                                                            <div class="" style="padding:0 0 10px;width: 100%;float: left;">
                                                                <div class="value currentBalance">$90,688 

                                                                </div>
                                                                <div class="value sumWalletBalance" style="float:right;">
                                                                    <div class="sumWalletBal" >  $134,160 </div>
                                                                    <span class="founder-icon-color"><div class="percentage" > 16.23% </div> <i class="fa fa-arrow-up" aria-hidden="true"></i></span>
                                                                </div>

                                                            </div>

                                                        </div>

                                                        <div class="tab-pane active" id="tab_all_days">

                                                            <div id="stockbalance" style="min-width: 100%; height: 400px; margin: 0 auto;"></div>
                                                        </div>
                                                        <div class="tab-pane" id="investment-composition">
                                                            <div class="label">
                                                                BALANCE   <img src="./resources/img/info-icon.png" style="vertical-align:text-bottom">
                                                            </div>
                                                            <div class="el-tablo">
                                                                <div class="value prev-actual-balance">$ 50,000.00

                                                                </div>
                                                            </div>
                                                            <div id="composition-chart" style="min-width: 100%; height: 400px; margin: 0 auto;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="element-wrapper">
                                            <div class="element-box">
                                                <div class="os-tabs-w">
                                                    <div class="os-tabs-controls">


                                                        <ul class="nav nav-tabs justify-content-center" role="tablist">

                                                            <li class="nav-item">
                                                                <a class="nav-link active" id="tabhome224-tab" data-toggle="tab" href="#tabhome224" role="tab" aria-controls="tabhome224" aria-selected="false">
                                                                    Investment Breakdown 
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-content">

                                                        <div class="tab-pane active" id="tab_latest_week-cons-invs-pieChart" style="" aria-expanded="false">
                                                            <div class=" el-tablo">

                                                                <div class="el-chart-w" id="investmentBreakdown" style="height:434px"></div>
                                                                <div id="legend-2"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="element-wrapper">
                                            <div class="element-box">
                                                <ul class="nav nav-tabs justify-content-center" role="tablist">

                                                    <li class="nav-item">
                                                        <a class="nav-link active" id="tabhome224-tab" data-toggle="tab" href="#tabhome224" role="tab" aria-controls="tabhome224" aria-selected="false">
                                                            Performance    
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div class="os-tabs-w">
                                                    <div class="os-tabs-controls">
                                                        <ul class="nav nav-tabs smaller">
                                                            <li class="nav-item">
                                                                <a class="nav-link active" data-toggle="tab" href="#tab_overview-cons-invs-pieChart" aria-expanded="true">Last Year
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="tab" href="#tab_overview-cons-invs-pieChart2" aria-expanded="true">2 Years ago
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="tab" href="#tab_overview-cons-invs-pieChart3" aria-expanded="true">3 Years ago
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="tab" href="#tab_overview-cons-invs-pieChart4" aria-expanded="true">Average Return
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab_overview-cons-invs-pieChart" aria-expanded="true" style="">
                                                            <div class=" el-tablo">

                                                                <div id="performanceBarChart" style="height: 383px;"></div>
                                                                <div id="legend-1"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_overview-cons-invs-pieChart2" aria-expanded="true" style="">
                                                            <div class=" el-tablo">

                                                                <div id="portfolio_pie21" style="height: 383px;"></div>
                                                                <div id="legend-1"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_overview-cons-invs-pieChart3" aria-expanded="true" style="">
                                                            <div class=" el-tablo">

                                                                <div id="portfolio_pie31" style="height: 383px;"></div>
                                                                <div id="legend-1"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_overview-cons-invs-pieChart4" aria-expanded="true" style="">
                                                            <div class=" el-tablo">

                                                                <div id="portfolio_pie41" style="height: 383px;"></div>
                                                                <div id="legend-1"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="element-wrapper">
                                            <div class="element-box">
                                                <div class="os-tabs-w">
                                                    <div class="os-tabs-controls">

                                                        <ul class="nav nav-tabs justify-content-center" role="tablist">

                                                            <li class="nav-item">
                                                                <a class="nav-link active" id="tabhome224-tab" data-toggle="tab" href="#tabhome224" role="tab" aria-controls="tabhome224" aria-selected="false">
                                                                    Asset Allocation  
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab_overview-cons-invs-pieChart" aria-expanded="true" style="">
                                                            <div class=" el-tablo">
                                                                <div id="assetAllocation" style="height:434px""></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <!--                                <div class="das-board"> 
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="element-wrapper">
                                                                                <h6 class="element-header">Investments </h6>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="element-box ele-overlay" style="background:url(http://backoffice.invsta.io/pocv/resources/img/) center center no-repeat ;background-size:cover">
                                                                                <h6 style="margin-bottom:20px; display: inline-block; margin-right: 5px;">Australasian Equity Fund</h6>
                                                                                <div class="row">
                                                                                    <div class="col-md-3">
                                                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                                                            <div class="label">
                                                                                                Amount Invested
                                                                                            </div>
                                                                                            <div class="value" id="initialInvSqr">
                                                                                                NZ$ 10,000 
                                                                                            </div>
                                
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                                                            <div class="label ">
                                                                                                Current Balance
                                                                                            </div>
                                                                                            <div class="value " id="currentInvSqr">
                                                                                                NZ$ 13,365
                                                                                            </div>
                                
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                                                            <div class="label">
                                                                                                performance
                                                                                            </div>
                                                                                            <div class="arrow-text">
                                                                                                <div class="value per- per-color- percenfund">
                                                                                                    33.66 %
                                
                                
                                                                                                </div>
                                
                                                                                                <i class=" os-icon os-icon-arrow-up6"></i>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                                                            <div id="lg-latestweek1" style="height:70px; width:100%"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="show_more_btn">
                                                                                    <a class="btn btn-primary btn-style showmemore" href="javascript:void(0)">Show Me More</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="element-box ele-overlay" style="background:url(http://backoffice.invsta.io/pocv/resources/img/) center center no-repeat ;background-size:cover">
                                                                                <h6 style="margin-bottom:20px; display: inline-block; margin-right: 5px;">Australasian Property Fund</h6>
                                                                                <div class="row">
                                                                                    <div class="col-md-3">
                                                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                                                            <div class="label">
                                                                                                Amount Invested
                                                                                            </div>
                                                                                            <div class="value" id="initialInvSqr1">
                                                                                                NZ$ 10,000
                                                                                            </div>
                                
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                                                            <div class="label ">
                                                                                                Current Balance
                                                                                            </div>
                                                                                            <div class="value " id="currentInvSqr1">
                                                                                                NZ$ 9,758
                                
                                                                                            </div>
                                
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                                                            <div class="label">
                                                                                                performance
                                                                                            </div>
                                                                                            <div class="arrow-text">
                                                                                                <div class="value per- per-color- percenfund1">
                                                                                                    7.58 %
                                
                                
                                                                                                </div>
                                
                                                                                                <i class=" os-icon os-icon-arrow-up6"></i>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                                                            <div id="lg-latestweek2" style="height:70px; width:100%"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="show_more_btn">
                                                                                    <a class="btn btn-primary btn-style showmemore1" href="javascript:void(0)">Show Me More</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>                                        
                                                                    </div>
                                                                </div>-->
                                <div class="das-board" id="investments">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="element-wrapper">
                                                <h6 class="element-header">Investments </h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="portfolio" style="display: none;">
                                        <div class="col-md-12">
                                            <div class="element-box ele-overlay" style="background:url(http://backoffice.invsta.io/pocv/resources/img/) center center no-repeat ;background-size:cover">
                                                <h6 style="margin-bottom:20px; display: inline-block; margin-right: 5px;" class="pcName">Australasian Equity Fund</h6>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                            <div class="label">
                                                                Contribution
                                                            </div>
                                                            <div class="value contribution">
                                                                NZ$ 10,000 
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                            <div class="label ">
                                                                Current Balance
                                                            </div>
                                                            <div class="value marketValue" id="currentInvSqr">
                                                                NZ$ 13,365
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                            <div class="label">
                                                                performance
                                                            </div>
                                                            <div class="arrow-text">
                                                                <div class="value per- per-color- percentage">
                                                                    33.66 %


                                                                </div>

                                                                <i class=" os-icon os-icon-arrow-up6"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                            <div id="lg-latestweek1" style="height:70px; width:100%"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="show_more_btn">
                                                    <a class="btn btn-primary btn-style showmemore" href="javascript:void(0)">Show Me More</a>
                                                </div>
                                            </div>
                                        </div>                                                                             
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="display-type"></div>
            </div>
            <script src="http://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" defer></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" defer></script>
            <!-- 		<script src="resources/bower_components/moment/moment.js" defer></script>
                            
                            <script src="resources/bower_components/ckeditor/ckeditor.js" defer></script>
                            <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" defer></script>
                            <script src="resources/bower_components/dropzone/dist/dropzone.js" defer></script> -->

            <script src="http://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" defer></script>
            <!-- 		<script src="resources/bower_components/tether/dist/js/tether.min.js" defer></script>
                           
                            <script src="resources/bower_components/bootstrap/js/dist/alert.js" defer></script>
                            <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" defer></script>
                            <script src="resources/bower_components/bootstrap/js/dist/button.js" defer></script>
                            <script src="resources/bower_components/bootstrap/js/dist/carousel.js" defer></script>
                            <script src="resources/bower_components/bootstrap/js/dist/collapse.js" defer></script>
                            <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" defer></script>
                            <script src="resources/bower_components/bootstrap/js/dist/modal.js" defer></script>
                            
                            <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" defer></script>
                            <script src="resources/bower_components/bootstrap/js/dist/popover.js" defer></script> -->
            <script src="http://backoffice.invsta.io/ui-fund/resources/bower_components/bootstrap/js/dist/util.js" defer></script>
            <script src="http://backoffice.invsta.io/ui-fund/resources/bower_components/bootstrap/js/dist/tab.js" defer></script>
            <script src="http://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" defer></script>
            <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
            <script src="https://code.highcharts.com/highcharts.js" ></script> 
            <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
            <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
            <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
            <script src="https://code.highcharts.com/modules/cylinder.js"></script>
            <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
            <script>
                $(document).ready(function () {
                    console.log('ready');
                    $.ajax({
                        type: 'GET',
                        url: './rest/3rd/party/api/beneficiary-dashboard-${id}',
//                        url: './rest/3rd/party/api/beneficiary-dashboard',
                        headers: {"Content-Type": 'application/json'},
                        success: function (data, textStatus, jqXHR) {
                            console.log('success');
                            dashboard(data);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log('error');
                        }
                    });
                });
                function dashboard(data) {
                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                    $('.logged-user-name').html(obj.investmentSummary.PrimaryBeneficiary.FirstName);
                    console.log(JSON.stringify(obj));
                    var amount = 0;
                    var contributionAmount = 0;
                    var arr = [];
                    var arr1 = [];
                    $.each(obj.transactionSummary, function (idx, val) {
                        if (val.Category === "Contribution") {
                            amount = amount + val.Amount;
                        }
                        contributionAmount = contributionAmount + val.Amount;
                    });
                    var inc = contributionAmount - amount;
                    var percent = inc * 100 / amount;
//                    $('.percentage').text(percent.toFixed(2) + '%');
                    var txnMap = new Map();
                    var portfolioCodes = new Array();
                    $.each(obj.transactionSummary, function (idx, val) {
                        var txnArray = txnMap[val.PortfolioName];
                        if (typeof txnArray === 'undefined') {
                            portfolioCodes.push(val.PortfolioName);
                            txnArray = new Array();
                        }
                        txnArray.push(val);
                        txnMap[val.PortfolioName] = txnArray;
                    });
                    var portfolio = document.getElementById("portfolio");
                    var investments = document.getElementById("investments");
                    for (var i = 0; i < portfolioCodes.length; i++) {
                        var pc = portfolioCodes[i];
                        var clone = portfolio.cloneNode(true);
                        var h6 = clone.getElementsByClassName("pcName")[0];
                        h6.innerHTML = pc;
                        clone.style.display = 'block';
                        investments.appendChild(clone);
                        var pcArray = txnMap[pc];
                        var contri = 0, total = 0;
                        for (var j = 0; j < pcArray.length; j++) {
                            var pcObj = pcArray[j];
                            if (pcObj.Category === "Contribution") {
                                contri += pcObj.Amount;
                            }
                            total += pcObj.Amount;
                        }
                        var inc = total - contri;
                        var percent100 = '16.22%';
                        if (i === 0) {
                            percent100 = '16.22%';
                        } else {
                            percent100 = '16.31%';
                        }

                        var cont = clone.getElementsByClassName("contribution")[0];
                        cont.innerHTML = '$' + contri.toFixed(0).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                        var marVal = clone.getElementsByClassName("marketValue")[0];
                        marVal.innerHTML = '$' + total.toFixed(0).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                        var percentage = clone.getElementsByClassName("percentage")[0];
                        percentage.innerHTML = percent100;
                        arr.push([pc, total]);
                        var taxnArr = txnMap[pc];
                        var id1 = './show-me-more-' + obj.investmentSummary.InvestmentCode + '?pc=' + taxnArr[i].PortfolioCode;
                        clone.getElementsByClassName("showmemore")[0].setAttribute('href', id1);
                    }

                    pieChart('investmentBreakdown', arr);
                    $.each(obj.fmcaInvestmentMix, function (idx, val) {
                        arr1.push([val.FMCAAssetClass, val.SectorValueBase]);
                    });
                    pieChart('assetAllocation', arr1);
                    var perSeries = new Array();
                    var perSeries2 = new Array();
                    var fundMap = new Map();
                    var keys = new Array();
                    $.each(obj.investmentPerformance, function (idx, fund) {
                        var PeriodEndDateArray = fundMap[fund.PeriodEndDate];
                        if (typeof PeriodEndDateArray === 'undefined') {
                            PeriodEndDateArray = new Array();
                            keys.push(fund.PeriodEndDate);
                        }
                        PeriodEndDateArray.push(fund);
                        fundMap[fund.PeriodEndDate] = PeriodEndDateArray;
                    });
                    for (var key of keys) {
                        var value = fundMap[key];
                        var array = [];
                        $.each(value, function (idx, fund) {
                            array.push({
                                name: fund.Portfolio,
                                y: fund.XIRRReturnRate
                            });
                        });
                        perSeries.push({
                            showInLegend: true,
                            name: key,
                            data: array
                        });
                        perSeries2.push({
                            showInLegend: false,
                            name: key,
                            data: [array[0].y]
                        });
                    }
                    barChart(perSeries2);
                }
        </script>
        <script>
            Highcharts.chart('stockbalance', {
                chart: {
                    type: 'spline',
                },
                title: {
                    text: ''
                },
                subtitle: {

                },
                xAxis: {
                    categories: [
                        '2017 Jan', '2017 Feb', '2017 Mar', '2017 Apr', '2017 May', '2017 Jun', '2017 Jul', '2017 Aug', '2017 Sep', '2017 Oct', '2017 Nov', '2017 Dec',
                        '2018 Jan', '2018 Feb', '2018 Mar', '2018 Apr', '2018 May', '2018 Jun', '2018 Jul', '2018 Aug', '2018 Sep', '2018 Oct', '2018 Nov', '2018 Dec',
                        '2019 Jan', '2019 Feb', '2019 Mar', '2019 Apr', '2019 May', '2019 Jun', '2019 Jul'
                    ],
                    tickInterval: 1,
                    labels: {
                        style: {
                            color: '#333',
                            fontSize: '12px',
                            textTransform: 'uppercase'
                        },
                        y: 20,
                        x: 10
                    },
                    lineColor: '#dadada'
                },
                yAxis: {
                    title: {
                        text: ''
                    }
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'center',
                    verticalAlign: 'bottom',
                    enabled: false

                },
                plotOptions: {
                    series: {
                        color: '#93dbd0',
                        shadow: true,
                        lineWidth: 3,
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    pointFormatter: function () {
                        var isNegative = this.y < 0 ? '-' : '';
                        return  isNegative + '$' + Math.abs(this.y.toFixed(0));
                    }
                },
                series: [{
                        name: '',
                        data: [90688,
                            90688 + 970.00,
                            90688 + 888.80,
                            90688 + 1009.60,
                            90688 + 1202.45,
                            90688 + 1460.47,
                            90688 + 1800.66,
                            90688 + 2092.99,
                            90688 + 2207.57,
                            90688 + 2654.50,
                            90688 + 3037.57,
                            90688 + 4883.94,
                            90688 + 5762.66,
                            90688 + 6864.18,
                            90688 + 7903.55,
                            90688 + 9018.43,
                            90688 + 10095.45,
                            90688 + 13109.44,
                            90688 + 19630.21,
                            90688 + 22010.11,
                            113081.42,
                            114035.62,
                            115055.92,
                            117057.41,
                            120343.07,
                            123068.20,
                            125022.82,
                            127051.06,
                            128001.64,
                            130004.75,
                            134644.35]
                    }],
                responsive: {
                    rules: [{
                            condition: {
                                maxWidth: 500
                            },

                        }]
                }

            });
        </script>
        <script>
            $.getJSON('https://www.highcharts.com/samples/data/aapl-c.json', function (data) {

// Create the chart
                Highcharts.stockChart('stockbalance1', {

                    rangeSelector: {
                        selected: 1
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false
                    },
                    navigator: {
                        enabled: false
                    },
                    navigation: {
                        buttonOptions: {
                            enabled: false
                        }
                    },
                    series: [{
                            name: 'Fund',
                            data: data,
                            tooltip: {
                                valueDecimals: 2
                            }
                        }]
                });
            });
        </script>

        <script>
//            Highcharts.setOptions({
//                colors: ['#91d9cf', '#54565a', '#879fc2', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
//            });
//            Highcharts.chart('assetAllocation', {
//                chart: {
//                    plotBackgroundColor: null,
//                    plotBorderWidth: null,
//                    plotShadow: false,
//                    type: 'pie'
//                },
//                credits: {
//                    enabled: false,
//                },
//                exporting: {
//                    enabled: false,
//                },
//                title: {
//                    text: ''
//                },
//                tooltip: {
//                    pointFormat: '<b>{point.percentage:.1f}%</b>'
//                },
//                plotOptions: {
//                    pie: {
//                        innerSize: 40,
//                        depth: 45,
//                        allowPointSelect: true,
//                        cursor: 'pointer',
//                        dataLabels: {
//                            distance: 2,
//                            connectorWidth: 0,
//                            enabled: true,
//                            format: '{point.percentage:.1f} %'
//                        },
//                        showInLegend: true
//                    }
//                },
//                series: [{
//                        name: 'Brands',
//                        colorByPoint: true,
//                        data: [{
//                                name: '	Australasian equities',
//                                y: 66,
//                            }, {
//                                name: '	Australian Equities',
//                                y: 1
//                            }, {
//                                name: '	Cash and cash equivalents',
//                                y: 9
//                            }, {
//                                name: 'Listed property',
//                                y: 24
//                            }, {
//                                name: 'New Zealand Fixed Interest',
//                                y: 0
//                            }]
//                    }]
//            });
        </script>
        <script>
            function pieChart(idc, arr) {
                Highcharts.setOptions({
                    colors: ['#91d9cf', '#54565a', '#879fc2', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
                });
                Highcharts.chart(idc, {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 0
                        }
                    },
                    credits: {
                        enabled: false,
                    },
                    exporting: {
                        enabled: false,
                    },
                    legend: {
                        enabled: true,
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name} <br>{point.percentage:.1f} %<br>total: $ {point.total:.2f}',
                    },
                    plotOptions: {
                        pie: {
                            innerSize: 100,
                            depth: 45,
                            dataLabels: {
                                enabled: false,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %<br>Value: {point.y}',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            },
                        },
                    },
                    series: [{
                            showInLegend: true,
                            name: '',
                            data: arr
                        }]
                });
            }
        </script>
        <script>
            Highcharts.setOptions({
                colors: ['#606065', '#c64b38', '#2d2a26']
            });
            Highcharts.chart('performanceBarChart', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: [''],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    max: 30,
                    tickInterval: 5,
                    labels: {
                        format: '{value}%'
                    },
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                plotOptions: {
                    column: {
                        pointPadding: 0,
                        borderWidth: 0
                    }
                },
                series: [{
                        name: 'Australasian Equity Fund',
                        data: [14.83]

                    }, {
                        name: 'Australasian Property Fund',
                        data: [25.73]

                    }, {
                        name: 'Summary',
                        data: [17.63]

                    }]
            });
        </script>

        <script>
            Highcharts.setOptions({
                colors: ['#606065', '#c64b38', '#2d2a26']
            });
            Highcharts.chart('portfolio_pie21', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: [''],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    max: 30,
                    tickInterval: 5,
                    labels: {
                        format: '{value}%'
                    },
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                plotOptions: {
                    column: {
                        pointPadding: 0,
                        borderWidth: 0
                    }
                },
                series: [{
                        name: 'Australasian Equity Fund',
                        data: [18.32]

                    }, {
                        name: 'Australasian Property Fund',
                        data: [7.27]

                    }, {
                        name: 'Summary',
                        data: [15.26]

                    }]
            });
        </script>
        <script>
            Highcharts.setOptions({
                colors: ['#606065', '#c64b38', '#2d2a26']
            });
            Highcharts.chart('portfolio_pie31', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: [''],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    max: 30,
                    tickInterval: 5,
                    labels: {
                        format: '{value}%'
                    },
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                plotOptions: {
                    column: {
                        pointPadding: 0,
                        borderWidth: 0
                    }
                },
                series: [{
                        name: 'Australasian Equity Fund',
                        data: [9.46]

                    }, {
                        name: 'Australasian Property Fund',
                        data: [0.00]

                    }, {
                        name: 'Summary',
                        data: [10.12]

                    }]
            });
        </script>
        <script>
            Highcharts.setOptions({
                colors: ['#606065', '#c64b38', '#2d2a26']
            });
            Highcharts.chart('portfolio_pie41', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: [''],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    max: 30,
                    tickInterval: 5,
                    labels: {
                        format: '{value}%'
                    },
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                plotOptions: {
                    column: {
                        pointPadding: 0,
                        borderWidth: 0
                    }
                },
                series: [{
                        name: 'Australasian Equity Fund',
                        data: [14.15]

                    }, {
                        name: 'Australasian Property Fund',
                        data: [16.13]

                    }, {
                        name: 'Summary',
                        data: [14.49]

                    }]
            });
        </script>
        <script>
            Highcharts.setOptions({
                colors: ['#65b9ac']
            });
            Highcharts.chart('lg-latestweek1', {
                title: {
                    text: ''
                },
                legend: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                xAxis: {
                    categories: [''],
                    title: {
                        text: null
                    },
                    labels: {enabled: false, }

                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {enabled: false, }, title: {
                        text: ''
                    },
                },
                credits: {
                    enabled: false
                },
                series: [{
                        name: 'Investment Balance',
                        data: [10000, 9000, 9999, 8000, 9600, 13365.55]
                    }],
            });
        </script>
        <script>
            Highcharts.chart('lg-latestweek2', {
                title: {
                    text: ''
                },
                legend: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                xAxis: {
                    categories: [''],
                    title: {
                        text: null
                    },
                    labels: {enabled: false, }

                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {enabled: false, }, title: {
                        text: ''
                    },
                },
                credits: {
                    enabled: false
                },
                series: [{
                        name: 'Investment Balance',
                        data: [10000, 9700, 8700, 9280, 8900, 9758.43]
                    }],
            });
        </script>
        <script>
            Highcharts.chart('lg-latestweek3', {
                title: {
                    text: ''
                },
                legend: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                xAxis: {
                    categories: [''],
                    title: {
                        text: null
                    },
                    labels: {enabled: false, }

                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {enabled: false, }, title: {
                        text: ''
                    },
                },
                credits: {
                    enabled: false
                },
                series: [{
                        name: 'Investment Balance',
                        data: [10000, 9700, 8500, 9280, 9100, 10898.47]
                    }],
            });
        </script>
        <script>
            Highcharts.chart('lg-latestweek4', {
                title: {
                    text: ''
                },
                legend: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                xAxis: {
                    categories: [''],
                    title: {
                        text: null
                    },
                    labels: {enabled: false, }

                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {enabled: false, }, title: {
                        text: ''
                    },
                },
                credits: {
                    enabled: false
                },
                series: [{
                        name: 'Investment Balance',
                        data: [10000, 8700, 9000, 9280, 8900, 10258.28]
                    }],
            });
        </script>
        <script>
            Highcharts.chart('lg-latestweek5', {
                title: {
                    text: ''
                },
                legend: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                xAxis: {
                    categories: [''],
                    title: {
                        text: null
                    },
                    labels: {enabled: false, }

                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {enabled: false, }, title: {
                        text: ''
                    },
                },
                credits: {
                    enabled: false
                },
                series: [{
                        name: 'Investment Balance',
                        data: [10000, 9700, 8700, 9280, 8900, 11540.38]
                    }],
            });
        </script>
        <script>
            Highcharts.chart('lg-latestweek6', {
                title: {
                    text: ''
                },
                legend: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                xAxis: {
                    categories: [''],
                    title: {
                        text: null
                    },
                    labels: {enabled: false, }

                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {enabled: false, }, title: {
                        text: ''
                    },
                },
                credits: {
                    enabled: false
                },
                series: [{
                        name: 'Investment Balance',
                        data: [10000, 9700, 8700, 9280, 8900, 11629.15]
                    }],
            });
        </script>
        <script>
            Highcharts.chart('lg-latestweek7', {
                title: {
                    text: ''
                },
                legend: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                xAxis: {
                    categories: [''],
                    title: {
                        text: null
                    },
                    labels: {enabled: false, }

                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {enabled: false, }, title: {
                        text: ''
                    },
                },
                credits: {
                    enabled: false
                },
                series: [{
                        name: 'Investment Balance',
                        data: [10000, 9700, 8596, 9280, 10890, 12545.00]
                    }],
            });
        </script>
        <script>
            Highcharts.chart('composition-chart', {
                chart: {
                    type: 'spline',
                },
                credits: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                yAxis: {
                    title: {
                        text: 'Performance'
                    }
                },
                series: [{
                        name: 'Magellan Global Equities Fund (MGE)',
                        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
                    }, {
                        name: 'Magellan Global Equities Fund (Currency Hedged) (MHG)',
                        data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
                    }, {
                        name: 'Magellan Global Trust (MGG)',
                        data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
                    }, {
                        name: 'Magellan Global Fund',
                        data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
                    }, {
                        name: 'Magellan Global Fund (Hedged)',
                        data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]

                    }]
            });
        </script>
    </body>
</html>