<%-- 
    Document   : temp
    Created on : 10/09/2019, 5:39:47 PM
    Author     : TOSHIBA R830
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
      <div class="modal fade" id="add-funds-to-portfolio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" ria-hiddena="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"> Mint Diversified Income Fund </h5>
   
                                </div>
                                <div class="modal-body">
                                    <div class="row">

                                       

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Beneficiary Name :</label><br>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select name="beneficiaryId" id="beneficiaryId"  class="form-group">
                                                    <c:forEach items="${beneficiaryDetails}" var="category">
                                                        <option value="${category.getId()}">${category.getName()}</option>
                                                    </c:forEach> 
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Investment Name :</label><br>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" id="investmentName" name ="investmentName" pattern="Enter Investment Name"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Investment Amount :</label><br>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" id="investmentAmount" name ="investmentAmount" pattern="Enter Investment Amount"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Bank Account Details:</label><br>
                                        </div>
                                    </div>
                                    <c:forEach items="${bankAccountDetails}" var="bank">
                                        <div id="bankaccountdiv">
                                            <input type="hidden" name="id" class="bankid" value=" ${bank.getId()}">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    Account Name
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group accountname">
                                                    ${bank.getAccountName()}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    Bank
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group bankname">
                                                    ${bank.getBank()}  
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    Branch
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group branchname">
                                                    ${bank.getBranch()} 
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    Account 
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group account">
                                                    ${bank.getAccount()} 
                                                </div>
                                            </div>
                                            <button type="button" id="saveInvestmentbtn" class="btn btn-primary">pay</button>
                                        </div>
                                    </c:forEach>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <!--<button type="button" id="saveInvestmentbtn" class="btn btn-primary">Save changes</button>-->
                                </div>
                            </div>
                        </div>
                    </div>
    </body>
</html>
       $('#saveInvestmentbtn').click(function () {
            var root = $(this).closest(".bankaccountdiv");
            var bankAccountId = root.find('.bankid').val();
            alert("click" + bankAccountId);
            var investmentName = $('#investmentName').val();
            var investmentAmount = $('#investmentAmount').val();
            var beneficiaryId = $('#beneficiaryId option:selected').val();
            var obj = {investmentName: investmentName,investmentcode:'${ic}', investedAmount: investmentAmount, portfolioCode: '${pc}', beneficiaryId: beneficiaryId, bankAccountId: bankAccountId};
            alert(JSON.stringify(obj));
            $.ajax({
                type: 'POST',
                url: './pending-investment',
                data: JSON.stringify(obj),
                headers: {"Content-Type": 'application/json'},
                success: function (data, textStatus, jqXHR) {

                    alert(data);

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("error");
                }
            });
        });