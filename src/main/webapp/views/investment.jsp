<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Investment</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <!--<link href="resources/images/mint.png" rel="shortcut icon"/>-->
        <link rel="icon" href="./resources/images/invsta-color-logo.png" type="image/gif" sizes="16x16">
        <link href="https://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <!--<link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>-->
        <link href="https://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>
        <!--<link href="https://backoffice.invsta.io/pocv/resources/css/main.css?version=3.5.1" rel="stylesheet"/>-->
        <link href="https://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link href="./resources/css/custom.css" rel="stylesheet"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
        <!--  <link href="resources/css/osfonts.css" rel="stylesheet"> -->



        <style>
            .el-chart-w {
                overflow: visible!important;
            }
            .portfolio_overlay {
                height: 75%;
                display: block;
            }
            .ilumony-box:hover .portfolio_overlay {
                display: block;
            }
            .funds-deatil {
                background: #fff;
                padding: 1rem;
                border-radius: 4px;
                margin-bottom: 10px;
            }
            .show_more_btn_new {
                position: absolute;
                top: 0;
                right: 0px;
                margin: 11px 27px;
            }
            .funds-deatil h4 {
                max-width: 80%;
                margin-bottom: 15px;
            }
            .collapse {
                display: none;
            }
            .collapse.show {
                display: block;
            }

            .menu-side .layout-w {

                min-height: 669px;
            }
            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('./resources/images/Preloader_3.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
        </style>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="">
                            <span><a href="./home">Home</a></span>
                        </li>
                        <li class="">
                            <span class="right-mark"><i class="fa fa-caret-right"></i></span>
                        </li>
                        <li class="">
                            <span><a href="">Investment</a></span>
                        </li>

                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-12 invest-page">
                                    <div class="card mt-3 tab-card">
                                        <div class="card-header tab-card-header">
                                            <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Funds</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" id="two" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Shares</a>
                                                </li>

                                            </ul>
                                        </div>

                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active p-3" id="one" role="tabpanel" aria-labelledby="one-tab">
                                                <div class="section1">
                                                    <div class="row">
                                                    <div class="col-md-12">
                                                        <h6>View our range of managed funds, and easily make an investment online. </h6>
                                                    </div>
                                                    </div>
                                                    <div class="row">
                                                        <div id="accordion" class="dropdown">

                                                            <div class="card">
                                                                <a class="card-link" data-toggle="collapse" href="#collapseOne">
                                                                    <div class="card-header">
                                                                        Enhanced Cash Fund
                                                                    </div>
                                                                </a>
                                                                <div id="collapseOne" class="collapse show" data-parent="#accordion">
                                                                    <div class="card-body">
                                                                        The fund aims to provide stable returns over the short term by investing primarily in cash and cash equivalents. 
                                                                        <a href="./home-new-portfolio" class="tabl-btn kiwi-btn invst-btn">Learn More</a>
                                                                    </div>


                                                                </div>
                                                            </div>

                                                            <div class="card">
                                                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                                                    <div class="card-header">
                                                                        Corporate Bond Fund
                                                                    </div>
                                                                </a>
                                                                <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                                                    <div class="card-body">
                                                                        The fund aims to provide stable returns over the short term by investing primarily in cash and cash equivalents. 
                                                                        <a href="./home-new-portfolio-1" class="tabl-btn kiwi-btn invst-btn">Learn More</a>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="card">
                                                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                                                                    <div class="card-header">
                                                                        Australasian Diversified Share Fund
                                                                    </div>
                                                                </a>
                                                                <div id="collapseThree" class="collapse" data-parent="#accordion">
                                                                    <div class="card-body">
                                                                        The fund aims to provide stable returns over the short term by investing primarily in cash and cash equivalents. 
                                                                        <a href="./home-new-portfolio-2" class="tabl-btn kiwi-btn invst-btn">Learn More</a>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="card">
                                                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
                                                                    <div class="card-header">
                                                                        Property Fund 
                                                                    </div>
                                                                </a>
                                                                <div id="collapseFour" class="collapse" data-parent="#accordion">
                                                                    <div class="card-body">
                                                                        The fund aims to provide stable returns over the short term by investing primarily in cash and cash equivalents. 
                                                                        <a href="./home-new-portfolio-3" class="tabl-btn kiwi-btn invst-btn">Learn More</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>              
                                            </div>
                                            <div class="tab-pane fade p-3" id="two" role="tabpanel" aria-labelledby="two-tab">
                                                <div class="section1">
                                                    <div class="row">
                                                    <div class="col-md-12">
                                                        <h6>View our range of managed funds, and easily make an investment online. </h6>
                                                    </div>
                                                    </div>
                                                    <div class="row">
                                                        <div id="accordion" class="dropdown">

                                                            <div class="card">
                                                                <a class="card-link" data-toggle="collapse" href="#collapsefive">
                                                                    <div class="card-header">
                                                                        Enhanced Cash Fund
                                                                    </div>
                                                                </a>
                                                                <div id="collapsefive" class="collapse show" data-parent="#accordion">
                                                                    <div class="card-body">
                                                                        The fund aims to provide stable returns over the short term by investing primarily in cash and cash equivalents. 
                                                                        <a href="javascript:void(0)" class="tabl-btn kiwi-btn invst-btn">Learn More</a>
                                                                    </div>


                                                                </div>
                                                            </div>

                                                            <div class="card">
                                                                <a class="collapsed card-link" data-toggle="collapse" href="#collapsesix">
                                                                    <div class="card-header">
                                                                        Corporate Bond Fund
                                                                    </div>
                                                                </a>
                                                                <div id="collapsesix" class="collapse" data-parent="#accordion">
                                                                    <div class="card-body">
                                                                        The fund aims to provide stable returns over the short term by investing primarily in cash and cash equivalents. 
                                                                        <a href="javascript:void(0)" class="tabl-btn kiwi-btn invst-btn">Learn More</a>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="card">
                                                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseseven">
                                                                    <div class="card-header">
                                                                        Australasian Diversified Share Fund
                                                                    </div>
                                                                </a>
                                                                <div id="collapseseven" class="collapse" data-parent="#accordion">
                                                                    <div class="card-body">
                                                                        The fund aims to provide stable returns over the short term by investing primarily in cash and cash equivalents. 
                                                                        <a href="javascript:void(0)" class="tabl-btn kiwi-btn invst-btn">Learn More</a>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="card">
                                                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseeight">
                                                                    <div class="card-header">
                                                                        Property Fund 
                                                                    </div>
                                                                </a>
                                                                <div id="collapseeight" class="collapse" data-parent="#accordion">
                                                                    <div class="card-body">
                                                                        The fund aims to provide stable returns over the short term by investing primarily in cash and cash equivalents. 
                                                                        <a href="javascript:void(0)" class="tabl-btn kiwi-btn invst-btn">Learn More</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div> 
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="display-type"></div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
    <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
    <!--    <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>-->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(window).load(function () {

            $(".loader").fadeOut("slow");
        });
    </script>

    <script>
        $('.clickinput').on("click", function () {
            var recemt = $(this).closest('.funds-deatil');
            recemt.find(".offer-input").toggle();
        });
        $(document).ready(function () {
            $(".loader").fadeOut("slow");
            $('.hidediv1').hide();
            $('.hidediv2').hide();
            $('.hidediv3').hide();
        });
        $('.democlick').click(function () {
            var data = $(this).data("target");
            $('.showclick').hide();
            $(data).show();
        });
    </script>
</body>
</html>