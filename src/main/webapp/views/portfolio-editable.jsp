
<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>westpac</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="http://backoffice.invsta.io/mint/images/mint.png" rel="shortcut icon"/>
        <link href="http://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>
        <link href="http://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
        <!--<link href="http://backoffice.invsta.io/pocv/resources/css/main.css?version=3.5.1" rel="stylesheet"/>-->
        <link href="http://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>
        <link href="http://backoffice.invsta.io/pocv/resources/css/custom.css" rel="stylesheet"/>
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link href="resources/css/osfonts.css" rel="stylesheet"> 

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">

                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="./home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Investment Fund</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>

                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box" style="display: block">
                            <div class="Portfolio-page">
                                <div class="header-image">
                                    <div class="edit-using-btn">
                                        
                                                    <a id="" href="./investment-fund-${pc}" role="tab">Back</a>
                                                </div>
                                    <div class="container1">
                                        <div class="header-logo">
                                            <div class="header-logo_row">
                                                <div class="cols-1">
                                                    <img  src="${portfolioDetail.portfolio_pic}" id="portfolio_pic">
                                                    <div class="camra-icon icon-set">
                                                    <input type="file" form="myForm" id="upload_portfolio_details" name="portfolio_image" onchange="setDp(this);" >
                                                </div>
                                                </div>
                                                
                                                <div class="cols-2">
                                                    <p>Single Currency Portfolio</p> 
                                                    <div class="btn-warning2">
                                                        <button class="updateportfolio " onclick="updatePortfolio();">
                                                            <i class="fa fa-save" aria-hidden="true"></i>
                                                        </button>
                                                    </div>
                                                    <h1 id="fund-name" contenteditable="true">${portfolioDetail.fundName}</h1>

                                                    <div class="info-port companies-dta-value-fund until-info">

                                                        <div class="info-port-text">
                                                            <div class="up_makt">
                                                                <p>Unit price</p>
                                                                <i class="fa fa-arrow-up"></i>
                                                                <span id="fund-price" contenteditable="true">00</span> </div>
                                                        </div>



                                                        <div class="info-port-text">
                                                            <div class="up_makt">
                                                                <p>Fund size </p>
                                                                <i class="fa fa-arrow-up"></i>
                                                                <span id="fund-size" contenteditable="true">00</span>
                                                            </div>
                                                        </div>



                                                        <div class="info-port-text">
                                                            <div class="up_makt">
                                                                <p>1 Year Return</p>
                                                                <i class="fa fa-arrow-up"></i>
                                                                <span id="fund-return" contenteditable="true">0.0</span>
                                                                <!--<i class="fas fa-arrow-up"></i>-->
                                                                <!--                                                                <span contenteditable="true">
                                                                                                                                    14.15%
                                                                                                                                </span> -->

                                                            </div>
                                                        </div>
                                                        <div class="btn-warning2">
                                                            <button class="updateportfolio " onclick="updateFundPerformance();">
                                                                <i class="fa fa-save" aria-hidden="true"></i>
                                                            </button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="add-investment-fund-modal-lg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" ria-hiddena="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="fund-name" contenteditable="true">${portfolioDetail.fundName}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Investment Name :</label><br>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name ="investmentName" pattern="Enter Investment Name"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Investment Amount :</label><br>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name ="investmentAmount" pattern="Enter Investment Amount"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary prim-btn">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="container ">
                                    <div class="row Portfolio-page-text">
                                        <div class="col-sm-12 col-md-10">
                                            <div class="element-wrapper">
                                                <h6 class="element-header">Performance Since Inception </h6>
                                            </div>

                                            <div class="char_section1">
                                                <div id="fundPerformanceChart1" style="height: 400px; overflow: hidden;" data-highcharts-chart="1"></div>


                                                <div class="row">



                                                    <div class="col-md-12 mt-3">
                                                        <div class="element-wrapper">

                                                            <!--<button class="updateportfolio .btn-warning" onclick="updateCurrentFundPerformance();">Update currentFund Performance</button>-->

                                                        </div>
                                                        <div class="element-wrapper">
                                                            <h6 class="element-header">Fund Performance </h6>
                                                            <div class="btn-warning2">
                                                                <button class="updateportfolio " onclick="updateFundPerformance();">
                                                                    <i class="fa fa-save" aria-hidden="true"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div id="elementboxcontent" class="">

                                                            <div class="row">
                                                                <div class="col-md-3 col-sm-3">
                                                                    <div class="element-box el-tablo centered trend-in-corner smaller">
                                                                        <div class="label">
                                                                            1 Month
                                                                        </div>
                                                                        <div class="value" id="1-month" contenteditable="true">0.0</div>

                                                                        <div class="trending trending-up " hidden>
                                                                            <span class="1-monthTrending" contenteditable="true">0.0</span>
                                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                                        </div> 


                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3">
                                                                    <div class="element-box el-tablo centered trend-in-corner smaller">
                                                                        <div class="label">
                                                                            3 Months
                                                                        </div>
                                                                        <div class="value" id="3-month" contenteditable="true">0.0</div>



                                                                        <div class="trending trending-up " hidden>
                                                                            <span class="3-monthTrending" contenteditable="true">0.0</span>
                                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                                        </div> 

                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3">
                                                                    <div class="element-box el-tablo centered trend-in-corner smaller">
                                                                        <div class="label">
                                                                            1 Year
                                                                        </div>
                                                                        <div class="value" id="1-year" contenteditable="true">0.0</div>


                                                                        <div class="trending trending-up" hidden>
                                                                            <span class="1-yearTrending" contenteditable="true">0.0</span>
                                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                                        </div> 


                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3">
                                                                    <div class="element-box el-tablo centered trend-in-corner smaller">
                                                                        <div class="label">
                                                                            5 Years
                                                                        </div>
                                                                        <div class="value" id="5-year" contenteditable="true">0.0</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">

                                                        <div id="elementboxcontent" class="element-box">

                                                            <div class="element-wrapper">
                                                                <form id="myForm" action="./update-PortfolioDetail" method="POST" enctype="multipart/form-data">
                                                                    <input type="hidden" name="Portfolio">
<!--                                                                    <input type="hidden" name="portfolio_pic">-->
                                                                    <input type="hidden" name="portfolio_pic" value="${portfolioDetail.portfolio_pic}">
                                                                    <input type="hidden" name="FundOverview">
                                                                    <input type="hidden" name="MonthlyFundUpdate">
                                                                    <input type="hidden" name="FeaturesOftheFund">
                                                                    <input type="hidden" name="monthlyReturnValue">
                                                                    <input type="hidden" name="fundName">

                                                                    <!--<input type="submit" value="Submit"/>-->
                                                                </form>
                                                            </div>
                                                            <div class="element-wrapper">
                                                                <div class="btn-warning2">
                                                                    <button class="updateportfolio " onclick="updatePortfolio();">
                                                                        <i class="fa fa-save" aria-hidden="true"></i>
                                                                    </button>
                                                                </div>
                                                                <h6 class="element-header">Fund Overview  </h6>
                                                                
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 update-fund-pera" id="paragraph" contenteditable="true">${portfolioDetail.getFundOverview()}</div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="element-box">

                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="risk-indicator">
                                                                        <div class="element-wrapper">
                                                                            <div class="btn-warning2">
                                                                                <button class="updateportfolio " onclick="updateFundPerformance();">
                                                                                    <i class="fa fa-save" aria-hidden="true"></i>
                                                                                </button>
                                                                            </div>
                                                                            <h6 class="element-header">Risk Indicator</h6>
                                                                            
                                                                        </div>
                                                                        <span class="indicator-box">
                                                                            <span class="risk-btns ">1</span>
                                                                            <span class="risk-btns addactive-cls">2</span>
                                                                            <span class="risk-btns">3</span>
                                                                            <span class="risk-btns">4</span>
                                                                            <span class="risk-btns">5</span>
                                                                            <span class="risk-btns">6</span>
                                                                            <span class="risk-btns">7</span>
                                                                        </span>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="element-box table-box">
                                                            <form action="./UpdateTotalFmcaInvestmentMix" method="post" id="submit-UpdateTotalFmcaInvestmentMix" name="submit-complete2" class="editable-btn">  
                                                            <div class="element-wrapper">
                                                                                     <div class="btn-warning2">
                                                                        <button type="submit" class="save_btn" id="btn-UpdateTotalFmcaInvestmentMix-save" style="" >
                                                                            <i class="fa fa-save" aria-hidden="true"></i>

                                                                        </button>
                                                                    </div>
                                                                <h6 class="element-header">Target Investment Mix</h6>
                                                            </div>
                                                            <div class="el-chart-w data-funds" id="container19" style="height:340px">
                                                                    
                                               
                                                                    <table id="portfolio-tableFmcaInvestmentMix" class="table table-bordered" style="height: 255px; max-width: 600px; margin: 0px auto;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    Investment Name
                                                                                    <input type="hidden" name="portfolio" value="${pc}">
                                                                                </th>
                                                                                <th>
                                                                                    price
                                                                                </th>
                                                                                <th>
                                                                                     ColorCode

                                                                                </th>
                                                                                <th>

                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="targetdata1">


                                                                        <tfoot>
                                                                           <tr>
                                                                               <th class="ab">
                                                                                   <a href="javascript:void(0)" onclick="onTargetFund1();"><span class="extra-tooltip"></span><i class="fa fa-plus"></i></a>
                                                                                  </th>
                                                                                </tr>
                                                                        </tfoot>
                                                                    </table>
                                                                </form>
                                                            </div>


                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="element-box table-box">
                                                             <form action="./UpdatefmcaTopAsset" method="post" id="submit-complete2" name="submit-complete2" class="editable-btn"> 
                                                            <div class="element-wrapper">
                                                                 <div class="btn-warning2">
                                                                        <button type="submit" class="save_btn" id="btn-save2" style="" >
                                                                            <i class="fa fa-save" aria-hidden="true"></i>
                                                                        </button>
                                                                    </div>
                                                                <h6 class="element-header">Sector Allocation</h6>
                                                            </div>
                                                            <!--                                                            <div class="el-chart-w" id="container-221" style="height:340px"></div>
                                                            -->    <div class="el-chart-w data-funds" style="height:340px">
                                                                    
                                                                   
                                                                    <table id="portfolio-table" class="table table-bordered" style="height: 255px; max-width: 600px; margin: 0px auto;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    investmentName
                                                                                    <input type="hidden" name="portfolio" value="${pc}">
                                                                                </th>
                                                                                <th>
                                                                                    price
                                                                                </th>
                                                                                <th>
                                                                                    ColorCode

                                                                                </th>
                                                                                <th>

                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="targetdata">


                                                                        <thead>
                                                                         <tr>
                                                                              <th>
                                                                                   <a href="javascript:void(0)" onclick="onTargetFund();"><span class="extra-tooltip"></span><i class="fa fa-plus"></i></a>

                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                    </table>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>                                                   
                                                    <div class="col-md-12">

                                                        <div id="elementboxcontent" class="element-box">
                                                            <div class="element-wrapper">
                                                                   <div class="btn-warning2">
                                                                    <button class="updateportfolio " onclick="updatePortfolio();">
                                                                        <i class="fa fa-save" aria-hidden="true"></i>
                                                                    </button>
                                                                </div>
                                                                <h6 class="element-header"> Monthly Fund Update <a href="" class="fund-detail-btn">Download Full Update</a></h6>
                                                             
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12" >
                                                                    <h6><span id="portheading" contenteditable="true">${portfolioDetail.getMonthlyReturnValue()}</span></h6>
                                                                </div>
                                                                <div class="col-md-12" id="portfoliooverview" contenteditable="true">${portfolioDetail.getMonthlyFundUpdate()}</div>
                                                            </div>                                                           
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="element-box">
                                                            <div class="element-wrapper">
                                                                 <div class="btn-warning2">
                                                                    <button class="updateportfolio " onclick="updatePortfolio();">
                                                                        <i class="fa fa-save" aria-hidden="true"></i>
                                                                    </button>
                                                                </div>
                                                                <h6 class="element-header">Features of the fund </h6>
                                                               
                                                            </div>
                                                            <div class="feature-text">${portfolioDetail.getFeaturesOftheFund()}</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="element-header">Investment Team</h6>
                                                        </div> 
                                                        <div class="element-box">
                                                            <div class="invest-team" id="invest-team">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <form id="investerForm" action="./UpdateInvestmentTeam" method="post" enctype="multipart/form-data">
                                                                            <input type="hidden" name="portfolio" value="${pc}"/>
                                                                            <c:forEach items="${investmentTeam}" var="team">
                                                                                <div class="col-md-3 col-sm-3 five-div invest-team-images">
                                                                                    <div class="image_mang">
                                                                                    <img src="${team.pic_url}" alt="image" class="Imgurl"/>
                                                                                     <div class="camra-icon camra-icon1">
                                                                                        <input type="file" name="image_file" onchange="readURL(this);"/>
                                                                                    </div>
                                                                                      <span class="minus-div" onclick="removejsdata(this);"> <i class="fa fa-trash-o"></i></span>
                                                                                    </div>
                                                                                  
                                                                                   
                                                                                    <input type="hidden" name="pic_url" value="${team.pic_url}"/>
                                                                                    <input type="text" name="name" value="${team.name}"/>
                                                                                    <select name="place">
                                                                                        <option value="Head of Investments" <c:if test="${team.place eq 'Head of Investments'}"> selected </c:if>>Head of Investments</option>
                                                                                        <option value="Portfolio Manager"<c:if test="${team.place eq 'Portfolio Manager'}"> selected </c:if>>Portfolio Manager</option>
                                                                                        </select>
                                                                                    </div>
                                                                         
<!--                                                                            <div class="appenddiv"></div> 
                                                                            <span class="add-plus-div"> <i class="fa fa-plus"></i></span>-->
                                                                            <div class="col-md-9 col-sm-9 five-div invest-team-images">
<!--                                                                        <div class="element-wrapper">
                                                                            <h6 class="element-header">BIO</h6>
                                                                        </div> -->
                                                                                <div class="bio-input" contenteditable="true">${team.team_bio}</div>
                                                                            </div>
                                                                                          </c:forEach>
                                                                            <div class="btn-warning3 submit-data btn-botom">
                                                                                <button type="submit" class="updateportfolio " onclick="submitTeam();">Submit</button>
                                                                            </div>
                                                                            <input type="hidden" name="team_bio">
                                                                        </form>

                                                                        <%--<h6 class="team-position" id="team-position${list.index}" contenteditable="true">Head of Investments</h6>
                                                                            <p class="team-name" id="team-name${list.index}" contenteditable="true">${team.name}</p>--%>
                                                                        <!--<p>Anthony Halls</p>-->
                                                                        <!--                                                                    <div class="col-md-3 col-sm-3 five-div invest-team-images">
                                                                                                                                                <img src="http://backoffice.invsta.io/pocv/resources/img/team-3.png" alt="image">
                                                                                                                                                <h6>Portfolio Manager</h6>
                                                                                                                                                <p>David Fyfe</p>
                                                                                                                                            </div>
                                                                                                                                            <div class="col-md-3 col-sm-3 five-div invest-team-images">
                                                                                                                                                <img src="http://backoffice.invsta.io/pocv/resources/img/team-4.png" alt="image">
                                                                                                                                                <h6>Portfolio Manager</h6>
                                                                                                                                                <p>John Middleton</p>
                                                                                                                                            </div>
                                                                                                                                            <div class="col-md-3 col-sm-3 five-div invest-team-images">
                                                                                                                                                <img src="http://backoffice.invsta.io/pocv/resources/img/team-5.png" alt="image">
                                                                                                                                                <h6>Portfolio Manager</h6>
                                                                                                                                                <p>Marek Krzeczkowski</p>
                                                                                                                                            </div>-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 resource-pdf">
                                                        <div class="element-box">
                                                            <form id="portfolioPdfForm" action="./UpdateInvestmentPdf" method="post" enctype="multipart/form-data">
                                                                <input type="hidden" name="portfolio" value="${pc}"/>
                                                                <div class="element-wrapper">
                                                                    <h6 class="element-header">Resources </h6>
                                                                </div>
                                                                <c:forEach items="${investmentPdf}" var="pdf">
                                                                    <div class="resources-pdf">
                                                                        <span class="minus-div-pdf"  onclick="removejsdiv(this);"> <i class="fa fa-trash-o"></i></span>
                                                                        <input type="text" name="pdf_name" value="${pdf.pdf_name}" class="red_file"/>
                                                                        <input type="hidden" name="pdf_url" value="${pdf.pdf_url}"/>
                                                                        <label class="file_tag">
                                                                            Upload File
                                                                            <input type="file" name="pdf_file" />
                                                                        </label>
                                                                        <span class="url-input"><input type="text" placeholder="Enter URL" name="enterurl"></span>
                                                                        <span>

                                                                            <a href="${pdf.pdf_url}" title="download pdf"><i class="fa fa-file-pdf-o"></i></a>
                                                                        </span>
                                                                    </div>
                                                                </c:forEach> 
                                                                <div class="appenddivpdf"></div> 
                                                                <span class="add-plus-div-pdf"> <i class="fa fa-plus"></i></span>
                                                                <div class="btn-warning3 submit-data">
                                                                    <button type="submit" class="updateportfolio " onclick="submitPdf();">Submit</button>
                                                                </div>

                                                            </form>
                                                            <!--                                                                <div class="resources-pdf">
                                                                                                                                <p>Mint Statement of Investment Policy Objectives 2019 </p>
                                                                                                                                <span>
                                                                                                                                    <a href="https://www.mintasset.co.nz/assets/PDS-SIPO/Mint-Statement-of-Investment-Policy-Objectives-2019.pdf">Download Pdf</a>
                                                                                                                                </span>
                                                                                                                            </div>
                                                                                                                            <div class="resources-pdf">
                                                                                                                                <p>Mint Asset Management Fund Financial Statements FY19 </p>
                                                                                                                                <span>
                                                                                                                                    <a href="https://www.mintasset.co.nz/assets/Uploads/Mint-Asset-Management-Funds-Financial-Statements-FY19.pdf">Download Pdf</a>
                                                                                                                                </span>
                                                                                                                            </div>
                                                                                                                            <div class="resources-pdf">
                                                                                                                                <p>Mint SRI Policy</p>
                                                                                                                                <span >
                                                                                                                                    <a href="https://www.mintasset.co.nz/assets/Uploads/Mint-SRI-Policy.pdf">Download Pdf</a>
                                                                                                                                </span>
                                                                                                                            </div>
                                                                                                                            <div class="resources-pdf">
                                                                                                                                <p>UNPRI Report</p>
                                                                                                                                <span>
                                                                                                                                    <a href="https://www.mintasset.co.nz/assets/Uploads/UNPRI-Report-2019.pdf">Download Pdf</a>
                                                                                                                                </span>
                                                                                                                            </div>-->

                                                        </div>
                                                    </div>
                                                                                                                        <div class="col-md-12 resource-pdf">
                                                        <div class="element-box">
                                                            <div class="element-wrapper">
                                                                <h6 class="element-header">Quarterly Fund Updates  </h6>
                                                            </div>
                                                            <c:forEach items="${investmentPdf}" var="pdf">
                                                                <div class="resources-pdf">
                                                                    <p>${pdf.pdf_name} </p>
                                                                    <span>
                                                                        <a href="${pdf.pdf_url}"><i class="fa fa-file-pdf-o"></i></a>
                                                                    </span>
                                                                </div>
                                                            </c:forEach>
                                                            <!--                                                            <div class="resources-pdf">
                                                                                                                            <p>Mint Statement of Investment Policy Objectives 2019 </p>
                                                                                                                            <span>
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/PDS-SIPO/Mint-Statement-of-Investment-Policy-Objectives-2019.pdf">Download Pdf</a>-->
                                                            <!--                                                                </span>
                                                                                                                        </div>
                                                                                                                        <div class="resources-pdf">
                                                                                                                            <p>Mint Asset Management Fund Financial Statements FY19 </p>
                                                                                                                            <span>
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/Uploads/Mint-Asset-Management-Funds-Financial-Statements-FY19.pdf">Download Pdf</a>
                                                                                                                            </span>
                                                                                                                        </div>
                                                                                                                        <div class="resources-pdf">
                                                                                                                            <p>Mint SRI Policy</p>
                                                                                                                            <span >
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/Uploads/Mint-SRI-Policy.pdf">Download Pdf</a>
                                                                                                                            </span>
                                                                                                                        </div>
                                                                                                                        <div class="resources-pdf">
                                                                                                                            <p>UNPRI Report</p>
                                                                                                                            <span>
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/Uploads/UNPRI-Report-2019.pdf">Download Pdf</a>
                                                                                                                            </span>
                                                                                                                        </div>-->

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-2">
                                            <div class="side_barr">
                                                <h3>Other Funds</h3>
                                                <div class="other_port">
                                                    <c:forEach items="${otherPortfolioDetail}" var="port">
                                                        <a href="javascript:void(0)" id="fundlink1">
                                                            <div class="other_row">
                                                                <div class="other_row-img">
                                                                    <img src="./resources/images/globe-plane.jpg">
                                                                </div>
                                                                <div class="other_row-text">
                                                                    <p><b id="other-fund1">${port.fundName}</b></p>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="display-type"></div>
    </div>
    <script src="http://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" defer></script>
    <script src="http://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" defer></script>
    <script src="http://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" defer></script>
    <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
    <script src="https://code.highcharts.com/highcharts.js" ></script> 
    <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
    <script src="https://code.highcharts.com/modules/cylinder.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">
                                                                            $(window).load(function () {
                                                                                $(".loader").fadeOut("slow");
                                                                            });
                                                                            
                                                                            
                                                                            function submitPdf(){
                                                                                var newUrl=$('#portfolioPdfForm input[name=enterurl]').val();
                                                                                if(newUrl!==""){
                                                                                $('#portfolioPdfForm input[name=pdf_url]').val(newUrl);
                                                                            }
                                                                            $('#portfolioPdfForm ').submit();
                                                                            }
                                                                            function submitTeam(){
                                                                                var bio=$('.bio-input').text();
//                                                                                alert(bio);
                                                                             
                                                                                $('#investerForm input[name=team_bio]').val(bio);
                                                                            
                                                                            $('#investerForm ').submit();
                                                                            }
                                                                            
                                                                            
                                                                            
                                                                            
//                                                                            function updatepic(input) {
////                                                                                alert(input);
////                                                                                var fileName = $(input).target.files[0].name;
//////                                                                            var pic=document.getElementById('upload_portfolio_details');
////                                                                                alert(fileName);
////                                                                            $('#upload_portfolio_details').change(function () {
////                                                                                alert();
//////                                                                                $('input[type=file]').change(function () {
////                                                                                alert(this.files[0].mozFullPath);
////                                                                            });
////                                                                            $('#btnDisplay').click(function () {
//                                                                                //get file object
//                                                                                var file = document.getElementById('upload_portfolio_details').files[0];
//                                                                                if (file) {
//                                                                                    // create reader
//                                                                                    var reader = new FileReader();
//                                                                                 
//                                                                                    reader.readAsText(file);
//                                                                                    reader.onload = function (e) {
//                                                                                        // browser completed reading file - display it
//                                                                                        alert(e.target.result);
//                                                                                    };
//                                                                                }
////                                                                            });
////                                                                                    var $this = $(input), $clone = $this.clone();
////                                                                                    $this.after($clone).appendTo('#myForm');
////                                                                                    alert($('form input[name=Portfolio_pic]').val());
////                                                                                });
////                                                                                $('form input[name=Portfolio_pic]').val(input);
////                                                                                alert($('form input[name=Portfolio_pic]').val());
//
//                                                                            }




    </script>
    <script>
//                                                   $(".ImageBrowse").change(function () {
//       
//                                                if (this.files && this.files[0]) {
//                                                    var reader = new FileReader();
//                                                    reader.onload = imageIsLoaded;
//                                                    reader.readAsDataURL(this.files[0]);
//                                                }
//                                            });
//
//
//                                            function imageIsLoaded(e) {
//                                                alert(e);
//                                             var root=   e.closest(".invest-team-images");
//                                             alert(root);
//                                                $(root).find('.Imgurl').attr('src', e.target.result);
//                                            }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(input).closest('.invest-team-images').find('.Imgurl').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);

            }
        }
        function setDp(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#portfolio_pic').attr('src', e.target.result);
//                    $(input).closest('.invest-team-images').find('#portfolio_pic').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);

            }
        }


        $('.add-plus-div').click(function () {
            $('.appenddiv').append("<div class='col-md-3 col-sm-3 five-div invest-team-images'><img src='' alt='' class='Imgurl'/><span  class='minus-div' onclick='removejsdata(this);'> <i class='fa fa-trash-o'></i></i></span><input type='hidden' name='pic_url' value='globe-plane.jpg'/><div class='camra-icon'><input type='file' name='image_file' onchange='readURL(this);'></div><input type='text' name='name' value=''/><select name='place'><option value='Head of Investments'>Head of Investments</option><option value='Portfolio Manager'>Portfolio Manager</option> </select></div>");
        });

        function removejsdata(ele) {
            $(ele).closest(".invest-team-images").remove();

        }
        ;
        $('.add-plus-div-pdf').click(function () {
            $('.appenddivpdf').append(" <div class='resources-pdf'><span class='minus-div-pdf' onclick='removejsdiv(this);'> <i class='fa fa-trash-o'></i></span><input type='text' name='pdf_name' value='' class='red_file'/><input type='hidden' name='pdf_url' value='pdf'/><label class='file_tag'>Upload File<input type='file' name='pdf_file' /></label><span class='url-input'><input type='text' placeholder='Enter URL' name='enterurl'></span>");
        });
        function removejsdiv(ele) {
            $(ele).closest(".resources-pdf").remove();

        }
        ;var performanceSinceinceptionDate=[];
        var performanceSinceinceptionData=[];
          var  performanceinception=[];
        $(document).ready(function () {
            var htmlCode = "";
            var targetlastindex = '0';
            var targetlastindex1 = '0';
            var fundName = '${portfolioDetail.fundName}';   
                 $.each(${performanceSinceinception}, function (idx, val) {          
                performanceSinceinceptionDate.push(val.date);
                performanceSinceinceptionData.push(parseInt(val.data));
            });
              
            areachart1(fundName);
        <c:forEach items="${fmcaTopAssets}" var="aShare" varStatus="map">
            htmlCode = '<tr>';
            //                                                     
            htmlCode = htmlCode + '<td class="text-right">';
            htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="investmentName" value="${aShare.investmentName}" required/>';
            htmlCode = htmlCode + '</td>';
            htmlCode = htmlCode + '<td class="text-right">';
            htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="prices" value="${aShare.price}" required/>';
            htmlCode = htmlCode + '</td>';
            htmlCode = htmlCode + '<td class="text-right">';
            htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="colorCode" value="${aShare.colorCode}" required/>';
            htmlCode = htmlCode + '</td>';
            addTargetDeletedata('${map.index+1}');
            targetlastindex = '${map.index+2}';
            htmlCode = htmlCode + '</tr>';
            $("#targetdata").append(htmlCode);
        </c:forEach>
        <c:forEach items="${totalFmcaInvestmentMix}" var="aShare" varStatus="map">
            htmlCode = '<tr>';
            //                                                     
            htmlCode = htmlCode + '<td class="text-right">';
            htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="investmentName" value="${aShare.investmentName}" required/>';
            htmlCode = htmlCode + '</td>';
            htmlCode = htmlCode + '<td class="text-right">';
            htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="prices" value="${aShare.price}" required/>';
            htmlCode = htmlCode + '</td>';
            htmlCode = htmlCode + '<td class="text-right">';
            htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="colorCode" value="${aShare.colorCode}" required/>';
            htmlCode = htmlCode + '</td>';
            addTargetDeletedata1('${map.index+1}');
            targetlastindex1 = '${map.index+2}';
            htmlCode = htmlCode + '</tr>';
            $("#targetdata1").append(htmlCode);
        </c:forEach>


            function addTargetDeletedata(idx) {
                htmlCode = htmlCode + '<td class="text-right d-flex justify-content-betweeen delete-btn-end">';
//                htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFund(' + (idx) + ');"><span class="extra-tooltip"></span><i class="fa fa-plus"></i></a>';
                htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFundDeleteRow(this);"><span class="extra-tooltip"></span><i class="fa fa-trash-o"></i></i></a>';
                htmlCode = htmlCode + '</td>';
            }
            function addTargetDeletedata1(idx) {
                htmlCode = htmlCode + '<td class="text-right d-flex justify-content-betweeen delete-btn-end">';
//                htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFund1(' + (idx) + ');"><span class="extra-tooltip"></span><i class="fa fa-plus"></i></a>';
                htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFundDeleteRow1(this);"><span class="extra-tooltip"></span><i class="fa fa-trash-o"></i></i></a>';
                htmlCode = htmlCode + '</td>';
            }

        });
        function onTargetFundDeleteRow(anchor) {
            var idx = anchor.parentNode.parentNode.rowIndex;
            document.getElementById("portfolio-table").deleteRow(idx);
            percentageTotal();
        }
        function onTargetFundDeleteRow1(anchor) {
            var idx = anchor.parentNode.parentNode.rowIndex;
            document.getElementById("portfolio-tableFmcaInvestmentMix").deleteRow(idx);
            percentageTotal();
        }
        function onTargetFund() {
            //                                        alert(0);
            var htmlCode = '';
            htmlCode = '<tr>';
            htmlCode = htmlCode + '<td class="text-right">';
            htmlCode = htmlCode + '<input class="form-control" type="text" name="investmentName" id="investmentName" required="required" placehoder="investmentName" value="investmentName" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode !== 32) && !(event.charCode >=48 && event.charCode <=57 ))"/>';
            htmlCode = htmlCode + '</td>';
            htmlCode = htmlCode + '<td class="text-right">';
            htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="prices" id="prices" value="0" required="required" />';
            htmlCode = htmlCode + '</td>';
            htmlCode = htmlCode + '<td class="text-right">';
            htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="colorCode" id="colorCode" value="0" required="required" />';
            htmlCode = htmlCode + '</td>';
            htmlCode = htmlCode + '<td class="text-right d-flex justify-content-betweeen delete-btn-end">';
//            htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFund(' + eval(idx) + ');"><span class="extra-tooltip"></span><i class="fa fa-plus"></i></a>';
            htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFundDeleteRow(this);"><span class="extra-tooltip"></span><i class="fa fa-trash-o"></i></a>';
            htmlCode = htmlCode + '</td>';
            htmlCode = htmlCode + '</tr>';
            $("#targetdata").append(htmlCode);
            
        }
        function onTargetFund1() {
            //                                        alert(0);
            var htmlCode = '';
            htmlCode = '<tr>';
            htmlCode = htmlCode + '<td class="text-right">';
            htmlCode = htmlCode + '<input class="form-control" type="text" name="investmentName" id="investmentName" required="required" placehoder="investmentName" value="investmentName" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode !== 32) && !(event.charCode >=48 && event.charCode <=57 ))"/>';
            htmlCode = htmlCode + '</td>';
            htmlCode = htmlCode + '<td class="text-right">';
            htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="prices" id="prices" value="0" required="required" onchange=" percentageTotal();"/>';
            htmlCode = htmlCode + '</td>';
            htmlCode = htmlCode + '<td class="text-right">';
            htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="colorCode" id="colorCode" value="0" required="required" onchange=" percentageTotal();"/>';
            htmlCode = htmlCode + '</td>';
            htmlCode = htmlCode + '<td class="text-right d-flex justify-content-betweeen delete-btn-end">';
//            htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFund1(' + eval(idx) + ');"><span class="extra-tooltip"></span><i class="fa fa-plus"></i></a>';
            htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFundDeleteRow1(this);"><span class="extra-tooltip"></span><i class="fa fa-trash-o"></i></a>';
            htmlCode = htmlCode + '</td>';
            htmlCode = htmlCode + '</tr>';
            $("#targetdata1").append(htmlCode);
            percentageTotal();
        }
        $('#btn-save2').click(function () {
            var x = document.getElementById("portfolio").value;
            var y = document.getElementById("investmentName").value;
            var Z = document.getElementById("price").value;
            if (x === "" || y === "" || Z === "") {
                //                                                                alert("Filled should not blank");
                return false;
            } else {
                swal({
                    title: "Proceed",
                    text: "CHANGES PUBLISHED SUCCESSFULLY.",
                    type: "success",
                    timer: 4000,
                    showConfirmButton: true
                });
                document.getElementById("submit-complete2").submit(); //form submission
            }
        });
        $('#btn-UpdateTotalFmcaInvestmentMix-save').click(function () {
            var x = document.getElementById("portfolio").value;
            var y = document.getElementById("investmentName").value;
            var Z = document.getElementById("price").value;
            if (x === "" || y === "" || Z === "") {
                //                                                                alert("Filled should not blank");
                return false;
            } else {
                swal({
                    title: "Proceed",
                    text: "CHANGES PUBLISHED SUCCESSFULLY.",
                    type: "success",
                    timer: 4000,
                    showConfirmButton: true
                });
                document.getElementById("submit-UpdateTotalFmcaInvestmentMix-save").submit(); //form submission
            }
        });






    </script>
    <script>




        //        function onTargetFund(idx) {
        ////                                        alert(0);
        //            var htmlCode = '';
        //            htmlCode = '<tr>';
        //            htmlCode = htmlCode + '<td class="text-right">';
        //            htmlCode = htmlCode + '<input class="form-control" type="text" name="shareId" id="shareId" required="required" placehoder="Bitcoin" value="Bitcoin" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode !== 32) && !(event.charCode >=48 && event.charCode <=57 ))"/>';
        //            htmlCode = htmlCode + '</td>';
        //            htmlCode = htmlCode + '<td class="text-right">';
        //            htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="number" step="0.01" name="percentage" id="percentage" value="0" required="required" onchange=" percentageTotal();"/>';
        //            htmlCode = htmlCode + '</td>';
        //            htmlCode = htmlCode + '<td class="text-right d-flex justify-content-betweeen">';
        //            htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFund(' + eval(idx) + ');"><span class="extra-tooltip"></span><i class="fa fa-plus"></i></a>';
        //            htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFundDeleteRow(this);"><span class="extra-tooltip"></span><i class="fas fa-trash-alt"></i></a>';
        //            htmlCode = htmlCode + '</td>';
        //            htmlCode = htmlCode + '</tr>';
        //            $("#targetdata").append(htmlCode);
        //            percentageTotal();
        //        }

        $(document).ready(function () {
            var pc = '${pc}';
            $(".loader").hide();
            var fundName;
            //            $.ajax({
            //                type: 'GET',
            //                url: './rest/3rd/party/api/all-funds',
            //                headers: {"Content-Type": 'application/json'},
            //                success: function (data, textStatus, jqXHR) {
            //                 
            //                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
            //                    $.each(obj, function (idx, val) {
            //                        if (val.Code === pc) {
            //                            if (val.Name.includes("Mint")) {
            //                                fundName = val.Name;
            //                            } else {
            //                                fundName = 'Mint ' + val.Name;
            //                            }
            //                            fundName = fundName.replace('Wholesale', '');
            //                            $('#fund-name').text(fundName).attr('contenteditable', 'true');
            //
            //                        }
            //                        if (val.Code === "290002") {
            //                            $('#other-fund1').text(val.Name);
            //                            if ($('#other-fund1').text() === fundName) {
            //                                $('#fundlink1').hide();
            //                            }
            //                            var id1 = './investment-fund-' + val.Code;
            //                            $('#fundlink1').attr('href', id1);
            //                        }
            //                        if (val.Code === "290004") {
            //                            $('#other-fund2').text('Mint ' + val.Name);
            //                            if ($('#other-fund2').text() === fundName) {
            //                                $('#fundlink2').hide();
            //                            }
            //                            var id1 = './investment-fund-' + val.Code;
            //                            $('#fundlink2').attr('href', id1);
            //                        }
            //                        if (val.Code === "290006") {
            //                            $('#other-fund3').text('Mint ' + val.Name.replace('Wholesale', ''));
            //                            if ($('#other-fund3').text() === fundName) {
            //                                $('#fundlink3').hide();
            //                            }
            //                            var id1 = './investment-fund-' + val.Code;
            //                            $('#fundlink3').attr('href', id1);
            //                        }
            //
            //                        if (val.Code === "290002") {
            //                            $('#other-fund4').text('Mint ' + val.Name);
            //                            if ($('#other-fund4').text() === fundName) {
            //                                $('#fundlink4').hide();
            //                            }
            //                            var id1 = './investment-fund-' + val.Code;
            //                            $('#fundlink4').attr('href', id1);
            //                        }
            //                        areachart1(fundName);
            //                    });
            //
            //                },
            //                error: function (jqXHR, textStatus, errorThrown) {
            //                }
            //            });
        });
        //        $('#paragraph').html('');
        //        $('#portfoliooverview').html('');
        //        $('#portheading').html('');
        //        $('.feature-text').html('');
        //                $.ajax({
        //                type: 'GET',
        //                url: './rest/3rd/party/api/fund-details-' + pc,
        //                headers: {"Content-Type": 'application/json'},
        //                success: function (data, textStatus, jqXHR) {
        //                    //                        alert(data);
        //                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
        //                    $.each(obj.pricePortfolioUnitPrices, function (idx, val) {
        //                        priceArr.push(val.Prices);
        //
        //                    });
        //                    price = priceArr[priceArr.length - 1];
        //                    $('#fund-price').text(price);
        //                },
        //                error: function (jqXHR, textStatus, errorThrown) {
        //                }
        //            });
        //            $.ajax({
        //                type: 'GET',
        //                url: './rest/groot/db/api/investment-fund-details-'+pc,
        //                headers: {"Content-Type": 'application/json'},
        //                success: function (data, textStatus, jqXHR) {
        //                    $(".loader").hide();
        //                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
        //                    console.log(JSON.stringify(obj));
        //                    $.each(obj, function (idx, val) {
        //                        console.log(JSON.stringify(val[0].Portfolio));
        //                        $('#paragraph').html(val[0].FundOverview);
        //                        $('#portfoliooverview').html(val[0].MonthlyFundUpdate);
        //                        $('#portheading').html(val[0].monthlyReturnValue);
        //                        $('.feature-text').html(val[0].FeaturesOftheFund);
        //                    });
        //                },
        //                error: function (jqXHR, textStatus, errorThrown) {
        //                    alert(textStatus);
        //                }
        //            });
        //      });
        //        price = priceArr[priceArr.length - 1];
        //        $('#fund-price').text(price);
        //        },
        //                error: function (jqXHR, textStatus, errorThrown) {
        //                }
        //        $.ajax({
        //        type: 'GET',
        //                url: './rest/groot/db/api/investment-fund-details-' + id,
        //                headers: {"Content-Type": 'application/json'},
        //                success: function (data, textStatus, jqXHR) {
        //                var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
        //                console.log(JSON.stringify(obj));
        //                $.each(obj.investmentFundDetails, function (idx, val) {
        //                console.log(JSON.stringify(val.Portfolio));
        ////                $('#paragraph').html(val.FundOverview);
        ////                $('#portfoliooverview').html(val.MonthlyFundUpdate);
        ////                $('#portheading').html(val.monthlyReturnValue);
        ////                $('.feature-text').html(val.FeaturesOftheFund);
        //                });
        //                },
        //                error: function (jqXHR, textStatus, errorThrown) {
        //                }
        //        });
    </script>
    <script>
        Highcharts.chart('livebalance1', {
            chart: {
                type: 'spline',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {

                        // set up the updating of the chart each second
                        var series = this.series[0];
                        setInterval(function () {
                            var x = (new Date()).getTime(), // current time
                                    y = Math.random();
                            series.addPoint([x, y], true, true);
                        }, 1000);
                    }
                }
            },
            time: {
                useUTC: false
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    color: '#03A9F4',
                    shadow: true,
                    lineWidth: 3,
                    marker: {
                        enabled: false
                    }
                }
            },
            title: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {

                title: {
                    text: 'Portfolio Investments ($)'
                },
                plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br/>',
                pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                    name: 'Random data',
                    data: (function () {
                        // generate an array of random data
                        var data = [],
                                time = (new Date()).getTime(),
                                i;
                        for (i = -19; i <= 0; i += 1) {
                            data.push({
                                x: time + i * 1000,
                                y: Math.random()
                            });
                        }
                        return data;
                    }())
                }]
        });
    </script>

    <script>
        Highcharts.chart('stockbalance1', {
            chart: {
                type: 'area'
            },
            accessibility: {
                description: ''
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            legend: {
                enabled: false
            },
            xAxis: [{
                    categories: ['21 Oct', '28 Oct', '4 Nov', '11 Nov', '18 Nov', '25 Nov',
                        '1 Dec', '8 Dec']

                }],
            yAxis: {
                title: {
                    text: ''
                },
                labels: {
                    formatter: function () {
                        return this.value / 1000 + 'k';
                    }
                }
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name} had stockpiled <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
            },
            plotOptions: {
                area: {
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                }
            },
            series: [{
                    name: '',
                    data: [
                        0, 110, 250, 300, 410, 535,
                        709
                    ]
                }, ]
        });
    </script>
    <script>
        $.getJSON('https://www.highcharts.com/samples/data/aapl-c.json', function (data) {

            // Create the chart
            Highcharts.stockChart('container', {

                rangeSelector: {
                    selected: 1
                },
                title: {
                    text: 'AAPL Stock Price'
                },
                navigator: {
                    enabled: false
                },
                series: [{
                        name: 'AAPL Stock Price',
                        data: data,
                        tooltip: {
                            valueDecimals: 2
                        }
                    }]
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $("#view_transaction").hide();
            $("#view_transaction_status").click(function () {
                $("#view_transaction").toggle();
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $(".company-show1").hide();
            $(".company-show2").hide();
            $(".company-show3").hide();
            $(".company-show4").hide();
            $(".company-show5").hide();
            $(".company-show6").hide();
            $(".company-show7").hide();
            $(".company-show8").hide();
            $(".company-show9").hide();
            $(".companyshow1").click(function () {
                $(".company-show1").toggle();
            });
            $(".companyshow2").click(function () {
                $(".company-show2").toggle();
            });
            $(".companyshow3").click(function () {
                $(".company-show3").toggle();
            });
            $(".companyshow4").click(function () {
                $(".company-show4").toggle();
            });
            $(".companyshow5").click(function () {
                $(".company-show5").toggle();
            });
            $(".companyshow6").click(function () {
                $(".company-show6").toggle();
            });
            $(".companyshow7").click(function () {
                $(".company-show7").toggle();
            });
            $(".companyshow8").click(function () {
                $(".company-show8").toggle();
            });
            $(".companyshow9").click(function () {
                $(".company-show9").toggle();
            });
        });
    </script>
    <script>
        Highcharts.chart('fund-container22', {
            chart: {
                type: 'pie'
            },
            credits: {
                enabled: false,
            },
            exporting: {
                enabled: false,
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            plotOptions: {
                pie: {
                    innerSize: 100,
                    depth: 45,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}</b>: {point.y:,.2f}',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    },
                },
            },
            series: [{
                    showInLegend: true,
                    name: 'Percentage',
                    data: [
                        ['Australia 24.2%', 24.2],
                        ['China 17.3%', 17.3],
                        ['Korea 15.6%', 15.6],
                        ['Hong Kong 9.2%', 9.2],
                        ['India 7.8%', 7.8],
                        ['Taiwan 7.0%', 7.0],
                        ['Thailand 4.7%', 4.7],
                        ['Singapore 4.5%', 4.5],
                        ['Indonesia 4.2%', 4.2],
                        ['Other 5.5%', 5.5],
                    ]
                }]
        });
    </script>
    <script>
        function piechart1() {
            Highcharts.setOptions({
                colors: ['#91d9cf', '#54565a', '#879fc2', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
            });
            Highcharts.chart('container-221', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                legend: {
                    maxHeight: 70,
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 70,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                                name: '	Health Care',
                                y: 19,
                            }, {
                                name: 'Industrials',
                                y: 18
                            }, {
                                name: 'Utilities',
                                y: 18
                            }, {
                                name: 'Consumer Staples',
                                y: 12
                            }, {
                                name: 'Communication Services',
                                y: 8,
                            }, {
                                name: 'Real Estate',
                                y: 7
                            }, {
                                name: 'Materials',
                                y: 5
                            }, {
                                name: 'Information Technology',
                                y: 5
                            }, {
                                name: 'Consumer Discretionary 	',
                                y: 3,
                            }, {
                                name: 'Energy',
                                y: 2
                            }, {
                                name: 'Cash and cash equivalents',
                                y: 2
                            }, {
                                name: 'Financials',
                                y: 1
                            }]
                    }]
            });
            Highcharts.setOptions({
                colors: ['#93dacf', '#7ba0c4']
            });
            Highcharts.chart('container19', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 0,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            distance: 2,
                            connectorWidth: 0,
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                                name: '	Cash and cash equivalents',
                                y: 10
                            }, {
                                name: 'Australasian Equities ',
                                y: 90
                            }]
                    }]
            });
        }
    </script>
    <script>
        function piechart2() {
            Highcharts.setOptions({
                colors: ['#91d9cf', '#54565a', '#879fc2', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
            });
            Highcharts.chart('container-221', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                legend: {
                    maxHeight: 70,
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 70,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                                name: 'Diversified REITs',
                                y: 55,
                            }, {
                                name: 'Real Estate Operating Companies',
                                y: 15
                            }, {
                                name: 'Industrial REITs',
                                y: 8
                            }, {
                                name: 'Cash and cash equivalents',
                                y: 6
                            }, {
                                name: 'Specialised REITs',
                                y: 5,
                            }, {
                                name: 'Retail REITs ',
                                y: 4
                            }, {
                                name: 'Real Estate Development',
                                y: 3
                            }, {
                                name: 'Health Care',
                                y: 3
                            }, {
                                name: 'Office REITs',
                                y: 2,
                            }, {
                                name: 'Residential REITs',
                                y: 1
                            }]
                    }]
            });
            Highcharts.setOptions({
                colors: ['#93dacf', '#3b888a']
            });
            Highcharts.chart('container19', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 0,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            distance: 2,
                            connectorWidth: 0,
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                                name: 'Cash and cash equivalents',
                                y: 5
                            }, {
                                name: 'Listed property',
                                y: 95
                            }]
                    }]
            });
        }
    </script>
    <script>
        function piechart3() {
            Highcharts.setOptions({
                colors: ['#91d9cf', '#54565a', '#879fc2', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
            });
            Highcharts.chart('container-221', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                legend: {
                    maxHeight: 70,
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 70,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                                name: 'New Zealand fixed interest ',
                                y: 56,
                            }, {
                                name: 'Cash and cash equivalents',
                                y: 15
                            }, {
                                name: 'Listed property',
                                y: 12
                            }, {
                                name: 'International fixed interest',
                                y: 6
                            }, {
                                name: 'Australasian equities',
                                y: 6
                            }, {
                                name: 'International equities',
                                y: 5
                            }]
                    }]
            });
            Highcharts.setOptions({
                colors: ['#7ba0c4', '#3b888a', '#8f8d8c', '#c54b38', '#93dacf']
            });
            Highcharts.chart('container19', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                legend: {
                    maxHeight: 70,
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 0,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            distance: 2,
                            connectorWidth: 0,
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                                name: 'Cash and Cash Equivalents',
                                y: 5
                            }, {
                                name: 'Listed property',
                                y: 15
                            }, {
                                name: 'Fixed Interest',
                                y: 65
                            }, {
                                name: 'International Equities',
                                y: 10
                            }, {
                                name: 'Australasian equities',
                                y: 5
                            }]
                    }]
            });
            $('.risk-btns').removeClass('active');
            $('.addactive-cls').addClass('active');
        }
    </script>
    <script>
        function piechart4() {
            Highcharts.setOptions({
                colors: ['#91d9cf', '#54565a', '#879fc2', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
            });
            Highcharts.chart('container-221', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                legend: {
                    maxHeight: 70,
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 70,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                                name: 'International equities',
                                y: 59,
                            }, {
                                name: 'Australasian equities',
                                y: 14
                            }, {
                                name: 'New Zealand fixed interest',
                                y: 13
                            }, {
                                name: 'Cash and cash equivalents',
                                y: 10
                            }, {
                                name: 'Listed property ',
                                y: 4
                            }]
                    }]
            });
            Highcharts.setOptions({
                colors: ['#93dacf', '#3b888a', '#8f8d8c', '#c54b38', '#7ba0c4']
            });
            Highcharts.chart('container19', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                legend: {
                    maxHeight: 70,
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 0,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            distance: 2,
                            connectorWidth: 0,
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                                name: 'Cash and Cash Equivalents',
                                y: 5
                            }, {
                                name: 'Listed Property',
                                y: 5
                            }, {
                                name: 'Fixed Interest',
                                y: 15
                            }, {
                                name: 'International Equities',
                                y: 60
                            }, {
                                name: 'Australasian equities',
                                y: 15
                            }]
                    }]
            });
        }
    </script>
    <script>
        function areachart1(fundName) {
              
            Highcharts.chart('fundPerformanceChart1', {
                chart: {
                    type: 'area'
                },
                title: {
                    text: '$10,000 invested since inception '
                },
                subtitle: {

                },
                xAxis: {
//                    categories: [
//                        'Feb-07', 'Feb-08', 'Feb-09', 'Feb-10', 'Feb-11', 'Feb-12', 'Feb-13', 'Feb-14', 'Feb-15', 'Feb-16', 'Feb-17', 'Feb-18', 'Feb-19'
//                    ],
                    categories: performanceSinceinceptionDate,
                    //tickInterval: 1,
                    labels: {
                        style: {
                            color: '#333',
                            fontSize: '12px',
                            textTransform: 'uppercase'
                        },
                        //                    y: 20,
                        //                    x: 10
                    },
                    lineColor: '#dadada'
                },
                yAxis: {
                    min: 6000,
                    max: 33000,
                    tickInterval: 3000,
                    labels: {
                        format: '$ {value}',
                        //                    style: {
                        //                        color: Highcharts.getOptions().colors[2]
                        //                    }
                    },
                    title: {
                        text: ''
                    },
                    opposite: true
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'center',
                    verticalAlign: 'bottom',
                    enabled: true
                },
                plotOptions: {
                    series: {
                        color: '#93dbd0',
                        shadow: true,
                        lineWidth: 1,
                        marker: {
                            enabled: false
                        }
                    }
                },
                series: [{
                        name: fundName,
                        data: performanceSinceinceptionData         
                    }],
                responsive: {
                    rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                }

            });
                            console.log("hi");
        }
    </script>
    <script>
        function areachart2(fundName) {
            Highcharts.chart('fundPerformanceChart1', {
                chart: {
                    type: 'area'
                },
                title: {
                    text: '$10,000 invested since inception '
                },
                subtitle: {

                },
                xAxis: {
                    categories: [
                        'Dec-07', 'Dec-08', 'Dec-09', 'Dec-10', 'Dec-11', 'Dec-12', 'Dec-13', 'Dec-14', 'Dec-15', 'Dec-16', 'Dec-17', 'Dec-18'
                    ],
                    //tickInterval: 1,
                    labels: {
                        style: {
                            color: '#333',
                            fontSize: '12px',
                            textTransform: 'uppercase'
                        },
                        //                    y: 20,
                        //                    x: 10
                    },
                    lineColor: '#dadada'
                },
                yAxis: {
                    min: 6000,
                    max: 24000,
                    tickInterval: 2000,
                    labels: {
                        format: '$ {value}',
                        //                    style: {
                        //                        color: Highcharts.getOptions().colors[2]
                        //                    }
                    },
                    title: {
                        text: ''
                    },
                    opposite: true
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'center',
                    verticalAlign: 'bottom',
                    enabled: true
                },
                plotOptions: {
                    series: {
                        color: '#93dbd0',
                        shadow: true,
                        lineWidth: 1,
                        marker: {
                            enabled: false
                        }
                    }
                },
                series: [{
                        name: fundName,
                        data: [10000.00,
                            7270.00,
                            8200.00,
                            9309.60,
                            10229.45,
                            11229.45,
                            12035.03,
                            14060.47,
                            16469.66,
                            17275.78,
                            18603.49,
                            19575.7]
                    }],
                responsive: {
                    rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                }

            });
        }
    </script>
    <script>
        function areachart3(fundName) {
            Highcharts.chart('fundPerformanceChart1', {
                chart: {
                    type: 'area'
                },
                title: {
                    text: '$10,000 invested since inception '
                },
                subtitle: {

                },
                xAxis: {
                    categories: [
                        'Aug-14', 'Aug-15', 'Aug-16', 'Aug-17', 'Aug-18'
                    ],
                    //tickInterval: 1,
                    labels: {
                        style: {
                            color: '#333',
                            fontSize: '12px',
                            textTransform: 'uppercase'
                        },
                        //                    y: 20,
                        //                    x: 10
                    },
                    lineColor: '#dadada'
                },
                yAxis: {
                    min: 9000,
                    max: 13000,
                    tickInterval: 500,
                    labels: {
                        format: '$ {value}',
                        //                    style: {
                        //                        color: Highcharts.getOptions().colors[2]
                        //                    }
                    },
                    title: {
                        text: ''
                    },
                    opposite: true
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'center',
                    verticalAlign: 'bottom',
                    enabled: true
                },
                plotOptions: {
                    series: {
                        color: '#93dbd0',
                        shadow: true,
                        lineWidth: 1,
                        marker: {
                            enabled: false
                        }
                    }
                },
                series: [{
                        name: fundName,
                        data: [10000.00,
                            10570.00,
                            11509.60,
                            11729.45,
                            12335.03]
                    }],
                responsive: {
                    rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                }

            });
        }
    </script>
    <script>
        function areachart4(fundName) {
            Highcharts.chart('fundPerformanceChart1', {
                chart: {
                    type: 'area'
                },
                title: {
                    text: '$10,000 invested since inception '
                },
                subtitle: {

                },
                xAxis: {
                    categories: [
                        'Dec-18', 'Jan-19', 'Feb-19', 'Mar-19', 'Apr-19', 'May-19', 'Jun-19'
                    ],
                    //tickInterval: 1,
                    labels: {
                        style: {
                            color: '#333',
                            fontSize: '12px',
                            textTransform: 'uppercase'
                        },
                        //                    y: 20,
                        //                    x: 10
                    },
                    lineColor: '#dadada'
                },
                yAxis: {
                    min: 9000,
                    max: 11000,
                    tickInterval: 250,
                    labels: {
                        format: '$ {value}',
                        //                    style: {
                        //                        color: Highcharts.getOptions().colors[2]
                        //                    }
                    },
                    title: {
                        text: ''
                    },
                    opposite: true
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'center',
                    verticalAlign: 'bottom',
                    enabled: true
                },
                plotOptions: {
                    series: {
                        color: '#93dbd0',
                        shadow: true,
                        lineWidth: 1,
                        marker: {
                            enabled: false
                        }
                    }
                },
                series: [{
                        name: fundName,
                        data: [10000.00,
                            10070.00,
                            10200.00,
                            10259.60,
                            10429.45,
                            10535.03,
                            10555.03]
                    }],
                responsive: {
                    rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                }

            });
        }

    </script>
    <script>

        function updatePortfolio() {
            
                        $('.feature-text').find('p').removeAttr("contenteditable");
            var Portfolio =${pc};
            var FundOverview = $('.update-fund-pera').html();
            //        
            var MonthlyFundUpdate = $('#portfoliooverview').html();
            var FeaturesOftheFund = $('.feature-text').html();
            var monthlyReturnValue = $('.portheading').text();
            var fund = $('#fund-name').text();
            //            alert(fund);
            //            alert(JSON.stringify(FundOverview));
            //            alert(JSON.stringify(Portfolio));
            //            alert(JSON.stringify(MonthlyFundUpdate));
            //            alert(JSON.stringify(FeaturesOftheFund));
            $('form input[name=Portfolio]').val(Portfolio);
            $('form input[name=FundOverview]').val(FundOverview);
            $('form input[name=MonthlyFundUpdate]').val(MonthlyFundUpdate);
            $('form input[name=FeaturesOftheFund]').val(FeaturesOftheFund);
            $('form input[name=monthlyReturnValue]').val(monthlyReturnValue);
            $('form input[name=fundName]').val(fund);
            $("#myForm").submit();
        }
        ;

    </script>
    <script>

        function  updateFundPerformance() {
            var portfolio = '${pc}';
            var onemonth = $('#1-month').text();
            var onemonthTrending = $('.1-monthTrending').text();
            var threemonth = $('#3-month').text();
            var threemonthTrending = $('.3-monthTrending').text();
            var oneyear = $('#1-year').text();
            var oneyeartrending = $('.1-yearTrending').text();
            var fiveyear = $('#5-year').text();
            var unit_price = $('#fund-price').text();
            var fund_size = $('#fund-size').text();
            var one_year_return = $('#fund-return').text();
            var riskIndicator = $('.indicator-box .active').text();
            var obj = {portfolio: portfolio, onemonth: onemonth, riskIndicator: riskIndicator, onemonthTrending: onemonthTrending, threemonth: threemonth, threemonthTrending: threemonthTrending, oneyear: oneyear, oneyeartrending: oneyeartrending, fiveyear: fiveyear, unit_price: unit_price, fund_size: fund_size, one_year_return: one_year_return};
            console.log(JSON.stringify(obj));
            //            alert(JSON.stringify(obj));
            $.ajax({
                type: 'POST',
                url: './rest/groot/db/api/update-FundPerformance',
                headers: {"Content-Type": 'application/json'},
                data: JSON.stringify(obj),
                success: function (data, textStatus, jqXHR) {
                    alert("sucess");
                    location.reload();


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("error");
                }
            });

        }
        ;
        //        function  updateCurrentFundPerformance() {
        //            var portfolio = '${pc}';
        //            var unit_price = $('#fund-price').text();
        //            var fund_size = $('#fund-size').find('span').text();
        //            var one_year_return = $('#fund-return').text();
        //
        //            var obj = {portfolio: portfolio, unit_price: unit_price, fund_size: fund_size, one_year_return: one_year_return};
        //            console.log(JSON.stringify(obj));
        //            $.ajax({
        //                type: 'POST',
        //                url: './rest/groot/db/api/update-CurrentFundPerformance',
        //                headers: {"Content-Type": 'application/json'},
        //                data: JSON.stringify(obj),
        //                success: function (data, textStatus, jqXHR) {
        //                    alert("sucess");
        //
        //
        //                },
        //                error: function (jqXHR, textStatus, errorThrown) {
        //                    alert("error");
        //                }
        //            });
        //
        //        }
        //        ;

    </script>
    <script>
        var funddetails = new Map();

        $(document).ready(function () {

            $('.feature-text').find('p').attr("contenteditable", "true");

            $.ajax({
                type: 'GET',
                url: './rest/groot/db/api/get-FundPerformance-${pc}',
                headers: {"Content-Type": 'application/json'},
                success: function (data, textStatus, jqXHR) {
                    console.log("sucess" + data);
                    var obj = JSON.parse(data);
                    //                    alert(JSON.stringify(obj));
                    console.log("obj" + obj);
                    funddetails = obj;
                    //                    alert(JSON.stringify(funddetails));

                    $('#1-month').text(funddetails.onemonth);
                    $('.1-monthTrending').text(funddetails.onemonthTrending);
                    $('#3-month').text(funddetails.threemonth);
                    $('.3-monthTrending').text(funddetails.threemonthTrending);
                    $('#1-year').text(funddetails.oneyear);
                    $('.1-yearTrending').text(funddetails.oneyeartrending);
                    $('#5-year').text(funddetails.fiveyear);
                    $('#fund-price').text(funddetails.unit_price);
                    $('#fund-size').text(funddetails.fund_size);
                    $('#fund-return').text(funddetails.one_year_return);
                    $('.risk-btns').each(function (i) {
                        var statsValue = $(this).text();
                        if (statsValue === funddetails.riskIndicator) {
                            $(this).addClass("active");
                        }
                    });

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("error");
                }
            });



        });



        $(".indicator-box span").on("click", function () {
            $(".indicator-box span").removeClass("active");
            $(this).addClass("active");
        });



    </script>


</body>
</html>