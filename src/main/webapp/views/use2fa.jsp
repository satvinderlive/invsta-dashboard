<%-- 
    Document   : temp2FA
    Created on : 27 Sep, 2019, 9:39:33 AM
    Author     : innovative002
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--<c:set var="home" value="${pageContext.request.contextPath}"/>--%>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>westpac</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="http://backoffice.invsta.io/mint/images/mint.png" rel="shortcut icon"/>
        <link href="http://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <!--<link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>-->
        <link href="http://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>
        <!--<link href="http://backoffice.invsta.io/pocv/resources/css/main.css?version=3.5.1" rel="stylesheet"/>-->
        <link href="http://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link href="./resources/css/custom.css" rel="stylesheet"/>
        <link rel="stylesheet" href="./resources/css/signup/intlTelInput.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
        <!--  <link href="resources/css/osfonts.css" rel="stylesheet"> -->
        <!--<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css'>-->
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->


        <style>
            .profile-weight {
                font-weight: 500;
            }
            .tabpill-margin {
                margin-top: 20px;
            }
            .image-select {
                width: 100%;
                font-size: 13px;
                position: relative;
                top: -5px;
            }
            .profile-pic {
                max-width: 200px;
                max-height: 200px;
                display: block;
                width: 100%;
                height: 100%;
                object-fit: cover;
                border-radius: 50%;
            }

            .file-upload {
                display: none;
            }
            .profile-circle {
                border-radius: 1000px !important;
                overflow: hidden;
                width: 200px;
                height: 200px;
                border: 8px solid rgb(147, 219, 208);
                max-width: 200px;
                margin: auto;

            }

            .upload-button {
                font-size: 1.2em;
            }

            .upload-button:hover {
                transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
                color: #999;
            }
            .emp-profile {
                padding: 3%;
                margin-top: 3%;
                margin-bottom: 3%;
                border-radius: 0.5rem;
                background: #fff;
            }
            /*start setting checkbox*/
            .new-notification .custom-check {
                display: block;
                position: relative;
                padding-left: 35px;
                margin-bottom: 12px;
                cursor: pointer;
                font-size: 16px;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }
            .new-notification .custom-check input {
                position: absolute;
                opacity: 0;
                cursor: pointer;
                height: 0;
                width: 0;
            }
            .new-notification .checkmark {
                position: absolute;
                top: 0;
                left: 0;
                height: 23px;
                width: 23px;
                background-color: #eee;
            }
            .new-notification .custom-check:hover input ~ .checkmark {
                background-color: #ccc;
            }
            .new-notification .custom-check input:checked ~ .checkmark {
                background-color: #93dbd0;
            }
            .new-notification .checkmark:after {
                content: "";
                position: absolute;
                display: none;
            }
            .new-notification .custom-check input:checked ~ .checkmark:after {
                display: block;
            }
            .new-notification .custom-check .checkmark:after {
                left: 8px;
                top: 4px;
                width: 8px;
                height: 12px;
                border: solid white;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }
            .color-take {
                background: #e9eaed;
                width: 50px;
                height: 45px;
                cursor: pointer;
                max-width: 90%;
            }
            .color-line {
                display: flex;
                margin-bottom: 15px;
            }
            .color-red {
                background: red;
            }
            .color-green {
                background: green;
            }
            .color-blue {
                background: blue;
            }
            .color-yellow-red {
                background: #dc7b17;
            }
            .color-purple {
                background: purple;
            }
            .color-pink {
                background: pink;
            }
            .vc-chrome {
                position: absolute;
                top: 35px;
                right: 0;
                z-index: 9;
                width: 100%
            }
            .current-color {
                display: inline-block;
                width: 16px;
                height: 16px;
                background-color: #000;
                cursor: pointer;
            }
            .submit_btn_new {
                display: table;
                margin-left: auto;
            }
            /*end setting checkbox*/
            .night-mode{
                filter: invert(100%);
                background-color: #000;
            }

            .night-mode img {
                filter: invert(100%);
            }
            .day-night-mode a {
                color: #fff!important;
                cursor: pointer;
            }

        </style>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <%--<jsp:include page="header.jsp" />--%>  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="./home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Profile</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)"></a>
                        </li>
                    </ul>
                

                    
                 
                    <div class="content-i">
                        <div class="row">
                      <form id="2faAuthentication" action="./use2fa" method="POST" class="form-login">
                    <fieldset>
                        <h6 class="${classname}">
                            ${message}
                        </h6>
                        <div class="form-group">
                            <label for="2facode">2FA Code</label>
                            <input type="text" class="form-control form-control-style" id="code" name="code" placeholder="Enter your code" >
                            <p class="text-muted col-xs-10">Use Google Authenticator app on your phone to obtain the verification Code</p>
                            <div class="pre-icon os-icon os-icon-fingerprint"></div>                         
                        </div> 
                        <div class="buttons-w">  
                            <button class="btn btn-primary btn-style" id="buttonload">
                                <span>Verify</span>
                            </button>
                        </div>
                    </fieldset> 
                      </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="display-type"></div>
        <jsp:include page = "../views/models/updateBankAccount.jsp"></jsp:include>
        <jsp:include page = "../views/models/updatePhoneNumber.jsp"></jsp:include>
        <jsp:include page = "../views/models/updateAddress.jsp"></jsp:include>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <script src="http://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
            <script src="./resources/js/intlTelInput_1.js"></script>
            <!-- <script src="resources/bower_components/moment/moment.js" ></script>
            <script src="./resources/js/intlTelInput_1.js"></script>
            <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
            <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
            <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

            <script src="http://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
            <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
            <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
            <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
            <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
            <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
            <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
            <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
            <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
            <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
            <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
            <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
            <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
            <script src="http://backoffice.invsta.io/ui-fund/resources/bower_components/bootstrap/js/dist/util.js" defer></script>
            <script src="http://backoffice.invsta.io/ui-fund/resources/bower_components/bootstrap/js/dist/tab.js" defer></script>

            <script src="http://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
            <script src="http://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
            <script src="http://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
            <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
            <script src="https://code.highcharts.com/highcharts.js" ></script> 
            <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
            <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
            <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
            <script src="https://code.highcharts.com/modules/cylinder.js"></script>
            <script src='https://cdnjs.cloudflare.com/ajax/libs/vue/2.3.4/vue.min.js'></script>
            <script src='https://unpkg.com/vue-color/dist/vue-color.min.js'></script>

            <script>
                $(document).ready(function () {
                });

                $('#qr-profile').click(function () {
                    $("#qr").show();
                    getQRCode('${userInfo.email}');
                });



                function getQRCode(email) {
                    $.get("./code?username=" + email, function (data) {
                        console.log(data);
                        $("#barcode").append('<img id="barcodeurl" src="' + data.url + '" />');
                        $("#barcode").append('<span class="barcodelabel"> 16-Digit key: </span>');
                        $("#barcode").append('<span class="barcode_digits">' + data.key + '</span>');
                    });
                }

        </script>      




    </body>
</html>