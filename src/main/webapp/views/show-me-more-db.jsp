<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <!--<link href="resources/images/mint.png" rel="shortcut icon"/>-->
        <link rel="icon" href="./resources/images/invsta-color-logo.png" type="image/gif" sizes="16x16">
        <link href="https://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <!--<link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>-->
        <link href="https://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>
        <!--<link href="https://backoffice.invsta.io/pocv/resources/css/main.css?version=3.5.1" rel="stylesheet"/>-->
        <link href="https://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.css">
        <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.css">
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link href="./resources/css/custom.css" rel="stylesheet"/>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">-->
        <link href="./resources/css/sweetalert.css" rel="stylesheet" />
        <script>
            !function(){var analytics = window.analytics = window.analytics || []; if (!analytics.initialize)if (analytics.invoked)window.console && console.error && console.error("Segment snippet included twice."); else{analytics.invoked = !0; analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"]; analytics.factory = function(t){return function(){var e = Array.prototype.slice.call(arguments); e.unshift(t); analytics.push(e); return analytics}}; for (var t = 0; t < analytics.methods.length; t++){var e = analytics.methods[t]; analytics[e] = analytics.factory(e)}analytics.load = function(t, e){var n = document.createElement("script"); n.type = "text/javascript"; n.async = !0; n.src = ("https:" === document.location.protocol?"https://":"https://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js"; var o = document.getElementsByTagName("script")[0]; o.parentNode.insertBefore(n, o); analytics._loadOptions = e}; analytics.SNIPPET_VERSION = "4.1.0";
            analytics.load("yqEQxH2Ri5xkKD71qxtWFFkjEfox60zX");
            analytics.page();
            }}();
        </script>
        <script>
            (function(h, o, t, j, a, r){
            h.hj = h.hj || function(){(h.hj.q = h.hj.q || []).push(arguments)};
            h._hjSettings = {hjid:846291, hjsv:6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
        <!-- <script>
                window.intercomSettings = {
                app_id: "q28x66d9"
                };
                </script>
                <script>
                (function(){var w = window; varic = w.Intercom; if (typeof ic === "function"){ic('reattach_activator'); ic('update', intercomSettings); } else{var d = document; var i = function(){i.c(arguments)}; i.q = []; i.c = function(args){i.q.push(args)}; w.Intercom = i; function l(){var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/q28x66d9'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); }if (w.attachEvent){w.attachEvent('onload', l); } else{w.addEventListener('load', l, false); }}})()
        </script>  -->
        <style>
            div#heat-map-1 .highcharts-point-hover {
                padding: 10px!important;
                box-shadow: 0 0 14px 10px black;
                stroke: #d0c9c970!important;
                stroke-width: 5px!important;
            }
            .el-chart-w {
                overflow: visible!important;
            }
            .portfolio_overlay {
                height: 75%;
            }
            .more_fund tr:nth-child(odd) {
                background-color: #dddddd;
            }
            .row.performance-inline {
                display: flex;
            }

            .col-md-3.col-sm-3.five-div {
                flex: 1;
            }

            span.risk-btns .active {
                background: red;
            }
            span.risk-btns.active {
                /*                background: #4564d0;*/
                color: #fff;
                background: #65b9ac;
            }
            .data-funds::-webkit-scrollbar {
                width: 7px;
            }
            a.fund-detail-btn {
                /*                background: #2e5ea5;*/
                background: #65b9ac;
                padding: 8px 10px;
                color: #fff;
                border-radius: 3px;
                font-weight: 400;
                font-size: 13px;
            }
            .bootstrap3 .fixed-table-toolbar {
                display: none;
            }
            .value.percentage.founder-icon-color {
                color: #0f9d58;
                font-size: 18px;
                margin-left: 10px;
                margin-right: 24px;
            }

            .value.marketValue {
                float: initial!important;
            }
            table.table-value-footer th {
                padding: 0.75rem;
                width: 35%;
            }
            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('./resources/images/Preloader_3.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }

        </style>
        <style>
            .selectedbank{
                background-color: #65b9ac;
            }
        </style>
    </head>
    <body>
        <!--<div class="loader"></div>-->
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="">
                            <span><a href="./home">Home</a></span>
                        </li>
                        <li class="">
                            <span class="right-mark"><i class="fa fa-caret-right"></i></span>
                        </li>
                        <li class="">
                            <span><a href="">PRIVATE PORTFOLIO OVERVIEW</a></span>
                        </li>

                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="element-wrapper">
                                        <h6>Hi John, here’s an overview of your Private Portfolio.</h6>
                                        <br>
                                        <!--                                        <h6 class="element-header" id="fundName"></h6>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="element-box">
                                        <div class="os-tabs-w">
                                            <div class="os-tabs-controls">
                                                <ul class="nav nav-tabs smaller">
                                                    <!--  <li class="nav-item">
                                                            <a class="nav-link active" data-toggle="tab" href="#tab-live">Live</a>
                                                    </li> -->
                                                    <li class="nav-item">
                                                        <a class="nav-link active" data-toggle="tab" href="#tab-latestweek">Investment Performance</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane active">
                                                    <div class="" style="padding:0 0 10px;width: 100%;float: left; display:flex; justify-content:space-between" >
                                                        <div class="contribution_value">
                                                            <div class="label lbl-main">
                                                                Contributions 
                                                                <!--<div class="pulsating-circle new-circle-1" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom"></div>-->
                                                            </div>
                                                            <div class="value  add-value">$3,800,000 
                                                            </div>
                                                        </div>
                                                        <div class="current_value">
                                                            <div class="label  lbl-main">
                                                                CURRENT Value
                                                                <!--<div class="pulsating-circle new-circle-2" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom"></div>-->
                                                            </div>
                                                            <div class="value  add-value">
                                                                $4,500,376 
                                                                <!-- <span class="founder-icon-color">16.46%  <i class="fa fa-arrow-up" aria-hidden="true"></i></span> -->
                                                            </div>
                                                            <div class="value percentage founder-icon-color">
                                                                18.4%
                                                                <!--<div class="pulsating-circle new-circle-1 check-info" data-toggle="tooltip" title="This is an annualised* money weighted** return after fees and tax, since you have been invested***.The return weights how long each amount has been invested for by how much has been invested to work out the average years each dollar has been invested for. If you have been invested for less than one year, then the return is not annualised.**The return takes into account when your cash flows occur and how big they are.***The return is calculated on all funds that you are currently invested in and any funds that you previously held.Past performance is not a reliable indicator of future returns." data-placement="right" style="vertical-align:text-bottom"></div>--> 
                                                                <!-- <span class="founder-icon-color">16.46%  <i class="fa fa-arrow-up" aria-hidden="true"></i></span> -->
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div id="stockbalance" style="min-width: 100%; height: 270px; margin: 0 auto;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row performance-inline">
                                <div class="col-md-4">
                                    <div class="element-wrapper">
                                        <h6 class="new-design-hearder">Asset allocation: Current </h6>
                                    </div>
                                    <div class="element-box">
                                        <div class="el-chart-w" id="container19" style="height:448px"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="element-wrapper">
                                        <h6 class="new-design-hearder">Asset allocation: Benchmark</h6>
                                    </div>
                                    <div class="element-box">
                                        <div class="el-chart-w" id="container-221" style="height:448px"></div>
                                    </div>
                                </div>  
                                <div class="col-md-4">
                                    <div class="element-wrapper">
                                        <h6 class="new-design-hearder">Performance</h6>
                                    </div>
                                    <div class="element-box22">
                                        <div class="card tab-card private-tab-card">
                                            <div class="card-header tab-card-header private-tab">
                                                <ul class="nav nav-tabs smaller">
                                                    <li class="nav-item">
                                                        <a class="nav-link active show" data-toggle="tab" href="#tab_overview-cons-invs-pieChart" aria-expanded="true">Last Year
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#tab_overview-cons-invs-pieChart2" aria-expanded="true">2 Years ago
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#tab_overview-cons-invs-pieChart3" aria-expanded="true">3 Years ago
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#tab_overview-cons-invs-pieChart4" aria-expanded="true">Average Return
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="tab-content" id="myTabContent">
                                                <div class="tab-pane active" id="tab_overview-cons-invs-pieChart" aria-expanded="true" style="">
                                                    <div class=" el-tablo">

                                                        <div id="invest-bar-1" style="height: 409px;"></div>
                                                        <div id="legend-1"></div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_overview-cons-invs-pieChart2" aria-expanded="true" style="">
                                                    <div class=" el-tablo">

                                                        <div id="invest-bar-2" style="height: 409px;"></div>
                                                        <div id="legend-1"></div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_overview-cons-invs-pieChart3" aria-expanded="true" style="">
                                                    <div class=" el-tablo">

                                                        <div id="invest-bar-3" style="height: 409px;"></div>
                                                        <div id="legend-1"></div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_overview-cons-invs-pieChart4" aria-expanded="true" style="">
                                                    <div class=" el-tablo">

                                                        <div id="invest-bar-4" style="height: 409px;"></div>
                                                        <div id="legend-1"></div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="element-wrapper">
                                        <h6 class="new-design-hearder">Asset allocation & Performance </h6>
                                    </div>
                                    <div class="element-box">
                                        <div id="heat-map-1"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="element-wrapper">
                                        <h6 class="new-design-hearder">Geographical Allocation</h6>
                                    </div>
                                    <div class="element-box">
                                        <div id="countryhighchart"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="element-box">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span class="list-tabl-1">
                                                    <div class="tabl-btn">
                                                        List of Assets
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="col-md-3">
                                                <span class="list-tabl-2">
                                                    <div class="tabl-btn">
                                                        Performance by Asset
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="col-md-3">
                                                <span class="list-tabl-3">
                                                    <div class="tabl-btn">
                                                        Transactions by Asset
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="col-md-3">
                                                <span class="list-tabl-4">
                                                    <div class="tabl-btn">
                                                        Forecast Income
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="list-tabl-toggle-1 row">
                                            <div class="col-md-12">
                                                <table class="table table-bordered">
                                                    <thead class="tabl-color">
                                                        <tr>
                                                            <th></th>
                                                            <th>PORTFOLIO</th>
                                                            <th>BENCHMARK</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Average Daily Return</td>
                                                            <td>0.32%</td>
                                                            <td>-0.07%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ave winning day</td>
                                                            <td>2.00%</td>
                                                            <td>2.23%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ave losing day</td>
                                                            <td>-1.84%</td>
                                                            <td>-2.34%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Winning days %</td>
                                                            <td>47.96%</td>
                                                            <td>49.78%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Max Daily Return</td>
                                                            <td>23.00%</td>
                                                            <td>14.06%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Max Daily DD</td>
                                                            <td>-10.00%</td>
                                                            <td>-10.84%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Std Dev Daily Returns</td>
                                                            <td>3.14%</td>
                                                            <td>3.24%</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div> 
                                        <div class="list-tabl-toggle-2 row">
                                            <div class="col-md-12">
                                                <table class="table table-bordered">
                                                    <thead class="tabl-color">
                                                        <tr>
                                                            <th></th>
                                                            <th>PORTFOLIO</th>
                                                            <th>BENCHMARK</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Average Daily Return</td>
                                                            <td>0.32%</td>
                                                            <td>-0.07%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ave winning day</td>
                                                            <td>2.00%</td>
                                                            <td>2.23%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ave losing day</td>
                                                            <td>-1.84%</td>
                                                            <td>-2.34%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Winning days %</td>
                                                            <td>47.96%</td>
                                                            <td>49.78%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Max Daily Return</td>
                                                            <td>23.00%</td>
                                                            <td>14.06%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Max Daily DD</td>
                                                            <td>-10.00%</td>
                                                            <td>-10.84%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Std Dev Daily Returns</td>
                                                            <td>3.14%</td>
                                                            <td>3.24%</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="list-tabl-toggle-3 row">
                                            <div class="col-md-12">
                                                <table class="table table-bordered">
                                                    <thead class="tabl-color">
                                                        <tr>
                                                            <th></th>
                                                            <th>PORTFOLIO</th>
                                                            <th>BENCHMARK</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Average Daily Return</td>
                                                            <td>0.32%</td>
                                                            <td>-0.07%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ave winning day</td>
                                                            <td>2.00%</td>
                                                            <td>2.23%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ave losing day</td>
                                                            <td>-1.84%</td>
                                                            <td>-2.34%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Winning days %</td>
                                                            <td>47.96%</td>
                                                            <td>49.78%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Max Daily Return</td>
                                                            <td>23.00%</td>
                                                            <td>14.06%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Max Daily DD</td>
                                                            <td>-10.00%</td>
                                                            <td>-10.84%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Std Dev Daily Returns</td>
                                                            <td>3.14%</td>
                                                            <td>3.24%</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="list-tabl-toggle-4 row">
                                            <div class="col-md-12">
                                                <table class="table table-bordered">
                                                    <thead class="tabl-color">
                                                        <tr>
                                                            <th></th>
                                                            <th>PORTFOLIO</th>
                                                            <th>BENCHMARK</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Average Daily Return</td>
                                                            <td>0.32%</td>
                                                            <td>-0.07%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ave winning day</td>
                                                            <td>2.00%</td>
                                                            <td>2.23%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ave losing day</td>
                                                            <td>-1.84%</td>
                                                            <td>-2.34%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Winning days %</td>
                                                            <td>47.96%</td>
                                                            <td>49.78%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Max Daily Return</td>
                                                            <td>23.00%</td>
                                                            <td>14.06%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Max Daily DD</td>
                                                            <td>-10.00%</td>
                                                            <td>-10.84%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Std Dev Daily Returns</td>
                                                            <td>3.14%</td>
                                                            <td>3.24%</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!--<div class="display-type"></div>-->
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" defer></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script>
        <!--               <script src="https://code.highcharts.com/highcharts.js" ></script> -->

        <!--        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>-->
        <!--<script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>-->
        <!--<script src="https://code.highcharts.com/maps/highmaps.js"></script>-->
        <script src="https://code.highcharts.com/maps/modules/map.js"></script>

        <script src="https://code.highcharts.com/maps/modules/data.js"></script>
        <script src="https://code.highcharts.com/mapdata/custom/world.js"></script>
        <!--<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>-->
        <!--<script src="https://code.highcharts.com/maps/modules/offline-exporting.js"></script>-->


        <script src="https://code.highcharts.com/modules/heatmap.js"></script>
        <script src="https://code.highcharts.com/modules/treemap.js"></script>       




        <script th:inline="javascript">
//            $(window).load(function () {
//            $(".loader").fadeOut("slow");
//            });
        </script>

        <script>
            // Create a timer
//                    function perfomancechart(arr, year, month, day) {
            // Create the chart
            Highcharts.stockChart('stockbalance', {
            chart: {
            zoomType: 'xy',
                    type: 'spline',
                    events: {
                    load: function () {
                    if (!window.TestController) {
                    this.setTitle(null, {
                    text: ''
                    });
                    }
                    }
                    },
            },
                    rangeSelector: {
                    allButtonsEnabled: true,
                            buttons: [{
                            type: 'day',
                                    count: 3,
                                    text: '3d'
                            }, {
                            type: 'week',
                                    count: 1,
                                    text: '1w'
                            }, {
                            type: 'month',
                                    count: 1,
                                    text: '1m'
                            }, {
                            type: 'month',
                                    count: 6,
                                    text: '6m'
                            }, {
                            type: 'year',
                                    count: 1,
                                    text: '1y'
                            }, {
                            type: 'all',
                                    text: 'All'
                            }],
                    },
                    yAxis: {
                    title: {
                    text: ''
                    }
                    },
                    title: {
                    text: ''
                    },
                    credits: {
                    enabled: false
                    },
                    exporting: {
                    enabled: false
                    },
                    subtitle: {
                    text: ''
                    },
                    series : [{
                    name: 'US$',
                            color: '#2c94ec',
                            data : [
                            [1346450400000, 67.79],
                            [1349042400000, 47.79],
                            [1351724400000, 57.79],
                            [1354316400000, 27.79],
                            [1356994800000, 67.79],
                            [1359673200000, 37.79],
                            [1362092400000, 61.79],
                            [1364767200000, 67.79],
                            [1367359200000, 64.79],
                            [1370037600000, 56.79],
                            [1372629600000, 59.79],
                            [1375308000000, 23.79],
                            [1377986400000, 40.79],
                            [1380578400000, 24.79],
                            [1383260400000, 60.98],
                            [1385852400000, 71.26]
                            ],
//                            pointStart: Date.UTC("2016", "11", "29"),
//                            pointEnd: Date.UTC("2017", "07", "29"),
//                                    pointInterval: 1215 * 60 * 60 * 24 * 30,
//                            pointInterval: 1200 * 60 * 60 * 24,
                    }]

            });
//                    }
        </script>

        <script>
            // Build the chart
            Highcharts.setOptions({
            colors: ['#575f65', '#d7d2cb', '#2c94ec', '#9f4585', '#2d373e', '#621a4b']
            });
            Highcharts.chart('container19', {
            chart: {
            plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
            },
                    title: {
                    text: ''
                    },
                    tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    credits: {
                    enabled: false,
                    },
                    navigation: {
                    buttonOptions: {
                    enabled: false
                    }
                    },
                    plotOptions: {
                    pie: {
                    innerSize: 170,
                            depth: 45,
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                            distance: 2,
                                    connectorWidth: 0,
                                    enabled: false,
                                    format: '{point.percentage:.1f} %'
                            },
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                            enabled: false
                            },
                            showInLegend: true
                    }
                    },
                    series: [{
                    name: 'Allocation',
                            colorByPoint: true,
                            data: [{
                            name: 'International Equities ',
                                    y: 70
                            }, {
                            name: 'Cash & Equivalents ',
                                    y: 10
                            }, {
                            name: 'Australasian Equities',
                                    y: 10
                            }, {
                            name: 'Property ',
                                    y: 10
                            }]
                    }]
            });
        </script>
        <script>
            // Build the chart
            Highcharts.setOptions({
            colors: ['#575f65', '#d7d2cb', '#2c94ec', '#9f4585', '#2d373e', '#621a4b']
            });
            Highcharts.chart('container-221', {
            chart: {
            plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
            },
                    title: {
                    text: ''
                    },
                    tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    credits: {
                    enabled: false,
                    },
                    navigation: {
                    buttonOptions: {
                    enabled: false
                    }
                    },
                    plotOptions: {
                    pie: {
                    innerSize: 170,
                            depth: 45,
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                            distance: 2,
                                    connectorWidth: 0,
                                    enabled: false,
                                    format: '{point.percentage:.1f} %'
                            },
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                            enabled: false
                            },
                            showInLegend: true
                    }
                    },
                    series: [{
                    name: 'Allocation',
                            colorByPoint: true,
                            data: [{
                            name: 'International Equities ',
                                    y: 10
                            }, {
                            name: 'Cash & Equivalents ',
                                    y: 10
                            }, {
                            name: 'Australasian Equities',
                                    y: 50
                            }, {
                            name: 'Property ',
                                    y: 10
                            }, {
                            name: 'New Zealand Fixed Interest  ',
                                    y: 10
                            }, {
                            name: 'Global Fixed Interest  ',
                                    y: 10
                            }]
                    }]
            });
        </script>
        <script>
            Highcharts.setOptions({
            // colors: totalFmcaInvestmentMixcolorCode
            colors: ['#575f65', '#d7d2cb', '#2c94ec', '#9f4585', '#2d373e', '#621a4b']
            });
            Highcharts.chart('invest-bar-1', {
            chart: {
            type: 'column'
            },
                    title: {
                    text: ''
                    },
                    subtitle: {
                    text: ''
                    },
                    xAxis: {
                    categories: [
                            ''
                    ],
                            crosshair: true
                    },
                    yAxis: {
                    labels: {
                    formatter: function () {
                    return  this.axis.defaultLabelFormatter.call(this) + '%';
                    }
                    },
                            min: 0,
                            max: 30,
                            title: {
                            text: ''
                            }


                    },
                    tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                    },
                    credits: {
                    enabled: false
                    },
                    navigation: {
                    buttonOptions: {
                    enabled: false
                    }
                    },
                    plotOptions: {
                    column: {
                    pointPadding: 0.2,
                            borderWidth: 0
                    }
                    },
                    series: [{
                    name: 'International Equities',
                            data: [27]

                    }, {
                    name: 'Cash & Equivalents',
                            data: [17]

                    }, {
                    name: 'Australasian Equities',
                            data: [23]

                    }, {
                    name: 'Summary',
                            data: [17]

                    }]
            });
        </script>
        <script>
            Highcharts.setOptions({
            // colors: totalFmcaInvestmentMixcolorCode
            colors: ['#575f65', '#d7d2cb', '#b6000b', '#9f4585', '#2d373e', '#621a4b']
            });
            Highcharts.chart('invest-bar-2', {
            chart: {
            type: 'column'
            },
                    title: {
                    text: ''
                    },
                    subtitle: {
                    text: ''
                    },
                    xAxis: {
                    categories: [
                            ''
                    ],
                            crosshair: true
                    },
                    yAxis: {
                    labels: {
                    formatter: function () {
                    return  this.axis.defaultLabelFormatter.call(this) + '%';
                    }
                    },
                            min: 0,
                            max: 30,
                            title: {
                            text: ''
                            }


                    },
                    tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                    },
                    credits: {
                    enabled: false
                    },
                    navigation: {
                    buttonOptions: {
                    enabled: false
                    }
                    },
                    plotOptions: {
                    column: {
                    pointPadding: 0.2,
                            borderWidth: 0
                    }
                    },
                    series: [{
                    name: 'International Equities',
                            data: [10]

                    }, {
                    name: 'Cash & Equivalents',
                            data: [25]

                    }, {
                    name: 'Australasian Equities',
                            data: [40]

                    }, {
                    name: 'Summary',
                            data: [25]

                    }]
            });
        </script>
        <script>
            Highcharts.setOptions({
            // colors: totalFmcaInvestmentMixcolorCode
            colors: ['#575f65', '#d7d2cb', '#b6000b', '#9f4585', '#2d373e', '#621a4b']
            });
            Highcharts.chart('invest-bar-3', {
            chart: {
            type: 'column'
            },
                    title: {
                    text: ''
                    },
                    subtitle: {
                    text: ''
                    },
                    xAxis: {
                    categories: [
                            ''
                    ],
                            crosshair: true
                    },
                    yAxis: {
                    labels: {
                    formatter: function () {
                    return  this.axis.defaultLabelFormatter.call(this) + '%';
                    }
                    },
                            min: 0,
                            max: 30,
                            title: {
                            text: ''
                            }


                    },
                    tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                    },
                    credits: {
                    enabled: false
                    },
                    navigation: {
                    buttonOptions: {
                    enabled: false
                    }
                    },
                    plotOptions: {
                    column: {
                    pointPadding: 0.2,
                            borderWidth: 0
                    }
                    },
                    series: [{
                    name: 'International Equities',
                            data: [25]

                    }, {
                    name: 'Cash & Equivalents',
                            data: [45]

                    }, {
                    name: 'Australasian Equities',
                            data: [20]

                    }, {
                    name: 'Summary',
                            data: [10]

                    }]
            });
        </script>
        <script>
            Highcharts.setOptions({
            // colors: totalFmcaInvestmentMixcolorCode
            colors: ['#575f65', '#d7d2cb', '#b6000b', '#9f4585', '#2d373e', '#621a4b']
            });
            Highcharts.chart('invest-bar-4', {
            chart: {
            type: 'column'
            },
                    title: {
                    text: ''
                    },
                    subtitle: {
                    text: ''
                    },
                    xAxis: {
                    categories: [
                            ''
                    ],
                            crosshair: true
                    },
                    yAxis: {
                    labels: {
                    formatter: function () {
                    return  this.axis.defaultLabelFormatter.call(this) + '%';
                    }
                    },
                            min: 0,
                            max: 30,
                            title: {
                            text: ''
                            }


                    },
                    tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                    },
                    credits: {
                    enabled: false
                    },
                    navigation: {
                    buttonOptions: {
                    enabled: false
                    }
                    },
                    plotOptions: {
                    column: {
                    pointPadding: 0.2,
                            borderWidth: 0
                    }
                    },
                    series: [{
                    name: 'International Equities',
                            data: [40]

                    }, {
                    name: 'Cash & Equivalents',
                            data: [25]

                    }, {
                    name: 'Australasian Equities',
                            data: [10]

                    }, {
                    name: 'Summary',
                            data: [25]

                    }]
            });
        </script>
        <script>
            $(".list-tabl-toggle-1").hide("slow");
            $(".list-tabl-toggle-2").hide("slow");
            $(".list-tabl-toggle-3").hide("slow");
            $(".list-tabl-toggle-4").hide("slow");
            $(document).ready(function(){
            $(".list-tabl-1").click(function(){
            $(".list-tabl-toggle-1").toggle("slow");
            $(".list-tabl-toggle-2").hide("slow");
            $(".list-tabl-toggle-3").hide("slow");
            $(".list-tabl-toggle-4").hide("slow");
            $('html, body').animate({scrollTop: $(".list-tabl-1").offset().top}, 1000);
            });
            $(".list-tabl-2").click(function(){
            $(".list-tabl-toggle-1").hide("slow");
            $(".list-tabl-toggle-2").toggle("slow");
            $(".list-tabl-toggle-3").hide("slow");
            $(".list-tabl-toggle-4").hide("slow");
            $('html, body').animate({scrollTop: $(".list-tabl-2").offset().top}, 1000);
            });
            $(".list-tabl-3").click(function(){
            $(".list-tabl-toggle-1").hide("slow");
            $(".list-tabl-toggle-2").hide("slow");
            $(".list-tabl-toggle-3").toggle("slow");
            $(".list-tabl-toggle-4").hide("slow");
            $('html, body').animate({scrollTop: $(".list-tabl-3").offset().top}, 1000);
            });
            $(".list-tabl-4").click(function(){
            $(".list-tabl-toggle-1").hide("slow");
            $(".list-tabl-toggle-2").hide("slow");
            $(".list-tabl-toggle-3").hide("slow");
            $(".list-tabl-toggle-4").toggle("slow");
            $('html, body').animate({scrollTop: $(".list-tabl-4").offset().top}, 1000);
            });
            });
        </script>
        <script>
            Highcharts.chart('heat-map-1', {
            colorAxis: {
            minColor: '#84060e',
                    maxColor: Highcharts.getOptions().colors[0]
            },
                    credits: {
                    enabled: false
                    },
                    navigation: {
                    buttonOptions: {
                    enabled: false
                    }
                    },
                    legend: {
                    enabled: false
                    },
                    series: [{
                    dataLabels: {
                    //useHTML: true,
                    //format: '<div style="width:150px;height:38px;border:solid 3px #B1B1B1!important; border-radius:5px;padding-left:60px;">{point.value}</div>',
                    //color: 'white',
                    enabled: true,
                            borderRadius: 0,
                            //backgroundColor: 'rgba(252, 0, 0, 0.4)',
                            borderWidth: 0,
                            //borderColor: 'rgba(252, 0, 0, 0.2)',
                            style: {
                            fontSize: "16px",
                                    textShadow: 'none',
                                    HcTextStroke: null
                            }
                    },
                            type: 'treemap',
                            layoutAlgorithm: 'squarified',
                            data: [{
                            name: 'NZ Fixed Interest: 3.2%',
                                    value: 3.2,
//                                    colorValue: 1,
                                    color: "#575f65"
                            }, {
                            name: 'New Zealand Equities: 7% ',
                                    value: 7,
//                                    colorValue: 2,
                                    color: "#2d373e"
                            }, {
                            name: 'Australian Equities: 8%',
                                    value: 8,
//                                    colorValue: 3,
                                    color: "#2c94ec"
                            }, {
                            name: 'International Fixed Interest: 2.1%',
                                    value: 2.1,
//                                    colorValue: 4,
                                    color: "#2c94ec"
                            }, {
                            name: 'Listed Property: 4.2%',
                                    value: 4.2,
//                                    colorValue: 5,
                                    color: "#70787e"
                            }, {
                            name: 'Cash: 1.2%',
                                    value: 1.2,
//                                    colorValue: 6,
                                    color: "#ec548c",
                            }, {
                            name: 'International Equities: 13%',
                                    value: 13,
//                                    colorValue: 7,
                                    color: "#d7d2cb"
                            }],
//                            tooltip: {
//                            valueSuffix: '%'
//                            },
                    }],
                    tooltip: {
                    backgroundColor: '#fff',
                            formatter: function () {
                            return this.point.name;
//                                            + this.point.value;
                            }
                    },
                    title: {
                    text: ''
                    }
            });
        </script>
        <script>
            $(document).ready(function(){

            Highcharts.getJSON('https://cdn.jsdelivr.net/gh/highcharts/highcharts@v7.0.0/samples/data/world-population-density.json', function (data) {
            var total = 94579.83999999998;
            // Prevent logarithmic errors in color calulcation
//                 data.forEach(function (p) {
//                    total = total+ (p.value < 1 ? 1 : p.value);
//                });
//                 console.log(total);
            data.forEach(function (p) {
            p.value = (p.value < 1 ? 1 : p.value);
            console.log(p.value);
            });
            // Initiate the chart
            Highcharts.mapChart('countryhighchart', {
            chart: {
            map: 'custom/world'
            },
                    title: {
                    text: ''
                    },
                    mapNavigation: {
                    enabled: false,
                            enableDoubleClickZoomTo: true
                    },
                    colorAxis: {
                    min: 1,
                            max: 1000,
                            type: 'logarithmic',
                            stops: [
                            [0, '#2c94ec'],
                            [0.5, Highcharts.getOptions().colors[1]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[1]).brighten( - 0.5).get()]
                            ]
                    },
                    credits: {
                    enabled: false,
                    },
                    navigation: {
                    buttonOptions: {
                    enabled: false
                    }
                    },
                    legend: {
                    enabled: false
                    },
//                    tooltip: {
//                        pointFormat: '{series.name}: <b>{value.percentage:.1f}%</b>'
//                    },

                    series: [{
                    data: data,
                            joinBy: ['iso-a3', 'code3'],
                            name: 'Investment Allocation',
                            states: {
                            hover: {
                            color: '#304bb7'
                            }
                            },
                            tooltip: {
                            valueSuffix: '%'
                            }
                    }]
            });
            });
            });
        </script>
    </body>
</html>									