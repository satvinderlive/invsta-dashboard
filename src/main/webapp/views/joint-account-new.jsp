<%-- 
    Document   : join-account
    Created on : Sep 11, 2019, 10:02:34 AM
    Author     : ADMIN
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" >
    <head>
        <title>My Farm</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="/resources/favicon.png" rel="shortcut icon"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="./resources/css/signup/style.css">
        <link rel="stylesheet" href="./resources/css/signup/custom.css">
        <link rel="icon" href="./resources/images/invsta-color-logo.png" type="image/gif" sizes="16x16">
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <link rel="stylesheet" href="./resources/css/signup/intlTelInput.css">
        <link href="./resources/css/sweetalert.css" rel="stylesheet" />
        <style>
            img.logo {
                width: 30%;
            }
            .intl-tel-input .flag-dropdown .selected-flag .down-arrow {
                top: 6px;
                position: relative;
                left: 20px;
                width: 0px;
                height: 0;
                border-left: 3px solid transparent;
                border-right: 3px solid transparent;
                border-top: 6px solid black;
            }

            input.verify-dl {
                background: ba;
                background: #65b9ac!important;
                z-index: 999999999;
                color: #fff!important;
                max-width: 103px;
                height: 26px;
                padding: 2px!important;
                font-size: 11px!important;
                cursor: pointer;
            }
            .mobile-space input#more-investor-countrycode {
                width: 80px;
            }

            .mobile-space input#more-investor-mobile {
                width: 100%!important;
            }
            input.verify-dl.verify-btn {
                max-width: 123px;
                height: auto;
                padding: 7px 14px!important;
                font-size: 14px!important;
                border-radius: 4px!important;
            }
            .otp-down span#error-generateOtp {
                position: absolute;
                top: 100px;
                left: 33%;
            }
            @media (min-width:441px) and (max-width:540px){
                .otp-down span#error-generateOtp {
                    position: absolute;
                    top: 100px;
                    left: 30%;
                }  
                .row.otp-area.verify-down {
                    margin-top: 20px;
                }
            }
            @media (min-width:327px) and (max-width:440px){
                .otp-down span#error-generateOtp {
                    position: absolute;
                    top: 125px;
                    left: 20%;
                }  
                .row.otp-area.verify-down {
                    margin-top: 20px;
                }
            }
            @media (min-width:320px) and (max-width:326px){
                .otp-down span#error-generateOtp {
                    position: absolute;
                    top: 149px;
                    left: 19%;
                }  
                .row.otp-area.verify-down {
                    margin-top: 20px;
                }
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="loged_user signup-btn-west">
                <a href="./login?logout"><p>Already have an Account Login</p></a>
            </div>
            <div class="row">
                <div class="col-md-12 ">
                    <div class="content_body">
                        <form id="msform" name="msRegform" class="form-signup" action="/chelmer" method="POST" autocomplete="off">
                            <div class="w3-light-grey">
                                <!--<div id="myBar" class="progress-bar" style="width:0%"></div>-->
                            </div>

                            <div class="logo-w">
                                <a href="#"><img class="logo" alt="" src="./resources/images/logo.png" ></a>
                            </div>
                            <div class="save-new-btn">
                                <input type="hidden" name="register_type" id="register_type" value="JOINT_ACCOUNT">
                                <input type="hidden" name="step" id="step" value="1">
                                <c:choose>
                                    <c:when test="${joint.curr_idx gt 0}">
                                        <input type="hidden" name="more_investor_index" id="more_investor_index" value="${joint.curr_idx}">
                                    </c:when>
                                    <c:otherwise>
                                        <input type="hidden" name="more_investor_index" id="more_investor_index" value="0">
                                    </c:otherwise>
                                </c:choose>
                                <button class="director-btn all-btn-color save-font saveExit">Save & Exit</button>
                                <div class="pulsating-circle4 pulsating-around6" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                    <span class="tooltiptext">You can save and exit this application at any time by clicking on this Save &amp; Exit button. We will then send you an email from which you can resume your application when you are ready.</span>
                                </div>
                            </div>
                            <fieldset id="step1">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please provide your details below (Investor 1).
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Title
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption form-group option-add" id="title" >
                                                    <%-- <c:if test = "${joint.title eq ''}">
                                                         <option value="title">${joint.title}</option>
                                                     </c:if> --%>
                                                    <option value="Mr">Mr</option>
                                                    <option value="Mrs">Mrs</option>
                                                    <option value="Miss">Miss</option>
                                                    <option id="other" value="Master">Master</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Full name
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control input-field" id="fullName" name="fullName" value="${joint.fullName}" required="required" placeholder="Enter full name " onkeyup="myFunction()" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                <span class="error" id="span-fullName" ></span>
                                                <input type="hidden" id="directors-index"/>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Preferred first name (if applicable) 
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control input-field" id="" name="preferredName" value="${joint.preferred_name}" required="required" placeholder="Enter preferred name (optional)" onkeyup="myFunction()" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                <span class="error" id="span-preferredName" ></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Date of birth
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="dob" id="dob" placeholder="dd/mm/yyyy" class="input-field dateOfBirth" value="${joint.date_of_Birth}" data-format="DD/MM/YY" data-lang="en" required/>
                                                <span class="error" id="span-dob-j"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Country of residence ${joint.country_residence}
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-group country-set flag-drop"> 
                                                <input type="text" class="form-control countryname abc Country-of-residence"  id="countryResidence" name="countryCode" value="${joint.country_residence}" readonly="readonly">
                                                <span class="error" id="span-countryResidence"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Occupation 
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <select name="occupation" id="select-occupation" class="Occupation occupationOption selectoption  otherOcc  nameTitle form-group">
                                                    <option value="-Select-">–Select–</option>
                                                    
                                                        <option value="5">Arts and Media Professionals</option>
                                                    
                                                        <option value="13">Automotive and Engineering Trades Workers</option>
                                                    
                                                        <option value="6">Business, Human Resource and Marketing Professionals</option>
                                                    
                                                        <option value="20">Carers and Aides</option>
                                                    
                                                        <option value="1">Chief Executives, General Managers and Legislators</option>
                                                    
                                                        <option value="38">Cleaners and Laundry Workers</option>
                                                    
                                                        <option value="29">Clerical and Office Support Workers</option>
                                                    
                                                        <option value="39">Construction and Mining Labourers</option>
                                                    
                                                        <option value="14">Construction Trades Workers</option>
                                                    
                                                        <option value="7">Design, Engineering, Science and Transport Professionals</option>
                                                    
                                                    <option value="0">Other</option>
                                                </select>
                                                <input type="text" name="Occupation" id="input-occupation" placeholder="Enter occupation " class="Occupation selectOcc input-field" />
                                                <span class="error" id="span-occupation"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Are you working with an Investment Adviser?
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption form-group avisor-click working_with_adviser">
                                                    <c:if test = "${joint.working_with_adviser eq ''}">
                                                        <option value="">${joint.working_with_adviser}</option>
                                                    </c:if>
                                                    <option value="1">No</option>
                                                    <option value="2">Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row advisor-show">
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Select Advisory Company 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption advisor_company form-group" id="isAdvisorCompany" >
                                                    <c:forEach items="${advisories}" var="advisor">
                                                        <option value="${advisor.login_id}">${advisor.companyName}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Select an Adviser 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption advisor form-group" id="isAdviser" >
                                                    <c:forEach items="${advisories}" var="advisor">
                                                        <option value="${advisor.login_id}">${advisor.advisorName}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <a href="javascript:void(0)" name="previous" class="previous action-button-previous privous-signup" value="Previous" >Previous</a>
                                <input type="button" name="next" class="next1 action-button"  value="Continue"/>
                            </fieldset>

                            <fieldset id="step2">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter your contact details (Investor 1). 
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Home address
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" id="address" class="form-control input-field" value="${joint.homeAddress}"  placeholder="Enter home address" />
                                                <span class="error" id="span-homeaddress"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    <!--<div class="pulsating-circle4 circler" data-toggle="tooltip" title=" We require at least one contact number for you. If you do not have a mobile number, please provide an other type of number." data-placement="right" style="vertical-align:text-bottom"></div>-->
                                                    <div class="pulsating-circle4 pulsating-around6 circler" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                                        <span class="tooltiptext minor-tooltip-2 ">We require at least one contact number for you. If you do not have a mobile number, please provide an other type of number.</span>
                                                    </div>
                                                    Mobile number (required)
                                                </label>
                                            </div>
                                            <div class="col-sm-6 mobile_number first-mobile">
                                                <input type="text" class="form-control codenumber mobile_country_code"  id="countryCode3" value="${joint.mobile_country_code}" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">
                                                <input type="tel" class="form-control error codename mobile_number1" id="mobileNo" name="mobileNo" value="${joint.mobile_number}" required="required" placeholder=" Enter mobile number" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                <span class="error" id="span-mobileNumber"></span>
                                            </div>

                                            <div class="col-sm-3">
                                                <label class="label_input" style="text-align:left">
                                                    Other number (optional) 
                                                </label>
                                            </div>
                                            <div class="col-sm-3">
                                                <select class="selectoption form-group" id="typeOfMobile" >
                                                    <c:if test = "${joint.working_with_adviser eq ''}">
                                                        <option value="">${joint.optional_num_type}</option>
                                                    </c:if>
                                                    <option value="1">Home</option>
                                                    <option value="2">Work</option>
                                                    <option value="3">Other</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6 mobile_number first-mobile mobile-index1">
                                                <input type="text" class="form-control codenumber optional_num_code"  id="countryCode" value="${joint.optional_num_code}" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">
                                                <input type="tel" class="form-control error codename optional_num" id="mobileNo" name="mobileNo" value="${joint.optional_num}" required="required" placeholder=" Enter number" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous1 action-button-previous" value="Previous" />
                                <input type="button" name="next"  class=" next2 action-button"  value="Continue"/>
                            </fieldset>

                            <fieldset id="step3">
                                <div class="content-section">

                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Account security: mobile verification (Investor 1).  
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="input-content-text">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <small class="verification-otp1">
                                                        To verify your mobile number, please choose to receive a unique verification code via text message or phone call. 
                                                    </small>
                                                </div>
                                            </div>
                                            <div class="otp-down row">
                                                <div class="col-sm-12 otp-icon">
                                                    <ul class="radio_button">
                                                        <li>
                                                            <input type="radio" class="senderType" id="f-option"  name="senderType" value="sms" style="width: auto;">
                                                            <img src="./resources/images/icon/message-icon.png" style="width: 20px; margin-left: 5px;">
                                                            <label for="f-option"></label>
                                                            <div class="check"></div>
                                                        </li>
                                                        <li>
                                                            <input type="radio" class="senderType" id="s-option" name="senderType" value="call" style="width: auto;">
                                                            <i class="fas fa-phone"></i>
                                                            <label for="s-option"></label>
                                                            <div class="check">
                                                                <div class="inside"></div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <span class="error" id="error-generateOtp"></span>
                                            </div>
                                            <div class="row otp-area verify-down">
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Verification code 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="verification" placeholder="Enter unique code  " class="input-field otpkey"/>
                                                     <span class=" spanverification" id="error-verification"></span>
                                                </div>
                                            </div>
                                            <small class="verification-otp"> If you have not received your unique verification code within one minute, you can request another by  <a href="#">clicking here.</a>.</small>
                                        </div>
                                    </div>

                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous2 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next3 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step4">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter your identification details (Investor 1).  
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input detail-2">
                                                    Which type of ID are you providing
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption form-group id_type Id_Type" id="src_of_fund1">
                                                    <option value="1"
                                                            <c:if test="${joint.id_type eq 'NZ Driver Licence'}">selected</c:if>
                                                                >NZ Driver Licence</option>
                                                            <option value="2"
                                                            <c:if test="${joint.id_type eq 'NZ Passport'}">selected</c:if>
                                                                >NZ Passport</option>
                                                            <option value="3"
                                                            <c:if test="${joint.id_type eq 'Other'}">selected</c:if>
                                                                >Other</option>
                                                    </select>
                                                </div>
                                                <div class="row drivery-licence">
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            First name 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="first_name" name="licence_first_name" placeholder="Enter first name" onkeyup="myFunction()" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                        <span class="error error-first_name" id="error_licence_first_name"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Middle name 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="middle_name" name="licence_middle_name" placeholder="Enter middle name" onkeyup="myFunction()" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                        <span class="error" id="error_licence_middle_name"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Last name 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="last_name" name="licence_last_name" placeholder="Enter last name" onkeyup="myFunction()" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                        <span class="error error-last_name" id="error_licence_last_name"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Licence number 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-field License_number" id="license_number" value="${joint.license_number}" name="license_number" required="required" placeholder="Enter licence number" onkeyup="myFunction()" />
                                                    <span class="error" id="span-license_number"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Expiry date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name="dob" id="dob1" placeholder="dd/mm/yyyy" class="input-field dob1 licence_expiry_Date lic_expiry_Date" value="${joint.licence_expiry_Date}" data-format="DD/MM/YY" data-lang="en" required/>                                 
                                                    <span class="error" id="error-licence_expiry"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Version number 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="college" value="${joint.licence_verson_number}" placeholder="Enter version number" class="input-field licence_verson_number  lic_verson_number" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                                                    <span class="error" id="error-Version_number"></span>
                                                </div>
                                            </div>
                                            <div class="row passport-select">

                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        First name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="first_name PassFirst_name" name="passport_first_name" placeholder="Enter first name" onkeyup="myFunction()" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="errorPassFirst_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Middle name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="middle_name" name="passport_middle_name" placeholder="Enter middle name" onkeyup="myFunction()" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_passport_middle_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Last name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="last_name PassLast_name" name="passport_last_name" placeholder="Enter last name" onkeyup="myFunction()" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="errorPassLast_name"></span>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Passport number
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-field passport_number" id="fullName" value="${joint.passport_number}" name="passport_number" required="required" placeholder="Enter passport number" onkeyup="myFunction()" />
                                                    <span class="error" id="error-passportNumber"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Expiry date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name="dob" id="dob2" placeholder="dd/mm/yyyy" class="input-field dob1 passport_expiry pass_expiry"   name="passport_expiry" value="${joint.passport_expiry}" data-format="DD/MM/YY" data-lang="en" required/>                                 
                                                    <span class="error" id="error-passportExpiry"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Country of issue  
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 flag-drop">
                                                    <input type="text" class="form-control countryname issue_by"  id="countryname11" value="${joint.passport_issue_by}" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                    <span class="error" id="error_countryname11"></span>
                                                </div>
                                            </div>
                                            <div class="row other-select">
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Type of ID   
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-field other_id_type" id="TypeofID" value="${joint.other_id_type}" name="fullName" required="required" placeholder="Enter ID type" onkeyup="myFunction()" />
                                                    <span class="error" id="error-TypeofID" ></span>
                                                </div>
                                                    <div class="col-sm-6">
                                                    <label class="label_input">
                                                        First name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="first_name error-remove" id="minor_other_first_name " name="other_first_name" placeholder="Enter first name"/>
                                                    <span class="error error-minor" id="error_minor_other_first_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Middle name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="middle_name error-remove" name="other_middle_name" placeholder="Enter middle name"/>
                                                    <span class="error error-minor" id="error_minor_other_middle_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Last name 
                                                    </label>
                                                </div>                                                    
                                                <div class="col-sm-6">
                                                    <input type="text" class="last_name error-remove" id="minor_other_last_name" name="other_last_name" placeholder="Enter last name"/>
                                                    <span class="error error-minor" id="error_minor_other_last_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Expiry date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name="dob" id="dob3" placeholder="dd/mm/yyyy" value="${joint.other_id_expiry}" class="input-field dob1 other_id_expiry" data-format="DD/MM/YY" data-lang="en" required/>                                 
                                                    <span class="error" id="error-TypeofIdExpiry"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Country of issue  
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 flag-drop">
                                                    <input type="text" class="form-control countryname other_id_issueBy"  id="countryResidence1" value="${joint.other_id_issueBy}" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                    <span class="error" id="error_countryResidence1"></span>
                                                </div>
                                                <div class="col-sm-129 closestcls">
                                                    <input type="file" name="myFile" id="other_id_myFile" class="attach-btn checkname" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" onchange="checkname(this)">
                                                    <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);"><i class=" removefile far fa-times-circle"></i></a>
                                                    <span class="error" id="error_other_id_myFile"></span>
                                                </div>
                                            </div>

                                            <input type="hidden" name="investor_verify" id="investor_verify" value="false">
                                            <!--<div class="col-sm-12">-->

                                            <!--                                            <div class="col-sm-12 verifybtn" >
                                                                                            <input type="button" name="myFile" class="verify-dl verify-btn" value="Verify Details">
                                                                                            <span class="error" id="error_other_ile"></span>
                                                                                        </div>-->
                                            <!--</div>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous3 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next4 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step5">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter your tax details (Investor 1). 
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Prescribed Investor Rate (PIR)
                                                </label>
                                            </div>
                                            <div class="col-sm-3 details-pos">
                                                <select class="selectoption form-group aml-select PIR" >
                                                    <c:if test="${joint.pir eq ''}">
                                                        <option value="">${joint.pir}</option>
                                                    </c:if>
                                                    <option value="28%">28%</option>
                                                    <option value="17.5%">17.5%</option>
                                                    <option value="10.5%">10.5%</option>
                                                </select>

                                            </div>
                                            <div class="col-sm-3">
                                                <a href="javascript:void(0)" class="aml-link pir">What’s my PIR?</a>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    IRD Number 
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control IRD_Number" value="${joint.ird_Number}" name="fullName" required="required" placeholder="XXX-XXX-XXX" onkeydown="checkird(this)" onkeypress="return event.charCode >= 48 && event.charCode <= 57" />
                                                <span class="error" id="error-irdNumber"></span>
                                            </div>
                                                <div class="col-sm-6">
                                                <label class="label_input">
                                                    What is the source of funds or wealth for this account? 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption" id="wealth_src">
                                                    <c:if test = "${not empty person.wealth_src}">
                                                        <option value="${person.wealth_src}">${person.wealth_src}</option>
                                                    </c:if>
                                                    <option value="0">–Select–</option>
                                                    <option value="1"> Property sale </option>
                                                    <option value="2"> Personal</option>
                                                    <option value="3"> Employment</option>
                                                    <option value="4"> Financial investment</option>
                                                    <option value="5"> Business sale</option>
                                                    <option value="6"> Inheritance/gift</option>
                                                    <option value="7"> Other </option>
                                                </select>
                                                <span class="error" id="error_wealth_src"></span>
                                            </div>
                                                <div class="col-sm-6 des-togle">
                                                <label class="label_input" style="text-align:left">
                                                   Description 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 des-togle">
                                                <input type="text" class="" id="" name="other_description" placeholder="Enter description"/>
                                                    <span class="error" id=""></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Are you a US citizen or US tax resident, or a tax resident in any other country?
                                                    <div class="pulsating-circle4 pulsating-around6 joint-circler" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                                        <span class="tooltiptext joint-tooltip ">
                                                            New Zealand has implemented rules which require financial institutions, including westpac, to collect certain information about
                                                            their clients’ foreign tax residency. For further information about the Foreign Account Tax Compliance Act (FATCA) or the
                                                            Common Reporting Standard (CRS) you can visit the Inland Revenue website at www.ird.govt.nz/international/exchange or
                                                            speak to a tax adviser. For further information about international tax residency rules you can visit the OECD website at
                                                            www.oecd.org/tax/automatic-exchange/crs-implementation-and-assistance/tax-residency. An ‘entity’ includes a company,
                                                            trust, partnership, association, registered co-operative, or government body.

                                                        </span>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption selectoption1 form-group" id="isUSCitizen">
                                                    <option <c:if test="${joint.isUSCitizen eq 1}">selected</c:if> value="1">No</option>
                                                    <option <c:if test="${joint.isUSCitizen eq 2}">selected</c:if> value="2">Yes</option>
                                                    </select>
                                                </div>
                                                <div class="row yes-option" style="<c:if test="${joint.isUSCitizen eq 1 || (empty joint.isUSCitizen)}">display:none;</c:if><c:if test="${joint.isUSCitizen eq 2}">display:flex;</c:if>">
                                                    <div class="row yes-new" >
                                                        <div class="col-sm-12">
                                                            <h5 class="element-header aml-text">
                                                                Please enter all of the countries (excluding NZ) of which you are a tax resident.
                                                            </h5>
                                                        </div>
                                                    <c:choose>
                                                        <c:when test="${fn:length(joint.countryTINList) gt 0}">
                                                            <c:forEach items="${joint.countryTINList}" var="tin">                                                    
                                                                <div class="row checktindata">
                                                                    <div class="col-sm-6">
                                                                        <label class="label_input" style="text-align:left">
                                                                            Country of tax residence
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-sm-6 details-pos flag-drop">
                                                                        <input type="text" class="form-control countrynameoutnz tex_residence_Country" value="${tin.country}"  id="countryname22" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                                        <span class="error" id="error-tex-residence-Country"></span>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <label class="label_input">
                                                                            Tax Identification Number (TIN)
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <input type="text" class="form-control input-field TIN" value="${tin.tin}" name="fullName" required="required" placeholder="Enter TIN " onkeyup="myFunction()" />
                                                                        <span class="error" id="error-TIN"></span>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <label class="label_input">
                                                                            Reason if TIN not available
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <input type="text" class="form-control input-field resn_tin_unavailable" value="${tin.reason}" name="tin" required="required" placeholder=""/>
                                                                        <span class="error error-resn-tin-unavailable" id="error-resn-tin-unavailable"></span>
                                                                    </div>
                                                                </div>
                                                            </c:forEach>
                                                        </c:when>
                                                        <c:otherwise> 
                                                            <div class="row checktindata">
                                                                <div class="col-sm-6">
                                                                    <label class="label_input" style="text-align:left">
                                                                        Country of tax residence
                                                                    </label>
                                                                </div>
                                                                <div class="col-sm-6 details-pos flag-drop">
                                                                    <input type="text" class="form-control countrynameoutnz tex_residence_Country countryname202" value="${tin.country}"  id="countryname202" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                                    <span class="error error-tex-residence-Country" id="error-tex-residence-Country"></span>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label class="label_input">
                                                                        Tax Identification Number (TIN) 
                                                                    </label>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control input-field TIN" value="${tin.tin}" name="fullName" required="required" placeholder="Enter TIN " onkeyup="myFunction()" />
                                                                    <span class="error error-TIN" id="error-TIN"></span>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label class="label_input">
                                                                        Reason if TIN not available
                                                                    </label>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control input-field resn_tin_unavailable" value="${tin.reason}" name="tin" required="required" placeholder=""/>
                                                                    <span class="error error-resn-tin-unavailable" id="error-resn-tin-unavailable"></span>
                                                                </div>
                                                            </div>
                                                        </c:otherwise>
                                                    </c:choose>

                                                </div>

                                                <div class="col-sm-129 add-another">
                                                    <a class="add-another2 all-btn-color" href="javascript:void(0)" id="add-country-another">Add another country</a>
                                                </div>                                                            

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous4 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next5 action-button" value="Continue" />
                            </fieldset>        
                            <fieldset id="step6">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please provide details of all other joint account holders. 
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <small class="verification-otp1">
                                                    We require further information about each Investor. You can provide this information now, alternatively we’ll send them an email requesting this information. 
                                                </small>
                                                <input type="hidden" class="input-field counter" id="counter" >
                                            </div>
                                            <div class="row investor-section">
                                                <c:choose>
                                                    <c:when test="${not empty joint.moreInvestorList}">
                                                        <c:forEach items="${joint.moreInvestorList}" var="moreInvestor" varStatus="loop">
                                                            <div class="row investor-add" id="investor">
                                                                <div class="col-sm-4">
                                                                    <label class="label_input">
                                                                        Full name of investor
                                                                    </label>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <select class="selectoption more-investor-title adjust-option">
                                                                        <option value="Appointment">Mr</option>
                                                                        <option value="Interview">Mrs</option>
                                                                        <option value="Regarding a post">Miss</option>
                                                                        <option id="other" value="Other">Master</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="fname input-field otherfname more-investor-fname" id="otherfname" value="${moreInvestor.fullName}" name="fullName" placeholder="Enter full name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'>
                                                                    <span class="error error-fname" id="error-otherfname" ></span>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label class="label_input">
                                                                        Email address
                                                                    </label>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="email" id="emailAddress" class="form-control input-field emailAddress otheremailAddress more-investor-email" value="${moreInvestor.email}" name="email" placeholder="Enter email address" onkeyup="myFunction()" />
                                                                    <span class="error error-emailAddress" ></span>
                                                                </div>
                                                                <div class="col-sm-12 new-box">
                                                                    <input type="radio" id="radio05${loop.index}" class="radio1 checkradio" value="1" name="year${loop.index}" checked>
                                                                    <label for="radio05${loop.index}" class="forlabel1"><span class="enter-btn-small small-padd">I’ll enter their details now</span> </label>
                                                                    <input type="radio" id="radio06${loop.index}" class="radio2 checkradio" value="2" name="year${loop.index}">
                                                                    <label for="radio06${loop.index}" class="forlabel2"><span class="enter-btn-small small-padd1">Send them an email requesting info</span> </label>
                                                                </div>
                                                            </div>
                                                        </c:forEach>        
                                                    </c:when>
                                                    <c:otherwise>
                                                        <div class="row investor-add" id="investor">
                                                            <div class="col-sm-4">
                                                                <label class="label_input">
                                                                    Full name of investor
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <select class="selectoption more-investor-title adjust-option"  id="more-investor-title">
                                                                    <option value="Appointment">Mr</option>
                                                                    <option value="Interview">Mrs</option>
                                                                    <option value="Regarding a post">Miss</option>
                                                                    <option id="other" value="Other">Master</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text" class="fname input-field more-investor-fname" id="fname" name="fullName" placeholder="Enter full name" onkeyup="removespan()" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'>
                                                                <a href='javascript:void(0);' class="removeData" onclick='removedatadiv(this);' style="display :none"><i class='far fa-times-circle'></i></a>
                                                                <span class="error error-fname" id="error-fname"></span>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label class="label_input">
                                                                    Email address
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-6" data-validate = "Valid email is: a@b.c">
                                                                <input type="email" class="form-control input-field emailAddress  more-investor-email" name="email" placeholder="Enter email address" onkeyup="myFunction1()" />
                                                                <span class="error error-emailAddress" id="error-emailAddress"></span>
                                                            </div>
                                                            <div class="col-sm-12 new-box">
                                                                <input type="radio" id="radio05" class="radio1 checkradio" value="1" name="year" checked>
                                                                <label for="radio05" class="forlabel1"><span class="enter-btn-small small-padd">I’ll enter their details now</span> </label>
                                                                <input type="radio" id="radio06" class="radio2 checkradio" value="2" name="year">
                                                                <label for="radio06" class="forlabel2"><span class="enter-btn-small small-padd1">Send them an email requesting info</span> </label>
                                                            </div>
                                                        </div>
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-129">
                                                <button id="btn22" class="director-btn all-btn-color">Add another Investor</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous5 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next6 action-button" value="Continue" />
                            </fieldset>
                            <jsp:include page="./more-investor-view.jsp"></jsp:include>
                                <fieldset id="step11">
                                    <div class="content-section">
                                        <div class="element-wrapper">
                                            <h5 class="element-header">
                                                Please enter your bank account details (for distributions and/or redemptions).  
                                            </h5>
                                        </div>
                                        <div class="input-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Bank name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 details-pos">
                                                    <select class="selectoption form-group aml-select bank_name" id="bankName">
                                                    <%-- <c:if test="${joint.bank_name eq ''}">
                                                         <option value="">${joint.bank_name}</option>
                                                     </c:if>--%>
                                                    <option value="No Bank Select yet">–Select–</option>
                                                    <option value="Bank of New Zealand">Bank of New Zealand</option>
                                                    <option value="ANZ Bank New Zealand">ANZ Bank New Zealand</option>
                                                    <option value="ASB Bank">ASB Bank</option>
                                                    <option value="Westpac">Westpac</option>
                                                    <option value="Heartland Bank">Heartland Bank</option>
                                                    <option value="Kiwibank">Kiwibank</option>
                                                    <option value="SBS Bank">SBS Bank</option>
                                                    <option value="TSB Bank">TSB Bank</option>
                                                    <option value="The Co-operative Bank">The Co-operative Bank</option>
                                                    <option value="NZCU">NZCU</option>
                                                    <option value="Rabobank New Zealand">Rabobank New Zealand</option>
                                                    <option value="National Bank of New Zealand">National Bank of New Zealand</option>
                                                    <option value="National Australia Bank">National Australia Bank</option>
                                                    <option value="Industrial and Commercial Bank of China">Industrial and Commercial Bank of China</option>
                                                    <option value="PostBank">PostBank</option>
                                                    <option value="Trust Bank Southland">Trust Bank Southland</option>
                                                    <option value="Trust Bank Otago">Trust Bank Otago</option>
                                                    <option value="Trust Bank Canterbury">Trust Bank Canterbury</option>
                                                    <option value="Trust Bank Waikato">Trust Bank Waikato</option>
                                                    <option value="Trust Bank Bay of Plenty">Trust Bank Bay of Plenty</option>
                                                    <option value="Trust Bank South Canterbury">Trust Bank South Canterbury</option>
                                                    <option value="Trust Bank Auckland">Trust Bank Auckland</option>
                                                    <option value="Trust Bank Central">Trust Bank Central</option>
                                                    <option value="Trust Bank Wanganui">Trust Bank Wanganui</option>
                                                    <option value="Westland Bank">Westland Bank</option>
                                                    <option value="Trust Bank Wellington">Trust Bank Wellington</option>
                                                    <option value="Countrywide">Countrywide</option>
                                                    <option value="United Bank">United Bank</option>
                                                    <option value="HSBC">HSBC</option>
                                                    <option value="Citibank">Citibank</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                                <span class="error" id="error_select_bank_name"></span> 
                                            </div>
                                            <div class="col-sm-6 toggal_other">
                                                <label class="label_input" style="text-align:left">
                                                    Other Bank name
                                                </label>
                                            </div>
                                            <div class="col-sm-6 toggal_other">
                                                <input type="text" class="Other_Bank_name form-control input-field" name="other_bank_name" id="other_other_name" value="" required="required" placeholder="Enter your Bank name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' />
                                                <span class="error" id="error_Other_Bank_name"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Account name 
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control input-field acount_holder_name" value="${joint.acount_holder_name}" id="nameOfAccount" name="nameOfAccount" required="required" placeholder="Enter your account name" onkeyup="myFunction()" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                <span class="error" id="error_acount_holder_name1"></span>   
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Account number
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <input type="text" class="form-control input-field account_number" value="${joint.account_number}" id="myspan" name="accountNumber" required="required" placeholder="12-3456-1234567-000"  onkeypress="myFunction7()"/>
                                                <span class="error" id="error_acount_holder_number"></span>                                         
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="new-attach-info">
                                                    <div class="col-sm-129 closestcls">
                                                        <input type="file" name="myFile" id="bank_document" class="bank_document attach-btn2 checkname" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" onchange="checkname(this)">
                                                        <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);"><i class=" removefile far fa-times-circle"></i></a>
                                                        <span class="error" id="error_bank_document" ></span>
                                                    </div>
                                                    <!--<span class="info-icon"><a href="#" data-toggle="tooltip" title="To confirm your account details, attach one of the following (must show account number and name and be less than 3 months old): Copy of bank or credit card statement, copy of electronic statement, screenshot from online banking"><img src="./resources/images/i-icon.png"></a></span>-->
                                                    <!--<span class="info-icon"><a href="#" data-toggle="tooltip" title="><img src="./resources/images/i-icon.png"></a></span>-->
                                                    <div class="pulsating-circle5 btn-info-chng" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom"></div>
                                                    <div class="pulsating-circle5 pulsating-around6 btn-info-chng" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                                        <span class="tooltiptext tooltip-1 ">To confirm your account details, attach one of the following (must show account number and name and be less than 3 months old): Copy of bank or credit card statement, copy of electronic statement, screenshot from online banking.</span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous10 action-button-previous"  onclick="prev(this)" value="Previous" />
                                <input type="button" name="next" class="next11 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step12">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please read and accept the below Terms and Conditions to continue.
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="input-content-text">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h4 class="text-center">Agreement of Terms</h4>
                                                    <div class="terms_cond" style="overflow:auto;height: 200px;word-break: break-all;">
                                                        <ol>
                                                            <li> <b>APPLICATION OF THESE TERMS AND CONDITIONS</b>
                                                                <ol>
                                                                    <li>If you create a User Profile and choose to access services via our Platform you accept and agree that you will be bound by these Terms and Conditions. Capitalised words are defined in clause 22.</li>
                                                                    <li>The risk of loss in trading or holding Digital Currency can be substantial. You should therefore carefully consider whether trading or holding Digital Currency is suitable for you in light of your financial condition, tolerance to risk and trading experience.
                                                                    </li>
                                                                </ol>
                                                            </li>
                                                            <li> <b>SERVICES PROVIDED</b>
                                                                <ol>
                                                                    <li>Through the Platform you will have access to the following services ("Crypto Traded Portfolio Services") in relation to digital currencies (such as, but not limited to, Bitcoin and Ethereum) selected by us from time to time (?Digital Currency?):
                                                                        <ol>
                                                                            <li>Model crypto currency portfolios on our Platform providing a range of crypto currency investment options and strategies;</li>
                                                                            <li>High Frequency Trading (HFT) Algorithm based portfolios; </li>
                                                                            <li>General information, including performance data, on the various portfolio options and any portfolios selected by you; </li>
                                                                            <li>General information and education about crypto-currencies and related topics, including investment strategies; </li>
                                                                            <li>Assistance with managing your Portfolio, including executing lump-sum investment Transactions and regular monthly investment Transactions on your behalf, and the ability to track and transfer your supported Digital Currencies;</li>
                                                                            <li>Portfolio re-balancing to reflect the target asset allocation in the model portfolio selected and accepted by you;</li>
                                                                            <li>Foreign and digital currency transactions as required to effect the purchase of selected Portfolios.</li>
                                                                            <li>Digital currency wallets that hold the underlying digital currency assets of the various portfolios on offer, like Bitcoin or Ethereum ("Digital Currency").</li>
                                                                            <li>Chat and user support services.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>We do not deal in digital currencies or tokens which are or may be Financial Products under New Zealand law on the
                                                                        Platform. For this reason, we are not required to hold any licence or authorization in relation to portfolio management of
                                                                        the Digital Currency we do support on the Platform. If we become aware that any Digital Currency we have supported on
                                                                        the Platform is or may be (in our sole discretion) a Financial Product, you authorize and instruct us to take immediate steps
                                                                        to divest any such holdings at the price applying at the time of disposal.</li>
                                                                </ol>
                                                            </li>
                                                            <li> <b>USER PROFILE</b>
                                                                <ol>
                                                                    <li>In order to access the Crypto Traded Portfolio Services via our Platform you need to create a User Profile and meet the following requirements:
                                                                        <ol>
                                                                            <li>be 18 years of age or older;</li>
                                                                            <li>reside in a country that meets our eligibility requirements and is not on our excluded list; </li>
                                                                            <li>complete our verification processes in relation to your identity and personal information to our satisfaction; and</li>
                                                                            <li>meet any other requirements or provide information notified by us to you from time to time.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>To create a User Profile you must submit personal information, details and copies of documents via the Platform or via another process (such as email) as requested. We will rely on that information in order to provide Services to you. You agree that all information you submit to us is complete, accurate and not misleading. If any of the information provided by you changes, you must notify us immediately.</li>
                                                                    <li>You agree to take personal responsibility for any actions in respect of instructions you give us through your User Profile.</li>
                                                                    <li>The User Profile may be used only for the provision of  Services on your behalf.</li>
                                                                    <li>Only you may operate the User Profile. You agree to keep any passwords, codes or other security information safe and secure and to not provide that information to any other person. You accept that we are not responsible for any loss or damage you suffer as a result of a person accessing your User Profile who you have not authorized to do so. </li>
                                                                    <li>You must report any suspected loss, theft or misuse of your User Profile to us immediately. </li>
                                                                    <li>You agree to give us clear, consistent and properly authorized instructions via the Platform.</li>
                                                                </ol>
                                                            </li><li><b>INSTRUCTIONS TO EXECUTE A TRANSACTION</b>
                                                                <ol>
                                                                    <li>You may make lump sum investments or regular monthly investments via our Platform.</li>
                                                                    <li>All money paid by you into your Account will be allocated to and invested in accordance with your chosen Portfolio where possible (subject to clause 4.3 below) until such time you instruct us to withdraw your investments.</li>
                                                                    <li>For lump sum investments, you may give instructionstoexecuteaTransactionviaour Platform usingyour User Profile.</li>
                                                                    <li>For regular monthly investments, you may select a monthly contribution amount when you create your User Profile.
                                                                        This amount may be amended from time to time via our Platform.</li>
                                                                    <li>You agree that you authorize us to submit Transactions on your behalf to reflect the target allocation and strategy
                                                                        for your selected Portfolio at the time we receive the allocated funds. You acknowledge that we will buy the
                                                                        maximum number of units (including fractions of units, if available) of the relevant investments in accordance with
                                                                        your selected Portfolio. Any remaining money after a Transaction has been executed will be held in your Wallet. You
                                                                        acknowledge that your Portfolio may not exactly reflect the target asset allocation set out in your model portfolio.
                                                                        You appoint us, and we accept the appointment, to act as your agent for the execution of any Transaction, in
                                                                        accordance with these Terms and Conditions.</li>
                                                                    <li>We will use reasonable endeavors to execute Transactions within 24 hours of receiving allocated funds into your
                                                                        Wallet (subject to these Terms and Conditions, including clause 4.10). The actual price of a particular investment is
                                                                        set by the issuer, provider or the market (as applicable) at the time the order is executed by us. We may set a
                                                                        minimum transaction amount at any time. We may choose not to process any orders below the minimum transaction
                                                                        amount at our discretion. </li>
                                                                    <li>We are under no obligation to verify the authenticity of any instruction or purported instruction and may act on any instruction given using your User Profile without further enquiry.</li>
                                                                    <li>Any Transaction order placed by you forms a commitment, which once you submit, cannot subsequently be amended or revoked by you. However, our acceptance of Transactions via the Platform and our execution of those Transactions is at our sole discretion and subject to change at any time without notice.</li>
                                                                    <li>After a Transaction has been executed, we reserve the right to void any Transaction which we consider contains any
                                                                        manifest error or is contrary to any law or these Terms and Conditions. In the absence of our fraud or willful default,
                                                                        we will not be liable to you for any loss, cost, claim, demand or expense following any such action on our part or
                                                                        otherwise in relation to the manifest error or Transaction.  </li>
                                                                    <li>We are not responsible for any delay in the settlement of a Transaction resulting from circumstances beyond our control, or the failure of any other person or party (including you) to perform all necessary steps to enable completion of the transaction.</li>
                                                                    <li>Any Transaction via the Platform is deemed to take place in New Zealand and you are deemed to take possession
                                                                        of your Portfolio in New Zealand
                                                                    </li>
                                                                </ol>
                                                            </li>
                                                            <li><b>PORTFOLIO REBALANCING</b>
                                                                <ol>
                                                                    <li>All Portfolios are monitored and are rebalanced in line with the Portfolio?s mandate, and at the discretion of the Portfolio Manager. Subject to these Terms and Conditions, and by continuing to hold the Portfolio, you instruct us to automatically rebalance your portfolio back to the target asset allocation based on your selected portfolio. </li>
                                                                    <li>We rebalance by buying and/or selling investments and any other assets within the portfolio, including by using any available cash balances, so that your Portfolio reflects the target asset allocation of your chosen model portfolio.  </li>
                                                                </ol>
                                                            </li>
                                                            <li><b>TARGET ASSET ALLOCATION</b>
                                                                <ol>
                                                                    <li>Each Portfolio will have a Target Asset Allocation, which is set by the Portfolio Manager. The Portfolio Manager may choose to alter the Target Asset Allocation from time to time and at their sole discretion, in line with the mandate and strategy of the portfolio.</li>
                                                                    <li>We will notify you of any changes to the Target Asset Allocation promptly following these changes taking effect.
                                                                        Notification of any such changes to the Target Asset Allocation are made via the Platform by way of an update to
                                                                        the Target Asset Allocation details provided for each Portfolio. </li>
                                                                </ol>
                                                            </li>

                                                            <li><b>DIGITAL CURRENCY WALLET</b>
                                                                <ol>
                                                                    <li><b>Digital Currency Transactions:</b> Invsta processes supported Digital Currency according to the instructions received
                                                                        from its users and we do not guarantee the identity of any party to a Transaction. It is your responsibility to verify all
                                                                        transaction information prior to submitting instructions to Invsta. Once submitted to a Digital Currency network, a
                                                                        Digital Currency Transaction will be unconfirmed for a period of time pending sufficient confirmation of the
                                                                        Transaction by the Digital Currency network. A Transaction is not complete while it is in a pending state. Funds
                                                                        associated with Transactions that are in a pending state will be designated accordingly, and will not be included in
                                                                        your Invsta Account balance or be available to conduct Transactions. We may charge network fees (miner fees) to
                                                                        process a Transaction on your behalf. We will calculate the network fee in our sole discretion by reference to the 
                                                                        cost to us. </li>
                                                                    <li><b>Digital Currency Storage &amp; Transmission Delays:</b> Invsta securely stores all Digital Currency private keys in our control
                                                                        in a combination of online and offline storage. As a result, it may be necessary for Invsta to retrieve certain information
                                                                        from offline storage in order to facilitate a Transaction in accordance with your instructions, which may delay the
                                                                        initiation or crediting of a Transaction for 48 hours or more. You accept that a Digital Currency Transaction facilitated
                                                                        by Invsta may be delayed.</li>
                                                                    <li><b>Operation of Digital Currency Protocols.</b> Invsta does not own or control the underlying software protocols which
                                                                        govern the operation of Digital Currencies supported on our platform. By using the Invsta platform, you acknowledge
                                                                        and agree (i) that Invsta is not responsible for operation of the underlying protocols and that Invsta makes no
                                                                        guarantee of their functionality, security, or availability; and (ii) that the underlying protocols may be subject to
                                                                        sudden changes in operating rules (a/k/a ?forks?), and that such forks may materially affect the value, function,
                                                                        and/or essential nature of the Digital Currency you hold in the Invsta platform. In the event of a fork, you agree that
                                                                        Invsta may temporarily suspend Invsta operations (with or without advance notice to you) and that Invsta may, in its
                                                                        sole discretion, decide whether or not to support (or cease supporting) either branch of the forked protocol entirely.
                                                                        You acknowledge and agree that Invsta assumes absolutely no responsibility in respect of an unsupported branch
                                                                        of a forked Digital Currency and/or protocol.</li>
                                                                    <li>Payments of money can be made to your Wallet electronically, including through credit card payments, e-Poli and bank transfers. The acceptable forms of payment will be set out on our Platform and may be subject to change from time to time.</li>
                                                                    <li>You agree that Digital Curreny investments and any other assets held in your Wallet <b>("Client Property")</b> may be
                                                                        pooled with the investments and other assets of other clients and therefore your holdings may not be individually
                                                                        identifiable within the Wallet. </li>
                                                                    <li>You agree money held in your Wallet (including money held pending investment as well as the proceeds and income from selling investments) <b>("Client Money")</b> may be held in pooled accounts, which means your money may be held in the same accounts as that of other clients using the Platform.</li>
                                                                    <li>Money received from you or a third party for your benefit, which includes your Client Money held pending
                                                                        investment, payment to you as the proceeds and income from selling investments, may attract interest from the
                                                                        bank at which it is deposited. You consent to such interest being deducted from that bank account and being
                                                                        retained by us as part of the fees for using the Services.
                                                                    </li>
                                                                    <li>We will keep detailed records of all your Client Property and Client Money in your Wallet at all times. </li>
                                                                    <li>Client Property and Client Money received or purchased by us on your behalf will be held on bare trust as your
                                                                        nominee in the name or to the order of Invsta or any other approved third party custodian to our order. </li>
                                                                    <li>You confirm that you are the beneficial owner of all of the Client Property and Client Money in your account, or you
                                                                        are the sole trustee on behalf of the beneficial owner, and that they are and will remain free from any encumbrance.</li>
                                                                    <li>Withdrawals from the Service or of any Client Money can be made via the Platform by initiating a ?Withdrawal?.
                                                                        Withdrawals will be processed on a best endeavors basis as outlined in clause 4.6. Withdrawals of Client Money will
                                                                        be processed and paid into a bank account nominated by you in the same name of your User Profile upon
                                                                        settlement, the timing of which depends on the market. We may set a maximum daily amount that can be withdrawn
                                                                        from your Wallet. </li>
                                                                    <li>It is up to you to understand whether and to what extent, any taxes apply to any Transactions through the Services
                                                                        or the Platform, or in relation to your Portfolio. We accept no responsibility for, nor make any representation in respect
                                                                        of, your tax liability.
                                                                    </li>
                                                                </ol>
                                                            </li>

                                                            <li><b>OUR WEBSITE POLICY</b>
                                                                <ol>
                                                                    <li>You agree to receive any and all advice, documents, information, or other communications from Invsta electronically through the Platform, by email, or otherwise over the internet,</li>
                                                                    <li>You agree to receive any statements, confirmations, prospectuses, disclosures, tax reports, notices, documents, information, amendments to the agreements, or other communications transmitted to you from time to time electronically.</li>
                                                                    <li>You agree that we may use the email address provided in your application for a User Profile or such other email address as you notify to us from time to time to provide such information to you. Any electronic communication will be deemed to have been received by you when it is transmitted by us.</li>
                                                                    <li>Access to our Platform is at our absolute discretion. You acknowledge that access to our Platform may be interrupted and the Services may be unavailable in certain circumstances.</li>
                                                                </ol>
                                                            </li>

                                                            <li><b>RISK WARNINGS</b>
                                                                <ol>
                                                                    <li>You acknowledge and agree that:
                                                                        <ol>
                                                                            <li>investing in Digital Currency is unlike investing in traditional currencies, goods or commodities: it involves
                                                                                significant and exceptional risks and the losses can be substantial. You should carefully consider and assess
                                                                                whether trading or holding of digital currency is suitable for you depending upon your financial condition,
                                                                                tolerance to risk and trading experience and consider whether you should take independent financial
                                                                                advice; </li>
                                                                            <li>we have not advised you to, or recommended that you should, use the Platform and/or Services, or trade
                                                                                and/or hold Digital Currency;</li>
                                                                            <li>unlike other traditional forms of currency, Digital Currency is decentralised and is not backed by a central
                                                                                bank, government or legal entities. As such, the value of Digital Currency can be extremely volatile and may
                                                                                swing depending upon the market, confidence of investors, competing currencies, regulatory
                                                                                announcements or changes, technical problems or any other factors. We give no warranties or
                                                                                representations as to the future value of any Digital Currency and accept no liability for any change in value
                                                                                of your Portfolio; and
                                                                            </li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You acknowledge that we do not issue or deal in Financial Products or provide any financial advice and no offer or
                                                                        other disclosure document has been, or will be, prepared in relation to the Services, the Platform and/or any of the
                                                                        Digital Currencies, under the Financial Markets Conduct Act 2013, the Financial Advisers Act 2008 or any other similar
                                                                        legislation in any jurisdiction.</li>
                                                                </ol>
                                                            </li>

                                                            <li><b>DISPUTE RESOLUTION</b>
                                                                <ol>
                                                                    <li>You will promptly inform us via the Platform and/or by email of any complaint you have regarding the standard of service we provide to you. We will promptly respond to any complaint we receive from you.</li>
                                                                    <li>If you are unsatisfied with our response, you may direct any complaints to:<br>
                                                                        <b>Financial Services Complaints Limited (FSCL)</b><br>
                                                                        PO Box 5967, Lambton Quay<br>
                                                                        Wellington, 6145<br> 
                                                                        Email: info@fscl.org.nz<br>
                                                                        FSCL is our independent external dispute resolution scheme that has been approved by the Minister of Consumer Affairs under the Financial Service Providers (Registration and Dispute Resolution) Act 2008. This service costs you nothing.
                                                                    </li>
                                                                </ol>
                                                            </li>

                                                            <li><b>USE AND DISCLOSURE OF INFORMATION</b>
                                                                <ol>
                                                                    <li>You authorize us to:
                                                                        <ol>
                                                                            <li>Collect, hold and disclose personal information about you for the purpose of providing Services to you, creating your User Profile and for our own marketing purposes;</li>
                                                                            <li>Aggregate and anonymize your data along with that of other users, and use that data ourselves or sell or supply that anonymized data to other financial service providers for marketing, product design and other commercial purposes;</li>
                                                                            <li>Keep records of all information and instructions submitted by you via the Platform or by email;</li>
                                                                            <li>Record all telephone conversations with you;</li>
                                                                            <li>Record and identify the calling telephone from which you instruct us;</li>
                                                                            <li>Record and retain copies of all information and documents for the purposes of the various Financial Market Regulations under which we may operate;</li>
                                                                            <li>Obtain credit information concerning you if we consider it relevant to determine whether to agree to perform Services or administer your User Profile.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You agree to give us any information we ask you for if we (or any affiliates or third parties with whom you are dealing with through us) believe we need it in order to comply with any laws in New Zealand or overseas. You agree that we can use information that we have about you to:
                                                                        <ol>
                                                                            <li>Assess whether we will provide you with a User Profile;</li>
                                                                            <li>Provide you with, or manage any of, our Services;</li>
                                                                            <li>Comply with any laws in New Zealand or overseas applying to us or the Services we provide to you; or</li>
                                                                            <li>Compare with publicly available information about you or information held by other reputable companies or organizations we have a continuing relationship with, for any of the above reasons.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You agree that we can obtain information about you from or give your information to any of the following people or organizations:
                                                                        <ol>
                                                                            <li>Our agents or third parties (whether in New Zealand or overseas) that provide services to, through or via us such as execution, data hosting (including cloud-based storage providers) and processing, tax services, anti-money laundering services or support services; or</li>
                                                                            <li>A regulator or exchange for the purposes of carrying out its statutory functions.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You agree that where required to help us comply with laws in New Zealand or overseas or if we believe giving the information will help prevent fraud, money laundering or other crimes, we may give information we hold about you to others including:
                                                                        <ol>
                                                                            <li>Police or government agencies in New Zealand and overseas; or</li>
                                                                            <li>The issuers of Financial Products in order for them to satisfy their obligations under New Zealand anti-money laundering laws and regulations.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>We may not be allowed to tell you if we do give out information about you. We are not responsible to you or anyone else if we give information for the purposes above. We will not disclose information about you except as authorized by you or as required or authorized by law.</li>
                                                                </ol>
                                                            </li>
                                                            <li><b>USE AND DISCLOSURE OF INFORMATION</b>
                                                                <ol>
                                                                    <li>You have rights of access to, and correction of, personal information supplied to and held by us.</li>
                                                                </ol>
                                                            </li>
                                                            <li><b>ANTI-MONEY LAUNDERING</b>
                                                                <ol>
                                                                    <li>We may need to identify you in order to comply with laws in New Zealand and overseas.</li>
                                                                    <li>We are required to comply with all applicable New Zealand or overseas anti-money laundering laws and regulations and may ask for information identifying you, and then verification for such identity, including references and written evidence. We may ask you for details of the source or destination of your funds.</li>
                                                                    <li>You agree to complete our identification and verification processes in relation to your identity and personal information to our satisfaction. We reserve the right to refuse to provide you Services or to cancel your User Profile if this information is not provided on request.</li>
                                                                    <li>
                                                                        You accept that there may be circumstances where we are required to suspend or disable your User Profile to meet
                                                                        our anti-money laundering obligations.
                                                                    </li>
                                                                    <li>You agree that we may use personal information provided by you for the purpose of electronic identity verification using third party contractors and databases including the Department of Internal Affairs, NZ Transport Agency, Companies Office, electronic role, a credit reporting agency or other entity for that purpose.</li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>LIMITATION OF LIABILITY</b>
                                                                <ol>
                                                                    <li>You agree that where our Services are acquired for business purposes, or where you hold yourself out as acquiring our Services for business purposes, the Consumer Guarantees Act 1993 (?the CGA?) will not apply to any supply of products or services by us to you. Nothing in these Terms and Conditions will limit or abrogate your rights and remedies under the CGA except to the extent that contracting out is permitted under the CGA and all provisions of these Terms and Conditions will be modified to the extent necessary to give effect to that intention.</li>
                                                                    <li>Subject to any terms implied by law which cannot be excluded and in the absence of our fraud or willful default, we will not be liable in contract, tort (including negligence), equity, or otherwise for any direct, indirect, incidental, consequential, special or punitive damage, or for any loss of profit, income or savings, or any costs or expenses incurred or suffered by you or any other person in respect of Services supplied to you or in connection with your use of our Platform.</li>
                                                                    <li>You acknowledge that:
                                                                        <ol>
                                                                            <li>Our advice may be based on information provided to us by you or by third parties which may not have been independently verified by us (?Information from Third Parties?);</li>
                                                                            <li>We are entitled to rely on Information from Third Parties and we are under no obligation to verify or investigate that information. We will not be liable under any circumstances where we rely on Information from Third Parties;</li>
                                                                            <li>Our Services do not include tax advice. We recommend that you consult your tax adviser before making a decision to invest or trade in Financial Products;</li>
                                                                            <li>Without limiting any obligations we have under the various Financial Market Regulations, it is your responsibility to:
                                                                                <ol>
                                                                                    <li>Satisfy yourself that our Crypto Traded Portfolios are appropriate to your circumstances; and</li>
                                                                                    <li>Make further enquiries as should reasonably be made by you before making a decision to invest in Crypto Currency Portfolios.</li>
                                                                                </ol>
                                                                            </li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>We will be under no liability for any loss or expense which arises as a result of a delay by us in executing a Transaction via the Platform (due to a network outage, system failure or otherwise or for any reason whatsoever).</li>
                                                                    <li>We will not be liable for any failure to provide products or services to you or to perform our obligations to you under these Terms and Conditions if such failure is caused by any event of force majeure beyond our reasonable control, or the reasonable control of our employees, agents or contractors. For the purposes of this clause, an event of force majeure includes (but is not limited to) a network outage, an inability to communicate with other financial providers, brokers, financial intermediaries, a failure of any computer dealing or settlement system, an inability to obtain the necessary supplies for the proper conduct of business, and the actions or failures of any counterparty or any other broker or agent, or the systems of that broker or agent.</li>
                                                                    <li>The provisions of this clause 14 will extend to all our employees, agents and contractors, and to all corporate entities
                                                                        in which we may have an interest and to all entities which may distribute our publications.</li>
                                                                    <li>Despite anything else in these Terms and Conditions, if we are found to be liable for any loss, cost, damage or
                                                                        expense arising out of or in connection with your use of the Platform or the Services or these Terms and Conditions,
                                                                        our maximum aggregate liability to you will be limited to two times the total amount of fees and charges that you
                                                                        have paid to us in the previous twelve months in accordance with clause 15 below.</li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>FEES AND CHARGES FOR SERVICES</b>
                                                                <ol>
                                                                    <li>We may charge an annual fee for accessing the Platform and associated services. Currently no such fee is charged. </li>
                                                                    <li>Each Portfolio will charge various fees for investing into that portfolio. All portfolio fees are automatically deducted from the assets held within the Portfolio each month, these will not be charged to you directly. Each portfolio may charge, and must pay to us, on demand, the following fees and charges ("Fees"):
                                                                        <ol>
                                                                            <li>Crypto-Currency Portfolio Services:</li>
                                                                            <table class="table-bordered">
                                                                                <tbody><tr>
                                                                                        <td>Portfolio Management Fee</td>
                                                                                        <td>Description</td>
                                                                                        <td>Other Expenses</td>
                                                                                        <td>Performance Fee</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Up to 3% per annum of all funds invested (plus GST if any). Fees may be changed from time to time at our absolute discretion.</td>
                                                                                        <td>Based on total investment value, charged monthly</td>
                                                                                        <td>In operating the Portfolios and offering this service to you, we may incur other expenses such as transaction fees, bank charges, audit and legal fees. Such expenses will be an addition fees these are charged to the Portfolios. </td>
                                                                                        <td>We may change a performance fee on some Portfolios, at a rate of up to 30% of outperformance against given hurdles. </td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You must also pay for any other fees and charges for any add on services you obtain via the Platform or as specified
                                                                        in these Terms and Conditions.</li>
                                                                    <li>All Fees are automatically debited monthly in arrears from each Portfolio. You agree that we have the absolute right of sale of investments in each Portfolio to meet all amounts due to us.</li>
                                                                </ol>
                                                            </li>

                                                            <li>
                                                                <b>TERMINATION OF YOUR USER PROFILE</b>
                                                                <ol>
                                                                    <li>Either you or we may cancel your User Profile at any time. If we cancel your User Profile we will notify you by email.   If you wish to cancel your User Profile you may do so using the facility available on the Platform.</li>
                                                                    <li>Examples of when we will cancel your User Profile include (but are not limited to) where:
                                                                        <ol>
                                                                            <li>you are insolvent or in liquidation or bankruptcy; or</li>
                                                                            <li>you have not paid Fees or other amounts due under these Terms and Conditions by the due date</li>
                                                                            <li>you gain or attempt to gain unauthorised access to the Platform or another member?s User Profile or Wallet;</li>
                                                                            <li>we consider any conduct by you (whether or not that conduct is related to the Platform or the Services) puts
                                                                                the Platform, the Services or other users at risk; </li>
                                                                            <li>you use or attempt to use the Platform in order to perform illegal or criminal activities;</li>
                                                                            <li>your use of the Platform is subject to any pending investigation, litigation or government proceeding;
                                                                            </li>
                                                                            <li>you fail to pay or fraudulently pay for any transactions;</li>
                                                                            <li>you breach these Terms and Conditions and, where capable of remedy, fail to remedy such breach within 30
                                                                                days? written notice from us specifying the breach and requiring it to be remedied;</li>
                                                                            <li>your conduct may, in our reasonable opinion, bring us into disrepute or adversely affect our reputation or
                                                                                image; and/or</li>
                                                                            <li>we receive a valid request from a law enforcement or government agency to do so.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>We may also terminate these Terms and cease to provide the Services and the Platform if we undergo an insolvency
                                                                        event, meaning that where that party becomes unable to pay its debts as they fall due, or a statutory demand is
                                                                        served, a liquidator, receiver or manager (or any similar person) is appointed, or any insolvency procedure under
                                                                        the Companies Act 1993 is instituted or occurs. </li>
                                                                    <li>If either you or we terminate your User Profile you will still be responsible for any Transaction made up to the time of termination, and Fees for Services rendered to you and our rights under these Terms and Conditions in respect of those matters will continue to apply accordingly.</li>
                                                                    <li>You agree that we will not be liable for any loss you suffer where we act in accordance with this clause.</li>
                                                                    <li>On termination of your User Profile, we will redeem all of the investments held in your Portfolio and transfer the
                                                                        proceeds of sale (less any applicable Fees) to a bank account nominated by you. </li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>ASSIGNMENT</b>
                                                                <ol>
                                                                    <li>You agree that these Terms and Conditions bind you personally and you may not assign any of your rights or obligations under it. Any such purported assignment will be ineffective.</li>
                                                                    <li>We may assign all or any of our rights, and transfer all or any of our obligations under these Terms and Conditions to any person, including a purchaser of the Platform or all or substantially all of our business.</li>
                                                                </ol>
                                                            </li>
                                                            <li><b>INDEMNITY</b>
                                                                <ol>
                                                                    <li>You must, on demand being made by us and our partners, affiliated persons, officers and employees, indemnify those persons against any and all losses, costs, claims, damages, penalties, fines, expenses and liabilities: 
                                                                        <ol>
                                                                            <li>in the performance of their duties or exercise of their authorities, except to the extent arising as a result of their own negligence, fraud or willful default; and</li>
                                                                            <li>which they may incur or suffer as a result of:
                                                                                <ol>
                                                                                    <li>relying in good faith on, and implementing instructions given by any person using your User Profile, unless there are reasonable grounds for us to doubt the identity or authority of that person; and</li>
                                                                                    <li>relying in good faith on information you have either provided to us or made available to us.</li>
                                                                                </ol>
                                                                            </li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>If any person who is not you (except for the Financial Markets Authority or any other regulatory authority of competent jurisdiction) makes any claim, or brings any proceedings in any Court, against us in connection with Services we provide to you, you will indemnify us for and against, and pay to us on demand, all legal costs and other expenses that we incur in connection with that claim or proceeding.</li>
                                                                    <li>You must also indemnify us and our partners, affiliated persons, officers and their respective employees, agents and contractors in the case of any portfolio investment entity tax liability required to be deducted (at the Prescribed Investor Rate nominated by you or us) from your investment, even if that liability exceeds the value of their investments, or any incorrect notification or failure to notify or update annually your PIR or tax rates.</li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>AMENDMENTS</b>
                                                                <ol>
                                                                    <li>We may, at our sole discretion, amend these Terms and Conditions (including our Fees) by giving ten working days' prior notice to you either by:
                                                                        <ol>
                                                                            <li>Notice on our website; or</li>
                                                                            <li>Direct communication with you via email,</li>
                                                                        </ol>
                                                                        unless the change is immaterial (e.g. drafting and typographical amendments) or we are required to make the change sooner (e.g. for regulatory reasons), in which case the changes will be made immediately.
                                                                    </li>
                                                                    <li>You may request a copy of our latest Terms and Conditions by contacting us via the Platform or email.</li>
                                                                    <li>If you access the Platform or otherwise use our Services after the expiry of the notice given in accordance with clause 17.1 you will be deemed to have accepted the amended Terms and Conditions.</li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>NOTICES</b>
                                                                <ol>
                                                                    <li>Any notice or other communication ("Notice") given for the purposes of these Terms and Conditions:
                                                                        <ol>
                                                                            <li>Must be in writing; and</li>
                                                                            <li>Must be sent to the relevant party?s email address.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>Any notice is deemed served or received on the day it is sent to the correct email address.</li>
                                                                    <li>Any notice that is served on a Saturday, Sunday or public holiday is deemed to be served on the first working day after that.</li>
                                                                    <li>A notice may be given by an authorized officer, employee or agent.
                                                                        <ol>
                                                                            <li>Notice may be given personally to a director, employee or agent of the party at the party?s address or to a person who appears to be in charge at the time of delivery or according to section 387 to section 390 of the Companies Act 1993.</li>
                                                                            <li>If the party is a natural person, partnership or association, the notice may be given to that person or any partner or responsible person. If they refuse to accept the notice, it may be brought to their attention and left in a place accessible to them.</li>
                                                                        </ol>
                                                                    </li>
                                                                </ol>                
                                                            </li>

                                                            <li><b>GOVERNING LAW AND JURISDICTION</b>
                                                                <ol>
                                                                    <li>These Terms and Conditions are governed by and construed according to the current laws of New Zealand. The parties agree to submit to the non-exclusive jurisdiction of the Courts of New Zealand.</li>
                                                                    <li>If you bring any claim or proceeding against us in any Court which is not a Court of New Zealand, you will indemnify us for and against, and pay to us on demand, all legal costs and other expenses that we incur in connection with that claim or proceeding.</li>
                                                                </ol>
                                                            </li>

                                                            <li><b>DEFINITIONS</b><br>
                                                                <b>"Account"</b> means the Client Property and Client Money we hold for you and represents an entry in your name on the
                                                                general ledger of ownership of Digital Currency and money maintained and held by<br> 

                                                                <!-- <b>"Authorized Financial Adviser"</b> has the same meaning as in section 51 of the Financial Advisers Act.</br> -->

                                                                <b>"Crypto Traded Portfolio Services"</b> means the services described in clause 2.1.<br>

                                                                <b>"Client"</b> means the person in whose name a User Profile has been opened.<br>
                                                                <b>"Client Money"</b> has the meaning given to it in clause 7.6.<br>
                                                                <b>"Client Property"</b> has the meaning given to it in clause 7.5.<br>
                                                                <b>"Digital Currency"</b> means the supported tokens or cryptocurrencies offered on the Platform.<br>

                                                                <b>"Fees"</b> means any fees or other charges charged for the Services, including, but not limited to the fees set out in clause 15.<br> 

                                                                <!-- <b>"Financial Advisers Act"</b> means the Financial Advisers Act 2008.</br> -->

                                                                <!-- <b>"Financial Adviser Service"</b> has the same meaning as in section 9 of the Financial Advisers Act.</br>  -->
                                                                <!-- <b>"Financial Product"</b> has the same meaning as in section 7 of the Financial Markets Conduct Act 2013.</br>  -->
                                                                <b>"Minor"</b> means a person under the age of 18.<br>
                                                                <b>"Platform"</b> means the Invsta Investment Platform. (www.invsta.com) and any associated variations of this website <br>

                                                                <b>"Portfolio"</b> means a portfolio of assets that is managed by Invsta via the Platform. <br>
                                                                <b>"Portfolio Manager"</b> means Invsta and associated people responsible for managing your Portfolio.<br>

                                                                <b>"Services"</b> means a Service we provide to you via our Platform including the Crypto Traded Portfolio Services <br>

                                                                <b>"Terms and Conditions"</b> means these Terms and Conditions.<br>

                                                                <b>"Transaction"</b> means a transaction effected or to be effected using the Platform pursuant to your instructions.<br>

                                                                <b>"User Profile"</b> means a User Profile in your name created by you in accordance with these Terms and Conditions through which you are entitled to gain access to our Platform.<br>

                                                            </li>

                                                            <li><b>GENERAL INTERPRETATION</b>
                                                                <ol>
                                                                    <li>In these Terms and Conditions:
                                                                        <ol>
                                                                            <li>Unless the context otherwise requires, references to:
                                                                                <ol>
                                                                                    <li>"we", "us", Invsta, and ?Ilumony? refer to Ilumony Limited, trading as Invsta, and related companies (as defined in section 2(3) of the Companies Act 1993); and</li>
                                                                                    <li>?you?, ?your? and ?yourself? are references to the Client and where appropriate any person who you have advised us are authorized to act on your behalf.</li>
                                                                                </ol>
                                                                            </li>
                                                                            <li>A reference to these Terms and Conditions (including these Terms and Conditions) includes a reference to that agreement as novated, altered or replaced from time to time;</li>
                                                                            <li>A reference to a party includes the party?s administrators, successors and permitted assigns;</li>
                                                                            <li>Words in the plural include the singular and vice versa;</li>
                                                                            <li>Headings are inserted for convenience only and will be ignored in construing these Terms and Conditions;</li>
                                                                            <li>References to any legislation includes statutory regulations, rules, orders or instruments made pursuant to that legislation and any amendments, re-enactments, or replacements; and</li>
                                                                            <li>Expressions referring to writing will be construed as including references to words printed, typewritten, or by email or otherwise traced, copied or reproduced.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>These Terms and Conditions are intended to benefit and be enforceable by Invsta Limited and any related companies (as defined in section 2(3) of the Companies Act 1993) in accordance with the Contracts (Privity) Act 1982.</li>
                                                                </ol>
                                                            </li>

                                                        </ol>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top:20px">
                                                <div class="col-sm-12" style="text-align: center">
                                                    <a href="./resources/images/pdf/Crypto-T&C.pdf" style=" color: #011c53;  font-size: 14px; font-weight: bold; padding: 10px;
                                                       text-decoration: underline;" target="black"> Download Client Terms and Conditions</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 condition-details">
                                                    <input class="checkbox-check" id="term_condition" type="checkbox" name="term_condition">
                                                    <label class="label_input" style="text-align:left">
                                                        I have read and agree to the Terms and Conditions
                                                    </label>
                                                </div>
                                                <span class="error" id="error_term_condition" ></span>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 condition-details">
                                                    <input class="checkbox-check"  id="authorized_condition" type="checkbox" name="authorized_condition">
                                                    <label class="label_input" style="text-align:left">
                                                        I am authorized to accept and act on behalf of all account holders
                                                    </label>
                                                </div>
                                                <span class="error" id="error_authorized_condition" ></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous11 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next12 action-button" value="Accept" />
                            </fieldset>
                            <fieldset id="step13">
                                <div class="content-section">
                                    <div class="row">
                                        <div class="col-md-12 thanku">
                                            <div class="element-wrapper">
                                                <h5 class="element-header">
                                                    Thank for submitting your application details, we'll let you know if we require anything further.
                                                </h5>
                                            </div>

                                            <h5 class="element-header3">
                                                To continue to your account and make an investment, please verify your email address by clicking on the link in the email we have sent.
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>

                                <div class="col-md-12" style="max-width: 113px;margin: auto;">
                                    <div class="ok-submit">
                                        <input type="button" name="previous" id="submit" class=" action-button-previous new-ok-btn " value="OK" />
                                    </div>
                                </div>

                                <!--<input type="button" name="next" class="next12 action-button" value="Accept" />--> 
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
        <script src='https://code.jquery.com/ui/1.12.1/jquery-ui.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
        <script src="./resources/js/index.js"></script>        
        <script src="./resources/js/intlTelInput_1.js"></script>
        <script src="./resources/js/intlTelInput_2.js"></script>
        <script src="./resources/js/intlTelInput_3.js"></script>
        <script src="./resources/js/user-main.js"></script>
        <script type="text/javascript" src="./resources/js/countries.js"></script>
        <script src="./resources/js/sweetalert.min.js"></script>
        <script>
                                    function initAutocomplete() {
                                        autocomplete = new google.maps.places.Autocomplete(
                                                (document.getElementById('address')),
                                                {types: ['address'], componentRestrictions: {country: 'nz'}});
                                        autocomplete1 = new google.maps.places.Autocomplete(
                                                (document.getElementById('address1')),
                                                {types: ['address'], componentRestrictions: {country: 'nz'}});
                                        
                                    }
        </script>
        <script>
            var date = new Date();
            var day = date.getDate();
            var month = date.getMonth();
            var year = date.getFullYear();
            adultDOB = date.setFullYear(year - 18, month, day);
            var moreInvestorArr = [];
            var investors = [];
            var current_investor = {};
            var step = 0, y = 0, idx = 0;
            <c:forEach items="${joint.getMoreInvestorList()}" var="moreInvestor" varStatus="list">
            current_investor = ${moreInvestor};
            moreInvestorArr.push(current_investor);
            investors.push(current_investor);
            //                idx = ${list.index};
            </c:forEach>
            $(".countryname").intlTelInput_2();
            $(".countrynameoutnz").intlTelInput_3();
            $(".codenumber").intlTelInput_1();
            $(".more-investor-codenumber").intlTelInput_1();
        </script>
        <script>
            $('.more_licence_first_name').keyup(function () {
                $('.error_more_licence_first_name').text('');
            });
            $('.more_licence_last_name').keyup(function () {
                $('.error_more_licence_last_name').text('');
            });
            $('.more_passport_first_name').keyup(function () {
                $('.error_passport_first_name').text('');
            });
            $('.more_passport_last_name').keyup(function () {
                $('.error_passport_last_name').text('');
            });
            function removeDate() {
                $('.error').text('');
            }
            function prev7(ele) {
                prev(ele);
            }
            function next7(ele) {
//                var curr = $(ele).parent();
//                var idc = '#' + curr.attr('id');
//                var investor_dob = $(idc + " .more-investor-dob").val();
//                var selectOccupation = $(idc + " .more-investor-occupation").val();
//                var inputOccupation = $(idc + " .more-input-occupation").val();
//                var country = $(idc + " .more-investor-countryname").val();
//                country = country.trim();
//                if (investor_dob === "") {
//                    $(".error-more-dob").text("This field is required");
//                } else if (country === "-Select-") {
//                    $(".error-countryOptions").text("This field is required");
//                } else if (selectOccupation === "-Select-" || selectOccupation === "0" && inputOccupation === "") {
//                    $(".error-more-occupation").text("This field is required");
//                } else {
                    $("#step").attr("value", 8);
                    next(ele);
//                }
            }
            function prev8(ele) {
                prev(ele);
            }
            function next8(ele) {
//                var curr = $(ele).parent();
//                var idc = '#' + curr.attr('id');
//                var investor_address = $(idc + " .more-investor-address").val();
//                var investor_mobile = $(idc + " .more-investor-mobile").val();
//                if (investor_address === "") {
//                    $(".error-more-address").text("This field is required");
//                } else if (investor_mobile === "") {
//                    $(".error-more-mobile").text("This field is required");
//                } else {
                    $("#step").attr("value", 9);
                    next(ele);
//                }
            }

            function prev9(ele) {
                prev(ele);
            }
            function next9(ele) {
//                var curr = $(ele).parent();
//                var idc = '#' + curr.attr('id');
//                var url = '';
//                var firstName = '';
//                var middleName = '';
//                var lastName = '';
//                var License_number = '';
//                var licence_expiry_Date = '';
//                var licence_verson_number = '';
//                var passport_number = '';
//                var passport_expiry = '';
//                var index = $(idc + " .src_of_fund2 option:selected").val();
//                if (index === "1") {
//                    url = './rest/groot/db/api/dl-verification';
//                    var licenseFirstName = $(idc + " .more_licence_first_name").val();
//                    firstName = licenseFirstName;
//                    var licenseLastName = $(idc + " .more_licence_last_name").val();
//                    var licenseNumber = $(idc + " .more-investor-licenseNumber").val();
//                    lastName = licenseLastName;
//                    licenseNumber = licenseNumber.trim();
//                    License_number = licenseNumber;
//                    var Expirydate = $(idc + " .more-investor-licenseExpiryDate").val();
//                    Expirydate = Expirydate.trim();
//                    licence_expiry_Date = Expirydate;
//                    licence_verson_number = $(idc + " .more-investor-versionNumber").val();
//                    licence_verson_number = licence_verson_number.trim();
//                    if (licenseFirstName === "") {
//                        $(".error_more_licence_first_name").text("This field is required ");
//                    } else if (licenseLastName === "") {
//                        $(".error_more_licence_last_name").text("This field is required ");
//                    } else if (licenseNumber === "") {
//                        $(".error-more-licenseNumber").text("This field is required ");
//                    } else if (Expirydate === "") {
//                        $(".error-more-licenseExpiryDate").text("This field is required ");
//                    } else if (licence_verson_number === "") {
//                        $(".error-more-versionNumber").text("This field is required ");
//                    } else {
//                        $("#step").attr("value", 10);
//                        next(ele);
//                    }
//                } else if (index === "2") {
//                    url = './rest/groot/db/api/pp-verification';
//                    var more_passport_first_name = $(idc + " .more_passport_first_name").val();
//                    firstName = more_passport_first_name;
//                    var more_passport_last_name = $(idc + " .more_passport_last_name").val();
//                    lastName = more_passport_last_name;
//                    var Passportnumber = $(idc + " .more-investor-passportNumber").val();
//                    Passportnumber = Passportnumber.trim();
//                    passport_number = Passportnumber;
//                    var dob2 = $(idc + " .more-investor-passportExpiryDate").val();
//                    passport_expiry = dob2;
//                    var passport_section = $(idc + " .more-investor-passportCountryOfIssue").val();
//                    //                     alert(passport-section);
//                    dob2 = dob2.trim();
//                    if (more_passport_first_name === "") {
//                        $(".error_passport_first_name").text("This field is reqired ");
//                    } else if (more_passport_last_name === "") {
//                        $(".error_passport_last_name").text("This field is reqired ");
//                    } else if (Passportnumber === "") {
//                        $(".error-more-passportNumber").text("This field is reqired ");
//                    } else if (dob2 === "") {
//                        $(".error-more-passportExpiryDate").text("This field is reqired ");
//                    } else if (passport_section === " -Select-") {
//                        $(".error-more-investor-passportCountryOfIssue").text("This field is reqired ");
//                    } else {
//                        $("#step").attr("value", 10);
//                        next(ele);
//                    }
//                } else {
//                    var TypeofID = $(idc + " .more-investor-typeOfId").val();
//                    var IdExpirydate = $(idc + " .more-investor-typeOfIdExpiryDate").val();
//                    var IdDocument = $(idc + " .more-investor-file").val();
//                    var more_other_issueBy = $(idc + " .more-investor-typeOfIdCountryOfIssue").val();
//                    TypeofID = TypeofID.trim();
//                    IdExpirydate = IdExpirydate.trim();
//                    if (TypeofID === "") {
//                        $(".error-more-typeOfId").text("This field is reqired ");
//                    } else if (IdExpirydate === "") {
//                        $(".error-more-typeOfIdExpiryDate").text("This field is reqired ");
//                    } else if (more_other_issueBy === " -Select-") {
//                        $(".error-more-investor-typeOfIdCountryOfIssue").text("This field is reqired ");
//                    } else if (IdDocument === "") {
//                        $(".error-more-file").text("Please attach the Document");
//                    } else {
                        $("#step").attr("value", 10);
                        next(ele);
//                    }
//                }
//                var Date_of_Birth = $('#dob').val();
//                var DataObj = {license_number: License_number, licence_expiry_Date: licence_expiry_Date,
//                    licence_verson_number: licence_verson_number, date_of_Birth: Date_of_Birth, firstName: firstName,
//                    middleName: middleName, lastName: lastName, passport_number: passport_number, passport_expiry: passport_expiry};
//                console.log(DataObj);
//                $.ajax({
//                    type: 'POST',
//                    url: url,
//                    headers: {"Content-Type": 'application/json'},
//                    data: JSON.stringify(DataObj), success: function (data, textStatus, jqXHR) {
//                        console.log(data);
//                        var obj = JSON.parse(data);
//                        if (index === "1") {
//                            if (obj.driversLicence.verified) {
//                                $('#more_investor_verify').val('true');
//                            }
//                        } else if (index === "2") {
//                            if (obj.passport.verified) {
//                                $('#more_investor_verify').val('true');
//                            } else {
//                                //                                alert("wrong data");
//                            }
//
//                        }
//                    },
//                    error: function (jqXHR, textStatus, errorThrown) {
//                        //                        alert(" inside error" + jqXHR);
//                    }
//                });
            }
            function prev10(ele) {
                prev(ele);
            }
            function next10(ele) {
//                var curr = $(ele).parent();
//                var idc = '#' + curr.attr('id');
//                var irdno = $(idc + " .more-investor-irdno").val();
//                var errorIrdNo = $(idc + " .error_more_irdNumber");
//                var countryOfTax = $(idc + " .more-investor-countryOfTaxResidence").val();
//                countryOfTax = countryOfTax.trim();
//                var USCitizen = $(idc + " .more-investor-usCitizen option:selected").val();
//                var j = 0;
//                if (irdno.length !== 11) {
//                    errorIrdNo.html("IRD number must contain 9 digits (if less than this, enter a 0 for the first number on the left)");
//                }
////                else if (countryOfTax === "-Select-") {
////                    errorCountryOfTax.html("This field is required");
////                } else if (tin === "" && reasonTin === "") {
////                    errorTin.html("This field is required");
////                    errorReasonTin.html("This field is required");
////                }
//                else if (USCitizen === "2") {
//                    var tindivs = $(curr).find('.checktindata1');
//                    for (var i = 0; i < tindivs.length; i++) {
//                        var tindiv = tindivs[i];
//                        var tinnum = tindiv.getElementsByClassName('more-investor-TIN')[0];
//                        var tinerror = tindiv.getElementsByClassName('more-investor-TIN-error')[0];
//                        var country = tindiv.getElementsByClassName('countrynameoutnz')[0];
//                        var countryerror = tindiv.getElementsByClassName('error-more-investor-tex_residence_Country')[0];
//                        var reason = tindiv.getElementsByClassName('more-investor-resn_tin_unavailable')[0];
//                        var reasonerror = tindiv.getElementsByClassName('error-resn_tin_unavailable')[0];
//                        if (country.value === " -Select-") {
//                            countryerror.innerHTML = "This field is required ";
//                        } else if (tinnum.value === "" && reason.value === "") {
//                            reasonerror.innerHTML = "This field is required ";
//                            tinerror.innerHTML = "This field is required ";
//                        } else {
//                            j++;
//                        }
//                    }
//                    if (tindivs.length > j) {
//                    } else {
//                        $("#step").attr("value", 12);
//                        next(ele);
//                    }
//                } else {
                    $("#step").attr("value", 11);
                    next(ele);
//                }
            }
            function selectOccupation(ele) {
                $('.error').text('');
                var curr = $(ele).closest('.morestep1');
                var idc = '#' + curr.attr('id');
                var Occupation = $(idc + ' .more-select-occupation').val();
                var OccupationStatus = $(idc + ' more-select-occupation  option:selected').text();
                if (Occupation === "0") {
                    $(idc + ' .more-input-occupation').val('');
                    $(idc + ' .more-input-occupation').show();
                } else {
                    $(idc + ' .more-input-occupation').val(OccupationStatus);
                    $(idc + ' .more-input-occupation').hide();
                }
            }
            function sameAddress(ele) {
                var curr = $(ele).closest('.morestep2');
                var idc = '#' + curr.attr('id');
                var address = $('#address').val();
                $(idc + " .more-investor-address").val(address);
            }
            function prev(ele) {
                var currfs = $(ele).parent();
                var prevfs = $(ele).parent().prev();
                var cidc = currfs.attr('id');
                var pidc = prevfs.attr('id');
                var hasCls = prevfs.hasClass('more-investor-fs');
                if (cidc === 'step11' || cidc === 'morestep1' || cidc === 'morestep2' || cidc === 'morestep3' || cidc === 'morestep4') {
                    var idx = document.getElementsByClassName('morestep1').length - 2;//2=>1
                    var s6x = '#step6' + idx;
                    var chk = $(s6x + '1').find('.more-investor-radio').val();
                    if (chk === '2') {
                        $(currfs).hide();
                        var btn = prevfs.find('input[name="previous"]');
                        prev(btn);
                    } else if (chk === '1') {
                        $(currfs).hide();
                        $(s6x + '4').show();
                    } else {
                        $(currfs).hide();
                        $('#step6').show();
                    }
                } else if (hasCls === true) {
                    pidc = pidc.substring(0, pidc.length - 1);
                    var chk = $('#' + pidc + '1').find('.more-investor-radio').val();
                    if (chk === '2') {
                        var btn = prevfs.find('input[name="previous"]');
                        $(currfs).hide();
                        prev(btn);
                    } else {
                        $(currfs).hide();
                        $(prevfs).show();
                    }
                } else {
                    $(currfs).hide();
                    $(prevfs).show();
                }
            }
            function next(ele) {
                var currfs = $(ele).parent();
                var nextfs = $(ele).parent().next();
                var idc = nextfs.attr('id');
                var hasCls = nextfs.hasClass('more-investor-fs');
                if (idc === 'morestep1') {
                    $(currfs).hide();
                    $('#step11').show();
                } else if (hasCls === true) {
//                    alert(1);
                    idc = idc.substring(0, idc.length - 1);
                    var chk = $('#' + idc + '1').find('.more-investor-radio').val();
//                    alert(chk);
                    if (chk === '2') {
                        var btn = nextfs.find('input[type="button"]');
                        $(currfs).hide();
                        next(btn);
                    } else {
                        $(currfs).hide();
                        $(nextfs).show();
                    }
                } else {
                    $(currfs).hide();
                    $(nextfs).show();
                }
            }
            $(".companyaccount").click(function () {
                $("#step").attr("value", 10);
                $("#step10").show();
                $("#step2").hide();
            });
        </script>
        <script>
            $(document).ready(function () {
                console.log('${joint}');
                $(window).keydown(function (event) {
                    if (event.keyCode === 13) {
                        event.preventDefault();
                        return false;
                    }
                });
                $('.save-new-btn').show();
                $('.director-count').hide();
                $('.passport-select').hide();
                $('.other-select').hide();
                //                $('.yes-option').hide();
                $('.yes-option2').hide();
                $('.passport-select2').hide();
                $('.other-select2').hide();
                $(".selectOcc").hide();
                $(".removefile").hide();
//                $("#step6").show();
//                $("#step1").hide(); 
                $("#select-occupation").click(function () {
                    var Occupation = $('#select-occupation').val();
                    var OccupationStatus = $('#select-occupation  option:selected').text();
                    if (Occupation === "0") {
                        $("#input-occupation").val('');
                        $("#input-occupation").show();
                    } else {
                        $("#input-occupation").val(OccupationStatus);
                        $("#input-occupation").hide();
                    }
                });
                $('#mobileNo').keyup(function () {
                    $('#error-generateOtp').text('');
                });
                $('#otp').keyup(function () {
                    $('#error-generateOtp').text('');
                });
                $("#dob").datepicker({
                    yearRange: (year - 80) + ':' + year,
                    changeMonth: true,
                    changeYear: true,
                    'maxDate': new Date(adultDOB),
                    dateFormat: 'dd/mm/yy'
                }).datepicker().attr('readonly', 'readonly');
                $(".dob1").datepicker({
                    yearRange: (year) + ':' + (year + 80),
                    changeMonth: true,
                    changeYear: true,
                    'minDate': new Date(),
                    dateFormat: 'dd/mm/yy'
                }).datepicker().attr('readonly', 'readonly');
                $(".dob").datepicker({
                    yearRange: (year - 80) + ':' + year,
                    changeMonth: true,
                    changeYear: true,
                    'maxDate': new Date(adultDOB),
                    dateFormat: 'dd/mm/yy'
                }).datepicker().attr('readonly', 'readonly');
                $('input:radio[name="senderType"]').change(function () {
                    $('#otp-block').show();
                    document.getElementById('error-loader').style.display = 'none';
                    document.getElementById('error-generateOtp').innerHTML = "Please check your Inbox";
                    $('input:radio[name="senderType"]').prop("checked", false);
                });
                $('#fourthfs-continue').click(function () {
                    var ele = $(this);
                    moveNextProcess(ele);
                });
                $('#fullName').keyup(function () {
                    $('#preferredName').val('');
                    $('.first_name').val('');
                    $('.middle_name').val('');
                    $('.last_name').val('');
                    var fn = $('#fullName').val();
                    $('#preferredName').val(fn);
                    var fnarr = fn.split(" ");
                    $('.first_name').val(fnarr[0]);
                    if (fnarr.length === 3) {
                        $('.middle_name').val(fnarr[1]);
                        $('.last_name').val(fnarr[2]);
                    } else if (fnarr.length === 2) {
                        $('.last_name').val(fnarr[1]);
                    }
                });
            <c:if test="${joint.id_type eq 'NZ Driver Licence'}">
                $('.passport-select').hide();
                $('.other-select').hide();
                $('.drivery-licence').show();
            </c:if>
            <c:if test="${joint.id_type eq 'NZ Passport'}">
                $('.passport-select').show();
                $('.other-select').hide();
                $('.drivery-licence').hide();
            </c:if>
            <c:if test="${joint.id_type eq 'Other'}">
                $('.passport-select').hide();
                $('.other-select').show();
                $('.drivery-licence').hide();
            </c:if>
            });
            clearValidationId3 = function () {
                $("#validationId3").html('');
            };
            isInviteCodeUsed = function (ele) {
                var inviteCode = $('#inviteCode').val();
                var url = './rest/cryptolabs/api/isInviteCode?ic=' + inviteCode;
                $.ajax({
                    url: url,
                    type: "GET",
                    async: false,
                    success: function (response) {
                        if (response === 'true') {
                            $("#error-inviteCode").css({'color': 'green', 'margin-left': '10px'}).html('Invite Code is valid.');
                            $("#error-inviteCode").focus();
                            moveNextProcess(ele);
                            return true;
                        } else {
                            $("#error-inviteCode").css({'color': 'red', 'margin-left': '10px'}).html('InviteCode is used or invalid.');
                            $("#error-inviteCode").focus();
                            return false;
                        }
                    },
                    error: function (e) {
                        //handle error
                    }
                });
            };
            //            if you modify these methods after first call maninderjit singh
            decStep = function () {
                step = $('[name="step"]').val();
                $("#step").attr("value", parseInt(step) - 1);
            };
            incStep = function () {
                step = $('[name="step"]').val();
                $("#step").attr("value", parseInt(step) + 1);
            };
            getMoreInvestorArr = function () {
                var gender = "male";
                var moreInvestorArray = new Array();
                var moreInvestorInfoArr = document.getElementsByClassName('morestep1');
                for (var i = 0; i < moreInvestorInfoArr.length - 1; i++) {
                    var s6x = 'step6' + i;
                    var s6xh = '#step6' + i;
                    var s6xh1 = s6xh + 1;
                    var s6xh2 = s6xh + 2;
                    var s6xh3 = s6xh + 3;
                    var s6xh4 = s6xh + 4;
//                    alert(s6xh1);
                    var title = $(s6xh1 + ' .more-investor-title option:selected').text();
                    var fname = $(s6xh1 + ' .more-investor-fname').val();
//                    alert(fname);
                    var email = $(s6xh1 + ' .more-investor-email').val();
                    var pname = $(s6xh1 + ' .more-investor-preffered-name').val();
                    var send_email = '1';
                    var dob = $(s6xh1 + ' .more-investor-dob').val();
                    var countryname = $(s6xh1 + ' .more-investor-countryname').val();
                    var occupation = $(s6xh1 + ' .more-investor-occupation').val();

                    var address = $(s6xh2 + ' .more-investor-address').val();
                    var countrycode = $(s6xh2 + ' .more-investor-countrycode').val();
                    var mobile = $(s6xh2 + ' .more-investor-mobile').val();

                    var idType = $(s6xh3 + ' .src_of_fund2 option:selected').val();
                    var more_licence_first_name = $(s6xh3 + ' .more_licence_first_name').val();
                    var more_licence_middle_name = $(s6xh3 + ' .more_licence_middle_name').val();
                    var more_licence_last_name = $(s6xh3 + ' .more_licence_last_name').val();
                    var licenseNumber = $(s6xh3 + ' .more-investor-licenseNumber').val();
                    var licenseExpiryDate = $(s6xh3 + ' .more-investor-licenseExpiryDate').val();
                    var versionNumber = $(s6xh3 + ' .more-investor-versionNumber').val();
                    var passportNumber = $(s6xh3 + ' .more-investor-passportNumber').val();
                    var passportExpiryDate = $(s6xh3 + ' .more-investor-passportExpiryDate').val();
                    var passportCountryOfIssue = $(s6xh3 + ' .more-investor-passportCountryOfIssue').val();
                    var typeOfId = $(s6xh3 + ' .more-investor-typeOfId').val();
                    var typeOfIdExpiryDate = $(s6xh3 + ' .more-investor-typeOfIdExpiryDate').val();
                    var typeOfIdCountryOfIssue = $(s6xh3 + ' .more-investor-typeOfIdCountryOfIssue').val();
                    var more_idverified = $(s6xh3 + ' .more_investor_verify').val();
                    var src_of_fund4 = $(s6xh3 + ' .src_of_fund4 option:selected').text();

                    var irdNumber = $(s6xh4 + ' .more-investor-irdNumber').val();
                    var usCitizen = $(s6xh4 + ' .more-investor-usCitizen').val();
                    var fs = document.getElementById(s6x + '4');
                    if (fs !== null && typeof fs !== "null") {
                        var countryArr = fs.getElementsByClassName('more-investor-tex_residence_Country');
                        var TINArr = fs.getElementsByClassName('more-investor-TIN');
                        var reasonArr = fs.getElementsByClassName('more-investor-resn_tin_unavailable');
                        var countryTINList = new Array();
                        for (var j = 0; j < countryArr.length; j++) {
                            var country = countryArr[j].value;
                            var tin = TINArr[j].value;
                            var reason = reasonArr[j].value;
                            countryTINList.push({country: country, tin: tin, reason: reason});
                        }
                    }
                    if (title === "Mrs" || title === "Miss") {
                        gender = "female";
                    }
                    var moreInvestor = {usCitizen: usCitizen, name: fname, fullName: fname, preferred_name: pname, title: title, Gender: gender, email: email, send_email: send_email, date_of_Birth: dob, country_residence: countryname, occupation: occupation,
                        homeAddress: address, firstName: more_licence_first_name, lastName: more_licence_last_name, middleName: more_licence_middle_name, mobile_country_code: countrycode, mobile_number: mobile, id_type: idType, license_number: licenseNumber,
                        licence_expiry_Date: licenseExpiryDate, licence_verson_number: versionNumber, passport_number: passportNumber,
                        passport_expiry: passportExpiryDate, passport_issue_by: passportCountryOfIssue, other_id_type: typeOfId, other_id_expiry: typeOfIdExpiryDate,
                        other_id_issueBy: typeOfIdCountryOfIssue, pir: src_of_fund4, ird_Number: irdNumber, countryTINList: countryTINList, investor_idverified: more_idverified};
                    console.log('[' + i + '] ' + JSON.stringify(moreInvestor));
                    moreInvestorArray.push(moreInvestor);
                }
                console.log(JSON.stringify(moreInvestorArray));
                return moreInvestorArray;
            };

            if (typeof moreInvestorArr === 'array' && moreInvestorArr.length > 0) {
                current_investor = moreInvestorArr['${joint.curr_idx}'];
                setMoreInvestor(current_investor);
            }
            $(".toggle-password").click(function () {
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") === "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });
            $(".previous1").click(function () {
                $("#step").attr("value", 1);
                $("#step2").hide();
                $("#step1").show();
            });
            $(".previous2").click(function () {
                $("#step").attr("value", 2);
                $("#step3").hide();
                $("#step2").show();
            });
            $(".previous3").click(function () {
                $("#step").attr("value", 3);
                $("#step4").hide();
                $("#step3").show();
            });
            $(".previous4").click(function () {
                $("#step").attr("value", 4);
                $("#step5").hide();
                $("#step4").show();
            });
            $(".previous5").click(function () {
                $("#step").attr("value", 5);
                $("#step6").hide();
                $("#step5").show();
            });
            $(".previous6").click(function () {
                $("#step").attr("value", 1);
                $('.save-new-btn').hide();
                $("#step7").hide();
                $("#step6").show();
            });
            $(".previous11").click(function () {
                $("#step").attr("value", 11);
                $("#step12").hide();
                $("#step11").show();
            });
            $(".previous12").click(function () {
                $("#step").attr("value", 12);
                $("#step13").hide();
                $("#step12").show();
            });
            $(".next1").click(function () {
//                var fullName = $("#fullName").val();
//                fullName = fullName.trim();
//                var occupationOption = $(".occupationOption option:selected").val();
//                var occupation = $(".selectOcc").val();
//                occupation = occupation.trim();
//                var dob = $('.dateOfBirth').val();
//                dob = dob.trim();
//                var countryResidence = $("#countryResidence").val();
//                countryResidence = countryResidence.trim();
//                if (fullName === "") {
//                    $("#span-fullName").text("This field is reqired ");
//                } else if (dob === "") {
//                    $("#span-dob-j").text("This field is reqired ");
//                } else if (countryResidence === "-Select-") {
//                    $("#span-countryResidence").text("This field is reqired ");
//                } else if (occupationOption === "-Select-" || occupationOption === "0" && occupation === "") {
//                    $("#span-occupation").text("This field is reqired ");
//                } else {
                    $("#step").attr("value", 2);
                    $("#step2").show();
                    $("#step1").hide();
//                }
            });
            $(".next2").click(function () {
//                var homeAddress = $('#address').val();
//                var mobile_number = $('.mobile_number1').val();
//                if (homeAddress === "") {
//                    $("#span-homeaddress").text("This field is reqired ");
//                } else if (mobile_number === "") {
//                    $("#span-mobileNumber").text("This field is reqired ");
//                } else {
                    $("#step").attr("value", 3);
                    $("#step3").show();
                    $("#step2").hide();
//                }

            });
            $(".next3").click(function () {
                $("#step").attr("value", 4);
                $("#step4").show();
                $("#step3").hide();
            });
            $('.first_name').keyup(function () {
                $('.error-first_name').text('');
            });
            $('.last_name').keyup(function () {
                $('.error-last_name').text('');
            });
            $('.PassFirst_name').keyup(function () {
                $('#errorPassFirst_name').text('');
            });
            $('.PassLast_name').keyup(function () {
                $('#errorPassLast_name').text('');
            });
            $('.country').keyup(function () {
                $('.error-tex-residence-Country').text('');
            });
            $(".next4").click(function () {
//                var url = '';
//                var firstName = '';
//                var middleName = '';
//                var lastName = '';
//                var License_number = '';
//                var licence_expiry_Date = '';
//                var passport_number = '';
//                var passport_expiry = '';
//                var index = $("#src_of_fund1 option:selected").val();
//                if (index === "1") {
//                    url = './rest/groot/db/api/dl-verification';
//                    var first_name = $(".first_name").val();
//                    first_name = first_name.trim();
//                    firstName = first_name;
//                    var last_name = $(".last_name").val();
//                    last_name = last_name.trim();
//                    lastName = last_name;
//                    var licenseNumber = $("#license_number").val();
//                    licenseNumber = licenseNumber.trim();
//                    License_number = licenseNumber;
//                    var Expirydate = $(".licence_expiry_Date").val();
//                    Expirydate = Expirydate.trim();
//                    licence_expiry_Date = Expirydate;
//                    var licence_verson_number = $(".licence_verson_number").val();
//                    licence_verson_number = licence_verson_number.trim();
//                    if (first_name === "") {
//                        $(".error-first_name").text("This field is reqired ");
//                    } else if (last_name === "") {
//                        $(".error-last_name").text("This field is reqired ");
//                    } else if (licenseNumber === "") {
//                        $("#span-license_number").text("This field is reqired ");
//                    } else if (Expirydate === "") {
//                        $("#error-licence_expiry").text("This field is reqired ");
//                    } else if (licence_verson_number === "") {
//                        $("#error-Version_number").text("This field is reqired ");
//                    } else {
//                        $("#step").attr("value", 5);
//                        $("#step5").show();
//                        $("#step4").hide();
//                    }
//                } else if (index === "2") {
//                    url = './rest/groot/db/api/pp-verification';
//                    var PassFirst_name = $(".PassFirst_name").val();
//                    firstName = PassFirst_name;
//                    var PassLast_name = $(".PassLast_name").val();
//                    lastName = PassLast_name;
//                    var Passportnumber = $(".passport_number").val();
//                    Passportnumber = Passportnumber.trim();
//                    passport_number = Passportnumber;
//                    var countryname11 = $("#countryname11").val();
//                    var dob2 = $(".passport_expiry").val();
//                    dob2 = dob2.trim();
//                    passport_expiry = dob2;
//                    if (PassFirst_name === "") {
//                        $("#errorPassFirst_name").text("This field is reqired ");
//                    } else if (PassLast_name === "") {
//                        $("#errorPassLast_name").text("This field is reqired ");
//                    } else if (Passportnumber === "") {
//                        $("#error-passportNumber").text("This field is reqired ");
//                    } else if (dob2 === "") {
//                        $("#error-passportExpiry").text("This field is reqired ");
//                    } else if (countryname11 === " -Select-") {
//                        $("#error_countryname11 ").text("This field is reqired ");
//                    } else {
//                        $("#step").attr("value", 5);
//                        $("#step5").show();
//                        $("#step4").hide();
//                    }
//                } else {
//                    var TypeofID = $("#TypeofID").val();
//                    var IdExpirydate = $(".other_id_expiry").val();
//                    var IdDocument = $("#other_id_myFile").val();
//                    var countryResidence1 = $("#countryResidence1").val();
//                    TypeofID = TypeofID.trim();
//                    IdExpirydate = IdExpirydate.trim();
//                    if (TypeofID === "") {
//                        $("#error-TypeofID").text("This field is reqired ");
//                    } else if (IdExpirydate === "") {
//                        $("#error-TypeofIdExpiry").text("This field is reqired ");
//                    } else if (countryResidence1 === " -Select-") {
//                        $("#error_countryResidence1").text("This field is reqired ");
//                    } else if (IdDocument === "") {
//                        $("#error_other_id_myFile").text("Please attach copy of ID document  ");
//                    } else {
                        $("#step").attr("value", 5);
                        $("#step5").show();
                        $("#step4").hide();
//                    }
//                }
//
//                var Date_of_Birth = $('#dob').val();
//                var DataObj = {license_number: License_number, licence_expiry_Date: licence_expiry_Date,
//                    licence_verson_number: licence_verson_number, date_of_Birth: Date_of_Birth, firstName: firstName,
//                    middleName: middleName, lastName: lastName, passport_number: passport_number, passport_expiry: passport_expiry};
//                console.log(DataObj);
//                $.ajax({
//                    type: 'POST',
//                    url: url,
//                    headers: {"Content-Type": 'application/json'},
//                    data: JSON.stringify(DataObj),
//                    success: function (data, textStatus, jqXHR) {
//                        console.log(data);
//                        var obj = JSON.parse(data);
//                        if (index === "1") {
//                            if (obj.driversLicence.verified) {
//                                $('#investor_verify').val('true');
//                            }
//                        } else if (index === "2") {
//                            if (obj.passport.verified) {
//                                $('#investor_verify').val('true');
//                            }
//                        }
//                    },
//                    error: function (jqXHR, textStatus, errorThrown) {
//                    }
//                });
            });
            $(".next5").click(function () {
//                var IRDNumber = $(".IRD_Number").val();
//                var USCitizen = $("#isUSCitizen option:selected").val();
//                var j = 0;
//                if (IRDNumber.length !== 11) {
//                    $("#error-irdNumber").text("IRD number must contain 9 digits (if less than this, enter a 0 for the first number on the left)");
//                } else if (USCitizen === "2") {
//                    var tindivs = document.getElementsByClassName('checktindata');
//                    for (var i = 0; i < tindivs.length; i++) {
//                        var tindiv = tindivs[i];
//                        var tinnum = tindiv.getElementsByClassName('TIN')[0];
//                        var tinerror = tindiv.getElementsByClassName('error-TIN')[0];
//                        var country = tindiv.getElementsByClassName('tex_residence_Country')[0];
//                        var countryerror = tindiv.getElementsByClassName('error-tex-residence-Country')[0];
//                        var reason = tindiv.getElementsByClassName('resn_tin_unavailable')[0];
//                        var reasonerror = tindiv.getElementsByClassName('error-resn-tin-unavailable')[0];
//                        if (country.value === " -Select-") {
//                            countryerror.innerHTML = "This field is reqired ";
//                            //                            alert("country span");
//                        } else if (tinnum.value === "" && reason.value === "") {
//                            tinerror.innerHTML = "This field is reqired ";
//                            reasonerror.innerHTML = "This field is reqired ";
//                        } else {
//                            j++;
//                        }
//                    }
//                    if (tindivs.length > j) {
//                    } else {
//                        $("#step").attr("value", 6);
//                        $("#step6").show();
//                        $("#step5").hide();
//                    }
//                } else {
                    $("#step").attr("value", 6);
                    $("#step6").show();
                    $("#step5").hide();
//                }
            });
            $(".next6").click(function () {
                var moreInvestorDivs1 = document.getElementsByClassName('investor-add');
                var j = 0;
//                for (var i = 0; i < moreInvestorDivs1.length; i++) {
//                    var moreInvestor1 = moreInvestorDivs1[i];
//                    var fnameInput1 = moreInvestor1.getElementsByClassName('fname')[0];
//                    var emailInput1 = moreInvestor1.getElementsByClassName('emailAddress')[0];
//                    var erroremail = moreInvestor1.getElementsByClassName('error-emailAddress')[0];
//                    var errorname = moreInvestor1.getElementsByClassName('error-fname')[0];
//                    var emailExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//                    if (fnameInput1.value === "") {
//                        errorname.innerHTML = "This field is required ";
//                    } else if (emailInput1.value === "" || !emailExpression.test(emailInput1.value)) {
//                        erroremail.innerHTML = "Email must be 'example@gmail.com' format ";
//                    } else {
//                        j++;
//                    }
//                }
//                if (moreInvestorDivs1.length <= j) {
                    for (var i = 0; i < moreInvestorDivs1.length; i++) {
                        var moreInvestor1 = moreInvestorDivs1[i];
                        var fnameInput1 = moreInvestor1.getElementsByClassName('fname')[0];
                        var emailInput1 = moreInvestor1.getElementsByClassName('emailAddress')[0];
                        var checkradio = moreInvestor1.querySelector('input[type="radio"]:checked');
                        var m1 = document.getElementById('morestep1');
                        var m2 = document.getElementById('morestep2');
                        var m3 = document.getElementById('morestep3');
                        var m4 = document.getElementById('morestep4');
                        var s6x = 'step6' + i;
                        var idc = '#' + s6x;
                        var s6x1 = s6x + 1;
                        var s6x1ele = document.getElementById(s6x1);
                        if (s6x1ele === null || typeof s6x1ele === 'undefined') {
                            var m1c = m1.cloneNode(true);
                            var m2c = m2.cloneNode(true);
                            var m3c = m3.cloneNode(true);
                            var m4c = m4.cloneNode(true);
                            m1c.id = s6x + 1;//601
                            m2c.id = s6x + 2;//602
                            m3c.id = s6x + 3;//603
                            m4c.id = s6x + 4;//604
                            m1c.classList.add("more-investor-info");
                            m1c.classList.add("more-investor-fs");
                            m2c.classList.add("more-investor-info");
                            m2c.classList.add("more-investor-fs");
                            m3c.classList.add("more-investor-info");
                            m3c.classList.add("more-investor-fs");
                            m4c.classList.add("more-investor-info");
                            m4c.classList.add("more-investor-fs");
                            if (i === 0) {
                                $("#step6").after(m1c);
                            } else {
                                var s6x4 = "#step6" + (i - 1) + "4";
                                $(s6x4).after(m1c);
                            }
                            $(m1c).after(m2c);
                            $(m2c).after(m3c);
                            $(m3c).after(m4c);
                            var fn = fnameInput1.value;
                            var fnarr = fn.split(" ");
                            $(idc + 3 + ' .first_name').val(fnarr[0]);
                            if (fnarr.length === 3) {
                                $(idc + 3 + ' .middle_name').val(fnarr[1]);
                                $(idc + 3 + ' .last_name').val(fnarr[2]);
                            } else if (fnarr.length === 2) {
                                $(idc + 3 + ' .last_name').val(fnarr[1]);
                            }
                        }
                        $(idc + 1 + " .investername").text(fnameInput1.value);
                        $(idc + 2 + " .investername").text(fnameInput1.value);
                        $(idc + 3 + " .investername").text(fnameInput1.value);
                        $(idc + 4 + " .investername").text(fnameInput1.value);
                        $(idc + 1 + " .more-investor-fname").val(fnameInput1.value);
                        $(idc + 1 + " .more-investor-email").val(emailInput1.value);
                        $(idc + 1 + " .more-investor-radio").val(checkradio.value);
                        $(idc + 1 + " .more-investor-preferred-name").val(fnameInput1.value);
                        $(idc + 1 + " .more-investor-dob").datepicker({
                            yearRange: (year - 80) + ':' + year,
                            changeMonth: true,
                            changeYear: true,
                            'maxDate': new Date(adultDOB),
                            dateFormat: 'dd/mm/yy'
                        }).datepicker().attr('readonly', 'readonly');
                        $(idc + 3 + " .more-investor-exp").datepicker({
                            yearRange: (year) + ':' + (year + 80),
                            changeMonth: true,
                            changeYear: true,
                            'minDate': new Date(),
                            dateFormat: 'dd/mm/yy'
                        }).datepicker().attr('readonly', 'readonly');
                        $(idc + 2 + " .more-investor-countrycode").intlTelInput_1();
                        $(idc + 1 + " .more-investor-countryname").intlTelInput_2();
                        $(idc + 3 + " .more-investor-countryname").intlTelInput_2();
                        $(idc + 4 + " .countrynameoutnz").intlTelInput_3();
                         new google.maps.places.Autocomplete(
                                    ( $(idc + 2 + " .more-investor-address")[0]),
                                    {types: ['address'], componentRestrictions: {country: 'nz'}});
                    }
                    
                    next(this);
//                }
            });
            $(".next11").click(function () {
//                var bank_name = $('.bank_name option:selected').text();
//                bank_name = bank_name.trim();
//                var other_name = $("#other_other_name").val();
//                var acount_holder_name = $("#nameOfAccount").val();
//                acount_holder_name = acount_holder_name.trim();
//                var acount_number = $(".account_number").val();
//                var bank_document = $("#bank_document").val();
//                if (bank_name === "–Select–" || bank_name === "Other") {
//                    if (bank_name === "–Select–") {
//                        $("#error_select_bank_name").text("This field is reqired ");
//                    }
//                    if (bank_name === "Other") {
//                        if (other_name === "") {
//                            $("#error_Other_Bank_name").text("This field is reqired ");
//                        }
//                    }
//                } else if (acount_holder_name === "") {
//                    $("#error_acount_holder_name1").text("This field is reqired ");
//                } else if (acount_number === "") {
//                    $("#error_acount_holder_number").text("This field is reqired");
//                } else if (bank_document === "") {
//                    $("#error_bank_document").text("Please attach verification of your bank account.");
//                } else {
                    $("#step").attr("value", 12);
                    $("#step12").show();
                    $("#step11").hide();
//                }

            });
            $(".next12").click(function () {
//                $('.save-new-btn').hide();
//                var y = $('input[name=authorized_condition]').is(':checked');
//                var x = $('input[name=term_condition]').is(':checked');
//                if (!x) {
//                    $("#error_term_condition").text("Please read and agree to the Terms and Conditions");
//                } else if (!y) {
//                    $("#error_authorized_condition").text("I am authorised to act and accept on behalf of all account holders");
//                } else {
                    $('.save-new-btn').hide();
                    $("#step").attr("value", 13);
                    $("#step13").show();
                    $("#step12").hide();
//                }

            });
            $('.des-togle').hide();
            $('#wealth_src').change(function () {
                var val = $("#wealth_src option:selected").val();
                if (val === "7") {
                    $('.des-togle').show();
                }
                if (val === "6") {
                    $('.des-togle').hide();
                }
                if (val === "5") {
                    $('.des-togle').hide();
                }
                if (val === "4") {
                    $('.des-togle').hide();
                }
                if (val === "3") {
                    $('.des-togle').hide();
                }
                if (val === "2") {
                    $('.des-togle').hide();
                }
                if (val === "1") {
                    $('.des-togle').hide();
                }
                if (val === "0") {
                    $('.des-togle').hide();
                }
            });
            $('#src_of_fund1').change(function () {
                var val = $("#src_of_fund1 option:selected").val();
                if (val === "1") {
                    $('.verifybtn').show();
                    $('.passport-select').hide();
                    $('.other-select').hide();
                    $('.drivery-licence').show();
                } else if (val === "2") {
                    $('.verifybtn').show();
                    $('.passport-select').show();
                    $('.other-select').hide();
                    $('.drivery-licence').hide();
                } else if (val === "3") {
                    $('.verifybtn').hide();
                    $('.passport-select').hide();
                    $('.other-select').show();
                    $('.drivery-licence').hide();
                }
            });
            $('#src_of_fund2').change(function () {
                var val = $("#src_of_fund2 option:selected").val();
                if (val === "1") {
                    $('.verifybtn2').show();
                    $('.passport-select2').hide();
                    $('.other-select2').hide();
                    $('.drivery-licence2').show();
                }
                if (val === "2") {
                    $('.verifybtn2').show();
                    $('.passport-select2').show();
                    $('.other-select2').hide();
                    $('.drivery-licence2').hide();
                }
                if (val === "3") {
                    $('.verifybtn2').hide();
                    $('.passport-select2').hide();
                    $('.other-select2').show();
                    $('.drivery-licence2').hide();
                }
            });
            $('.selectoption1').change(function () {
                var val = $(".selectoption1 option:selected").val();
                if (val === "1") {
                    $('.yes-option').hide();
                }
                if (val === "2") {
                    $('.yes-option').show();
                }
            });
            $('.selectoption2').change(function () {
                var val = $(".selectoption2 option:selected").val();
                if (val === "1") {
                    $('.yes-option2').hide();
                }
                if (val === "2") {
                    $('.yes-option2').show();
                }

            });
            $(document).ready(function () {
                $("#add-country-another").click(function () {
                    $(".yes-new").append("<div class='row checktindata removecountry'><div class='col-sm-6'><label class='label_input' style='text-align:left'>Country of tax residence</label></div><div class='col-sm-6 details-pos flag-drop'><input type='text' class='form-control countrynameoutnz tex_residence_Country'  id='countryname' name='countryCode' placeholder='Enter Country Code' readonly='readonly' onkeyup='removerror(this)'/><span class='error error-tex-residence-Country' ></span></div><div class='col-sm-6'><label class='label_input'>Tax Identification Number (TIN) </label></div><div class='col-sm-6'><input type='text' class='form-control input-field TIN' name='tin' required='required' placeholder='Enter TIN' onkeyup='removerror(this)' /><span class='error error-TIN'></span></div><div class='col-sm-6'><label class='label_input'>Reason if TIN not available </label></div><div class='col-sm-6'> <input type='text' class='form-control input-field resn_tin_unavailable'  name='tin' required='required' onkeyup='removerror(this)'/><span class='error error-resn-tin-unavailable' id='error-resn-tin-unavailable'></span><a href='javascript:void(0);' onclick='removecountrydiv(this);'><i class='far fa-times-circle'></i></a></div></div>");
                    $(".countrynameoutnz").intlTelInput_3();
                });
                $("#add-country-another3").click(function () {
                    $(".yes-new3").append("<div class='row checktindata1 yes-new3-clone removecountry'><div class='col-sm-6'><label class='label_input' style='text-align:left'>Country of tax residence</label></div><div class='col-sm-6 details-pos flag-drop'><input type='text' class='form-control countrynameoutnz more-investor-tex_residence_Country'  id='countryname' name='countryCode' placeholder='Enter Country Code' readonly='readonly'></div><div class='col-sm-6'><label class='label_input'>Tax Identification Number (TIN) </label></div><div class='col-sm-6'><input type='text' class='form-control input-field more-investor-TIN' name='tin' required='required' placeholder='Enter TIN' onkeyup='myFunction()' /><input type='hidden' class='more-investor-resn_tin_unavailable' name='reason'/><a href='javascript:void(0);' onclick='removecountrydiv(this);'><i class='far fa-times-circle'></i></a></div></div>");
                    $(".countrynameoutnz").intlTelInput_3();
                });
            });
            $(document).ready(function () {
                var count = 1;
                var id = 1;
                $('#counter').val(count);
                $("#btn22").click(function () {
                    var changeid1 = "firstid" + id;
                    var changeid2 = "secondid" + id;
                    id++;
                    var investor = document.getElementById("investor").cloneNode(true);
                    investor.getElementsByClassName("fname")[0].value = '';
                    investor.getElementsByClassName("emailAddress")[0].value = '';
                    investor.getElementsByClassName("radio1")[0].removeAttribute("id");
                    investor.getElementsByClassName("radio2")[0].removeAttribute("id");
                    investor.getElementsByClassName("forlabel1")[0].removeAttribute("id");
                    investor.getElementsByClassName("forlabel2")[0].removeAttribute("id");
                    investor.getElementsByClassName("radio1")[0].setAttribute("id", changeid1);
                    investor.getElementsByClassName("radio2")[0].setAttribute("id", changeid2);
                    investor.getElementsByClassName("radio1")[0].setAttribute("name", changeid1);
                    investor.getElementsByClassName("radio2")[0].setAttribute("name", changeid1);
                    investor.getElementsByClassName("forlabel1")[0].setAttribute("for", changeid1);
                    investor.getElementsByClassName("forlabel2")[0].setAttribute("for", changeid2);
                    investor.getElementsByClassName("removeData")[0].removeAttribute("style");
                    //                    investor.getElementsByClassName("checkradio")[0].checked = false;
                    $(".investor-section").append(investor);
                    counter = $('#counter').val();
                    counter++;
                    $('#counter').val(counter);
                });
            });
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
                $('.advisor-show').hide();
            });
            $('.avisor-click').change(function () {
                var val = $(".avisor-click option:selected").val();
                if (val === "1") {
                    $('.advisor-show').hide();
                }
                if (val === "2") {
                    $('.advisor-show').show();
                }
            });
            $('.pir').click(function () {
                $('.pir').attr("href", "https://www.ird.govt.nz/roles/portfolio-investment-entities/using-prescribed-investor-rates");
                window.open(this.href);
                return false;
            });
            $('#same-as-investor1').click(function () {
                var address = $('#address').val();
                $(".more-investor-address").val(address);
            });
//            $('#submit').click(function () {
//                var status = "SUBMISSION";
//                //                verify();
//                saveData(status);
//            });
//            $('.saveExit').click(function () {
//                var status = "PENDING";
//                saveData(status);
//            });
            saveData = function (status) {
                swal({
                    title: "",
                    text: "Application data being saved...  ",
                    type: "success",
                    timer: 3500,
                    showConfirmButton: false});
                var id = null, email = null, password = null;
                var reg_id = null;
                var raw_password = null;
            <c:choose>
                <c:when test="${not empty joint.id}">
                id = ${joint.id};
                email = '${joint.email}';
                reg_id = '${joint.reg_id}';
                raw_password = '${joint.raw_password}';
                </c:when>
                <c:otherwise>
                email = '${email}';
                password = '${password}';
                </c:otherwise>
            </c:choose>
                var Title = $('#title option:selected').text();
                var fullName = $('#fullName').val();
                var preferredName = $('#preferredName').val();
                var Date_of_Birth = $('.dateOfBirth').val();
                var country_residence = $("#countryResidence").val();
//                alert(country_residence);
                var Occupation = $('.Occupation').val();
                var occupationStatus = $('.Occupation option:selected').text();
                var working_with_adviser = $('.working_with_adviser option:selected').text();
                var advisor_company = $('#isAdvisorCompany option:selected').val();
                var advisor = $('#isAdviser option:selected').val();
                var homeAddress = $('#address').val();
                var mobile_country_code = $('.mobile_country_code').val();
                var mobile_number = $('.mobile_number1').val();
                var optional_num_type = $('#typeOfMobile option:selected').text();
                var optional_num_code = $('.optional_num_code').val();
                var optional_num = $('.optional_num').val();
                var id_type = $('.id_type option:selected').text();
                var License_number = $('.License_number').val();
                var licence_expiry_Date = $('.licence_expiry_Date').val();
                var licence_verson_number = $('.licence_verson_number').val();
                var passport_number = $('.passport_number').val();
                var passport_expiry = $('.passport_expiry').val();
                var passport_issue_by = $('.issue_by').val();
                var investor_idverified = $('#investor_verify').val();
                var other_id_type = $('.other_id_type').val();
                var other_id_expiry = $('.other_id_expiry').val();
                var other_id_issueBy = $('.other_id_issueBy').val();
                var PIR = $('.PIR option:selected').text();
                var IRD_Number = $('.IRD_Number').val();
                var isUSCitizen = $('#isUSCitizen option:selected').val();
                var countryArr = document.getElementsByClassName('tex_residence_Country');
                var TINArr = document.getElementsByClassName('TIN');
                var reasonArr = document.getElementsByClassName('resn_tin_unavailable');
                var countryTINList = new Array();
                for (var i = 0; i < countryArr.length; i++) {
                    var country = countryArr[i].value;
                    var tin = TINArr[i].value;
                    var reason = reasonArr[i].value;
                    countryTINList.push({country: country, tin: tin, reason: reason});
                }
                var bank_name = $('.bank_name option:selected').text();
                var acount_holder_name = $('.acount_holder_name').val();
                var account_number = $('.account_number').val();
                var curr_idx = $('[name="more_investor_index"]').val();
                var step_id = $('[name="step"]').val();
                var reg_type = $('[name="register_type"]').val();
                var first_name = $('.first_name').val();
                var middle_name = $('.middle_name').val();
                var last_name = $('.last_name').val();
                var moreInvestorInfos = getMoreInvestorArr();
                var mainInvestor = {curr_idx: curr_idx, id: id, email: email, password: password, raw_password: raw_password, title: Title, fullName: fullName, preferred_name: preferredName, date_of_Birth: Date_of_Birth,
                    country_residence: country_residence, occupation: Occupation, occupationStatus: occupationStatus, working_with_adviser: working_with_adviser,
                    advisor_company: advisor_company, advisor: advisor, homeAddress: homeAddress, mobile_country_code: mobile_country_code,
                    mobile_number: mobile_number, optional_num_type: optional_num_type, optional_num_code: optional_num_code,
                    optional_num: optional_num, id_type: id_type, firstName: first_name, lastName: last_name, middleName: middle_name,
                    license_number: License_number, licence_expiry_Date: licence_expiry_Date, licence_verson_number: licence_verson_number,
                    passport_number: passport_number, passport_expiry: passport_expiry, passport_issue_by: passport_issue_by,
                    other_id_type: other_id_type, other_id_expiry: other_id_expiry, other_id_issueBy: other_id_issueBy,
                    pir: PIR, ird_Number: IRD_Number, isUSCitizen: isUSCitizen, bank_name: bank_name, acount_holder_name: acount_holder_name,
                    account_number: account_number, status: status, step: step_id, reg_type: reg_type, countryTINList: countryTINList, moreInvestorList: moreInvestorInfos, reg_id: reg_id, investor_idverified: investor_idverified};
                console.log(JSON.stringify(mainInvestor));
                //                alert(JSON.stringify(mainInvestor.moreInvestorList));
                $.ajax({
                    type: 'POST',
                    url: './rest/groot/db/api/joint-registeration',
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(mainInvestor), success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        swal({
                            title: "Complete",
                            text: "Your application data has been saved. To resume your application, please see the email we have now sent you. ",
                            type: "success",
                            timer: 2500,
                            showConfirmButton: false
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(" inside error" + JSON.stringify(jqXHR));
                        console.log(" textStatus error" + JSON.stringify(textStatus));
                        console.log(" errorThrown error" + JSON.stringify(errorThrown));
                    }
                });
            };
        </script>
        <script>
            //            $(".verify-dl").click(function () {
            verify = function () {
                var x = $(this).closest(".content-section");
                var index = x.find($(".Id_Type option:selected")).val();
                var url = '';
                var firstName = '';
                var middleName = '';
                var lastName = '';
                if (index === "1") {
                    url = './rest/groot/db/api/dl-verification';
                    firstName = x.find($('input[name=licence_first_name]')).val();
                    middleName = x.find($('input[name=licence_middle_name]')).val();
                    lastName = x.find($('input[name="licence_last_name"]')).val();
                    var License_number = x.find($('input[name="license_number"]')).val();
                    //                    var License_number = $('.License_number').val();
                    var licence_expiry_Date = x.find($('.lic_expiry_Date')).val();
                    var licence_verson_number = x.find($('.lic_verson_number')).val();
                } else if (index === "2") {
                    url = './rest/groot/db/api/pp-verification';
                    firstName = x.find($('input[name=passport_first_name]')).val();
                    middleName = x.find($('input[name=passport_middle_name]')).val();
                    lastName = x.find($('input[name="passport_last_name"]')).val();
                    var passport_number = x.find($('input[name="passport_number"]')).val();
                    var dob2 = x.find('.pass_expiry').val();
                    dob2 = dob2.trim();
                    passport_number = passport_number.trim();
                }
                var Date_of_Birth = $('#dob').val();
                var passport_expiry = x.find('.pass_expiry').val();
                var DataObj = {license_number: License_number, licence_expiry_Date: licence_expiry_Date,
                    licence_verson_number: licence_verson_number, date_of_Birth: Date_of_Birth, firstName: firstName,
                    middleName: middleName, lastName: lastName, passport_number: passport_number, passport_expiry: passport_expiry};
                console.log(DataObj);
                $.ajax({
                    type: 'POST',
                    url: url,
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(DataObj), success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        var obj = JSON.parse(data);
                        if (index === "1") {
                            if (obj.driversLicence.verified) {
                                swal({
                                    title: "Success",
                                    text: "Data is Verified.",
                                    type: "info",
                                    timer: 3500,
                                    showConfirmButton: true
                                });
                            }
                        } else if (index === "2") {
                            if (obj.passport.verified) {
                                swal({
                                    title: "Success",
                                    text: "Data is Verified.",
                                    type: "info",
                                    timer: 3500,
                                    showConfirmButton: true
                                });
                            }

                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //                        alert(" inside error" + jqXHR);
                    }
                });
            };
            $('.country').click(function () {
                $('.error').text("");
            });
            function removerror(ele) {
                $('.error').text("");
            }
            $(".OccupationOption").click(function () {
                var Occupation = $('.OccupationOption').val();
                if (Occupation === "other") {
                    $(".selectOcc").show();
                    $(".otherOcc").hide();
                }
            });
        </script>
        <script>
            function myFunction1() {
                $("#error-emailAddress").text("");
            }
            function removecountrydiv(ele) {
                ele.closest('.removecountry').remove();
            }
            function removedatadiv(ele) {
                ele.closest('.investor-add').remove();
            }
        </script>
        <script>
            function myFunction7() {
                var ac = document.getElementById("myspan").value;
                if (ac.length == 2) {
                    var ac20 = ac.substr(0, 2);
                    var a = document.getElementById("myspan").innerHTML = (ac20 + '-');
                    $("#myspan").val(a);
                }
                if (ac.length == 7) {
                    var ac21 = ac.substr(7);
                    var b = document.getElementById("myspan").innerHTML = (ac + '-' + ac21);
                    $("#myspan").val(b);
                }
                if (ac.length == 15) {
                    var ac212 = ac.substr(15);
                    //                alert("c14 7S");
                    var c = document.getElementById("myspan").innerHTML = (ac + '-' + ac212);
                    $("#myspan").val(c);
                }
                if (ac.length > 18) {
                    var ac23 = ac.substr(0, 18);
                    var d = document.getElementById("myspan").innerHTML = (ac23);
                    $("#myspan").val(d);
                }
            }
        </script>

        <script>
            function checkAccountNO(obj) {
                var ac = $('#accountNumber').val();
                //                alert(ac.length);
                if (ac.length > 2) {
                    var ac20 = ac.substr(0, 2);
                    var ac21 = ac.substr(2);
                    //                    alert(ac21);
                    $('#accountNumberTemp').val(ac20 + '-' + ac21);
                }
                if (ac.length > 6) {
                    var ac60 = ac.substr(0, 2);
                    var ac61 = ac.substr(0, 6);
                    $('#accountNumberTemp').val(ac60 + '-' + ac61);
                }
                if (ac.length > 13) {
                    var ac130 = ac.substr(0, 2);
                    var ac131 = ac.substr(0, 6);
                    var ac132 = ac.substr(0, 13);
                    $('#accountNumberTemp').val(ac130 + '-' + ac131 + '-' + ac132);
                }
                $('#accountNumberTemp').val(ac);
            }
            function checkird(obj) {
                str = obj.value.replace('', '');
                if (str.length > 10) {
                    str = str.substr(0, 10);
                } else
                {
                    str = str.replace(/\D+/g, "").replace(/([0-9]{3})([0-9]{3})([0-9]{2}$)/gi, "$1-$2-$3");
                }
                obj.value = str;
            }

        </script>
        <script>
            $('.senderType').click(function () {
                var mNo = $('.mobile_number1').val();
                var cCo = $('.mobile_country_code').val();
                var sTy = $('.senderType:checked').val();
                cCo = cCo.replace(/\+/g, "");
                var url = 'https://jj1stbnzb3.execute-api.ap-southeast-2.amazonaws.com/otp/generateotp?pn=' + mNo + '&cc=' + cCo + '&sTy=' + sTy;
                //document.getElementById('error-loader').style.display = 'block';
                document.getElementById('error-generateOtp').innerHTML = "";
                $.ajax({
                    url: url,
                    type: 'GET',
                    async: true,
                    dataType: "json", success: function (data) {
                        console.log(data);
                        $('#error-generateOtp').html("Please check your messages");
                        $('#otp-block').show();
                        //            document.getElementById('error-loader').style.display = 'none';
                        $('input:radio[name="senderType"]').prop("checked", false);
                    },
                    error: function (e) {
                        var mobileNo = document.getElementsByClassName('mobile_number1');
                        var result = true;
                        $('#otp-block').show();
                        if (mobileNo.value === '') {
                            //document.getElementById('error-loader').style.display = 'none';
                            //                            $('#error-generateOtp').text("Mobile number is required."); 
                            $('input:radio[name="senderType"]').prop("checked", false);
                            result = result && false;
                        } else {
                            //document.getElementById('error-loader').style.display = 'none';
                            //$('input:radio[name="senderType"]').prop("checked", false); 
                            result = result && false;
                        }
                        return result;
                    }
                });
                return sTy;
            });
        </script>
        <script>
            $(document).ready(function () {
                $(".toggal_other").hide();
                var step = '${joint.step}';
                var fn = '${joint.fullName}';
                var fnarr = fn.split(" ");
                $('.first_name').val(fnarr[0]);
                if (fnarr.length === 3) {
                    $('.middle_name').val(fnarr[1]);
                    $('.last_name').val(fnarr[2]);
                } else if (fnarr.length === 2) {
                    $('.last_name').val(fnarr[1]);
                }
                $('[name="step"]').val(parseInt(step));
                if (step === '1') {
                    $("#step1").show();
                    $('.save-new-btn').show();
                } else if (step === '2') {
                    $("#step2").show();
                    $("#step1").hide();
                    $('.save-new-btn').show();
                } else if (step === '3') {
                    $("#step3").show();
                    $("#step1").hide();
                    $('.save-new-btn').show();
                } else if (step === '4') {
                    $("#step4").show();
                    $("#step1").hide();
                    $('.save-new-btn').show();
                } else if (step === '5') {
                    $("#step5").show();
                    $("#step1").hide();
                    $('.save-new-btn').show();
                } else if (step === '6') {
                    $("#step6").show();
                    $("#step1").hide();
                    $('.save-new-btn').hide();
                } else if (step === '7') {
                    $("#step6").show();
                    $("#step1").hide();
                    $('.save-new-btn').show();
                } else if (step === '8') {
                    $("#step6").show();
                    $("#step1").hide();
                    $('.save-new-btn').show();
                } else if (step === '9') {
                    $("#step6").show();
                    $("#step1").hide();
                    $('.save-new-btn').show();
                } else if (step === '10') {
                    $("#step1").hide();
                    $("#step6").show();
                    $('.save-new-btn').show();
                } else if (step === '11') {
                    $("#step1").hide();
                    $("#step11").show();
                    $('.save-new-btn').show();
                } else if (step === '12') {
                    $("#step1").hide();
                    $("#step12").show();
                    $('.save-new-btn').show();
                }
            });
            $('.bank_name').change(function () {
                //                alert('other');
                if ($('.bank_name option:selected').val() === 'Other') {
                    $(".toggal_other").show();
                } else {
                    $(".toggal_other").hide();
                }
            });
            function removespan() {
                $('.error').text('');
            }
            function removeSpanError() {
                $('.error').text('');
            }
            $('.country').click(function () {
                $('.error').text("");
            });
        </script>
        <script>
            function checkname(ele) {
                console.log("daya");
                var root = ele.closest('.closestcls');
                var name = ele.value;
                var filename = name.split('\\').pop().split('/').pop();
                filename = filename.substring(0, filename.lastIndexOf('.'));
                console.log(name);
                $(root).find('.shownamedata').text("File: " + filename);
                $(root).find(".removefile").show();
            }
            function  removedatafile(ele) {
                var root = ele.closest('.closestcls');
                $(root).find('.shownamedata').text("");
                $(root).find('.checkname').val("");
                $(root).find(".removefile").hide();
            }
            function changeFund(ele) {
                var root = $(ele).closest('.morestep3');
                console.log(root);
                var val = ele.value;
                var idc = "#" + (root.attr("id"));
                if (val === "1") {
                    $(idc + ' .other-select2').hide();
                    $(idc + ' .drivery-licence2').show();
                    $(idc + ' .passport-select2').hide();
                } else if (val === "2") {
                    $(idc + ' .passport-select2').show();
                    $(idc + ' .other-select2').hide();
                    $(idc + ' .drivery-licence2').hide();
                } else if (val === "3") {
                    $(idc + ' .passport-select2').hide();
                    $(idc + ' .other-select2').show();
                    $(idc + ' .drivery-licence2').hide();
                }
            }
            function changeCountry(ele) {
                var root = $(ele).closest('.morestep4');
                var val = ele.value;
                var idc = "#" + (root.attr("id"));
                if (val === "1") {
                    $(idc + ' .yes-option2').hide();
                }
                if (val === "2") {
                    $(idc + ' .yes-option2').show();
                }
            }
            function addAnotherCountry(ele) {
                var root = $(ele).closest('.morestep4');
                var idc = "#" + (root.attr("id"));
                $(idc + " .yes-new3").append("<div class='row checktindata1'><div class='col-sm-6'><label class='label_input' style='text-align:left'>Country of tax residence</label></div><div class='col-sm-6 details-pos flag-drop'><input type='text' class='form-control countrynameoutnz tex_residence_Country'  id='countryname' name='countryCode' placeholder='Enter Country Code' readonly='readonly'><span class='error error-more-investor-tex_residence_Country' ></span></div>\n\
                <div class='col-sm-6'><label class='label_input'>Tax Identification Number (TIN)  </label></div><div class='col-sm-6'><input type='text' class='form-control input-field TIN more-investor-TIN' name='tin' required='required' placeholder='Enter TIN' onkeyup='removerror(this)' /><span class='error error-TIN more-director-TIN-error'></span></div><div class='col-sm-6'><label class='label_input'>Reason if TIN not available </label></div>\n\
                <div class='col-sm-6'> <input type='text' class='form-control input-field resn_tin_unavailable more-investor-resn_tin_unavailable'  name='tin' required='required' onkeyup='removerror(this)'/><span class='error  error-resn_tin_unavailable more-director-resn_tin_unavailable-error' id='error-resn-tin-unavailable'></span></div></div>");
                $(idc + " .countrynameoutnz").intlTelInput_3();
            }
        </script>
        <script>
//            $('.Occupation option').each(function () {
//                var optionText = this.text;
//                var newOption = optionText.substring(0, 25);
//                $(this).text(newOption + '...');
//            });
            
            
            
                $('.otpkey').keyup(function () {
//                    $('.spanverification').html("");
                    if ($('.otpkey').val().length === 6) {
//                                                                
//                                                                side");
                        var mNo = $('.mobile_number1').val();
                        var cCo = $('.mobile_country_code').val();
                        var otp = $('.otpkey').val();
                        cCo = cCo.replace(/\+/g, "");
                        validateOTP(mNo, cCo, otp);
                    }
                });
        </script>


        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKuUGMz8-xYyc6t1YWUhygrrkB4-WAFeY&libraries=places&callback=initAutocomplete"
        async defer></script>
    </body>
</html>				