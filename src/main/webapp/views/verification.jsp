<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>westpac</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="http://backoffice.invsta.io/mint/images/mint.png" rel="shortcut icon"/>
        <link href="http://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <!--<link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>-->
        <link href="http://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>
        <!--<link href="http://backoffice.invsta.io/pocv/resources/css/main.css?version=3.5.1" rel="stylesheet"/>-->
        <link href="http://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <!--<link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>-->
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link href="./resources/css/custom.css" rel="stylesheet"/>
        <link href="./resources/css/idVerification.css" rel="stylesheet"/>
        <link href="./resources/css/sweetalert.css" rel="stylesheet" />
        <script src="./resources/js/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
        <!--  <link href="resources/css/osfonts.css" rel="stylesheet"> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <style>
            .add-check-btn i {
                bottom: 21px;
                position: absolute;
                right: 0;
                left: 502px;
                z-index: 1;
                color: green;
            }

            i.fa.fa-remove.btn-close {
                color: red;
            }
        </style>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Verify</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">

                                <div class="col-md-12 invst-option">

                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active p-31" id="one" role="tabpanel" aria-labelledby="one-tab">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="zoo-fielset">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="zoo-content_body">
                                                                        <form id="msform">
                                                                            <fieldset id="step1">
                                                                                <div class="country-option-header">
                                                                                    <span class="region-text">Region / Country</span>
                                                                                    <span class="region-text-icon"><a href=""><i class="fa fa-arrow-right" aria-hidden="true"></i></a></span>
                                                                                </div>
                                                                                <div class="zoo-content-section">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <p class="select-country__option">Select a Region / Country to view your subscribed data sources:</p>
                                                                                            <div class="select-country__region-header">
                                                                                                <h5 class="select-country__region-name">APAC</h5>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class="select-radios-zoo">
                                                                                                <p>
                                                                                                    <input type="radio" id="test1" name="radio-group" value="Australia">
                                                                                                    Australia <label for="test1"></label>
                                                                                                </p>
                                                                                                <p>
                                                                                                    <input type="radio" id="test2" name="radio-group" value="Cambodia">
                                                                                                    Cambodia <label for="test2"></label>
                                                                                                </p>
                                                                                                <p>
                                                                                                    <input type="radio" id="test3" name="radio-group" value=" New Zealand">
                                                                                                    New Zealand <label for="test3"></label>
                                                                                                </p>
                                                                                                <p>
                                                                                                    <input type="radio" id="test4" name="radio-group" value="Vietnam">
                                                                                                    Vietnam <label for="test4"></label>
                                                                                                </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class="zoo-btns">
                                                                                                <!--<input type="button" name="previous" value="Previous Page" class="previous1 country-option-btn">-->
                                                                                                <input type="button" name="previous" value="Next Page" class="next1 country-option-btn">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </fieldset>
                                                                            <fieldset id="step2">
                                                                                <div class="country-option-header">
                                                                                    <span class="region-text region">Australia</span>
                                                                                    <span class="region-text-icon"><a href=""><i class="fa fa-arrow-right" aria-hidden="true"></i></a></span>
                                                                                </div>
                                                                                <div class="zoo-content-section">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <p class="select-country__option">Select data source(s):</p>

                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class="select-radios-zoo-check">
                                                                                                <ul class="unstyled centered">
                                                                                                    <li class="lable-cut">
                                                                                                        <input class="styled-checkbox idtype" name="idtype" id="styled-checkbox-1" type="radio" value="license">
                                                                                                        Driver's Licence<label for="styled-checkbox-1"></label>
                                                                                                    </li>
                                                                                                    <li class="lable-cut">
                                                                                                        <input class="styled-checkbox idtype"  name="idtype" id="styled-checkbox-2" type="radio" value="passport">
                                                                                                        Passport<label for="styled-checkbox-2"></label>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class="zoo-btns">
                                                                                                <input type="button" name="previous" value="Previous Page" class="previous2 country-option-btn">
                                                                                                <input type="button" name="previous" value="Next Page" class="next2 country-option-btn">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </fieldset>
                                                                            <fieldset id="step3">
                                                                                <div class="country-option-header">
                                                                                    <span class="region-text region">Australia</span>
                                                                                    <!--<span class="region-text-icon"><a href=""><i class="fa fa-pencil" aria-hidden="true"></i></a></span>-->
                                                                                </div>
                                                                                <div class="zoo-content-section">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12 mt-3">
                                                                                            <p class="select-country__option userdata" >Cambodia Consumer</p>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class="country-option-header">
                                                                                                <span class="region-text">Personal Information</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class="row">
                                                                                                <div class="col-md-6">
                                                                                                    <div class="select-zoo-form">
                                                                                                        <h6>FIRST NAME</h6>
                                                                                                        <div class="add-check-btn">
                                                                                                            <i class="fa fa-check-circle first_name_right "></i>
                                                                                                            <i class="fa fa-remove btn-close first_name_wrong"></i>
                                                                                                            <input  type="text" placeholder="" class="input-data" name="first_name" id="first_name">
                                                                                                            <span class="error" id="error_first_name"></span>
                                                                                                        </div>

                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-6">
                                                                                                    <div class="select-zoo-form">
                                                                                                        <h6>MIDDLE NAME</h6>
                                                                                                        <div class="add-check-btn">
                                                                                                            <i class="fa fa-check-circle middle_name_right"></i>
                                                                                                            <i class="fa fa-remove btn-close middle_name_wrong"></i>
                                                                                                            <input  type="text" placeholder="" class="input-data" name="middle_name" id="middle_name">
                                                                                                            <span class="error" id="error_first_name"></span>
                                                                                                        </div>

                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-6">
                                                                                                    <div class="select-zoo-form">
                                                                                                        <h6>LAST NAME</h6>
                                                                                                        <div class="add-check-btn">
                                                                                                            <i class="fa fa-check-circle last_name_right"></i>
                                                                                                            <i class="fa fa-remove btn-close last_name_wrong"></i>
                                                                                                            <input type="text" placeholder="" class="input-data" name="last_name" id="last_name">
                                                                                                            <span class="error" id="error_last_name"></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-6">
                                                                                                    <div class="select-zoo-form">
                                                                                                        <h6>DATE OF BIRTH</h6>
                                                                                                        <div class="add-check-btn">
                                                                                                            <i class="fa fa-check-circle dob_right"></i>
                                                                                                            <i class="fa fa-remove btn-close dob_wrong"></i>
                                                                                                            <input type="text" placeholder="" class="input-data dob date_of_birth" name="date_of_birth">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12 passport">
                                                                                            <div class="row">
                                                                                                <div class="col-md-12 mt-3 ">
                                                                                                    <div class="country-option-header">
                                                                                                        <span class="region-text">Passport Details</span>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-6 ">
                                                                                                    <div class="select-zoo-form">
                                                                                                        <h6>Passport Number</h6>
                                                                                                        <div class="add-check-btn">
                                                                                                            <i class="fa fa-check-circle passport_number_right"></i>
                                                                                                            <input type="text" placeholder="" class="input-data " name="passport_number" id="Passportnumber">
                                                                                                            <span class="error" id="spanPassportnumber"></span>
                                                                                                        </div></div>
                                                                                                </div>
                                                                                                <div class="col-md-6 ">
                                                                                                    <div class="select-zoo-form">
                                                                                                        <h6>Passport Expiry</h6>
                                                                                                        <div class="add-check-btn">
                                                                                                            <i class="fa fa-check-circle passport_expiry_right"></i>
                                                                                                            <input type="text" placeholder="" class="input-data exp  passportExp" name="passport_expiry">
                                                                                                            <span class="error" id="error-passportExpiry"></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <!--                                                                                                <div class="col-md-6 ">
                                                                                                                                                                                                    <div class="select-zoo-form">
                                                                                                                                                                                                        <input class="btn-vrfy verify-dl" type="button" name="myFile"  value="Verify Passport">
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                </div>-->
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12 license">
                                                                                            <div class="row">
                                                                                                <div class="col-md-12  mt-3 ">
                                                                                                    <div class="country-option-header">
                                                                                                        <span class="region-text">Driving Licence details</span>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-6">
                                                                                                    <div class="select-zoo-form">
                                                                                                        <h6>Licence Number</h6>
                                                                                                        <div class="add-check-btn">
                                                                                                            <i class="fa fa-check-circle License_number_right"></i>
                                                                                                            <i class="fa fa-remove btn-close License_number_wrong"></i>
                                                                                                            <input type="text" placeholder="" class="input-data" name="license_number" id="licenseNumber">
                                                                                                            <span class="error" id="spanlicenseNumber"></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-6">
                                                                                                    <div class="select-zoo-form">
                                                                                                        <h6>Licence Version</h6>
                                                                                                        <div class="add-check-btn">
                                                                                                            <i class="fa fa-check-circle lic_verson_number_right"></i>
                                                                                                            <i class="fa fa-remove btn-close lic_verson_number_wrong"></i>
                                                                                                            <input type="text" placeholder="" class="input-data"  name="license_version" id="Versionnumber">
                                                                                                            <span class="error" id="spanVersionnumber"></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <!--                                                                                                <div class="col-md-6">
                                                                                                                                                                                                    <div class="select-zoo-form">
                                                                                                                                                                                                        <input class="btn-vrfy verify-dl" type="button" name="myFile"  value="Verify Driving License">
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                </div>-->

                                                                                                <!--                                                                                                <div class="col-md-6">
                                                                                                                                                                                                    <div class="select-zoo-form">
                                                                                                                                                                                                        <h6>PROVINCE</h6>
                                                                                                                                                                                                        <input type="text" placeholder="" class="input-data">
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                </div>-->

                                                                                            </div>
                                                                                        </div>
                                                                                        <!--                                                                                        <div class="col-md-12  mt-3">
                                                                                                                                                                                    <div class="country-option-header">
                                                                                                                                                                                        <span class="region-text">Current Residential Address</span>
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>-->
                                                                                        <!--                                                                                        <div class="col-md-12  mt-3">
                                                                                                                                                                                    <div class="country-option-pera">
                                                                                                                                                                                        <p>I confirm that I am authorised to provide the personal details presented and I consent to my information being checked with the document issuer or office record holder via third party systems for the purpose of confirming my identity.</p>
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>-->
                                                                                        <!--                                                                                        <div class="col-md-6">
                                                                                                                                                                                    <div class="select-radios-zoo-check zoo-terms">
                                                                                                                                                                                        <ul class="unstyled centered">
                                                                                                                                                                                            <li>
                                                                                                                                                                                                <input class="styled-checkbox" id="styled-checkbox-6" type="checkbox" value="value2">
                                                                                                                                                                                                I agree to the terms and conditions<label for="styled-checkbox-6"></label>
                                                                                                                                                                                            </li>
                                                                                                                                                                                        </ul>
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>-->
                                                                                        <!--                                                                                        <div class="col-md-12">
                                                                                                                                                                                    <div class="row">
                                                                                                                                                                                        <div class="col-md-6">
                                                                                                                                                                                            <div class="select-zoo-form">
                                                                                                                                                                                                <h6>CLIENT (YOUR) REFERENCE</h6>
                                                                                                                                                                                                <input type="text" placeholder="" class="input-data">
                                                                                                                                                                                            </div>
                                                                                                                                                                                        </div>
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>-->
                                                                                        <div class="col-md-12">
                                                                                            <div class="zoo-btns">
                                                                                                <input type="button" name="previous" value="Previous Page" class="previous3 country-option-btn">
                                                                                                <input type="button" name="previous" value="Verify" class="country-option-btn verify-dl">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </fieldset>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>       
                                            </div>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="http://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <!--<script src="./resources/js/index.js"></script>-->  
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>
        <!--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $(window).load(function () {

                $(".loader").fadeOut("slow");
            });
        </script>
        <script>
            $('.clickinput').on("click", function () {
                var recemt = $(this).closest('.funds-deatil');
                recemt.find(".offer-input").toggle();
            });
            var adultDOB = '2001-07-22';
            var adultDOB1 = '2019-07-22';
            var arr = adultDOB.split("-");
            var year = parseInt(arr[0]);
            var month = parseInt(arr[1]);
            var day = parseInt(arr[2]);
            var arr1 = adultDOB1.split("-");
            var year1 = parseInt(arr1[0]);
            var month1 = parseInt(arr1[1]);
            var day1 = parseInt(arr1[2]);
            $(document).ready(function () {
                $(".dob").datepicker({
                    yearRange: (year - 80) + ':' + year,
                    changeMonth: true,
                    changeYear: true,
                    'maxDate': new Date(adultDOB),
                    dateFormat: 'dd/mm/yy'
                }).datepicker('setDate', new Date(adultDOB), ).attr('readonly', 'readonly');
                $(".exp").datepicker({
                    yearRange: (year - 80) + ':' + new Date(),
                    changeMonth: true,
                    changeYear: true,
                    'maxDate': new Date(),
                    dateFormat: 'dd/mm/yy'
                }).datepicker('setDate', new Date(), ).attr('readonly', 'readonly');


                $(".loader").fadeOut("slow");
                $('.hidediv1').hide();
                $('.hidediv2').hide();
                $('.hidediv3').hide();
            });
            $('.democlick').click(function () {
                var data = $(this).data("target");
                $('.showclick').hide();
                $(data).show();
            });
        </script>
        <script>
            $(".previous2").click(function () {
                $("#step2").hide();
                $("#step1").show();
            });
            $(".previous3").click(function () {
                $("#step3").hide();
                $("#step2").show();
                $('.passport').show();
                $('.license').show();
            });
            $(".previous4").click(function () {
                $("#step4").hide();
                $("#step3").show();
            });
            $(".next1").click(function () {
                var region = $('input[name="radio-group"]:checked').val();
                if (typeof region !== "undefined" && region !== "") {
                    $('.region').text(region);
                    $('.userdata').text(region + " Consumer Data ");
                    $("#step2").show();
                    $("#step1").hide();
                }
            });
            $(".next2").click(function () {
                var idtype = $('.idtype:checked').val();
                if (idtype === "license") {
                    $('.passport').hide();
                    $("#step3").show();
                    $("#step2").hide();

                } else if (idtype === "passport") {
                    $('.license').hide();
                    $("#step3").show();
                    $("#step2").hide();
                } else {
                    return false;
                }
                $('.first_name_right').hide();
                $('.first_name_wrong').hide();
                $('.middle_name_right').hide();
                $('.middle_name_wrong').hide();
                $('.last_name_right').hide();
                $('.last_name_wrong').hide();
                $('.dob_right').hide();
                $('.dob_wrong').hide();
                $('.License_number_right').hide();
                $('.License_number_wrong').hide();
                $('.lic_verson_number_right').hide();
                $('.lic_verson_number_wrong').hide();
                $('.passport_expiry_right').hide();
                $('.passport_number_right').hide();


            });
            $(".next3").click(function () {
                $("#step4").show();
                $("#step3").hide();
            });
        </script>
        <script>
            $(document).ready(function () {
                $(".select-radios-zoo").hide();
                $(".select-country__region-header").click(function () {
                    $(".select-radios-zoo").toggle("slow");
                });
            });
        </script>
        <script>



            $(".verify-dl").click(function () {
                var url = '';
                var firstName = $('input[name=first_name]').val();
                var middleName = $('input[name=middle_name]').val();
                var lastName = $('input[name="last_name"]').val();
                var idtype = $('.idtype:checked').val();
                if (idtype === "license") {
                    url = './rest/groot/db/api/dl-verification';

                    var License_number = $('input[name="license_number"]').val();
                    var licence_verson_number = $('input[name="license_version"]').val();
                    if (firstName === "") {
                        $("#error_first_name").text("This field is required ");
                        return false;
                    } else if (lastName === "") {
                        $("#error_last_name").text("This field is required ");
                        return false;
                    } else if (License_number === "") {
                        $("#spanlicenseNumber").text("This field is required ");
                        return false;
                    } else if (licence_verson_number === "") {
                        $("#spanVersionnumber").text("This field is required ");
                        return false;
                    }
                } else if (idtype === "passport") {
                    url = './rest/groot/db/api/pp-verification';
                    var passport_number = $('input[name="passport_number"]').val();
                    var passport_expiry = $('input[name="passport_expiry"]').val();
                    if (firstName === "") {
                        $("#error_first_name").text("This field is required ");
                        return false;
                    } else if (lastName === "") {
                        $("#error_last_name").text("This field is required ");
                        return false;

                    } else if (passport_number === "") {
                        $("#spanPassportnumber").text("This field is required ");
                        return false;

                    }
                }

                var Date_of_Birth = $('input[name="date_of_birth"]').val();
                var DataObj = {license_number: License_number,
                    licence_verson_number: licence_verson_number, date_of_Birth: Date_of_Birth, firstName: firstName,
                    middleName: middleName, lastName: lastName, passport_number: passport_number, passport_expiry: passport_expiry};
                console.log(JSON.stringify(DataObj));
//                alert(JSON.stringify(DataObj));

                swal({
                    title: "Processing",
                    text: "Please wait while we are verifying your details.",
                    type: "info",
                    timer: 3500,
                    showConfirmButton: true
                });
                $.ajax({
                    type: 'POST',
                    url: url,
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(DataObj),
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        var obj = JSON.parse(data);
                        console.log(JSON.stringify(obj));
                        if (idtype === "license") {
                            if (obj.driversLicence.verified) {
                                swal({
                                    title: "Success",
                                    text: "Data is Verified.",
                                    type: "info",
                                    timer: 3500,
                                    showConfirmButton: true
                                });
                                if (obj.driversLicence.fields.firstName === "Yes") {
                                    $('.first_name_wrong').hide();
                                    $('.first_name_right').show();


                                } else if (obj.driversLicence.fields.firstName === "No") {

                                    $('.first_name_right').hide();
                                    $('.first_name_wrong').show();

                                }
                                if (obj.driversLicence.fields.middleName === "Yes") {
                                    $('.middle_name_wrong').hide();
                                    $('.middle_name_right').show();

                                } else if (obj.driversLicence.fields.middleName === "No") {
                                    $('.middle_name_right').hide();
                                    $('.middle_name_wrong').show();

                                }
                                if (obj.driversLicence.fields.lastName === "Yes") {
                                    $('.last_name_right').show();
                                    $('.last_name_wrong').hide();

                                } else if (obj.driversLicence.fields.lastName === "No") {

                                    $('.last_name_right').hide();
                                    $('.last_name_wrong').show();

                                }
                                if (obj.driversLicence.fields.dateOfBirth === "Yes") {
                                    $('.dob_wrong').hide();
                                    $('.dob_right').show();

                                } else if (obj.driversLicence.fields.dateOfBirth === "No") {

                                    $('.dob_right').hide();
                                    $('.dob_wrong').show();

                                }
                                if (obj.driversLicence.fields.licenceNo === "Yes") {
                                    $('.License_number_wrong').hide();
                                    $('.License_number_right').show();

                                } else if (obj.driversLicence.fields.licenceNo === "No") {

                                    $('.License_number_right').hide();
                                    $('.License_number_wrong').show();

                                }

                            } else {
                                swal({
                                    title: "Failed",
                                    text: "Wrong Detils.",
                                    type: "info",
                                    timer: 3500,
                                    showConfirmButton: true
                                });
                                $('.fa-check-circle ').hide();
                                $('.btn-close').hide();


//                                alert("wrong data");
                            }
                        } else if (idtype === "passport") {
                            if (obj.passport.verified) {
                                swal({
                                    title: "Success",
                                    text: "Data is Verified.",
                                    type: "info",
                                    timer: 3500,
                                    showConfirmButton: true
                                });

                                $('.first_name_right').show();
                                $('.middle_name_right').show();
                                $('.last_name_right').show();
                                $('.dob_right').show();
                                $('.passport_expiry_right').show();
                                $('.passport_number_right').show();


                            } else {
                                swal({
                                    title: "Failed",
                                    text: "Wrong Detils.",
                                    type: "info",
                                    timer: 3500,
                                    showConfirmButton: true
                                });
                                $('.first_name_right').hide();
                                $('.middle_name_right').hide();
                                $('.last_name_right').hide();
                                $('.dob_right').hide();
                                $('.passport_expiry_right').hide();
                                $('.passport_number_right').hide();
                                $('.fa-check-circle ').hide();
//                                alert("wrong data");
                            }

                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(" inside error" + jqXHR);
                    }
                });
            });
            $('.country').click(function () {
                $('.error').text("");
            });
        </script>
        <script>
            $('#first_name').keyup(function () {
                $('#error_first_name').text('');
                $('.first_name_right').hide();
                $('.first_name_wrong').hide();
            });
            $('#middle_name').keyup(function () {
                $('#error_first_name').text('');
                $('.middle_name_right').hide();
                $('.middle_name_wrong').hide();
            });
            $('#last_name').keyup(function () {
                $('#error_last_name').text('');
                $('.last_name_right').hide();
                $('.last_name_wrong').hide();
            });
            $('#Passportnumber').keyup(function () {
                $('#spanPassportnumber').text('');
                $('.passport_number_right').hide();

            });
            $('#licenseNumber').keyup(function () {
                $('#spanlicenseNumber').text('');
                $('.License_number_right').hide();
                $('.License_number_wrong').hide();

            });
            $('#Versionnumber').keyup(function () {
                $('#spanVersionnumber').text('');
            });
            $('.date_of_birth').change(function () {
                $('.dob_right').hide();
                $('.dob_wrong').hide();
            });
            $('.passportExp').change(function () {
                $('.passport_expiry_right').hide();

            });

        </script>
    </body>
</html>