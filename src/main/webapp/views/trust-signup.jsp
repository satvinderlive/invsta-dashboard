<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en" >
    <head>
        <title>westpac</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="/resources/favicon.png" rel="shortcut icon"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="./resources/css/signup/style.css">
        <link rel="stylesheet" href="./resources/css/signup/custom.css">
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <link rel="stylesheet" href="./resources/css/signup/intlTelInput.css">
        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css'>
        <link href="./resources/css/sweetalert.css" rel="stylesheet" />
        <style>
            img.logo {
                width: 30%;
            }
            .intl-tel-input .flag-dropdown .selected-flag .down-arrow {
                top: 6px;
                position: relative;
                left: 20px;
                width: 0px;
                height: 0;
                border-left: 3px solid transparent;
                border-right: 3px solid transparent;
                border-top: 5px solid black;
            }
            input.verify-dl {
                background: ba;
                background: #65b9ac!important;
                z-index: 999999999;
                color: #fff!important;
                max-width: 103px;
                height: 26px;
                padding: 2px!important;
                font-size: 11px!important;
                cursor: pointer;
            }
            @media(max-width:575px){
                .this-space {
                    margin: 10px 0!important;
                }
            }
            div#detectcompany {
                position: absolute;
                background: white;
                z-index: 999;
                height: 163px;
                overflow: auto;
                border: 1px solid #ccc;
                padding: 4px 5px;
                border-top: 0;
                display: none;
            }

            .checknear {
                text-align: left;
                font-size: 12px;
                border-bottom: 1px solid #ccc;
                padding-bottom: 6px;
            }

            input#companyName {
                margin: 0;
            }
            .autocomplete {
                position: relative;
                display: inline-block;
            }

            .autocomplete-items {
                position: absolute;
                border: 1px solid #d4d4d4;
                border-bottom: none;
                border-top: none;
                z-index: 99;
                /*position the autocomplete items to be the same width as the container:*/
                top: 100%;
                left: 0;
                right: 0;
            }

            .autocomplete-items div {
                padding: 10px;
                cursor: pointer;
                background-color: #fff; 
                border-bottom: 1px solid #d4d4d4; 
            }

            /*when hovering an item:*/
            .autocomplete-items div:hover {
                background-color: #e9e9e9; 
            }

            /*when navigating through the items using the arrow keys:*/
            .autocomplete-active {
                background-color: DodgerBlue !important; 
                color: #ffffff; 
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="loged_user">
                <p>Already have an Account<a href="./login?logout">Login</a></p>
            </div>
            <div class="row">
                <div class="col-md-12 ">
                    <div class="content_body">
                        <form id="msform" name="msRegform" class="form-signup" action="/chelmer" method="POST" autocomplete="off">
                            <div class="w3-light-grey">
                                <!--<div id="myBar" class="progress-bar" style="width:0%"></div>-->
                            </div>
                            <div class="logo-w">
                                <a href="#"><img class="logo" alt="" src="./resources/images/mint.png" ></a>
                            </div>
                            <div class="save-new-btn">
                                <input type="hidden" name="register_type" id="register_type" value="TRUST_ACCOUNT">
                                <input type="hidden" name="step" id="step" value="1">
                                <button id="" class=" saveExit director-btn all-btn-color save-font">Save & Exit</button>
                                <!--<span class="info-icon"><a href="#" data-toggle="tooltip" title="You can save and exit this application at any time by clicking on this Save & Exit button. We will then send you an email from which you can resume your application when you are ready. "><img src="./resources/images/i-icon.png"></a></span>-->
                                <div class="pulsating-circle4 pulsating-around6" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                    <span class="tooltiptext">You can save and exit this application at any time by clicking on this Save &amp; Exit button. We will then send you an email from which you can resume your application when you are ready.</span>
                                </div>
                            </div>
                            <fieldset id="step1">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please provide the below Trust details.  
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Name of Trust 
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control input-field " id="companyName" value="${company.companyName}" name="companyName" required="required" placeholder="Enter Trust name" onkeyup="myFunction()" />
                                                <div id="detectcompany"></div>
                                                <span class="error" id="spanCompanyName"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Trust address 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <input type="text" class="form-control input-field " value="${company.companyAddress}" placeholder="Trust address" name="companyAddress" id="companyAddress" autocomplete="off">
                                                <span class="error" id="spanCompanyAddress"></span>
                                            </div>
                                            <!--                                            <div class="col-sm-6">
                                                                                            <label class="label_input" style="text-align:left">
                                                                                                Postal address (if different from registered address) 
                                                                                            </label>
                                                                                        </div>
                                                                                        <div class="col-sm-6 ">
                                                                                            <input type="text" class="form-control input-field" value="${company.postalAddress}" placeholder="Enter postal address" name="companyAddress" id="postalAddress" autocomplete="off">
                                                                                            <span class="error" id="spanPostalAddress"></span>
                                                                                        </div>-->
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Country of registration
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-group country-set flag-drop">
                                                <input type="text" class="form-control countryname" id="Country_of_incorporation"   value="${company.countryOfResidence}" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">
                                                <span class="error" id="spanCountryCode"></span>
                                            </div>

                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Date of establishment
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="dob" id="Date_of_incorporation" placeholder="dd/mm/yyyy" class="input-field doc" value="${company.dateOfInco}"  data-lang="en"/>
                                                <span class="error" id="spanCompanyDate"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Type of Trust
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption form-group selectType">
                                                    <option value="1">-Select-</option>
                                                    <option value="2">Family</option>
                                                    <option value="4">Charitable</option>
                                                    <option value="3">Other</option>
                                                </select>
                                                <!--                                            </div>
                                                                                            <div class="col-sm-6 inputtype1">
                                                                                                <label class="label_input">
                                                                                                    Enter type
                                                                                                </label>
                                                                                            </div>
                                                                                            <div class="col-sm-6 ">-->
                                                <input type="text" class="form-control input-field otherType" id="" name="trustType" placeholder="Enter type"/>
                                                <span class="error" id="spanTypeTrust"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Trust registration number
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-group country-set flag-drop">
                                                <input type="text" class="form-control input-field" id="registrationNumber" name="registrationNumber" value="${company.registerNumber}"required="required" placeholder="Enter registration number" onkeyup="myFunction()" />
                                                <span class="error" id="spanRegistrationNumber"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Are you working with an Investment Adviser?
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption form-group avisor-click" id="investmentAdviser" >
                                                    <c:if test = "${company.investAdviser == ''}">
                                                        <option value="">${company.investAdviser}</option>
                                                    </c:if>
                                                    <option value="1">No</option>
                                                    <option value="2">Yes</option>
                                                </select>
                                            </div>
                                                <div class="row advisor-show">
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Select Advisery Company
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption advisor_company form-group" id="companyAdvisor" >
                                                    <c:if test = "${company.companyAdvisor  eq ''}">
                                                        <option value="">${company.companyAdvisor}</option>
                                                    </c:if>
                                                    <c:forEach items="${advisories}" var="advisor">
                                                        <option value="${advisor.login_id}">${advisor.companyName}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Select an Adviser 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption advisor form-group" id="selectAnAdviser" >
                                                    <c:if test = "${company.selectAdviser eq ''}">
                                                        <option value="">${company.selectAdviser}</option>
                                                    </c:if>
                                                    <c:forEach items="${advisories}" var="advisor">
                                                        <option value="${advisor.login_id}">${advisor.advisorName}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <!--                                            <div class="col-sm-6">
                                                                                            <label class="label_input" style="text-align:left">
                                                                                                Select Advisor Company
                                                                                            </label>
                                                                                        </div>
                                                                                        <div class="col-sm-6 ">
                                                                                            <select class="selectoption form-group" id="companyAdvisor" >
                                            <c:if test = "${company.companyAdvisor  != ''}">
                                                <option value="">${company.companyAdvisor}</option>
                                            </c:if>
                                            <option value="1"></option>
                                            <option value="2"></option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="label_input" style="text-align:left">
                                            Select an Adviser 
                                        </label>
                                    </div>
                                    <div class="col-sm-6 ">
                                        <select class="selectoption form-group" id="selectAnAdviser" >
                                            <c:if test = "${company.selectAdviser eq ''}">
                                                <option value="">${company.selectAdviser}</option>
                                            </c:if>
                                            <option value="1"></option>
                                            <option value="2"></option>
                                        </select>
                                    </div>-->
                                        </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Please upload a copy of trust deed
                                                </label>
                                            </div>
                                            <div class="col-sm-6 closestcls">
                                                <input type="file" name="myDeedFile" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" class="attach-btn2 checkname" id="attachdeedfile">
                                                <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile"><i class="far fa-times-circle"></i></a>
                                                <span class=" error error_attachdeedfile"></span>
                                            </div>
                                        </div>
                                        

                                    </div> 
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <a href="#" name="previous" class="previous action-button-previous privous-signup" value="Previous" >Previous</a>
                                <input type="button" name="next" class="next1 action-button"  value="Continue"/>
                            </fieldset>
                            <fieldset id="step2">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Trustees and beneficial owners.
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                                <small class="verification-otp1">
                                                    Please enter the details for all trustees, beneficial owners, and other people with effective control.
                                                </small>
                                            <div class="col-sm-12">
                                                <input type="hidden" class="input-field counter" id="counter" >
                                                <input type="hidden" id="directors-index"/>
                                                <input type="hidden" id="directors-value"/>
                                            </div>
                                        </div>
                                        <div class="row director-section">

                                            <div class="row director-add">
                                                <div class="col-sm-4">
                                                    <label class="label_input" style="text-align:left">
                                                        Full name of trustee/beneficial owner
                                                    </label>
                                                </div>
                                                <div class="col-sm-2 ">
                                                    <select class="selectoption option-add" id="shareholder">
                                                        <option value="Appointment">Mr</option>
                                                        <option value="Interview">Mrs</option>
                                                        <option value="Regarding a post">Miss</option>
                                                        <option id="other" value="Other">Master</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" class="fname input-field tags" id="fname" name="fullName" value="${company.fname}" placeholder="Enter full name" >
                                                    <span class="error spanfname" id="spanfname"></span>
                                                    <input type='hidden' class='cls-me' name='me' value='Y'>
                                                </div>
                                                <div class="col-sm-2 this-space">
                                                    <a href="javascript:void(0)" class="this-btn this-is-me check-this-btn"  onclick='addbtnfn(this);'>This is me</a>
                                                </div>
                                            </div> 
                                            <c:choose>
                                                <c:when test="${not empty company.moreInvestorList}">
                                                    <c:forEach items="${company.moreInvestorList}" var="moreInvestor" varStatus="loop">
                                                        <div class="row director-add">
                                                            <div class="col-sm-4">
                                                                <label class="label_input" style="text-align:left">
                                                                    Full name of trustee/beneficial owner
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 ">
                                                                <select class="selectoption " id="shareholder">
                                                                    <option value="Appointment">Mr</option>
                                                                    <option value="Interview">Mrs</option>
                                                                    <option value="Regarding a post">Miss</option>
                                                                    <option id="other" value="Other">Master</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="fname input-field" id="fname" name="fullName" value="${moreInvestor.fname}" placeholder="Enter full name" >
                                                                <span class="error spanfname" id="spanfname"></span>
                                                                <input type='hidden' class='cls-me' name='me' value='N'>
                                                            </div>
                                                            <div class="col-sm-2 this-space">
                                                                <a href="javascript:void(0)" class="this-btn this-is-me "  onclick='addbtnfn(this);'>This is me</a>
                                                            </div>
                                                        </div>
                                                    </c:forEach>
                                                </c:when>


                                            </c:choose>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <button id="btn20" class="director-btn all-btn-color">Add another trustee/beneficial owner</button>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="focus-border" style="display:block;" id="validationId" ></span>
                                </div>

                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous1 action-button-previous" value="Previous" />
                                <input type="button" name="next"  class=" next2 action-button"  value="Continue"/>
                            </fieldset>

                            <fieldset id="step3">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please provide the following information for  <span class="director-name this-name"></span>
                                        </h5>
                                    </div>

                                    <div class="input-content">
                                        <div class="row">
                                            <!-- <div class="col-sm-12">
                                                    <p><span class="director-name this-name"></span></p>
                                            </div> -->
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Preferred first name (if applicable) 
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="dob"  id="preffered_name" value="" placeholder="Enter preferred name (optional)" class="input-field continer3Dob" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Date of birth
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="dob" class="dob" id="Date_of_Birth" value="${company.dateOfBirth}" placeholder="dd/mm/yyyy" class="input-field continer3Dob" data-format="DD/MM/YY" data-lang="en" required/>
                                                <span class="error" id="spanDate_of_Birth"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Occupation
                                                </label>
                                            </div>
                                            <div class="col-sm-6" >
                                                <select name="occupation" class="Occupation selectoption  otherOcc  form-group OccupationOption">
                                                    <option value="-Select-">-Select-</option>
                                                    <c:forEach items="${occupation}" var="occ">
                                                        <option value="${occ.mmc_occu_id}" 
                                                                <c:if test="${occ.mmc_occu_id eq person.occupation}">
                                                                    selected                                                                     
                                                                </c:if> >${occ.mmc_occu_name}</option>
                                                    </c:forEach>
                                                    <option value="other">Other</option>
                                                </select>
                                                <input type="text" name="Occupation" id="cOccupation" placeholder="Enter occupation " value="${company.occupation}" class="Occupation selectOcc input-field" onkeyup="myFunction()"/>
                                                <span class="error" id="spanOccupation"></span>
                                            </div>
                                            <!--                                            <div class="col-sm-6">
                                                                                            <input type="text" class="form-control input-field" id="cOccupation" value="${company.occupation}" name="Occupation" placeholder="Enter occupation" onkeyup="myFunction()" />
                                                                                            <span class="error" id="spanOccupation"></span>
                                                                                        </div>-->
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Country of residence 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-group country-set flag-drop">
                                                <input type="text" class="form-control countryname"  id="holderCountryOfResidence" value="${company.holderCountryOfResidence}" name="holderCountryOfResidence" required="required" placeholder="Enter Country Code" readonly="readonly">                                                                                                                                                                                                   
                                                <span class="error" id="spanHolderCountryOfResidence"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Position in Trust 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 trust-signup">
                                                <select class="selectoption form-group" id="positionInCompany">
                                                    <option value="-Select-">-Select-</option>
                                                    <c:if test = "${company.positionInCompany eq ''}">
                                                        <option value="${company.positionInCompany}" selected>${company.positionInCompany} </option>
                                                    </c:if>
                                                    <option value="Independent trustee">Independent trustee</option>
                                                    <option value="Interview">Beneficial owner</option>
                                                    <option value="4"> Effective control</option>
                                                </select>
                                                <span class="error" id="error-positionInCompany"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous2 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next3 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step4">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter contact details for  <span class="director-name this-name"></span>
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <!-- <div class="col-sm-12">
                                                    <p><span class="director-name this-name"></span></p>
                                            </div> -->
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Home address
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" id="address" value="${company.address}" class="form-control input-field"  placeholder="Enter home address" />
                                                <span class="error" id="spanHomeaddress"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Mobile number (required)
                                                </label>
                                                    <div class="pulsating-circle4 pulsating-around6 circler" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                                        <span class="tooltiptext minor-tooltip-2 ">We require at least one contact number for you. If you do not have a mobile number, please provide an other type of number.</span>
                                                    </div>
                                            </div>
                                            <div class="col-sm-6 mobile_number first-mobile top-show">
                                                <input type="text" class="form-control codenumber mobile_country_code" value="${company.countryCode3}" id="countryCode3" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">
                                                <input type="tel" class="form-control error codename mobile_number1" id="mobileNo" name="mobileNo" value="${company.mobileNo}" required="required" placeholder="Enter mobile number" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                                <span class="error" id="spanOPmobileNo"></span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label class="label_input" style="text-align:left">
                                                    Other number (optional) 
                                                </label>
                                            </div>
                                            <div class="col-sm-3">
                                                <select class="selectoption form-group" id="otherNumber" >
                                                    <option value="Home">Home</option>
                                                    <option value="Work">Work</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                            </div> 
                                            <div class="col-sm-6 mobile_number first-mobile mobile-index1">
                                                <input type="text" class="form-control codenumber"  id="countryCode" value="${company.otherCountryCode}" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">
                                                <input type="tel" class="form-control error codename" id="mobileNo2" value="${company.otherMobileNo}" name="otherNumber" required="required" placeholder="Enter number " onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                                <span class="error" id=""></span>
                                            </div>
                                            <!-- <div class="col-sm-6">
                                                    <label class="label_input">
                                                    Work phone number (optional) 
                                                    </label>
                                                    </div>
                                                    <div class="col-sm-6 mobile_number first-mobile mobile-index2">
                                                    <input type="text" class="form-control codenumber"  id="countryCode2" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">
                                                    <input type="tel" class="form-control error codename" name="mobileNo" required="required" placeholder="Enter work number (optional)">
                                                    
                                            </div> -->

                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->
                                </div>
                                <input type="button" name="previous" class="previous3 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next4 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step5">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Account security: mobile verification for  <span class="director-name this-name"></span>
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="input-content-text">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <small class="verification-otp1">
                                                        To verify your mobile number, please choose to receive a unique verification code via text message or phone call. 
                                                    </small>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 otp-icon">
                                                    <ul class="radio_button">
                                                        <li>
                                                            <input type="radio" class="senderType" id="f-option"  name="senderType" value="sms" style="width: auto;">
                                                            <img src="./resources/images/icon/message-icon.png" style="width: 20px; margin-left: 5px;">
                                                            <label for="f-option"></label>
                                                            <div class="check"></div>
                                                        </li>
                                                        <li>
                                                            <input type="radio" class="senderType" id="s-option" name="senderType" value="sms" style="width: auto;">
                                                            <i class="fas fa-phone"></i>
                                                            <label for="s-option"></label>
                                                            <div class="check"><div class="inside"></div></div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!--                                                <div class="col-sm-6">
                                                        <input type="button" name="next" id="generate-otp" class="action-button" value="Generate OTP" />
                                                </div>-->                                              
                                                <span class="error" id="error-generateOtp"></span>

                                            </div>
                                            <div class="row otp-area">
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Verification code 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" id="verificationa" name="verification" placeholder="Enter unique code  " class="input-field otpkey"/>
                                                    <span class="error spanverification" id="spanverificationa" ></span>
                                                </div>
                                            </div>
                                            <!-- <div class="face" id="error-loader" style="display:none" >
                                                    <div class="hand" ></div>
                                            </div> -->
                                            <small class="verification-otp"> If you have not received your unique verification code within one minute, you can request another by  <a href="#">clicking here.</a>.</small>
                                        </div>
                                    </div>



                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->
                                </div>
                                <input type="button" name="previous" class="previous4 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next5 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step6">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter the identification details for <span class="director-name this-name"></span>
                                        </h5>
                                    </div>
                                    <input type="hidden" class="which_container abc" value="#Date_of_Birth"/>
                                    <div class="input-content">
                                        <div class="row">
                                            <!-- <div class="col-sm-12">
                                                    <p><span class="director-name this-name"></span></p>
                                            </div> -->
                                            <div class="col-sm-6">
                                                <label class="label_input detail-2" style="text-align:left">
                                                    Which type of ID are you providing
                                                </label>
                                            </div>
                                            <div class="col-sm-6 selDiv">
                                                <select class="selectoption form-group option Id_Type" id="src_of_fund1">
                                                    <option value="1">NZ Driver Licence</option>
                                                    <option value="2">NZ Passport</option>
                                                    <option value="3">Other </option>

                                                </select>
                                            </div>
                                            <div class="row drivery-licence">
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        First Name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="first_name" name="licence_first_name" placeholder="Enter first name" id="licence_first_name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_licence_first_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Middle Name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="middle_name" name="licence_middle_name" placeholder="Enter middle name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_licence_middle_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Last Name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="last_name" name="licence_last_name" placeholder="Enter last name"  id="licence_last_name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_licence_last_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Licence number
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-field" id="licenseNumber" value="${company.licenseNumber}" name="license_number" required="required" placeholder="Enter licence number " onkeyup="myFunction()" />
                                                    <span class="error" id="spanlicenseNumber" ></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Expiry date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name="dob" id="Expirydate" placeholder="dd/mm/yyyy" value="${company.licenseExpiryDate}" class="input-field dob1 lic_expiry_Date" data-format="DD/MM/YY" data-lang="en" required/>                                 
                                                    <span class="error" id="spanExpirydate"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Version number 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="college" id="versionNumber" value="${company.versionNumber}" placeholder="Enter version number" class="input-field lic_verson_number" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                                                    <span class="error" id="spanVersionnumber"></span>
                                                </div>
                                            </div>
                                            <div class="row passport-select">

                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        First Name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="first_name" name="passport_first_name" placeholder="Enter first name" id="passport_first_name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_passport_first_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Middle Name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="middle_name" name="passport_middle_name" placeholder="Enter middle name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_passport_middle_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Last Name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="last_name" name="passport_last_name" placeholder="Enter last name" id="passport_last_name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_passport_last_name"></span>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Passport number
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-field" id="Passportnumber" name="passport_number" value="${company.passportNumber}" required="required" placeholder="Enter passport number" onkeyup="myFunction()" />
                                                    <span class="error" id="spanPassportnumber" ></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Expiry date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name="dob" id="PExpirydate" placeholder="dd/mm/yyyy" value="${company.passportExpiryDate}" class="input-field dob1  pass_expiry" data-format="DD/MM/YY" data-lang="en" required/>                                 
                                                    <span class="error" id="spanPExpirydate"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Country of issue  
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 flag-drop">
                                                    <input type="text" class="form-control countryname"  id="Countryofissue" name="countryCode" required="required" value="${company.countryOfIssue}" placeholder="Enter Country Code" readonly="readonly">    
                                                    <span class="error" id="spanCountryofissue"></span>
                                                </div>
                                            </div>
                                            <div class="row other-select">
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Type of ID   
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-field" id="TypeofID" name="fullName" required="required" placeholder="Enter ID type" onkeyup="myFunction()" />
                                                    <span class="error" id="spanTypeofID" ></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Expiry date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name="dob" id="typeExpirydate" placeholder="dd/mm/yyyy" class="input-field dob1" data-format="dd/mm/yyyy" data-lang="en" required/>                                 
                                                    <span class="error" id="spantypeExpirydate"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Country of issue  
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 flag-drop">
                                                    <input type="text" class="form-control countryname"  id="countryname1" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                    <span class="error" id="span_countryname1"></span>
                                                </div>
                                                <div class="col-sm-129 closestcls">
                                                    <input type="file" name="myFile" id="otherDocument" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" class="attach-btn checkname">
                                                    <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile"><i class="far fa-times-circle"></i></a>
                                                    <span class="error" id="span-otherDocument"></span>
                                                </div>
                                            </div>
                                            <input type="hidden" name="investor_verify" id="investor_verify" value="false">
                                            <!--                                            <div class="col-sm-6 verifybtn">
                                                                                            <input type="button" name="myFile" class="verify-dl" value="Verify Details">
                                                                                            <span class="error" id="error_other_ile"></span>
                                                                                        </div>-->

                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous5 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next6 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step7">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter the tax details for <span class="director-name this-name"></span>
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <!-- <div class="col-sm-12">
                                                    <p><span class="director-name this-name"></span></p>
                                            </div> -->
                                            <!-- <div class="col-sm-6">
                                                <label class="label_input">
                                                                                                Prescribed Investors Rate (PIR)
                                                                                                </label>
                                                                                                </div>
                                                                                                <div class="col-sm-6 details-pos">
                                                <select class="selectoption form-group aml-select" id="src_of_fund11">
                                                                                                <option value="1">28%</option>
                                                                                                <option value="2">10.5%</option>
                                                                                                <option value="3">17.5%</option>
                                                                                                </select>
                                                                                                <a href="javascript:void(0)" class="aml-link">What’s my PIR?</a>
                                                                                        </div>-->


                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    IRD Number 
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control input-field" id="irdNumber" name="IRDNumber" required="required" placeholder="XXX-XXX-XXX" onkeydown="checkird(this)" onkeypress='return event.charCode >= 48 && event.charCode <= 57' />
                                                <span class="error" id="spanIRDNumber"></span>
                                            </div> 
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Are you a US citizen or US tax resident, or a tax resident in any other country?
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption selectoption1 form-group" id="USCitizen">
                                                    <option value="1">No</option>
                                                    <option value="2">Yes</option>
                                                </select>
                                            </div>

                                            <div class="row yes-option">
                                                <div class="row yes-new checktindata">
                                                    <div class="col-sm-12">
                                                        <h5 class="element-header aml-text">
                                                            Please enter all of the countries (excluding NZ) of which you are a tax resident.
                                                        </h5>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input" style="text-align:left">
                                                            Country of tax residence:
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6 details-pos flag-drop">
                                                        <input type="text" class="form-control countrynameexcludenz tex_residence_Country"  id="countryOfTaxResidence" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                        <span class="error countrynameexcludenz-error" ></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Tax Identification Number (TIN) 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-field TIN" id="taxIdenityNumber" name="taxIdenityNumber" required="required" placeholder="Enter TIN " onkeyup="myFunction()" />
                                                        <span class="error tin-error" ></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Reason if TIN not available  
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-field removedata resn_unavailable" name="reasonTIN" value="${company.countryOfResidence}" required="required" placeholder=""/>
                                                        <span class="error resn_unavailable-error" id="reason_tin_error"></span>
                                                    </div> 
                                                </div>
                                                <div class="col-sm-12 add-another">
                                                    <a class="add-another2 all-btn-color" href="javascript:void(0)" id="add-country-another">Add another country </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous6 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next7 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step8">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                                <span> We require further information about each trustee/beneficial owner. </span>
                                        </h5>
                                        <div class="header-line">
                                        <h6 >You can provide this information now, alternatively we’ll send an email to them requesting they provide this information.</h6>
                                        </div>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p><span class="director-name curr-director"></span></p>
                                            </div>


                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Email address
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="email" class="form-control input-field removedata" id="more-investor-email" name="email" required="required" placeholder="Enter email address" onkeyup="myFunction()" />
                                                <span class="error" id="more-investor-email-error"></span>
                                            </div>
                                            <!-- <div class="col-sm-12">
                                                    <small>
                                                    We require further information about each Director/Shareholder. You can provide this information now, alternatively we’ll send an email to them requesting they provide this information. 
                                                    </small>
                                            </div> -->
                                            <div class="col-sm-12 new-box">


                                                <input type="radio" id="radio05" value="send-now" data-id="send-now" class="s-options removecheck" name="year" checked>
                                                <label for="radio05"><span class="enter-btn-small small-padd">I will enter their details now</span> </label>
                                                <input type="radio" id="radio06" value="send-email"  data-id="send-email" class="s-options removecheck"  name="year">
                                                <label for="radio06"><span class="enter-btn-small small-padd1">Send them an email requesting info</span> </label>


                                            </div>
                                            <!-- <div class="col-sm-129">
                                                    <input type="file" name="myFile" class="attach-btn2">
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous7 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next8 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step9">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter the details for <span class="director-name curr-director"></span>
                                        </h5>
                                    </div>

                                    <div class="input-content">
                                        <div class="row">
                                            <!-- <div class="col-sm-12">
                                                    <p><span class="director-name curr-director"></span></p>
                                            </div> -->
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Home address
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" id="address1" class="form-control input-field removedata more-investor-address"  placeholder="Enter home address" />
                                                <span class="error" id="more-investor-address-error"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Mobile phone number
                                                </label>
                                            </div>
                                            <div class="col-sm-6 mobile_number first-mobile mobile-index1">
                                                <input type="text" class="form-control codenumber countryCode"  id="more-investor-countrycode" name="countryCode" required="required" placeholder="Enter mobile number" readonly="readonly">
                                                <input type="tel" class="form-control error codename removedata" id="more-investor-mobile" name="mobileNo" required="required" placeholder="Enter mobile number" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                                <span class="error" id="more-investor-mobile-error"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Date of birth
                                                </label>
                                            </div>
                                            <div class="col-sm-6">

                                                <input type="text" name="dob" id="more-investor-dob" placeholder="dd/mm/yyyy" class="input-field dob " data-format="dd/mm/yyyy" data-lang="en" required/>
                                                <span class="error" id="more-investor-dob-error"></span>

                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Occupation
                                                </label>
                                            </div>
                                            <!--                                            <div class="col-sm-6">
                                                                                            <input type="text" class="form-control input-field removedata" id="more-investor-occupation" name="fullName" required="required" placeholder="Enter occupation" onkeyup="myFunction()" />
                                                                                            <span class="error" id="more-investor-occupation-error"></span>
                                                                                        </div>-->
                                            <div class="col-sm-6" >
                                                <select name="occupation" class="Occupation selectoption  otherOcc  form-group OccupationOption" >
                                                    <option value="-Select-">-Select-</option>
                                                    <c:forEach items="${occupation}" var="occ">
                                                        <option value="${occ.mmc_occu_id}" 
                                                                <c:if test="${occ.mmc_occu_id eq person.occupation}">
                                                                    selected                                                                     
                                                                </c:if> >${occ.mmc_occu_name}</option>
                                                    </c:forEach>
                                                    <option value="other">Other</option>
                                                </select>
                                                <input type="text" name="Occupation" id="more-investor-occupation" placeholder="Enter occupation " value="${company.occupation}" class="form-control selectOcc input-field removedata" onkeyup="myFunction()"/>
                                                <span class="error" id="spanOccupation"></span>
                                            </div>

                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Position in Trust 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption form-group" id="more-investor-positionInCompany">
                                                    <option value="0">-Select-</option>
                                                    <option value="Independent trustee">Independent trustee</option>
                                                    <option value="Interview">Beneficial owner</option>
                                                    <option value="4"> Effective control</option>
                                                </select>
                                                <span class="error" id="more-investor-positionInCompany-error"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous8 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next9 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step10">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter the identification details for <span class="director-name curr-director"></span>
                                        </h5>
                                    </div>
                                    <input type="hidden" class="which_container abc" value="#more-investor-dob"/>
                                    <div class="input-content">
                                        <div class="row">
                                            <!-- <div class="col-sm-12">
                                                    <p><span class="director-name curr-director"></span></p>
                                            </div> -->
                                            <div class="col-sm-6">
                                                <label class="label_input detail-2">
                                                    Which type of ID are you providing 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption form-group Id_Type" id="src_of_fund2">
                                                    <option value="1">NZ Driver Licence</option>
                                                    <option value="2">NZ Passport</option>
                                                    <option value="3">Other</option>

                                                </select>
                                            </div>
                                            <div class="row drivery-licence1">

                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        First Name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="first_name" name="licence_first_name" placeholder="Enter first name" id="more_licence_first_name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="more_error_licence_first_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Middle Name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="middle_name" name="licence_middle_name" placeholder="Enter middle name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_licence_middle_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Last Name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="last_name" name="licence_last_name" placeholder="Enter last name" id="more_licence_last_name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="more_error_licence_last_name"></span>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Licence number 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-field removedata" id="more-investor-licenseNumber" name="license_number" value="${company.licenseNumber}" required="required" placeholder="Enter licence number " onkeyup="myFunction()" />
                                                    <span class="error" id="more-investor-licenseNumber-error"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Expiry date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name="expiryDate" id="more-investor-licenseExpiryDate" placeholder="dd/mm/yyyy" class="input-field dob1 lic_expiry_Date" value="${company.licenseExpiryDate}" data-format="DD/MM/YY" data-lang="en" required/>                                 
                                                    <span class="error" id="more-investor-licenseExpiryDate-error"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Version number 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="versionNumber" id="more-investor-versionNumber" value="${company.versionNumber}" placeholder="Enter version number" class="input-field removedata lic_verson_number" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
                                                    <span class="error" id="more-investor-versionNumber-error"></span>
                                                </div>
                                            </div>
                                            <div class="row passport-select1">
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        First Name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="first_name" name="passport_first_name" placeholder="Enter first name" id="more_passport_first_name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="more_error_passport_first_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Middle Name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="middle_name" name="passport_middle_name" placeholder="Enter middle name" id="more_passport_middle_name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_passport_middle_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Last Name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="last_name" name="passport_last_name" placeholder="Enter last name" id="more_passport_last_name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="more_error_passport_last_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Passport number 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-field removedata" id="more-investor-passportNumber" name="passport_number" value="${company.passportNumber}" required="required" placeholder="Enter passport number" onkeyup="myFunction()" />
                                                    <span class="error" id="more-investor-passportNumber-error"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Expiry date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name=" passportExpiryDate " id="more-investor-passportExpiryDate" value="${company.passportExpiryDate}" placeholder="dd/mm/yyyy" class="input-field dob1 pass_expiry" data-format="DD/MM/YY" data-lang="en" required/>                                 
                                                    <span class="error" id="more-investor-passportExpiryDate-error"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Country of issue  
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 flag-drop">
                                                    <input type="text" class="form-control countryname"  id="more-investor-passportCountryOfIssue" value="${company.countryOfIssue}" name="countryOfIssue" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                    <span class="error" id="more-investor-passportCountryOfIssue-error"></span>
                                                </div>
                                            </div>
                                            <div class="row other-select1">
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Type of ID   
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-field removedata" id="more-investor-typeOfId" name="typeOfID" value="${company.typeOfID}" required="required" placeholder="Enter ID type" onkeyup="myFunction()" />
                                                    <span class="error" id="more-investor-typeOfId-error"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Expiry date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name="dob" id="more-investor-typeOfIdExpiryDate" placeholder="dd/mm/yyyy" class="input-field dob1" value="${company.typeCountryOfIssue}" data-format="DD/MM/YY" data-lang="en" required/>                                 
                                                    <span class="error" id="more-investor-typeOfIdExpiryDate-error"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Country of issue  
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 flag-drop">
                                                    <input type="text" class="form-control countryname"  id="more-investor-typeOfIdCountryOfIssue" name="Country of issue" value="${company.typeCountryOfIssue}" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                    <span class="error" id="more-investor-typeOfIdCountryOfIssue-error"></span>

                                                </div>
                                                <div class="col-sm-129 closestcls">
                                                    <input type="file" id="myFile" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" name="myFile" class="attach-btn checkname">
                                                    <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile"><i class="far fa-times-circle"></i></a>
                                                </div>
                                            </div>
                                            <input type="hidden" name="more_investor_verify" id="more_investor_verify" value="false">
                                            <!--                                            <div class="col-sm-6 verifybtn">
                                                                                            <input type="button" name="myFile" class="verify-dl" value="Verify Details">
                                                                                            <span class="error" id="error_other_ile"></span>
                                                                                        </div>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous9 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next10 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step11">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter the tax details for: <span class="director-name curr-director"></span>
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <!-- <div class="col-sm-12">
                                                                                                <p><span class="director-name curr-director"></span></p>
                                                                                        </div> -->

                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Country of residence 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-group country-set flag-drop">
                                                <input type="text" class="form-control countryname"  id="more-investor-countryOfResidence" name="countryOfResidence" value="${company.countryOfResidence}" required="required" placeholder="Enter Country Code" readonly="readonly">
                                                <span class="error" id="more-investor-countryOfResidence-error"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    IRD Number 
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control input-field removedata" id="more-investor-irdNumber" name="IRDNumber" value="${company.irdNumber}" required="required" placeholder="XXX-XXX-XXX" onkeydown="checkird(this)" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  />
                                                <span class="error" id="more-investor-irdNumber-error"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Are you a US citizen or US tax resident, or a tax resident in any other country?
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption selectoption2 form-group" id="more-investor-usCitizen">
                                                    <option value="1">No</option>
                                                    <option value="2">Yes</option>
                                                </select>
                                            </div>

                                            <div class="row yes-option1">
                                                <div class="row yes-new3 checktin3data">
                                                    <div class="col-sm-12">
                                                        <h5 class="element-header aml-text">
                                                            Please enter all of the countries (excluding NZ) of which you are a tax resident.
                                                        </h5>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input" style="text-align:left">
                                                            Country of tax residence:
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6 details-pos flag-drop">
                                                        <input type="text" class="form-control countrynameexcludenz removedata more-investor-tex_residence_Country" value="${company.countryOfTaxResidence}" name="countryOfTaxResidence" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                        <span class="error countrynameexcludenz-error" ></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Tax Identification Number (TIN) 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-field removedata more-investor-TIN" name="TIN" value="${company.taxIdentityNumber}" required="required" placeholder="Enter TIN " onkeyup="myFunction()" />
                                                        <span class="error more-investor-TIN-error" ></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Reason if TIN not available  
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-field removedata more-investor-resn_tin_unavailable" name="reasonTIN" value="${company.countryOfResidence}" required="required" placeholder=""/>
                                                        <span class="error more-investor-resn_tin_unavailable-error" ></span>
                                                    </div> 
                                                </div>
                                                <div class="col-sm-12 add-another">
                                                    <a class="add-another2 all-btn-color" href="javascript:void(0)" id="add-country-another3">Add another Country</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous10 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next11 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step12">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter the below tax information for the Trust.
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    What is the PIR 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption form-group selectoption3" id="pir">
                                                    <option value="1">28%</option>
                                                    <option value="2">17.5%</option>
                                                    <option value="3">10.5%</option>
                                                    <option value="4">0%</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Trust IRD number 
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control input-field" id="comIRDNumber" value="${company.companyIRDNumber}" name="companyIRDNumber" required="required" placeholder="XXX-XXX-XXX" onkeydown="checkird(this)" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
                                                <span class="error" id="spancomIRDNumber"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    What is the source of funds or wealth for this account?
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption form-group selectoption3" id="sourceOfFunds">
                                                    <option value="1">-Select-</option>
                                                    <option value="2">Business earnings</option>
                                                    <option value="3">Personal employment</option>
                                                    <option value="4">Financial investments</option>
                                                    <option value="5">Inheritance</option>
                                                    <option value="6">Gift</option>
                                                    <option value="7">Other</option>
                                                </select>
                                                <span class="error" id="sourceOfFunds-error"></span>
                                            </div>
                                            <div class="col-sm-6 inputtype">
                                                <label class="label_input">
                                                    Enter Detail
                                                </label>
                                            </div>
                                            <div class="col-sm-6 inputtype">
                                                <input type="text" class="form-control input-field" id="detail" name="detail" required="required" placeholder="Enter detail" onkeyup="myFunction()" />

                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Is the Trust a listed issuer?  
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption form-group" id="isCompanyListed">
                                                    <option value="1">No</option>
                                                    <option value="2">Yes</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Is the Trust a Government Department named in Schedule 1 of the State Sector Act 1998, or a local authority as defined in Section 5(2) of the Local Government Act 2002?
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption form-group" id="isCompanyGovernment">
                                                    <option value="1">No</option>
                                                    <option value="2">Yes</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input police-text" style="text-align:left">
                                                    Is the Trust the New Zealand Police? 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption form-group police-text" id="isCompanyNZPolice">
                                                    <option value="1">No</option>
                                                    <option value="2">Yes</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous11 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next12 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step13">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter the following CRS & FATCA information for the Trust: 
                                            <div class="pulsating-circle4 pulsating-around4 btn-chng-1 trust-info" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                                <span class="tooltiptext tool-ad-2">New Zealand has implemented rules which require financial institutions, including westpac, to collect certain information abouttheir clients’ foreign tax residency. For further information about the Foreign Account Tax Compliance Act (FATCA) or theCommon Reporting Standard (CRS) you can visit the Inland Revenue website at www.ird.govt.nz/international/exchange orspeak to a tax adviser. For further information about international tax residency rules you can visit the OECD website atwww.oecd.org/tax/automatic-exchange/crs-implementation-and-assistance/tax-residency. An ‘entity’ includes a company,trust, partnership, association, registered co-operative, or government body.</span></div>
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input " style="text-align:left">
                                                    Is the Trust a Financial Institution for FATCA or CRS purposes?
                                                    <div class="pulsating-circle4  pulsating-around4 btn-chng-2 trust-info-2 " data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                                        <span class="tooltiptext tool-ad">The term Financial Institution as defined by FATCA and CRS includes custodial institutions, depository institutions,investment entities or specified insurance companies. A family trust is likely to be a Financial Institution if 50% or more of the trust’s income is from financial assets or if the trust is managed by another financial institution.</span>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption selectoption6 form-group" id="isCompanyFinancialInstitution">
                                                    <option value="0">-Select-</option>
                                                    <option value="1">Yes</option>
                                                    <option value="2">No</option>
                                                </select>
                                                <span class="error" id="isCompanyFinancialInstitution-error"></span>
                                            </div>
                                            <div class="col-sm-6 selectno6">
                                                <label class="label_input police-text" style="text-align:left">
                                                    Is the Trust an Active or Passive Non-Financial Entity for FATCA and CRS purposes?
                                                    <div class="pulsating-circle4 pulsating-around5 btn-chng-3" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                                        <span class="tooltiptext">An Entity will be an Active NFE where less than 50% of the NFE’s income for the preceding calendar year or other appropriate reporting period is passive income and less than 50% of the NFE’s assets for the preceding calendar year or other appropriate reporting period produce, or are held for the production of, passive income. Passive income generally include non-trading investment income in the form of: interest, dividends, annuities, other financial arrangements income, rents and royalties.The Entity may also be characterized as an Active NFE under other criteria.Generally, an Entity will be a Passive NFE if it is not an active NFE.</span>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-sm-6 selectno6">
                                                <select class="selectoption form-group police-text" id="isCompanyActivePassive">
                                                    <option value="0">-Select-</option>
                                                    <option value="2">Active</option>
                                                    <option value="2">Passive</option>
                                                </select>
                                                <span class="error" id="isCompanyActivePassive-error"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input police-text" style="text-align:left">
                                                    Is the Trust a US citizen or US tax resident, or a tax resident in any other country?
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption selectoption4 form-group police-text" id="isCompanyUSCitizen">
                                                    <option value="0">-Select-</option>
                                                    <option value="1">No</option>
                                                    <option value="2">Yes</option>
                                                </select>
                                                <span class="error" id="isCompanyUSCitizen-error"></span>
                                            </div>

                                            <div class="row yes-option4">
                                                <div class="row yes-new4 checktin4data">
                                                    <div class="col-sm-12">
                                                        <h5 class="element-header aml-text">
                                                            Please enter all countries (excluding NZ) of which the Trust is a tax resident.
                                                        </h5>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input" style="text-align:left">
                                                            Country of tax residence
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6 details-pos flag-drop">
                                                        <input type="text" class="form-control countrynameexcludenz"  id="companyCountryOfTaxResidence" name="countryOfTaxResidence" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                        <span class="error countrynameexcludenz-error" id="countrynameexcludenz-error1"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Tax Identification Number (TIN)
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-field companyTaxIdentityNumber" id="companyTaxIdentityNumber" name="companyTaxIdentityNumber" required="required" placeholder="Enter TIN " onkeyup="myFunction()" />
                                                        <span class="error companyTaxIdentityNumber-error" ></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Reason if TIN not available 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-field companyReasonTIN" id="companyReasonTIN" name="companyReasonTIN" required="required" placeholder=""/>
                                                        <span class="error companyReasonTIN-error" ></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 add-another">
                                                    <a class="add-another2 all-btn-color" href="javascript:void(0)" id="add-country-another4">Add another country</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous12 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next13 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step14">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter the Trust bank account details (for distributions and/or redemptions).
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Bank name 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 details-pos">
                                                <select class="selectoption form-group aml-select bank_name" id="bankName">
                                                    <option value="No Bank Select yet">– Select – </option>
                                                    <option value="Bank of New Zealand" data-id="02">Bank of New Zealand</option>
                                                    <option value="ANZ Bank New Zealand" data-id="01">ANZ Bank New Zealand</option>
                                                    <option value="ASB Bank" data-id="12">ASB Bank</option>
                                                    <option value="Westpac" data-id="03">Westpac</option>
                                                    <option value="Heartland Bank" data-id="03">Heartland Bank</option>
                                                    <option value="Kiwibank" data-id="38">Kiwibank</option>
                                                    <option value="SBS Bank" data-id="">SBS Bank</option>
                                                    <option value="TSB Bank" data-id="15">TSB Bank</option>
                                                    <option value="The Co-operative Bank" data-id="02">The Co-operative Bank</option>
                                                    <option value="NZCU" data-id="03">NZCU</option>
                                                    <option value="Rabobank New Zealand" data-id="">Rabobank New Zealand</option>
                                                    <option value="National Bank of New Zealand" data-id="">National Bank of New Zealand</option>
                                                    <option value="National Australia Bank" data-id="08">National Australia Bank</option>
                                                    <option value="Industrial and Commercial Bank of China" data-id="10">Industrial and Commercial Bank of China</option>
                                                    <option value="PostBank" data-id="11">PostBank</option>
                                                    <option value="Trust Bank Southland" data-id="13">Trust Bank Southland</option>
                                                    <option value="Trust Bank Otago" data-id="14">Trust Bank Otago</option>
                                                    <option value="Trust Bank Canterbury" data-id="16">Trust Bank Canterbury</option>
                                                    <option value="Trust Bank Waikato" data-id="17">Trust Bank Waikato</option>
                                                    <option value="Trust Bank Bay of Plenty" data-id="18">Trust Bank Bay of Plenty</option>
                                                    <option value="Trust Bank South Canterbury" data-id="19">Trust Bank South Canterbury</option>
                                                    <option value="Trust Bank Auckland" data-id="21">Trust Bank Auckland</option>
                                                    <option value="Trust Bank Central" data-id="20">Trust Bank Central</option>
                                                    <option value="Trust Bank Wanganui" data-id="22">Trust Bank Wanganui</option>
                                                    <option value="Westland Bank" data-id="24">Westland Bank</option>
                                                    <option value="Trust Bank Wellington" data-id="23">Trust Bank Wellington</option>
                                                    <option value="Countrywide" data-id="25">Countrywide</option>
                                                    <option value="United Bank" data-id="29">United Bank</option>
                                                    <option value="HSBC" data-id="30">HSBC</option>
                                                    <option value="Citibank" data-id="31">Citibank</option>
                                                    <option value="Other" class="otherBank">Other</option>
                                                </select>
                                                <span class="error" id="error_bank_name"></span> 

                                            </div>
                                            <div class="col-sm-6 toggal_other">
                                                <label class="label_input" style="text-align:left">
                                                    Other Bank name
                                                </label>
                                            </div>
                                            <div class="col-sm-6 toggal_other">
                                                <input type="text" class="Other_Bank_name form-control input-field" name="other_bank_name" id="other_other_name" value="" required="required" placeholder="Enter your Bank name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' />
                                                <span class="error" id="error_other_bank_name"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Account name
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control input-field" id="nameOfAccount" name="nameOfAccount" value="${company.nameOfAccount}" required="required" placeholder="Enter account name"  />
                                                <span class="error" id="spannameOfAccount"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Account number
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <input type="tel" class="form-control input-field" id="accountNumber" name="accountNumber" value="${company.accountNumber}" required="required" placeholder="xx-xxxx-xxxxxxx-xxx" onkeypress="checkAccountNO()" />
                                                <span class="error" id="spanaccountNumber"></span>
                                            </div>
                                            <!-- <div class="col-sm-129">
                                                    <input type="file" name="myFile" class="attach-btn2">
                                            </div> -->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="new-attach-info">
                                                    <div class="col-sm-129 closestcls">
                                                        <input type="file" name="myFile" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" class="attach-btn2 checkname" id="attachbankfile">
                                                        <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile"><i class="far fa-times-circle"></i></a>
                                                        <span class=" error error_attachbankfile"></span>
                                                    </div>
                                                    <!--<span class="info-icon"><a href="#" data-toggle="tooltip" title="To confirm your account details, attach one of the following (must show account number and name and be less than 3 months old): Copy of bank or credit card statement, copy of electronic statement, screenshot from online banking"><img src="./resources/images/i-icon.png"></a></span>-->
                                                    <div class="pulsating-circle6 pulsating-around6" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                                        <span class="tooltiptext">To confirm your account details, attach one of the following (must show account number and name and be less than 3 months old): Copy of bank or credit card statement, copy of electronic statement, screenshot from online banking.</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous13 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next14 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step15">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please read and accept the below Terms and Conditions to continue: 
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="input-content-text">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h4 class="text-center">Agreement of Terms</h4>
                                                    <div class="terms_cond" style="overflow:auto;height: 200px;word-break: break-word;">
                                                        <ol>
                                                            <li> <b>APPLICATION OF THESE TERMS AND CONDITIONS</b>
                                                                <ol>
                                                                    <li>If you create a User Profile and choose to access services via our Platform you accept and agree that you will be bound by these Terms and Conditions. Capitalised words are defined in clause 22.</li>
                                                                    <li>The risk of loss in trading or holding Digital Currency can be substantial. You should therefore carefully consider whether trading or holding Digital Currency is suitable for you in light of your financial condition, tolerance to risk and trading experience.
                                                                    </li>
                                                                </ol>
                                                            </li>
                                                            <li> <b>SERVICES PROVIDED</b>
                                                                <ol>
                                                                    <li>Through the Platform you will have access to the following services ("Crypto Traded Portfolio Services") in relation to digital currencies (such as, but not limited to, Bitcoin and Ethereum) selected by us from time to time (?Digital Currency?):
                                                                        <ol>
                                                                            <li>Model crypto currency portfolios on our Platform providing a range of crypto currency investment options and strategies;</li>
                                                                            <li>High Frequency Trading (HFT) Algorithm based portfolios; </li>
                                                                            <li>General information, including performance data, on the various portfolio options and any portfolios selected by you; </li>
                                                                            <li>General information and education about crypto-currencies and related topics, including investment strategies; </li>
                                                                            <li>Assistance with managing your Portfolio, including executing lump-sum investment Transactions and regular monthly investment Transactions on your behalf, and the ability to track and transfer your supported Digital Currencies;</li>
                                                                            <li>Portfolio re-balancing to reflect the target asset allocation in the model portfolio selected and accepted by you;</li>
                                                                            <li>Foreign and digital currency transactions as required to effect the purchase of selected Portfolios.</li>
                                                                            <li>Digital currency wallets that hold the underlying digital currency assets of the various portfolios on offer, like Bitcoin or Ethereum ("Digital Currency").</li>
                                                                            <li>Chat and user support services.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>We do not deal in digital currencies or tokens which are or may be Financial Products under New Zealand law on the
                                                                        Platform. For this reason, we are not required to hold any licence or authorization in relation to portfolio management of
                                                                        the Digital Currency we do support on the Platform. If we become aware that any Digital Currency we have supported on
                                                                        the Platform is or may be (in our sole discretion) a Financial Product, you authorize and instruct us to take immediate steps
                                                                        to divest any such holdings at the price applying at the time of disposal.</li>
                                                                </ol>
                                                            </li>
                                                            <li> <b>USER PROFILE</b>
                                                                <ol>
                                                                    <li>In order to access the Crypto Traded Portfolio Services via our Platform you need to create a User Profile and meet the following requirements:
                                                                        <ol>
                                                                            <li>be 18 years of age or older;</li>
                                                                            <li>reside in a country that meets our eligibility requirements and is not on our excluded list; </li>
                                                                            <li>complete our verification processes in relation to your identity and personal information to our satisfaction; and</li>
                                                                            <li>meet any other requirements or provide information notified by us to you from time to time.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>To create a User Profile you must submit personal information, details and copies of documents via the Platform or via another process (such as email) as requested. We will rely on that information in order to provide Services to you. You agree that all information you submit to us is complete, accurate and not misleading. If any of the information provided by you changes, you must notify us immediately.</li>
                                                                    <li>You agree to take personal responsibility for any actions in respect of instructions you give us through your User Profile.</li>
                                                                    <li>The User Profile may be used only for the provision of  Services on your behalf.</li>
                                                                    <li>Only you may operate the User Profile. You agree to keep any passwords, codes or other security information safe and secure and to not provide that information to any other person. You accept that we are not responsible for any loss or damage you suffer as a result of a person accessing your User Profile who you have not authorized to do so. </li>
                                                                    <li>You must report any suspected loss, theft or misuse of your User Profile to us immediately. </li>
                                                                    <li>You agree to give us clear, consistent and properly authorized instructions via the Platform.</li>
                                                                </ol>
                                                            </li><li><b>INSTRUCTIONS TO EXECUTE A TRANSACTION</b>
                                                                <ol>
                                                                    <li>You may make lump sum investments or regular monthly investments via our Platform.</li>
                                                                    <li>All money paid by you into your Account will be allocated to and invested in accordance with your chosen Portfolio where possible (subject to clause 4.3 below) until such time you instruct us to withdraw your investments.</li>
                                                                    <li>For lump sum investments, you may give instructionstoexecuteaTransactionviaour Platform usingyour User Profile.</li>
                                                                    <li>For regular monthly investments, you may select a monthly contribution amount when you create your User Profile.
                                                                        This amount may be amended from time to time via our Platform.</li>
                                                                    <li>You agree that you authorize us to submit Transactions on your behalf to reflect the target allocation and strategy
                                                                        for your selected Portfolio at the time we receive the allocated funds. You acknowledge that we will buy the
                                                                        maximum number of units (including fractions of units, if available) of the relevant investments in accordance with
                                                                        your selected Portfolio. Any remaining money after a Transaction has been executed will be held in your Wallet. You
                                                                        acknowledge that your Portfolio may not exactly reflect the target asset allocation set out in your model portfolio.
                                                                        You appoint us, and we accept the appointment, to act as your agent for the execution of any Transaction, in
                                                                        accordance with these Terms and Conditions.</li>
                                                                    <li>We will use reasonable endeavors to execute Transactions within 24 hours of receiving allocated funds into your
                                                                        Wallet (subject to these Terms and Conditions, including clause 4.10). The actual price of a particular investment is
                                                                        set by the issuer, provider or the market (as applicable) at the time the order is executed by us. We may set a
                                                                        minimum transaction amount at any time. We may choose not to process any orders below the minimum transaction
                                                                        amount at our discretion. </li>
                                                                    <li>We are under no obligation to verify the authenticity of any instruction or purported instruction and may act on any instruction given using your User Profile without further enquiry.</li>
                                                                    <li>Any Transaction order placed by you forms a commitment, which once you submit, cannot subsequently be amended or revoked by you. However, our acceptance of Transactions via the Platform and our execution of those Transactions is at our sole discretion and subject to change at any time without notice.</li>
                                                                    <li>After a Transaction has been executed, we reserve the right to void any Transaction which we consider contains any
                                                                        manifest error or is contrary to any law or these Terms and Conditions. In the absence of our fraud or willful default,
                                                                        we will not be liable to you for any loss, cost, claim, demand or expense following any such action on our part or
                                                                        otherwise in relation to the manifest error or Transaction.  </li>
                                                                    <li>We are not responsible for any delay in the settlement of a Transaction resulting from circumstances beyond our control, or the failure of any other person or party (including you) to perform all necessary steps to enable completion of the transaction.</li>
                                                                    <li>Any Transaction via the Platform is deemed to take place in New Zealand and you are deemed to take possession
                                                                        of your Portfolio in New Zealand
                                                                    </li>
                                                                </ol>
                                                            </li>
                                                            <li><b>PORTFOLIO REBALANCING</b>
                                                                <ol>
                                                                    <li>All Portfolios are monitored and are rebalanced in line with the Portfolio?s mandate, and at the discretion of the Portfolio Manager. Subject to these Terms and Conditions, and by continuing to hold the Portfolio, you instruct us to automatically rebalance your portfolio back to the target asset allocation based on your selected portfolio. </li>
                                                                    <li>We rebalance by buying and/or selling investments and any other assets within the portfolio, including by using any available cash balances, so that your Portfolio reflects the target asset allocation of your chosen model portfolio.  </li>
                                                                </ol>
                                                            </li>
                                                            <li><b>TARGET ASSET ALLOCATION</b>
                                                                <ol>
                                                                    <li>Each Portfolio will have a Target Asset Allocation, which is set by the Portfolio Manager. The Portfolio Manager may choose to alter the Target Asset Allocation from time to time and at their sole discretion, in line with the mandate and strategy of the portfolio.</li>
                                                                    <li>We will notify you of any changes to the Target Asset Allocation promptly following these changes taking effect.
                                                                        Notification of any such changes to the Target Asset Allocation are made via the Platform by way of an update to
                                                                        the Target Asset Allocation details provided for each Portfolio. </li>
                                                                </ol>
                                                            </li>

                                                            <li><b>DIGITAL CURRENCY WALLET</b>
                                                                <ol>
                                                                    <li><b>Digital Currency Transactions:</b> Invsta processes supported Digital Currency according to the instructions received
                                                                        from its users and we do not guarantee the identity of any party to a Transaction. It is your responsibility to verify all
                                                                        transaction information prior to submitting instructions to Invsta. Once submitted to a Digital Currency network, a
                                                                        Digital Currency Transaction will be unconfirmed for a period of time pending sufficient confirmation of the
                                                                        Transaction by the Digital Currency network. A Transaction is not complete while it is in a pending state. Funds
                                                                        associated with Transactions that are in a pending state will be designated accordingly, and will not be included in
                                                                        your Invsta Account balance or be available to conduct Transactions. We may charge network fees (miner fees) to
                                                                        process a Transaction on your behalf. We will calculate the network fee in our sole discretion by reference to the 
                                                                        cost to us. </li>
                                                                    <li><b>Digital Currency Storage &amp; Transmission Delays:</b> Invsta securely stores all Digital Currency private keys in our control
                                                                        in a combination of online and offline storage. As a result, it may be necessary for Invsta to retrieve certain information
                                                                        from offline storage in order to facilitate a Transaction in accordance with your instructions, which may delay the
                                                                        initiation or crediting of a Transaction for 48 hours or more. You accept that a Digital Currency Transaction facilitated
                                                                        by Invsta may be delayed.</li>
                                                                    <li><b>Operation of Digital Currency Protocols.</b> Invsta does not own or control the underlying software protocols which
                                                                        govern the operation of Digital Currencies supported on our platform. By using the Invsta platform, you acknowledge
                                                                        and agree (i) that Invsta is not responsible for operation of the underlying protocols and that Invsta makes no
                                                                        guarantee of their functionality, security, or availability; and (ii) that the underlying protocols may be subject to
                                                                        sudden changes in operating rules (a/k/a ?forks?), and that such forks may materially affect the value, function,
                                                                        and/or essential nature of the Digital Currency you hold in the Invsta platform. In the event of a fork, you agree that
                                                                        Invsta may temporarily suspend Invsta operations (with or without advance notice to you) and that Invsta may, in its
                                                                        sole discretion, decide whether or not to support (or cease supporting) either branch of the forked protocol entirely.
                                                                        You acknowledge and agree that Invsta assumes absolutely no responsibility in respect of an unsupported branch
                                                                        of a forked Digital Currency and/or protocol.</li>
                                                                    <li>Payments of money can be made to your Wallet electronically, including through credit card payments, e-Poli and bank transfers. The acceptable forms of payment will be set out on our Platform and may be subject to change from time to time.</li>
                                                                    <li>You agree that Digital Curreny investments and any other assets held in your Wallet <b>("Client Property")</b> may be
                                                                        pooled with the investments and other assets of other clients and therefore your holdings may not be individually
                                                                        identifiable within the Wallet. </li>
                                                                    <li>You agree money held in your Wallet (including money held pending investment as well as the proceeds and income from selling investments) <b>("Client Money")</b> may be held in pooled accounts, which means your money may be held in the same accounts as that of other clients using the Platform.</li>
                                                                    <li>Money received from you or a third party for your benefit, which includes your Client Money held pending
                                                                        investment, payment to you as the proceeds and income from selling investments, may attract interest from the
                                                                        bank at which it is deposited. You consent to such interest being deducted from that bank account and being
                                                                        retained by us as part of the fees for using the Services.
                                                                    </li>
                                                                    <li>We will keep detailed records of all your Client Property and Client Money in your Wallet at all times. </li>
                                                                    <li>Client Property and Client Money received or purchased by us on your behalf will be held on bare trust as your
                                                                        nominee in the name or to the order of Invsta or any other approved third party custodian to our order. </li>
                                                                    <li>You confirm that you are the beneficial owner of all of the Client Property and Client Money in your account, or you
                                                                        are the sole trustee on behalf of the beneficial owner, and that they are and will remain free from any encumbrance.</li>
                                                                    <li>Withdrawals from the Service or of any Client Money can be made via the Platform by initiating a ?Withdrawal?.
                                                                        Withdrawals will be processed on a best endeavors basis as outlined in clause 4.6. Withdrawals of Client Money will
                                                                        be processed and paid into a bank account nominated by you in the same name of your User Profile upon
                                                                        settlement, the timing of which depends on the market. We may set a maximum daily amount that can be withdrawn
                                                                        from your Wallet. </li>
                                                                    <li>It is up to you to understand whether and to what extent, any taxes apply to any Transactions through the Services
                                                                        or the Platform, or in relation to your Portfolio. We accept no responsibility for, nor make any representation in respect
                                                                        of, your tax liability.
                                                                    </li>
                                                                </ol>
                                                            </li>

                                                            <li><b>OUR WEBSITE POLICY</b>
                                                                <ol>
                                                                    <li>You agree to receive any and all advice, documents, information, or other communications from Invsta electronically through the Platform, by email, or otherwise over the internet,</li>
                                                                    <li>You agree to receive any statements, confirmations, prospectuses, disclosures, tax reports, notices, documents, information, amendments to the agreements, or other communications transmitted to you from time to time electronically.</li>
                                                                    <li>You agree that we may use the email address provided in your application for a User Profile or such other email address as you notify to us from time to time to provide such information to you. Any electronic communication will be deemed to have been received by you when it is transmitted by us.</li>
                                                                    <li>Access to our Platform is at our absolute discretion. You acknowledge that access to our Platform may be interrupted and the Services may be unavailable in certain circumstances.</li>
                                                                </ol>
                                                            </li>

                                                            <li><b>RISK WARNINGS</b>
                                                                <ol>
                                                                    <li>You acknowledge and agree that:
                                                                        <ol>
                                                                            <li>investing in Digital Currency is unlike investing in traditional currencies, goods or commodities: it involves
                                                                                significant and exceptional risks and the losses can be substantial. You should carefully consider and assess
                                                                                whether trading or holding of digital currency is suitable for you depending upon your financial condition,
                                                                                tolerance to risk and trading experience and consider whether you should take independent financial
                                                                                advice; </li>
                                                                            <li>we have not advised you to, or recommended that you should, use the Platform and/or Services, or trade
                                                                                and/or hold Digital Currency;</li>
                                                                            <li>unlike other traditional forms of currency, Digital Currency is decentralised and is not backed by a central
                                                                                bank, government or legal entities. As such, the value of Digital Currency can be extremely volatile and may
                                                                                swing depending upon the market, confidence of investors, competing currencies, regulatory
                                                                                announcements or changes, technical problems or any other factors. We give no warranties or
                                                                                representations as to the future value of any Digital Currency and accept no liability for any change in value
                                                                                of your Portfolio; and
                                                                            </li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You acknowledge that we do not issue or deal in Financial Products or provide any financial advice and no offer or
                                                                        other disclosure document has been, or will be, prepared in relation to the Services, the Platform and/or any of the
                                                                        Digital Currencies, under the Financial Markets Conduct Act 2013, the Financial Advisers Act 2008 or any other similar
                                                                        legislation in any jurisdiction.</li>
                                                                </ol>
                                                            </li>

                                                            <li><b>DISPUTE RESOLUTION</b>
                                                                <ol>
                                                                    <li>You will promptly inform us via the Platform and/or by email of any complaint you have regarding the standard of service we provide to you. We will promptly respond to any complaint we receive from you.</li>
                                                                    <li>If you are unsatisfied with our response, you may direct any complaints to:<br>
                                                                        <b>Financial Services Complaints Limited (FSCL)</b><br>
                                                                        PO Box 5967, Lambton Quay<br>
                                                                        Wellington, 6145<br> 
                                                                        Email: info@fscl.org.nz<br>
                                                                        FSCL is our independent external dispute resolution scheme that has been approved by the Minister of Consumer Affairs under the Financial Service Providers (Registration and Dispute Resolution) Act 2008. This service costs you nothing.
                                                                    </li>
                                                                </ol>
                                                            </li>

                                                            <li><b>USE AND DISCLOSURE OF INFORMATION</b>
                                                                <ol>
                                                                    <li>You authorize us to:
                                                                        <ol>
                                                                            <li>Collect, hold and disclose personal information about you for the purpose of providing Services to you, creating your User Profile and for our own marketing purposes;</li>
                                                                            <li>Aggregate and anonymize your data along with that of other users, and use that data ourselves or sell or supply that anonymized data to other financial service providers for marketing, product design and other commercial purposes;</li>
                                                                            <li>Keep records of all information and instructions submitted by you via the Platform or by email;</li>
                                                                            <li>Record all telephone conversations with you;</li>
                                                                            <li>Record and identify the calling telephone from which you instruct us;</li>
                                                                            <li>Record and retain copies of all information and documents for the purposes of the various Financial Market Regulations under which we may operate;</li>
                                                                            <li>Obtain credit information concerning you if we consider it relevant to determine whether to agree to perform Services or administer your User Profile.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You agree to give us any information we ask you for if we (or any affiliates or third parties with whom you are dealing with through us) believe we need it in order to comply with any laws in New Zealand or overseas. You agree that we can use information that we have about you to:
                                                                        <ol>
                                                                            <li>Assess whether we will provide you with a User Profile;</li>
                                                                            <li>Provide you with, or manage any of, our Services;</li>
                                                                            <li>Comply with any laws in New Zealand or overseas applying to us or the Services we provide to you; or</li>
                                                                            <li>Compare with publicly available information about you or information held by other reputable companies or organizations we have a continuing relationship with, for any of the above reasons.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You agree that we can obtain information about you from or give your information to any of the following people or organizations:
                                                                        <ol>
                                                                            <li>Our agents or third parties (whether in New Zealand or overseas) that provide services to, through or via us such as execution, data hosting (including cloud-based storage providers) and processing, tax services, anti-money laundering services or support services; or</li>
                                                                            <li>A regulator or exchange for the purposes of carrying out its statutory functions.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You agree that where required to help us comply with laws in New Zealand or overseas or if we believe giving the information will help prevent fraud, money laundering or other crimes, we may give information we hold about you to others including:
                                                                        <ol>
                                                                            <li>Police or government agencies in New Zealand and overseas; or</li>
                                                                            <li>The issuers of Financial Products in order for them to satisfy their obligations under New Zealand anti-money laundering laws and regulations.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>We may not be allowed to tell you if we do give out information about you. We are not responsible to you or anyone else if we give information for the purposes above. We will not disclose information about you except as authorized by you or as required or authorized by law.</li>
                                                                </ol>
                                                            </li>
                                                            <li><b>USE AND DISCLOSURE OF INFORMATION</b>
                                                                <ol>
                                                                    <li>You have rights of access to, and correction of, personal information supplied to and held by us.</li>
                                                                </ol>
                                                            </li>
                                                            <li><b>ANTI-MONEY LAUNDERING</b>
                                                                <ol>
                                                                    <li>We may need to identify you in order to comply with laws in New Zealand and overseas.</li>
                                                                    <li>We are required to comply with all applicable New Zealand or overseas anti-money laundering laws and regulations and may ask for information identifying you, and then verification for such identity, including references and written evidence. We may ask you for details of the source or destination of your funds.</li>
                                                                    <li>You agree to complete our identification and verification processes in relation to your identity and personal information to our satisfaction. We reserve the right to refuse to provide you Services or to cancel your User Profile if this information is not provided on request.</li>
                                                                    <li>
                                                                        You accept that there may be circumstances where we are required to suspend or disable your User Profile to meet
                                                                        our anti-money laundering obligations.
                                                                    </li>
                                                                    <li>You agree that we may use personal information provided by you for the purpose of electronic identity verification using third party contractors and databases including the Department of Internal Affairs, NZ Transport Agency, Companies Office, electronic role, a credit reporting agency or other entity for that purpose.</li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>LIMITATION OF LIABILITY</b>
                                                                <ol>
                                                                    <li>You agree that where our Services are acquired for business purposes, or where you hold yourself out as acquiring our Services for business purposes, the Consumer Guarantees Act 1993 (?the CGA?) will not apply to any supply of products or services by us to you. Nothing in these Terms and Conditions will limit or abrogate your rights and remedies under the CGA except to the extent that contracting out is permitted under the CGA and all provisions of these Terms and Conditions will be modified to the extent necessary to give effect to that intention.</li>
                                                                    <li>Subject to any terms implied by law which cannot be excluded and in the absence of our fraud or willful default, we will not be liable in contract, tort (including negligence), equity, or otherwise for any direct, indirect, incidental, consequential, special or punitive damage, or for any loss of profit, income or savings, or any costs or expenses incurred or suffered by you or any other person in respect of Services supplied to you or in connection with your use of our Platform.</li>
                                                                    <li>You acknowledge that:
                                                                        <ol>
                                                                            <li>Our advice may be based on information provided to us by you or by third parties which may not have been independently verified by us (?Information from Third Parties?);</li>
                                                                            <li>We are entitled to rely on Information from Third Parties and we are under no obligation to verify or investigate that information. We will not be liable under any circumstances where we rely on Information from Third Parties;</li>
                                                                            <li>Our Services do not include tax advice. We recommend that you consult your tax adviser before making a decision to invest or trade in Financial Products;</li>
                                                                            <li>Without limiting any obligations we have under the various Financial Market Regulations, it is your responsibility to:
                                                                                <ol>
                                                                                    <li>Satisfy yourself that our Crypto Traded Portfolios are appropriate to your circumstances; and</li>
                                                                                    <li>Make further enquiries as should reasonably be made by you before making a decision to invest in Crypto Currency Portfolios.</li>
                                                                                </ol>
                                                                            </li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>We will be under no liability for any loss or expense which arises as a result of a delay by us in executing a Transaction via the Platform (due to a network outage, system failure or otherwise or for any reason whatsoever).</li>
                                                                    <li>We will not be liable for any failure to provide products or services to you or to perform our obligations to you under these Terms and Conditions if such failure is caused by any event of force majeure beyond our reasonable control, or the reasonable control of our employees, agents or contractors. For the purposes of this clause, an event of force majeure includes (but is not limited to) a network outage, an inability to communicate with other financial providers, brokers, financial intermediaries, a failure of any computer dealing or settlement system, an inability to obtain the necessary supplies for the proper conduct of business, and the actions or failures of any counterparty or any other broker or agent, or the systems of that broker or agent.</li>
                                                                    <li>The provisions of this clause 14 will extend to all our employees, agents and contractors, and to all corporate entities
                                                                        in which we may have an interest and to all entities which may distribute our publications.</li>
                                                                    <li>Despite anything else in these Terms and Conditions, if we are found to be liable for any loss, cost, damage or
                                                                        expense arising out of or in connection with your use of the Platform or the Services or these Terms and Conditions,
                                                                        our maximum aggregate liability to you will be limited to two times the total amount of fees and charges that you
                                                                        have paid to us in the previous twelve months in accordance with clause 15 below.</li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>FEES AND CHARGES FOR SERVICES</b>
                                                                <ol>
                                                                    <li>We may charge an annual fee for accessing the Platform and associated services. Currently no such fee is charged. </li>
                                                                    <li>Each Portfolio will charge various fees for investing into that portfolio. All portfolio fees are automatically deducted from the assets held within the Portfolio each month, these will not be charged to you directly. Each portfolio may charge, and must pay to us, on demand, the following fees and charges ("Fees"):
                                                                        <ol>
                                                                            <li>Crypto-Currency Portfolio Services:</li>
                                                                            <table class="table-bordered">
                                                                                <tbody><tr>
                                                                                        <td>Portfolio Management Fee</td>
                                                                                        <td>Description</td>
                                                                                        <td>Other Expenses</td>
                                                                                        <td>Performance Fee</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Up to 3% per annum of all funds invested (plus GST if any). Fees may be changed from time to time at our absolute discretion.</td>
                                                                                        <td>Based on total investment value, charged monthly</td>
                                                                                        <td>In operating the Portfolios and offering this service to you, we may incur other expenses such as transaction fees, bank charges, audit and legal fees. Such expenses will be an addition fees these are charged to the Portfolios. </td>
                                                                                        <td>We may change a performance fee on some Portfolios, at a rate of up to 30% of outperformance against given hurdles. </td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You must also pay for any other fees and charges for any add on services you obtain via the Platform or as specified
                                                                        in these Terms and Conditions.</li>
                                                                    <li>All Fees are automatically debited monthly in arrears from each Portfolio. You agree that we have the absolute right of sale of investments in each Portfolio to meet all amounts due to us.</li>
                                                                </ol>
                                                            </li>

                                                            <li>
                                                                <b>TERMINATION OF YOUR USER PROFILE</b>
                                                                <ol>
                                                                    <li>Either you or we may cancel your User Profile at any time. If we cancel your User Profile we will notify you by email.   If you wish to cancel your User Profile you may do so using the facility available on the Platform.</li>
                                                                    <li>Examples of when we will cancel your User Profile include (but are not limited to) where:
                                                                        <ol>
                                                                            <li>you are insolvent or in liquidation or bankruptcy; or</li>
                                                                            <li>you have not paid Fees or other amounts due under these Terms and Conditions by the due date</li>
                                                                            <li>you gain or attempt to gain unauthorised access to the Platform or another member?s User Profile or Wallet;</li>
                                                                            <li>we consider any conduct by you (whether or not that conduct is related to the Platform or the Services) puts
                                                                                the Platform, the Services or other users at risk; </li>
                                                                            <li>you use or attempt to use the Platform in order to perform illegal or criminal activities;</li>
                                                                            <li>your use of the Platform is subject to any pending investigation, litigation or government proceeding;
                                                                            </li>
                                                                            <li>you fail to pay or fraudulently pay for any transactions;</li>
                                                                            <li>you breach these Terms and Conditions and, where capable of remedy, fail to remedy such breach within 30
                                                                                days? written notice from us specifying the breach and requiring it to be remedied;</li>
                                                                            <li>your conduct may, in our reasonable opinion, bring us into disrepute or adversely affect our reputation or
                                                                                image; and/or</li>
                                                                            <li>we receive a valid request from a law enforcement or government agency to do so.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>We may also terminate these Terms and cease to provide the Services and the Platform if we undergo an insolvency
                                                                        event, meaning that where that party becomes unable to pay its debts as they fall due, or a statutory demand is
                                                                        served, a liquidator, receiver or manager (or any similar person) is appointed, or any insolvency procedure under
                                                                        the Companies Act 1993 is instituted or occurs. </li>
                                                                    <li>If either you or we terminate your User Profile you will still be responsible for any Transaction made up to the time of termination, and Fees for Services rendered to you and our rights under these Terms and Conditions in respect of those matters will continue to apply accordingly.</li>
                                                                    <li>You agree that we will not be liable for any loss you suffer where we act in accordance with this clause.</li>
                                                                    <li>On termination of your User Profile, we will redeem all of the investments held in your Portfolio and transfer the
                                                                        proceeds of sale (less any applicable Fees) to a bank account nominated by you. </li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>ASSIGNMENT</b>
                                                                <ol>
                                                                    <li>You agree that these Terms and Conditions bind you personally and you may not assign any of your rights or obligations under it. Any such purported assignment will be ineffective.</li>
                                                                    <li>We may assign all or any of our rights, and transfer all or any of our obligations under these Terms and Conditions to any person, including a purchaser of the Platform or all or substantially all of our business.</li>
                                                                </ol>
                                                            </li>
                                                            <li><b>INDEMNITY</b>
                                                                <ol>
                                                                    <li>You must, on demand being made by us and our partners, affiliated persons, officers and employees, indemnify those persons against any and all losses, costs, claims, damages, penalties, fines, expenses and liabilities: 
                                                                        <ol>
                                                                            <li>in the performance of their duties or exercise of their authorities, except to the extent arising as a result of their own negligence, fraud or willful default; and</li>
                                                                            <li>which they may incur or suffer as a result of:
                                                                                <ol>
                                                                                    <li>relying in good faith on, and implementing instructions given by any person using your User Profile, unless there are reasonable grounds for us to doubt the identity or authority of that person; and</li>
                                                                                    <li>relying in good faith on information you have either provided to us or made available to us.</li>
                                                                                </ol>
                                                                            </li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>If any person who is not you (except for the Financial Markets Authority or any other regulatory authority of competent jurisdiction) makes any claim, or brings any proceedings in any Court, against us in connection with Services we provide to you, you will indemnify us for and against, and pay to us on demand, all legal costs and other expenses that we incur in connection with that claim or proceeding.</li>
                                                                    <li>You must also indemnify us and our partners, affiliated persons, officers and their respective employees, agents and contractors in the case of any portfolio investment entity tax liability required to be deducted (at the Prescribed Investor Rate nominated by you or us) from your investment, even if that liability exceeds the value of their investments, or any incorrect notification or failure to notify or update annually your PIR or tax rates.</li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>AMENDMENTS</b>
                                                                <ol>
                                                                    <li>We may, at our sole discretion, amend these Terms and Conditions (including our Fees) by giving ten working days' prior notice to you either by:
                                                                        <ol>
                                                                            <li>Notice on our website; or</li>
                                                                            <li>Direct communication with you via email,</li>
                                                                        </ol>
                                                                        unless the change is immaterial (e.g. drafting and typographical amendments) or we are required to make the change sooner (e.g. for regulatory reasons), in which case the changes will be made immediately.
                                                                    </li>
                                                                    <li>You may request a copy of our latest Terms and Conditions by contacting us via the Platform or email.</li>
                                                                    <li>If you access the Platform or otherwise use our Services after the expiry of the notice given in accordance with clause 17.1 you will be deemed to have accepted the amended Terms and Conditions.</li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>NOTICES</b>
                                                                <ol>
                                                                    <li>Any notice or other communication ("Notice") given for the purposes of these Terms and Conditions:
                                                                        <ol>
                                                                            <li>Must be in writing; and</li>
                                                                            <li>Must be sent to the relevant party?s email address.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>Any notice is deemed served or received on the day it is sent to the correct email address.</li>
                                                                    <li>Any notice that is served on a Saturday, Sunday or public holiday is deemed to be served on the first working day after that.</li>
                                                                    <li>A notice may be given by an authorized officer, employee or agent.
                                                                        <ol>
                                                                            <li>Notice may be given personally to a director, employee or agent of the party at the party?s address or to a person who appears to be in charge at the time of delivery or according to section 387 to section 390 of the Companies Act 1993.</li>
                                                                            <li>If the party is a natural person, partnership or association, the notice may be given to that person or any partner or responsible person. If they refuse to accept the notice, it may be brought to their attention and left in a place accessible to them.</li>
                                                                        </ol>
                                                                    </li>
                                                                </ol>                
                                                            </li>

                                                            <li><b>GOVERNING LAW AND JURISDICTION</b>
                                                                <ol>
                                                                    <li>These Terms and Conditions are governed by and construed according to the current laws of New Zealand. The parties agree to submit to the non-exclusive jurisdiction of the Courts of New Zealand.</li>
                                                                    <li>If you bring any claim or proceeding against us in any Court which is not a Court of New Zealand, you will indemnify us for and against, and pay to us on demand, all legal costs and other expenses that we incur in connection with that claim or proceeding.</li>
                                                                </ol>
                                                            </li>

                                                            <li><b>DEFINITIONS</b><br>
                                                                <b>"Account"</b> means the Client Property and Client Money we hold for you and represents an entry in your name on the
                                                                general ledger of ownership of Digital Currency and money maintained and held by<br> 

                                                                <!-- <b>"Authorized Financial Adviser"</b> has the same meaning as in section 51 of the Financial Advisers Act.</br> -->

                                                                <b>"Crypto Traded Portfolio Services"</b> means the services described in clause 2.1.<br>

                                                                <b>"Client"</b> means the person in whose name a User Profile has been opened.<br>
                                                                <b>"Client Money"</b> has the meaning given to it in clause 7.6.<br>
                                                                <b>"Client Property"</b> has the meaning given to it in clause 7.5.<br>
                                                                <b>"Digital Currency"</b> means the supported tokens or cryptocurrencies offered on the Platform.<br>

                                                                <b>"Fees"</b> means any fees or other charges charged for the Services, including, but not limited to the fees set out in clause 15.<br> 

                                                                <!-- <b>"Financial Advisers Act"</b> means the Financial Advisers Act 2008.</br> -->

                                                                <!-- <b>"Financial Adviser Service"</b> has the same meaning as in section 9 of the Financial Advisers Act.</br>  -->
                                                                <!-- <b>"Financial Product"</b> has the same meaning as in section 7 of the Financial Markets Conduct Act 2013.</br>  -->
                                                                <b>"Minor"</b> means a person under the age of 18.<br>
                                                                <b>"Platform"</b> means the Invsta Investment Platform. (www.invsta.com) and any associated variations of this website <br>

                                                                <b>"Portfolio"</b> means a portfolio of assets that is managed by Invsta via the Platform. <br>
                                                                <b>"Portfolio Manager"</b> means Invsta and associated people responsible for managing your Portfolio.<br>

                                                                <b>"Services"</b> means a Service we provide to you via our Platform including the Crypto Traded Portfolio Services <br>

                                                                <b>"Terms and Conditions"</b> means these Terms and Conditions.<br>

                                                                <b>"Transaction"</b> means a transaction effected or to be effected using the Platform pursuant to your instructions.<br>

                                                                <b>"User Profile"</b> means a User Profile in your name created by you in accordance with these Terms and Conditions through which you are entitled to gain access to our Platform.<br>

                                                            </li>

                                                            <li><b>GENERAL INTERPRETATION</b>
                                                                <ol>
                                                                    <li>In these Terms and Conditions:
                                                                        <ol>
                                                                            <li>Unless the context otherwise requires, references to:
                                                                                <ol>
                                                                                    <li>"we", "us", Invsta, and ?Ilumony? refer to Ilumony Limited, trading as Invsta, and related companies (as defined in section 2(3) of the Companies Act 1993); and</li>
                                                                                    <li>?you?, ?your? and ?yourself? are references to the Client and where appropriate any person who you have advised us are authorized to act on your behalf.</li>
                                                                                </ol>
                                                                            </li>
                                                                            <li>A reference to these Terms and Conditions (including these Terms and Conditions) includes a reference to that agreement as novated, altered or replaced from time to time;</li>
                                                                            <li>A reference to a party includes the party?s administrators, successors and permitted assigns;</li>
                                                                            <li>Words in the plural include the singular and vice versa;</li>
                                                                            <li>Headings are inserted for convenience only and will be ignored in construing these Terms and Conditions;</li>
                                                                            <li>References to any legislation includes statutory regulations, rules, orders or instruments made pursuant to that legislation and any amendments, re-enactments, or replacements; and</li>
                                                                            <li>Expressions referring to writing will be construed as including references to words printed, typewritten, or by email or otherwise traced, copied or reproduced.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>These Terms and Conditions are intended to benefit and be enforceable by Invsta Limited and any related companies (as defined in section 2(3) of the Companies Act 1993) in accordance with the Contracts (Privity) Act 1982.</li>
                                                                </ol>
                                                            </li>

                                                        </ol>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top:20px">
                                                <div class="col-sm-12" style="text-align: center">
                                                    <a href="./resources/images/pdf/Crypto-T&C.pdf" style=" color: #011c53;  font-size: 14px; font-weight: bold; padding: 10px;
                                                       text-decoration: underline;" target="black"> Download Client Terms and Conditions</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 condition-details">
                                                    <input class="checkbox-check ch1"  type="checkbox" name="term_condition">
                                                    <label class="label_input" style="text-align:left">
                                                        I have read and agree to the Terms and Conditions
                                                    </label>
                                                </div>
                                                <span class="error" id="error_term_condition" ></span>
                                                <div class="col-sm-12 condition-details">
                                                    <input class="checkbox-check ch2 " type="checkbox" name="authorized_condition">
                                                    <label class="label_input" style="text-align:left">
                                                        I am authorised to accept and act on behalf of all account holders
                                                    </label>
                                                </div>
                                                <span class="error" id="error_authorized_condition" ></span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous14 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next15 action-button" value="Accept" />
                            </fieldset>
                            <fieldset id="step16">
                                <div class="content-section">
                                    <div class="row">
                                        <div class="col-md-12 thanku">
                                            <div class="element-wrapper">
                                                <h5 class="element-header">
                                                    Thank for submitting your application details, we'll let you know if we require anything further.
                                                </h5>
                                            </div>
                                            <h5 class="element-header3">
                                                To continue to your account and make an investment, please verify your email address by clicking on the link in the email we have sent.
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>

                                <div class="col-md-12" style="max-width: 113px;margin: auto;">
                                    <div class="ok-submit">
                                        <input type="button" name="previous" id="submit" class=" action-button-previous new-ok-btn " value="OK" />
                                    </div>
                                </div>
                                <!-- <input type="button" name="next" class="next12 action-button" value="Accept" /> -->
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
        <script src='https://code.jquery.com/ui/1.12.1/jquery-ui.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
        <script src="./resources/js/index.js"></script>        
        <script src="./resources/js/intlTelInput_1.js"></script>
        <script src="./resources/js/intlTelInput_2.js"></script>
        <script src="./resources/js/intlTelInput_3.js"></script>
        <script src="./resources/js/user-main.js"></script>
        <script type="text/javascript" src="./resources/js/countries.js"></script>
        <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.js'></script>
        <script src="./resources/js/sweetalert.min.js"></script>

        <script>
                                                            $(function () {
                                                                $('.chosen-select').chosen();
                                                                $('.chosen-select-deselect').chosen({allow_single_deselect: true});
                                                            });
                                                            $(document).ready(function () {
                                                                $(".otherType").hide();
                                                                $(".toggal_other").hide();
                                                                $('.save-new-btn').show();
                                                                $('#cOccupation').hide();
                                                                $('.removefile').hide();
                                                                $('#more-investor-occupation').hide();
                                                                var arrName = [];
                                                                var thisName;
                                                                $(window).keydown(function (event) {
                                                                    if (event.keyCode == 13) {
                                                                        event.preventDefault();
                                                                        return false;
                                                                    }
                                                                });
                                                            });
                                                            function initAutocomplete() {
                                                                autocomplete = new google.maps.places.Autocomplete(
                                                                        (document.getElementById('address')),
                                                                        {types: ['address'], componentRestrictions: {country: 'nz'}});

                                                                autocomplete1 = new google.maps.places.Autocomplete(
                                                                        (document.getElementById('companyAddress')),
                                                                        {types: ['address'], componentRestrictions: {country: 'nz'}});
                                                                autocomplete2 = new google.maps.places.Autocomplete(
                                                                        (document.getElementById('address1')),
                                                                        {types: ['address'], componentRestrictions: {country: 'nz'}});
                                                                autocomplete.addListener('place_changed', fillInAddress);
                                                                autocomplete1.addListener('place_changed', fillInAddress);
                                                                autocomplete2.addListener('place_changed', fillInAddress);

                                                            }
                                                            var date = new Date();
                                                            var day = date.getDate();
                                                            var month = date.getMonth();
                                                            var year = date.getFullYear();
                                                            var adultDOB = date.setFullYear(year - 18, month, day);
//                                                    var adultDOB = '2001-07-22';
//                                                    var adultDOB1 = '2019-07-22';
//                                                    var arr = adultDOB.split("-");
//                                                    var year = parseInt(arr[0]);
//                                                    var month = parseInt(arr[1]);
//                                                    var day = parseInt(arr[2]);
//                                                    var arr1 = adultDOB1.split("-");
//                                                    var year1 = parseInt(arr1[0]);
//                                                    var month1 = parseInt(arr1[1]);
//                                                    var day1 = parseInt(arr1[2]);
                                                            $("#countryCode").intlTelInput_1();
                                                            $("#countryCode2").intlTelInput_1();
                                                            $("#countryCode3").intlTelInput_1();
                                                            $("#countryCode4").intlTelInput_1();
                                                            $("#countryCode5").intlTelInput_1();
                                                            $(".countryname").intlTelInput_2();
                                                            $(".countrynameexcludenz").intlTelInput_3();
                                                            $(".countryCode").intlTelInput_1();
                                                            $('#otp-block').hide();
                                                            $('.inputtype').hide();
                                                            $(document).ready(function () {
                                                                $('.passport-select').hide();
                                                                $('.other-select').hide();
                                                                $('.passport-select1').hide();
                                                                $('.other-select1').hide();
                                                                $('.yes-option').hide();
                                                                $('.yes-option1').hide();
                                                                $('.yes-option4').hide();
                                                                $('.selectno6').hide();
                                                                $('#mobileNo').keyup(function () {
                                                                    $('#error-generateOtp').text('');
                                                                });
                                                                $('#otp').keyup(function () {
                                                                    $('#error-generateOtp').text('');
                                                                });
                                                                $("#dob").datepicker({
                                                                    yearRange: (year - 80) + ':' + year,
                                                                    changeMonth: true,
                                                                    changeYear: true,
                                                                    'maxDate': new Date(adultDOB),
                                                                    dateFormat: 'dd/mm/yy'
                                                                }).datepicker().attr('readonly', 'readonly');
                                                                $(".dob1").datepicker({
                                                                    yearRange: (year) + ':' + (year + 80),
                                                                    changeMonth: true,
                                                                    changeYear: true,
                                                                    'minDate': new Date(),
                                                                    dateFormat: 'dd/mm/yy'
                                                                }).datepicker().attr('readonly', 'readonly');
                                                                $(".dob").datepicker({
                                                                    yearRange: (year - 80) + ':' + year,
                                                                    changeMonth: true,
                                                                    changeYear: true,
                                                                    'maxDate': new Date(adultDOB),
                                                                    dateFormat: 'dd/mm/yy'
                                                                }).datepicker().attr('readonly', 'readonly');
                                                                $(".doc").datepicker({
                                                                    yearRange: (year - 80) + ':' + year,
                                                                    changeMonth: true,
                                                                    changeYear: true,
                                                                    'maxDate': new Date(),
                                                                    dateFormat: 'dd/mm/yy'
                                                                }).datepicker().attr('readonly', 'readonly');
                                                                $('input:radio[name="senderType"]').change(function () {
                                                                    var mNo = $('.mobile_number1').val();
                                                                    var cCo = $('.mobile_country_code').val();
                                                                    var sTy = $('.senderType:checked').val();
                                                                    cCo = cCo.replace(/\+/g, "");
//                                                            genrateotp(mNo, cCo, sTy);

                                                                    $('#otp-block').show();
                                                                    document.getElementById('error-loader').style.display = 'none';
                                                                    document.getElementById('error-generateOtp').innerHTML = "Please check your messages";
                                                                    $('input:radio[name="senderType"]').prop("checked", false);
                                                                });
                                                                $('.otpkey').keyup(function () {
                                                                    $('.spanverification').html("");
                                                                    if ($('#verificationa').val().length === 6) {
                                                                        alert("inside");
                                                                        var mNo = $('.mobile_number1').val();
                                                                        var cCo = $('.mobile_country_code').val();
                                                                        var otp = $('#verificationa').val();
                                                                        cCo = cCo.replace(/\+/g, "");
                                                                        validateOTP(mNo, cCo, otp);
                                                                    }
                                                                });

                                                                $('input[name="term_condition"]:checked').change(function () {
//                                                            alert("hello");
                                                                });
                                                                $('#fourthfs-continue').click(function () {
                                                                    var ele = $(this);
                                                                    moveNextProcess(ele);
                                                                });
                                                            });
                                                            //                                                isEmailAlreadyExist = function () {
                                                            //                                                    var email = $('#email').val().trim();
                                                            //                                                    if (email.length > 0) {
                                                            //                                                        var url = './rest/cryptolabs/api/isEmailAlreadyExist?e=' + email;
                                                            //                                                        $.ajax({
                                                            //                                                            url: url,
                                                            //                                                            type: "GET",
                                                            //                                                            async: false,
                                                            //                                                            success: function (response) {
                                                            //                                                                $("#validationId").css('display', 'inline');
                                                            //                                                                if (response === 'Email is Valid.') {
                                                            //                                                                    $("#validationId").css('color', 'green').html('');
                                                            //                                                                    $("#validationId").focus();
                                                            //                                                                    $('#firstfs-continue').removeAttr("disabled");
                                                            //                                                                    return true;
                                                            //                                                                } else {
                                                            //                                                                    $("#validationId").css('color', '#e90e0e').html(response);
                                                            //                                                                    $("#validationId").focus();
                                                            //                                                                    $('#firstfs-continue').attr("disabled", "disabled");
                                                            //                                                                    return false;
                                                            //                                                                }
                                                            //                                                            },
                                                            //                                                            error: function (e) {
                                                            //                                                                //handle error
                                                            //                                                            }
                                                            //                                                        });
                                                            //                                                    }
                                                            //                                                };

                                                            clearValidationId3 = function () {
                                                                $("#validationId3").html('');
                                                            };
                                                            isInviteCodeUsed = function (ele) {
                                                                var inviteCode = $('#inviteCode').val();
                                                                var url = './rest/cryptolabs/api/isInviteCode?ic=' + inviteCode;
                                                                $.ajax({
                                                                    url: url,
                                                                    type: "GET",
                                                                    async: false,
                                                                    success: function (response) {
                                                                        if (response === 'true') {
                                                                            $("#error-inviteCode").css({'color': 'green', 'margin-left': '10px'}).html('Invite Code is valid.');
                                                                            $("#error-inviteCode").focus();
                                                                            moveNextProcess(ele);
                                                                            return true;
                                                                        } else {
                                                                            $("#error-inviteCode").css({'color': 'red', 'margin-left': '10px'}).html('Invite Code is used or invalid.');
                                                                            $("#error-inviteCode").focus();
                                                                            return false;
                                                                        }
                                                                    },
                                                                    error: function (e) {
                                                                        //handle error
                                                                    }
                                                                });
                                                            };
                                                            $(".selectType").click(function () {
                                                                var trustType = $(".selectType option:selected").text();
                                                                if (trustType === "Other") {
                                                                    $(".selectType").show();
                                                                    $(".otherType").show();
                                                                } else {
                                                                    $(".selectType").show();
                                                                    $(".otherType").hide();
                                                                }
                                                            });


        </script>
        <script>
            $(".toggle-password").click(function () {
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") === "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });
            var moreInvestorArr = [];
            var investors = [];
            var current_investor = {};
            var directors = [];
//            var directorArr = new Array();
            var x = 0, y = 0, idx = 0;
            var checkfirstdir = 0;
            <c:forEach items="${company.getMoreInvestorList()}" var="moreInvestor" varStatus="list">
            current_investor = ${moreInvestor};
            //            moreInvestorArr.push(current_investor);
            investors.push(current_investor);
            //                positionInCompany = ;
            </c:forEach>
            getMoreInvestor = function (idx) {
//                    alert("function" + idx);
                var inv = directors[idx];
//                    alert(JSON.stringify(inv));
                console.log(JSON.stringify(inv));
                var email = $('#more-investor-email').val();
                var dob = $('#more-investor-dob').val();
                var countryname = $('#more-investor-countryname').val();
                var positionInCompany = $('#more-investor-positionInCompany').val();
                var occupation = $('.OccupationOption').val();
                if (occupation === "other") {
                    occupation = $('#more-investor-occupation').val();
                }
                var address = $('.more-investor-address').val();
                var countrycode = $('#more-investor-countrycode').val();
                var mobile = $('#more-investor-mobile').val();
                var idType = $('#src_of_fund2 option:selected').val();
                var licenseNumber = $('#more-investor-licenseNumber').val();
                var licenseExpiryDate = $('#more-investor-licenseExpiryDate').val();
                var versionNumber = $('#more-investor-versionNumber').val();
                var passportNumber = $('#more-investor-passportNumber').val();
                var passportExpiryDate = $('#more-investor-passportExpiryDate').val();
                var passportCountryOfIssue = $('#more-investor-passportCountryOfIssue').val();
                var more_idverified = $('#more_investor_verify').val();
                var typeOfId = $('#more-investor-typeOfId').val();
                var typeOfIdExpiryDate = $('#more-investor-typeOfIdExpiryDate').val();
                var typeOfIdCountryOfIssue = $('#more-investor-typeOfIdCountryOfIssue').val();
                var src_of_fund4 = $('#src_of_fund4 ').val();
                var irdNumber = $('#more-investor-irdNumber').val();
                var usCitizen = $('#more-investor-usCitizen').val();
                var countryArr = document.getElementsByClassName('more-investor-tex_residence_Country');
                var TINArr = document.getElementsByClassName('more-investor-TIN');
                var reasonArr = document.getElementsByClassName('more-investor-resn_tin_unavailable');
                var countryTINList = new Array();
                for (var i = 0; i < countryArr.length; i++) {
                    var country = countryArr[i].value;
                    var tin = TINArr[i].value;
                    var reason = reasonArr[i].value;
                    countryTINList.push({country: country, tin: tin, reason: reason});
                }
                var moreInvestor = {fname: inv.name, email: email, positionInCompany: positionInCompany, dateOfBirth: dob, countryOfResidence: countryname, occupation: occupation,
                    address: address, countryCode3: countrycode, mobileNo: mobile, typeOfID: idType, license_number: licenseNumber,
                    licenseExpiryDate: licenseExpiryDate, versionNumber: versionNumber, passportNumber: passportNumber,
                    passportExpiryDate: passportExpiryDate, countryOfIssue: passportCountryOfIssue, isInvestor_idverified: more_idverified,
                    typeCountryOfIssue: typeOfIdCountryOfIssue, pir: src_of_fund4, irdNumber: irdNumber, countryTINList: countryTINList};
                console.log(JSON.stringify(moreInvestor));
                return moreInvestor;
            };
            setMoreInvestor = function (inv) {
                $('#more-investor-email').val(inv.email);
                $('#more-investor-dob').val(inv.dateOfBirth);
                $('#more-investor-countryname').val(inv.countryOfResidence);
                $('#more-investor-occupation').val(inv.occupation);
                $('.more-investor-address').val(inv.address);
                $('#src_of_fund2').val(inv.typeOfID);
                $('#more-investor-mobile').val(inv.mobileNo);
                $('#more-investor-licenseNumber').val(inv.license_number);
                $('#more-investor-licenseExpiryDate').val(inv.licenseExpiryDate);
                $('#more-investor-versionNumber').val(inv.versionNumber);
                $('#more-investor-passportNumber').val(inv.passportNumber);
                $('#more-investor-passportExpiryDate').val(inv.passportExpiryDate);
                $('#more-investor-passportCountryOfIssue').val(inv.countryOfIssue);
                $('#more-investor-irdNumber').val(inv.irdNumber);
//                }
            };
        </script>
        <script>
            var x = 0;
            $(".previous1").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step2").hide();
                $("#step1").show();
            });
            $(".previous2").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step3").hide();
                $("#step2").show();
            });
            $(".previous3").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step4").hide();
                $("#step3").show();
            });
            $(".previous4").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step5").hide();
                $("#step4").show();
            });
            $(".previous5").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step6").hide();
                $("#step5").show();
            });
            $(".previous6").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step7").hide();
                $("#step6").show();
            });
            $(".previous7").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step8").hide();
                $("#step7").show();
            });
            $(".previous8").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step9").hide();
                $("#step8").show();
            });
            $(".previous9").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step10").hide();
                $("#step9").show();
            });
            $(".previous10").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step11").hide();
                $("#step10").show();
            });
            $(".previous11").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step12").hide();
                $("#step11").show();
            });
            $(".previous12").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step13").hide();
                $("#step12").show();
            });
            $(".previous13").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step14").hide();
                $("#step13").show();
            });
            $(".previous14").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step15").hide();
                $("#step14").show();
            });
            $(".previous15").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step16").hide();
                $("#step15").show();
            });
            $(".next1").click(function () {
                var companyName = $("#companyName").val();
                companyName = companyName.trim();
                var countryname = $("#Country_of_incorporation").val();
                countryname = countryname.trim();
                var companyDate = $("#Date_of_incorporation").val();
                companyDate = companyDate.trim();
                var companyRegistration = $("#registrationNumber").val();
                companyRegistration = companyRegistration.trim();
                var companyAddress = $("#companyAddress").val();
                companyAddress = companyAddress.trim();
                var file = $("#attachdeedfile").val();
//                companyPostalAddress = companyPostalAddress.trim();
                if (companyName === "") {
                    $("#spanCompanyName").text("This field is reqired ");
                } else if (countryname === "-Select-") {
                    $("#spanCountryCode").text("This field is reqired ");
                } else if (companyDate === "") {
                    $("#spanCompanyDate").text("This field is reqired ");
                } else if (companyRegistration === "") {
                    $("#spanRegistrationNumber").text("This field is reqired ");
                } else if (companyAddress === "") {
                    $("#spanCompanyAddress").text("This field is reqired ");
                } else if (file === "") {
                    $("#error_attachdeedfile").text("Please attach a copy of document");
                } else {
                    x = $('[name="step"]').val();
                    $("#step").attr("value", parseInt(x) + 1);
                    $("#step2").show();
                    $("#step1").hide();
                }
            });
            $(".next2").click(function () {
                var directorDivs1 = document.getElementsByClassName('director-add');
                var j = 0;
                for (var i = 0; i < directorDivs1.length; i++) {
                    var directorDiv1 = directorDivs1[i];
                    var fnameInput1 = directorDiv1.getElementsByClassName('fname')[0];
                    var spanfname = directorDiv1.getElementsByClassName('spanfname')[0];
                    if (fnameInput1.value.trim() === "") {
                        spanfname.innerHTML = "This field is reqired ";
                    } else {

                        j++;
                    }
                }
                if (directorDivs1.length > j) {

                } else {
                    x = $('[name="step"]').val();
                    $("#step").attr("value", parseInt(x) + 1);
                    var array = [];
                    var directorDivs = document.getElementsByClassName('director-add');
                    var meObj = {};
                    for (var i = 0; i < directorDivs.length; i++) {
                        var directorDiv = directorDivs[i];
                        var fnameInput = directorDiv.getElementsByClassName('fname')[0];
                        var meInput = directorDiv.getElementsByClassName('cls-me')[0];
                        if (meInput.value === 'N') {
                            array.push({name: fnameInput.value, me: meInput.value});
                        } else {
                            meObj = {name: fnameInput.value, me: meInput.value};
                        }
                    }
                    //                directors[0] = meObj;
                    for (var i = 0; i < array.length; i++) {

                        directors.push(array[i]);
                        //                    console.log("console"+JSON.strigify(directors[0]));
                    }
                    $('#directors-index').val(0);
                    $('.this-name').text(meObj.name);
                    $('.first_name').val('');
                    $('.middle_name').val('');
                    $('.last_name').val('');
                    var fn = meObj.name;
                    var fnarr = fn.split(" ");
                    $('.first_name').val(fnarr[0]);
                    if (fnarr.length === 3) {
                        $('.middle_name').val(fnarr[1]);
                        $('.last_name').val(fnarr[2]);
                    } else if (fnarr.length === 2) {
                        $('.last_name').val(fnarr[1]);
                    }

                    //                var idx = parseInt($('#directors-index').val());
                    //                idx++;
                    //                $('#directors-index').val(idx);
                    $("#step3").show();
                    $("#step2").hide();
                }

            });
            $(".next3").click(function () {
                var Date_of_Birth = $("#Date_of_Birth").val();
                Date_of_Birth = Date_of_Birth.trim();
                var OccupationOption = $('.OccupationOption').val();
                var Occupation = $("#cOccupation").val();
                Occupation = Occupation.trim();
                var holderCountryOfResidence = $("#holderCountryOfResidence").val();
                holderCountryOfResidence = holderCountryOfResidence.trim();
                var positionInCompany = $("#positionInCompany").val();
                positionInCompany = positionInCompany.trim();
                //                holderCountryOfResidence = holderCountryOfResidence.trim();
                if (Date_of_Birth === "") {
                    $("#spanDate_of_Birth").text("This field is reqired ");
                } else if (OccupationOption === "-Select-" || OccupationOption === "other" && Occupation === "") {
                    $("#spanOccupation").text("This field is reqired ");
                } else if (holderCountryOfResidence === "-Select-") {
                    $("#spanHolderCountryOfResidence").text("This field is reqired ");
                } else if (positionInCompany === "-Select-") {
                    $("#error-positionInCompany").text("This field is reqired ");
                } else {
                    x = $('[name="step"]').val();
                    $("#step").attr("value", parseInt(x) + 1);
                    $("#step4").show();
                    $("#step3").hide();
                }
            });
            $(".next4").click(function () {
                var address = $("#address").val();
                address = address.trim();
                var OPmobileNo = $("#mobileNo").val();
                OPmobileNo = OPmobileNo.trim();
                if (address === "") {
                    $("#spanHomeaddress").text("This field is reqired ");
                } else if (OPmobileNo === "") {
                    $("#spanOPmobileNo").text("This field is reqired ");
                } else {
                    x = $('[name="step"]').val();
                    $("#step").attr("value", parseInt(x) + 1);
                    $("#step5").show();
                    $("#step4").hide();
                }
            });
            $(".next5").click(function () {

                var verificationa = $("#verificationa").val();
                verificationa = verificationa.trim();
                if (verificationa === "") {
                    $("#spanverificationa").text("This field is reqired ");
                } else {
                    x = $('[name="step"]').val();
                    $("#step").attr("value", parseInt(x) + 1);
                    $("#step6").show();
                    $("#step5").hide();
                }
            });
//            $(".countryname").change(function () {
//                alert("ssssssssssssss");
//    $('#spanCountryofissue').text('');
//});             
//            $('#IRD_Number').keyup(function () {
//    $('#error_IRD_Number').text('');
//});         
            $('#typeExpirydate').change(function () {
//                    alert("ssssssssssssssexp");
                $('#spanPExpirydate').text('');
            });
            $("#Countryofissue").change(function () {
//                    alert("sssssssssssssscuntry");
                $('#spanCountryofissue').text('');
            });

            $(".next6").click(function () {
                var url = '';
                var firstName = '';
                var middleName = '';
                var lastName = '';
                var License_number = '';
                var licence_verson_number = '';
                var licence_expiry_Date = '';
                var passport_number = '';
                var passport_expiry = '';
                var index = $("#src_of_fund1 option:selected").val();
                if (index === "1") {
                    url = './rest/groot/db/api/dl-verification';
                    firstName = $("#licence_first_name").val();
                    lastName = $("#licence_last_name").val();
                    License_number = $("#licenseNumber").val();
                    License_number = License_number.trim();
                    var Expirydate = $("#Expirydate").val();
                    Expirydate = Expirydate.trim();
                    licence_verson_number = $("#versionNumber").val();
                    licence_verson_number = licence_verson_number.trim();
                    if (firstName === "") {
                        $("#error_licence_first_name").text("This field is reqired ");
                    } else if (lastName === "") {
                        $("#error_licence_last_name").text("This field is reqired ");
                    } else if (License_number === "") {
                        $("#spanlicenseNumber").text("This field is reqired ");
                    } else if (Expirydate === "") {
                        $("#spanExpirydate").text("This field is reqired ");
                    } else if (licence_verson_number === "") {
                        $("#spanVersionnumber").text("This field is reqired ");
                    } else {
                        x = $('[name="step"]').val();
                        $("#step").attr("value", parseInt(x) + 1);
                        $("#step7").show();
                        $("#step6").hide();
                    }
                } else if (index === "2") {
                    url = './rest/groot/db/api/pp-verification';
                    firstName = $("#passport_first_name").val();
                    lastName = $("#passport_last_name").val();
                    passport_number = $("#Passportnumber").val();
                    passport_expiry = $("#PExpirydate").val();
                    var CountryOfIssue = $("#Countryofissue").val();
//                     alert("cuntry = -"+CountryOfIssue);
                    passport_number = passport_number.trim();
                    if (firstName === "") {
                        $("#error_passport_first_name").text("This field is reqired ");
                    } else if (lastName === "") {
                        $("#error_passport_last_name").text("This field is reqired ");
                    } else if (passport_number === "") {
                        $("#spanPassportnumber").text("This field is reqired ");
                    } else if (passport_expiry === "") {
                        $("#spanPExpirydate").text("This field is reqired ");
                    } else if (CountryOfIssue === " -Select-") {
                        $("#spanCountryofissue").text("This field is reqired ");
                    } else {
                        x = $('[name="step"]').val();
                        $("#step").attr("value", parseInt(x) + 1);
                        $("#step7").show();
                        $("#step6").hide();
                    }
                    //                             
                } else {
                    var TypeofID = $("#TypeofID").val();
                    var ExpirydateOther = $("#typeExpirydate").val();
                    var countryname1 = $("#countryname1").val();
                    var otherDocument = $("#otherDocument").val();
                    TypeofID = TypeofID.trim();
                    if (TypeofID === "") {
                        $("#spanTypeofID").text("This field is reqired ");
                    } else if (ExpirydateOther === "") {
                        $("#spantypeExpirydate").text("This field is reqired ");
                    } else if (countryname1 === " -Select-") {
                        $("#span_countryname1").text("This field is reqired ");
                    } else if (otherDocument === "") {
                        $("#span-otherDocument").text("This field is reqired ");
                    } else {
                        x = $('[name="step"]').val();
                        $("#step").attr("value", parseInt(x) + 1);
                        $("#step7").show();
                        $("#step6").hide();
                    }
                }

                var Date_of_Birth = $('#Date_of_Birth').val();
//                alert(Date_of_Birth);
//                var passport_expiry = x.find('.pass_expiry').val();
//                var passport_expiry = "2017-10-10";
                var DataObj = {license_number: License_number, licence_expiry_Date: licence_expiry_Date,
                    licence_verson_number: licence_verson_number, date_of_Birth: Date_of_Birth, firstName: firstName,
                    middleName: middleName, lastName: lastName, passport_number: passport_number, passport_expiry: passport_expiry};
                console.log(DataObj);
                $.ajax({
                    type: 'POST',
                    url: url,
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(DataObj),
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        var obj = JSON.parse(data);
                        if (index === "1") {
                            if (obj.driversLicence.verified) {
                                $('#investor_verify').val('True');
                            } else {
                                $('#investor_verify').val('false');
//                                alert("wrong data");
                            }
                        } else if (index === "2") {
                            if (obj.passport.verified) {
//                               
                                $('#investor_verify').val('True');
                            } else {
//                                alert("wrong data");
                                $('#investor_verify').val('false');
                            }

                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert(" inside error" + jqXHR);
                    }
                });



            });
            $(".next7").click(function () {
//                alert("ffs");
                var i = $('#counter').val();
                var IRDNumber = $("#irdNumber").val().trim();
                var USCitizen = $("#USCitizen option:selected").val();
                var j = 0;
                if (IRDNumber.length !== 11) {
                    $("#spanIRDNumber").text("This field is reqired ");
                } else if (USCitizen === "2") {
                    var tindivs = document.getElementsByClassName('checktindata');
                    for (var i = 0; i < tindivs.length; i++) {
                        var tindiv = tindivs[i];
                        var tinnum = tindiv.getElementsByClassName('TIN')[0];
                        var tinerror = tindiv.getElementsByClassName('tin-error')[0];
                        var country = tindiv.getElementsByClassName('countrynameexcludenz')[0];
                        var countryerror = tindiv.getElementsByClassName('countrynameexcludenz-error')[0];
                        var reason = tindiv.getElementsByClassName('resn_unavailable')[0];
                        var reasonerror = tindiv.getElementsByClassName('resn_unavailable-error')[0];
                        if (country.value === " -Select-") {
                            countryerror.innerHTML = "This field is reqired ";
//                            alert("country span");
                        } else if (tinnum.value === "" && reason.value === "") {
                            tinerror.innerHTML = "This field is reqired ";
                            reasonerror.innerHTML = "This field is reqired ";

                        }
//                            tinerror.innerHTML = "This field is reqired ";

                        else {
                            j++;
                        }
                    }
                    if (tindivs.length > j) {
//                            alert("lenght grater");
                    } else {
                        tinfornext7();
                    }
                } else {
                    tinfornext7();
                }
            });
            $(".next8").click(function () {

                var radioValue = $("input[name='year']:checked").val();
                var email = $('#more-investor-email').val().trim();
                var emailExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (email === "" || !emailExpression.test(email)) {
                    $('#more-investor-email-error').text("Email must be 'example@gmail.com' format");
                } else {
                    x = $('[name="step"]').val();
                    $("#step").attr("value", parseInt(x) + 1);
                    if (radioValue === "send-now") {
                        var idx = parseInt($('#directors-index').val());
                        if (directors.length > idx) {

                            $('#directors-index').val(idx);
                            current_director = directors[idx];
                            $('.curr-director').text(current_director.name);
                            $('.first_name').val('');
                            $('.middle_name').val('');
                            $('.last_name').val('');
                            var fn = current_director.name;
                            var fnarr = fn.split(" ");
                            $('.first_name').val(fnarr[0]);
                            if (fnarr.length === 3) {
                                $('.middle_name').val(fnarr[1]);
                                $('.last_name').val(fnarr[2]);
                            } else if (fnarr.length === 2) {
                                $('.last_name').val(fnarr[1]);
                            }

                            $("#step9").show();
                            $("#step8").hide();
                        } else {
                            $("#step12").show();
                            $("#step8").hide();
                        }

                    } else if (radioValue === "send-email") {
                        $("#step12").show();
                        $("#step8").hide();
                    } else {
                        $("#step12").show();
                        $("#step8").hide();
                    }

                }
            });
            $(".next9").click(function () {
                var address = $('.more-investor-address').val().trim();
                var dob = $('#more-investor-dob').val().trim();
                var OccupationOption = $('.OccupationOption').val();
                var Occupation = $('#more-investor-occupation').val();
                var mobile = $('#more-investor-mobile').val().trim();
                var position = $('#more-investor-positionInCompany option:selected').val();
                if (address === "") {
                    $('#more-investor-address-error').text("This field is reqired");
                } else if (mobile === "") {
                    $('#more-investor-mobile-error').text("This field is reqired");
                } else if (dob === "") {
                    $('#more-investor-dob-error').text("This field is reqired");
                } else if (OccupationOption === "-Select-" || OccupationOption === "other" && Occupation === "") {
                    $("#spanOccupation").text("This field is reqired ");
                } else if (position === "0") {
                    $('#more-investor-positionInCompany-error').text("This field is reqired");
                } else {
                    x = $('[name="step"]').val();
                    $("#step").attr("value", parseInt(x) + 1);
                    $("#step10").show();
                    $("#step9").hide();
                }

            });
            $(".next10").click(function () {
                var url = '';
                var firstName = '';
                var middleName = '';
                var lastName = '';
                var License_number = '';
                var licence_verson_number = '';
                var licence_expiry_Date = '';
                var passport_number = '';
                var passport_expiry = '';
                var index = $("#src_of_fund2 option:selected").val();
                if (index === "1") {
                    url = './rest/groot/db/api/dl-verification';
                    firstName = $("#more_licence_first_name").val().trim();
//                 alert(licenseFN);
                    lastName = $("#more_licence_last_name").val().trim();
//                  alert(licenseLN);
                    License_number = $("#more-investor-licenseNumber").val().trim();
                    licence_expiry_Date = $("#more-investor-licenseExpiryDate").val();
                    licence_verson_number = $("#more-investor-versionNumber").val().trim();
                    if (firstName === "") {
                        $("#more_error_licence_first_name").text("This field is reqired ");
                    } else if (lastName === "") {
                        $("#more_error_licence_last_name").text("This field is reqired ");
                    } else if (License_number === "") {
                        $("#more-investor-licenseNumber-error").text("This field is reqired ");
                    } else if (Expirydate === "") {
                        $("#more-investor-licenseExpiryDate-error").text("This field is reqired ");
                    } else if (licence_verson_number === "") {
                        $("#more-investor-versionNumber-error").text("This field is reqired ");
                    } else {
                        x = $('[name="step"]').val();
                        $("#step").attr("value", parseInt(x) + 1);
                        $("#step11").show();
                        $("#step10").hide();
                    }
                } else if (index === "2") {
                    url = './rest/groot/db/api/pp-verification';
                    firstName = $('#more_passport_first_name').val();
                    middleName = $('#more_passport_middle_name').val();
                    lastName = $('#more_passport_last_name').val();
                    passport_number = $("#more-investor-passportNumber").val().trim();
                    passport_expiry = $('.more-investor-passportExpiryDate').val();
                    if (passport_number === "") {
                        $("#more-investor-passportNumber-error").text("This field is reqired ");
                    } else {
                        x = $('[name="step"]').val();
                        $("#step").attr("value", parseInt(x) + 1);
                        $("#step11").show();
                        $("#step10").hide();
                    }
                    //                             
                } else {
                    var TypeofID = $("#more-investor-typeOfId").val().trim();
                    var Expirydate = $("#more-investor-typeOfIdExpiryDate").val();
                    var country = $("#more-investor-typeOfIdCountryOfIssue").val().trim();
                    if (TypeofID === "") {
                        $("#more-investor-typeOfId-error").text("This field is reqired ");
                    } else if (Expirydate === "") {
                        $("#more-investor-typeOfIdExpiryDate-error").text("This field is reqired ");
                    } else if (country === " -Select-") {
                        $("#more-investor-typeOfIdCountryOfIssue-error").text("This field is reqired ");
                    } else {
                        x = $('[name="step"]').val();
                        $("#step").attr("value", parseInt(x) + 1);
                        $("#step11").show();
                        $("#step10").hide();
                    }
                }


                var Date_of_Birth = $('#more-investor-dob').val();
                if (index === "1" || index === "2") {
                    var DataObj = {license_number: License_number, licence_expiry_Date: licence_expiry_Date,
                        licence_verson_number: licence_verson_number, date_of_Birth: Date_of_Birth, firstName: firstName,
                        middleName: middleName, lastName: lastName, passport_number: passport_number, passport_expiry: passport_expiry};
                    console.log(DataObj);
                    $.ajax({
                        type: 'POST',
                        url: url,
                        headers: {"Content-Type": 'application/json'},
                        data: JSON.stringify(DataObj),
                        success: function (data, textStatus, jqXHR) {
                            console.log(data);
                            var obj = JSON.parse(data);
                            if (index === "1") {
                                if (obj.driversLicence.verified) {

                                    $('#more_investor_verify').val('True');
                                } else {
                                    $('#more_investor_verify').val('false');
                                }
                            } else if (index === "2") {
                                if (obj.passport.verified) {
//                                swal({
//                                    title: "Success",
//                                    text: "Data is Verified.",
//                                    type: "info",
//                                    timer: 3500,
//                                    showConfirmButton: true
//                                });
                                    $('#more_investor_verify').val('True');
                                } else {
//                                alert("wrong data");
                                    $('#more_investor_verify').val('false');
                                }

                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
//                        alert(" inside error" + jqXHR);
                        }
                    });
                }


            });
            $(".next11").click(function () {
                var countryOfResidence = $('#more-investor-countryOfResidence').val();
                var idr = $('#more-investor-irdNumber').val();
                var USCitizen = $("#more-investor-usCitizen option:selected").val();
                var j = 0;
                if (countryOfResidence === "-Select-") {
                    $('#more-investor-countryOfResidence-error').text("This field is reqired");
                } else if (idr === "") {
                    $('#more-investor-irdNumber-error').text("This field is reqired");
                } else if (idr.length !== 11) {
                    $('#more-investor-irdNumber-error').text("9 Digit IDR no. Required");
                } else if (USCitizen === "2") {
                    var tindivs = document.getElementsByClassName('checktin3data');
                    for (var i = 0; i < tindivs.length; i++) {
                        var tindiv = tindivs[i];
                        var tinnum = tindiv.getElementsByClassName('more-investor-TIN')[0];
                        var tinerror = tindiv.getElementsByClassName('more-investor-TIN-error')[0];
                        var country = tindiv.getElementsByClassName('countrynameexcludenz')[0];
                        var countryerror = tindiv.getElementsByClassName('countrynameexcludenz-error')[0];
                        var reason = tindiv.getElementsByClassName('more-investor-resn_tin_unavailable')[0];
                        var reasonerror = tindiv.getElementsByClassName('more-investor-resn_tin_unavailable-error')[0];
                        if (country.value === " -Select-") {
                            countryerror.innerHTML = "This field is reqired ";
                        } else if (tinnum.value === "" && reason.value === "") {
                            reasonerror.innerHTML = "This field is reqired ";
                            tinerror.innerHTML = "This field is reqired ";
                        } else {
                            j++;
                        }
                    }
                    if (tindivs.length > j) {

                    } else {
                        tinfornext11();
                    }
                } else {
                    tinfornext11();
                }
            });
            $(".next12").click(function () {

                var IRDNumber = $("#comIRDNumber").val().trim();
                var source = $("#sourceOfFunds option:selected").val();
                if (IRDNumber === "") {
                    $("#spancomIRDNumber").text("field should be of 9 degits");
                } else if (source === "1") {
                    $("#sourceOfFunds-error").text("This field is reqired ");
                } else {
                    x = $('[name="step"]').val();
                    $("#step").attr("value", parseInt(x) + 1);
                    $("#step13").show();
                    $("#step12").hide();
                }
            });
            $(".next13").click(function () {
                var firstselect = $('#isCompanyFinancialInstitution option:selected').val();
                var secondselect = $('#isCompanyUSCitizen option:selected').val();
                var j = 0;
                if (firstselect === "0") {
                    $("#isCompanyFinancialInstitution-error").text("This field is reqired ");
                } else if (secondselect === "0") {
                    $("#isCompanyUSCitizen-error").text("This field is reqired ");
                } else if (secondselect === "2") {
                    var tindivs = document.getElementsByClassName('checktin4data');
                    for (var i = 0; i < tindivs.length; i++) {
                        var tindiv = tindivs[i];
                        var tinnum = tindiv.getElementsByClassName('companyTaxIdentityNumber')[0];
                        var tinerror = tindiv.getElementsByClassName('companyTaxIdentityNumber-error')[0];
                        var reason = tindiv.getElementsByClassName('companyReasonTIN')[0];
                        var reasonerror = tindiv.getElementsByClassName('companyReasonTIN-error')[0];
                        var country = tindiv.getElementsByClassName('countrynameexcludenz')[0];
                        var countryerror = tindiv.getElementsByClassName('countrynameexcludenz-error')[0];
                        if (country.value === " -Select-") {
                            countryerror.innerHTML = "This field is reqired ";
                        } else if (tinnum.value === "" && reason.value === "") {
                            tinerror.innerHTML = "This field is reqired ";
                            reasonerror.innerHTML = "This field is reqired ";

                        } else {
                            j++;
                        }
                    }
                    if (tindivs.length > j) {

                    } else {
                        x = $('[name="step"]').val();
                        $("#step").attr("value", parseInt(x) + 1);
                        $("#step14").show();
                        $("#step13").hide();
                    }
                } else {
                    x = $('[name="step"]').val();
                    $("#step").attr("value", parseInt(x) + 1);
                    $("#step14").show();
                    $("#step13").hide();
                }

            });
             $('.bank_name').change(function () {
//                if ($('.bank_name option:selected').val() === 'Other') {
//                    $(".toggal_other").show();
//                } else {
//                    $(".toggal_other").hide();
//                }
               var data = $('.bank_name option:selected').data('id');
//               alert(data);   
               $('#accountNumber').val(data);
            });
            $(".next14").click(function () {
                var bank_name = $('.bank_name option:selected').text();
                bank_name = bank_name.trim();
                var other_name = $("#other_other_name").val();
                var accountNumber = $("#accountNumber").val().trim();
                var nameOfAccount = $("#nameOfAccount").val().trim();
                var attachbankfile = $("#attachbankfile").val().trim();
//                if (bank_name === "– Select –" || bank_name === "Other") {

                if (bank_name === "– Select –") {
                    $("#error_bank_name").text("This field is reqired ");
                } else if (bank_name === "Other" && other_name === "") {

                    $("#error_other_bank_name").text("This field is reqired ");

                } else if (nameOfAccount === "") {
                    $("#spannameOfAccount").text("This field is reqired ");
                } else
                if (accountNumber === "") {
                    $("#spanaccountNumber").text("This field is reqired ");
                } else if (attachbankfile === "") {
                    $(".error_attachbankfile").text("Please attach verification of your bank account.");
                } else {
                    x = $('[name="step"]').val();
                    $("#step").attr("value", parseInt(x) + 1);
                    $("#step15").show();
                    $("#step14").hide();
                }
            });
            $(".next15").click(function () {
                var y = $('input[name=authorized_condition]').is(':checked');
                var x = $('input[name=term_condition]').is(':checked');
                if (!x) {
                    $("#error_term_condition").text("Please read and agree to the Terms and Conditions");
                } else if (!y) {
                    $("#error_authorized_condition").text("I am authorised to act and accept on behalf of all account holders");
                } else {
                    $('.save-new-btn').hide();
                    x = $('[name="step"]').val();
                    $("#step").attr("value", parseInt(x) + 1);
                    $("#step16").show();
                    $("#step15").hide();
                }

            });
            $(".companyaccount").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) + 1);
                $("#step10").show();
                $("#step2").hide();
            });


            $('.bank_name').change(function () {
                //                alert('other');
                if ($('.bank_name option:selected').val() === 'Other') {
                    $(".toggal_other").show();
                } else {
                    $(".toggal_other").hide();
                }
            });


        </script>
        <script>
            $('#src_of_fund1').change(function () {
                var val = $("#src_of_fund1 option:selected").val();
                if (val === "1") {
                    $('.verifybtn').show();
                    $('.passport-select').hide();
                    $('.other-select').hide();
                    $('.drivery-licence').show();
                }
                if (val === "2") {
                    $('.verifybtn').show();
                    $('.passport-select').show();
                    $('.other-select').hide();
                    $('.drivery-licence').hide();
                }
                if (val === "3") {
                    $('.verifybtn').hide();
                    $('.passport-select').hide();
                    $('.other-select').show();
                    $('.drivery-licence').hide();
                }
            });
            $('#src_of_fund2').change(function () {
                var val = $("#src_of_fund2 option:selected").val();
                if (val === "1") {
                    $('.passport-select1').hide();
                    $('.other-select1').hide();
                    $('.drivery-licence1').show();
                }
                if (val === "2") {
                    $('.passport-select1').show();
                    $('.other-select1').hide();
                    $('.drivery-licence1').hide();
                }
                if (val === "3") {
                    $('.passport-select1').hide();
                    $('.other-select1').show();
                    $('.drivery-licence1').hide();
                }
            });
            $('.selectoption1').change(function () {
                var val = $(".selectoption1 option:selected").val();
                if (val === "1") {
                    $('.yes-option').hide();
                }
                if (val === "2") {
                    $('.yes-option').show();
                }

            });
            $('.selectoption2').change(function () {
                var val = $(".selectoption2 option:selected").val();
                if (val === "1") {
                    $('.yes-option1').hide();
                }
                if (val === "2") {
                    $('.yes-option1').show();
                }

            });
            $('.selectoption3').change(function () {
                var val = $(".selectoption3 option:selected").val();
                if (val === "7") {
                    $('.inputtype').show();
                } else {
                    $('.inputtype').hide();
                }

            });
            $('.selectoption6').change(function () {
                var val = $(".selectoption6 option:selected").val();
                if (val === "1") {
                    $('.selectno6').show();
                } else {
                    $('.selectno6').hide();
                }

            });
            $('.selectoption4').change(function () {
                var val = $(".selectoption4 option:selected").val();
                if (val === "1") {
                    $('.yes-option4').hide();
                }
                if (val === "2") {
                    $('.yes-option4').show();
                }

            });
        </script>
        <script>
            $(document).ready(function () {
                $("#add-country-another").click(function () {
                    $(".yes-new").append("<div class='row checktindata removecountry'><div class='col-sm-6' ><label class='label_input' style='text-align:left'>Country of tax residence:</label></div><div class='col-sm-6 details-pos flag-drop' onclick='changecoutry(this);'><input type='text' class='form-control countrynameexcludenz'  id='countryname' name='countryCode' placeholder='Enter Country Code' readonly='readonly'> <span class='error countrynameexcludenz-error' ></span></div><div class='col-sm-6'><label class='label_input'>Tax Identification Number (TIN)</label></div><div class='col-sm-6'><input type='text' class='form-control input-field TIN' name='fullName' required='required' placeholder='Enter TIN' onkeyup='removetinspan()' ><span class='error tin-error' ></span> </div><div class='col-sm-6'><label class='label_input'> Reason if TIN not available </label></div><div class='col-sm-6'><input type='text' class='form-control input-field resn_unavailable' name='fullName' required='required' placeholder='' onkeyup='removetinspan();'/><span class='error resn_unavailable-error' ></span><a href='javascript:void(0);' onclick='removecountrydiv(this);'><i class='far fa-times-circle'></i></a></div></div>");
                    $(".countrynameexcludenz").intlTelInput_3();
                });
                $("#add-country-another4").click(function () {
                    $(".yes-new4").append("<div class='row checktin4data removecountry'><div class='col-sm-6' ><label class='label_input' style='text-align:left'>Country of tax residence:</label></div><div class='col-sm-6 details-pos flag-drop' onclick='changecoutry(this);'><input type='text' class='form-control countrynameexcludenz'  id='countryname' name='countryCode' placeholder='Enter Country Code' readonly='readonly'><span class='error countrynameexcludenz-error' ></span></div><div class='col-sm-6'><label class='label_input'>Tax Identification Number (TIN) </label></div><div class='col-sm-6'><input type='text' class='form-control input-field companyTaxIdentityNumber' name='fullName' required='required' placeholder='Enter TIN' onkeyup='removetinspan()' /><span class='error companyTaxIdentityNumber-error' ></span></div><div class='col-sm-6'><label class='label_input'> Reason if TIN not available </label></div><div class='col-sm-6'><input type='text' class='form-control input-field companyReasonTIN' name='fullName' required='required' placeholder='' onkeyup='removetinspan();'/><span class='error companyReasonTIN-error' ></span><a href='javascript:void(0);' onclick='removecountrydiv(this);'><i class='far fa-times-circle'></i></a></div></div>");
                    $(".countrynameexcludenz").intlTelInput_3();
                });
                $("#add-country-another3").click(function () {
                    $(".yes-new3").append("<div class='row yes-new3-clone checktin3data removecountry'><div class='col-sm-6' ><label class='label_input' style='text-align:left'>Country of tax residence:</label></div><div class='col-sm-6 details-pos flag-drop' onclick='changecoutry(this);'><input type='text' class='form-control countrynameexcludenz more-investor-tex_residence_Country'  name='countryCode' placeholder='Enter Country Code' readonly='readonly'><span class='error countrynameexcludenz-error' ></span></div><div class='col-sm-6'><label class='label_input'>Tax Identification Number (TIN)</label></div><div class='col-sm-6 '><input type='text' class='form-control input-field more-investor-TIN' name='fullName' required='required' placeholder='Enter TIN' onkeyup='removetinspan()'/><span class='error more-investor-TIN-error' ></span></div><div class='col-sm-6'><label class='label_input'>Reason if TIN not available :</label></div><div class='col-sm-6'><input type='text' class='form-control input-field more-investor-resn_tin_unavailable' name='fullName' required='required' placeholder='' onkeyup='removetinspan();'/><span class='error more-investor-resn_tin_unavailable-error' ></span><a href='javascript:void(0);' onclick='removecountrydiv(this);'><i class='far fa-times-circle'></i></a></div></div>");
                    $(".countrynameexcludenz").intlTelInput_3();
                });
            });
        </script>
        <script>

            var current_director = {};
            $(document).ready(function () {
                var count = 1;
                $('#counter').val(count);
                $("#btn20").click(function () {
                    $(".director-section").append("<div class='row director-add removedirector'><div class='col-sm-4'><label class='label_input' style='text-align:left'>Full name of trustee/beneficial owner</label></div><div class='col-sm-2'><select class='selectoption option-add' id=''><option value='Appointment'>Mr</option><option value='Interview'>Mrs</option><option value='Regarding a post'>Miss</option><option id='other' value='Other'>Master</option></select></div><div class='col-sm-4'><input type='text' class='fname input-field' id='fname' name='fullName' placeholder='Enter full name' onkeyup='removespan();'><span class='error spanfname' id='spanfname'></span><input type='hidden' class='cls-me' name='me' value='N'></div><div class='col-sm-2 this-space'><a href='javascript:void(0)' class='this-btn' onclick='addbtnfn(this);'>This is me</a><a href='javascript:void(0);' onclick='removedatadiv(this);'><i class='far fa-times-circle'></i></a></div></div>");
                    counter = $('#counter').val();
                    counter++;
                    $('#counter').val(counter);
                });
            });
            function removedatadiv(ele) {
                ele.closest('.removedirector').remove();
            }
            function removecountrydiv(ele) {
                ele.closest('.removecountry').remove();
            }
            function addbtnfn(ele) {
                var close = ele.closest('.director-add');
                var find = $(close).find('.fname');
                var btn = $(close).find('.this-btn');
                var cls_me = $(close).find('.cls-me');
                var name = find.val();
                $("a.this-btn").removeClass("check-this-btn");
                $('.cls-me').val('N');
                btn.addClass("check-this-btn");
                cls_me.val('Y');
            }
        </script>
        <script>
            function checkird(obj) {
                str = obj.value.replace('', '');
                if (str.length > 10) {
                    str = str.substr(0, 10);
                } else
                {
                    str = str.replace(/\D+/g, "").replace(/([0-9]{3})([0-9]{3})([0-9]{2}$)/gi, "$1-$2-$3");
                }
                obj.value = str;
            }

        </script>
        <script>
//            function checkAccountNO(obj) {
//                str = obj.value.replace('', '');
//                if (str.length > 18) {
//                    str = str.substr(0, 18);
//                } else
//                {
//                    str = str.replace(/([A-Za-z0-9]{2})([[A-Za-z0-9]{4})([[A-Za-z0-9]{7})([[A-Za-z0-9]{4}$)/gi, "$1-$2-$3-$4"); //mask numbers (xxx) xxx-xxxx    
//
//                }
//                obj.value = str;
//            }
            function checkAccountNO() {
                var ac = document.getElementById("accountNumber").value;
                if (ac.length === 2) {
                    var ac20 = ac.substr(0, 2);
                    var a = ac20 + '-';
                    $("#accountNumber").val(a);
                }
                if (ac.length === 7) {
                    var ac21 = ac.substr(7);
                    var b = ac + '-' + ac21;
                    $("#accountNumber").val(b);
                }
                if (ac.length === 15) {
                    var ac212 = ac.substr(15);
                    var c = ac + '-' + ac212;
                    $("#accountNumber").val(c);
                }
                if (ac.length > 18) {
                    var ac23 = ac.substr(0, 18);
                    var d = ac23;
                    $("#accountNumber").val(d);
                }
            }
        </script>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
                $('.advisor-show').hide();
            });
            $('.avisor-click').change(function () {
                var val = $(".avisor-click option:selected").val();
                if (val === "1") {
                    $('.advisor-show').hide();
                }
                if (val === "2") {
                    $('.advisor-show').show();
                }

            });
            function removespan() {
                $('.spanfname').text('');
            }
        </script>
        <script>

            $('#submit').click(function () {
                var status = "SUBMISSION";
                saveData(status);
            });
            $('.saveExit').click(function () {
                var status = "PENDING";
                saveData(status);
            });
            saveData = function (status) {

                var companyName = $('#companyName').val();
                var countryCode = $('#countryCode').val();
                var date_of_incorporation = $('#Date_of_incorporation').val();
                var registrationNumber = $('#registrationNumber').val();
                var companyAddress = $('#companyAddress').val();
                var postalAddress = $('#postalAddress').val();
                var investmentAdviser = $('#investmentAdviser option:selected').text();
                var companyAdvisor = $('#companyAdvisor option:selected').text();
                var selectAdviser = $('#selectAnAdviser option:selected').text();
                var fname = $('#fname').val(); // it may be multiple name
                var holder_email = $('#email').val(); // it may be multiple name
                var date_of_Birth = $('#Date_of_Birth').val();
                var occupation = $('#cOccupation').val();
//                    alert("occupation" + occupation);
                var holderCountryOfResidence = $('#countryOfResidence').val();
                var positionInCompany = $('#positionInCompany option:selected').val();
//                    alert(positionInCompany);
                var address = $('#address').val();
                var countryCode3 = $('#countryCode3').val();
                var mobileNo = $('#mobileNo').val();
                var otherNumber = $('#otherNumber option:selected').text();
//                    alert("otherNumber" + otherNumber);
                var otherCountryCode = $('#countryCode').val();
                var otherMobileNo = $('#mobileNo2').val();
                var src_of_fund2 = $('#src_of_fund1 option:selected').text();
                var licenseNumber = $('#licenseNumber').val(); //for license ID
                var licenseExpiryDate = $('#expiryDate').val(); //for license ID
//                alert(licenseExpiryDate);
                var versionNumber = $('#versionNumber').val();
//                    alert("versionNumber" + versionNumber);//for license ID
                var passportNumber = $('#passportNumber').val(); //for passport ID
                var passportExpiryDate = $('#passportExpiryDate').val(); //for passport ID
                var countryOfIssue = $('#countryOfIssue').val(); //for passport ID
                var investor_verify = $('#investor_verify').val();
                var typeOfID = $('#typeOfID').val(); //for Other ID
                var typeExpiryDate = $('#typeExpiryDate').val(); //for Other ID
                var typeCountryOfIssue = $('#typeCountryOfIssue').val(); //for Other ID 
                var myFile = $('#myFile').val();
                //  Please enter the tax details
                var countryOfResidence = $('#countryOfResidence').val();
                var irdNumber = $('#irdNumber').val();
//                    alert("irdNumber" + irdNumber);
                var citizenUS = $('#citizenUS option:selected').text(); // if citizen yes
                var countryOfTaxResidence = $('#countryOfTaxResidence').val(); //repeat multiple time 
                var taxIdenityNumber = $('#taxIdenityNumber').val(); //repeat multiple time
                //                var reasonTIN = $('#reasonTIN').val();                          //repeat multiple time
                var pir = $('#pir option:selected').text();
                var companyIRDNumber = $('#comIRDNumber').val();
                var sourceOfFunds = $('#sourceOfFunds').val();
                var detail = $('#detail').val();
                var isCompanyListed = $('#isCompanyListed option:selected').text();
                var isCompanyGovernment = $('#isCompanyGovernment option:selected').text();
                var isCompanyNZPolice = $('#isCompanyNZPolice option:selected').text();
                var isCompanyFinancialInstitution = $('#isCompanyFinancialInstitution option:selected').text();
                var isCompanyActivePassive = $('#isCompanyActivePassive option:selected').text();
                var isCompanyUSCitizen = $('#isCompanyUSCitizen option:selected').text(); // if citizen yes
                var companyCountryOfTaxResidence = $('#companyCountryOfTaxResidence').val(); //repeat multiple time 
                var companyTaxIdentityNumber = $('#companyTaxIdentityNumber').val(); //repeat multiple time
                var companyReasonTIN = $('#companyReasonTIN').val(); //repeat multiple time
                var bankName = $('#bankName').val();
                var nameOfAccount = $('#nameOfAccount').val();
                var accountNumber = $('#accountNumber').val();
                var step = $('[name="step"]').val();
                var reg_type = $('[name="register_type"]').val();
                var reg_id = '${company.reg_id}';
                var id = '${company.id}';
                var email = '${company.email}';
                var password = '${company.password}';
                var raw_password = '${company.raw_password}';
                var countryArr = document.getElementsByClassName('tex_residence_Country');
                var TINArr = document.getElementsByClassName('TIN');
//                var reasonArr = document.getElementsByClassName('resn_tin_unavailable');
                var countryTINList = new Array();
                for (var i = 0; i < countryArr.length; i++) {
                    var country = countryArr[i].value;
                    var tin = TINArr[i].value;
//                    var reason = reasonArr[i].value;
                    countryTINList.push({country: country, tin: tin});
                }
                var DataObj = {id: id, companyName: companyName, email: email, password: password, raw_password:raw_password,countryCode: countryCode, dateOfInco: date_of_incorporation,
                    registerNumber: registrationNumber, companyAddress: companyAddress, postalAddress: postalAddress, investAdviser: investmentAdviser,
                    companyAdvisor: companyAdvisor, selectAdviser: selectAdviser, fname: fname, holder_email: holder_email, dateOfBirth: date_of_Birth,
                    occupation: occupation, holderCountryOfResidence: holderCountryOfResidence, positionInCompany: positionInCompany, address: address,
                    countryCode3: countryCode3, mobileNo: mobileNo, otherNumber: otherNumber, otherCountryCode: otherCountryCode,
                    otherMobileNo: otherMobileNo, srcOfFund: src_of_fund2, licenseNumber: licenseNumber,
                    licenseExpiryDate: licenseExpiryDate, versionNumber: versionNumber, passportNumber: passportNumber,
                    passportExpiryDate: passportExpiryDate, countryOfIssue: countryOfIssue, typeOfID: typeOfID, typeExpiryDate: typeExpiryDate,
                    typeCountryOfIssue: typeCountryOfIssue, myFile: myFile, countryOfResidence: countryOfResidence, irdNumber: irdNumber, citizenUS: citizenUS, isInvestor_idverified: investor_verify,
                    countryOfTaxResidence: countryOfTaxResidence, taxIdentityNumber: taxIdenityNumber, pir: pir,
                    companyIRDNumber: companyIRDNumber, sourceOfFunds: sourceOfFunds, detail: detail,
                    isCompanyListed: isCompanyListed, isCompanyGovernment: isCompanyGovernment, isCompanyNZPolice: isCompanyNZPolice,
                    isCompanyFinancialInstitution: isCompanyFinancialInstitution, isCompanyActivePassive: isCompanyActivePassive,
                    isCompanyUSCitizen: isCompanyUSCitizen, companyCountryOfTaxResidence: companyCountryOfTaxResidence, companyTaxIdentityNumber: companyTaxIdentityNumber,
                    companyReasonTIN: companyReasonTIN, bankName: bankName, nameOfAccount: nameOfAccount, accountNumber: accountNumber, step: step, status: status, reg_type: reg_type, reg_id: reg_id, countryTINList: countryTINList, moreInvestorList: moreInvestorArr};
//                alert(JSON.stringify(DataObj));
                swal({title: "",
                    text: "Application data being saved...  ",
                    type: "success",
                    timer: 3500,
                    showConfirmButton: false
                });
                console.log(JSON.stringify(DataObj));
                $.ajax({
                    type: 'POST',
                    url: './rest/groot/db/api/company-registeration',
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(DataObj),
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
//                        swal({
//                            title: "Complete",
//                            text: "Your application data has been saved. To resume your application, please see the email we have now sent you. ",
//                            type: "success",
//                            timer: 2500,
//                            showConfirmButton: false
//                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert(" inside error" + jqXHR);
                    }
                });
            };
        </script>
        <script>
            function tinfornext7() {
                var idx = parseInt($('#directors-index').val());
                if (directors.length > idx) {
                    if (typeof investors[idx] === 'object') {
                        setMoreInvestor(investors[idx]);
                    }
                    x = $('[name="step"]').val();
                    $("#step").attr("value", parseInt(x) + 1);
//                            var idx = parseInt($('#directors-index').val());
//                            $('#directors-index').val(idx);
                    current_director = directors[idx];
                    $('.curr-director').text(current_director.name);
                    $("#step8").show();
                    $("#step7").hide();
                } else {
                    x = $('[name="step"]').val();
                    $("#step").attr("value", parseInt(x) + 1);
                    $("#step12").show();
                    $("#step7").hide();
                }
            }
            function tinfornext11() {
                var idx = parseInt($('#directors-index').val());
                if (directors.length > idx) {
//                        alert(idx);
                    moreInvestorArr.push(getMoreInvestor(idx));
                }
                idx++;
                if (directors.length > idx) {
                    $('#directors-index').val(idx);
                    $('.removedata').val("");
//                        $('.removecheck').prop("checked", false);
                    if (typeof investors[idx] === 'object') {
                        setMoreInvestor(investors[idx]);
                    }
                    current_director = directors[idx];
                    $('.curr-director').text(current_director.name);
                    var new3 = document.getElementsByClassName("yes-new3")[0];
                    var clones = new3.getElementsByClassName("yes-new3-clone");
                    for (var i = 0; i < clones.length; i++) {
                        var clone = clones[i];
                        clone.parentNode.removeChild(clone);
                    }
                    $(".selectoption2").val('1').change();
                    x = $('[name="step"]').val();
                    $("#step").attr("value", parseInt(x) + 1);
                    $("#step8").show();
                    $("#step11").hide();
                } else {
                    x = $('[name="step"]').val();
                    $("#step").attr("value", parseInt(x) + 1);
                    $("#step12").show();
                    $("#step11").hide();
                }
            }
            function removetinspan() {
                $('.error').text('');
            }
        </script>
        <script>
            $('.bank_name').change(function () {
                //                alert('other');
                if ($('.bank_name option:selected').val() === 'Other') {
                    $(".toggal_other").show();
                } else {
                    $(".toggal_other").hide();
                }
            });

            $('.checkname').change(function () {
                console.log("daya");
                var root = $(this).closest('.closestcls');
                var name = $(this).val();
                var filename = name.split('\\').pop().split('/').pop();
                filename = filename.substring(0, filename.lastIndexOf('.'));
                console.log(name);
                root.find('.shownamedata').text("File: " + filename);
                root.find(".removefile").show();
            });
            function  removedatafile(ele) {
                var root = ele.closest('.closestcls');
                $(root).find('.shownamedata').text("");
                $(root).find('.checkname').val("");
                $(root).find(".removefile").hide();
            }
            $(".Occupation").click(function () {
                var OccupationOption = $('.Occupation').val();
//                alert(OccupationOption);
                if (OccupationOption === "other") {
                    $(".selectOcc").show();
                    $(".otherOcc").show();
                } else {
                    $(".selectOcc").hide();
                    $(".otherOcc").show();
                }
            });
        </script>

        <script>
            $(".verify-dl").click(function () {
                var x = $(this).closest(".content-section");
                var index = x.find(".Id_Type option:selected").val();
                var s = $(x).find('.which_container').val();
                var url = '';
                var firstName = '';
                var middleName = '';
                var lastName = '';
                var Date_of_Birth = '';
                if (index === "1") {
                    url = './rest/groot/db/api/dl-verification';
                    firstName = x.find('input[name=licence_first_name]').val();
                    middleName = x.find('input[name=licence_middle_name]').val();
                    lastName = x.find('input[name="licence_last_name"]').val();
                    var License_number = x.find('input[name="license_number"]').val();
//                    var License_number = $('.License_number').val();
                    var licence_expiry_Date = x.find($('.lic_expiry_Date')).val();
                    var licence_verson_number = x.find($('.lic_verson_number')).val();
                    if (firstName === "") {

                        if (s === "#Date_of_Birth") {

                            $("#error_licence_first_name").text("This field is required ");
                            return false;
                        } else if (s === "#more-investor-dob") {
                            $("#more_error_licence_first_name").text("This field is required ");
                            return false;
                        }
                    } else if (License_number === "") {
                        if (s === "#Date_of_Birth") {
                            $("#spanlicenseNumber").text("This field is required ");
                            return false;
                        } else if (s === "#more-investor-dob") {
                            $("#more-investor-licenseNumber-error").text("This field is required ");
                            return false;
                        }
                    } else if (licence_verson_number === "") {
                        if (s === "#Date_of_Birth") {
                            $("#spanVersionnumber").text("This field is required ");
                            return false;
                        } else if (s === "#more-investor-dob") {
                            $("#more-investor-versionNumber-error").text("This field is required ");
                            return false;
                        }

                    } else if (lastName === "") {
                        if (s === "#Date_of_Birth") {
                            $("#error_licence_last_name").text("This field is required ");
                            return false;
                        } else if (s === "#more-investor-dob") {
                            $("#more_error_licence_last_name").text("This field is required ");
                            return false;
                        }
                    }

                } else if (index === "2") {
                    url = './rest/groot/db/api/pp-verification';
                    firstName = x.find('input[name=passport_first_name]').val();
                    middleName = x.find('input[name=passport_middle_name]').val();
                    lastName = x.find('input[name="passport_last_name"]').val();
                    var passport_number = x.find('input[name="passport_number"]').val();
//                    var passport_expiry =  "2017-10-10";
                    var passport_expiry = x.find('.pass_expiry').val();
                    var dob2 = x.find('.pass_expiry').val();
                    dob2 = dob2.trim();
                    passport_number = passport_number.trim();
                    if (firstName === "") {
                        if (s === "#Date_of_Birth") {

                            $("#error_passport_first_name").text("This field is required ");
                            return false;
                        } else if (s === "#more-investor-dob") {
                            $("#more_error_passport_first_name").text("This field is required ");
                            return false;
                        }
                    } else if (passport_number === "") {
                        if (s === "#Date_of_Birth") {

                            $("#spanPassportnumber").text("This field is required ");
                            return false;
                        } else if (s === "#more-investor-dob") {
                            $("#more-investor-passportNumber-error").text("This field is required ");
                            return false;
                        }

                    } else if (passport_expiry === "") {
                        if (s === "#Date_of_Birth") {

                            $("#spanPExpirydate").text("This field is required ");
                            return false;
                        } else if (s === "#more-investor-dob") {
                            $("#more-investor-passportExpiryDate-error").text("This field is required ");
                            return false;
                        }

                    } else if (lastName === "") {
                        if (s === "#Date_of_Birth") {

                            $("#error_passport_last_name").text("This field is required ");
                            return false;
                        } else if (s === "#more-investor-dob") {
                            $("#more_error_passport_last_name").text("This field is required ");
                            return false;
                        }
                    }
                }
                Date_of_Birth = $(s).val();
//                alert(Date_of_Birth);
                var DataObj = {license_number: License_number, licence_expiry_Date: licence_expiry_Date,
                    licence_verson_number: licence_verson_number, date_of_Birth: Date_of_Birth, firstName: firstName,
                    middleName: middleName, lastName: lastName, passport_number: passport_number, passport_expiry: passport_expiry};
                console.log(JSON.stringify(DataObj));
//                alert(JSON.stringify(DataObj));
                $.ajax({
                    type: 'POST',
                    url: url,
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(DataObj),
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        var obj = JSON.parse(data);
                        if (index === "1") {
                            if (obj.driversLicence.verified) {
                                swal({
                                    title: "Success",
                                    text: "Data is Verified.",
                                    type: "info",
                                    timer: 3500,
                                    showConfirmButton: true
                                });
                            } else {
//                                alert("wrong data");
                            }
                        } else if (index === "2") {
                            if (obj.passport.verified) {
                                swal({
                                    title: "Success",
                                    text: "Data is Verified.",
                                    type: "info",
                                    timer: 3500,
                                    showConfirmButton: true
                                });
                            } else {
//                                alert("wrong data");
                            }

                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert(" inside error" + jqXHR);
                    }
                });
            });
            $('.country').click(function () {
                $('.error').text("");
            });
            function changecoutry(ele) {
                ele.getElementsByClassName('error')[0].innerHTML = "";
            }
        </script>
        <script>
            var data;
            var token;
            $(document).ready(function () {
                $.ajax({
                    url: 'https://toyfjejt90.execute-api.ap-southeast-2.amazonaws.com/prod/companysearch',
                    headers: {"Content-Type": 'application/json'},
                    method: 'GET',
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        token = data;
                    }, error: function (jqXHR, textStatus, errorThrown) {

                    }
                });
            });
            $('.search').keyup(function () {
                data = $('.search').val();
                if (data.trim() === "") {
                    $('#detectcompany').html('');
                    $("#detectcompany").hide();
                }
                //             $('.companyName').html("");
                //            $('#showcode').html("");
                getData();
            });
            function getData() {
                //            $('#resultdata').html(' <h4>ShareHolder</h4>');
                //            $('#resultdata1').html('<h4>Director</h4>');
                //            $('#resultdata2').html('<h4>Beneficial Owner</h4>');
                $.ajax({
                    url: 'https://api.business.govt.nz/services/v4/nzbn/entities?search-term=' + data,
                    headers: {
                        'Authorization': `Bearer ` + token
                    },
                    method: 'GET',
                    success: function (data, textStatus, jqXHR) {
                        $('#detectcompany').html('');
                        $('.removedirector').remove();
                        $('.fname').val("");
                        var obj = JSON.stringify(data);
                        console.log(obj);
                        obj = JSON.parse(obj);

                        $("#detectcompany").show();
                        for (var i = 0; i < obj.items.length; i++) {
                            var div = document.createElement('div');
                            div.classList.add("checknear");
                            document.getElementById('detectcompany').appendChild(div);
                            var item = obj.items[i];
                            console.log(item.entityName);

                            var a = document.createElement('a');

                            a.innerHTML = item.entityName;
                            a.classList.add("clickcompany");
                            a.setAttribute("href", "javascript:void(0)");
                            a.setAttribute("onclick", "clickcompany(this)");
                            div.appendChild(a);
                            var span = document.createElement('span');
                            var code = item.nzbn;
                            span.innerHTML = code;
                            span.classList.add("spancode");
                            span.style.display = "none";
                            div.appendChild(span);


                        }
                    }, error: function (jqXHR, textStatus, errorThrown) {

                    }
                });
            }
            function clickcompany(ele) {
                $('#detectcompany').html('');
                $('#detectcompany').hide();
                var code = $(ele).closest('.checknear').find('.spancode').text();
                var compoanyname = $(ele).text();
                $('.search').val(compoanyname);
                getdirector(code);
                var link = 'https://api.business.govt.nz/services/v4/nzbn/entities/' + code;
                $.ajax({
                    url: link,
                    headers: {
                        'Authorization': `Bearer ` + token
                    },
                    method: 'GET',
                    success: function (data, textStatus, jqXHR) {
                        var obj = JSON.stringify(data);
                        console.log(obj);
                        obj = JSON.parse(obj);
                        var date = new Date(obj.registrationDate);
                        var dd = String(date.getDate()).padStart(2, '0');
                        var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = date.getFullYear();
                        date = mm + '/' + dd + '/' + yyyy;
                        $('#Date_of_incorporation').val(date);
                        $('#registrationNumber').val(obj.nzbn);
                        var address1 = obj.addresses.addressList[0].address1;
                        var address2 = obj.addresses.addressList[0].address2;
                        var address3 = obj.addresses.addressList[0].address3;
                        var address4 = obj.addresses.addressList[0].address4;
                        $('#companyAddress').val(address1 + " " + address2 + " " + address3 + " " + address4);
                        $('#postalAddress').val(address1 + " " + address2 + " " + address3 + " " + address4);
                        $('#Country_of_incorporation').val(" New Zealand");
                        for (var i = 0; i < obj.roles.length; i++) {
                            checkfirstdir++;
                            var director = obj.roles[i];
                            var fullName;
                            if (director.rolePerson.middleNames === null || typeof director.rolePerson.middleNames === "null") {
                                var firstName = director.rolePerson.firstName;
                                var lastName = director.rolePerson.lastName;
                                fullName = firstName + " " + lastName;
                            } else {
                                var firstName = director.rolePerson.firstName;
                                var middleNames = director.rolePerson.middleNames;
                                var lastName = director.rolePerson.lastName;
                                fullName = firstName + " " + middleNames + " " + lastName;
                            }
//                            directorArr.push(firstName + " " + middleNames + " " + lastName);

                            if (checkfirstdir === 1) {
                                $('.fname').val(fullName);
                            } else {
                                $(".director-section").append("<div class='row director-add removedirector'><div class='col-sm-4'><label class='label_input' style='text-align:left'>Full name of trustee/beneficial owner</label></div><div class='col-sm-2'><select class='selectoption option-add' id=''><option value='Appointment'>Mr</option><option value='Interview'>Mrs</option><option value='Regarding a post'>Miss</option><option id='other' value='Other'>Master</option></select></div><div class='col-sm-4'><input type='text' class='fname input-field' id='fname' name='fullName' value='" + fullName + "' placeholder='Enter full name' onkeyup='removespan();'><span class='error spanfname' id='spanfname'></span><input type='hidden' class='cls-me' name='me' value='N'></div><div class='col-sm-2 this-space'><a href='javascript:void(0)' class='this-btn' onclick='addbtnfn(this);'>This is me</a></div></div>");
                            }
                        }


                    }, error: function (jqXHR, textStatus, errorThrown) {


                    }
                });

            }

            function getdirector(code) {

                var link = 'https://api.business.govt.nz/services/v4/nzbn/entities/' + code + '/company-details';
                $.ajax({
                    url: link,
                    headers: {
                        'Authorization': `Bearer ` + token
                    },
                    method: 'GET',
                    success: function (data, textStatus, jqXHR) {
                        var obj = JSON.stringify(data);
                        console.log(obj);
                        obj = JSON.parse(obj);
                        var numberOfShares = obj.shareholding.numberOfShares;
                        for (var i = 0; i < obj.shareholding.shareAllocation.length; i++) {
                            var shareAllocation = obj.shareholding.shareAllocation[i];
                            for (var j = 0; j < shareAllocation.shareholder.length; j++) {
                                var shareholder = shareAllocation.shareholder[j];
                                var allocation = shareAllocation.allocation;
                                var percentage = parseInt(allocation) / parseInt(numberOfShares) * 100;
                                if (percentage > 24) {
                                    var fullName = shareholder.individualShareholder.fullName;
                                    if (typeof fullName !== 'undefined') {
                                        checkfirstdir++;
                                        if (checkfirstdir === 1) {
                                            $('.fname').val(fullName);
                                        } else {
                                            $(".director-section").append("<div class='row director-add removedirector'><div class='col-sm-4'><label class='label_input' style='text-align:left'>Full name of trustee/beneficial owner</label></div><div class='col-sm-2'><select class='selectoption option-add' id=''><option value='Appointment'>Mr</option><option value='Interview'>Mrs</option><option value='Regarding a post'>Miss</option><option id='other' value='Other'>Master</option></select></div><div class='col-sm-4'><input type='text' class='fname input-field' id='fname' name='fullName' value='" + fullName + "' placeholder='Enter full name' onkeyup='removespan();'><span class='error spanfname' id='spanfname'></span><input type='hidden' class='cls-me' name='me' value='N'></div><div class='col-sm-2 this-space'><a href='javascript:void(0)' class='this-btn' onclick='addbtnfn(this);'>This is me</a></div></div>");
                                        }
                                    }
                                }
                            }
                        }
                    }, error: function (jqXHR, textStatus, errorThrown) {

                    }
                });

            }
        </script>
        <script>
            $('.senderType').click(function () {
                //                var mNo = $("#mobileNo3").val();
                //                var cCo = $("#countryCode3").val();
                var mNo = $('.mobile_number1').val();
                var cCo = $('.mobile_country_code').val();
                var sTy = $('.senderType:checked').val();
                cCo = cCo.replace(/\+/g, "");
                var url = 'https://jj1stbnzb3.execute-api.ap-southeast-2.amazonaws.com/otp/generateotp?pn=' + mNo + '&cc=' + cCo + '&sTy=' + sTy;
                //document.getElementById('error-loader').style.display = 'block';
                document.getElementById('error-generateOtp').innerHTML = "";
                $.ajax({
                    url: url,
                    type: 'GET',
                    async: true,
                    dataType: "json",
                    success: function (data) {
                        $('#error-generateOtp').html("Please check your Inbox");
                        $('#otp-block').show();
                        //            document.getElementById('error-loader').style.display = 'none';
                        $('input:radio[name="senderType"]').prop("checked", false);
                    },
                    error: function (e) {
                        var mobileNo = document.getElementsByClassName('mobile_number1');
                        var result = true;
                        $('#otp-block').show();
                        if (mobileNo.value === '') {
                            //document.getElementById('error-loader').style.display = 'none';
                            //                 $('#error-generateOtp').text("Mobile number is required.");
                            $('input:radio[name="senderType"]').prop("checked", false);
                            result = result && false;
                        } else {
                            //document.getElementById('error-loader').style.display = 'none';

                            //                 $('input:radio[name="senderType"]').prop("checked", false);
                            result = result && false;
                        }
                        return result;
                    }
                });
                return sTy;
            });
        </script>
        <script>

            function validateOTP(mNo, cCo, otp) {
                var url = 'https://jj1stbnzb3.execute-api.ap-southeast-2.amazonaws.com/otp/verifyotp?pn=' + mNo + '&cc=' + cCo + '&code=' + otp;
                $.ajax({
                    url: url,
                    type: 'POST',
                    async: true,
                    dataType: "json",
                    success: function (data) {
                        console.log(" sucess" + JSON.stringify(data.status));
                        if (data.status === "approved") {
                            $('.spanverification').html("Sucess ");

                        } else {
                            $('.spanverification').html("invalid code");

                        }
                    },
                    error: function (e) {
//                            alert();
//                            $('.msg').html("Wrong OTP");
//                        alert("otp failed" + JSON.stringify(e));
                    }
                });
            }
        </script>

        <!--        <script>
                    function autocom(arr) {
                        /*the autocomplete function takes two arguments,
                         the text field element and an array of possible autocompleted values:*/
                        var currentFocus;
                        /*execute a function when someone writes in the text field:*/
                        document.getElementsById("fname").addEventListener("input", function (e) {
                            var a, b, i, val = this.value;
                            /*close any already open lists of autocompleted values*/
                            closeAllLists();
                            if (!val) {
                                return false;
                            }
                            currentFocus = -1;
                            /*create a DIV element that will contain the items (values):*/
                            a = document.createElement("DIV");
                            a.setAttribute("id", this.id + "autocomplete-list");
                            a.setAttribute("class", "autocomplete-items");
                            /*append the DIV element as a child of the autocomplete container:*/
                            this.parentNode.appendChild(a);
                            /*for each item in the array...*/
                            for (i = 0; i < arr.length; i++) {
                                /*check if the item starts with the same letters as the text field value:*/
                                if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                                    /*create a DIV element for each matching element:*/
                                    b = document.createElement("DIV");
                                    /*make the matching letters bold:*/
                                    b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                                    b.innerHTML += arr[i].substr(val.length);
                                    /*insert a input field that will hold the current array item's value:*/
                                    b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                                    /*execute a function when someone clicks on the item value (DIV element):*/
                                    b.addEventListener("click", function (e) {
                                        /*insert the value for the autocomplete text field:*/
                                        document.getElementById("fname").value = this.getElementsByTagName("input")[0].value;
                                        /*close the list of autocompleted values,
                                         (or any other open lists of autocompleted values:*/
                                        closeAllLists();
                                    });
                                    a.appendChild(b);
                                }
                            }
                        });
                        /*execute a function presses a key on the keyboard:*/
                        document.getElementById("fname").addEventListener("keydown", function (e) {
                            var x = document.getElementById(this.id + "autocomplete-list");
                            if (x)
                                x = x.getElementsByTagName("div");
                            if (e.keyCode == 40) {
                                /*If the arrow DOWN key is pressed,
                                 increase the currentFocus variable:*/
                                currentFocus++;
                                /*and and make the current item more visible:*/
                                addActive(x);
                            } else if (e.keyCode == 38) { //up
                                /*If the arrow UP key is pressed,
                                 decrease the currentFocus variable:*/
                                currentFocus--;
                                /*and and make the current item more visible:*/
                                addActive(x);
                            } else if (e.keyCode == 13) {
                                /*If the ENTER key is pressed, prevent the form from being submitted,*/
                                e.preventDefault();
                                if (currentFocus > -1) {
                                    /*and simulate a click on the "active" item:*/
                                    if (x)
                                        x[currentFocus].click();
                                }
                            }
                        });
                        function addActive(x) {
                            /*a function to classify an item as "active":*/
                            if (!x)
                                return false;
                            /*start by removing the "active" class on all items:*/
                            removeActive(x);
                            if (currentFocus >= x.length)
                                currentFocus = 0;
                            if (currentFocus < 0)
                                currentFocus = (x.length - 1);
                            /*add class "autocomplete-active":*/
                            x[currentFocus].classList.add("autocomplete-active");
                        }
                        function removeActive(x) {
                            /*a function to remove the "active" class from all autocomplete items:*/
                            for (var i = 0; i < x.length; i++) {
                                x[i].classList.remove("autocomplete-active");
                            }
                        }
                        function closeAllLists(elmnt) {
                            /*close all autocomplete lists in the document,
                             except the one passed as an argument:*/
                            var x = document.getElementsByClassName("autocomplete-items");
                            for (var i = 0; i < x.length; i++) {
                                if (elmnt != x[i] && elmnt != document.getElementById("fname")) {
                                    x[i].parentNode.removeChild(x[i]);
                                }
                            }
                        }
                        /*execute a function when someone clicks in the document:*/
                        document.addEventListener("click", function (e) {
                            closeAllLists(e.target);
                        });
                    }
        
        
                </script>-->

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKuUGMz8-xYyc6t1YWUhygrrkB4-WAFeY&libraries=places&callback=initAutocomplete"
        async defer></script>
    </body>
</html>														