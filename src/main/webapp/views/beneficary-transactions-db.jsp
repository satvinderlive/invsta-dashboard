
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>westpac</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="http://backoffice.invsta.io/mint/images/mint.png" rel="shortcut icon"/>
        <link href="http://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <!--<link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>-->
        <link href="http://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>
        <!--<link href="http://backoffice.invsta.io/pocv/resources/css/main.css?version=3.5.1" rel="stylesheet"/>-->
        <link href="http://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link href="./resources/css/newcss/new-custom.css" rel="stylesheet"/>
        <link href="./resources/css/custom.css" rel="stylesheet"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.css">
        <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.css">
        <!--  <link href="resources/css/osfonts.css" rel="stylesheet"> -->

    </head>
    <body>
         <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                             <a href="./home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Transactions</a>
                        </li>
<!--                        <li class="breadcrumb-item">
                            <span></span>
                        </li>-->
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">                                  
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <div class="element-actions">
                                        </div>
                                        <h6 class="element-header">
                                           CASH & TRANSACTIONS 
                                            <!--<img src="https://s3-ap-southeast-2.amazonaws.com/invsta.com/public_resources/img/info-icon.png" style="vertical-align:text-top">-->
                                        </h6>
                                        <div class="element-content">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">
                                                            WALLET BALANCE
                                                        </div>
                                                        <div class="value">
                                                           US$ 51,477.52

                                                        </div>
                                                        <div class="trending trending-up">
                                                            <!--<span>12%</span><i class="os-icon os-icon-arrow-up2"></i>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">
                                                           PENDING INVESTMENT AMOUNT
                                                        </div>
                                                        <div class="value">
                                                          US$ 10,000.00

                                                        </div>
                                                        <div class="trending trending-down-basic">
                                                            <!--<span>12%</span><i class="os-icon os-icon-arrow-2-down"></i>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">
                                                           PENDING WITHDRAWAL AMOUNT
                                                        </div>
                                                        <div class="value currentBalance">
                                                            US$ 0.00

                                                        </div>
                                                        <div class="trending trending-down-basic">
                                                            <!--<span>9%</span><i class="os-icon os-icon-graph-down"></i>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                       <div class="col-sm-4">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">
                                                            AVAILABLE WALLET BALANCE
                                                        </div>
                                                        <div class="value currentBalance">
                                                            US$ 0.00

                                                        </div>
                                                        <div class="trending trending-down-basic">
                                                            <!--<span>9%</span><i class="os-icon os-icon-graph-down"></i>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                       <div class="col-sm-4">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">
                                                           LAST TRANSACTION
                                                        </div>
                                                        <div class="value currentBalance">
                                                            US$ 0.00

                                                        </div>
                                                        <div class="trending trending-down-basic">
                                                            <!--<span>9%</span><i class="os-icon os-icon-graph-down"></i>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                       <div class="col-sm-4">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">
                                                           ACCOUNT STATUS
                                                        </div>
                                                        <div class="value currentBalance">
                                                           Active

                                                        </div>
                                                        <div class="trending trending-down-basic">
                                                            <!--<span>9%</span><i class="os-icon os-icon-graph-down"></i>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-12 invst-option">
                                    <div class="element-wrapper"> 
                                                    <h6 class="element-header">
                                            Completed Transactions
                                            <!--<img src="https://s3-ap-southeast-2.amazonaws.com/invsta.com/public_resources/img/info-icon.png" style="vertical-align:text-top">-->
                                        </h6>
                                    
                                    <div class="card mt-3 tab-card">
                                        <!-- <div class="card-header tab-card-header">
                                                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                                        <li class="nav-item">
                                                                <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Funds</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Direct Companies</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">New Offers</a>
                                                        </li>
                                                </ul>
                                        </div> -->
                                        <table id="bene-transactions-table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="fasle" data-show-pagination-switch="fasle" data-show-refresh="false" data-key-events="fasle" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                               data-cookie-id-table="saveId" data-show-export="fasle" data-click-to-select="fasle" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <th data-field="id" data-checkbox="true"></th>
                                                    <!--<th data-field="Id" ><span >Id</span></th>-->
                                                    <!--<th data-field="InvestmentCode" ><span>Investment Code</span></th>-->
                                                    <th data-field="EffectiveDate" ><span>Effective Date</span></th>
                                                    <!--<th data-field="Type" ><span>Type</span></th>-->
                                                    <th data-field="TypeDisplayName"><span>Type Display Name</span></th>
                                                    <th data-field="PortfolioName"><span>Portfolio Name</span></th>
                                                    <th data-field="TransactionTypeDescription" ><span>Transaction Type Description</span></th>
                                                    <th data-field="SubType" ><span>Sub Type</span></th>
                                                    <th data-field="Units" ><span>Units</span></th>
                                                    <th data-field="Price" ><span>Price</span></th>
                                                    <th data-field="Value" ><span>Value</span></th>
                                                    <th data-field="Tax" ><span>Tax</span></th>
                                                    <!--<th data-formatter="viewButtonFormatter">Action</th>-->
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                                                <div class="col-md-12 invst-option">
                                                   <div class="element-wrapper"> 
                                                    <h6 class="element-header">
                                            Pending Transactions
                                            <!--<img src="https://s3-ap-southeast-2.amazonaws.com/invsta.com/public_resources/img/info-icon.png" style="vertical-align:text-top">-->
                                        </h6>
                                    <div class="card mt-3 tab-card">
                                        <!-- <div class="card-header tab-card-header">
                                                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                                        <li class="nav-item">
                                                                <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Funds</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Direct Companies</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">New Offers</a>
                                                        </li>
                                                </ul>
                                        </div> -->
                                        <table id="bene-transactions-table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="fasle" data-show-pagination-switch="fasle" data-show-refresh="false" data-key-events="fasle" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                               data-cookie-id-table="saveId" data-show-export="fasle" data-click-to-select="fasle" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <th data-field="id" data-checkbox="true"></th>
                                                    <!--<th data-field="Id" ><span >Id</span></th>-->
                                                    <!--<th data-field="InvestmentCode" ><span>Investment Code</span></th>-->
                                                    <th data-field="EffectiveDate" ><span>Effective Date</span></th>
                                                    <!--<th data-field="Type" ><span>Type</span></th>-->
                                                    <th data-field="TypeDisplayName"><span>Type Display Name</span></th>
                                                    <th data-field="PortfolioName"><span>Portfolio Name</span></th>
                                                    <th data-field="TransactionTypeDescription" ><span>Transaction Type Description</span></th>
                                                    <th data-field="SubType" ><span>Sub Type</span></th>
                                                    <th data-field="Units" ><span>Units</span></th>
                                                    <th data-field="Price" ><span>Price</span></th>
                                                    <th data-field="Value" ><span>Value</span></th>
                                                    <th data-field="Tax" ><span>Tax</span></th>
                                                    <!--<th data-formatter="viewButtonFormatter">Action</th>-->
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="display-type"></div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <script src="http://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
    <!-- <script src="resources/bower_components/moment/moment.js" ></script>
    
    <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
    <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
    <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

    <script src="http://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
    <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
    <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
    <script src="http://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
    <script src="http://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
    <script src="http://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
    <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
    <script src="https://code.highcharts.com/highcharts.js" ></script> 
    <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
    <script src="https://code.highcharts.com/modules/cylinder.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.js"></script> 
<script type="text/javascript">
        $(window).load(function () {
            $(".loader").fadeOut("slow");
        });
    </script>
    <script>
        $(document).ready(function () {
            $.ajax({
                type: 'GET',
                url: './rest/groot/db/api/investment-transactions?id=${id}',
                headers: {"Content-Type": 'application/json'},
                success: function (data, textStatus, jqXHR) {
                    $(".loader").hide();
                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                    var transactions = obj.investmentTransactions;
                    $('#bene-transactions-table').bootstrapTable('load', transactions);
//                    var TotalItemCount = obj.investmentTransactions.TotalItemCount;
//                    var FieldTotals = obj.investmentTransactions.FieldTotals;
//                    var Units = FieldTotals.Units;
//                    var Value = FieldTotals.Value;
//                    var Cash = FieldTotals.Cash;
//                    var Tax = FieldTotals.Tax;
//                    var UnitsScale = FieldTotals.UnitsScale;
//                    var TotalContributions = FieldTotals.TotalContributions;
//                    var TotalWithdrawals = FieldTotals.TotalWithdrawals;
//                    var TotalTransferIn = FieldTotals.TotalTransferIn;
//                    var Total = FieldTotals.Total;
//                    setTransactionsStatus(TotalItemCount, Units, Value, Cash, Tax, UnitsScale, TotalContributions, TotalWithdrawals, TotalTransferIn, Total);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });


        });
    </script>

</body>
</html>