<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en" >
    <head>
        <title>My Farm</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="/resources/favicon.png" rel="shortcut icon"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="./resources/css/signup/style.css">
        <link rel="stylesheet" href="./resources/css/signup/custom.css">
        <link rel="icon" href="./resources/images/invsta-color-logo.png" type="image/gif" sizes="16x16">
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <link rel="stylesheet" href="./resources/css/signup/intlTelInput.css">
        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css'>
        <link href="./resources/css/sweetalert.css" rel="stylesheet" />
        <style>
            .logo {
                width: 20%;
            }
            .intl-tel-input .flag-dropdown .selected-flag .down-arrow {
                top: 6px;
                position: relative;
                left: 20px;
                width: 0px;
                height: 0;
                border-left: 3px solid transparent;
                border-right: 3px solid transparent;
                border-top: 6px solid black;
            }
            .output .row.director-adds input#Date_of_incorporation {
                margin: 0;
            }
            div.nonResident .intl-tel-input {
                float: left;
                width: 100%;
            }
            .output .down-arrow.down-arrow-new {
                left: 224px !important;
            }
            .output .selected-flag {
                width: 242px !important;
            }
            .row.director-adds .col-sm-2.this-space {
                position: absolute;
                margin-top: 28px;
            }
            .col-sm-6.closestcls.ss i.far.fa-times-circle {
                position: relative;
                left: 5px;
                top: -1px;
            }
            .col-sm-6.closestcls.ss {
                width: 302px !important;
                padding: 0;
                margin: 0;
                max-width: 100%;
            }
            .col-sm-6.closestcls i.far.fa-times-circle {
                position: relative;
                left: 5px;
                top: 0px
            }
            .row.director-adds.removedirector {
                margin-top: 14px;
                margin-bottom: -22px;
            }
            .attach-btn3:after {
                position: absolute;
                content: "Attach letter";
                color: #fff;
                background: linear-gradient(to right, #9b6f52, #b39d79);
                width: 100%;
                height: 32px;
                font-weight: 400;
                text-align: center;
                left: 0px;
                padding-top: 5px;
                border-radius: 4px;
            }
            .addss {
                width: 100%;
            }
            div#rightDiv .addss {
                width: 100%;
                margin-bottom: 8px;
            }
            .this-space {
                display: inline;
                padding: 0;
            }
            div#rightDiv {
                width: 100%;
            }
            select#select-occupation {
                margin: 0;
            }
            .units {
                text-align: justify;
            }
            .units h5 {
                font-size: 13px;
                color: #334152;
                text-align: left;
                margin-bottom: 24px;
                font-weight: 100;
            }
            a.this-btn.this-is-me.check-this-btn.one.colorchnge.highlight {
                background: #a98a69;
                color:#fff;
            }
            .units h4 {
                font-size: 18px;
                margin-bottom: 21px;
                margin-top: 11px;
            }
            button#clear-signature {
                border: none;
                border-radius: 4px;
                padding: 7px 22px;
                font-size: 13px;
                background: linear-gradient(to right, #9b6f52, #b39d79);
                margin-top: 20px;
                color: #fff;
                cursor: pointer;
            }

            h4.firstname {
                font-size: 14px;
                padding: 0;
                margin-top: 13px;
                text-align: justify;
            }
            a.this-btn.check-this-btn {
                font-size: 10px;
                color: #000;
                border: 1px solid #a37e5f;
                padding: 7px 17px;
                border-radius: 16px;
                background: transparent;
            }
            .col-sm-6.spacebtn {
                margin: 9px 0px;
            }
            a.this-btn.this-is-me.check-this-btn.one {
                margin: 0px 4px;
            }
            .col-sm-6.form-group.country-set.flag-drop.selects {
                z-index: 1111;
            }
            input.verify-dl {
                background: ba;
                background: #65b9ac!important;
                z-index: 999999999;
                color: #fff!important;
                max-width: 103px;
                height: 26px;
                padding: 2px!important;
                font-size: 11px!important;
                cursor: pointer;
            }
            input#certificate:after {
                content: "Attach Certificate of Exemption";
            }
            @media(max-width:575px){
                .this-space {
                    margin: 10px 0!important;
                }
            }
            div#detectcompany {
                position: absolute;
                background: white;
                z-index: 999;
                height: 163px;
                overflow: auto;
                border: 1px solid #ccc;
                padding: 4px 5px;
                border-top: 0;
                display: none;
            }

            .checknear {
                text-align: left;
                font-size: 12px;
                border-bottom: 1px solid #ccc;
                padding-bottom: 6px;
            }
            .colors {
                padding: 1em 0px;
                color: #fff;
                display: none;
            }
            .sign button {
                background: #a78565;
                border: none;
                color: white;
                padding: 3px 22px;
                border-radius: 11px;
                font-size: 14px;
                margin-top: 22px;
            }
            .col-sm-6.this-space.spacebtn.twobtn {
                display: flex;
                width: 444px;
            }
            a.this-btn {
                margin-left: -19px;
            }
            .addss.cloneName {
                margin-bottom: 0px;
            }
            .row.director-adds input {
                margin: 0 !important;
            }
            .row.director-adds {
                margin: 4px 0px;
            }
            .col-sm-6.emailadd {
                text-align: justify;
            }
            input#registrationNumber {
                width: 222px;
            }

            .col-sm-8.emailadd label {
                text-align: justify;
                font-size: 13px;
            }
            canvas#signature-pad {
                border: 1px solid grey;
            }
            .col-sm-8.emailadd {
                text-align: justify;
            }

            input#registrationNumber {}

            .col-sm-4.emailaddw {
                width: 283px;
            }
            input#companyName {
                margin: 0;
            }
            .autocomplete {
                position: relative;
                display: inline-block;
            }

            .autocomplete-items {
                position: absolute;
                border: 1px solid #d4d4d4;
                border-bottom: none;
                border-top: none;
                z-index: 99;
                /*position the autocomplete items to be the same width as the container:*/
                top: 100%;
                left: 0;
                right: 0;
            }
            button.director-btnmy {
                border: none;
                background: linear-gradient(to right, #2c94ec, #304bb7);
                color: #fff;
                margin: 16px 0px 0px;
                width: 182px;
                height: 47px;
                border-radius: 22px;
            }
            .autocomplete-items div {
                padding: 10px;
                cursor: pointer;
                background-color: #fff; 
                border-bottom: 1px solid #d4d4d4; 
            }
            .col-lg-12.units.new {
                padding: 0px 15px;
            }
            /*when hovering an item:*/
            .autocomplete-items div:hover {
                background-color: #e9e9e9; 
            }
            i.far.fa-times-circle {
                position: absolute;
                top: -35px;
                left: 430px;
                font-size: medium;
            }
            i.far.trusteeCross {
                position: relative;
                top: -27px;
                left: 55%;
                font-size: medium;
            }
            /*when navigating through the items using the arrow keys:*/
            .autocomplete-active {
                background-color: DodgerBlue !important; 
                color: #ffffff; 
            }
        </style>
    </head>
   <body  style="background-image: url('./resources/images/bckground.png')!important; background-size: cover !important;">
        <div class="container">
            <div class="loged_user signup-btn-west">
                <a href="./login?logout"><p>Already have an Account Login</p></a>
            </div>
            <div class="row">
                <div class="col-md-12 ">
                    <div class="content_body">
                        <form id="msform" name="msRegform" class="form-signup" action="/chelmer" method="POST" autocomplete="off">
                            <div class="w3-light-grey">
                                <!--<div id="myBar" class="progress-bar" style="width:0%"></div>-->
                            </div>
                            <div class="logo-w">
                                <a href="#"><img class="logo" alt="" src="./resources/images/logo.png" ></a>
                            </div>
                            <div class="save-new-btn">
                                <input type="hidden" name="register_type" id="register_type" value="TRUST_ACCOUNT">
                                <input type="hidden" name="step" id="step" value="1">
                                <button class=" saveExit director-btn all-btn-color save-font">Save & Exit</button>
                                <!--<span class="info-icon"><a href="#" data-toggle="tooltip" title="You can save and exit this application at any time by clicking on this Save & Exit button. We will then send you an email from which you can resume your application when you are ready. "><img src="./resources/images/i-icon.png"></a></span>-->
                                <div class="pulsating-circle4 pulsating-around6" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                    <span class="tooltiptext">You can save and exit this application at any time by clicking on this Save &amp; Exit button. We will then send you an email from which you can resume your application when you are ready.</span>
                                </div>
                            </div>
                            <fieldset id="step1">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please provide details of the Trust below. 
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Name of Trust 
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control input-field " id="companyName" value="${company.companyName}" name="companyName" required="required" placeholder="Enter Trust name" onkeyup="myFunction()" />
                                                <div id="detectcompany"></div>
                                                <span class="error" id="spanCompanyName"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Trust address 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <input type="text" class="form-control input-field " value="${company.companyAddress}" placeholder="Trust address"   id="trustAddress" >
                                                <span class="error" id="spanCompanyAddress"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Trust IRD number
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <input type="text" class="form-control input-field " value="${company.companyAddress}" placeholder="XXX-XXX-XXX" name="companyAddress" id="IRDNumber" onkeypress="checkird(this, event)" onkeyup="pasteIRRD(this, event)" autocomplete="off">
                                                <span class="error" id="spanCompanyAddress"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Withholding tax rate (RWT)
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select name="occupation" id="selecTaxRate" class="Occupation occupationOption selectoption  otherOcc  nameTitle form-group">
                                                    <option value="-Select-">–Select–</option>
                                                    <option value="1">10.5%</option>
                                                    <option value="2">17.5%</option>
                                                    <option value="3">28%</option>
                                                    <option value="4">30%</option>
                                                    <option value="5">33%</option>
                                                    <option value="yellow">Exempt</option>
                                                    <option value="blue">Non Resident</option>
                                                </select>
                                                <span class="error" id="spanCompanyAddress"></span>

                                            </div>
                                            <div class="col-sm-6 exemptBtn">
                                                <label class="label_input" style="text-align:left">

                                                </label>
                                            </div>
                                            <div class="col-sm-6 exemptBtn">
                                                <div class="col-sm-6 closestcls ss">
                                                    <input type="file" name="myFile" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" class="attach-btn2 checkname" id="certificate">
                                                    <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile" style="display: inline;"><i class="far fa-times-circle"></i></a>
                                                    <span class=" error error_attachbankfile"></span>
                                                </div>
                                            </div> 
                                            <div class="col-sm-6 nonResident">
                                                <label class="label_input" style="text-align:left">Resident Country</label>
                                            </div>
                                            <div class="nonResident col-sm-6 form-group country-set flag-drop selects">
                                                <div class="intl-tel-input"><div class="flag-dropdown f16"><div class="selected-flag"><div class="down-arrow down-arrow-new"></div></div><ul class="country-list hide"><li class="country preferred active highlight" data-dial-code="New Zealand" data-country-code="nz"><div class="flag nz"></div><span class="country-name">New Zealand</span></li><li class="divider"></li><li class="country" data-dial-code="Afghanistan" data-country-code="af"><div class="flag af"></div><span class="country-name">Afghanistan</span></li><li class="country" data-dial-code="Albania" data-country-code="al"><div class="flag al"></div><span class="country-name">Albania</span></li><li class="country" data-dial-code="Algeria" data-country-code="dz"><div class="flag dz"></div><span class="country-name">Algeria</span></li><li class="country" data-dial-code="American Samoa" data-country-code="as"><div class="flag as"></div><span class="country-name">American Samoa</span></li><li class="country" data-dial-code="Andorra" data-country-code="ad"><div class="flag ad"></div><span class="country-name">Andorra</span></li><li class="country" data-dial-code="Angola" data-country-code="ao"><div class="flag ao"></div><span class="country-name">Angola</span></li><li class="country" data-dial-code="Anguilla" data-country-code="ai"><div class="flag ai"></div><span class="country-name">Anguilla</span></li><li class="country" data-dial-code="Antigua and Barbuda" data-country-code="ag"><div class="flag ag"></div><span class="country-name">Antigua and Barbuda</span></li><li class="country" data-dial-code="Argentina" data-country-code="ar"><div class="flag ar"></div><span class="country-name">Argentina</span></li><li class="country" data-dial-code="Armenia" data-country-code="am"><div class="flag am"></div><span class="country-name">Armenia</span></li><li class="country" data-dial-code="Aruba" data-country-code="aw"><div class="flag aw"></div><span class="country-name">Aruba</span></li><li class="country" data-dial-code="Australia" data-country-code="au"><div class="flag au"></div><span class="country-name">Australia</span></li><li class="country" data-dial-code="Austria" data-country-code="at"><div class="flag at"></div><span class="country-name">Austria</span></li><li class="country" data-dial-code="Azerbaijan" data-country-code="az"><div class="flag az"></div><span class="country-name">Azerbaijan</span></li><li class="country" data-dial-code="Bahamas" data-country-code="bs"><div class="flag bs"></div><span class="country-name">Bahamas</span></li><li class="country" data-dial-code="Bahrain" data-country-code="bh"><div class="flag bh"></div><span class="country-name">Bahrain</span></li><li class="country" data-dial-code="Bangladesh" data-country-code="bd"><div class="flag bd"></div><span class="country-name">Bangladesh</span></li><li class="country" data-dial-code="Barbados" data-country-code="bb"><div class="flag bb"></div><span class="country-name">Barbados</span></li><li class="country" data-dial-code="Belarus" data-country-code="by"><div class="flag by"></div><span class="country-name">Belarus</span></li><li class="country" data-dial-code="Belgium" data-country-code="be"><div class="flag be"></div><span class="country-name">Belgium</span></li><li class="country" data-dial-code="Belize" data-country-code="bz"><div class="flag bz"></div><span class="country-name">Belize</span></li><li class="country" data-dial-code="Benin" data-country-code="bj"><div class="flag bj"></div><span class="country-name">Benin</span></li><li class="country" data-dial-code="Bermuda" data-country-code="bm"><div class="flag bm"></div><span class="country-name">Bermuda</span></li><li class="country" data-dial-code="Bhutan" data-country-code="bt"><div class="flag bt"></div><span class="country-name">Bhutan</span></li><li class="country" data-dial-code="Bolivia" data-country-code="bo"><div class="flag bo"></div><span class="country-name">Bolivia</span></li><li class="country" data-dial-code="Bosnia and Herzegovina" data-country-code="ba"><div class="flag ba"></div><span class="country-name">Bosnia and Herzegovina</span></li><li class="country" data-dial-code="Botswana" data-country-code="bw"><div class="flag bw"></div><span class="country-name">Botswana</span></li><li class="country" data-dial-code="Brazil" data-country-code="br"><div class="flag br"></div><span class="country-name">Brazil</span></li><li class="country" data-dial-code="Brunei Darussalam" data-country-code="bn"><div class="flag bn"></div><span class="country-name">Brunei Darussalam</span></li><li class="country" data-dial-code="Bulgaria" data-country-code="bg"><div class="flag bg"></div><span class="country-name">Bulgaria</span></li><li class="country" data-dial-code="Burkina Faso" data-country-code="bf"><div class="flag bf"></div><span class="country-name">Burkina Faso</span></li><li class="country" data-dial-code="Burundi" data-country-code="bi"><div class="flag bi"></div><span class="country-name">Burundi</span></li><li class="country" data-dial-code="Cambodia" data-country-code="kh"><div class="flag kh"></div><span class="country-name">Cambodia</span></li><li class="country" data-dial-code="Cameroon" data-country-code="cm"><div class="flag cm"></div><span class="country-name">Cameroon</span></li><li class="country" data-dial-code="Canada" data-country-code="ca"><div class="flag ca"></div><span class="country-name">Canada</span></li><li class="country" data-dial-code="Cape Verde" data-country-code="cv"><div class="flag cv"></div><span class="country-name">Cape Verde</span></li><li class="country" data-dial-code="Cayman Islands" data-country-code="ky"><div class="flag ky"></div><span class="country-name">Cayman Islands</span></li><li class="country" data-dial-code="Central African Republic" data-country-code="cf"><div class="flag cf"></div><span class="country-name">Central African Republic</span></li><li class="country" data-dial-code="Chad" data-country-code="td"><div class="flag td"></div><span class="country-name">Chad</span></li><li class="country" data-dial-code="Chile" data-country-code="cl"><div class="flag cl"></div><span class="country-name">Chile</span></li><li class="country" data-dial-code="China" data-country-code="cn"><div class="flag cn"></div><span class="country-name">China</span></li><li class="country" data-dial-code="Colombia" data-country-code="co"><div class="flag co"></div><span class="country-name">Colombia</span></li><li class="country" data-dial-code="Comoros" data-country-code="km"><div class="flag km"></div><span class="country-name">Comoros</span></li><li class="country" data-dial-code="Congo (DRC)" data-country-code="cd"><div class="flag cd"></div><span class="country-name">Congo (DRC)</span></li><li class="country" data-dial-code="Congo (Republic)" data-country-code="cg"><div class="flag cg"></div><span class="country-name">Congo (Republic)</span></li><li class="country" data-dial-code="Cook Islands" data-country-code="ck"><div class="flag ck"></div><span class="country-name">Cook Islands</span></li><li class="country" data-dial-code="Costa Rica" data-country-code="cr"><div class="flag cr"></div><span class="country-name">Costa Rica</span></li><li class="country" data-dial-code="CÃ´te d" ivoire'="" data-country-code="ci"><div class="flag ci"></div><span class="country-name">CÃ´te d'Ivoire</span></li><li class="country" data-dial-code="Croatia" data-country-code="hr"><div class="flag hr"></div><span class="country-name">Croatia</span></li><li class="country" data-dial-code="Cuba" data-country-code="cu"><div class="flag cu"></div><span class="country-name">Cuba</span></li><li class="country" data-dial-code="Cyprus" data-country-code="cy"><div class="flag cy"></div><span class="country-name">Cyprus</span></li><li class="country" data-dial-code="Czech Republic" data-country-code="cz"><div class="flag cz"></div><span class="country-name">Czech Republic</span></li><li class="country" data-dial-code="Denmark" data-country-code="dk"><div class="flag dk"></div><span class="country-name">Denmark</span></li><li class="country" data-dial-code="Djibouti" data-country-code="dj"><div class="flag dj"></div><span class="country-name">Djibouti</span></li><li class="country" data-dial-code="Dominica" data-country-code="dm"><div class="flag dm"></div><span class="country-name">Dominica</span></li><li class="country" data-dial-code="Dominican Republic" data-country-code="do"><div class="flag do"></div><span class="country-name">Dominican Republic</span></li><li class="country" data-dial-code="Ecuador" data-country-code="ec"><div class="flag ec"></div><span class="country-name">Ecuador</span></li><li class="country" data-dial-code="Egypt" data-country-code="eg"><div class="flag eg"></div><span class="country-name">Egypt</span></li><li class="country" data-dial-code="El Salvador" data-country-code="sv"><div class="flag sv"></div><span class="country-name">El Salvador</span></li><li class="country" data-dial-code="Equatorial Guinea" data-country-code="gq"><div class="flag gq"></div><span class="country-name">Equatorial Guinea</span></li><li class="country" data-dial-code="Eritrea" data-country-code="er"><div class="flag er"></div><span class="country-name">Eritrea</span></li><li class="country" data-dial-code="Estonia" data-country-code="ee"><div class="flag ee"></div><span class="country-name">Estonia</span></li><li class="country" data-dial-code="Ethiopia" data-country-code="et"><div class="flag et"></div><span class="country-name">Ethiopia</span></li><li class="country" data-dial-code="Faroe Islands" data-country-code="fo"><div class="flag fo"></div><span class="country-name">Faroe Islands</span></li><li class="country" data-dial-code="Fiji" data-country-code="fj"><div class="flag fj"></div><span class="country-name">Fiji</span></li><li class="country" data-dial-code="Finland" data-country-code="fi"><div class="flag fi"></div><span class="country-name">Finland</span></li><li class="country" data-dial-code="France" data-country-code="fr"><div class="flag fr"></div><span class="country-name">France</span></li><li class="country" data-dial-code="French Polynesia" data-country-code="pf"><div class="flag pf"></div><span class="country-name">French Polynesia</span></li><li class="country" data-dial-code="Gabon" data-country-code="ga"><div class="flag ga"></div><span class="country-name">Gabon</span></li><li class="country" data-dial-code="Gambia" data-country-code="gm"><div class="flag gm"></div><span class="country-name">Gambia</span></li><li class="country" data-dial-code="Georgia" data-country-code="ge"><div class="flag ge"></div><span class="country-name">Georgia</span></li><li class="country" data-dial-code="Germany" data-country-code="de"><div class="flag de"></div><span class="country-name">Germany</span></li><li class="country" data-dial-code="Ghana" data-country-code="gh"><div class="flag gh"></div><span class="country-name">Ghana</span></li><li class="country" data-dial-code="Gibraltar" data-country-code="gi"><div class="flag gi"></div><span class="country-name">Gibraltar</span></li><li class="country" data-dial-code="Greece" data-country-code="gr"><div class="flag gr"></div><span class="country-name">Greece</span></li><li class="country" data-dial-code="Greenland" data-country-code="gl"><div class="flag gl"></div><span class="country-name">Greenland</span></li><li class="country" data-dial-code="Grenada" data-country-code="gd"><div class="flag gd"></div><span class="country-name">Grenada</span></li><li class="country" data-dial-code="Guadeloupe" data-country-code="gp"><div class="flag gp"></div><span class="country-name">Guadeloupe</span></li><li class="country" data-dial-code="Guam" data-country-code="gu"><div class="flag gu"></div><span class="country-name">Guam</span></li><li class="country" data-dial-code="Guatemala" data-country-code="gt"><div class="flag gt"></div><span class="country-name">Guatemala</span></li><li class="country" data-dial-code="Guernsey" data-country-code="gg"><div class="flag gg"></div><span class="country-name">Guernsey</span></li><li class="country" data-dial-code="Guinea" data-country-code="gn"><div class="flag gn"></div><span class="country-name">Guinea</span></li><li class="country" data-dial-code="Guinea-Bissau" data-country-code="gw"><div class="flag gw"></div><span class="country-name">Guinea-Bissau</span></li><li class="country" data-dial-code="Guyana" data-country-code="gy"><div class="flag gy"></div><span class="country-name">Guyana</span></li><li class="country" data-dial-code="Haiti" data-country-code="ht"><div class="flag ht"></div><span class="country-name">Haiti</span></li><li class="country" data-dial-code="Honduras" data-country-code="hn"><div class="flag hn"></div><span class="country-name">Honduras</span></li><li class="country" data-dial-code="Hong Kong" data-country-code="hk"><div class="flag hk"></div><span class="country-name">Hong Kong</span></li><li class="country" data-dial-code="Hungary" data-country-code="hu"><div class="flag hu"></div><span class="country-name">Hungary</span></li><li class="country" data-dial-code="Iceland" data-country-code="is"><div class="flag is"></div><span class="country-name">Iceland</span></li><li class="country" data-dial-code="India" data-country-code="in"><div class="flag in"></div><span class="country-name">India</span></li><li class="country" data-dial-code="Indonesia" data-country-code="id"><div class="flag id"></div><span class="country-name">Indonesia</span></li><li class="country" data-dial-code="Iran" data-country-code="ir"><div class="flag ir"></div><span class="country-name">Iran</span></li><li class="country" data-dial-code="Iraq" data-country-code="iq"><div class="flag iq"></div><span class="country-name">Iraq</span></li><li class="country" data-dial-code="Ireland" data-country-code="ie"><div class="flag ie"></div><span class="country-name">Ireland</span></li><li class="country" data-dial-code="Isle of Man" data-country-code="im"><div class="flag im"></div><span class="country-name">Isle of Man</span></li><li class="country" data-dial-code="Israel" data-country-code="il"><div class="flag il"></div><span class="country-name">Israel</span></li><li class="country" data-dial-code="Italy" data-country-code="it"><div class="flag it"></div><span class="country-name">Italy</span></li><li class="country" data-dial-code="Jamaica" data-country-code="jm"><div class="flag jm"></div><span class="country-name">Jamaica</span></li><li class="country" data-dial-code="Japan" data-country-code="jp"><div class="flag jp"></div><span class="country-name">Japan</span></li><li class="country" data-dial-code="Jersey" data-country-code="je"><div class="flag je"></div><span class="country-name">Jersey</span></li><li class="country" data-dial-code="Jordan" data-country-code="jo"><div class="flag jo"></div><span class="country-name">Jordan</span></li><li class="country" data-dial-code="Kazakhstan" data-country-code="kz"><div class="flag kz"></div><span class="country-name">Kazakhstan</span></li><li class="country" data-dial-code="Kenya" data-country-code="ke"><div class="flag ke"></div><span class="country-name">Kenya</span></li><li class="country" data-dial-code="Kiribati" data-country-code="ki"><div class="flag ki"></div><span class="country-name">Kiribati</span></li><li class="country" data-dial-code="Kuwait" data-country-code="kw"><div class="flag kw"></div><span class="country-name">Kuwait</span></li><li class="country" data-dial-code="Kyrgyzstan" data-country-code="kg"><div class="flag kg"></div><span class="country-name">Kyrgyzstan</span></li><li class="country" data-dial-code="Laos" data-country-code="la"><div class="flag la"></div><span class="country-name">Laos</span></li><li class="country" data-dial-code="Latvia" data-country-code="lv"><div class="flag lv"></div><span class="country-name">Latvia</span></li><li class="country" data-dial-code="Lebanon" data-country-code="lb"><div class="flag lb"></div><span class="country-name">Lebanon</span></li><li class="country" data-dial-code="Lesotho" data-country-code="ls"><div class="flag ls"></div><span class="country-name">Lesotho</span></li><li class="country" data-dial-code="Liberia" data-country-code="lr"><div class="flag lr"></div><span class="country-name">Liberia</span></li><li class="country" data-dial-code="Libya" data-country-code="ly"><div class="flag ly"></div><span class="country-name">Libya</span></li><li class="country" data-dial-code="Liechtenstein" data-country-code="li"><div class="flag li"></div><span class="country-name">Liechtenstein</span></li><li class="country" data-dial-code="Lithuania" data-country-code="lt"><div class="flag lt"></div><span class="country-name">Lithuania</span></li><li class="country" data-dial-code="Luxembourg" data-country-code="lu"><div class="flag lu"></div><span class="country-name">Luxembourg</span></li><li class="country" data-dial-code="Macao" data-country-code="mo"><div class="flag mo"></div><span class="country-name">Macao</span></li><li class="country" data-dial-code="Macedonia" data-country-code="mk"><div class="flag mk"></div><span class="country-name">Macedonia</span></li><li class="country" data-dial-code="Madagascar" data-country-code="mg"><div class="flag mg"></div><span class="country-name">Madagascar</span></li><li class="country" data-dial-code="Malawi" data-country-code="mw"><div class="flag mw"></div><span class="country-name">Malawi</span></li><li class="country" data-dial-code="Malaysia" data-country-code="my"><div class="flag my"></div><span class="country-name">Malaysia</span></li><li class="country" data-dial-code="Maldives" data-country-code="mv"><div class="flag mv"></div><span class="country-name">Maldives</span></li><li class="country" data-dial-code="Mali" data-country-code="ml"><div class="flag ml"></div><span class="country-name">Mali</span></li><li class="country" data-dial-code="Malta" data-country-code="mt"><div class="flag mt"></div><span class="country-name">Malta</span></li><li class="country" data-dial-code="Marshall Islands" data-country-code="mh"><div class="flag mh"></div><span class="country-name">Marshall Islands</span></li><li class="country" data-dial-code="Martinique" data-country-code="mq"><div class="flag mq"></div><span class="country-name">Martinique</span></li><li class="country" data-dial-code="Mauritania" data-country-code="mr"><div class="flag mr"></div><span class="country-name">Mauritania</span></li><li class="country" data-dial-code="Mauritius" data-country-code="mu"><div class="flag mu"></div><span class="country-name">Mauritius</span></li><li class="country" data-dial-code="Mexico" data-country-code="mx"><div class="flag mx"></div><span class="country-name">Mexico</span></li><li class="country" data-dial-code="Micronesia" data-country-code="fm"><div class="flag fm"></div><span class="country-name">Micronesia</span></li><li class="country" data-dial-code="Moldova" data-country-code="md"><div class="flag md"></div><span class="country-name">Moldova</span></li><li class="country" data-dial-code="Monaco" data-country-code="mc"><div class="flag mc"></div><span class="country-name">Monaco</span></li><li class="country" data-dial-code="Mongolia" data-country-code="mn"><div class="flag mn"></div><span class="country-name">Mongolia</span></li><li class="country" data-dial-code="Montenegro" data-country-code="me"><div class="flag me"></div><span class="country-name">Montenegro</span></li><li class="country" data-dial-code="Montserrat" data-country-code="ms"><div class="flag ms"></div><span class="country-name">Montserrat</span></li><li class="country" data-dial-code="Morocco" data-country-code="ma"><div class="flag ma"></div><span class="country-name">Morocco</span></li><li class="country" data-dial-code="Mozambique" data-country-code="mz"><div class="flag mz"></div><span class="country-name">Mozambique</span></li><li class="country" data-dial-code="Myanmar (Burma)" data-country-code="mm"><div class="flag mm"></div><span class="country-name">Myanmar (Burma)</span></li><li class="country" data-dial-code="Namibia" data-country-code="na"><div class="flag na"></div><span class="country-name">Namibia</span></li><li class="country" data-dial-code="Nauru" data-country-code="nr"><div class="flag nr"></div><span class="country-name">Nauru</span></li><li class="country" data-dial-code="Nepal" data-country-code="np"><div class="flag np"></div><span class="country-name">Nepal</span></li><li class="country" data-dial-code="Netherlands" data-country-code="nl"><div class="flag nl"></div><span class="country-name">Netherlands</span></li><li class="country" data-dial-code="New Caledonia" data-country-code="nc"><div class="flag nc"></div><span class="country-name">New Caledonia</span></li><li class="country active highlight" data-dial-code="New Zealand" data-country-code="nz"><div class="flag nz"></div><span class="country-name">New Zealand</span></li><li class="country" data-dial-code="Nicaragua" data-country-code="ni"><div class="flag ni"></div><span class="country-name">Nicaragua</span></li><li class="country" data-dial-code="Niger" data-country-code="ne"><div class="flag ne"></div><span class="country-name">Niger</span></li><li class="country" data-dial-code="Nigeria" data-country-code="ng"><div class="flag ng"></div><span class="country-name">Nigeria</span></li><li class="country" data-dial-code="North Korea" data-country-code="kp"><div class="flag kp"></div><span class="country-name">North Korea</span></li><li class="country" data-dial-code="Norway" data-country-code="no"><div class="flag no"></div><span class="country-name">Norway</span></li><li class="country" data-dial-code="Oman" data-country-code="om"><div class="flag om"></div><span class="country-name">Oman</span></li><li class="country" data-dial-code="Pakistan" data-country-code="pk"><div class="flag pk"></div><span class="country-name">Pakistan</span></li><li class="country" data-dial-code="Palau" data-country-code="pw"><div class="flag pw"></div><span class="country-name">Palau</span></li><li class="country" data-dial-code="Palestinian Territory" data-country-code="ps"><div class="flag ps"></div><span class="country-name">Palestinian Territory</span></li><li class="country" data-dial-code="Panama" data-country-code="pa"><div class="flag pa"></div><span class="country-name">Panama</span></li><li class="country" data-dial-code="Papua New Guinea" data-country-code="pg"><div class="flag pg"></div><span class="country-name">Papua New Guinea</span></li><li class="country" data-dial-code="Paraguay" data-country-code="py"><div class="flag py"></div><span class="country-name">Paraguay</span></li><li class="country" data-dial-code="Peru" data-country-code="pe"><div class="flag pe"></div><span class="country-name">Peru</span></li><li class="country" data-dial-code="Philippines" data-country-code="ph"><div class="flag ph"></div><span class="country-name">Philippines</span></li><li class="country" data-dial-code="Poland" data-country-code="pl"><div class="flag pl"></div><span class="country-name">Poland</span></li><li class="country" data-dial-code="Portugal" data-country-code="pt"><div class="flag pt"></div><span class="country-name">Portugal</span></li><li class="country" data-dial-code="Puerto Rico" data-country-code="pr"><div class="flag pr"></div><span class="country-name">Puerto Rico</span></li><li class="country" data-dial-code="Qatar" data-country-code="qa"><div class="flag qa"></div><span class="country-name">Qatar</span></li><li class="country" data-dial-code="RÃ©union" data-country-code="re"><div class="flag re"></div><span class="country-name">RÃ©union</span></li><li class="country" data-dial-code="Romania" data-country-code="ro"><div class="flag ro"></div><span class="country-name">Romania</span></li><li class="country" data-dial-code="Russian Federation" data-country-code="ru"><div class="flag ru"></div><span class="country-name">Russian Federation</span></li><li class="country" data-dial-code="Rwanda" data-country-code="rw"><div class="flag rw"></div><span class="country-name">Rwanda</span></li><li class="country" data-dial-code="Saint Kitts and Nevis" data-country-code="kn"><div class="flag kn"></div><span class="country-name">Saint Kitts and Nevis</span></li><li class="country" data-dial-code="Saint Lucia" data-country-code="lc"><div class="flag lc"></div><span class="country-name">Saint Lucia</span></li><li class="country" data-dial-code="Saint Vincent and the Grenadines" data-country-code="vc"><div class="flag vc"></div><span class="country-name">Saint Vincent and the Grenadines</span></li><li class="country" data-dial-code="Samoa" data-country-code="ws"><div class="flag ws"></div><span class="country-name">Samoa</span></li><li class="country" data-dial-code="San Marino" data-country-code="sm"><div class="flag sm"></div><span class="country-name">San Marino</span></li><li class="country" data-dial-code="SÃ£o TomÃ© and PrÃ&shy;ncipe" data-country-code="st"><div class="flag st"></div><span class="country-name">SÃ£o TomÃ© and PrÃ&shy;ncipe</span></li><li class="country" data-dial-code="Saudi Arabia" data-country-code="sa"><div class="flag sa"></div><span class="country-name">Saudi Arabia</span></li><li class="country" data-dial-code="Senegal" data-country-code="sn"><div class="flag sn"></div><span class="country-name">Senegal</span></li><li class="country" data-dial-code="Serbia" data-country-code="rs"><div class="flag rs"></div><span class="country-name">Serbia</span></li><li class="country" data-dial-code="Seychelles" data-country-code="sc"><div class="flag sc"></div><span class="country-name">Seychelles</span></li><li class="country" data-dial-code="Sierra Leone" data-country-code="sl"><div class="flag sl"></div><span class="country-name">Sierra Leone</span></li><li class="country" data-dial-code="Singapore" data-country-code="sg"><div class="flag sg"></div><span class="country-name">Singapore</span></li><li class="country" data-dial-code="Slovakia" data-country-code="sk"><div class="flag sk"></div><span class="country-name">Slovakia</span></li><li class="country" data-dial-code="Slovenia" data-country-code="si"><div class="flag si"></div><span class="country-name">Slovenia</span></li><li class="country" data-dial-code="Solomon Islands" data-country-code="sb"><div class="flag sb"></div><span class="country-name">Solomon Islands</span></li><li class="country" data-dial-code="Somalia" data-country-code="so"><div class="flag so"></div><span class="country-name">Somalia</span></li><li class="country" data-dial-code="South Africa" data-country-code="za"><div class="flag za"></div><span class="country-name">South Africa</span></li><li class="country" data-dial-code="South Korea" data-country-code="kr"><div class="flag kr"></div><span class="country-name">South Korea</span></li><li class="country" data-dial-code="Spain" data-country-code="es"><div class="flag es"></div><span class="country-name">Spain</span></li><li class="country" data-dial-code="Sri Lanka" data-country-code="lk"><div class="flag lk"></div><span class="country-name">Sri Lanka</span></li><li class="country" data-dial-code="Sudan" data-country-code="sd"><div class="flag sd"></div><span class="country-name">Sudan</span></li><li class="country" data-dial-code="Suriname" data-country-code="sr"><div class="flag sr"></div><span class="country-name">Suriname</span></li><li class="country" data-dial-code="Swaziland" data-country-code="sz"><div class="flag sz"></div><span class="country-name">Swaziland</span></li><li class="country" data-dial-code="Sweden" data-country-code="se"><div class="flag se"></div><span class="country-name">Sweden</span></li><li class="country" data-dial-code="Switzerland" data-country-code="ch"><div class="flag ch"></div><span class="country-name">Switzerland</span></li><li class="country" data-dial-code="Syrian Arab Republic" data-country-code="sy"><div class="flag sy"></div><span class="country-name">Syrian Arab Republic</span></li><li class="country" data-dial-code="Taiwan, Province of China" data-country-code="tw"><div class="flag tw"></div><span class="country-name">Taiwan, Province of China</span></li><li class="country" data-dial-code="Tajikistan" data-country-code="tj"><div class="flag tj"></div><span class="country-name">Tajikistan</span></li><li class="country" data-dial-code="Tanzania" data-country-code="tz"><div class="flag tz"></div><span class="country-name">Tanzania</span></li><li class="country" data-dial-code="Thailand" data-country-code="th"><div class="flag th"></div><span class="country-name">Thailand</span></li><li class="country" data-dial-code="Timor-Leste" data-country-code="tl"><div class="flag tl"></div><span class="country-name">Timor-Leste</span></li><li class="country" data-dial-code="Togo" data-country-code="tg"><div class="flag tg"></div><span class="country-name">Togo</span></li><li class="country" data-dial-code="Tonga" data-country-code="to"><div class="flag to"></div><span class="country-name">Tonga</span></li><li class="country" data-dial-code="Trinidad and Tobago" data-country-code="tt"><div class="flag tt"></div><span class="country-name">Trinidad and Tobago</span></li><li class="country" data-dial-code="Tunisia" data-country-code="tn"><div class="flag tn"></div><span class="country-name">Tunisia</span></li><li class="country" data-dial-code="Turkey" data-country-code="tr"><div class="flag tr"></div><span class="country-name">Turkey</span></li><li class="country" data-dial-code="Turkmenistan" data-country-code="tm"><div class="flag tm"></div><span class="country-name">Turkmenistan</span></li><li class="country" data-dial-code="Turks and Caicos Islands" data-country-code="tc"><div class="flag tc"></div><span class="country-name">Turks and Caicos Islands</span></li><li class="country" data-dial-code="Tuvalu" data-country-code="tv"><div class="flag tv"></div><span class="country-name">Tuvalu</span></li><li class="country" data-dial-code="Uganda" data-country-code="ug"><div class="flag ug"></div><span class="country-name">Uganda</span></li><li class="country" data-dial-code="Ukraine" data-country-code="ua"><div class="flag ua"></div><span class="country-name">Ukraine</span></li><li class="country" data-dial-code="United Arab Emirates" data-country-code="ae"><div class="flag ae"></div><span class="country-name">United Arab Emirates</span></li><li class="country" data-dial-code="United Kingdom" data-country-code="gb"><div class="flag gb"></div><span class="country-name">United Kingdom</span></li><li class="country" data-dial-code="United States" data-country-code="us"><div class="flag us"></div><span class="country-name">United States</span></li><li class="country" data-dial-code="Uruguay" data-country-code="uy"><div class="flag uy"></div><span class="country-name">Uruguay</span></li><li class="country" data-dial-code="Uzbekistan" data-country-code="uz"><div class="flag uz"></div><span class="country-name">Uzbekistan</span></li><li class="country" data-dial-code="Vanuatu" data-country-code="vu"><div class="flag vu"></div><span class="country-name">Vanuatu</span></li><li class="country" data-dial-code="Vatican City" data-country-code="va"><div class="flag va"></div><span class="country-name">Vatican City</span></li><li class="country" data-dial-code="Venezuela" data-country-code="ve"><div class="flag ve"></div><span class="country-name">Venezuela</span></li><li class="country" data-dial-code="Viet Nam" data-country-code="vn"><div class="flag vn"></div><span class="country-name">Viet Nam</span></li><li class="country" data-dial-code="Virgin Islands (British)" data-country-code="vg"><div class="flag vg"></div><span class="country-name">Virgin Islands (British)</span></li><li class="country" data-dial-code="Virgin Islands (U.S.)" data-country-code="vi"><div class="flag vi"></div><span class="country-name">Virgin Islands (U.S.)</span></li><li class="country" data-dial-code="Western Sahara" data-country-code="eh"><div class="flag eh"></div><span class="country-name">Western Sahara</span></li><li class="country" data-dial-code="Yemen" data-country-code="ye"><div class="flag ye"></div><span class="country-name">Yemen</span></li><li class="country" data-dial-code="Zambia" data-country-code="zm"><div class="flag zm"></div><span class="country-name">Zambia</span></li><li class="country" data-dial-code="Zimbabwe" data-country-code="zw"><div class="flag zw"></div><span class="country-name">Zimbabwe</span></li></ul></div><input type="text" class="form-control countryname" id="Country_of_incorporation" value="" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly"></div>
                                                <span class="error" id="spanCountryCode"></span>
                                            </div>
                                            <div class="col-sm-6 nonResident">
                                                <label class="label_input" style="text-align:left">NRWT Rate</label>
                                            </div>
                                            <div class="col-sm-6 nonResident">
                                                <div class="col-sm-6 closestcls ss">
                                                    <input type="text" class="form-control input-field " value="" placeholder="NRWT Rate" name="companyAddress" id="account" autocomplete="off">
                                                </div>
                                            </div>  
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Bank Account Name
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <input type="text" class="form-control input-field " value="${company.companyAddress}" placeholder="Enter account name" name="companyAddress" id="account" autocomplete="off">
                                                <span class="error" id="spanCompanyAddress"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Bank Account Number
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <input type="text" class="form-control input-field " value="${company.companyAddress}" placeholder="xx-xxxx-xxxxxxx-xxx" name="companyAddress" id="accountNumber" onkeypress="checkAccountNO(this, event)" onkeyup="checkAccountNO(this, event)" autocomplete="off">
                                                <span class="error" id="spanCompanyAddress"></span>
                                            </div>

                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Please attach copy of bank statement 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 closestcls">
                                                <input type="file" name="myDeedFile" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" class="attach-btn2 checkname" id="attachdeedfile">
                                                <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile"><i class="far fa-times-circle"></i></a>
                                                <span class=" error error_attachdeedfile"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Please attach a letter from your Accountant or Lawyer confirming the Trust’s source of funds or wealth 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 closestcls">
                                                <input type="file" name="myDeedFile" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" class="attach-btn3 checkname new" id="attachdeedfile">
                                                <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile"><i class="far fa-times-circle"></i></a>
                                                <span class=" error error_attachdeedfile"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Please upload a copy of trust deed
                                                </label>
                                            </div>
                                            <div class="col-sm-6 closestcls">
                                                <input type="file" name="myDeedFile" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" class="attach-btn2 checkname" id="attachdeedfile">
                                                <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile"><i class="far fa-times-circle"></i></a>
                                                <span class=" error error_attachdeedfile"></span>
                                            </div>
                                        </div>


                                    </div> 
                                </div>
                                <div class="footer_form">

                                </div>
                                <a href="#" name="previous" class="previous action-button-previous privous-signup" >Previous</a>
                                <input type="button" name="next" class="next1 action-button"  value="Continue"/>
                            </fieldset>
                            <fieldset id="step2">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Beneficiary Details.
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <small class="verification-otp1">
                                                Please enter the full name and date of birth of all beneficiaries of the Trust 
                                            </small>
                                            <div class="col-sm-12">
                                                <input type="hidden" class="input-field counter" id="counter" >
                                                <input type="hidden" id="directors-index"/>
                                                <input type="hidden" id="directors-value"/>
                                            </div>
                                        </div>
                                        <div class="row director-sections">
                                            <div class="row director-adds">
                                                <div class="col-lg-7 ">
                                                    <input type="text" class="fname input-field tags" id="fname" name="fullName" value="${company.fname}" placeholder="Enter full name" >
                                                    <span class="error spanfname" id="spanfname"></span>
                                                </div>
                                                <div class="col-lg-5">
                                                    <input type="text" name="dob" placeholder="Enter DOB" class="input-field dateOfBirth" value="${company.dateOfInco}"  data-lang="en"/>
                                                    <span class="error" id="spanCompanyDate"></span>

                                                    <input type='hidden' class='cls-me' name='me' value='Y'>
                                                </div>
                                            </div> 
                                            <div class="cloneDiv"></div>

                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <button id="btns20" class="director-btn all-btn-color">Add Another Beneficiary</button>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="focus-border" style="display:block;" id="validationId" ></span>
                                </div>

                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous1 action-button-previous" value="Previous" />
                                <input type="button" name="next"  class=" next2 action-button"  value="Continue"/>
                            </fieldset>

                            <fieldset id="step3">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Trustee details. 
                                        </h5>

                                    </div>


                                    <div class="input-content">
                                        <div class="row closeSet">
                                            <small class="verification-otp1">
                                                Please enter the full name of all Trustees, indicating yourself (if applicable) and the primary contact.  
                                            </small>
                                            <!-- <div class="col-sm-12">
                                                    <p><span class="director-name this-name"></span></p>
                                            </div> -->
                                            <div class="addss cloneName">
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        <input type="text" name="dob"  id="trusteeName" value="" placeholder="Trustee full name " class="input-field continer3Dob" /> 
                                                    </label>
                                                </div>
                                                <div class="col-sm-2 this-space">
                                                    <a href="javascript:void(0)" onclick="active(this)" data-id="thisIsMe" class="thisIsMe this-btn this-is-me check-this-btn one colorchnge">This is me</a>
                                                </div>
                                                <div class="col-sm-4 this-space">
                                                    <a href="javascript:void(0)" onclick="active(this)" data-id="contact" class="contact this-btn this-is-me check-this-btn one colorchnge">Primary Contact</a>
                                                </div>

                                            </div>
                                            <div id="rightDiv" class="cloneName">
                                            </div>
                                            <button id="btnnsmy" class="director-btn all-btn-color">Add Another Trustee</button>



                                            <!--                                            <div class="col-sm-6">
                                                                                            <input type="text" class="form-control input-field" id="cOccupation" value="${company.occupation}" name="Occupation" placeholder="Enter occupation" onkeyup="myFunction()" />
                                                                                            <span class="error" id="spanOccupation"></span>
                                                                                        </div>-->


                                        </div>

                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous2 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next3 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step4">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please provide the following information for <span class="investerName"> </span>
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <!-- <div class="col-sm-12">
                                                    <p><span class="director-name this-name"></span></p>
                                            </div> -->
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Home address
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" id="trusteeAddresses" value="${company.address}" class="form-control input-field trusteeAddresses"  placeholder="Enter home address" />
                                                <span class="error" id="spanHomeaddress"></span>
                                            </div>

                                            <div class="col-sm-3">
                                                <label class="label_input" style="text-align:left">
                                                    Contact number 
                                                </label>
                                            </div>
                                            <div class="col-sm-3">
                                                <select class="selectoption form-group" id="otherNumber" >
                                                    <option value="Mobile">Mobile</option>
                                                    <option value="Home">Home</option>
                                                    <option value="Work">Work</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                            </div> 
                                            <div class="col-sm-6 mobile_number first-mobile mobile-index1">
                                                <input type="text" class="form-control codenumber"  id="countryCode" value="${company.otherCountryCode}" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">
                                                <input type="tel" class="form-control error codename" id="mobileNo2" value="${company.otherMobileNo}" name="otherNumber" required="required" placeholder="Enter number " onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                                <span class="error"></span>
                                            </div>
                                            <!-- <div class="col-sm-6">
                                                    <label class="label_input">
                                                    Work phone number (optional) 
                                                    </label>
                                                    </div>
                                                    <div class="col-sm-6 mobile_number first-mobile mobile-index2">
                                                    <input type="text" class="form-control codenumber"  id="countryCode2" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">
                                                    <input type="tel" class="form-control error codename" name="mobileNo" required="required" placeholder="Enter work number (optional)">
                                                    
                                            </div> -->
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Date of birth
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="dob" id="Date_of_incorporation" placeholder="dd/mm/yyyy" class="input-field doc" value="${company.dateOfInco}"  data-lang="en"/>
                                                <span class="error" id="spanCompanyDate"></span>
                                            </div>

                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Place of birth 
                                                </label>
                                            </div>

                                            <div class="col-sm-6 ">
                                                <input type="text" id="address" value="" class="form-control input-field pac-target-input" placeholder="Enter Town/City" autocomplete="off">

                                                <span class="error" id="spanCompanyAddress"></span>
                                            </div>

                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Country of birth 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-group country-set flag-drop selects">
                                                <input type="text" class="form-control countryname" id="Country_of_incorporation"   value="${company.countryOfResidence}" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">
                                                <span class="error" id="spanCountryCode"></span>
                                            </div>

                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    IRD number 
                                                </label>
                                            </div>

                                            <div class="col-sm-6">
                                                <input type="text" class="form-control input-field" id="irdNumber" name="IRDNumber" required="required" placeholder="XXX-XXX-XXX" value="" onkeypress="checkird(this, event)" onkeyup="pasteIRRD(this, event)">
                                                <span class="error" id="spanIRDNumber"></span>
                                            </div>

                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Are you a New Zealand Citizen?  
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-group country-set flag-drop">
                                                <select name="occupation" id="select-occupation" class="Occupation occupationOption selectoption  otherOcc  nameTitle form-group">
                                                    <option value="Select">Select</option>
                                                    <option value="No">No</option>
                                                    <option value="yes">Yes</option>


                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->
                                </div>
                                <input type="button" name="previous" class="previous3 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next4 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step5">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter the indentification details for <span class="investerName"> </span>
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="input-content-text">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="label_input detail-2" style="text-align:left">
                                                        Which type of ID are you providing
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 selDiv">
                                                    <select class="selectoption form-group option Id_Type" id="src_of_fund1">
                                                        <option value="1">NZ Driver Licence</option>
                                                        <option value="2">NZ Passport</option>
                                                        <option value="3">Other </option>

                                                    </select>
                                                </div>
                                                <div class="row drivery-licence">
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            First name 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="first_name removeSet" name="licence_first_name" placeholder="Enter first name" id="licence_first_name" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))">
                                                        <span class="error" id="error_licence_first_name"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Middle name 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="middle_name removeSet" name="licence_middle_name" placeholder="Enter middle name" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))">
                                                        <span class="error" id="error_licence_middle_name"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Last name 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="last_name removeSet" name="licence_last_name" placeholder="Enter last name" id="licence_last_name" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))">
                                                        <span class="error" id="error_licence_last_name"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Licence number 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control removeSet input-field" id="licenseNumber" value="" name="license_number" required="required" placeholder="Enter licence number ">
                                                        <span class="error" id="spanlicenseNumber"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Expiry date 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6 form-group">
                                                        <input type="text" name="dob" placeholder="dd/mm/yyyy" class="input-field dob1 typeExpirydate" value="${company.dateOfInco}"  data-lang="en"/>
                                                        <span class="error" id="spanCompanyDate"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Version number 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" name="college" id="versionNumber" value="" placeholder="Enter version number" class="input-field removeSet lic_verson_number" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                                        <span class="error" id="spanVersionnumber"></span>
                                                    </div>
                                                </div>
                                                <div class="row passport-select" style="display: flex;">

                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            First name 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="first_name removeSet" name="passport_first_name" placeholder="Enter first name" id="passport_first_name" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))">
                                                        <span class="error" id="error_passport_first_name"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Middle name 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="middle_name removeSet" name="passport_middle_name" placeholder="Enter middle name" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))">
                                                        <span class="error" id="error_passport_middle_name"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Last name 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="last_name removeSet" name="passport_last_name" placeholder="Enter last name" id="passport_last_name" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))">
                                                        <span class="error" id="error_passport_last_name"></span>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Passport number
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control removeSet input-field" id="Passportnumber" name="passport_number" value="" required="required" placeholder="Enter passport number">
                                                        <span class="error" id="spanPassportnumber"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Expiry date 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6 form-group">
                                                        <input type="text" name="dob" placeholder="dd/mm/yyyy" class="input-field dob1 typeExpirydate" value="${company.dateOfInco}"  data-lang="en"/>
                                                        <span class="error" id="spanCompanyDate"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Country of issue  
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6 flag-drop">
                                                        <div class="intl-tel-input"><div class="flag-dropdown f16"><div class="selected-flag"><div class="down-arrow down-arrow-new"></div></div><ul class="country-list hide"><li class="country preferred active" data-dial-code="New Zealand" data-country-code="nz"><div class="flag nz"></div><span class="country-name">New Zealand</span></li><li class="divider"></li><li class="country" data-dial-code="Afghanistan" data-country-code="af"><div class="flag af"></div><span class="country-name">Afghanistan</span></li><li class="country" data-dial-code="Albania" data-country-code="al"><div class="flag al"></div><span class="country-name">Albania</span></li><li class="country" data-dial-code="Algeria" data-country-code="dz"><div class="flag dz"></div><span class="country-name">Algeria</span></li><li class="country" data-dial-code="American Samoa" data-country-code="as"><div class="flag as"></div><span class="country-name">American Samoa</span></li><li class="country" data-dial-code="Andorra" data-country-code="ad"><div class="flag ad"></div><span class="country-name">Andorra</span></li><li class="country" data-dial-code="Angola" data-country-code="ao"><div class="flag ao"></div><span class="country-name">Angola</span></li><li class="country" data-dial-code="Anguilla" data-country-code="ai"><div class="flag ai"></div><span class="country-name">Anguilla</span></li><li class="country" data-dial-code="Antigua and Barbuda" data-country-code="ag"><div class="flag ag"></div><span class="country-name">Antigua and Barbuda</span></li><li class="country" data-dial-code="Argentina" data-country-code="ar"><div class="flag ar"></div><span class="country-name">Argentina</span></li><li class="country" data-dial-code="Armenia" data-country-code="am"><div class="flag am"></div><span class="country-name">Armenia</span></li><li class="country" data-dial-code="Aruba" data-country-code="aw"><div class="flag aw"></div><span class="country-name">Aruba</span></li><li class="country" data-dial-code="Australia" data-country-code="au"><div class="flag au"></div><span class="country-name">Australia</span></li><li class="country" data-dial-code="Austria" data-country-code="at"><div class="flag at"></div><span class="country-name">Austria</span></li><li class="country" data-dial-code="Azerbaijan" data-country-code="az"><div class="flag az"></div><span class="country-name">Azerbaijan</span></li><li class="country" data-dial-code="Bahamas" data-country-code="bs"><div class="flag bs"></div><span class="country-name">Bahamas</span></li><li class="country" data-dial-code="Bahrain" data-country-code="bh"><div class="flag bh"></div><span class="country-name">Bahrain</span></li><li class="country" data-dial-code="Bangladesh" data-country-code="bd"><div class="flag bd"></div><span class="country-name">Bangladesh</span></li><li class="country" data-dial-code="Barbados" data-country-code="bb"><div class="flag bb"></div><span class="country-name">Barbados</span></li><li class="country" data-dial-code="Belarus" data-country-code="by"><div class="flag by"></div><span class="country-name">Belarus</span></li><li class="country" data-dial-code="Belgium" data-country-code="be"><div class="flag be"></div><span class="country-name">Belgium</span></li><li class="country" data-dial-code="Belize" data-country-code="bz"><div class="flag bz"></div><span class="country-name">Belize</span></li><li class="country" data-dial-code="Benin" data-country-code="bj"><div class="flag bj"></div><span class="country-name">Benin</span></li><li class="country" data-dial-code="Bermuda" data-country-code="bm"><div class="flag bm"></div><span class="country-name">Bermuda</span></li><li class="country" data-dial-code="Bhutan" data-country-code="bt"><div class="flag bt"></div><span class="country-name">Bhutan</span></li><li class="country" data-dial-code="Bolivia" data-country-code="bo"><div class="flag bo"></div><span class="country-name">Bolivia</span></li><li class="country" data-dial-code="Bosnia and Herzegovina" data-country-code="ba"><div class="flag ba"></div><span class="country-name">Bosnia and Herzegovina</span></li><li class="country" data-dial-code="Botswana" data-country-code="bw"><div class="flag bw"></div><span class="country-name">Botswana</span></li><li class="country" data-dial-code="Brazil" data-country-code="br"><div class="flag br"></div><span class="country-name">Brazil</span></li><li class="country" data-dial-code="Brunei Darussalam" data-country-code="bn"><div class="flag bn"></div><span class="country-name">Brunei Darussalam</span></li><li class="country" data-dial-code="Bulgaria" data-country-code="bg"><div class="flag bg"></div><span class="country-name">Bulgaria</span></li><li class="country" data-dial-code="Burkina Faso" data-country-code="bf"><div class="flag bf"></div><span class="country-name">Burkina Faso</span></li><li class="country" data-dial-code="Burundi" data-country-code="bi"><div class="flag bi"></div><span class="country-name">Burundi</span></li><li class="country" data-dial-code="Cambodia" data-country-code="kh"><div class="flag kh"></div><span class="country-name">Cambodia</span></li><li class="country" data-dial-code="Cameroon" data-country-code="cm"><div class="flag cm"></div><span class="country-name">Cameroon</span></li><li class="country" data-dial-code="Canada" data-country-code="ca"><div class="flag ca"></div><span class="country-name">Canada</span></li><li class="country" data-dial-code="Cape Verde" data-country-code="cv"><div class="flag cv"></div><span class="country-name">Cape Verde</span></li><li class="country" data-dial-code="Cayman Islands" data-country-code="ky"><div class="flag ky"></div><span class="country-name">Cayman Islands</span></li><li class="country" data-dial-code="Central African Republic" data-country-code="cf"><div class="flag cf"></div><span class="country-name">Central African Republic</span></li><li class="country" data-dial-code="Chad" data-country-code="td"><div class="flag td"></div><span class="country-name">Chad</span></li><li class="country" data-dial-code="Chile" data-country-code="cl"><div class="flag cl"></div><span class="country-name">Chile</span></li><li class="country" data-dial-code="China" data-country-code="cn"><div class="flag cn"></div><span class="country-name">China</span></li><li class="country" data-dial-code="Colombia" data-country-code="co"><div class="flag co"></div><span class="country-name">Colombia</span></li><li class="country" data-dial-code="Comoros" data-country-code="km"><div class="flag km"></div><span class="country-name">Comoros</span></li><li class="country" data-dial-code="Congo (DRC)" data-country-code="cd"><div class="flag cd"></div><span class="country-name">Congo (DRC)</span></li><li class="country" data-dial-code="Congo (Republic)" data-country-code="cg"><div class="flag cg"></div><span class="country-name">Congo (Republic)</span></li><li class="country" data-dial-code="Cook Islands" data-country-code="ck"><div class="flag ck"></div><span class="country-name">Cook Islands</span></li><li class="country" data-dial-code="Costa Rica" data-country-code="cr"><div class="flag cr"></div><span class="country-name">Costa Rica</span></li><li class="country" data-dial-code="CÃ´te d" ivoire'="" data-country-code="ci"><div class="flag ci"></div><span class="country-name">CÃ´te d'Ivoire</span></li><li class="country" data-dial-code="Croatia" data-country-code="hr"><div class="flag hr"></div><span class="country-name">Croatia</span></li><li class="country" data-dial-code="Cuba" data-country-code="cu"><div class="flag cu"></div><span class="country-name">Cuba</span></li><li class="country" data-dial-code="Cyprus" data-country-code="cy"><div class="flag cy"></div><span class="country-name">Cyprus</span></li><li class="country" data-dial-code="Czech Republic" data-country-code="cz"><div class="flag cz"></div><span class="country-name">Czech Republic</span></li><li class="country" data-dial-code="Denmark" data-country-code="dk"><div class="flag dk"></div><span class="country-name">Denmark</span></li><li class="country" data-dial-code="Djibouti" data-country-code="dj"><div class="flag dj"></div><span class="country-name">Djibouti</span></li><li class="country" data-dial-code="Dominica" data-country-code="dm"><div class="flag dm"></div><span class="country-name">Dominica</span></li><li class="country" data-dial-code="Dominican Republic" data-country-code="do"><div class="flag do"></div><span class="country-name">Dominican Republic</span></li><li class="country" data-dial-code="Ecuador" data-country-code="ec"><div class="flag ec"></div><span class="country-name">Ecuador</span></li><li class="country" data-dial-code="Egypt" data-country-code="eg"><div class="flag eg"></div><span class="country-name">Egypt</span></li><li class="country" data-dial-code="El Salvador" data-country-code="sv"><div class="flag sv"></div><span class="country-name">El Salvador</span></li><li class="country" data-dial-code="Equatorial Guinea" data-country-code="gq"><div class="flag gq"></div><span class="country-name">Equatorial Guinea</span></li><li class="country" data-dial-code="Eritrea" data-country-code="er"><div class="flag er"></div><span class="country-name">Eritrea</span></li><li class="country" data-dial-code="Estonia" data-country-code="ee"><div class="flag ee"></div><span class="country-name">Estonia</span></li><li class="country" data-dial-code="Ethiopia" data-country-code="et"><div class="flag et"></div><span class="country-name">Ethiopia</span></li><li class="country" data-dial-code="Faroe Islands" data-country-code="fo"><div class="flag fo"></div><span class="country-name">Faroe Islands</span></li><li class="country" data-dial-code="Fiji" data-country-code="fj"><div class="flag fj"></div><span class="country-name">Fiji</span></li><li class="country" data-dial-code="Finland" data-country-code="fi"><div class="flag fi"></div><span class="country-name">Finland</span></li><li class="country" data-dial-code="France" data-country-code="fr"><div class="flag fr"></div><span class="country-name">France</span></li><li class="country" data-dial-code="French Polynesia" data-country-code="pf"><div class="flag pf"></div><span class="country-name">French Polynesia</span></li><li class="country" data-dial-code="Gabon" data-country-code="ga"><div class="flag ga"></div><span class="country-name">Gabon</span></li><li class="country" data-dial-code="Gambia" data-country-code="gm"><div class="flag gm"></div><span class="country-name">Gambia</span></li><li class="country" data-dial-code="Georgia" data-country-code="ge"><div class="flag ge"></div><span class="country-name">Georgia</span></li><li class="country" data-dial-code="Germany" data-country-code="de"><div class="flag de"></div><span class="country-name">Germany</span></li><li class="country" data-dial-code="Ghana" data-country-code="gh"><div class="flag gh"></div><span class="country-name">Ghana</span></li><li class="country" data-dial-code="Gibraltar" data-country-code="gi"><div class="flag gi"></div><span class="country-name">Gibraltar</span></li><li class="country" data-dial-code="Greece" data-country-code="gr"><div class="flag gr"></div><span class="country-name">Greece</span></li><li class="country" data-dial-code="Greenland" data-country-code="gl"><div class="flag gl"></div><span class="country-name">Greenland</span></li><li class="country" data-dial-code="Grenada" data-country-code="gd"><div class="flag gd"></div><span class="country-name">Grenada</span></li><li class="country" data-dial-code="Guadeloupe" data-country-code="gp"><div class="flag gp"></div><span class="country-name">Guadeloupe</span></li><li class="country" data-dial-code="Guam" data-country-code="gu"><div class="flag gu"></div><span class="country-name">Guam</span></li><li class="country" data-dial-code="Guatemala" data-country-code="gt"><div class="flag gt"></div><span class="country-name">Guatemala</span></li><li class="country" data-dial-code="Guernsey" data-country-code="gg"><div class="flag gg"></div><span class="country-name">Guernsey</span></li><li class="country" data-dial-code="Guinea" data-country-code="gn"><div class="flag gn"></div><span class="country-name">Guinea</span></li><li class="country" data-dial-code="Guinea-Bissau" data-country-code="gw"><div class="flag gw"></div><span class="country-name">Guinea-Bissau</span></li><li class="country" data-dial-code="Guyana" data-country-code="gy"><div class="flag gy"></div><span class="country-name">Guyana</span></li><li class="country" data-dial-code="Haiti" data-country-code="ht"><div class="flag ht"></div><span class="country-name">Haiti</span></li><li class="country" data-dial-code="Honduras" data-country-code="hn"><div class="flag hn"></div><span class="country-name">Honduras</span></li><li class="country" data-dial-code="Hong Kong" data-country-code="hk"><div class="flag hk"></div><span class="country-name">Hong Kong</span></li><li class="country" data-dial-code="Hungary" data-country-code="hu"><div class="flag hu"></div><span class="country-name">Hungary</span></li><li class="country" data-dial-code="Iceland" data-country-code="is"><div class="flag is"></div><span class="country-name">Iceland</span></li><li class="country" data-dial-code="India" data-country-code="in"><div class="flag in"></div><span class="country-name">India</span></li><li class="country" data-dial-code="Indonesia" data-country-code="id"><div class="flag id"></div><span class="country-name">Indonesia</span></li><li class="country" data-dial-code="Iran" data-country-code="ir"><div class="flag ir"></div><span class="country-name">Iran</span></li><li class="country" data-dial-code="Iraq" data-country-code="iq"><div class="flag iq"></div><span class="country-name">Iraq</span></li><li class="country" data-dial-code="Ireland" data-country-code="ie"><div class="flag ie"></div><span class="country-name">Ireland</span></li><li class="country" data-dial-code="Isle of Man" data-country-code="im"><div class="flag im"></div><span class="country-name">Isle of Man</span></li><li class="country" data-dial-code="Israel" data-country-code="il"><div class="flag il"></div><span class="country-name">Israel</span></li><li class="country" data-dial-code="Italy" data-country-code="it"><div class="flag it"></div><span class="country-name">Italy</span></li><li class="country" data-dial-code="Jamaica" data-country-code="jm"><div class="flag jm"></div><span class="country-name">Jamaica</span></li><li class="country" data-dial-code="Japan" data-country-code="jp"><div class="flag jp"></div><span class="country-name">Japan</span></li><li class="country" data-dial-code="Jersey" data-country-code="je"><div class="flag je"></div><span class="country-name">Jersey</span></li><li class="country" data-dial-code="Jordan" data-country-code="jo"><div class="flag jo"></div><span class="country-name">Jordan</span></li><li class="country" data-dial-code="Kazakhstan" data-country-code="kz"><div class="flag kz"></div><span class="country-name">Kazakhstan</span></li><li class="country" data-dial-code="Kenya" data-country-code="ke"><div class="flag ke"></div><span class="country-name">Kenya</span></li><li class="country" data-dial-code="Kiribati" data-country-code="ki"><div class="flag ki"></div><span class="country-name">Kiribati</span></li><li class="country" data-dial-code="Kuwait" data-country-code="kw"><div class="flag kw"></div><span class="country-name">Kuwait</span></li><li class="country" data-dial-code="Kyrgyzstan" data-country-code="kg"><div class="flag kg"></div><span class="country-name">Kyrgyzstan</span></li><li class="country" data-dial-code="Laos" data-country-code="la"><div class="flag la"></div><span class="country-name">Laos</span></li><li class="country" data-dial-code="Latvia" data-country-code="lv"><div class="flag lv"></div><span class="country-name">Latvia</span></li><li class="country" data-dial-code="Lebanon" data-country-code="lb"><div class="flag lb"></div><span class="country-name">Lebanon</span></li><li class="country" data-dial-code="Lesotho" data-country-code="ls"><div class="flag ls"></div><span class="country-name">Lesotho</span></li><li class="country" data-dial-code="Liberia" data-country-code="lr"><div class="flag lr"></div><span class="country-name">Liberia</span></li><li class="country" data-dial-code="Libya" data-country-code="ly"><div class="flag ly"></div><span class="country-name">Libya</span></li><li class="country" data-dial-code="Liechtenstein" data-country-code="li"><div class="flag li"></div><span class="country-name">Liechtenstein</span></li><li class="country" data-dial-code="Lithuania" data-country-code="lt"><div class="flag lt"></div><span class="country-name">Lithuania</span></li><li class="country" data-dial-code="Luxembourg" data-country-code="lu"><div class="flag lu"></div><span class="country-name">Luxembourg</span></li><li class="country" data-dial-code="Macao" data-country-code="mo"><div class="flag mo"></div><span class="country-name">Macao</span></li><li class="country" data-dial-code="Macedonia" data-country-code="mk"><div class="flag mk"></div><span class="country-name">Macedonia</span></li><li class="country" data-dial-code="Madagascar" data-country-code="mg"><div class="flag mg"></div><span class="country-name">Madagascar</span></li><li class="country" data-dial-code="Malawi" data-country-code="mw"><div class="flag mw"></div><span class="country-name">Malawi</span></li><li class="country" data-dial-code="Malaysia" data-country-code="my"><div class="flag my"></div><span class="country-name">Malaysia</span></li><li class="country" data-dial-code="Maldives" data-country-code="mv"><div class="flag mv"></div><span class="country-name">Maldives</span></li><li class="country" data-dial-code="Mali" data-country-code="ml"><div class="flag ml"></div><span class="country-name">Mali</span></li><li class="country" data-dial-code="Malta" data-country-code="mt"><div class="flag mt"></div><span class="country-name">Malta</span></li><li class="country" data-dial-code="Marshall Islands" data-country-code="mh"><div class="flag mh"></div><span class="country-name">Marshall Islands</span></li><li class="country" data-dial-code="Martinique" data-country-code="mq"><div class="flag mq"></div><span class="country-name">Martinique</span></li><li class="country" data-dial-code="Mauritania" data-country-code="mr"><div class="flag mr"></div><span class="country-name">Mauritania</span></li><li class="country" data-dial-code="Mauritius" data-country-code="mu"><div class="flag mu"></div><span class="country-name">Mauritius</span></li><li class="country" data-dial-code="Mexico" data-country-code="mx"><div class="flag mx"></div><span class="country-name">Mexico</span></li><li class="country" data-dial-code="Micronesia" data-country-code="fm"><div class="flag fm"></div><span class="country-name">Micronesia</span></li><li class="country" data-dial-code="Moldova" data-country-code="md"><div class="flag md"></div><span class="country-name">Moldova</span></li><li class="country" data-dial-code="Monaco" data-country-code="mc"><div class="flag mc"></div><span class="country-name">Monaco</span></li><li class="country" data-dial-code="Mongolia" data-country-code="mn"><div class="flag mn"></div><span class="country-name">Mongolia</span></li><li class="country" data-dial-code="Montenegro" data-country-code="me"><div class="flag me"></div><span class="country-name">Montenegro</span></li><li class="country" data-dial-code="Montserrat" data-country-code="ms"><div class="flag ms"></div><span class="country-name">Montserrat</span></li><li class="country" data-dial-code="Morocco" data-country-code="ma"><div class="flag ma"></div><span class="country-name">Morocco</span></li><li class="country" data-dial-code="Mozambique" data-country-code="mz"><div class="flag mz"></div><span class="country-name">Mozambique</span></li><li class="country" data-dial-code="Myanmar (Burma)" data-country-code="mm"><div class="flag mm"></div><span class="country-name">Myanmar (Burma)</span></li><li class="country" data-dial-code="Namibia" data-country-code="na"><div class="flag na"></div><span class="country-name">Namibia</span></li><li class="country" data-dial-code="Nauru" data-country-code="nr"><div class="flag nr"></div><span class="country-name">Nauru</span></li><li class="country" data-dial-code="Nepal" data-country-code="np"><div class="flag np"></div><span class="country-name">Nepal</span></li><li class="country" data-dial-code="Netherlands" data-country-code="nl"><div class="flag nl"></div><span class="country-name">Netherlands</span></li><li class="country" data-dial-code="New Caledonia" data-country-code="nc"><div class="flag nc"></div><span class="country-name">New Caledonia</span></li><li class="country active" data-dial-code="New Zealand" data-country-code="nz"><div class="flag nz"></div><span class="country-name">New Zealand</span></li><li class="country" data-dial-code="Nicaragua" data-country-code="ni"><div class="flag ni"></div><span class="country-name">Nicaragua</span></li><li class="country" data-dial-code="Niger" data-country-code="ne"><div class="flag ne"></div><span class="country-name">Niger</span></li><li class="country" data-dial-code="Nigeria" data-country-code="ng"><div class="flag ng"></div><span class="country-name">Nigeria</span></li><li class="country" data-dial-code="North Korea" data-country-code="kp"><div class="flag kp"></div><span class="country-name">North Korea</span></li><li class="country" data-dial-code="Norway" data-country-code="no"><div class="flag no"></div><span class="country-name">Norway</span></li><li class="country" data-dial-code="Oman" data-country-code="om"><div class="flag om"></div><span class="country-name">Oman</span></li><li class="country" data-dial-code="Pakistan" data-country-code="pk"><div class="flag pk"></div><span class="country-name">Pakistan</span></li><li class="country" data-dial-code="Palau" data-country-code="pw"><div class="flag pw"></div><span class="country-name">Palau</span></li><li class="country" data-dial-code="Palestinian Territory" data-country-code="ps"><div class="flag ps"></div><span class="country-name">Palestinian Territory</span></li><li class="country" data-dial-code="Panama" data-country-code="pa"><div class="flag pa"></div><span class="country-name">Panama</span></li><li class="country" data-dial-code="Papua New Guinea" data-country-code="pg"><div class="flag pg"></div><span class="country-name">Papua New Guinea</span></li><li class="country" data-dial-code="Paraguay" data-country-code="py"><div class="flag py"></div><span class="country-name">Paraguay</span></li><li class="country" data-dial-code="Peru" data-country-code="pe"><div class="flag pe"></div><span class="country-name">Peru</span></li><li class="country" data-dial-code="Philippines" data-country-code="ph"><div class="flag ph"></div><span class="country-name">Philippines</span></li><li class="country" data-dial-code="Poland" data-country-code="pl"><div class="flag pl"></div><span class="country-name">Poland</span></li><li class="country" data-dial-code="Portugal" data-country-code="pt"><div class="flag pt"></div><span class="country-name">Portugal</span></li><li class="country" data-dial-code="Puerto Rico" data-country-code="pr"><div class="flag pr"></div><span class="country-name">Puerto Rico</span></li><li class="country" data-dial-code="Qatar" data-country-code="qa"><div class="flag qa"></div><span class="country-name">Qatar</span></li><li class="country" data-dial-code="RÃ©union" data-country-code="re"><div class="flag re"></div><span class="country-name">RÃ©union</span></li><li class="country" data-dial-code="Romania" data-country-code="ro"><div class="flag ro"></div><span class="country-name">Romania</span></li><li class="country" data-dial-code="Russian Federation" data-country-code="ru"><div class="flag ru"></div><span class="country-name">Russian Federation</span></li><li class="country" data-dial-code="Rwanda" data-country-code="rw"><div class="flag rw"></div><span class="country-name">Rwanda</span></li><li class="country" data-dial-code="Saint Kitts and Nevis" data-country-code="kn"><div class="flag kn"></div><span class="country-name">Saint Kitts and Nevis</span></li><li class="country" data-dial-code="Saint Lucia" data-country-code="lc"><div class="flag lc"></div><span class="country-name">Saint Lucia</span></li><li class="country" data-dial-code="Saint Vincent and the Grenadines" data-country-code="vc"><div class="flag vc"></div><span class="country-name">Saint Vincent and the Grenadines</span></li><li class="country" data-dial-code="Samoa" data-country-code="ws"><div class="flag ws"></div><span class="country-name">Samoa</span></li><li class="country" data-dial-code="San Marino" data-country-code="sm"><div class="flag sm"></div><span class="country-name">San Marino</span></li><li class="country" data-dial-code="SÃ£o TomÃ© and PrÃ&shy;ncipe" data-country-code="st"><div class="flag st"></div><span class="country-name">SÃ£o TomÃ© and PrÃ&shy;ncipe</span></li><li class="country" data-dial-code="Saudi Arabia" data-country-code="sa"><div class="flag sa"></div><span class="country-name">Saudi Arabia</span></li><li class="country" data-dial-code="Senegal" data-country-code="sn"><div class="flag sn"></div><span class="country-name">Senegal</span></li><li class="country" data-dial-code="Serbia" data-country-code="rs"><div class="flag rs"></div><span class="country-name">Serbia</span></li><li class="country" data-dial-code="Seychelles" data-country-code="sc"><div class="flag sc"></div><span class="country-name">Seychelles</span></li><li class="country" data-dial-code="Sierra Leone" data-country-code="sl"><div class="flag sl"></div><span class="country-name">Sierra Leone</span></li><li class="country" data-dial-code="Singapore" data-country-code="sg"><div class="flag sg"></div><span class="country-name">Singapore</span></li><li class="country" data-dial-code="Slovakia" data-country-code="sk"><div class="flag sk"></div><span class="country-name">Slovakia</span></li><li class="country" data-dial-code="Slovenia" data-country-code="si"><div class="flag si"></div><span class="country-name">Slovenia</span></li><li class="country" data-dial-code="Solomon Islands" data-country-code="sb"><div class="flag sb"></div><span class="country-name">Solomon Islands</span></li><li class="country" data-dial-code="Somalia" data-country-code="so"><div class="flag so"></div><span class="country-name">Somalia</span></li><li class="country" data-dial-code="South Africa" data-country-code="za"><div class="flag za"></div><span class="country-name">South Africa</span></li><li class="country" data-dial-code="South Korea" data-country-code="kr"><div class="flag kr"></div><span class="country-name">South Korea</span></li><li class="country" data-dial-code="Spain" data-country-code="es"><div class="flag es"></div><span class="country-name">Spain</span></li><li class="country" data-dial-code="Sri Lanka" data-country-code="lk"><div class="flag lk"></div><span class="country-name">Sri Lanka</span></li><li class="country" data-dial-code="Sudan" data-country-code="sd"><div class="flag sd"></div><span class="country-name">Sudan</span></li><li class="country" data-dial-code="Suriname" data-country-code="sr"><div class="flag sr"></div><span class="country-name">Suriname</span></li><li class="country" data-dial-code="Swaziland" data-country-code="sz"><div class="flag sz"></div><span class="country-name">Swaziland</span></li><li class="country" data-dial-code="Sweden" data-country-code="se"><div class="flag se"></div><span class="country-name">Sweden</span></li><li class="country" data-dial-code="Switzerland" data-country-code="ch"><div class="flag ch"></div><span class="country-name">Switzerland</span></li><li class="country" data-dial-code="Syrian Arab Republic" data-country-code="sy"><div class="flag sy"></div><span class="country-name">Syrian Arab Republic</span></li><li class="country" data-dial-code="Taiwan, Province of China" data-country-code="tw"><div class="flag tw"></div><span class="country-name">Taiwan, Province of China</span></li><li class="country" data-dial-code="Tajikistan" data-country-code="tj"><div class="flag tj"></div><span class="country-name">Tajikistan</span></li><li class="country" data-dial-code="Tanzania" data-country-code="tz"><div class="flag tz"></div><span class="country-name">Tanzania</span></li><li class="country" data-dial-code="Thailand" data-country-code="th"><div class="flag th"></div><span class="country-name">Thailand</span></li><li class="country" data-dial-code="Timor-Leste" data-country-code="tl"><div class="flag tl"></div><span class="country-name">Timor-Leste</span></li><li class="country" data-dial-code="Togo" data-country-code="tg"><div class="flag tg"></div><span class="country-name">Togo</span></li><li class="country" data-dial-code="Tonga" data-country-code="to"><div class="flag to"></div><span class="country-name">Tonga</span></li><li class="country" data-dial-code="Trinidad and Tobago" data-country-code="tt"><div class="flag tt"></div><span class="country-name">Trinidad and Tobago</span></li><li class="country" data-dial-code="Tunisia" data-country-code="tn"><div class="flag tn"></div><span class="country-name">Tunisia</span></li><li class="country" data-dial-code="Turkey" data-country-code="tr"><div class="flag tr"></div><span class="country-name">Turkey</span></li><li class="country" data-dial-code="Turkmenistan" data-country-code="tm"><div class="flag tm"></div><span class="country-name">Turkmenistan</span></li><li class="country" data-dial-code="Turks and Caicos Islands" data-country-code="tc"><div class="flag tc"></div><span class="country-name">Turks and Caicos Islands</span></li><li class="country" data-dial-code="Tuvalu" data-country-code="tv"><div class="flag tv"></div><span class="country-name">Tuvalu</span></li><li class="country" data-dial-code="Uganda" data-country-code="ug"><div class="flag ug"></div><span class="country-name">Uganda</span></li><li class="country" data-dial-code="Ukraine" data-country-code="ua"><div class="flag ua"></div><span class="country-name">Ukraine</span></li><li class="country" data-dial-code="United Arab Emirates" data-country-code="ae"><div class="flag ae"></div><span class="country-name">United Arab Emirates</span></li><li class="country" data-dial-code="United Kingdom" data-country-code="gb"><div class="flag gb"></div><span class="country-name">United Kingdom</span></li><li class="country" data-dial-code="United States" data-country-code="us"><div class="flag us"></div><span class="country-name">United States</span></li><li class="country" data-dial-code="Uruguay" data-country-code="uy"><div class="flag uy"></div><span class="country-name">Uruguay</span></li><li class="country" data-dial-code="Uzbekistan" data-country-code="uz"><div class="flag uz"></div><span class="country-name">Uzbekistan</span></li><li class="country" data-dial-code="Vanuatu" data-country-code="vu"><div class="flag vu"></div><span class="country-name">Vanuatu</span></li><li class="country" data-dial-code="Vatican City" data-country-code="va"><div class="flag va"></div><span class="country-name">Vatican City</span></li><li class="country" data-dial-code="Venezuela" data-country-code="ve"><div class="flag ve"></div><span class="country-name">Venezuela</span></li><li class="country" data-dial-code="Viet Nam" data-country-code="vn"><div class="flag vn"></div><span class="country-name">Viet Nam</span></li><li class="country" data-dial-code="Virgin Islands (British)" data-country-code="vg"><div class="flag vg"></div><span class="country-name">Virgin Islands (British)</span></li><li class="country" data-dial-code="Virgin Islands (U.S.)" data-country-code="vi"><div class="flag vi"></div><span class="country-name">Virgin Islands (U.S.)</span></li><li class="country" data-dial-code="Western Sahara" data-country-code="eh"><div class="flag eh"></div><span class="country-name">Western Sahara</span></li><li class="country" data-dial-code="Yemen" data-country-code="ye"><div class="flag ye"></div><span class="country-name">Yemen</span></li><li class="country" data-dial-code="Zambia" data-country-code="zm"><div class="flag zm"></div><span class="country-name">Zambia</span></li><li class="country" data-dial-code="Zimbabwe" data-country-code="zw"><div class="flag zw"></div><span class="country-name">Zimbabwe</span></li></ul></div><input type="text" class="form-control countryname" id="Countryofissue" name="countryCode" required="required" value="" placeholder="Enter Country Code" readonly="readonly"></div>    
                                                        <span class="error" id="spanCountryofissue"></span>
                                                    </div>
                                                </div>
                                                <div class="row other-select" style="display: none;">
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Type of ID   
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-field" id="TypeofID" name="fullName" required="required" placeholder="Enter ID type">
                                                        <span class="error" id="spanTypeofID"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            First name 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="first_name removeSet" id="other_first_name" name="other_first_name" placeholder="Enter first name">
                                                        <span class="error" id="error_other_first_name"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Middle name 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="middle_name removeSet" name="other_middle_name" placeholder="Enter middle name">
                                                        <span class="error" id="error_other_middle_name"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Last name 
                                                        </label>
                                                    </div>                                                    
                                                    <div class="col-sm-6">
                                                        <input type="text" class="last_name removeSet" id="other_last_name" name="other_last_name" placeholder="Enter last name">
                                                        <span class="error" id="error_other_last_name"></span>
                                                    </div> 
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Expiry date 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6 form-group">
                                                        <input type="text" name="dob" id="typeExpirydate" placeholder="dd/mm/yyyy" class="input-field dob1 " data-format="dd/mm/yyyy" data-lang="en" required="" readonly="readonly">                                 
                                                        <span class="error" id="spantypeExpirydate"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Country of issue  
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6 flag-drop">
                                                        <div class="intl-tel-input"><div class="flag-dropdown f16"><div class="selected-flag"><div class="down-arrow down-arrow-new"></div></div><ul class="country-list hide"><li class="country preferred active" data-dial-code="New Zealand" data-country-code="nz"><div class="flag nz"></div><span class="country-name">New Zealand</span></li><li class="divider"></li><li class="country" data-dial-code="Afghanistan" data-country-code="af"><div class="flag af"></div><span class="country-name">Afghanistan</span></li><li class="country" data-dial-code="Albania" data-country-code="al"><div class="flag al"></div><span class="country-name">Albania</span></li><li class="country" data-dial-code="Algeria" data-country-code="dz"><div class="flag dz"></div><span class="country-name">Algeria</span></li><li class="country" data-dial-code="American Samoa" data-country-code="as"><div class="flag as"></div><span class="country-name">American Samoa</span></li><li class="country" data-dial-code="Andorra" data-country-code="ad"><div class="flag ad"></div><span class="country-name">Andorra</span></li><li class="country" data-dial-code="Angola" data-country-code="ao"><div class="flag ao"></div><span class="country-name">Angola</span></li><li class="country" data-dial-code="Anguilla" data-country-code="ai"><div class="flag ai"></div><span class="country-name">Anguilla</span></li><li class="country" data-dial-code="Antigua and Barbuda" data-country-code="ag"><div class="flag ag"></div><span class="country-name">Antigua and Barbuda</span></li><li class="country" data-dial-code="Argentina" data-country-code="ar"><div class="flag ar"></div><span class="country-name">Argentina</span></li><li class="country" data-dial-code="Armenia" data-country-code="am"><div class="flag am"></div><span class="country-name">Armenia</span></li><li class="country" data-dial-code="Aruba" data-country-code="aw"><div class="flag aw"></div><span class="country-name">Aruba</span></li><li class="country" data-dial-code="Australia" data-country-code="au"><div class="flag au"></div><span class="country-name">Australia</span></li><li class="country" data-dial-code="Austria" data-country-code="at"><div class="flag at"></div><span class="country-name">Austria</span></li><li class="country" data-dial-code="Azerbaijan" data-country-code="az"><div class="flag az"></div><span class="country-name">Azerbaijan</span></li><li class="country" data-dial-code="Bahamas" data-country-code="bs"><div class="flag bs"></div><span class="country-name">Bahamas</span></li><li class="country" data-dial-code="Bahrain" data-country-code="bh"><div class="flag bh"></div><span class="country-name">Bahrain</span></li><li class="country" data-dial-code="Bangladesh" data-country-code="bd"><div class="flag bd"></div><span class="country-name">Bangladesh</span></li><li class="country" data-dial-code="Barbados" data-country-code="bb"><div class="flag bb"></div><span class="country-name">Barbados</span></li><li class="country" data-dial-code="Belarus" data-country-code="by"><div class="flag by"></div><span class="country-name">Belarus</span></li><li class="country" data-dial-code="Belgium" data-country-code="be"><div class="flag be"></div><span class="country-name">Belgium</span></li><li class="country" data-dial-code="Belize" data-country-code="bz"><div class="flag bz"></div><span class="country-name">Belize</span></li><li class="country" data-dial-code="Benin" data-country-code="bj"><div class="flag bj"></div><span class="country-name">Benin</span></li><li class="country" data-dial-code="Bermuda" data-country-code="bm"><div class="flag bm"></div><span class="country-name">Bermuda</span></li><li class="country" data-dial-code="Bhutan" data-country-code="bt"><div class="flag bt"></div><span class="country-name">Bhutan</span></li><li class="country" data-dial-code="Bolivia" data-country-code="bo"><div class="flag bo"></div><span class="country-name">Bolivia</span></li><li class="country" data-dial-code="Bosnia and Herzegovina" data-country-code="ba"><div class="flag ba"></div><span class="country-name">Bosnia and Herzegovina</span></li><li class="country" data-dial-code="Botswana" data-country-code="bw"><div class="flag bw"></div><span class="country-name">Botswana</span></li><li class="country" data-dial-code="Brazil" data-country-code="br"><div class="flag br"></div><span class="country-name">Brazil</span></li><li class="country" data-dial-code="Brunei Darussalam" data-country-code="bn"><div class="flag bn"></div><span class="country-name">Brunei Darussalam</span></li><li class="country" data-dial-code="Bulgaria" data-country-code="bg"><div class="flag bg"></div><span class="country-name">Bulgaria</span></li><li class="country" data-dial-code="Burkina Faso" data-country-code="bf"><div class="flag bf"></div><span class="country-name">Burkina Faso</span></li><li class="country" data-dial-code="Burundi" data-country-code="bi"><div class="flag bi"></div><span class="country-name">Burundi</span></li><li class="country" data-dial-code="Cambodia" data-country-code="kh"><div class="flag kh"></div><span class="country-name">Cambodia</span></li><li class="country" data-dial-code="Cameroon" data-country-code="cm"><div class="flag cm"></div><span class="country-name">Cameroon</span></li><li class="country" data-dial-code="Canada" data-country-code="ca"><div class="flag ca"></div><span class="country-name">Canada</span></li><li class="country" data-dial-code="Cape Verde" data-country-code="cv"><div class="flag cv"></div><span class="country-name">Cape Verde</span></li><li class="country" data-dial-code="Cayman Islands" data-country-code="ky"><div class="flag ky"></div><span class="country-name">Cayman Islands</span></li><li class="country" data-dial-code="Central African Republic" data-country-code="cf"><div class="flag cf"></div><span class="country-name">Central African Republic</span></li><li class="country" data-dial-code="Chad" data-country-code="td"><div class="flag td"></div><span class="country-name">Chad</span></li><li class="country" data-dial-code="Chile" data-country-code="cl"><div class="flag cl"></div><span class="country-name">Chile</span></li><li class="country" data-dial-code="China" data-country-code="cn"><div class="flag cn"></div><span class="country-name">China</span></li><li class="country" data-dial-code="Colombia" data-country-code="co"><div class="flag co"></div><span class="country-name">Colombia</span></li><li class="country" data-dial-code="Comoros" data-country-code="km"><div class="flag km"></div><span class="country-name">Comoros</span></li><li class="country" data-dial-code="Congo (DRC)" data-country-code="cd"><div class="flag cd"></div><span class="country-name">Congo (DRC)</span></li><li class="country" data-dial-code="Congo (Republic)" data-country-code="cg"><div class="flag cg"></div><span class="country-name">Congo (Republic)</span></li><li class="country" data-dial-code="Cook Islands" data-country-code="ck"><div class="flag ck"></div><span class="country-name">Cook Islands</span></li><li class="country" data-dial-code="Costa Rica" data-country-code="cr"><div class="flag cr"></div><span class="country-name">Costa Rica</span></li><li class="country" data-dial-code="CÃ´te d" ivoire'="" data-country-code="ci"><div class="flag ci"></div><span class="country-name">CÃ´te d'Ivoire</span></li><li class="country" data-dial-code="Croatia" data-country-code="hr"><div class="flag hr"></div><span class="country-name">Croatia</span></li><li class="country" data-dial-code="Cuba" data-country-code="cu"><div class="flag cu"></div><span class="country-name">Cuba</span></li><li class="country" data-dial-code="Cyprus" data-country-code="cy"><div class="flag cy"></div><span class="country-name">Cyprus</span></li><li class="country" data-dial-code="Czech Republic" data-country-code="cz"><div class="flag cz"></div><span class="country-name">Czech Republic</span></li><li class="country" data-dial-code="Denmark" data-country-code="dk"><div class="flag dk"></div><span class="country-name">Denmark</span></li><li class="country" data-dial-code="Djibouti" data-country-code="dj"><div class="flag dj"></div><span class="country-name">Djibouti</span></li><li class="country" data-dial-code="Dominica" data-country-code="dm"><div class="flag dm"></div><span class="country-name">Dominica</span></li><li class="country" data-dial-code="Dominican Republic" data-country-code="do"><div class="flag do"></div><span class="country-name">Dominican Republic</span></li><li class="country" data-dial-code="Ecuador" data-country-code="ec"><div class="flag ec"></div><span class="country-name">Ecuador</span></li><li class="country" data-dial-code="Egypt" data-country-code="eg"><div class="flag eg"></div><span class="country-name">Egypt</span></li><li class="country" data-dial-code="El Salvador" data-country-code="sv"><div class="flag sv"></div><span class="country-name">El Salvador</span></li><li class="country" data-dial-code="Equatorial Guinea" data-country-code="gq"><div class="flag gq"></div><span class="country-name">Equatorial Guinea</span></li><li class="country" data-dial-code="Eritrea" data-country-code="er"><div class="flag er"></div><span class="country-name">Eritrea</span></li><li class="country" data-dial-code="Estonia" data-country-code="ee"><div class="flag ee"></div><span class="country-name">Estonia</span></li><li class="country" data-dial-code="Ethiopia" data-country-code="et"><div class="flag et"></div><span class="country-name">Ethiopia</span></li><li class="country" data-dial-code="Faroe Islands" data-country-code="fo"><div class="flag fo"></div><span class="country-name">Faroe Islands</span></li><li class="country" data-dial-code="Fiji" data-country-code="fj"><div class="flag fj"></div><span class="country-name">Fiji</span></li><li class="country" data-dial-code="Finland" data-country-code="fi"><div class="flag fi"></div><span class="country-name">Finland</span></li><li class="country" data-dial-code="France" data-country-code="fr"><div class="flag fr"></div><span class="country-name">France</span></li><li class="country" data-dial-code="French Polynesia" data-country-code="pf"><div class="flag pf"></div><span class="country-name">French Polynesia</span></li><li class="country" data-dial-code="Gabon" data-country-code="ga"><div class="flag ga"></div><span class="country-name">Gabon</span></li><li class="country" data-dial-code="Gambia" data-country-code="gm"><div class="flag gm"></div><span class="country-name">Gambia</span></li><li class="country" data-dial-code="Georgia" data-country-code="ge"><div class="flag ge"></div><span class="country-name">Georgia</span></li><li class="country" data-dial-code="Germany" data-country-code="de"><div class="flag de"></div><span class="country-name">Germany</span></li><li class="country" data-dial-code="Ghana" data-country-code="gh"><div class="flag gh"></div><span class="country-name">Ghana</span></li><li class="country" data-dial-code="Gibraltar" data-country-code="gi"><div class="flag gi"></div><span class="country-name">Gibraltar</span></li><li class="country" data-dial-code="Greece" data-country-code="gr"><div class="flag gr"></div><span class="country-name">Greece</span></li><li class="country" data-dial-code="Greenland" data-country-code="gl"><div class="flag gl"></div><span class="country-name">Greenland</span></li><li class="country" data-dial-code="Grenada" data-country-code="gd"><div class="flag gd"></div><span class="country-name">Grenada</span></li><li class="country" data-dial-code="Guadeloupe" data-country-code="gp"><div class="flag gp"></div><span class="country-name">Guadeloupe</span></li><li class="country" data-dial-code="Guam" data-country-code="gu"><div class="flag gu"></div><span class="country-name">Guam</span></li><li class="country" data-dial-code="Guatemala" data-country-code="gt"><div class="flag gt"></div><span class="country-name">Guatemala</span></li><li class="country" data-dial-code="Guernsey" data-country-code="gg"><div class="flag gg"></div><span class="country-name">Guernsey</span></li><li class="country" data-dial-code="Guinea" data-country-code="gn"><div class="flag gn"></div><span class="country-name">Guinea</span></li><li class="country" data-dial-code="Guinea-Bissau" data-country-code="gw"><div class="flag gw"></div><span class="country-name">Guinea-Bissau</span></li><li class="country" data-dial-code="Guyana" data-country-code="gy"><div class="flag gy"></div><span class="country-name">Guyana</span></li><li class="country" data-dial-code="Haiti" data-country-code="ht"><div class="flag ht"></div><span class="country-name">Haiti</span></li><li class="country" data-dial-code="Honduras" data-country-code="hn"><div class="flag hn"></div><span class="country-name">Honduras</span></li><li class="country" data-dial-code="Hong Kong" data-country-code="hk"><div class="flag hk"></div><span class="country-name">Hong Kong</span></li><li class="country" data-dial-code="Hungary" data-country-code="hu"><div class="flag hu"></div><span class="country-name">Hungary</span></li><li class="country" data-dial-code="Iceland" data-country-code="is"><div class="flag is"></div><span class="country-name">Iceland</span></li><li class="country" data-dial-code="India" data-country-code="in"><div class="flag in"></div><span class="country-name">India</span></li><li class="country" data-dial-code="Indonesia" data-country-code="id"><div class="flag id"></div><span class="country-name">Indonesia</span></li><li class="country" data-dial-code="Iran" data-country-code="ir"><div class="flag ir"></div><span class="country-name">Iran</span></li><li class="country" data-dial-code="Iraq" data-country-code="iq"><div class="flag iq"></div><span class="country-name">Iraq</span></li><li class="country" data-dial-code="Ireland" data-country-code="ie"><div class="flag ie"></div><span class="country-name">Ireland</span></li><li class="country" data-dial-code="Isle of Man" data-country-code="im"><div class="flag im"></div><span class="country-name">Isle of Man</span></li><li class="country" data-dial-code="Israel" data-country-code="il"><div class="flag il"></div><span class="country-name">Israel</span></li><li class="country" data-dial-code="Italy" data-country-code="it"><div class="flag it"></div><span class="country-name">Italy</span></li><li class="country" data-dial-code="Jamaica" data-country-code="jm"><div class="flag jm"></div><span class="country-name">Jamaica</span></li><li class="country" data-dial-code="Japan" data-country-code="jp"><div class="flag jp"></div><span class="country-name">Japan</span></li><li class="country" data-dial-code="Jersey" data-country-code="je"><div class="flag je"></div><span class="country-name">Jersey</span></li><li class="country" data-dial-code="Jordan" data-country-code="jo"><div class="flag jo"></div><span class="country-name">Jordan</span></li><li class="country" data-dial-code="Kazakhstan" data-country-code="kz"><div class="flag kz"></div><span class="country-name">Kazakhstan</span></li><li class="country" data-dial-code="Kenya" data-country-code="ke"><div class="flag ke"></div><span class="country-name">Kenya</span></li><li class="country" data-dial-code="Kiribati" data-country-code="ki"><div class="flag ki"></div><span class="country-name">Kiribati</span></li><li class="country" data-dial-code="Kuwait" data-country-code="kw"><div class="flag kw"></div><span class="country-name">Kuwait</span></li><li class="country" data-dial-code="Kyrgyzstan" data-country-code="kg"><div class="flag kg"></div><span class="country-name">Kyrgyzstan</span></li><li class="country" data-dial-code="Laos" data-country-code="la"><div class="flag la"></div><span class="country-name">Laos</span></li><li class="country" data-dial-code="Latvia" data-country-code="lv"><div class="flag lv"></div><span class="country-name">Latvia</span></li><li class="country" data-dial-code="Lebanon" data-country-code="lb"><div class="flag lb"></div><span class="country-name">Lebanon</span></li><li class="country" data-dial-code="Lesotho" data-country-code="ls"><div class="flag ls"></div><span class="country-name">Lesotho</span></li><li class="country" data-dial-code="Liberia" data-country-code="lr"><div class="flag lr"></div><span class="country-name">Liberia</span></li><li class="country" data-dial-code="Libya" data-country-code="ly"><div class="flag ly"></div><span class="country-name">Libya</span></li><li class="country" data-dial-code="Liechtenstein" data-country-code="li"><div class="flag li"></div><span class="country-name">Liechtenstein</span></li><li class="country" data-dial-code="Lithuania" data-country-code="lt"><div class="flag lt"></div><span class="country-name">Lithuania</span></li><li class="country" data-dial-code="Luxembourg" data-country-code="lu"><div class="flag lu"></div><span class="country-name">Luxembourg</span></li><li class="country" data-dial-code="Macao" data-country-code="mo"><div class="flag mo"></div><span class="country-name">Macao</span></li><li class="country" data-dial-code="Macedonia" data-country-code="mk"><div class="flag mk"></div><span class="country-name">Macedonia</span></li><li class="country" data-dial-code="Madagascar" data-country-code="mg"><div class="flag mg"></div><span class="country-name">Madagascar</span></li><li class="country" data-dial-code="Malawi" data-country-code="mw"><div class="flag mw"></div><span class="country-name">Malawi</span></li><li class="country" data-dial-code="Malaysia" data-country-code="my"><div class="flag my"></div><span class="country-name">Malaysia</span></li><li class="country" data-dial-code="Maldives" data-country-code="mv"><div class="flag mv"></div><span class="country-name">Maldives</span></li><li class="country" data-dial-code="Mali" data-country-code="ml"><div class="flag ml"></div><span class="country-name">Mali</span></li><li class="country" data-dial-code="Malta" data-country-code="mt"><div class="flag mt"></div><span class="country-name">Malta</span></li><li class="country" data-dial-code="Marshall Islands" data-country-code="mh"><div class="flag mh"></div><span class="country-name">Marshall Islands</span></li><li class="country" data-dial-code="Martinique" data-country-code="mq"><div class="flag mq"></div><span class="country-name">Martinique</span></li><li class="country" data-dial-code="Mauritania" data-country-code="mr"><div class="flag mr"></div><span class="country-name">Mauritania</span></li><li class="country" data-dial-code="Mauritius" data-country-code="mu"><div class="flag mu"></div><span class="country-name">Mauritius</span></li><li class="country" data-dial-code="Mexico" data-country-code="mx"><div class="flag mx"></div><span class="country-name">Mexico</span></li><li class="country" data-dial-code="Micronesia" data-country-code="fm"><div class="flag fm"></div><span class="country-name">Micronesia</span></li><li class="country" data-dial-code="Moldova" data-country-code="md"><div class="flag md"></div><span class="country-name">Moldova</span></li><li class="country" data-dial-code="Monaco" data-country-code="mc"><div class="flag mc"></div><span class="country-name">Monaco</span></li><li class="country" data-dial-code="Mongolia" data-country-code="mn"><div class="flag mn"></div><span class="country-name">Mongolia</span></li><li class="country" data-dial-code="Montenegro" data-country-code="me"><div class="flag me"></div><span class="country-name">Montenegro</span></li><li class="country" data-dial-code="Montserrat" data-country-code="ms"><div class="flag ms"></div><span class="country-name">Montserrat</span></li><li class="country" data-dial-code="Morocco" data-country-code="ma"><div class="flag ma"></div><span class="country-name">Morocco</span></li><li class="country" data-dial-code="Mozambique" data-country-code="mz"><div class="flag mz"></div><span class="country-name">Mozambique</span></li><li class="country" data-dial-code="Myanmar (Burma)" data-country-code="mm"><div class="flag mm"></div><span class="country-name">Myanmar (Burma)</span></li><li class="country" data-dial-code="Namibia" data-country-code="na"><div class="flag na"></div><span class="country-name">Namibia</span></li><li class="country" data-dial-code="Nauru" data-country-code="nr"><div class="flag nr"></div><span class="country-name">Nauru</span></li><li class="country" data-dial-code="Nepal" data-country-code="np"><div class="flag np"></div><span class="country-name">Nepal</span></li><li class="country" data-dial-code="Netherlands" data-country-code="nl"><div class="flag nl"></div><span class="country-name">Netherlands</span></li><li class="country" data-dial-code="New Caledonia" data-country-code="nc"><div class="flag nc"></div><span class="country-name">New Caledonia</span></li><li class="country active" data-dial-code="New Zealand" data-country-code="nz"><div class="flag nz"></div><span class="country-name">New Zealand</span></li><li class="country" data-dial-code="Nicaragua" data-country-code="ni"><div class="flag ni"></div><span class="country-name">Nicaragua</span></li><li class="country" data-dial-code="Niger" data-country-code="ne"><div class="flag ne"></div><span class="country-name">Niger</span></li><li class="country" data-dial-code="Nigeria" data-country-code="ng"><div class="flag ng"></div><span class="country-name">Nigeria</span></li><li class="country" data-dial-code="North Korea" data-country-code="kp"><div class="flag kp"></div><span class="country-name">North Korea</span></li><li class="country" data-dial-code="Norway" data-country-code="no"><div class="flag no"></div><span class="country-name">Norway</span></li><li class="country" data-dial-code="Oman" data-country-code="om"><div class="flag om"></div><span class="country-name">Oman</span></li><li class="country" data-dial-code="Pakistan" data-country-code="pk"><div class="flag pk"></div><span class="country-name">Pakistan</span></li><li class="country" data-dial-code="Palau" data-country-code="pw"><div class="flag pw"></div><span class="country-name">Palau</span></li><li class="country" data-dial-code="Palestinian Territory" data-country-code="ps"><div class="flag ps"></div><span class="country-name">Palestinian Territory</span></li><li class="country" data-dial-code="Panama" data-country-code="pa"><div class="flag pa"></div><span class="country-name">Panama</span></li><li class="country" data-dial-code="Papua New Guinea" data-country-code="pg"><div class="flag pg"></div><span class="country-name">Papua New Guinea</span></li><li class="country" data-dial-code="Paraguay" data-country-code="py"><div class="flag py"></div><span class="country-name">Paraguay</span></li><li class="country" data-dial-code="Peru" data-country-code="pe"><div class="flag pe"></div><span class="country-name">Peru</span></li><li class="country" data-dial-code="Philippines" data-country-code="ph"><div class="flag ph"></div><span class="country-name">Philippines</span></li><li class="country" data-dial-code="Poland" data-country-code="pl"><div class="flag pl"></div><span class="country-name">Poland</span></li><li class="country" data-dial-code="Portugal" data-country-code="pt"><div class="flag pt"></div><span class="country-name">Portugal</span></li><li class="country" data-dial-code="Puerto Rico" data-country-code="pr"><div class="flag pr"></div><span class="country-name">Puerto Rico</span></li><li class="country" data-dial-code="Qatar" data-country-code="qa"><div class="flag qa"></div><span class="country-name">Qatar</span></li><li class="country" data-dial-code="RÃ©union" data-country-code="re"><div class="flag re"></div><span class="country-name">RÃ©union</span></li><li class="country" data-dial-code="Romania" data-country-code="ro"><div class="flag ro"></div><span class="country-name">Romania</span></li><li class="country" data-dial-code="Russian Federation" data-country-code="ru"><div class="flag ru"></div><span class="country-name">Russian Federation</span></li><li class="country" data-dial-code="Rwanda" data-country-code="rw"><div class="flag rw"></div><span class="country-name">Rwanda</span></li><li class="country" data-dial-code="Saint Kitts and Nevis" data-country-code="kn"><div class="flag kn"></div><span class="country-name">Saint Kitts and Nevis</span></li><li class="country" data-dial-code="Saint Lucia" data-country-code="lc"><div class="flag lc"></div><span class="country-name">Saint Lucia</span></li><li class="country" data-dial-code="Saint Vincent and the Grenadines" data-country-code="vc"><div class="flag vc"></div><span class="country-name">Saint Vincent and the Grenadines</span></li><li class="country" data-dial-code="Samoa" data-country-code="ws"><div class="flag ws"></div><span class="country-name">Samoa</span></li><li class="country" data-dial-code="San Marino" data-country-code="sm"><div class="flag sm"></div><span class="country-name">San Marino</span></li><li class="country" data-dial-code="SÃ£o TomÃ© and PrÃ&shy;ncipe" data-country-code="st"><div class="flag st"></div><span class="country-name">SÃ£o TomÃ© and PrÃ&shy;ncipe</span></li><li class="country" data-dial-code="Saudi Arabia" data-country-code="sa"><div class="flag sa"></div><span class="country-name">Saudi Arabia</span></li><li class="country" data-dial-code="Senegal" data-country-code="sn"><div class="flag sn"></div><span class="country-name">Senegal</span></li><li class="country" data-dial-code="Serbia" data-country-code="rs"><div class="flag rs"></div><span class="country-name">Serbia</span></li><li class="country" data-dial-code="Seychelles" data-country-code="sc"><div class="flag sc"></div><span class="country-name">Seychelles</span></li><li class="country" data-dial-code="Sierra Leone" data-country-code="sl"><div class="flag sl"></div><span class="country-name">Sierra Leone</span></li><li class="country" data-dial-code="Singapore" data-country-code="sg"><div class="flag sg"></div><span class="country-name">Singapore</span></li><li class="country" data-dial-code="Slovakia" data-country-code="sk"><div class="flag sk"></div><span class="country-name">Slovakia</span></li><li class="country" data-dial-code="Slovenia" data-country-code="si"><div class="flag si"></div><span class="country-name">Slovenia</span></li><li class="country" data-dial-code="Solomon Islands" data-country-code="sb"><div class="flag sb"></div><span class="country-name">Solomon Islands</span></li><li class="country" data-dial-code="Somalia" data-country-code="so"><div class="flag so"></div><span class="country-name">Somalia</span></li><li class="country" data-dial-code="South Africa" data-country-code="za"><div class="flag za"></div><span class="country-name">South Africa</span></li><li class="country" data-dial-code="South Korea" data-country-code="kr"><div class="flag kr"></div><span class="country-name">South Korea</span></li><li class="country" data-dial-code="Spain" data-country-code="es"><div class="flag es"></div><span class="country-name">Spain</span></li><li class="country" data-dial-code="Sri Lanka" data-country-code="lk"><div class="flag lk"></div><span class="country-name">Sri Lanka</span></li><li class="country" data-dial-code="Sudan" data-country-code="sd"><div class="flag sd"></div><span class="country-name">Sudan</span></li><li class="country" data-dial-code="Suriname" data-country-code="sr"><div class="flag sr"></div><span class="country-name">Suriname</span></li><li class="country" data-dial-code="Swaziland" data-country-code="sz"><div class="flag sz"></div><span class="country-name">Swaziland</span></li><li class="country" data-dial-code="Sweden" data-country-code="se"><div class="flag se"></div><span class="country-name">Sweden</span></li><li class="country" data-dial-code="Switzerland" data-country-code="ch"><div class="flag ch"></div><span class="country-name">Switzerland</span></li><li class="country" data-dial-code="Syrian Arab Republic" data-country-code="sy"><div class="flag sy"></div><span class="country-name">Syrian Arab Republic</span></li><li class="country" data-dial-code="Taiwan, Province of China" data-country-code="tw"><div class="flag tw"></div><span class="country-name">Taiwan, Province of China</span></li><li class="country" data-dial-code="Tajikistan" data-country-code="tj"><div class="flag tj"></div><span class="country-name">Tajikistan</span></li><li class="country" data-dial-code="Tanzania" data-country-code="tz"><div class="flag tz"></div><span class="country-name">Tanzania</span></li><li class="country" data-dial-code="Thailand" data-country-code="th"><div class="flag th"></div><span class="country-name">Thailand</span></li><li class="country" data-dial-code="Timor-Leste" data-country-code="tl"><div class="flag tl"></div><span class="country-name">Timor-Leste</span></li><li class="country" data-dial-code="Togo" data-country-code="tg"><div class="flag tg"></div><span class="country-name">Togo</span></li><li class="country" data-dial-code="Tonga" data-country-code="to"><div class="flag to"></div><span class="country-name">Tonga</span></li><li class="country" data-dial-code="Trinidad and Tobago" data-country-code="tt"><div class="flag tt"></div><span class="country-name">Trinidad and Tobago</span></li><li class="country" data-dial-code="Tunisia" data-country-code="tn"><div class="flag tn"></div><span class="country-name">Tunisia</span></li><li class="country" data-dial-code="Turkey" data-country-code="tr"><div class="flag tr"></div><span class="country-name">Turkey</span></li><li class="country" data-dial-code="Turkmenistan" data-country-code="tm"><div class="flag tm"></div><span class="country-name">Turkmenistan</span></li><li class="country" data-dial-code="Turks and Caicos Islands" data-country-code="tc"><div class="flag tc"></div><span class="country-name">Turks and Caicos Islands</span></li><li class="country" data-dial-code="Tuvalu" data-country-code="tv"><div class="flag tv"></div><span class="country-name">Tuvalu</span></li><li class="country" data-dial-code="Uganda" data-country-code="ug"><div class="flag ug"></div><span class="country-name">Uganda</span></li><li class="country" data-dial-code="Ukraine" data-country-code="ua"><div class="flag ua"></div><span class="country-name">Ukraine</span></li><li class="country" data-dial-code="United Arab Emirates" data-country-code="ae"><div class="flag ae"></div><span class="country-name">United Arab Emirates</span></li><li class="country" data-dial-code="United Kingdom" data-country-code="gb"><div class="flag gb"></div><span class="country-name">United Kingdom</span></li><li class="country" data-dial-code="United States" data-country-code="us"><div class="flag us"></div><span class="country-name">United States</span></li><li class="country" data-dial-code="Uruguay" data-country-code="uy"><div class="flag uy"></div><span class="country-name">Uruguay</span></li><li class="country" data-dial-code="Uzbekistan" data-country-code="uz"><div class="flag uz"></div><span class="country-name">Uzbekistan</span></li><li class="country" data-dial-code="Vanuatu" data-country-code="vu"><div class="flag vu"></div><span class="country-name">Vanuatu</span></li><li class="country" data-dial-code="Vatican City" data-country-code="va"><div class="flag va"></div><span class="country-name">Vatican City</span></li><li class="country" data-dial-code="Venezuela" data-country-code="ve"><div class="flag ve"></div><span class="country-name">Venezuela</span></li><li class="country" data-dial-code="Viet Nam" data-country-code="vn"><div class="flag vn"></div><span class="country-name">Viet Nam</span></li><li class="country" data-dial-code="Virgin Islands (British)" data-country-code="vg"><div class="flag vg"></div><span class="country-name">Virgin Islands (British)</span></li><li class="country" data-dial-code="Virgin Islands (U.S.)" data-country-code="vi"><div class="flag vi"></div><span class="country-name">Virgin Islands (U.S.)</span></li><li class="country" data-dial-code="Western Sahara" data-country-code="eh"><div class="flag eh"></div><span class="country-name">Western Sahara</span></li><li class="country" data-dial-code="Yemen" data-country-code="ye"><div class="flag ye"></div><span class="country-name">Yemen</span></li><li class="country" data-dial-code="Zambia" data-country-code="zm"><div class="flag zm"></div><span class="country-name">Zambia</span></li><li class="country" data-dial-code="Zimbabwe" data-country-code="zw"><div class="flag zw"></div><span class="country-name">Zimbabwe</span></li></ul></div><input type="text" class="form-control countryname" id="countryname1" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly"></div>    
                                                        <span class="error" id="span_countryname1"></span>
                                                    </div>
                                                    <div class="col-sm-129 closestcls">
                                                        <input type="file" name="myFile" id="otherDocument" class="attach-btn checkname" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf">
                                                        <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile" style="display: none;"><i class="far fa-times-circle"></i></a>
                                                        <span class="error" id="span-otherDocument"></span>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="investor_verify" id="investor_verify" value="false">       
                                            </div>


                                            <!-- <div class="face" id="error-loader" style="display:none" >
                                                    <div class="hand" ></div>
                                            </div> -->

                                        </div>
                                    </div>



                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->
                                </div>
                                <input type="button" name="previous" class="previous4 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next5 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step6">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Information for other Trustees.
                                        </h5>
                                    </div>
                                    <input type="hidden" class="which_container abc" value="#Date_of_Birth"/>
                                    <div class="input-content">
                                        <div class="row investAppend">
                                            <small class="verification-otp1">
                                                We require additional information for each of the Trustees, this is the same information you have just provided about yourself. You can either enter this information now, or we’ll send them an email requesting this information. 
                                            </small>
                                            <!-- <div class="col-sm-12">
                                                    <p><span class="director-name this-name"></span></p>
                                            </div> -->
                                            <div class="row otherInvester" >
                                                <div class="col-sm-6 spacebtn">
                                                    <h4 class="firstname">John Adam</h4>
                                                </div>

                                                <div class="col-sm-6 this-space spacebtn twobtn">
                                                    <a href="javascript:void(0)" class="previous4 this-btn this-is-me check-this-btn one left" onclick="setName(this);">Enter info now</a>
                                                    <a href="javascript:void(0)" class="this-btn this-is-me check-this-btn one colorchnge right" id="buttonrequest" onclick="buttonRequest(this);">Request via email</a>
                                                </div>
                                                <div class="row inputsmy">
                                                    <div class="col-sm-8 emailadd">
                                                        <label>Email address</label>
                                                    </div>
                                                    <div class="col-sm-4 emailaddw">
                                                        <input type="text" class="form-control input-field" id="registrationNumber" name="registrationNumber" value="" required="required" placeholder="Enter email address">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <!--                                        <div class="row passport-select">
                                        
                                                                                    <div class="col-sm-6">
                                                                                        <label class="label_input">
                                                                                            First name 
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <input type="text" class="first_name" name="passport_first_name" placeholder="Enter first name" id="passport_first_name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                                                        <span class="error" id="error_passport_first_name"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <label class="label_input">
                                                                                            Middle name 
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <input type="text" class="middle_name" name="passport_middle_name" placeholder="Enter middle name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                                                        <span class="error" id="error_passport_middle_name"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <label class="label_input">
                                                                                            Last name 
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <input type="text" class="last_name" name="passport_last_name" placeholder="Enter last name" id="passport_last_name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                                                        <span class="error" id="error_passport_last_name"></span>
                                                                                    </div>
                                        
                                                                                    <div class="col-sm-6">
                                                                                        <label class="label_input">
                                                                                            Passport number
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <input type="text" class="form-control input-field" id="Passportnumber" name="passport_number" value="${company.passportNumber}" required="required" placeholder="Enter passport number" onkeyup="myFunction()" />
                                                                                        <span class="error" id="spanPassportnumber" ></span>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <label class="label_input">
                                                                                            Expiry date 
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 form-group">
                                                                                        <input type="text" name="dob" id="PExpirydate" placeholder="dd/mm/yyyy" value="${company.passportExpiryDate}" class="input-field dob1  pass_expiry" data-format="DD/MM/YY" data-lang="en" required/>                                 
                                                                                        <span class="error" id="spanPExpirydate"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <label class="label_input">
                                                                                            Country of issue  
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 flag-drop">
                                                                                        <input type="text" class="form-control countryname"  id="Countryofissue" name="countryCode" required="required" value="${company.countryOfIssue}" placeholder="Enter Country Code" readonly="readonly">    
                                                                                        <span class="error" id="spanCountryofissue"></span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row other-select">
                                                                                    <div class="col-sm-6">
                                                                                        <label class="label_input">
                                                                                            Type of ID   
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <input type="text" class="form-control input-field" id="TypeofID" name="fullName" required="required" placeholder="Enter ID type" onkeyup="myFunction()" />
                                                                                        <span class="error" id="spanTypeofID" ></span>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <label class="label_input">
                                                                                            First name
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <input type="text" class="first_name" id="other_first_name" name="other_first_name" placeholder="Enter first name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                                                        <span class="error" id="error_other_first_name"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <label class="label_input">
                                                                                            Middle name
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <input type="text" class="middle_name" name="other_middle_name" placeholder="Enter middle name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                                                        <span class="error" id="error_other_middle_name"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <label class="label_input">
                                                                                            Last name  
                                                                                        </label>
                                                                                    </div>                                                    
                                                                                    <div class="col-sm-6">
                                                                                        <input type="text" class="last_name" id="other_last_name" name="other_last_name" placeholder="Enter last name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                                                        <span class="error" id="error_other_last_name"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <label class="label_input">
                                                                                            Expiry date 
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 form-group">
                                                                                        <input type="text" name="dob" id="typeExpirydate" placeholder="dd/mm/yyyy" class="input-field dob1" data-format="dd/mm/yyyy" data-lang="en" required/>                                 
                                                                                        <span class="error" id="spantypeExpirydate"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <label class="label_input">
                                                                                            Country of issue  
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 flag-drop">
                                                                                        <input type="text" class="form-control countryname"  id="countryname1" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                                                        <span class="error" id="span_countryname1"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-129 closestcls">
                                                                                        <input type="file" name="myFile" id="otherDocument" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" class="attach-btn checkname">
                                                                                        <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile"><i class="far fa-times-circle"></i></a>
                                                                                        <span class="error" id="span-otherDocument"></span>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="hidden" name="investor_verify" id="investor_verify" value="false">-->
                                        <!--                                            <div class="col-sm-6 verifybtn">
                                                                                        <input type="button" name="myFile" class="verify-dl" value="Verify Details">
                                                                                        <span class="error" id="error_other_ile"></span>
                                                                                    </div>-->


                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous5 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next6 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step7">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Investment application: AWATERE VALLEY LIMITED PARTNERSHIP
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <small class="verification-otp1 bold">
                                                Syndication of a 300 hectare vineyard property located at 176 Tallots Road, Seddon in Marlborough by way of purchase of units in Awatere Valley Partnership Limited. 
                                            </small>
                                            <div class="col-lg-12 units">

                                                <small class="verification-otp1">
                                                    The unit price is $10,000 per unit. Applications must be for a minimum of 10 units. A 25% deposit of the total investment amount (NZD$2,500 per unit) must be paid in full upon application no later than Thursday 9th April. A further payment of $7,500 per unit applied for (75%) is due on Tuesday 28th April 2020. Payments may be used for any deposit payable on the land or MyFarm fees.  
                                                </small>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Number of units applied for:  
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control input-field" id="units" name="IRDNumber" required="required" maxlength="3" placeholder="Enter number of units " />
                                                <span class="error" id="unitsError"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Total investment amount:($)   
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control input-field" id="totalInvestmentAmount" name="IRDNumber" readonly="readonly" required="required" placeholder="$xxx,xxxx " />
                                                <span class="error" id="spanIRDNumber"></span>
                                            </div> 
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Deposit payment (25% of total):    
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control input-field depositPayment" name="IRDNumber" required="required" readonly="readonly" placeholder="$xxx,xxxx " />
                                                <span class="error" id="spanIRDNumber"></span>
                                            </div> 




                                            <div class="row yes-option">
                                                <div class="row yes-new checktindata">
                                                    <div class="col-sm-12">
                                                        <h5 class="element-header aml-text">
                                                            Please enter all of the countries (excluding NZ) of which you are a tax resident.
                                                        </h5>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input" style="text-align:left">
                                                            Country of tax residence:
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6 details-pos flag-drop">
                                                        <input type="text" class="form-control countrynameexcludenz tex_residence_Country"  id="countryOfTaxResidence" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                        <span class="error countrynameexcludenz-error" ></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Tax Identification Number (TIN) 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-field TIN" id="taxIdenityNumber" name="taxIdenityNumber" required="required" placeholder="Enter TIN " onkeyup="myFunction()" />
                                                        <span class="error tin-error" ></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Reason if TIN not available  
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-field removedata resn_unavailable" name="reasonTIN" value="${company.countryOfResidence}" required="required" placeholder=""/>
                                                        <span class="error resn_unavailable-error" id="reason_tin_error"></span>
                                                    </div> 
                                                </div>
                                                <div class="col-sm-12 add-another">
                                                    <a class="add-another2 all-btn-color" href="javascript:void(0)" id="add-country-another">Add another country </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous6 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next7 action-button" value="Continue" />
                            </fieldset>
                            <jsp:include page="./more-trustee-view.jsp"></jsp:include>
                                <fieldset id="step12">
                                    <div class="content-section">
                                        <div class="element-wrapper">
                                            <h5 class="element-header">
                                                Payment information 
                                            </h5>
                                        </div>
                                        <div class="input-content">
                                            <div class="row">
                                                <small class="verification-otp1">To secure your investment in the AWATERE VALLEY LP, a deposit of 25% of your total investment amount is due no later than Thursday 9th April 2020. To make this payment, please visit your online banking, and arrange to pay the below deposit amount into the following bank account details, using the reference details provided. This payment information will also be emailed to you upon submitting this form.</small>
                                                <div class="col-lg-12 units new">
                                                    <h4>Bank account details for payment </h4>

                                                </div>

                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Account name
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Sharp Tudhope Lawyers
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Bank
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        ANZ
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Account number 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left" id="p1">
                                                        06-0433-0020939-00 
                                                    </label>
                                                    <button class="copy" onclick="copyToClipboard('#p1')">Copy</button>
                                                </div>

                                                <div class="col-lg-12 units new">
                                                    <h4>Reference details for payment </h4>

                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Reference
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        AVLP
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Particulars
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" id="nameOfTrust" style="text-align:left">
                                                        Name of Trust
                                                    </label>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Deposit payment amount (25%)
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input depositPayment" style="text-align:left">
                                                        Deposit amount
                                                    </label>
                                                </div>




                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer_form">
                                        <!--<img src="./resources/images/red.png">-->	
                                    </div>
                                    <input type="button" name="previous" class="previous11 action-button-previous" value="Previous" />
                                    <input type="button" name="next" class="next12 action-button" value="Continue" />
                                </fieldset>
                                <fieldset id="step13">
                                    <div class="content-section">
                                        <div class="element-wrapper">
                                            <h5 class="element-header">
                                                Please read and accept the below Terms and Conditions to continue: 
                                            </h5>
                                        </div>
                                        <div class="input-content">
                                            <div class="input-content-text">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <h4 class="text-center">Agreement of Terms</h4>
                                                        <div class="terms_cond" style="overflow:auto;height: 200px;word-break: break-word;">
                                                            <ol>
                                                                <li>I/We confirm that:</li>
                                                                <li>a) I/We have received a copy of the Information Memorandum
                                                                </li>
                                                                <li>b) Investment decisions are very important, and it has been made clear to me/us that I/we am/are free to take such other professional advice as I/we feel necessary. I/We have been provided with all the relevant information I/we required to make the investment decision and I/we have taken any advice that I/we think appropriate. 
                                                                </li>
                                                                <li> c) I/We acknowledge and accept the Disclaimers and Declarations of Interest as set out on page 3 of the Awatere Valley LP Information Memorandum.
                                                                    <ol>
                                                                        <li>a. I/We accept that I/We must make full payment totalling NZ $10,000 per Unit (100% of investment) as follows: 
                                                                            <ol>
                                                                                <li>i. A deposit payment of NZ $2,500 per Unit (25% of investment) upon application and no later than Thursday 9th April 2020; and </li>
                                                                                <li>ii. A payment of NZ $7,500 per Unit (75% of investment) on or before the close of business on Tuesday 28th April 2020.   </li>
                                                                                <li>b. I/We confirm that I /we am/are, or the entity, which takes up the investment, is/are able to make that payment.  </li>
                                                                            </ol>
                                                                        </li>

                                                                        <li>d) I/We confirm that I /we am/are, or the entity, which takes up the investment, is/are able to make that payment. I/we acknowledge and agree that I/we will become bound as a limited partner under the Limited Partnership Agreement, as set out in the Information Memorandum.  </li>
                                                                        <li>e) I/We acknowledge that my/our completed application once submitted to MyFarm, cannot be withdrawn without authorisation by MyFarm. </li>
                                                                        <li>f) I/We accept that if I/we do not make full payment by the due date advised I/we will be charged and interest will accrue at 13% (or such greater rate as specified in any relevant contract for which the funds are required) on all outstanding funds from the due date until payment or otherwise</li>
                                                                        <li>g) Under the terms of the Unsolicited Electronic Messages Act 2007, I/we provide my/our consent to receiving commercial electronic messages for the purpose of that Act.</li>
                                                                        <li>h) I/We acknowledge that Sharp Tudhope does not act for me/us in connection with this investment</li>
                                                                    </ol>
                                                                </li>
                                                            </ol> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top:20px">
                                                    <div class="col-sm-12" style="text-align: center">
                                                        <a href="javascript:void(0);" style=" color: #011c53;  font-size: 14px; font-weight: bold; padding: 10px;
                                                           text-decoration: underline;"> Download Client Terms and Conditions</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 condition-details">
                                                        <input class="checkbox-check ch1" type="checkbox" name="term_condition">
                                                        <label class="label_input" style="text-align:left">
                                                            I have read and agree to the Terms and Conditions
                                                        </label>
                                                    </div>
                                                    <span class="error" id="error_term_condition"></span>
                                                    <div class="col-sm-12 condition-details">
                                                        <input class="checkbox-check ch2 " type="checkbox" name="authorized_condition">
                                                        <label class="label_input" style="text-align:left">
                                                            I am authorised to accept and act on behalf of all account holders
                                                        </label>
                                                    </div>
                                                    <span class="error" id="error_authorized_condition"></span>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer_form">
                                        <!--<img src="./resources/images/red.png">-->	
                                    </div>
                                    <input type="button" name="previous" class="previous12 action-button-previous" value="Previous" />
                                    <input type="button" name="next" class="next13 action-button" value="Continue" />
                                </fieldset>
                                <fieldset id="step14">
                                    <div class="content-section">
                                        <div class="element-wrapper">
                                            <h5 class="element-header">
                                                Signature 
                                            </h5>
                                        </div>
                                        <div class="input-content">
                                            <div class="row">
                                                <small class="verification-otp1">Please draw your signature in the below box</small>
                                                <div class="col-lg-12 sign">

                                                    <canvas id="signature-pad" width="500" height="200"></canvas>

                                                    <div>
                                                        <!--<button id="save">Save</button>-->
                                                        <button id="clear">Clear</button>
                                                        <!--                                                        <button id="showPointsToggle">Show points?</button>-->
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <div class="footer_form">
                                        <!--<img src="./resources/images/red.png">-->	
                                    </div>
                                    <input type="button" name="previous" class="previous13 action-button-previous" value="Previous" />
                                    <input type="button" name="next" class="next14 action-button" value="Continue" />
                                </fieldset>

                                <fieldset id="step15">
                                    <div class="content-section">
                                        <div class="row">
                                            <div class="col-md-12 thanku">
                                                <div class="element-wrapper">
                                                    <h5 class="element-header">
                                                        Thank you for submitting your application details, we’ll let you know if we need anything further. 
                                                    </h5>
                                                </div>
                                                <h5 class="element-header3">
                                                    We have sent you an email providing the bank account details for payment of the 25% deposit, and have also provided a copy of your application details for your records.
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer_form">
                                        <!--<img src="./resources/images/red.png">-->	
                                    </div>

                                    <div class="col-md-12" style="max-width: 113px;margin: auto;">
                                        <div class="ok-submit">
                                            <input type="button" name="previous" id="submit" class=" action-button-previous new-ok-btn " value="OK" />
                                        </div>
                                    </div>
                                    <!-- <input type="button" name="next" class="next12 action-button" value="Accept" /> -->
                                </fieldset>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
            <script src='https://code.jquery.com/ui/1.12.1/jquery-ui.js'></script>
            <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
            <script src="./resources/js/index.js"></script>        
            <script src="./resources/js/intlTelInput_1.js"></script>
            <script src="./resources/js/intlTelInput_2.js"></script>
            <script src="./resources/js/intlTelInput_3.js"></script>
            <script src="./resources/js/user-main.js"></script>
            <script type="text/javascript" src="./resources/js/countries.js"></script>
            <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js'></script>
            <script src='https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.js'></script>
            <script src="./resources/js/sweetalert.min.js"></script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKuUGMz8-xYyc6t1YWUhygrrkB4-WAFeY&libraries=places&callback=initAutocomplete"
            async defer></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
            <script>
                                                        function buttonRequest(ele) {
                                                            var closeDiv = $(ele).closest(".otherInvester");
                                                            closeDiv.find(".inputsmy").toggle();
                                                        }
                                                        $(document).ready(function () {
                                                            $(".inputsmy").hide();
                                                        });
            </script>
            <script>
                function copyToClipboard(element) {
                    var $temp = $("<input>");
                    $("body").append($temp);
                    $temp.val($(element).text()).select();
                    document.execCommand("copy");
                    $temp.remove();
                }

            </script>
            <script>
                var otherName = [];
                var date = new Date();
                var day = date.getDate();
                var month = date.getMonth();
                var year = date.getFullYear();
                var adultDOB = date.setFullYear(year - 18, month, day);
                $('.dateOfBirth').datepicker({
                    yearRange: (year - 80) + ':' + year,
                    changeMonth: true,
                    changeYear: true,
                    'maxDate': new Date(adultDOB),
                    dateFormat: 'dd/mm/yy'
                }).datepicker().attr('readonly', 'readonly');
                $('#units').on('keyup', function () {
                    var units = $('#units').val();
                    if (units > 9 && units < 1000) {
                        var amount = units * 10000;
                        $('#totalInvestmentAmount').val(amount.toLocaleString());
                        var fouthspercent = units * 10000 / 4;
                        $('.depositPayment').val(fouthspercent.toLocaleString());
                        $('.depositPayment').text(fouthspercent.toLocaleString());
                    } else {
                        $('#unitsError').text('Please enter units between 10 to 999.');
                    }
                });
                $('#selecTaxRate').change(function () {
                    var seletedValue = $('#selecTaxRate option:selected').text();
                    if (seletedValue === 'Exempt') {
                        $('.nonResident').hide();
                        $('.exemptBtn').show();
                    } else if (seletedValue === 'Non Resident') {
                        $('.exemptBtn').hide();
                        $('.nonResident').show();
                    } else {
                        $('.nonResident').hide();
                        $('.exemptBtn').hide();
                    }
                });
                $(function () {
                    $('#select-occupation').change(function () {
                        $('.colors').hide();
                        $('#' + $(this).val()).show();
                    });
                });
                function checkird(valto, event) {
                    if (event.keyCode > 47 && event.keyCode < 58) {
                        if (valto.value.length < 11) {
                            if (valto.value.length === 3 || valto.value.length === 7) {
                                $(valto).val(valto.value + "-");
                            } else {
                                $(valto).val(valto.value);
                            }
                        } else {
                            event.preventDefault();
                        }
                    } else {
                        event.preventDefault();
                    }
                }
                function pasteIRRD(valto, event) {
                    var irdNumber = valto.value.toString().replace(/\D/g, '');
                    if (valto.value.length > 10) {
                        var irrd = irdNumber.substring(0, 9);
                        var iRRD = irrd.replace(/(\d{3})/g, "$1-");
                        $(valto).val(iRRD.substring(0, 11));
                    } else if (valto.value.length > 3 && !valto.value.includes('-')) {
                        var iRRD = irdNumber.replace(/(\d{3})/g, "$1-");
                        $(valto).val(iRRD.substring(0, 11));
                    } else if (valto.value.length > 6 && !valto.value.includes('-')) {
                        var iRRD = irdNumber.replace(/(\d{3})/g, "$1-");
                        $(valto).val(iRRD.substring(0, 11));
                    } else {
                        var val = document.getElementById('IRD_Number');
                        val.focus();
                        var caretPosition = event.target.selectionStart;
                        val.setSelectionRange(caretPosition, caretPosition);
                    }
                }
                function checkAccountNO(valto, event) {
                    let bankIrd = valto.value;
                    var v = bankIrd.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
                    if (event.key === "Backspace") {
                    } else {
                        if (event.charCode === 0 || event.charCode >= 48 && event.charCode <= 57) {
                            var matches = v.match(/\d{1,16}/g);
                            var match = matches && matches[0] || '';
                            var ac = match;
                            if (ac.length > 1 && ac.length < 5) {
                                let inputValue = ac.toString();
                                let first = inputValue.substr(0, 2);
                                let sec = inputValue.substr(2, 4);
                                let final = first + '-' + sec;
                                $(valto).val(final);
                            } else if (ac.length > 5 && ac.length < 13) {
                                let inputValue = ac.toString();
                                let first = inputValue.substr(0, 2);
                                let sec = inputValue.substr(2, 4);
                                let thrd = inputValue.substr(6, 10);
                                let final = first + '-' + sec + '-' + thrd;
                                $(valto).val(final);
                            } else if (ac.length > 12) {
                                let inputValue = ac.toString();
                                let first = inputValue.substr(0, 2);
                                let sec = inputValue.substr(2, 4);
                                let thrd = inputValue.substring(6, 13);
                                let forth = inputValue.substring(13, 16);
                                let final = first + '-' + sec + '-' + thrd + '-' + forth;
                                $(valto).val(final);
                            }
                        } else {
                            event.preventDefault();
                        }
                    }
                }
                function initAutocomplete() {
                    var input = document.getElementById("trustAddress");
                    var input1 = document.getElementById("trusteeAddresses");
                    var options = {
                        types: ["address"],
                        componentRestrictions: {
                            country: "nz"
                        }
                    };
                    var autocomplete = new google.maps.places.Autocomplete(input, options);
                    var autocomplete1 = new google.maps.places.Autocomplete(input1, options);
//                    google.maps.event.addListener(autocomplete, "place_changed", function () {
//                        var place = autocomplete.getPlace();
//                        console.log(JSON.stringify(place));
//                        for (var i = 0; i < place.address_components.length; i++) {
//                            var addressType = place.address_components[i].types[0];
//                            var val = place.address_components[i][componentForm[addressType]];
//                            document.getElementById(addressType).value = val;
//                        }
//                    });
                }
            </script>
            <script>
                $(document).ready(function () {
                    $('.nonResident').hide();
                    $('.exemptBtn').hide();
                    $("#btnnsmy").click(function () {
                        var cloneDiv = $(".addss:first").clone();
                        cloneDiv.find("#trusteeName").val("");
                        cloneDiv.find(".thisIsMe").removeClass("highlight");
                        cloneDiv.find(".contact").removeClass("highlight");
                        cloneDiv.append("<div class='col-sm-2 this-space'><a href='javascript:void(0)' class='this-btn' onclick='addbtnfn(this);'></a><a href='javascript:void(0);' onclick='removedatadivTrustee(this);'><i class='far fa-times-circle trusteeCross'></i></a></div>");
                        cloneDiv.appendTo("#rightDiv");
                    });
                });
            </script>
            <script>
                var SignaturePad = (function (document) {
                    "use strict";
                    var log = console.log.bind(console);
                    var SignaturePad = function (canvas, options) {
                        var self = this,
                                opts = options || {};
                        this.velocityFilterWeight = opts.velocityFilterWeight || 0.7;
                        this.minWidth = opts.minWidth || 0.5;
                        this.maxWidth = opts.maxWidth || 2.5;
                        this.dotSize = opts.dotSize || function () {
                            return (self.minWidth + self.maxWidth) / 2;
                        };
                        this.penColor = opts.penColor || "black";
                        this.backgroundColor = opts.backgroundColor || "rgba(0,0,0,0)";
                        this.throttle = opts.throttle || 0;
                        this.throttleOptions = {
                            leading: true,
                            trailing: true
                        };
                        this.minPointDistance = opts.minPointDistance || 0;
                        this.onEnd = opts.onEnd;
                        this.onBegin = opts.onBegin;
                        this._canvas = canvas;
                        this._ctx = canvas.getContext("2d");
                        this._ctx.lineCap = 'round';
                        this.clear();
                        // we need add these inline so they are available to unbind while still having
                        //  access to 'self' we could use _.bind but it's not worth adding a dependency
                        this._handleMouseDown = function (event) {
                            if (event.which === 1) {
                                self._mouseButtonDown = true;
                                self._strokeBegin(event);
                            }
                        };
                        var _handleMouseMove = function (event) {
                            event.preventDefault();
                            if (self._mouseButtonDown) {
                                self._strokeUpdate(event);
                                if (self.arePointsDisplayed) {
                                    var point = self._createPoint(event);
                                    self._drawMark(point.x, point.y, 5);
                                }
                            }
                        };
                        this._handleMouseMove = _.throttle(_handleMouseMove, self.throttle, self.throttleOptions);
                        //this._handleMouseMove = _handleMouseMove;

                        this._handleMouseUp = function (event) {
                            if (event.which === 1 && self._mouseButtonDown) {
                                self._mouseButtonDown = false;
                                self._strokeEnd(event);
                            }
                        };
                        this._handleTouchStart = function (event) {
                            if (event.targetTouches.length == 1) {
                                var touch = event.changedTouches[0];
                                self._strokeBegin(touch);
                            }
                        };
                        var _handleTouchMove = function (event) {
                            // Prevent scrolling.
                            event.preventDefault();
                            var touch = event.targetTouches[0];
                            self._strokeUpdate(touch);
                            if (self.arePointsDisplayed) {
                                var point = self._createPoint(touch);
                                self._drawMark(point.x, point.y, 5);
                            }
                        };
                        this._handleTouchMove = _.throttle(_handleTouchMove, self.throttle, self.throttleOptions);
                        //this._handleTouchMove = _handleTouchMove;

                        this._handleTouchEnd = function (event) {
                            var wasCanvasTouched = event.target === self._canvas;
                            if (wasCanvasTouched) {
                                event.preventDefault();
                                self._strokeEnd(event);
                            }
                        };
                        this._handleMouseEvents();
                        this._handleTouchEvents();
                    };
                    SignaturePad.prototype.clear = function () {
                        var ctx = this._ctx,
                                canvas = this._canvas;
                        ctx.fillStyle = this.backgroundColor;
                        ctx.clearRect(0, 0, canvas.width, canvas.height);
                        ctx.fillRect(0, 0, canvas.width, canvas.height);
                        this._reset();
                    };
                    SignaturePad.prototype.showPointsToggle = function () {
                        this.arePointsDisplayed = !this.arePointsDisplayed;
                    };
                    SignaturePad.prototype.toDataURL = function (imageType, quality) {
                        var canvas = this._canvas;
                        return canvas.toDataURL.apply(canvas, arguments);
                    };
                    SignaturePad.prototype.fromDataURL = function (dataUrl) {
                        var self = this,
                                image = new Image(),
                                ratio = window.devicePixelRatio || 1,
                                width = this._canvas.width / ratio,
                                height = this._canvas.height / ratio;
                        this._reset();
                        image.src = dataUrl;
                        image.onload = function () {
                            self._ctx.drawImage(image, 0, 0, width, height);
                        };
                        this._isEmpty = false;
                    };
                    SignaturePad.prototype._strokeUpdate = function (event) {
                        var point = this._createPoint(event);
                        if (this._isPointToBeUsed(point)) {
                            this._addPoint(point);
                        }
                    };
                    var pointsSkippedFromBeingAdded = 0;
                    SignaturePad.prototype._isPointToBeUsed = function (point) {
                        // Simplifying, De-noise
                        if (!this.minPointDistance)
                            return true;
                        var points = this.points;
                        if (points && points.length) {
                            var lastPoint = points[points.length - 1];
                            if (point.distanceTo(lastPoint) < this.minPointDistance) {
                                // log(++pointsSkippedFromBeingAdded);
                                return false;
                            }
                        }
                        return true;
                    };
                    SignaturePad.prototype._strokeBegin = function (event) {
                        this._reset();
                        this._strokeUpdate(event);
                        if (typeof this.onBegin === 'function') {
                            this.onBegin(event);
                        }
                    };
                    SignaturePad.prototype._strokeDraw = function (point) {
                        var ctx = this._ctx,
                                dotSize = typeof (this.dotSize) === 'function' ? this.dotSize() : this.dotSize;
                        ctx.beginPath();
                        this._drawPoint(point.x, point.y, dotSize);
                        ctx.closePath();
                        ctx.fill();
                    };
                    SignaturePad.prototype._strokeEnd = function (event) {
                        var canDrawCurve = this.points.length > 2,
                                point = this.points[0];
                        if (!canDrawCurve && point) {
                            this._strokeDraw(point);
                        }
                        if (typeof this.onEnd === 'function') {
                            this.onEnd(event);
                        }
                    };
                    SignaturePad.prototype._handleMouseEvents = function () {
                        this._mouseButtonDown = false;
                        this._canvas.addEventListener("mousedown", this._handleMouseDown);
                        this._canvas.addEventListener("mousemove", this._handleMouseMove);
                        document.addEventListener("mouseup", this._handleMouseUp);
                    };
                    SignaturePad.prototype._handleTouchEvents = function () {
                        // Pass touch events to canvas element on mobile IE11 and Edge.
                        this._canvas.style.msTouchAction = 'none';
                        this._canvas.style.touchAction = 'none';
                        this._canvas.addEventListener("touchstart", this._handleTouchStart);
                        this._canvas.addEventListener("touchmove", this._handleTouchMove);
                        this._canvas.addEventListener("touchend", this._handleTouchEnd);
                    };
                    SignaturePad.prototype.on = function () {
                        this._handleMouseEvents();
                        this._handleTouchEvents();
                    };
                    SignaturePad.prototype.off = function () {
                        this._canvas.removeEventListener("mousedown", this._handleMouseDown);
                        this._canvas.removeEventListener("mousemove", this._handleMouseMove);
                        document.removeEventListener("mouseup", this._handleMouseUp);
                        this._canvas.removeEventListener("touchstart", this._handleTouchStart);
                        this._canvas.removeEventListener("touchmove", this._handleTouchMove);
                        this._canvas.removeEventListener("touchend", this._handleTouchEnd);
                    };
                    SignaturePad.prototype.isEmpty = function () {
                        return this._isEmpty;
                    };
                    SignaturePad.prototype._reset = function () {
                        this.points = [];
                        this._lastVelocity = 0;
                        this._lastWidth = (this.minWidth + this.maxWidth) / 2;
                        this._isEmpty = true;
                        this._ctx.fillStyle = this.penColor;
                    };
                    SignaturePad.prototype._createPoint = function (event) {
                        var rect = this._canvas.getBoundingClientRect();
                        return new Point(
                                event.clientX - rect.left,
                                event.clientY - rect.top
                                );
                    };
                    SignaturePad.prototype._addPoint = function (point) {
                        var points = this.points,
                                c2, c3,
                                curve, tmp;
                        points.push(point);
                        if (points.length > 2) {
                            // To reduce the initial lag make it work with 3 points
                            // by copying the first point to the beginning.
                            if (points.length === 3)
                                points.unshift(points[0]);
                            tmp = this._calculateCurveControlPoints(points[0], points[1], points[2]);
                            c2 = tmp.c2;
                            tmp = this._calculateCurveControlPoints(points[1], points[2], points[3]);
                            c3 = tmp.c1;
                            curve = new Bezier(points[1], c2, c3, points[2]);
                            this._addCurve(curve);
                            // Remove the first element from the list,
                            // so that we always have no more than 4 points in points array.
                            points.shift();
                        }
                    };
                    SignaturePad.prototype._calculateCurveControlPoints = function (s1, s2, s3) {
                        var dx1 = s1.x - s2.x,
                                dy1 = s1.y - s2.y,
                                dx2 = s2.x - s3.x,
                                dy2 = s2.y - s3.y,
                                m1 = {
                                    x: (s1.x + s2.x) / 2.0,
                                    y: (s1.y + s2.y) / 2.0
                                },
                                m2 = {
                                    x: (s2.x + s3.x) / 2.0,
                                    y: (s2.y + s3.y) / 2.0
                                },
                                l1 = Math.sqrt(1.0 * dx1 * dx1 + dy1 * dy1),
                                l2 = Math.sqrt(1.0 * dx2 * dx2 + dy2 * dy2),
                                dxm = (m1.x - m2.x),
                                dym = (m1.y - m2.y),
                                k = l2 / (l1 + l2),
                                cm = {
                                    x: m2.x + dxm * k,
                                    y: m2.y + dym * k
                                },
                                tx = s2.x - cm.x,
                                ty = s2.y - cm.y;
                        return {
                            c1: new Point(m1.x + tx, m1.y + ty),
                            c2: new Point(m2.x + tx, m2.y + ty)
                        };
                    };
                    SignaturePad.prototype._addCurve = function (curve) {
                        var startPoint = curve.startPoint,
                                endPoint = curve.endPoint,
                                velocity, newWidth;
                        velocity = endPoint.velocityFrom(startPoint);
                        velocity = this.velocityFilterWeight * velocity +
                                (1 - this.velocityFilterWeight) * this._lastVelocity;
                        newWidth = this._strokeWidth(velocity);
                        this._drawCurve(curve, this._lastWidth, newWidth);
                        this._lastVelocity = velocity;
                        this._lastWidth = newWidth;
                    };
                    SignaturePad.prototype._drawPoint = function (x, y, size) {
                        var ctx = this._ctx;
                        ctx.moveTo(x, y);
                        ctx.arc(x, y, size, 0, 2 * Math.PI, false);
                        this._isEmpty = false;
                    };
                    SignaturePad.prototype._drawMark = function (x, y, size) {
                        var ctx = this._ctx;
                        ctx.save();
                        ctx.moveTo(x, y);
                        ctx.arc(x, y, size, 0, 2 * Math.PI, false);
                        ctx.fillStyle = 'rgba(255, 0, 0, 0.2)';
                        ctx.fill();
                        ctx.restore();
                    };
                    SignaturePad.prototype._drawCurve = function (curve, startWidth, endWidth) {
                        var ctx = this._ctx,
                                widthDelta = endWidth - startWidth,
                                drawSteps, width, i, t, tt, ttt, u, uu, uuu, x, y;
                        drawSteps = Math.floor(curve.length());
                        ctx.beginPath();
                        for (i = 0; i < drawSteps; i++) {
                            // Calculate the Bezier (x, y) coordinate for this step.
                            t = i / drawSteps;
                            tt = t * t;
                            ttt = tt * t;
                            u = 1 - t;
                            uu = u * u;
                            uuu = uu * u;
                            x = uuu * curve.startPoint.x;
                            x += 3 * uu * t * curve.control1.x;
                            x += 3 * u * tt * curve.control2.x;
                            x += ttt * curve.endPoint.x;
                            y = uuu * curve.startPoint.y;
                            y += 3 * uu * t * curve.control1.y;
                            y += 3 * u * tt * curve.control2.y;
                            y += ttt * curve.endPoint.y;
                            width = startWidth + ttt * widthDelta;
                            this._drawPoint(x, y, width);
                        }
                        ctx.closePath();
                        ctx.fill();
                    };
                    SignaturePad.prototype._strokeWidth = function (velocity) {
                        return Math.max(this.maxWidth / (velocity + 1), this.minWidth);
                    };
                    var Point = function (x, y, time) {
                        this.x = x;
                        this.y = y;
                        this.time = time || new Date().getTime();
                    };
                    Point.prototype.velocityFrom = function (start) {
                        return (this.time !== start.time) ? this.distanceTo(start) / (this.time - start.time) : 1;
                    };
                    Point.prototype.distanceTo = function (start) {
                        return Math.sqrt(Math.pow(this.x - start.x, 2) + Math.pow(this.y - start.y, 2));
                    };
                    var Bezier = function (startPoint, control1, control2, endPoint) {
                        this.startPoint = startPoint;
                        this.control1 = control1;
                        this.control2 = control2;
                        this.endPoint = endPoint;
                    };
                    // Returns approximated length.
                    Bezier.prototype.length = function () {
                        var steps = 10,
                                length = 0,
                                i, t, cx, cy, px, py, xdiff, ydiff;
                        for (i = 0; i <= steps; i++) {
                            t = i / steps;
                            cx = this._point(t, this.startPoint.x, this.control1.x, this.control2.x, this.endPoint.x);
                            cy = this._point(t, this.startPoint.y, this.control1.y, this.control2.y, this.endPoint.y);
                            if (i > 0) {
                                xdiff = cx - px;
                                ydiff = cy - py;
                                length += Math.sqrt(xdiff * xdiff + ydiff * ydiff);
                            }
                            px = cx;
                            py = cy;
                        }
                        return length;
                    };
                    Bezier.prototype._point = function (t, start, c1, c2, end) {
                        return start * (1.0 - t) * (1.0 - t) * (1.0 - t) +
                                3.0 * c1 * (1.0 - t) * (1.0 - t) * t +
                                3.0 * c2 * (1.0 - t) * t * t +
                                end * t * t * t;
                    };
                    return SignaturePad;
                })(document);
                var signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
                    backgroundColor: 'rgba(255, 255, 255, 0)',
                    penColor: 'rgb(0, 0, 0)',
                    velocityFilterWeight: .7,
                    minWidth: 0.5,
                    maxWidth: 2.5,
                    throttle: 16, // max x milli seconds on event update, OBS! this introduces lag for event update
                    minPointDistance: 3,
                });
                //                var saveButton = document.getElementById('save'),
                var clearButton = document.getElementById('clear');
                //                        showPointsToggle = document.getElementById('showPointsToggle');
                //                saveButton.addEventListener('click', function(event) {
                //                var data = signaturePad.toDataURL('image/png');
                //                window.open(data);
                //                });
                clearButton.addEventListener('click', function (event) {
                    signaturePad.clear();
                });
                showPointsToggle.addEventListener('click', function (event) {
                    signaturePad.showPointsToggle();
                    showPointsToggle.classList.toggle('toggle');
                });
            </script>

            <script>
                active = function (ele) {
                    var data = $(ele).data('id');
                    if (data === 'thisIsMe') {
                        $(ele).parents('.row').find('a').filter('.thisIsMe').removeClass('highlight');
                    } else {
                        $(ele).parents('.row').find('a').filter('.contact').removeClass('highlight');
                    }
                    $(ele).closest('a').toggleClass('highlight');
                };
            </script>

            <script>
                $(function () {
                    $('.chosen-select').chosen();
                    $('.chosen-select-deselect').chosen({allow_single_deselect: true});
                });
                $(document).ready(function () {
                    $(".toggal_other").hide();
                    $('.save-new-btn').show();
                    $('#cOccupation').hide();
                    var arrName = [];
                    var thisName;
                    $(window).keydown(function (event) {
                        if (event.keyCode === 13) {
                            event.preventDefault();
                            return false;
                        }
                    });
                });
                var date = new Date();
                var day = date.getDate();
                var month = date.getMonth();
                var year = date.getFullYear();
                var adultDOB = date.setFullYear(year - 18, month, day);
                $("#countryCode").intlTelInput_1();
                $("#countryCode2").intlTelInput_1();
                $("#countryCode3").intlTelInput_1();
                $("#countryCode4").intlTelInput_1();
                $("#countryCode5").intlTelInput_1();
                $(".countryname").intlTelInput_2();
                $(".countrynameexcludenz").intlTelInput_3();
                $(".countryCode").intlTelInput_1();
                $('#otp-block').hide();
                $('.removefile').hide();
                $('.inputtype').hide();
                $('.otherType').hide();
                $(document).ready(function () {
                    $('.passport-select').hide();
                    $('.other-select').hide();
                    $('.passport-select1').hide();
                    $('.other-select1').hide();
                    $('.yes-option').hide();
                    $('.yes-option1').hide();
                    $('.yes-option4').hide();
                    $('.selectno6').hide();
                    $('.des-togle').hide();
                    $('#mobileNo').keyup(function () {
                        $('#error-generateOtp').text('');
                    });
                    $('#otp').keyup(function () {
                        $('#error-generateOtp').text('');
                    });
                    $("#dob").datepicker({
                        yearRange: (year - 80) + ':' + year,
                        changeMonth: true,
                        changeYear: true,
                        'maxDate': new Date(adultDOB),
                        dateFormat: 'dd/mm/yy'
                    }).datepicker().attr('readonly', 'readonly');
                    $(".dob1").datepicker({
                        yearRange: (year) + ':' + (year + 80),
                        changeMonth: true,
                        changeYear: true,
                        'minDate': new Date(),
                        dateFormat: 'dd/mm/yy'
                    }).datepicker().attr('readonly', 'readonly');
                    $(".dob").datepicker({
                        yearRange: (year - 80) + ':' + year,
                        changeMonth: true,
                        changeYear: true,
                        'maxDate': new Date(adultDOB),
                        dateFormat: 'dd/mm/yy'
                    }).datepicker().attr('readonly', 'readonly');
                    $(".doc").datepicker({
                        yearRange: (year - 80) + ':' + year,
                        changeMonth: true,
                        changeYear: true,
                        'maxDate': new Date(),
                        dateFormat: 'dd/mm/yy'
                    }).datepicker().attr('readonly', 'readonly');
                    $('input:radio[name="senderType"]').change(function () {
                        var mNo = $('.mobile_number1').val();
                        var cCo = $('.mobile_country_code').val();
                        var sTy = $('.senderType:checked').val();
                        cCo = cCo.replace(/\+/g, "");
                        $('#otp-block').show();
                        document.getElementById('error-loader').style.display = 'none';
                        document.getElementById('error-generateOtp').innerHTML = "Please check your messages";
                        $('input:radio[name="senderType"]').prop("checked", false);
                    });
                    $('.otpkey').keyup(function () {
                        $('.spanverification').html("");
                        if ($('#verificationa').val().length === 6) {
                            var mNo = $('.mobile_number1').val();
                            var cCo = $('.mobile_country_code').val();
                            var otp = $('#verificationa').val();
                            cCo = cCo.replace(/\+/g, "");
                            validateOTP(mNo, cCo, otp);
                        }
                    });
                    $('input[name="term_condition"]:checked').change(function () {
                    });
                    $('#fourthfs-continue').click(function () {
                        var ele = $(this);
                        moveNextProcess(ele);
                    });
                });
                clearValidationId3 = function () {
                    $("#validationId3").html('');
                };
                isInviteCodeUsed = function (ele) {
                    var inviteCode = $('#inviteCode').val();
                    var url = './rest/cryptolabs/api/isInviteCode?ic=' + inviteCode;
                    $.ajax({
                        url: url,
                        type: "GET",
                        async: false,
                        success: function (response) {
                            if (response === 'true') {
                                $("#error-inviteCode").css({'color': 'green', 'margin-left': '10px'}).html('Invite Code is valid.');
                                $("#error-inviteCode").focus();
                                moveNextProcess(ele);
                                return true;
                            } else {
                                $("#error-inviteCode").css({'color': 'red', 'margin-left': '10px'}).html('Invite Code is used or invalid.');
                                $("#error-inviteCode").focus();
                                return false;
                            }
                        },
                        error: function (e) {
                            //handle error
                        }
                    });
                };
            </script>
            <script>
                $(".toggle-password").click(function () {
                    $(this).toggleClass("fa-eye fa-eye-slash");
                    var input = $($(this).attr("toggle"));
                    if (input.attr("type") === "password") {
                        input.attr("type", "text");
                    } else {
                        input.attr("type", "password");
                    }
                });
                var moreInvestorArr = [];
                var investors = [];
                var current_investor = {};
                var directors = [];
                var x = 0, y = 0, idx = 0;
                var checkfirstdir = 0;
                getMoreInvestorArr = function () {
                    var moreInvestorArray = new Array();
                    var moreInvestorInfoArr = document.getElementsByClassName('morestep1');
                    for (var i = 0; i < moreInvestorInfoArr.length - 1; i++) {
                        var s6x = 'step7' + i;
                        var s6xh = '#step7' + i;
                        var s6xh1 = s6xh + 1;
                        var s6xh2 = s6xh + 2;
                        var s6xh3 = s6xh + 3;
                        var s6xh4 = s6xh + 4;
                        var fname = $(s6xh1 + ' .director-name').text();
                        var email = $(s6xh1 + ' .more-director-email').val();
                        var send_email = '1';
                        var dob = $(s6xh2 + ' .more-director-dob').val();
                        var position = $(s6xh2 + ' .more-director-positionInCompany').val();
                        var countryname = $(s6xh2 + ' .more-director-countryname').val();
                        var occupation = $(s6xh2 + ' .more-director-select-occu').val();
                        var address = $(s6xh2 + ' .more-director-address').val();
                        var countrycode = $(s6xh2 + ' .more-director-countrycode').val();
                        var mobile = $(s6xh2 + ' .more-director-mobile').val();
                        var idType = $(s6xh3 + ' .src_of_fund2 option:selected').val();
                        var more_licence_first_name = $(s6xh3 + ' .more_licence_first_name').val();
                        var more_licence_middle_name = $(s6xh3 + ' .more_licence_middle_name').val();
                        var more_licence_last_name = $(s6xh3 + ' .more_licence_last_name').val();
                        var licenseNumber = $(s6xh3 + ' .more-director-licenseNumber').val();
                        var licenseExpiryDate = $(s6xh3 + ' .more-director-licenseExpiryDate').val();
                        var versionNumber = $(s6xh3 + ' .more-director-versionNumber').val();
                        var passportNumber = $(s6xh3 + ' .more-director-passportNumber').val();
                        var passportExpiryDate = $(s6xh3 + ' .more-director-passportExpiryDate').val();
                        var passportCountryOfIssue = $(s6xh3 + ' .more-director-passportCountryOfIssue').val();
                        var typeOfId = $(s6xh3 + ' .more-director-typeOfId').val();
                        var typeOfIdExpiryDate = $(s6xh3 + ' .more-director-typeOfIdExpiryDate').val();
                        var typeOfIdCountryOfIssue = $(s6xh3 + ' .more-director-typeOfIdCountryOfIssue').val();
                        var more_idverified = $(s6xh3 + ' .more_investor_verify').val();
                        var irdNumber = $(s6xh4 + ' .more-director-irdNumber').val();
                        var usCitizen = $(s6xh4 + ' .more-director-usCitizen').val();
                        var fs = document.getElementById(s6x + '4');
                        if (fs !== null && typeof fs !== "null") {
                            var countryArr = fs.getElementsByClassName('more-director-tex_residence_Country');
                            var TINArr = fs.getElementsByClassName('more-director-TIN');
                            var reasonArr = fs.getElementsByClassName('more-director-resn_tin_unavailable');
                            var countryTINList = new Array();
                            for (var j = 0; j < countryArr.length; j++) {
                                var country = countryArr[j].value;
                                var tin = TINArr[j].value;
                                var reason = reasonArr[j].value;
                                countryTINList.push({country: country, tin: tin, reason: reason});
                            }
                        }
                        var moreInvestor = {usCitizen: usCitizen, fname: fname, fullName: fname, email: email, send_email: send_email, dateOfBirth: dob, holderCountryOfResidence: countryname, occupation: occupation,
                            address: address, firstName: more_licence_first_name, lastName: more_licence_last_name, middleName: more_licence_middle_name, countryCode: countrycode, mobileNo: mobile, typeOfID: idType, licenseNumber: licenseNumber,
                            licenseExpiryDate: licenseExpiryDate, versionNumber: versionNumber, passportNumber: passportNumber,
                            passportExpiryDate: passportExpiryDate, countryOfIssue: passportCountryOfIssue, other_id_type: typeOfId, other_id_expiry: typeOfIdExpiryDate,
                            typeCountryOfIssue: typeOfIdCountryOfIssue, irdNumber: irdNumber, countryTINList: countryTINList, investor_idverified: more_idverified, positionInCompany: position};
                        console.log('[' + i + '] ' + JSON.stringify(moreInvestor));
                        moreInvestorArray.push(moreInvestor);
                    }
                    console.log(JSON.stringify(moreInvestorArray));
                    return moreInvestorArray;
                };
                function prev(ele) {
                    var currfs = $(ele).parent();
                    var prevfs = $(ele).parent().prev();
                    var cidc = currfs.attr('id');
                    var pidc = prevfs.attr('id');
                    var hasCls = prevfs.hasClass('more-director-fs');
                    if (cidc === 'step12' || cidc === 'morestep1' || cidc === 'morestep2' || cidc === 'morestep3' || cidc === 'morestep4') {
                        var idx = document.getElementsByClassName('morestep1').length - 2; //2=>1
                        var s6x = '#step7' + idx;
                        var chk = $(s6x + '1').find('input[type="radio"]:checked').val();
                        if (chk === '2') {
                            $(currfs).hide();
                            var btn = prevfs.find('input[name="previous"]');
                            $(s6x + '1').show();
                        } else if (chk === '1') {
                            $(currfs).hide();
                            $(s6x + '4').show();
                        } else {
                            $(currfs).hide();
                            $('#step7').show();
                        }
                    } else if (hasCls === true) {
                        pidc = pidc.substring(0, pidc.length - 1);
                        var chk = $('#' + pidc + '1').find('input[type="radio"]:checked').val();
                        if (chk === '2') {
                            var btn = prevfs.find('input[name="previous"]');
                            $(currfs).hide();
                            $('#' + pidc + '1').show();
                        } else {
                            $(currfs).hide();
                            $(prevfs).show();
                        }
                    } else {
                        $(currfs).hide();
                        $(prevfs).show();
                    }
                }
                function next(ele) {
                    var currfs = $(ele).parent();
                    var nextfs = $(ele).parent().next();
                    var idc = nextfs.attr('id');
                    var hasCls = currfs.hasClass('more-director-fs');
                    if (idc === 'morestep1') {
                        $(currfs).hide();
                        $('#step12').show();
                    } else if (hasCls === true) {
                        //                    alert(1);
                        idc = idc.substring(0, idc.length - 1);
                        var chk = $('#' + idc + '1').find('input[type="radio"]:checked').val();
                        if (chk === '2') {
                            var btn = nextfs.find('input[type="button"]');
                            $(currfs).hide();
                            next(btn);
                        } else {
                            $(currfs).hide();
                            $(nextfs).show();
                        }
                    } else {
                        $(currfs).hide();
                        $(nextfs).show();
                    }
                }
                function prev7(ele) {
                    $("#step").attr("value", 7);
                    prev(ele);
                }
                function prev8(ele) {
                    $("#step").attr("value", 8);
                    prev(ele);
                }
                function prev9(ele) {
                    $("#step").attr("value", 9);
                    prev(ele);
                }
                function prev10(ele) {
                    $("#step").attr("value", 10);
                    prev(ele);
                }
                function next8(ele) {
                    //                        alert("next8");
                    var curr = $(ele).parent();
                    var idc = '#' + curr.attr('id');
                    $("#step").attr("value", 9);
                    next(ele);
                }
                function next9(ele) {
                    //                         alert("step9");
                    var curr = $(ele).parent();
                    var idc = '#' + curr.attr('id');
                    //                var address = $(idc + ' .more-director-address').val();
                    //                var dob = $(idc + ' .more-director-dob').val();
                    //                 var occupation =$(idc + ' .more-director-select-occu option:selected').val();
                    //                var mobile = $(idc + ' .more-director-mobile').val();
                    //                var position = $(idc + ' .more-director-positionInCompany option:selected').val();
                    //                if (address === "") {
                    //                    $(idc + ' .more-director-address-error').text("This field is reqired");
                    //                } else if (mobile === "") {
                    //                    $(idc + ' .more-director-mobile-error').text("This field is reqired");
                    //                } else if (dob === "") {
                    //                    $(idc + ' .more-director-dob-error').text("This field is reqired");
                    //                } else if (occupation === "-Select-") {
                    //                            $(idc + ' .spanOccupation').text("This field is reqired");
                    //                 } else if (position === "0") {
                    //                    $(idc + ' .more-director-positionInCompany-error').text("This field is reqired");
                    //                } else {
                    $("#step").attr("value", 9);
                    next(ele);
                    //                }
                }
                function next10(ele) {
                    var curr = $(ele).parent();
                    //                var idc = '#' + curr.attr('id');
                    //                var url = '';
                    //                var firstName = '';
                    //                var middleName = '';
                    //                var lastName = '';
                    //                var License_number = '';
                    //                var licence_verson_number = '';                                                                             //                var licence_expiry_Date = '';
                    //                var passport_number = '';                                                         //                var passport_expiry = '';
                    //                var index = $(idc + " .src_of_fund2 option:selected").val();
                    //                if (index === "1") {
                    //                    url = './rest/groot/db/api/dl-verification';
                    //                    firstName = $(idc + " .more_licence_first_name").val();
                    //                    lastName = $(idc + " .more_licence_last_name").val();
                    //                    License_number = $(idc + " .more-director-licenseNumber").val();
                    //                    licence_expiry_Date = $(idc + " .more-director-licenseExpiryDate").val();
                    //                    licence_verson_number = $(idc + " .more-director-versionNumber").val();
                    //                    if (firstName === "") {
                    //                        $(idc + " .more_error_licence_first_name").text("This field is reqired ");
                    //                    } else if (lastName === "") {
                    //                        $(idc + " .more_error_licence_last_name").text("This field is reqired ");
                    //                    } else if (License_number === "") {
                    //                        $(idc + " .more-director-licenseNumber-error").text("This field is reqired ");
                    //                    } else if (Expirydate === "") {
                    //                        $(idc + " .more-director-licenseExpiryDate-error").text("This field is reqired ");
                    //                    } else if (licence_verson_number === "") {
                    //                        $(idc + " .more-director-versionNumber-error").text("This field is reqired ");
                    //                    } else {
                    //                        $("#step").attr("value", 11);
                    //                        next(ele);
                    //                    }
                    //                } else if (index === "2") {
                    //                    url = './rest/groot/db/api/pp-verification';
                    //                    firstName = $(idc + ' .more_passport_first_name').val();
                    //                    middleName = $(idc + ' .more_passport_middle_name').val();
                    //                    lastName = $(idc + ' .more_passport_last_name').val();
                    //                    passport_number = $(idc + " .more-director-passportNumber").val();
                    //                    passport_expiry = $(idc + ' .more-director-passportExpiryDate').val();
                    //                    if (passport_number === "") {
                    //                        $(idc + " .more-director-passportNumber-error").text("This field is reqired ");
                    //                    } else {
                    //                        $("#step").attr("value", 11);
                    //                        next(ele);
                    //                    }
                    //                } else {
                    //                    var TypeofID = $(idc + " .more-director-typeOfId").val();
                    //                    var Expirydate = $(idc + " .more-director-typeOfIdExpiryDate").val();
                    //                    var country = $(idc + " .more-director-typeOfIdCountryOfIssue").val();
                    //                    if (TypeofID === "") {
                    //                        $(idc + " .more-director-typeOfId-error").text("This field is reqired ");
                    //                    } else if (country === " -Select-") {
                    //                        $(idc + " .more-director-typeOfIdCountryOfIssue-error").text("This field is reqired ");
                    //                    } else {
                    $("#step").attr("value", 11);
                    next(ele);
                    //                    }
                    //                }
                    //                var Date_of_Birth = $(idc + ' .more-investor-dob').val();
                    //                if (index === "1" || index === "2") {
                    //                    var DataObj = {license_number: License_number, licence_expiry_Date: licence_expiry_Date,
                    //                        licence_verson_number: licence_verson_number, date_of_Birth: Date_of_Birth, firstName: firstName,
                    //                        middleName: middleName, lastName: lastName, passport_number: passport_number, passport_expiry: passport_expiry};
                    //                    console.log(DataObj);
                    //                    $.ajax({
                    //                        type: 'POST',
                    //                        url: url,
                    //                        headers: {"Content-Type": 'application/json'},
                    //                        data: JSON.stringify(DataObj),
                    //                        success: function (data, textStatus, jqXHR) {
                    //                            console.log(data);
                    //                            var obj = JSON.parse(data);
                    //                            if (index === "1") {
                    //                                if (obj.driversLicence.verified) {
                    //                                    $(idc + ' .more_director_verify').val('true');
                    //                                } else {
                    //                                    $(idc + ' .more_director_verify').val('false');
                    //                                }
                    //                            } else if (index === "2") {
                    //                                if (obj.passport.verified) {
                    //                                    $(idc + ' .more_director_verify').val('true');
                    //                                } else {
                    //                                    $(idc + ' .more_director_verify').val('false');
                    //                                }
                    //                            }
                    //                        },
                    //                        error: function (jqXHR, textStatus, errorThrown) {
                    ////                        alert(" inside error" + jqXHR);
                    //                        }
                    //                    });
                    //                }
                }
                function next11(ele) {
                    //                var curr = $(ele).parent();
                    //                var idc = '#' + curr.attr('id');
                    //                var idr = $(idc + ' .more-director-irdNumber').val();
                    //                  var country =  $(idc + ' .more-director-countryname').val();
                    //                 var USCitizen =  $(idc + " .more-director-usCitizen option:selected").val();
                    //                var j = 0;
                    //                if (country === " -Select-") {
                    //                     $(idc + ' .more-director-countryname-error').text("This field is reqired");
                    //                  }else if (idr === "") {
                    //                    $(idc + ' .more-director-irdNumber-error').text("This field is reqired");
                    //                } else if (idr.length !== 11) {
                    //                    $(idc + ' .more-director-irdNumber-error').text("9 Digit IDR no. Required");
                    //                }  else if (USCitizen === "2") {
                    //                             var tindivs = $(curr).find('.checktin3data');
                    //                            for (var i = 0; i < tindivs.length; i++) {
                    //                                var tindiv = tindivs[i];
                    //                                var tinnum = tindiv.getElementsByClassName('more-director-TIN')[0];
                    //                                var tinerror = tindiv.getElementsByClassName('more-director-TIN-error')[0];
                    //                                var country = tindiv.getElementsByClassName('excludenz')[0];
                    //                                var countryerror = tindiv.getElementsByClassName('excludenz-error')[0];
                    //                                var reason = tindiv.getElementsByClassName('more-director-resn_tin_unavailable')[0];
                    //                                var reasonerror = tindiv.getElementsByClassName('more-director-resn_tin_unavailable-error')[0];
                    //                                if (country.value === " -Select-") {
                    //                                    countryerror.innerHTML = "This field is reqired ";
                    //                                } else if (tinnum.value === "" && reason.value === "") {
                    //                                    reasonerror.innerHTML = "This field is reqired ";
                    //                                    tinerror.innerHTML = "This field is reqired ";
                    //                                } else {
                    //                                    j++;
                    //                                }
                    //                            }
                    //                            if (tindivs.length > j) {
                    //
                    //                            } else {
                    //                                 $("#step").attr("value", 12);
                    //                                 next(ele);
                    //                            }
                    //                        } else {
                    $("#step").attr("value", 12);
                    next(ele);
                    //                }
                }
                $('.selectoption2').change(function () {
                    var val = $(".selectoption2 option:selected").val();
                    if (val === "1") {
                        $('.yes-option1').hide();
                    }
                    if (val === "2") {
                        $('.yes-option1').show();
                    }
                });
            </script>
            <script>
                var x = 0;
                $(".previous1").click(function () {
                    $("#step").attr("value", 1);
                    $("#step2").hide();
                    $("#step1").show();
                });
                $(".previous2").click(function () {
                    $("#step").attr("value", 2);
                    $("#step3").hide();
                    $("#step2").show();
                });
                $(".previous3").click(function () {
                    $("#step").attr("value", 3);
                    $("#step4").hide();
                    $("#step3").show();
                });
                $(".previous4").click(function () {
                    $("#step").attr("value", 4);
                    $("#step6").hide();
                    $("#step4").show();
                });
                $(".previous4").click(function () {
                    $("#step").attr("value", 4);
                    $("#step5").hide();
                    $("#step4").show();
                });
                $(".previous5").click(function () {
                    $("#step").attr("value", 5);
                    $("#step6").hide();
                    $("#step5").show();
                });
                $(".previous6").click(function () {
                    $("#step").attr("value", 6);
                    $("#step7").hide();
                    $("#step5").show();
                });
                $(".previous11").click(function (event) {
                    $("#step").attr("value", 11);
                    prev(event.target);
                });
                $(".previous12").click(function () {
                    $("#step").attr("value", 12);
                    $("#step13").hide();
                    $("#step12").show();
                });
                $(".previous13").click(function () {
                    $("#step").attr("value", 13);
                    $("#step14").hide();
                    $("#step13").show();
                });
                $(".previous14").click(function () {
                    $("#step").attr("value", 14);
                    $("#step15").hide();
                    $("#step14").show();
                });
                $(".previous15").click(function () {
                    $("#step").attr("value", 15);
                    $("#step16").hide();
                    $("#step15").show();
                });
                $(".next1").click(function () {
                    //                var companyName = $("#companyName").val();
                    //                companyName = companyName.trim();
                    //                var countryname = $("#Country_of_incorporation").val();
                    //                countryname = countryname.trim();
                    //                var companyDate = $("#Date_of_incorporation").val();
                    //                companyDate = companyDate.trim();
                    //                var companyRegistration = $("#registrationNumber").val();
                    //                companyRegistration = companyRegistration.trim();
                    //                var companyAddress = $("#companyAddress").val();
                    //                companyAddress = companyAddress.trim();
                    //                var type = $(".selectType option:selected").val();
                    //                var file = $("#attachdeedfile").val();
                    //                if (companyName === "") {
                    //                    $("#spanCompanyName").text("This field is reqired ");
                    //                }else if (companyAddress === "") {
                    //                    $("#spanCompanyAddress").text("This field is reqired ");
                    //                }else if (countryname === "-Select-") {
                    //                    $("#spanCountryCode").text("This field is reqired ");
                    //                } else if (companyDate === "") {
                    //                    $("#spanCompanyDate").text("This field is reqired ");
                    //                } else if (type === "1") {
                    //                    $("#spanTypeTrust").text("This field is reqired ");
                    //                } else if (companyRegistration === "") {
                    //                    $("#spanRegistrationNumber").text("This field is reqired ");
                    //                } else if (file === "") {
                    //                    $(".error_attachdeedfile").text("Please attach a copy of document");
                    //                }else {
                    $("#step").attr("value", 2);
                    $("#step2").show();
                    $("#step1").hide();
                    //                }
                });
                $(".next2").click(function () {
                    var directorDivs1 = document.getElementsByClassName('director-add');
                    //                  alert("1");
                    var j = 0;
                    var k = 0;
                    //                for (var i = 0; i < directorDivs1.length; i++) {
                    //                    var directorDiv1 = directorDivs1[i];
                    //                    var fnameInput1 = directorDiv1.getElementsByClassName('fname')[0];
                    //                    var spanfname = directorDiv1.getElementsByClassName('spanfname')[0];
                    //                    if (fnameInput1.value.trim() === "") {
                    //                        spanfname.innerHTML = "This field is reqired ";
                    //                    }else{
                    //                        k++;
                    //                    }
                    //                }
                    //               if(directorDivs1.length<=k){
                    for (var i = 0; i < directorDivs1.length; i++) {
                        var moreInvestor1 = directorDivs1[i];
                        var fnameInput1 = moreInvestor1.getElementsByClassName('fname')[0];
                        var meInput = moreInvestor1.getElementsByClassName('cls-me')[0];
                        if (meInput.value === 'Y') {
                            var fn = fnameInput1.value;
                            $('.this-name').text(fn);
                            var fnarr = fn.split(" ");
                            $('#step6 .first_name').val(fnarr[0]);
                            if (fnarr.length === 3) {
                                $('#step6 .middle_name').val(fnarr[1]);
                                $('#step6 .last_name').val(fnarr[2]);
                            } else if (fnarr.length === 2) {
                                $('#step6 .last_name').val(fnarr[1]);
                            }
                        } else {
                            //                    var emailInput1 = moreInvestor1.getElementsByClassName('emailAddress')[0];
                            //                    var checkradio = moreInvestor1.querySelector('input[type="radio"]:checked');
                            var m1 = document.getElementById('morestep1');
                            var m2 = document.getElementById('morestep2');
                            var m3 = document.getElementById('morestep3');
                            var m4 = document.getElementById('morestep4');
                            var s6x = 'step7' + j;
                            var idc = '#' + s6x;
                            var s6x1 = s6x + 1;
                            var s6x1ele = document.getElementById(s6x1);
                            if (s6x1ele === null || typeof s6x1ele === 'undefined') {
                                var m1c = m1.cloneNode(true);
                                var m2c = m2.cloneNode(true);
                                var m3c = m3.cloneNode(true);
                                var m4c = m4.cloneNode(true);
                                m1c.id = s6x + 1; //601
                                m2c.id = s6x + 2; //602
                                m3c.id = s6x + 3; //603
                                m4c.id = s6x + 4; //604
                                var changeid1 = "firstid" + j;
                                var changeid2 = "secondid" + j;
                                m1c.getElementsByClassName("checkradio")[0].removeAttribute("id");
                                m1c.getElementsByClassName("radio1")[0].setAttribute("id", changeid1);
                                m1c.getElementsByClassName("radio2")[0].setAttribute("id", changeid2);
                                m1c.getElementsByClassName("radio1")[0].setAttribute("name", changeid1);
                                m1c.getElementsByClassName("radio2")[0].setAttribute("name", changeid1);
                                m1c.getElementsByClassName("forlabel1")[0].setAttribute("for", changeid1);
                                m1c.getElementsByClassName("forlabel2")[0].setAttribute("for", changeid2);
                                m1c.classList.add("more-director-info");
                                m1c.classList.add("more-director-fs");
                                m2c.classList.add("more-director-info");
                                m2c.classList.add("more-director-fs");
                                m3c.classList.add("more-director-info");
                                m3c.classList.add("more-director-fs");
                                m4c.classList.add("more-director-info");
                                m4c.classList.add("more-director-fs");
                                if (j === 0) {
                                    $("#step7").after(m1c);
                                } else {
                                    var s6x4 = "#step7" + (j - 1) + "4";
                                    $(s6x4).after(m1c);
                                }
                                $(m1c).after(m2c);
                                $(m2c).after(m3c);
                                $(m3c).after(m4c);
                                //                        alert("4");
                                var fn = fnameInput1.value;
                                var fnarr = fn.split(" ");
                                $(idc + 3 + ' .first_name').val(fnarr[0]);
                                if (fnarr.length === 3) {
                                    $(idc + 3 + ' .middle_name').val(fnarr[1]);
                                    $(idc + 3 + ' .last_name').val(fnarr[2]);
                                } else if (fnarr.length === 2) {
                                    $(idc + 3 + ' .last_name').val(fnarr[1]);
                                }
                            }
                            $(idc + 1 + " .director-name").text(fnameInput1.value);
                            $(idc + 2 + " .director-name").text(fnameInput1.value);
                            $(idc + 3 + " .director-name").text(fnameInput1.value);
                            $(idc + 4 + " .director-name").text(fnameInput1.value);
                            //                    alert("5");
                            //                    $(idc + 1 + " .more-investor-fname").val(fnameInput1.value);
                            //                    $(idc + 1 + " .more-investor-email").val(emailInput1.value);                                                         //                    $(idc + 1 + " .more-investor-radio").val(checkradio.value);
                            //                    $(idc + 1 + " .more-investor-preferred-name").val(fnameInput1.value);
                            $(idc + 2 + " .more-director-dob").datepicker({
                                yearRange: (year - 80) + ':' + year,
                                changeMonth: true,
                                changeYear: true,
                                'maxDate': new Date(adultDOB),
                                dateFormat: 'dd/mm/yy'
                            }).datepicker().attr('readonly', 'readonly');
                            $(idc + 3 + " .more-director-exp").datepicker({
                                yearRange: (year) + ':' + (year + 80),
                                changeMonth: true,
                                changeYear: true,
                                'minDate': new Date(),
                                dateFormat: 'dd/mm/yy'
                            }).datepicker().attr('readonly', 'readonly');
                            $(idc + 2 + " .more-director-countrycode").intlTelInput_1();
                            $(idc + 4 + " .more-director-countryname").intlTelInput_2();
                            $(idc + 3 + " .more-director-countryname").intlTelInput_2();
                            $(idc + 4 + " .excludenz").intlTelInput_3();
                            new google.maps.places.Autocomplete(
                                    ($(idc + 2 + " .more-director-address")[0]),
                                    {types: ['address'], componentRestrictions: {country: 'nz'}});
                            j++;
                        }
                    }
                    $("#step").attr("value", 3);
                    $("#step3").show();
                    $("#step2").hide();
                    //               }
                });
                $(".next3").click(function () {
                    //                var Date_of_Birth = $("#Date_of_Birth").val();
                    //                Date_of_Birth = Date_of_Birth.trim();
                    //                var OccupationOption = $('.OccupationOption').val();
                    //                var Occupation = $("#cOccupation").val();
                    //                Occupation = Occupation.trim();
                    //                var holderCountryOfResidence = $("#holderCountryOfResidence").val();
                    //                holderCountryOfResidence = holderCountryOfResidence.trim();
                    //                var positionInCompany = $("#positionInCompany").val();
                    //                positionInCompany = positionInCompany.trim();
                    //                //                holderCountryOfResidence = holderCountryOfResidence.trim();
                    //                if (Date_of_Birth === "") {
                    //                    $("#spanDate_of_Birth").text("This field is reqired ");
                    //                } else if (OccupationOption === "-Select-" || OccupationOption === "other" && Occupation === "") {
                    //                    $("#spanOccupation").text("This field is reqired ");
                    //                } else if (holderCountryOfResidence === "-Select-") {
                    //                    $("#spanHolderCountryOfResidence").text("This field is reqired ");
                    //                } else if (positionInCompany === "-Select-") {
                    //                    $("#error-positionInCompany").text("This field is reqired ");
                    //                } else {
                    var thisName = "";
                    var root = $('.thisIsMe').parents('.row');
                    var thisIsMeDiv = $(root).find('.highlight').filter('.thisIsMe');
                    var trusteeName = $(thisIsMeDiv).parents('.addss').find('.continer3Dob').val();
                    setAllNames(trusteeName);
                    let closeDiv = $("#step3").find(".closeSet");
                    var thisme = closeDiv.find(".thisIsMe");
                    for (var i = 0; i < thisme.length; i++) {
                        if (thisme.eq(i).is(".highlight")) {
                            let cloneDiv = thisme.eq(i).closest(".cloneName");
                            thisName = cloneDiv.find(".continer3Dob").val();
                        } else {
                            let cloneDiv = thisme.eq(i).closest(".cloneName");
                            var oName = cloneDiv.find(".continer3Dob").val();
                            otherName.push(oName);
                        }
                    }
                    $.each(otherName, function (index, value) {
                        // Get value in alert  
                        //                $.find(".otherInvester:first").removeAttr("style");
                        $(".otherInvester:first").find(".firstname").html(value);
                        $(".otherInvester:first").clone().appendTo(".investAppend");

                    });
                    $(".otherInvester:first").attr("style", "display:none");
                    $(".investerName").html(thisName);
                    $("#step").attr("value", 4);
                    $("#step4").show();
                    $("#step3").hide();
                    //                }
                });
                setAllNames = function (fullName) {
                    var allName = fullName.toString().split(' ');
                    if (allName.length === 1) {
                        $('.first_name').val(allName[0]);
                    } else if (allName.length === 2) {
                        $('.first_name').val(allName[0]);
                        $('.last_name').val(allName[1]);
                    } else {
                        $('.first_name').val(allName[0]);
                        $('.middle_name').val(allName[1]);
                        $('.last_name').val(allName[2]);
                    }
                };
                $(".next4").click(function () {
                    //                var address = $("#address").val();
                    //                address = address.trim();
                    //                var OPmobileNo = $("#mobileNo").val();
                    //                OPmobileN = OPmobileNo.trim();                                                                                     //                if (address === "") {
                    //                    $("#spanHomeaddress").text("This field is reqired ");
                    //                } else if (OPmobileNo === "") {
                    //                    $("#spanOPmobileNo").text("This field is reqired ");
                    //                } else {
                    //                    
//                    for (var i = 0; i < x.length; i++) {
//                        if (x.eq(i).is(":none")) {
//                            alert("The paragraph  is visible.");
//                        } else {
//                            alert("The paragraph  is hidden.");
//                        }
//                    }
//                    var x = $.find(".otherInvester").length;
//                    if (x === 1) {
//                        $("#step7").show();
//                        $("#step4").hide();
//                    } else {
                    $("#step").attr("value", 5);
                    $("#step5").show();
                    $("#step4").hide();
//                    }

                    //                }
                });
                function setName(ele) {
                    var closeDiv = $(ele).closest(".otherInvester");
                    closeDiv.remove();
                    var name = closeDiv.find(".firstname").html();
                    $(".investerName").html(name);
                    $("#step4 .trusteeAddresses").val("");
                    $("#step4 .codename").val("");
                    $("#step4 .selected-flag div.flag").removeAttr('class').attr('class', 'flag nz');
                    $("#step4 #countryCode").val("+64");
                    $("#step4 #Date_of_incorporation").val("");
                    $("#step5 .dob1").val("");
                    $("#step4 .pac-target-input").val("");
                    $("#step4 .pac-target-input").val("");
                    $("#step4 #irdNumber").val("");
                    $("#step4 .countryname").val("-Select-");
                    $("#step4 div.country-set select").val("Select");
                    $("#step5 .removeSet").val("");
                    $("#step5 .removeCountrySet").val("-Select-");
                    setAllNames(name);
                    $("#step6").hide();
                    $("#step4").show();
                }
                $(".next5").click(function () {
                    //                var verificationa = $("#verificationa").val();
                    //                verificationa = verificationa.trim();
                    //                if (verificationa === "") {
                    //                    $("#spanverificationa").text("This field is reqired ");
                    //                } else {
                    $.each(otherName, function (index, value) {
                        // Get value in alert  
                    });

                    var x = $.find(".otherInvester").length;
                    if (x === 1) {
                        $("#step7").show();
                        $("#step5").hide();
                    } else {
                        $("#step").attr("value", 5);
                        $("#step6").show();
                        $("#step5").hide();
                    }
//                    $("#step").attr("value", 6);
//                    $("#step6").show();
//                    $("#step5").hide();
                    //                }
                });
                $('#typeExpirydate').change(function () {
                    $('#spanPExpirydate').text('');
                });
                $("#Countryofissue").change(function () {
                    $('#spanCountryofissue').text('');
                });
                $(".next6").click(function () {
                    //                var url = '';
                    //                var firstName = '';
                    //                var middleName = '';
                    //                var lastName = '';
                    //                var License_number = '';
                    //                var licence_verson_number = '';
                    //                var licence_expiry_Date = '';
                    //                var passport_number = '';
                    //                var passport_expiry = '';
                    //                var index = $("#src_of_fund1 option:selected").val();
                    //                if (index === "1") {
                    //                    url = './rest/groot/db/api/dl-verification';
                    //                    firstName = $("#licence_first_name").val().trim();
                    //                    lastName = $("#licence_last_name").val().trim();
                    //                    License_number = $("#licenseNumber").val();
                    //                    License_number = License_number.trim();
                    //                    var Expirydate = $("#Expirydate").val();
                    //                    Expirydate = Expirydate.trim();
                    //                    licence_verson_number = $("#versionNumber").val();
                    //                    licence_verson_number = licence_verson_number.trim();
                    //                    if (firstName === "") {
                    //                        $("#error_licence_first_name").text("This field is reqired ");
                    //                    } else if (lastName === "") {
                    //                        $("#error_licence_last_name").text("This field is reqired ");
                    //                    } else if (License_number === "") {
                    //                        $("#spanlicenseNumber").text("This field is reqired ");
                    //                    } else if (Expirydate === "") {
                    //                        $("#spanExpirydate").text("This field is reqired ");
                    //                    } else if (licence_verson_number === "") {
                    //                        $("#spanVersionnumber").text("This field is reqired ");
                    //                    } else {
                    //                        $("#step").attr("value", 7);
                    //                        $("#step7").show();
                    //                        $("#step6").hide();
                    //                    }
                    //                } else if (index === "2") {
                    //                    url = './rest/groot/db/api/pp-verification';
                    //                    firstName = $("#passport_first_name").val().trim();
                    //                    lastName = $("#passport_last_name").val().trim();
                    //                    passport_number = $("#Passportnumber").val();
                    //                    passport_expiry = $("#PExpirydate").val();
                    //                    var CountryOfIssue = $("#Countryofissue").val();
                    //                    passport_number = passport_number.trim();
                    //                    if (firstName === "") {
                    //               $("#error_passport_first_name").text("This field is reqired ");
                    //                    } else if (lastName === "") {
                    //                        $("#error_passport_last_name").text("This field is reqired ");
                    //                    } else if (passport_number === "") {
                    //                        $("#spanPassportnumber").text("This field is reqired ");
                    //                    } else if (passport_expiry === "") {
                    //                        $("#spanPExpirydate").text("This field is reqired ");
                    //                    } else if (CountryOfIssue === " -Select-") {
                    //                        $("#spanCountryofissue").text("This field is reqired ");
                    //                    } else {
                    //                        $("#step").attr("value", 7);
                    //                        $("#step7").show();
                    //                        $("#step6").hide();
                    //                    }
                    //                } else {
                    //                    var TypeofID = $("#TypeofID").val();
                    //                    var ExpirydateOther = $("#typeExpirydate").val();
                    //                    var countryname1 = $("#countryname1").val();
                    //                    var otherDocument = $("#otherDocument").val();
                    //                    TypeofID = TypeofID.trim();
                    //                    if (TypeofID === "") {
                    //                        $("#spanTypeofID").text("This field is reqired ");
                    //                    } else if (countryname1 === " -Select-") {
                    //                        $("#span_countryname1").text("This field is reqired ");
                    //                    } else if (otherDocument === "") {
                    //                        $("#span-otherDocument").text("This field is reqired ");
                    //                    } else {
                    $("#step").attr("value", 7);
                    $("#step7").show();
                    $("#step6").hide();
                    //                    }
                    //                }
                    //                var Date_of_Birth = $('#Date_of_Birth').val();
                    //                var DataObj = {license_number: License_number, licence_expiry_Date: licence_expiry_Date,
                    //                    licence_verson_number: licence_verson_number, date_of_Birth: Date_of_Birth, firstName: firstName,
                    //                    middleName: middleName, lastName: lastName, passport_number: passport_number, passport_expiry: passport_expiry};
                    //                console.log(DataObj);
                    //                $.ajax({
                    //                    type: 'POST',
                    //                    url: url,
                    //                    headers: {"Content-Type": 'application/json'},
                    //                    data: JSON.stringify(DataObj),
                    //                    success: function (data, textStatus, jqXHR) {
                    //                        console.log(data);
                    //                        var obj = JSON.parse(data);
                    //                        if (index === "1") {
                    //                            if (obj.driversLicence.verified) {
                    //                                $('#investor_verify').val('true');
                    //                            } else {
                    //                                $('#investor_verify').val('false');
                    //                            }
                    //                        } else if (index === "2") {
                    //                            if (obj.passport.verified) {
                    //                                $('#investor_verify').val('true');
                    //                            } else {
                    //                                $('#investor_verify').val('false');
                    //                            }
                    //                        }
                    //                    },
                    //                    error: function (jqXHR, textStatus, errorThrown) {
                    ////                        alert(" inside error" + jqXHR);
                    //                    }
                    //                });
                });
                $(".next7").click(function (event) {
                    $('#nameOfTrust').text($('#companyName').val());
                    //                var i = $('#counter').val();
                    //                var IRDNumber = $("#irdNumber").val().trim();
                    //                var USCitizen = $("#USCitizen option:selected").val();
                    //                var j = 0;
                    //                if (IRDNumber === "") {
                    //                    $("#spanIRDNumber").text("This field is reqired ");
                    //                } else if (IRDNumber.length !== 11) {
                    //                    $("#spanIRDNumber").text("9 Digit IDR no. Required");
                    //                } else if (USCitizen === "2") {
                    //                    var tindivs = document.getElementsByClassName('checktindata');
                    //                    for (var i = 0; i < tindivs.length; i++) {
                    //                        var tindiv = tindivs[i];
                    //                        var tinnum = tindiv.getElementsByClassName('TIN')[0];
                    //                        var tinerror = tindiv.getElementsByClassName('tin-error')[0];
                    //                        var country = tindiv.getElementsByClassName('countrynameexcludenz')[0];
                    //                        var countryerror = tindiv.getElementsByClassName('countrynameexcludenz-error')[0];
                    //                        var reason = tindiv.getElementsByClassName('resn_unavailable')[0];
                    //                        var reasonerror = tindiv.getElementsByClassName('resn_unavailable-error')[0];
                    //                        if (country.value === " -Select-") {
                    //                            countryerror.innerHTML = "This field is reqired ";
                    ////                            alert("country span");
                    //                        } else if (tinnum.value === "" && reason.value === "") {
                    //                            tinerror.innerHTML = "This field is reqired ";
                    //                            reasonerror.innerHTML = "This field is reqired ";
                    //                        } else {
                    //                            j++;
                    //                        }
                    //                    }
                    //                    if (tindivs.length > j) {
                    ////                            alert("lenght grater");
                    //                    } else {
                    //                        tinfornext7(event.target);
                    //                    }
                    //                } else {
                    tinfornext7(event.target);
                    //                }
                });
                $(".next12").click(function () {

                    //                var IRDNumber = $("#comIRDNumber").val().trim();
                    //                var source = $("#sourceOfFunds option:selected").val();
                    //                if (IRDNumber === "") {
                    //                    $("#spancomIRDNumber").text("field should be of 9 degits");
                    //                } else if (source === "1") {
                    //                    $("#sourceOfFunds-error").text("This field is reqired ");
                    //                } else {
                    $("#step").attr("value", 13);
                    $("#step13").show();
                    $("#step12").hide();
                    //                }
                });
                $(".next13").click(function () {
                    //                var firstselect = $('#isCompanyFinancialInstitution option:selected').val();
                    //                var secondselect = $('#isCompanyUSCitizen option:selected').val();
                    //                var j = 0;
                    //                if (firstselect === "0") {
                    //                    $("#isCompanyFinancialInstitution-error").text("This field is reqired ");
                    //                } else if (secondselect === "0") {
                    //                    $("#isCompanyUSCitizen-error").text("This field is reqired ");
                    //                } else if (secondselect === "2") {
                    //                    var tindivs = document.getElementsByClassName('checktin4data');
                    //                    for (var i = 0; i < tindivs.length; i++) {
                    //                        var tindiv = tindivs[i];
                    //                        var tinnum = tindiv.getElementsByClassName('companyTaxIdentityNumber')[0];
                    //                        var tinerror = tindiv.getElementsByClassName('companyTaxIdentityNumber-error')[0];
                    //                        var reason = tindiv.getElementsByClassName('companyReasonTIN')[0];
                    //                        var reasonerror = tindiv.getElementsByClassName('companyReasonTIN-error')[0];
                    //                        var country = tindiv.getElementsByClassName('countrynameexcludenz')[0];
                    //                        var countryerror = tindiv.getElementsByClassName('countrynameexcludenz-error')[0];
                    //                        if (country.value === " -Select-") {
                    //                            countryerror.innerHTML = "This field is reqired ";
                    //                        } else if (tinnum.value === "" && reason.value === "") {
                    //                            tinerror.innerHTML = "This field is reqired ";
                    //                            reasonerror.innerHTML = "This field is reqired ";
                    //
                    //                        } else {
                    //                            j++;
                    //                        }
                    //                    }
                    //                    if (tindivs.length > j) {
                    //
                    //                    } else {
                    //                        $("#step").attr("value", 14);
                    //                        $("#step14").show();
                    //                        $("#step13").hide();
                    //                    }
                    //                } else {
                    $("#step").attr("value", 14);
                    $("#step14").show();
                    $("#step13").hide();
                    //                }

                });
                $('.bank_name').change(function () {
                    var data = $('.bank_name option:selected').data('id');
                    $('#accountNumber').val(data);
                });
                $(".next14").click(function () {
                    //                var bank_name = $('.bank_name option:selected').text();
                    //                bank_name = bank_name.trim();
                    //                var other_name = $("#other_other_name").val();
                    //                var accountNumber = $("#accountNumber").val().trim();
                    //                var nameOfAccount = $("#nameOfAccount").val().trim();
                    //                var attachbankfile = $("#attachbankfile").val().trim();
                    //                if (bank_name === "– Select –") {
                    //                    $("#error_bank_name").text("This field is reqired ");
                    //                } else if (bank_name === "Other" && other_name === "") {
                    //                    $("#error_other_bank_name").text("This field is reqired ");
                    //                } else if (nameOfAccount === "") {
                    //                    $("#spannameOfAccount").text("This field is reqired ");
                    //                } else if (accountNumber === "") {
                    //                    $("#spanaccountNumber").text("This field is reqired ");
                    //                } else if (attachbankfile === "") {
                    //                    $(".error_attachbankfile").text("Please attach verification of your bank account.");
                    //                } else {
                    $("#step").attr("value", 15);
                    $("#step15").show();
                    $("#step14").hide();
                    //                }
                });
                $(".next15").click(function () {
                    //                var y = $('input[name=authorized_condition]').is(':checked');
                    //                var x = $('input[name=term_condition]').is(':checked');
                    //                if (!x) {
                    //                    $("#error_term_condition").text("Please read and agree to the Terms and Conditions");
                    //                } else if (!y) {
                    //                    $("#error_authorized_condition").text("I am authorised to act and accept on behalf of all account holders");
                    //                } else {
                    $("#step").attr("value", 15);
                    $("#step16").show();
                    $("#step15").hide();
                    //                }
                });
                $(".companyaccount").click(function () {
                    x = $('[name="step"]').val();
                    $("#step").attr("value", parseInt(x) + 1);
                    $("#step10").show();
                    $("#step2").hide();
                });
                $('.bank_name').change(function () {
                    //                alert('other');
                    if ($('.bank_name option:selected').val() === 'Other') {
                        $(".toggal_other").show();
                    } else {
                        $(".toggal_other").hide();
                    }
                });</script>
            <script>
                $('#sourceOfFunds').change(function () {
                    var val = $("#sourceOfFunds option:selected").val();
                    if (val === "7") {
                        $('.des-togle').show();
                    }
                    if (val === "6") {
                        $('.des-togle').hide();
                    }
                    if (val === "5") {
                        $('.des-togle').hide();
                    }
                    if (val === "4") {
                        $('.des-togle').hide();
                    }
                    if (val === "3") {
                        $('.des-togle').hide();
                    }
                    if (val === "2") {
                        $('.des-togle').hide();
                    }
                    if (val === "1") {
                        $('.des-togle').hide();
                    }
                    if (val === "0") {
                        $('.des-togle').hide();
                    }
                });
                $('#src_of_fund1').change(function () {
//                    var firstName = $('.first_name').val();
//                    var middelName = $('.middle_name').val();
//                    var lstName = $('.last_name').val();
                    var val = $("#src_of_fund1 option:selected").val();
                    if (val === "1") {
                        $('.passport-select').hide();
                        $('.other-select').hide();
                        $('.verifybtn').show();
                        $('.drivery-licence').show();
//                        $('.first_name').val(firstName);
//                        $('.middle_name').val(middelName);
//                        $('.last_name').val(lstName);
                    }
                    if (val === "2") {
                        $('.other-select').hide();
                        $('.drivery-licence').hide();
                        $('.verifybtn').show();
                        $('.passport-select').show();
//                        $('.first_name').val(firstName);
//                        $('.middle_name').val(middelName);
//                        $('.last_name').val(lstName);
                    }
                    if (val === "3") {
                        $('.verifybtn').hide();
                        $('.passport-select').hide();
                        $('.drivery-licence').hide();
                        $('.other-select').show();
//                        $('.first_name').val(firstName);
//                        $('.middle_name').val(middelName);
//                        $('.last_name').val(lstName);
                    }
                });
                $('.selectoption1').change(function () {
                    var val = $(".selectoption1 option:selected").val();
                    if (val === "1") {
                        $('.yes-option').hide();
                    }
                    if (val === "2") {
                        $('.yes-option').show();
                    }
                });
                $('.selectoption3').change(function () {
                    var val = $(".selectoption3 option:selected").val();
                    if (val === "7") {
                        $('.inputtype').show();
                    } else {
                        $('.inputtype').hide();
                    }

                });
                $('.selectoption6').change(function () {
                    var val = $(".selectoption6 option:selected").val();
                    if (val === "2") {
                        $('.selectno6').show();
                    } else {
                        $('.selectno6').hide();
                    }

                });
                $('.selectoption4').change(function () {
                    var val = $(".selectoption4 option:selected").val();
                    if (val === "1") {
                        $('.yes-option4').hide();
                    }
                    if (val === "2") {
                        $('.yes-option4').show();
                    }

                });</script>
            <script>
                $(document).ready(function () {
                    $("#add-country-another").click(function () {
                        $(".yes-new").append("<div class='row checktindata removecountry'><div class='col-sm-6' ><label class='label_input' style='text-align:left'>Country of tax residence:</label></div><div class='col-sm-6 details-pos flag-drop' onclick='changecoutry(this);'><input type='text' class='form-control countrynameexcludenz'  id='countryname' name='countryCode' placeholder='Enter Country Code' readonly='readonly'> <span class='error countrynameexcludenz-error' ></span></div><div class='col-sm-6'><label class='label_input'>Tax Identification Number (TIN)</label></div><div class='col-sm-6'><input type='text' class='form-control input-field TIN' name='fullName' required='required' placeholder='Enter TIN' onkeyup='removetinspan()' ><span class='error tin-error' ></span> </div><div class='col-sm-6'><label class='label_input'> Reason if TIN not available </label></div><div class='col-sm-6'><input type='text' class='form-control input-field resn_unavailable' name='fullName' required='required' placeholder='' onkeyup='removetinspan();'/><span class='error resn_unavailable-error' ></span><a href='javascript:void(0);' onclick='removecountrydiv(this);'><i class='far fa-times-circle'></i></a></div></div>");
                        $(".countrynameexcludenz").intlTelInput_3();
                    });
                    $("#add-country-another4").click(function () {
                        $(".yes-new4").append("<div class='row checktin4data removecountry'><div class='col-sm-6' ><label class='label_input' style='text-align:left'>Country of tax residence:</label></div><div class='col-sm-6 details-pos flag-drop' onclick='changecoutry(this);'><input type='text' class='form-control countrynameexcludenz'  id='countryname' name='countryCode' placeholder='Enter Country Code' readonly='readonly'><span class='error countrynameexcludenz-error' ></span></div><div class='col-sm-6'><label class='label_input'>Tax Identification Number (TIN) </label></div><div class='col-sm-6'><input type='text' class='form-control input-field companyTaxIdentityNumber' name='fullName' required='required' placeholder='Enter TIN' onkeyup='removetinspan()' /><span class='error companyTaxIdentityNumber-error' ></span></div><div class='col-sm-6'><label class='label_input'> Reason if TIN not available </label></div><div class='col-sm-6'><input type='text' class='form-control input-field companyReasonTIN' name='fullName' required='required' placeholder='' onkeyup='removetinspan();'/><span class='error companyReasonTIN-error' ></span><a href='javascript:void(0);' onclick='removecountrydiv(this);'><i class='far fa-times-circle'></i></a></div></div>");
                        $(".countrynameexcludenz").intlTelInput_3();
                    });
                    $("#add-country-another3").click(function () {
                        $(".yes-new3").append("<div class='row yes-new3-clone checktin3data removecountry'><div class='col-sm-6' ><label class='label_input' style='text-align:left'>Country of tax residence:</label></div><div class='col-sm-6 details-pos flag-drop' onclick='changecoutry(this);'><input type='text' class='form-control countrynameexcludenz more-investor-tex_residence_Country'  name='countryCode' placeholder='Enter Country Code' readonly='readonly'><span class='error countrynameexcludenz-error' ></span></div><div class='col-sm-6'><label class='label_input'>Tax Identification Number (TIN)</label></div><div class='col-sm-6 '><input type='text' class='form-control input-field more-investor-TIN' name='fullName' required='required' placeholder='Enter TIN' onkeyup='removetinspan()'/><span class='error more-investor-TIN-error' ></span></div><div class='col-sm-6'><label class='label_input'>Reason if TIN not available :</label></div><div class='col-sm-6'><input type='text' class='form-control input-field more-investor-resn_tin_unavailable' name='fullName' required='required' placeholder='' onkeyup='removetinspan();'/><span class='error more-investor-resn_tin_unavailable-error' ></span><a href='javascript:void(0);' onclick='removecountrydiv(this);'><i class='far fa-times-circle'></i></a></div></div>");
                        $(".countrynameexcludenz").intlTelInput_3();
                    });
                });</script>
            <script>

                var current_director = {};
                $(document).ready(function () {
                    var count = 1;
                    $('#counter').val(count);
                    $("#btns20").click(function () {//.find("input").val("")
                        var divClone = $(".director-adds:first").clone();
                        divClone.find('.fname').val('');
                        divClone.find('.dateOfBirth').val('');
                        divClone.find('.dateOfBirth').removeClass('hasDatepicker');
                        divClone.find('.dateOfBirth').removeAttr('id');
                        divClone.append("<div class='col-sm-2 this-space'><a href='javascript:void(0)' class='this-btn' onclick='addbtnfn(this);'></a><a href='javascript:void(0);' onclick='removedatadiv(this);'><i class='far fa-times-circle'></i></a></div>");
                        divClone.find('.dateOfBirth').datepicker({
                            yearRange: (year - 80) + ':' + year,
                            changeMonth: true,
                            changeYear: true,
                            'maxDate': new Date(adultDOB),
                            dateFormat: 'dd/mm/yy'
                        }).datepicker().attr('readonly', 'readonly');
                        divClone.appendTo(".cloneDiv");
                        //                $(".director-sections").append("<div class='row director-adds removedirector'><div class='col-lg-7'><input type='text' class='fname input-field tags' id='fname' name='fullName' value='' placeholder='Enter full name'></div><div class='col-lg-4'>  <input type='text' name='dob' id='Date_of_incorporation' placeholder='Enter DOB' class='input-field doc hasDatepicker' value='' data-lang='en' readonly='readonly' onkeyup='removespan();'><span class='error spanfname' id='spanfname'></span><input type='hidden' class='cls-me' name='me' value='N'></div><div class='col-sm-2 this-space'><a href='javascript:void(0)' class='this-btn' onclick='addbtnfn(this);'></a><a href='javascript:void(0);' onclick='removedatadiv(this);'><i class='far fa-times-circle'></i></a></div></div>");
                        counter = $('#counter').val();
                        counter++;
                        $('#counter').val(counter);
                    });
                });
                function removedatadivTrustee(ele) {
                    ele.closest('.addss').remove();
                }
                function removedatadiv(ele) {
                    ele.closest('.director-adds').remove();
                }
                function removecountrydiv(ele) {
                    ele.closest('.removecountry').remove();
                }
                function addbtnfn(ele) {
                    var close = ele.closest('.director-add');
                    var find = $(close).find('.fname');
                    var btn = $(close).find('.this-btn');
                    var cls_me = $(close).find('.cls-me');
                    var name = find.val();
                    $("a.this-btn").removeClass("check-this-btn");
                    $('.cls-me').val('N');
                    btn.addClass("check-this-btn");
                    cls_me.val('Y');
                }</script>
            <script>
                //                function checkird(obj) {
                //                str = obj.value.replace('', '');
                //                if (str.length > 10) {
                //                str = str.substr(0, 10);
                //                } else
                //                {
                //                str = str.replace(/\D+/g, "").replace(/([0-9]{3})([0-9]{3})([0-9]{2}$)/gi, "$1-$2-$3");
                //                }
                //                obj.value = str;
                //                $('.error').text("");
                //                }
            </script>
            <script>
                //            function checkAccountNO(obj) {
                //                str = obj.value.replace('', '');
                //                if (str.length > 18) {
                //                    str = str.substr(0, 18);
                //                } else
                //                {
                //                    str = str.replace(/([A-Za-z0-9]{2})([[A-Za-z0-9]{4})([[A-Za-z0-9]{7})([[A-Za-z0-9]{4}$)/gi, "$1-$2-$3-$4"); //mask numbers (xxx) xxx-xxxx    
                //
                //                }
                //                obj.value = str;
                //            }
                //                function checkAccountNO() {
                //                var ac = document.getElementById("accountNumber").value;
                //                if (ac.length === 2) {
                //                var ac20 = ac.substr(0, 2);
                //                var a = ac20 + '-';
                //                $("#accountNumber").val(a);
                //                }
                //                if (ac.length === 7) {
                //                var ac21 = ac.substr(7);
                //                var b = ac + '-' + ac21;
                //                $("#accountNumber").val(b);
                //                }
                //                if (ac.length === 15) {
                //                var ac212 = ac.substr(15);
                //                var c = ac + '-' + ac212;
                //                $("#accountNumber").val(c);
                //                }
                //                if (ac.length > 18) {
                //                var ac23 = ac.substr(0, 18);
                //                var d = ac23;
                //                $("#accountNumber").val(d);
                //                }
                //                }            </script>
            <script>
                $(document).ready(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                    $('.advisor-show').hide();
                });
                $('.avisor-click').change(function () {
                    var val = $(".avisor-click option:selected").val();
                    if (val === "1") {
                        $('.advisor-show').hide();
                    }
                    if (val === "2") {
                        $('.advisor-show').show();
                    }

                });
                function removespan() {
                    $('.spanfname').text('');
                }</script>
            <script>

                //            $('#submit').click(function () {
                //                var status = "SUBMISSION";
                //                saveData(status);
                //            });
                //            $('.saveExit').click(function () {
                //                var status = "PENDING";
                //                saveData(status);
                //            });
                saveData = function (status) {

                    var companyName = $('#companyName').val();
                    var countryCode = $('#countryCode').val();
                    var countryofincorporation = $('#Country_of_incorporation').val();
                    var date_of_incorporation = $('#Date_of_incorporation').val();
                    var registrationNumber = $('#registrationNumber').val();
                    var companyAddress = $('#companyAddress').val();
                    var postalAddress = $('#postalAddress').val();
                    var investmentAdviser = $('#investmentAdviser option:selected').text();
                    var companyAdvisor = $('#companyAdvisor option:selected').text();
                    var selectAdviser = $('#selectAnAdviser option:selected').text();
                    var fname = $('#fname').val(); // it may be multiple name
                    var holder_email = $('#email').val(); // it may be multiple name
                    var date_of_Birth = $('#Date_of_Birth').val();
                    var OccupationOption = $('.Occupation option:selected').val();
                    var occupation = $('#cOccupation').val();
                    if (OccupationOption !== "0") {
                        occupation = OccupationOption;
                    }

                    var holderCountryOfResidence = $('#holderCountryOfResidence').val();
                    var positionInCompany = $('#positionInCompany option:selected').val();
                    //                    alert(positionInCompany);
                    var address = $('#address').val();
                    var countryCode3 = $('#countryCode3').val();
                    var mobileNo = $('#mobileNo').val();
                    var otherNumber = $('#otherNumber option:selected').text();
                    //                    alert("otherNumber" + otherNumber);
                    var otherCountryCode = $('#countryCode').val();
                    var otherMobileNo = $('#mobileNo2').val();
                    var src_of_fund2 = $('#src_of_fund1 option:selected').text();
                    var licenseNumber = $('#licenseNumber').val(); //for license ID
                    var licenseExpiryDate = $('#Expirydate').val(); //for license ID
                    //                alert(licenseExpiryDate);
                    var versionNumber = $('#versionNumber').val();
                    //                    alert("versionNumber" + versionNumber);//for license ID
                    var passportNumber = $('#passportNumber').val(); //for passport ID
                    var passportExpiryDate = $('#passportExpiryDate').val(); //for passport ID
                    var countryOfIssue = $('#countryOfIssue').val(); //for passport ID
                    var investor_verify = $('#investor_verify').val();
                    var typeOfID = $('#typeOfID').val(); //for Other ID
                    var typeExpiryDate = $('#typeExpiryDate').val(); //for Other ID
                    var typeCountryOfIssue = $('#typeCountryOfIssue').val(); //for Other ID 
                    var myFile = $('#myFile').val();
                    //  Please enter the tax details
                    var countryOfResidence = $('#holderCountryOfResidence').val();
                    var irdNumber = $('#irdNumber').val();
                    var citizenUS = $('#citizenUS option:selected').text(); // if citizen yes
                    var countryOfTaxResidence = $('#countryOfTaxResidence').val(); //repeat multiple time 
                    var taxIdenityNumber = $('#taxIdenityNumber').val(); //repeat multiple time
                    //                var reasonTIN = $('#reasonTIN').val();                          //repeat multiple time
                    var pir = $('#pir option:selected').text();
                    var companyIRDNumber = $('#comIRDNumber').val();
                    var sourceOfFunds = $('#sourceOfFunds').val();
                    var detail = $('#detail').val();
                    var isCompanyListed = $('#isCompanyListed option:selected').text();
                    var isCompanyGovernment = $('#isCompanyGovernment option:selected').text();
                    var isCompanyNZPolice = $('#isCompanyNZPolice option:selected').text();
                    var isCompanyFinancialInstitution = $('#isCompanyFinancialInstitution option:selected').text();
                    var isCompanyActivePassive = $('#isCompanyActivePassive option:selected').text();
                    var isCompanyUSCitizen = $('#isCompanyUSCitizen option:selected').text(); // if citizen yes
                    var companyCountryOfTaxResidence = $('#companyCountryOfTaxResidence').val(); //repeat multiple time 
                    var companyTaxIdentityNumber = $('#companyTaxIdentityNumber').val(); //repeat multiple time
                    var companyReasonTIN = $('#companyReasonTIN').val(); //repeat multiple time
                    var bankName = $('#bankName').val();
                    var nameOfAccount = $('#nameOfAccount').val();
                    var accountNumber = $('#accountNumber').val();
                    var step = $("#step").val();
                    var reg_type = $('[name="register_type"]').val();
                    var reg_id = '${company.reg_id}';
                    var id = '${company.id}';
                    var email = '${company.email}';
                    var raw_password = '${company.raw_password}';
                    var countryArr = document.getElementsByClassName('tex_residence_Country');
                    var TINArr = document.getElementsByClassName('TIN');
                    //                var reasonArr = document.getElementsByClassName('resn_tin_unavailable');
                    var countryTINList = new Array();
                    for (var i = 0; i < countryArr.length; i++) {
                        var country = countryArr[i].value;
                        var tin = TINArr[i].value;
                        //                    var reason = reasonArr[i].value;
                        countryTINList.push({country: country, tin: tin});
                    }
                    var moreInvestorInfos = getMoreInvestorArr();
                    var DataObj = {id: id, raw_password: raw_password, email: email, companyName: companyName, countryCode: countryCode, dateOfInco: date_of_incorporation,
                        registerNumber: registrationNumber, companyAddress: companyAddress, postalAddress: postalAddress, investAdviser: investmentAdviser,
                        companyAdvisor: companyAdvisor, selectAdviser: selectAdviser, fname: fname, holder_email: holder_email, dateOfBirth: date_of_Birth,
                        occupation: occupation, holderCountryOfResidence: holderCountryOfResidence, positionInCompany: positionInCompany, address: address,
                        countryCode3: countryCode3, mobileNo: mobileNo, otherNumber: otherNumber, otherCountryCode: otherCountryCode,
                        otherMobileNo: otherMobileNo, srcOfFund: src_of_fund2, licenseNumber: licenseNumber,
                        licenseExpiryDate: licenseExpiryDate, versionNumber: versionNumber, passportNumber: passportNumber,
                        passportExpiryDate: passportExpiryDate, countryOfIssue: countryOfIssue, typeOfID: typeOfID, typeExpiryDate: typeExpiryDate,
                        typeCountryOfIssue: typeCountryOfIssue, myFile: myFile, countryOfResidence: countryOfResidence, irdNumber: irdNumber, citizenUS: citizenUS, isInvestor_idverified: investor_verify,
                        countryOfTaxResidence: countryOfTaxResidence, taxIdentityNumber: taxIdenityNumber, pir: pir,
                        companyIRDNumber: companyIRDNumber, sourceOfFunds: sourceOfFunds, detail: detail,
                        isCompanyListed: isCompanyListed, isCompanyGovernment: isCompanyGovernment, isCompanyNZPolice: isCompanyNZPolice,
                        isCompanyFinancialInstitution: isCompanyFinancialInstitution, isCompanyActivePassive: isCompanyActivePassive,
                        isCompanyUSCitizen: isCompanyUSCitizen, companyCountryOfTaxResidence: companyCountryOfTaxResidence, companyTaxIdentityNumber: companyTaxIdentityNumber,
                        companyReasonTIN: companyReasonTIN, countryofincorporation: countryofincorporation, bankName: bankName, nameOfAccount: nameOfAccount, accountNumber: accountNumber, step: step, status: status, reg_type: reg_type, reg_id: reg_id, countryTINList: countryTINList, moreInvestorList: moreInvestorInfos};
                    //                alert(JSON.stringify(DataObj));
                    swal({title: "",
                        text: "Application data being saved...  ",
                        type: "success",
                        timer: 3500,
                        showConfirmButton: false
                    });
                    console.log(JSON.stringify(DataObj));
                    $.ajax({
                        type: 'POST',
                        url: './rest/groot/db/api/company-registeration',
                        headers: {"Content-Type": 'application/json'},
                        data: JSON.stringify(DataObj),
                        success: function (data, textStatus, jqXHR) {
                            console.log(data);
                            //                        swal({
                            //                            title: "Complete",
                            //                            text: "Your application data has been saved. To resume your application, please see the email we have now sent you. ",
                            //                            type: "success",
                            //                            timer: 2500,
                            //                            showConfirmButton: false
                            //                        });
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            //                        alert(" inside error" + jqXHR);
                        }
                    });
                };</script>
        <script>
            function tinfornext7(ele) {
                var directorDivs1 = document.getElementsByClassName('director-add');
                if (directorDivs1.length > 1) {
                    $("#step").attr("value", 8);
                    next(ele);
                } else {
                    $("#step").attr("value", 12);
                    $("#step12").show();
                    $("#step7").hide();
                }
            }

            function removetinspan() {
                $('.error').text('');
            }</script>
        <script>
            $('.checkname').change(function () {
                console.log("daya");
                var root = $(this).closest('.closestcls');
                var name = $(this).val();
                var filename = name.split('\\').pop().split('/').pop();
                filename = filename.substring(0, filename.lastIndexOf('.'));
                console.log(name);
                root.find('.shownamedata').text("File: " + filename);
                root.find(".removefile").show();
            });
            function  removedatafile(ele) {
                var root = ele.closest('.closestcls');
                $(root).find('.shownamedata').text("");
                $(root).find('.checkname').val("");
                $(root).find(".removefile").hide();
            }
            $("#step3 .Occupation").change(function () {
                var OccupationOption = $('#step3 .Occupation option:selected').val();
                if (OccupationOption === "0") {
                    $("#step3 .selectOcc").show();
                    $("#step3 .otherOcc").show();
                } else {
                    $("#step3 .selectOcc").hide();
                    $("#step3 .otherOcc").show();
                }
            });</script>

        <script>
            $(".verify-dl").click(function () {
                var x = $(this).closest(".content-section");
                var index = x.find(".Id_Type option:selected").val();
                var s = $(x).find('.which_container').val();
                var url = '';
                var firstName = '';
                var middleName = '';
                var lastName = '';
                var Date_of_Birth = '';
                if (index === "1") {
                    url = './rest/groot/db/api/dl-verification';
                    firstName = x.find('input[name=licence_first_name]').val();
                    middleName = x.find('input[name=licence_middle_name]').val();
                    lastName = x.find('input[name="licence_last_name"]').val();
                    var License_number = x.find('input[name="license_number"]').val();
                    //                    var License_number = $('.License_number').val();
                    var licence_expiry_Date = x.find($('.lic_expiry_Date')).val();
                    var licence_verson_number = x.find($('.lic_verson_number')).val();
                    if (firstName === "") {

                        if (s === "#Date_of_Birth") {

                            $("#error_licence_first_name").text("This field is required ");
                            return false;
                        } else if (s === "#more-investor-dob") {
                            $("#more_error_licence_first_name").text("This field is required ");
                            return false;
                        }
                    } else if (License_number === "") {
                        if (s === "#Date_of_Birth") {
                            $("#spanlicenseNumber").text("This field is required ");
                            return false;
                        } else if (s === "#more-investor-dob") {
                            $("#more-investor-licenseNumber-error").text("This field is required ");
                            return false;
                        }
                    } else if (licence_verson_number === "") {
                        if (s === "#Date_of_Birth") {
                            $("#spanVersionnumber").text("This field is required ");
                            return false;
                        } else if (s === "#more-investor-dob") {
                            $("#more-investor-versionNumber-error").text("This field is required ");
                            return false;
                        }

                    } else if (lastName === "") {
                        if (s === "#Date_of_Birth") {
                            $("#error_licence_last_name").text("This field is required ");
                            return false;
                        } else if (s === "#more-investor-dob") {
                            $("#more_error_licence_last_name").text("This field is required ");
                            return false;
                        }
                    }

                } else if (index === "2") {
                    url = './rest/groot/db/api/pp-verification';
                    firstName = x.find('input[name=passport_first_name]').val();
                    middleName = x.find('input[name=passport_middle_name]').val();
                    lastName = x.find('input[name="passport_last_name"]').val();
                    var passport_number = x.find('input[name="passport_number"]').val();
                    //                    var passport_expiry =  "2017-10-10";
                    var passport_expiry = x.find('.pass_expiry').val();
                    var dob2 = x.find('.pass_expiry').val();
                    dob2 = dob2.trim();
                    passport_number = passport_number.trim();
                    if (firstName === "") {
                        if (s === "#Date_of_Birth") {

                            $("#error_passport_first_name").text("This field is required ");
                            return false;
                        } else if (s === "#more-investor-dob") {
                            $("#more_error_passport_first_name").text("This field is required ");
                            return false;
                        }
                    } else if (passport_number === "") {
                        if (s === "#Date_of_Birth") {

                            $("#spanPassportnumber").text("This field is required ");
                            return false;
                        } else if (s === "#more-investor-dob") {
                            $("#more-investor-passportNumber-error").text("This field is required ");
                            return false;
                        }

                    } else if (passport_expiry === "") {
                        if (s === "#Date_of_Birth") {

                            $("#spanPExpirydate").text("This field is required ");
                            return false;
                        } else if (s === "#more-investor-dob") {
                            $("#more-investor-passportExpiryDate-error").text("This field is required ");
                            return false;
                        }

                    } else if (lastName === "") {
                        if (s === "#Date_of_Birth") {

                            $("#error_passport_last_name").text("This field is required ");
                            return false;
                        } else if (s === "#more-investor-dob") {
                            $("#more_error_passport_last_name").text("This field is required ");
                            return false;
                        }
                    }
                }
                Date_of_Birth = $(s).val();
                //                alert(Date_of_Birth);
                var DataObj = {license_number: License_number, licence_expiry_Date: licence_expiry_Date,
                    licence_verson_number: licence_verson_number, date_of_Birth: Date_of_Birth, firstName: firstName,
                    middleName: middleName, lastName: lastName, passport_number: passport_number, passport_expiry: passport_expiry};
                console.log(JSON.stringify(DataObj));
                //                alert(JSON.stringify(DataObj));
                $.ajax({
                    type: 'POST',
                    url: url,
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(DataObj),
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        var obj = JSON.parse(data);
                        if (index === "1") {
                            if (obj.driversLicence.verified) {
                                swal({
                                    title: "Success",
                                    text: "Data is Verified.",
                                    type: "info",
                                    timer: 3500,
                                    showConfirmButton: true
                                });
                            } else {
                                //                                alert("wrong data");
                            }
                        } else if (index === "2") {
                            if (obj.passport.verified) {
                                swal({
                                    title: "Success",
                                    text: "Data is Verified.",
                                    type: "info",
                                    timer: 3500,
                                    showConfirmButton: true
                                });
                            } else {
                                //                                alert("wrong data");
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //                        alert(" inside error" + jqXHR);
                    }
                });
            });
            $('.country').click(function () {
                $('.error').text("");
            });
            function changecoutry(ele) {
                ele.getElementsByClassName('error')[0].innerHTML = "";
            }
            function changeFund(ele) {
                var root = $(ele).closest('.morestep3');
                var val = ele.value;
                var idc = "#" + (root.attr("id"));
                if (val === "1") {
                    $(idc + ' .other-select1').hide();
                    $(idc + ' .drivery-licence1').show();
                    $(idc + ' .passport-select1').hide();
                } else if (val === "2") {
                    $(idc + ' .passport-select1').show();
                    $(idc + ' .other-select1').hide();
                    $(idc + ' .drivery-licence1').hide();
                } else if (val === "3") {
                    $(idc + ' .passport-select1').hide();
                    $(idc + ' .other-select1').show();
                    $(idc + ' .drivery-licence1').hide();
                }
                $('.error').text("");
            }
            function changeCountry(ele) {
                var root = $(ele).closest('.morestep4');
                var val = ele.value;
                var idc = "#" + (root.attr("id"));
                if (val === "1") {
                    $(idc + ' .yes-option1').hide();
                }
                if (val === "2") {
                    $(idc + ' .yes-option1').show();
                }
                $('.error').text("");
            }
            function addAnotherCountry(ele) {
                var root = $(ele).closest('.morestep4');
                var idc = "#" + (root.attr("id"));
                $(idc + " .yes-new3").append("<div class='row checktindata'><div class='col-sm-6'><label class='label_input' style='text-align:left'>Country of tax residence</label></div><div class='col-sm-6 details-pos flag-drop'><input type='text' class='form-control excludenz tex_residence_Country'  id='countryname' name='countryCode' placeholder='Enter Country Code' readonly='readonly'><span class='error error-tex-residence-Country' ></span></div><div class='col-sm-6'><label class='label_input'>Tax Identification Number </label></div><div class='col-sm-6'><input type='text' class='form-control input-field TIN' name='tin' required='required' placeholder='Enter TIN' onkeyup='removerror()' /><span class='error error-TIN'></span></div><div class='col-sm-6'><label class='label_input'>Reason if TIN not available </label></div><div class='col-sm-6'> <input type='text' class='form-control input-field resn_tin_unavailable'  name='tin' required='required' onkeyup='removerror()'/><span class='error error-resn-tin-unavailable' id='error-resn-tin-unavailable'></span></div></div>");
                $(idc + " .excludenz").intlTelInput_3();
                $('.error').text("");
            }
            function chnageoccupation(ele) {
                var root = $(ele).closest('.morestep2');
                var idc = "#" + (root.attr("id"));
                var val = $(idc + ' .selectOcc option:selected').val();
                if (val === "0") {
                    $(idc + ' .otherOcc').show();
                } else {
                    $(idc + ' .otherOcc').hide();
                }
                $('.error').text("");
            }
            $("#step1 .selectType").change(function () {
                var trustType = $("#step1 .selectType option:selected").val();
                if (trustType === "3") {
                    $("#step1 .selectType").show();
                    $("#step1 .otherType").show();
                } else {
                    $("#step1 .selectType").show();
                    $("#step1 .otherType").hide();
                }
                $('.error').text("");
            });</script>
        <script>
            $('.senderType').click(function () {
                //                var mNo = $("#mobileNo3").val();
                //                var cCo = $("#countryCode3").val();
                var mNo = $('.mobile_number1').val();
                var cCo = $('.mobile_country_code').val();
                var sTy = $('.senderType:checked').val();
                cCo = cCo.replace(/\+/g, "");
                var url = 'https://jj1stbnzb3.execute-api.ap-southeast-2.amazonaws.com/otp/generateotp?pn=' + mNo + '&cc=' + cCo + '&sTy=' + sTy;
                //document.getElementById('error-loader').style.display = 'block';
                document.getElementById('error-generateOtp').innerHTML = "";
                $.ajax({
                    url: url,
                    type: 'GET',
                    async: true,
                    dataType: "json",
                    success: function (data) {
                        $('#error-generateOtp').html("Please check your messages");
                        $('#otp-block').show();
                        //            document.getElementById('error-loader').style.display = 'none';
                        $('input:radio[name="senderType"]').prop("checked", false);
                    },
                    error: function (e) {
                        var mobileNo = document.getElementsByClassName('mobile_number1');
                        var result = true;
                        $('#otp-block').show();
                        if (mobileNo.value === '') {
                            //document.getElementById('error-loader').style.display = 'none';
                            //                 $('#error-generateOtp').text("Mobile number is required.");
                            $('input:radio[name="senderType"]').prop("checked", false);
                            result = result && false;
                        } else {
                            //document.getElementById('error-loader').style.display = 'none';

                            //                 $('input:radio[name="senderType"]').prop("checked", false);
                            result = result && false;
                        }
                        return result;
                    }
                });
                return sTy;
            });</script> 
    </body>
</html>
