 
<%-- 
    Document   : login
    Created on : Jul 24, 2019, 4:16:48 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<c:set var="home" value="${pageContext.request.contextPath}"/>

<html lang="en">
    <head>
        <title>Login V2</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="./resources/images/invsta-color-logo.png" type="image/gif" sizes="16x16">
        <!--===============================================================================================-->	
        <!--<link rel="icon" type="image/png" href="https://poc-as-product.s3-website-ap-southeast-2.amazonaws.com/resources/images/icons/favicon.ico"/>-->
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/fonts/iconic/css/material-design-iconic-font.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/animate.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/animsition.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/select2.min.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/daterangepicker.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/util.css">
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/main.css">
        <!--===============================================================================================-->
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link href="./resources/css/main1.css" rel="stylesheet"/>
        <style>
            .login100-form-bgbtn {
    background: linear-gradient(to right, #9b6f52, #9b6f52);
}
            .focus-input100::before {
                background: #2f5fc5;
            }
            .btn-show-pass {
                color: #9b6f52;
            }
            .btn-show-pass:hover {
                color: #b6000b;
            }
            .text-center.p-t-115 {
                padding: 40px 0px 0px;
            }

        </style>
    </head>
  <body style="background-image: url('./resources/images/bckground.png')!important;background-size: cover !important;">

        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <form class="login100-form validate-form" action="<c:url value='/j_spring_security_check'/>" id="loginForm" method="POST">
                        <!--                        <span class="login100-form-title p-b-26">
                                                    Welcome
                                                </span>-->
                        <span class="login100-form-title p-b-48">
                            <!--<i class="zmdi zmdi-font"></i>-->
                            <img src="resources/images/logo.png">
                        </span>
                        <div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
                            <lable class="enter-details">Email</lable>
                            <input  class="input100"  type="email" placeholder="Enter Email" name="username" required />
                            <span class="focus-input100" data-placeholder=""></span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate="Enter password">
                            <lable class="enter-details">Password</lable>
                            <span class="btn-show-pass">
                                <i class="zmdi zmdi-eye"></i>
                            </span>
                            <input  class="input100" type="password" id="password" placeholder="Enter password" name="password" required />
                            <!--                            <input class="input100" type="password" name="pass">-->
                            <span class="focus-input100" data-placeholder=""></span>
                        </div>

                        <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button class="login100-form-btn">
                                    Login
                                </button>
                            </div>
                        </div>

                        <div class="text-center p-t-115">
                            <span class="txt1">
                                Don't have an account?
                            </span>

                            <a class="txt2" id="txt2" href="./signUpPath">
                                Sign Up
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div id="dropDownSelect1"></div>

        <!--===============================================================================================-->
        <script src="./resources/js/login-js/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="./resources/js/login-js/animsition.min.js"></script>
        <!--===============================================================================================-->
        <script src="./resources/js/login-js/popper.js"></script>
        <script src="./resources/js/login-js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="./resources/js/login-js/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="./resources/js/login-js/moment.min.js"></script>
        <script src="./resources/js/login-js/daterangepicker.js"></script>
        <!--===============================================================================================-->
        <script src="./resources/js/login-js/countdowntime.js"></script>
        <!--===============================================================================================-->
        <script src="./resources/js/login-js/main.js"></script>

        <!--<script>-->
        <!--            $('#txt2').click(function () {
                        $('#txt2').attr("href", "./signUpPath");
                    });-->
        <!--</script>-->
    </body>
</html>

