<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>westpac</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="http://backoffice.invsta.io/mint/images/mint.png" rel="shortcut icon"/>
        <link href="http://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <!--<link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>-->
        <link href="http://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>
        <!--<link href="http://backoffice.invsta.io/pocv/resources/css/main.css?version=3.5.1" rel="stylesheet"/>-->
        <link href="http://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link href="./resources/css/custom.css" rel="stylesheet"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
        <!--  <link href="resources/css/osfonts.css" rel="stylesheet"> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

        <link rel="stylesheet" href="./resources/css/acc-style.css">


        <style>

            img.logo {
                width: 40%;
            }

            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('./resources/images/Preloader_3.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
        </style>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Payment</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">

                                <div class="col-md-12 invst-option">
                                    <div class="card mt-3 tab-card">
                                        <!-- <div class="card-header tab-card-header">
                                                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                                        <li class="nav-item">
                                                                <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Funds</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Direct Companies</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">New Offers</a>
                                                        </li>
                                                </ul>
                                        </div> -->

                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active p-31" id="one" role="tabpanel" aria-labelledby="one-tab">
                                                <div class="row1">
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="element-header">Payment</h6>
                                                        </div>

                                                        <div class="form-data">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="content-body">
                                                                            <div class="line-progress">
                                                                                <div class="line-bar6">
                                                                                    <div class="line-dot line-adjust">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="line-data">
                                                                                    <span>Amount</span>
                                                                                    <span>You</span>
                                                                                    <span>Recipient</span>
                                                                                    <span>Review</span>
                                                                                    <span>Pay</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="header-border">
                                                                            </div>
                                                                            <div class="input-content-text">
                                                                                <div class="main-header">
                                                                                    <h2>Review details of your transfer</h2>
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div class="trans-detail-2">
                                                                                                <div class="trans-bank-2">
                                                                                                    <h6 class="body-data">Transfer details</h6>
                                                                                                    <a href="">Edit</a>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">You send</span> 
                                                                                                    <span class="second-span">100 NZD</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Fee (included):</span> 
                                                                                                    <span class="second-span">-4.71 NZD</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Amount we'll convert</span> 
                                                                                                    <span class="second-span">95.29 NZD</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Guaranteed rate (48 hours)</span> 
                                                                                                    <span class="second-span">0.66075</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">You get</span> 
                                                                                                    <span class="second-span">62.96 NZD</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Should arrive</span> 
                                                                                                    <span class="second-span">by Aug 1</span>
                                                                                                </div>
                                                                                                <div class="heading-middle"></div>
                                                                                                <div class="trans-bank-2 trans-space">
                                                                                                    <h6 class="body-data">Recipient details</h6>
                                                                                                    <a href="">Changes</a>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Name</span> 
                                                                                                    <span class="second-span">Kayla White</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Email</span> 
                                                                                                    <span class="second-span">white.kaylaj@ gmail.com</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Routing number</span> 
                                                                                                    <span class="second-span">031176110</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Account number</span> 
                                                                                                    <span class="second-span">157040704</span>
                                                                                                </div>
                                                                                                <div class="trans-data-2">
                                                                                                    <span class="first-span">Account type</span> 
                                                                                                    <span class="second-span">Checking</span>
                                                                                                </div>
                                                                                                <div class="count-btn">
                                                                                                    <a href="./maketransfer">Continue</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <a href="#" class="btn btn-primary" id="viewmore1">View More</a>       -->        
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <!-- <script src="resources/bower_components/moment/moment.js" ></script>
        
        <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
        <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
        <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="http://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>
        <script type="text/javascript">
            $(window).load(function () {

                $(".loader").fadeOut("slow");
            });
        </script>

        <!--        <script>
                    //        $('#viewmore1').click(function () {
                    //            $('.hidediv1').show();
                    //            $('#viewmore1').hide();
                    //        });
                    //        $('#viewmore2').click(function () {
                    //            $('.hidediv2').show();
                    //            $('#viewmore2').hide();
                    //        });
                    //        $('#viewmore3').click(function () {
                    //            $('.hidediv3').show();
                    //            $('#viewmore3').hide();
                    //        });
                    $('.clickinput').on("click", function () {
                        var recemt = $(this).closest('.funds-deatil');
                        recemt.find(".offer-input").toggle();
                    });
                    $(document).ready(function () {
                        $(".loader").fadeOut("slow");
                        $('.hidediv1').hide();
                        $('.hidediv2').hide();
                        $('.hidediv3').hide();
                    });
                    $('.democlick').click(function () {
                        var data = $(this).data("target");
                        $('.showclick').hide();
                        $(data).show();
                    });
                </script>-->
        <script>
            $(".calculator-dropdown ul").on("click", ".init", function () {
                $(this).closest(".calculator-dropdown ul").children('li:not(.init)').toggle();
            });

            var allOptions = $(".calculator-dropdown ul").children('li:not(.init)');
            $(".calculator-dropdown ul").on("click", "li:not(.init)", function () {
                allOptions.removeClass('selected');
                $(this).addClass('selected');
                $(".calculator-dropdown ul").children('.init').html($(this).html());
                allOptions.toggle();
            });
        </script>
        <script>
            $(".calculator-dropdown2 ul").on("click", ".init2", function () {
                $(this).closest(".calculator-dropdown2 ul").children('li:not(.init2)').toggle();
            });

            var allOption = $(".calculator-dropdown2 ul").children('li:not(.init2)');
            $(".calculator-dropdown2 ul").on("click", "li:not(.init2)", function () {
                allOption.removeClass('selected');
                $(this).addClass('selected');
                $(".calculator-dropdown2 ul").children('.init2').html($(this).html());
                allOption.toggle();
            });
        </script>
        <script>
            $(document).ready(function () {
                $(".data-toggle").hide();
                $(".loader").fadeOut("slow");
                $(".see-toggle").click(function () {
                    $(".data-toggle").toggle();
                });
                $("#continue").click(function () {
                    window.location.href = './regularmethod';
                });
            });
        </script>
    </body>
</html>