<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>westpac</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="http://backoffice.invsta.io/mint/images/mint.png" rel="shortcut icon"/>
        <link href="http://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <!--<link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>-->
        <link href="http://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>
        <!--<link href="http://backoffice.invsta.io/pocv/resources/css/main.css?version=3.5.1" rel="stylesheet"/>-->
        <link href="http://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>
        <link href="./resources/css/custom.css" rel="stylesheet"/>
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
        <!--  <link href="resources/css/osfonts.css" rel="stylesheet"> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

        <link rel="stylesheet" href="./resources/css/acc-style.css">
        <link href="resources/css/bootstrap-pincode-input.css" rel="stylesheet">


        <style>

            img.logo {
                width: 40%;
            }

            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('./resources/images/Preloader_3.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
       .content_body {
    background: #f5f9f8;
}
            .content-section {
                padding: 40px;
            }
            .btns {
                display: flex;
                justify-content: space-between;
            }
            #msform fieldset:not(:first-of-type) {
                display: none;
            }
        </style>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Payment</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                                                                                        
                </div>
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="content_body">
                                <form id="msform">
                                    <fieldset id="step1">
                                            <div class="row">
                                                <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-12  box-payment-space">
                                                                   
                                                                        <div class="line-progress">
                                                                            <div class="line-bar2">
                                                                                <div class="line-dot">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-1">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-2">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-3">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-4">
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="line-data">
                                                                                <span>Amount</span>
                                                                                <span>You</span>
                                                                                <span>Recipient</span>
                                                                                <span>Review</span>
                                                                                <span>Pay</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="input-content-text">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="body-section">
                                                                                        <h6 class="body-data">Existing Recipients</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-4 continue">
                                                                                    <a href="javascript:void(0)" class="next-off">
                                                                                         <div class="on-btn">
                                                                                             <img src="./resources/images/icon-2.png">
                                                                                            <h6 class="icon-name">One Off</h6>
                                                                                            </div>
                                                                                        <div class="body-box">
                                                                                           <span class="flag-data">USD Account</span>
                                                                                            <p>ending with <span class="add-nmbr">0704 </span></p>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="col-md-4 continue">
                                                                                    <a href="javascript:void(0)" class="next1">
                                                                                         <div class="on-btn">
                                                                                             <img src="./resources/images/icon-1.png">
                                                                                            <h6 class="icon-name">Regular</h6>
                                                                                            </div>
                                                                                        <div class="body-box">
                                                                                            <span class="flag-data">USD Account</span>
                                                                                            <p>ending with <span class="add-nmbr">9524</span></p>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!--                                                    <div class="col-md-12">
                                                                                                        <div class="btns">
                                                                                                            <input type="button" name="previous" value="Previous" class="previous1 ">
                                                                                                            <input type="button" name="previous" value="Next" class="next1">
                                                                                                        </div>
                                                                                                    </div>-->
                                            </div>
                                    </fieldset>
                                    <fieldset id="step21">
                                            <div class="row">
                                                <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-12  box-payment-space">
                                                                        <div class="line-progress">
                                                                             <div class="line-bar2">
                                                                                <div class="line-dot ">
                                                                                </div>
                                                                                  <div class="line-dot progress-1 ">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-1">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-2">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-3">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-4">
                                                                                </div>
                                                                            </div>
                                                                            <div class="line-data">
                                                                                <span>Amount</span>
                                                                                <span>You</span>
                                                                                <span>Recipient</span>
                                                                                <span>Review</span>
                                                                                <span>Pay</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="input-content-text backgroud-add">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="body-section">
                                                                                        <img src="./resources/images/icon-2.png">
                                                                                        <h6 class="body-data">One Off</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="payment-regular  add-ragular one-off">
                                                                                        <p>Enter the Amount You want to invest</p>
                                                                                        <input type="text" placeholder="Enter Amount">
                                                                                    </div>
                                                                                </div>
                                                                               
                                                                            </div>
                                                                        </div>  
                                                                    <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="payment-btn btn-select">
                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back previous-off" id="">Back</a>
                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back next21" id="">Next</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!--                                                <div class="col-md-12">
                                                                                                    <div class="btns">
                                                                                                        <input type="button" name="previous" value="Previous" class="previous2 ">
                                                                                                        <input type="button" name="previous" value="Next" class="next2">
                                                                                                    </div>
                                                                                                </div>-->
                                            </div>
                                    </fieldset>
                                    <fieldset id="step2">
                                            <div class="row">
                                                <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-12  box-payment-space">
                                                                        <div class="line-progress">
                                                                            <div class="line-bar2">
                                                                                <div class="line-dot">
                                                                                </div>
                                                                                  <div class="line-dot progress-1 ">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-1">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-2">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-3">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-4">
                                                                                </div>
                                                                            </div>
                                                                            <div class="line-data">
                                                                                <span>Amount</span>
                                                                                <span>You</span>
                                                                                <span>Recipient</span>
                                                                                <span>Review</span>
                                                                                <span>Pay</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="input-content-text backgroud-add">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="body-section">
                                                                                        <img src="./resources/images/icon-1.png">
                                                                                        <h6 class="body-data">Regular</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <div class="payment-regular add-ragular">
                                                                                        <p>Lump Sum</p> 
                                                                                        <input type="text" placeholder="Enter Amount">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="payment-regular add-ragular">
                                                                                        <p>Regular Amount</p> 
                                                                                         <input type="text" placeholder="Enter Amount">
                                                                                    </div>
                                                                                </div>
                                                                                     <div class="col-md-4">
                                                                                    <div class="payment-regular add-ragular border-dlt">
                                                                                        <p>Frequency</p> 
                                                                                          <select>
                                                                                            <option>1 Month</option>
                                                                                            <option>3 Months</option>
                                                                                            <option>6 Months</option>
                                                                                            <option>1 Year</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                           
                                                                        </div>
                                                                     <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="payment-btn btn-select">
                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back previous2" id="">Back</a>
                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back next2" id="">Next</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!--                                                <div class="col-md-12">
                                                                                                    <div class="btns">
                                                                                                        <input type="button" name="previous" value="Previous" class="previous2">
                                                                                                        <input type="button" name="previous" value="Next" class="next2">
                                                                                                    </div>
                                                                                                </div>-->
                                            </div>
                                    </fieldset>
                                    <fieldset id="step3">
                                            <div class="row">
                                                <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-12 box-payment-space">
                                                                        <div class="line-progress">
                                                                             <div class="line-bar2">
                                                                                <div class="line-dot">
                                                                                </div>
                                                                                 <div class="line-dot progress-2">
                                                                                </div>
                                                                                   <div class="line-dot progress-1 ">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-2">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-3">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-4">
                                                                                </div>
                                                                            </div>
                                                                            <div class="line-data">
                                                                                <span>Amount</span>
                                                                                <span>You</span>
                                                                                <span>Recipient</span>
                                                                                <span>Review</span>
                                                                                <span>Pay</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="input-content-text backgroud-add">

                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="body-section">
                                                                                    <h5 class="text-center " style="text-align:left!important">How would you like to pay?</h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="body-bank radoi-set">
                                                                                        <label class="bank-radio nextcredit">
                                                                                            <i class="fa fa-credit-card" aria-hidden="true"></i>
                                                                                            <span class="bank-card">Direct Credit</span>
                                                                                            <p class="bank-card-text">5.20 NZD in total fees.</p>
                                                                                            <input type="radio" name="radio">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="bank-radio next3">
                                                                                            <i class="fa fa-credit-card" aria-hidden="true"></i>
                                                                                            <span class="bank-card">Direct Debit</span>
                                                                                            <p class="bank-card-text">5.20 NZD in total fees.</p>
                                                                                            <input type="radio" name="radio">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                          
                                                                        </div>
                                                                      <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="payment-btn btn-select">
                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl  payment-back previous3" id="">Back</a>
                                                                                        <!--                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back next3" id="">Next</a>-->
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                <div class="col-md-12">
                                                                                                    <div class="btns">
                                                                                                        <input type="button" name="previous" value="Previous" class="previous3 ">
                                                                                                        <input type="button" name="previous" value="Next" class="next3">
                                                                                                    </div>
                                                                                                </div>-->
                                            </div>
                                    </fieldset>
                                    <fieldset id="step4">
                                          <div class="row">
                                                <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-12 box-payment-space">
                                                                        <div class="line-progress">
                                                                             <div class="line-bar2">
                                                                                <div class="line-dot">
                                                                                </div>
                                                                                 <div class="line-dot progress-3">
                                                                                </div>
                                                                                    <div class="line-dot progress-1 ">
                                                                                </div>
                                                                                <div class="line-dot progress-2">
                                                                                </div>>
                                                                                  <div class="line-dot add-dot dot-show-3">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-4">
                                                                                </div>
                                                                            </div>
                                                                            <div class="line-data">
                                                                                <span>Amount</span>
                                                                                <span>You</span>
                                                                                <span>Recipient</span>
                                                                                <span>Review</span>
                                                                                <span>Pay</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="input-content-text">

                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="body-section">
                                                                                        <h6 class="body-data">Existing recipients</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                  
                                                                                <div class="col-md-4 continue">
                                                                                    <a href="javascript:void(0)" class="next4">
                                                                                         <div class="on-btn">
                                                                                           
                                                                                            <h6 class="icon-name">Existing</h6>
                                                                                            </div>
                                                                                        <div class="body-box">
                                                                                           <span class="flag-data">USD Account</span>
                                                                                            <p>ending with <span class="add-nmbr">0704 </span></p>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="col-md-4 continue">
                                                                                    <a href="javascript:void(0)" class="next-debit">
                                                                                        <div class="body-box">
                                                                                           <div class="body-icon">
                                                                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                                                            <h6 class="icon-name">Enter New Account</h6>
                                                                                            </div>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                       <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="payment-btn btn-select">
                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back previous4" id="">Back</a>
                                                                                        <!--                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back next2" id="">Next</a>-->
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                <div class="col-md-12">
                                                                                                    <div class="btns">
                                                                                                        <input type="button" name="previous" value="Previous" class="previous4 ">
                                                                                                        <input type="button" name="previous" value="Next" class="next4">
                                                                                                    </div>
                                                                                                </div>-->
                                            </div>
                                    </fieldset>
                                    <fieldset id="step5">
                                            <div class="row">
                                                <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                        <div class="form-data">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12 box-payment-space">
                                                                            <div class="line-progress">
                                                                               <div class="line-bar2">
                                                                                <div class="line-dot">
                                                                                </div>
                                                                                   <div class="line-dot progress-3">
                                                                                </div>
                                                                                   <div class="line-dot progress-1 ">
                                                                                </div>
                                                                               <div class="line-dot progress-2">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-3">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-4">
                                                                                </div>
                                                                            </div>
                                                                                <div class="line-data">
                                                                                    <span>Amount</span>
                                                                                    <span>You</span>
                                                                                    <span>Recipient</span>
                                                                                    <span>Review</span>
                                                                                    <span>Pay</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="input-content-text backgroud-add">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="trans-detail-2">
                                                                                                    <div class="trans-bank-2">
                                                                                                        <h6 class="body-data">Bank Account Details for Payment</h6>
                                                                                                        <!--                                                                                                    <a href="">Edit</a>-->
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Account Number</span> 
                                                                                                        <span class="second-span">12345678912</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Bank Name</span> 
                                                                                                        <span class="second-span">ANZ Bank New Zealand</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Branch</span> 
                                                                                                        <span class="second-span">New Zealand</span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="trans-detail-2">
                                                                                                    <div class="trans-bank-2">
                                                                                                        <h6 class="body-data">Reference Details for Payment</h6>
                                                                                                        <!--                                                                                                    <a href="">Edit</a>-->
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Reference Number</span> 
                                                                                                        <span class="second-span">998756789845</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Date</span> 
                                                                                                        <span class="second-span">Tuesday, 22 October 2019</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Time</span> 
                                                                                                        <span class="second-span">20:19:37</span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                   
                                                                                    <div class="instruction-btn">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="tree-btn">
                                                                                                    <a href="javascript:void(0)" class="btn btn-primary btn-styl">I've made payment</a>
                                                                                                    <a href="javascript:void(0)" class="btn btn-primary btn-styl transparent-style">I'll make payment later</a>

                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                
                                                                             
                                                                            </div>
                                                                           <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="payment-btn btn-select">
                                                                                            <a href="javascript:void(0)" class="btn btn-primary btn-styl  payment-back previous5" id="">Back</a>
                                                                                            <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back next5" id="">Next</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </fieldset>
                                    <fieldset id="step6">
                                        <div class="content-section">
                                            <div class="row">
                                                <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                        <div class="form-data">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12 ">
                                                                            <div class="line-progress">
                                                                                  <div class="line-bar2">
                                                                                <div class="line-dot">
                                                                                </div>
                                                                                        <div class="line-dot progress-3">
                                                                                </div>
                                                                                    <div class="line-dot progress-1 ">
                                                                                </div>
                                                                               <div class="line-dot progress-2">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-3">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-4">
                                                                                </div>
                                                                            </div>
                                                                                <div class="line-data">
                                                                                    <span>Amount</span>
                                                                                    <span>You</span>
                                                                                    <span>Recipient</span>
                                                                                    <span>Review</span>
                                                                                    <span>Pay</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="input-content-text backgroud-add">
                                                                                  
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="trans-detail-2">
                                                                                                    <div class="trans-bank-2">
                                                                                                        <h6 class="body-data">Use Existing Account</h6>
                                                                                                        <!--                                                                                                    <a href="">Edit</a>-->
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Account Number</span> 
                                                                                                        <span class="second-span">12345678912</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Bank Name</span> 
                                                                                                        <span class="second-span">ANZ Bank New Zealand</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Branch</span> 
                                                                                                        <span class="second-span">New Zealand</span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                            </div>
                                                                                 <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="payment-btn btn-select">
                                                                                            <a href="javascript:void(0)" class="btn btn-primary btn-styl  payment-back previous6" id="">Back</a>
                                                                                            <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back next6" id="">Next</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                      
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </fieldset>
                                    <fieldset id="step7">
                                        <div class="content-section">
                                            <div class="row">
                                                <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                        <div class="form-data">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12 ">
                                                                            <div class="line-progress">
                                                                                <div class="line-bar2">
                                                                                <div class="line-dot">
                                                                                </div>
                                                                                      <div class="line-dot progress-3">
                                                                                </div>
                                                                                      <div class="line-dot progress-1 ">
                                                                                </div>
                                                                                <div class="line-dot progress-2">
                                                                                </div>>
                                                                              
                                                                                  <div class="line-dot add-dot dot-show-3">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-4">
                                                                                </div>
                                                                            </div>
                                                                                <div class="line-data">
                                                                                    <span>Amount</span>
                                                                                    <span>You</span>
                                                                                    <span>Recipient</span>
                                                                                    <span>Review</span>
                                                                                    <span>Pay</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="input-content-text backgroud-add">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="col-md-12"> 
                                                                                                    <div class="debit-content_body only-debit-form">

                                                                                                        <div class="debit-content-section">
                                                                                                            <div class="row">
                                                                                                                <div class="col-md-7">
                                                                                                                    <div class="text-data">
                                                                                                                        <h6>Direct Debit Instruction:</h6>
                                                                                                                        <p>Details of the bank account from which the payment is to be mode:</p>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-md-5">
                                                                                                                    <div class="debit-bank-name digit-1">
                                                                                                                        <h6>Authorisation Code</h6>
                                                                                                                        <input type="text" id="pincode-input1" class="form-control" placeholder="0" maxlength="" value="0618754">
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="col-md-6">
                                                                                                                    <div class="debit-bank-name name-bank">
                                                                                                                        <input type="text" class="form-control" placeholder="Name of Bank Account" maxlength="20">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-md-6">
                                                                                                                    <div class="debit-bank-name name-bank">
                                                                                                                        <input type="text" class="form-control" placeholder="Bank Account Number" maxlength="11">
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="col-md-6">
                                                                                                                    <div class="debit-bank-name digit">
                                                                                                                        <h6>Payer Particulars</h6>
                                                                                                                        <input type="text" id="pincode-input2" class="form-control" placeholder="" maxlength="" value="MINT">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-md-6">
                                                                                                                    <div class="debit-bank-name digit">
                                                                                                                        <h6>Payer Code</h6>
                                                                                                                        <input type="text" id="pincode-input3" class="form-control" placeholder="" maxlength="" value="INVESTMENTS">
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="col-md-12">
                                                                                                                    <div class="debit-bank-name digits">
                                                                                                                        <h6>Payer Reference</h6>
                                                                                                                        <input type="text" id="pincode-input4" class="form-control" placeholder="" maxlength="">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-md-12">
                                                                                                                    <div class="debit-bank-pera">
                                                                                                                        <p>I/We authorize until further notice to debit my account with all amounts with the authorization code for direct debit.I/ We acknowledge and accept that the bank accepts the authority only upon the conditions listed below.</p>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                              
                                                                                                                <div class="col-md-6">
                                                                                                                    <div class="debit-bank-pera">
                                                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl debit-own-btn next7">I ACCEPT</a>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-md-12">
                                                                                                                    <div class="term-value">
                                                                                                                        <h5>CLIENT TERMS AND CONDITIONS</h5>
                                                                                                                        <p>
                                                                                                                        <ol>
                                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                                                                                                <ol>
                                                                                                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</li>
                                                                                                                                </ol>
                                                                                                                            </li>
                                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                                                                                                <ol>
                                                                                                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</li>
                                                                                                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</li>
                                                                                                                                </ol>
                                                                                                                            </li>
                                                                                                                        </ol>
                                                                                                                        </p>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                   
<!--                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div class="payment-btn">
                                                                                                <a href="javascript:void(0)" class="btn btn-primary btn-styl  payment-back previous7" id="">Back</a>
                                                                                                <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back next7" id="">Next</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>-->
                                                                               

                                                                            </div>
                                                                         <div class="row">
                                                                                    <div class="col-md-12">
                                                                                       <div class="debit-bank-pera">
                                                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl debit-own-btn previous7">BACK</a>
                                                                                                                    </div>
                                                                                    </div>
                                                                                </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                </fieldset>
                                                <fieldset id="step8">
                                                        <div class="row">
                                                            <div class="col-md-12 thanku">
                                                                <div class="form-data">
                                                                    <div class="form-data">
                                                                        <div class="container">
                                                                            <div class="row">
                                                                                <div class="col-md-12 box-payment-space">
                                                                                        <div class="line-progress">
                                                                                                     <div class="line-bar2">
                                                                                <div class="line-dot">
                                                                                </div>
                                                                                                           <div class="line-dot progress-4">
                                                                                </div>
                                                                                     <div class="line-dot progress-1 ">
                                                                                </div>
                                                                                <div class="line-dot progress-2">
                                                                                </div>>
                                                                                <div class="line-dot progress-3">
                                                                                </div>
                                                                                  <div class="line-dot add-dot dot-show-4">
                                                                                </div>
                                                                            </div>
                                                                                            <div class="line-data">
                                                                                                <span>Amount</span>
                                                                                                <span>You</span>
                                                                                                <span>Recipient</span>
                                                                                                <span>Review</span>
                                                                                                <span>Pay</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="input-content-text">
                                                                                            <div class="row">
                                                                                                <div class="col-md-12">
                                                                                                    <div class="trans-detail-2">
                                                                                                        <div class="trans-bank-2">
                                                                                                            <h6 class="body-data payment-thanks">Thanks</h6>
                                                                                                            <!--                                                                                                    <a href="">Edit</a>-->
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                        </div>

                                        </div>
                                        </div>
                                        <div class="display-type"></div>
                                        </div>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
                                        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
                                        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
                                        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
                                        <!-- <script src="resources/bower_components/moment/moment.js" ></script>
                                        
                                        <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
                                        <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
                                        <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

                                        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
                                        <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
                                        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
                                        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
                                        <script src="http://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
                                        <script src="http://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
                                        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
                                        <script src="https://code.highcharts.com/highcharts.js" ></script> 
                                        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
                                        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
                                        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
                                        <script src="https://code.highcharts.com/modules/cylinder.js"></script>
                                        <script type="text/javascript" src="resources/js/bootstrap-pincode-input.js"></script>
                                        <script>
                                            $(document).ready(function () {
                                                $('#pincode-input2').pincodeInput({hidedigits: false, inputs: 12});
                                                $('#pincode-input1').pincodeInput({hidedigits: false, inputs: 7});
                                                $('#pincode-input3').pincodeInput({hidedigits: false, inputs: 12});
                                                $('#pincode-input4').pincodeInput({hidedigits: false, inputs: 25});

                                            });
                                        </script>
                                        <script type="text/javascript">
                                            $(window).load(function () {

                                                $(".loader").fadeOut("slow");
                                            });
                                        </script>

                                        <!--        <script>
                                                    //        $('#viewmore1').click(function () {
                                                    //            $('.hidediv1').show();
                                                    //            $('#viewmore1').hide();
                                                    //        });
                                                    //        $('#viewmore2').click(function () {
                                                    //            $('.hidediv2').show();
                                                    //            $('#viewmore2').hide();
                                                    //        });
                                                    //        $('#viewmore3').click(function () {
                                                    //            $('.hidediv3').show();
                                                    //            $('#viewmore3').hide();
                                                    //        });
                                                    $('.clickinput').on("click", function () {
                                                        var recemt = $(this).closest('.funds-deatil');
                                                        recemt.find(".offer-input").toggle();
                                                    });
                                                    $(document).ready(function () {
                                                        $(".loader").fadeOut("slow");
                                                        $('.hidediv1').hide();
                                                        $('.hidediv2').hide();
                                                        $('.hidediv3').hide();
                                                    });
                                                    $('.democlick').click(function () {
                                                        var data = $(this).data("target");
                                                        $('.showclick').hide();
                                                        $(data).show();
                                                    });
                                                </script>-->
                                        <script>
                                            $(".calculator-dropdown ul").on("click", ".init", function () {
                                                $(this).closest(".calculator-dropdown ul").children('li:not(.init)').toggle();
                                            });

                                            var allOptions = $(".calculator-dropdown ul").children('li:not(.init)');
                                            $(".calculator-dropdown ul").on("click", "li:not(.init)", function () {
                                                allOptions.removeClass('selected');
                                                $(this).addClass('selected');
                                                $(".calculator-dropdown ul").children('.init').html($(this).html());
                                                allOptions.toggle();
                                            });
                                        </script>
                                        <script>
                                            $(".calculator-dropdown2 ul").on("click", ".init2", function () {
                                                $(this).closest(".calculator-dropdown2 ul").children('li:not(.init2)').toggle();
                                            });

                                            var allOption = $(".calculator-dropdown2 ul").children('li:not(.init2)');
                                            $(".calculator-dropdown2 ul").on("click", "li:not(.init2)", function () {
                                                allOption.removeClass('selected');
                                                $(this).addClass('selected');
                                                $(".calculator-dropdown2 ul").children('.init2').html($(this).html());
                                                allOption.toggle();
                                            });
                                        </script>
                                        <script>
                                            $(document).ready(function () {
                                                $(".data-toggle").hide();
                                                $(".loader").fadeOut("slow");
                                                $(".see-toggle").click(function () {
                                                    $(".data-toggle").toggle();
                                                });
                                                $("#continue").click(function () {
                                                    window.location.href = './regularmethod';
                                                });
                                            });
                                        </script>
                                        <script>
                                            $(".previous2").click(function () {
                                                $("#step2").hide();
                                                $("#step1").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".previous-off").click(function () {
                                                $("#step21").hide();
                                                $("#step1").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".previous3").click(function () {
                                                $("#step3").hide();
                                                $("#step2").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".previous4").click(function () {
                                                $("#step4").hide();
                                                $("#step3").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".previous5").click(function () {
                                                $("#step5").hide();
                                                $("#step3").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".previous6").click(function () {
                                                $("#step6").hide();
                                                $("#step4").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".previous7").click(function () {
                                                $("#step7").hide();
                                                $("#step4").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next1").click(function () {
                                                $("#step2").show();
                                                $("#step1").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next-off").click(function () {
                                                $("#step21").show();
                                                $("#step1").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next2").click(function () {
                                                $("#step3").show();
                                                $("#step2").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next21").click(function () {
                                                $("#step3").show();
                                                $("#step21").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next3").click(function () {
                                                $("#step4").show();
                                                $("#step3").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next4").click(function () {
                                                $("#step6").show();
                                                $("#step4").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".nextcredit").click(function () {
                                                $("#step5").show();
                                                $("#step3").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next5").click(function () {
                                                $("#step8").show();
                                                $("#step5").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next6").click(function () {
                                                $("#step8").show();
                                                $("#step6").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next7").click(function () {
                                                $("#step8").show();
                                                $("#step7").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next-debit").click(function () {
                                                $("#step7").show();
                                                $("#step4").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                        </script>

                                        </body>
                                        </html>
