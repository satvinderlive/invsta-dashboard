var width = 0;
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$('#email').keyup(function () {
    $('#error-email').text('');
});
$('#password').keyup(function () {
    $('#error-password').text('');
});
$('.form-control').keyup(function () {
    $('.error').text('');
});
$('#Licence_first_name').keyup(function () {
    $('#error-Licence_first_name').text('');
});
$('#Licence_last_name').keyup(function () {
    $('#error-Licence_last_name').text('');
});
$('.error-remove').keyup(function () {
    $('.error-minor').text('');
});
$('.error-remove').change(function () {
    $('.error-minor').text('');
});
$('.input-field').change(function () {
    $('.error').text('');
});
// $('.countrynameone').click(function () {
//                alert("country test error remove");
//            });
//           
$('#passport_first_name').keyup(function () {
    $('#error-passport_first_name').text('');
});
$('#passport_last_name').keyup(function () {
    $('#error-passport_last_name').text('');
});
$('#other_first_name').keyup(function () {
//     alert("passlast");
    $('#error_other_first_name').text('');
});
$('#other_last_name').keyup(function () {
    $('#error_other_last_name').text('');
});
$('.checkbox-check').change(function () {
    $('.error').text('');
});
$('.selectoption').change(function () {
    $('.error').text('');
});
$('.attach-btn').change(function () {
    $('.error').text('');
});
$('.attach-btn2').change(function () {
    $('.error').text('');
});
$('.input-field').keyup(function () {
    $('.error').text('');
});
$('#fullName').keyup(function () {
    $('#span-fullName').text('');
});
$('#dob').change(function () {
    $('#span-dob').text('');
});
$('#occupation').keyup(function () {
    $('#span-occupation').text('');
});
$('#address').keyup(function () {
    $('#span-address').text('');
});
$('#mobile').keyup(function () {
    $('#span-mobile').text('');
});
$('#mobileNo').keyup(function () {
    $('#span-mobileNo').text('');
});
$('#verification').keyup(function () {
    $('#error-verification').text('');
});
$('#Licence_number').keyup(function () {
    $('#error-Licence_number').text('');
});
$('#Expiry_date').change(function () {
    $('#error_Expiry_date').text('');
});
$('#Date_of_Birth').change(function () {
    $('#spanDate_of_Birth').text('');
});
$('#dob2').change(function () {
    $('#error_dob2').text('');
});
$('#IdExpirydate').change(function () {
    $('#error_IdExpirydate').text('');
});
$('#licence_Verson_number').keyup(function () {
    $('#error_licence_Verson_number').text('');
});
$('#passport_number').keyup(function () {
    $('#error_passport_number').text('');
});
$('#passport_issue_by').keyup(function () {
    $('#error_passport_issue_by').text('');
});
$('#Type_of_ID').keyup(function () {
    $('#error_TypeofID').text('');
});
$('#other_id_issueBy').keyup(function () {
    $('#error_other_id_issueBy').text('');
});
$('#IRD_Number').keyup(function () {
    $('#error_IRD_Number').text('');
});
$('#countryOfTaxResidence').change(function () {
    $('#countrynameexcludenz-error1').text('');
});
$('#countrynameexcludenz').change(function () {
    $('#countrynameexcludenz-error').text('');
});
$('#taxIdenityNumber').keyup(function () {
    $('.tin-error').text('');
});
$('#bank_name').change(function () {
    $('#error_bank_name').text('');
});

$('#acount_holder_name').keyup(function () {
    $('#error_acount_holder_name').text('');
});
$('#acount_holder_number').keyup(function () {
    $('#error_acount_holder_number').text('');
});
$("input[type=radio]").click(function () {
    $('#error-generateOtp').text('');
});
$('#otp').keyup(function () {
    $('#error-generateOtp').text('');
});
//company detail js///
$('#companyName').keyup(function () {
    $('#spanCompanyName').text('');
});
$('#countryCode').keyup(function () {
    $('#spanCountryCode').text('');
});
$('#Date_of_incorporation').keyup(function () {
    $('#spanCompanyDate').text('');
});
$('#registrationNumber').keyup(function () {
    $('#spanRegistrationNumber').text('');
});
$('#companyAddress').keyup(function () {
    $('#spanCompanyAddress').text('');
});
$('#postalAddress').keyup(function () {
    $('#spanPostalAddress').text('');
});
$('#fname').keyup(function () {
    $('#spanfname').text('');
});
$('#Date_of_Birth').keyup(function () {
    $('#spanDate_of_Birth').text('');
});
$('#cOccupation').keyup(function () {
    $('#spanOccupation').text('');
});
$('#holderCountryOfResidence').keyup(function () {
    $('#spanHolderCountryOfResidence').text('');
});
$('#address').keyup(function () {
    $('#spanHomeaddress').text('');
});
$('#mobileNo').keyup(function () {
    $('#spanOPmobileNo').text('');
});
$('#verificationa').keyup(function () {
    $('#spanverificationa').text('');
});
$('#licenseNumber').keyup(function () {
    $('#spanlicenseNumber').text('');
});
$('#Expirydate').keyup(function () {
    $('#spanExpirydate').text('');
});
$('#Versionnumber').keyup(function () {
    $('#spanVersionnumber').text('');
});
$('#Passportnumber').keyup(function () {
    $('#spanPassportnumber').text('');
});
$('#PExpirydate').keyup(function () {
    $('#spanPExpirydate').text('');
});
$('#TypeofID').keyup(function () {
    $('#spanTypeofID').text('');
});
$('#typeExpirydate').keyup(function () {
    $('#spantypeExpirydate').text('');
});
$('#Iverificationa').keyup(function () {
    $('#spanIverificationa').text('');
});
$('#IRDNumber').keyup(function () {
    $('#spanIRDNumber').text('');
});
$('#comIRDNumber').keyup(function () {
    $('#spancomIRDNumber').text('');
});
$('#ok').keyup(function () {
    $('#error_acount_holder_number').text('');
});
$('#nameOfAccount').keyup(function () {
    $('#spannameOfAccount').text('');
});
$('#more_licence_first_name').keyup(function () {
    $('#more_error_licence_first_name').text('');
});
$('#more_licence_last_name').keyup(function () {
    $('#more_error_licence_last_name').text('');
});
$('#more_passport_first_name').keyup(function () {
    $('#more_error_passport_first_name').text('');
});
$('#PExpirydate').change(function () {
    $('#spanPExpirydate').text('');
});
$('#attachbankfile').change(function () {
    $('.error_attachbankfile').text('');
});
$('#more-investor-passportExpiryDate').change(function () {
    $('#more-investor-passportExpiryDate-error').text('');
});
$('#licence_first_name').keyup(function () {
    $('#error_licence_first_name').text('');
});
$('#licence_last_name').keyup(function () {
    $('#error_licence_last_name').text('');
});
$('#more_passport_last_name').keyup(function () {
    $('#more_error_passport_last_name').text('');
});
$('#passport_last_name').keyup(function () {
    $('#error_passport_last_name').text('');
});
$('#passport_first_name').keyup(function () {
    $('#error_passport_first_name').text('');
});
$('#first_name').keyup(function () {
    $('#error_first_name').text('');
});
$('#last_name').keyup(function () {
    $('#error_last_name').text('');
});


function  genrateotp(mNo, cCo, sTy) {
    var url = 'https://jj1stbnzb3.execute-api.ap-southeast-2.amazonaws.com/otp/generateotp?pn=' + mNo + '&cc=' + cCo + '&sTy=' + sTy;
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (data) {
//                  alert("otp genrated" + data);
        },
        error: function (e) {
//            alert("otp failed");
        }
    });

}




function validateOTP(mNo, cCo, otp) {
    var url = 'https://jj1stbnzb3.execute-api.ap-southeast-2.amazonaws.com/otp/verifyotp?pn=' + mNo + '&cc=' + cCo + '&code=' + otp;
    $.ajax({
        url: url,
        type: 'POST',
        async: true,
        dataType: "json",
        success: function (data) {
            console.log(" sucess" + JSON.stringify(data.status));
            if (data.status === "approved") {
                $('.spanverification').html("Verification successful");
                $('.spanverification').css("color", "green").css("fontSize", "10px");

            } else {
                $('.spanverification').html("Invalid code");
                $('.spanverification').css("color", "red").css("fontSize", "10px");

            }
        },
        error: function (e) {
//                            alert();
//                            $('.msg').html("Wrong OTP");
//                        alert("otp failed" + JSON.stringify(e));
        }
    });
}
function validate(ele) {
    current_fs = ele.parent();
    regexp = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\+\-\_\@\#\$\%\&\!\*\^\=\(\)\.\,]).{8,15})/;
    var id = current_fs.attr('id');
    if (id === 'first-fs') {
        var result = true;
        var email = document.getElementById('email');
        if (email.value === '') {
            document.getElementById('error-email').innerHTML = "Enter your email.";
            result = result && false;
        }
        var password = document.getElementById('password');
        if (password.value === '') {
            document.getElementById('error-password').innerHTML = "Enter your password.";
            result = result && false;
        }
        if (!regexp.test(password.value)) {
            document.getElementById('error-password').innerHTML = "Password must have one upper case letter, one lower case letter, one special character, one numeric character and length must be in 8 to 15 characters";
            result = result && false;
        }
        return result;
    } else if (id === 'second-fs') {
        var fullName = document.getElementById('fullName');
        var result = true;
        regnameexp = /^[a-zA-Z\s]+$/;
        if (fullName.value === '') {
            document.getElementById('error-fullName').innerHTML = "Enter your fullName.";
            result = result && false;
        }
        if (!regnameexp.test(fullName.value)) {
            document.getElementById('error-fullName').innerHTML = "Name should be in alphabetical characters";
            result = result && false;
        }
        var dob = document.getElementById('dob');
        if (dob.value === '') {
            document.getElementById('error-dob').innerHTML = "Enter your dob.";
            result = result && false;
        }
        return result;
    } else if (id === 'third-fs') {
        var inviteCode = document.getElementById('inviteCode');
        var result = true;
        if (inviteCode.value === '') {
            document.getElementById('error-inviteCode').innerHTML = "Enter your invite code.";
            result = result && false;
        }
        return result;
    } else if (id === 'fourth-fs') {
        var countryCode = document.getElementById('countryCode');
        var result = true;
        if (countryCode.value === '') {
            document.getElementById('error-countryCode').innerHTML = "Enter your country code.";
            result = result && false;
        }
        var mobileNo = document.getElementById('mobileNo');
        if (mobileNo.value === '') {
            document.getElementById('error-mobileNo').innerHTML = "Enter your mobile no.";
            result = result && false;
        }
        return result;
    }
    return true;
}
function validation() {
    var mobileNo = document.getElementById('mobileNo');
    var result = true;
    if (!($("#f-option").is(":checked") || $("#s-option").is(":checked"))) {
        document.getElementById('error-generateOtp').innerHTML = "Please choose one option to send OTP";
        result = result && false;
    }
    if (mobileNo.value === '') {
        document.getElementById('error-generateOtp').innerHTML = "Mobile number is required.";
        result = result && false;
    }
    return result;
}
function moveNextProcess(ele) {
    if (animating)
        return false;
    animating = true;
    current_fs = ele.parent();
    next_fs = ele.parent().next();
    //activate next step on progressbar using the index of next_fs
//    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
//    var elem = document.getElementById("myBar");
//    if (width < 100) {
//        width += 33;
//        elem.style.width = width + '%';
//    }
//hide the current fieldset with style
    current_fs.animate(
            {
                opacity: 0
            },
            {
                step: function (now, mx) {
//as the opacity of current_fs reduces to 0 - stored in "now"
//1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50) + "%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({
                        'transform': 'scale(' + scale + ')',
                        'position': 'relative'
                    });
                    next_fs.css({'left': left, 'opacity': opacity});
                },
                duration: 0,
                complete: function () {
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
}

function movePrevProcess(ele) {
    if (animating)
        return false;
    animating = true;
    current_fs = ele.parent();
    previous_fs = ele.parent().prev();
    //de-activate current step on progressbar
//    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
//    var elem = document.getElementById("myBar");
//    if (width < 100) {
//        width = width - 33;
//        elem.style.width = width + '%';
//        //elem.innerHTML = width + '%';
//    }
//hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function (now, mx) {
//as the opacity of current_fs reduces to 0 - stored in "now"
//1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1 - now) * 50) + "%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'left': left});
            previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
        },
        duration: 0,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
}

$(".submit").click(function () {
    return false;
});

//jQuery.validator.addMethod("onespecial", function (value, element) {
//    var pattern = /^(?=.*[0-9])|(?=.*[!@#$%^&*()-+=]).*$/;
//    return (pattern.test(value));
//}, "Your password must contain 1 special character.");
///**
// * Custom validator for contains at least one upper-case letter.
// */
//$.validator.addMethod("OneUppercaseLetter", function (value, element) {
//    return this.optional(element) || /[A-Z]+/.test(value);
//}, "Must have at least one uppercase letter");

$("#msform").validate({
    rules: {
        email: "required",
        email: true,
        password: {
            required: true,
            minlength: 8,
            maxlength: 15,
            onespecial: true,
            OneUppercaseLetter: true
        },
        fullName: {
            required: true,
            minlength: 3,
            maxlength: 30
        },
        phone: {
            required: true,
            number: true,
            minlength: 5,
            maxlength: 20
        },
        dob: {
            required: true,
        },
        gender: {
            required: true
        }
        ,
        addres: {
            required: true,
        },
        income1: {
            required: true,
        },
        income2: {
            required: true,
        },
        bankName: {
            required: true,
        },
        accNumber: {
            required: true,
            number: false,
            maxlength: 18
        },
//            emotion:{
//              required: true,   
//            }

        tinRadio: {
            required: true,
        },
        ird: {
            required: true,
        },
    },
    messages: {
        email: {
            required: "Enter your Email",
            email: "Please enter a valid email address.",
        },
        password: {
            required: "Enter your password",
            pass: "Password should be minimum of 8 and maximum of 15 characters."
        },
        fullName: {
            required: "Enter your full name",
        },
        phone: {
            required: "Enter your phone/mobile number",
            number: "Please enter numeric value with no spaces"
        },
        dob: {
            required: "Enter your date of birth",
        },
        gender: {
            required: "Please select gender",
        },
        addres: {
            required: "Please enter address",
        },
        income1: {
            required: "Please enter amount",
        },
        income2: {
            required: "Please enter  amount",
        },
        bankName: {
            required: "Please enter  Bank Name",
        },
        accNumber: {
            required: "Please enter  Account Number.",
            number: "Please enter numeric value with no spaces.",
            maxlength: " maximum of 20 characters."
        },
//             emotion:{
//              required: "Please select atleast one option",   
//            }
    }
});
isNotEmpty = function (ele) {
    if (ele.val() !== "") {
        return true;
    } else {
        return false;
    }
};




      