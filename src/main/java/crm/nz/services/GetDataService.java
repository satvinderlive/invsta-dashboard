/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.services;

import crm.nz.table.bean.SecuredUser;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class GetDataService {

    public String getUserId(SecuredUser user, String UserId) {
        if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
            UserId = user.getUser_id();
        } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
            if (UserId == null || UserId != null && UserId.isEmpty()) {
                UserId = user.getUser_id();
            }
        } else if ("ADMIN".equalsIgnoreCase(user.getRole())) {
            if (UserId == null || UserId != null && UserId.isEmpty()) {
                UserId = user.getUser_id();
            }
        }
        return UserId;
    }
}
