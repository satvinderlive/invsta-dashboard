/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.services;

import com.google.gson.Gson;
import crm.nz.authorization.GetOAuth2TokenService;
import crm.nz.authorization.OnboardingsServices;
import crm.nz.beans.acc.api.Application;
import crm.nz.beans.acc.api.Beneficiary;
import crm.nz.beans.acc.api.JointDetailBean;
import crm.nz.beans.acc.api.PersonDetailsBean;
import crm.nz.components.CommonMethods;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class ThirdAPIRegisterServiceImpl implements ThirdAPIRegisterService {

    @Autowired
    private GetOAuth2TokenService oAuth2TokenService;
    @Autowired
    private OnboardingsServices onboardingsServices;
    @Autowired
    private Gson gson;
    @Autowired
    private CommonMethods common;

    @Override
    public PersonDetailsBean createIndividual(PersonDetailsBean individualAccount) {
        System.out.println("2");
        String clientCredentials = oAuth2TokenService.getClientCredentials();
        StringBuilder createApplication = onboardingsServices.createApplication(clientCredentials);
        Application application = gson.fromJson(createApplication.toString(), Application.class);
        if (application != null) {
            List<Beneficiary> beneficiaries = application.getBeneficiaries();
            Beneficiary beneficiary = beneficiaries.get(0);
            onboardingsServices.updateBeneficiary(clientCredentials, beneficiary.getApplicationId(), beneficiary.getBeneficiaryId(), beneficiary.getThirdAPIUpdateDetails(individualAccount));
//            onboardingsServices.fetchBeneficiaryByIds(clientCredentials, beneficiary.getApplicationId(), beneficiary.getBeneficiaryId());
            System.out.println("createIndividual" + application);
            individualAccount.setApplicationId(application.getApplicationId());
            individualAccount.setBeneficiaryId(application.getPrimaryBeneficiaryId());
        }
        System.out.println("3");
        System.out.println(individualAccount);
        return individualAccount;
    }

    @Override
    public JointDetailBean createJoint(JointDetailBean jointAccount) {
        String clientCredentials = oAuth2TokenService.getClientCredentials();
        StringBuilder createApplication = onboardingsServices.createApplication(clientCredentials);
        Application application = gson.fromJson(createApplication.toString(), Application.class);
        if (application != null) {
            jointAccount.setApplicationId(application.getApplicationId());
            jointAccount.setBeneficiaryId(application.getPrimaryBeneficiaryId());
            List<Beneficiary> beneficiaries = application.getBeneficiaries();
            Beneficiary beneficiary = beneficiaries.get(0);
            onboardingsServices.updateBeneficiary(clientCredentials, beneficiary.getApplicationId(), beneficiary.getBeneficiaryId(), beneficiary.getThirdAPIUpdateDetails(jointAccount));
//            onboardingsServices.fetchBeneficiaryByIds(clientCredentials, beneficiary.getApplicationId(), beneficiary.getBeneficiaryId());
            for (JointDetailBean moreInvestor : jointAccount.getMoreInvestorList()) {
                moreInvestor.setApplicationId(application.getApplicationId());
                moreInvestor.setBeneficiaryId("0");
                String date = common.changeFormat(moreInvestor.getDate_of_Birth(),CommonMethods.format8, CommonMethods.format4);
                moreInvestor.setDate_of_Birth(date);
                System.out.println("---" + moreInvestor.getThirdAPICreateDetails());
                StringBuilder createMoreInvestorBeneficiary = onboardingsServices.createBeneficiary(clientCredentials, application.getApplicationId(), moreInvestor.getThirdAPICreateDetails());
                Beneficiary moreInvestorBeneficiary = gson.fromJson(createMoreInvestorBeneficiary.toString(), Beneficiary.class);
                moreInvestor.setBeneficiaryId(moreInvestorBeneficiary.getBeneficiaryId());
            }
        }
        return jointAccount;
    }
}
