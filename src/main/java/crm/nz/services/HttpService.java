/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.services;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.net.ssl.HttpsURLConnection;

/**
 *
 * @author IESL
 */
public class HttpService {

    public StringBuilder getResponse(URL url, String bearerToken) {
        BufferedReader reader = null;
        try {
            System.out.println("Url--->" + url);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestProperty("Authorization", "Bearer " + bearerToken);
            connection.setDoOutput(true);
            connection.setRequestMethod("GET");
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = null;
            StringBuilder builder = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public StringBuilder postResponse(URL url, String bearerToken, String body, HashMap<String, String> headers, HashMap<String, String> params) {
        try {
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            if (bearerToken != null) {
                connection.setRequestProperty("Authorization", "Bearer " + bearerToken);
            }
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.addRequestProperty("User-Agent", "Mozilla/5.0");
            if (headers != null) {
                Set<Map.Entry<String, String>> headerSet = headers.entrySet();
                for (Map.Entry<String, String> header : headerSet) {
                    connection.setRequestProperty(header.getKey(), header.getValue());
                }
            }
            if (body != null) {
                connection.setRequestProperty("Content-Length", Integer.toString(body.length()));
                connection.getOutputStream().write(body.getBytes("UTF8"));
            }
                    System.out.println("insiede dl verify http serv 1----------------------------------");

            try (DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream())) {
                dataOutputStream.flush();
            }
            int responseCode = connection.getResponseCode();
                    System.out.println("insiede dl verify httpser 2----------------------------------");

            System.out.println("responseCode" + responseCode);
            String responseMessage = connection.getResponseMessage();
            System.out.println("responseMessage" + responseMessage);
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder builder = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                builder.append(inputLine);
            }
            in.close();
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public StringBuilder postResponse(URL url, String bearerToken, String body) {
        try {
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            if (bearerToken != null) {
                connection.setRequestProperty("Authorization", "Bearer " + bearerToken);
            }
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            connection.setRequestProperty("Content-Type", "application/json");
            if (body != null) {
                connection.setRequestProperty("Content-Length", Integer.toString(body.length()));
                connection.getOutputStream().write(body.getBytes("UTF8"));
            }
            try (DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream())) {
                dataOutputStream.flush();
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder builder = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                builder.append(inputLine);
            }
            in.close();
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public StringBuilder putResponse(URL url, String bearerToken, String body) {
        try {
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestProperty("Authorization", "Bearer " + bearerToken);
            connection.setDoOutput(true);
            connection.setRequestMethod("PUT");
            connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            connection.setRequestProperty("Content-Type", "application/json");
            if (body != null) {
                connection.setRequestProperty("Content-Length", Integer.toString(body.length()));
            }
            if (body != null) {
                connection.getOutputStream().write(body.getBytes("UTF8"));
            }
            try (DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream())) {
                dataOutputStream.flush();
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder builder = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                builder.append(inputLine);
            }
            in.close();
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void putRequest(URL url, String bearerToken, String body) {
        try {
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestProperty("Authorization", "Bearer " + bearerToken);
            connection.setDoOutput(true);
            connection.setRequestMethod("PUT");
            connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            connection.setRequestProperty("Content-Type", "application/json");
            if (body != null) {
                connection.setRequestProperty("Content-Length", Integer.toString(body.length()));
            }
            if (body != null) {
                connection.getOutputStream().write(body.getBytes("UTF8"));
            }
            try (DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream())) {
                dataOutputStream.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public StringBuilder deleteResponse(URL url, String bearerToken) {
        BufferedReader reader = null;
        try {
            System.out.println("Url--->" + url);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestProperty("Authorization", "Bearer " + bearerToken);
            connection.setDoOutput(true);
            connection.setRequestMethod("DELETE");
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = null;
            StringBuilder builder = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
