/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.services;

import com.google.gson.Gson;
import crm.nz.components.CommonMethods;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service
public class ObjectCastServiceImpl implements ObjectCastService {

    @Override
    public <T> T jSONcast(Class<T> clazz, String jsonInString) throws IOException {
        Gson gson = new Gson();
        T json = gson.fromJson(jsonInString, clazz);
        return json;
    }

    @Override
    public <T> T jSONcast(Class<T> clazz, Map map) throws IOException {
        Gson gson = new Gson();
        String toJson = gson.toJson(map);
        return jSONcast(clazz, toJson);
    }

    @Override
    public <T> T jSONcast(Class<T> clazz, Object obj) throws IOException, InstantiationException, IllegalAccessException {
        if (obj instanceof Map) {
            return jSONcast(clazz, (Map) obj);
        } else if (obj instanceof String) {
            return jSONcast(clazz, (String) obj);
        } else {
            return clazz.newInstance();
        }
    }

}
