/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.security;

import crm.nz.table.bean.SecuredUser;
import java.util.List;
import org.springframework.security.core.session.SessionInformation;

/**
 *
 * @author IESL
 */
public interface SessionManagementService {

    public List<SecuredUser> getAllOnlineUsers();

    public List<SessionInformation> getAllSessions(SecuredUser user, boolean bln);

    public SessionInformation getSessionInformation(String username);

    public void refreshLastRequest(String username);

    public void registerNewSession(String username, SecuredUser user);

    public void removeSessionInformation(String username);
}
