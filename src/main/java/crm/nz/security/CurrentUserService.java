/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.security;

import crm.nz.repository.CommonRepository;
import crm.nz.table.bean.SecuredUser;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author Administrator
 */
public interface CurrentUserService {

    default SecuredUser getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        SecuredUser user = null;
        if (authentication != null) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof SecuredUser) {
                user = ((SecuredUser) principal);
            }
        }
        if (user != null) {
            return user;
        }
//        securityServiceImpl.autologin(guestEmailId, guestPassword);
        return user;
    }

    default SecuredUser getCurrentUser(GrantedAuthority... roles) {
        SecuredUser securedUser = getCurrentUser();
        if (securedUser != null) {
            for (GrantedAuthority role : roles) {
                if (securedUser.getAuthorities().contains(role)) {
                    return securedUser;
                }
            }
            return null;
        }
        return securedUser;
    }

    default void logout(CommonRepository repository) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
//            Object principal = auth.getPrincipal();
//            if (principal instanceof SecuredUser) {
//                SecuredUser user = ((SecuredUser) principal);
//                repository.save(user.getId(), "LOGGED OUT");
//            }
            SecurityContextHolder.getContext().setAuthentication(null);
        }
    }

    default String requestedDomain(HttpServletRequest request) {
        String scheme = request.getScheme(); // "http"/"https"
        String serverName = request.getServerName(); // "localhost"/"beta.invsta.com"
        if ("localhost".equalsIgnoreCase(serverName) && request.getServerPort() > 0) {
            String serverPort = String.valueOf(request.getServerPort()); // "8080"
            String projectName = request.getContextPath(); // "crm-invsta-nz"
            return scheme.concat("://").concat(serverName).concat(":").concat(serverPort).concat("/").concat(projectName).concat("/");
        } else {
            return scheme.concat("://").concat(serverName).concat("/");
        }
    }
}
