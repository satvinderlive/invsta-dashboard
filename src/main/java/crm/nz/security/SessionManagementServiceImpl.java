/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.security;


import crm.nz.table.bean.SecuredUser;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service
public class SessionManagementServiceImpl implements SessionManagementService {

  
    @Autowired
    private SessionRegistry sessionRegistry;

    @Override
    public List<SecuredUser> getAllOnlineUsers() {
        if (sessionRegistry == null) {
            return null;
        }
        return (List) sessionRegistry.getAllPrincipals();
    }

    @Override
    public List<SessionInformation> getAllSessions(SecuredUser user, boolean bln) {
        if (sessionRegistry == null) {
            return null;
        }
        return (List) sessionRegistry.getAllSessions(user, bln);
    }

    @Override
    public SessionInformation getSessionInformation(String username) {
        if (sessionRegistry == null) {
            return null;
        }
        return sessionRegistry.getSessionInformation(username);
    }

    @Override
    public void refreshLastRequest(String username) {
        sessionRegistry.refreshLastRequest(username);
    }

    @Override
    public void registerNewSession(String username, SecuredUser user) {
        sessionRegistry.registerNewSession(username, user);
    }

    @Override
    public void removeSessionInformation(String username) {
        sessionRegistry.removeSessionInformation(username);
    }

}
