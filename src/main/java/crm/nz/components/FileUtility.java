/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.components;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Administrator
 */
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class FileUtility {

    @Autowired
    private ServletContext servletContext;

    public String rootDirectoryPath() {
        String path = servletContext.getRealPath("") + File.separator + "resources";
        return path;
    }

    public String imagesRootDirectoryPath() {
        return rootDirectoryPath() + File.separator + "img";
    }

    public String mailTemplatesDirectoryPath() {
        return rootDirectoryPath() + File.separator + "mail-templates";
    }

    public String cryptolabsDirectoryPath(String folderName) {
        if (folderName != null) {
            return rootDirectoryPath() + File.separator + "cryptolabs" + File.separator + folderName;
        }
        return rootDirectoryPath() + File.separator + "cryptolabs";
    }

    public File rootDirectory() {
        // Creating or fetch the directory to store file
        File root = new File(rootDirectoryPath());
        if (!root.exists()) {
            root.mkdirs();
        }
        return root;
    }

    public File imagesRootDirectory() {
        // Creating or fetch the directory to store file
        File root_images = new File(imagesRootDirectoryPath());
        if (!root_images.exists()) {
            root_images.mkdirs();
        }
        return root_images;
    }

    public File mailTemplatesDirectory() {
        // Creating or fetch the directory to store file
        File dir = new File(mailTemplatesDirectoryPath());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    public File cryptolabsDirectory() {
        // Creating or fetch the directory to store file
        File dir = new File(cryptolabsDirectoryPath(null));
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    

    public File storeImage(long createdBy, MultipartFile file, String imageName) throws IOException {
        if (!file.isEmpty()) {
            byte[] imageByte = file.getBytes();
            return storeImage(createdBy, imageByte, imageName);
        }
        return null;
    }

    public File storeImage(long createdBy, byte[] imageByte, String imageName) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
        BufferedImage image = ImageIO.read(bis);
        bis.close();
        String dirPath = imagesRootDirectoryPath();
        File outputfile = new File(dirPath);
        if (!outputfile.exists()) {
            outputfile.mkdirs();
        }
        outputfile = new File(dirPath + File.separator + imageName);
        int lastIndexOf = imageName.lastIndexOf(".") + 1;
        String extension = imageName.substring(lastIndexOf);
        ImageIO.write(image, extension, outputfile);
        return outputfile;
    }

   

    public File getFile(String fileDirectory, String filePath) {
        File f = new File(fileDirectory + File.separator + filePath);
        if (f.exists()) { 
            return f;
        } else {
            try {
                f.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
            }
            return f;
        }
    }

    public String getFileExists(String fileDirectoryPath, String filePath, String noFile) {
        String fileExists = "";
        fileExists = filePath.replace('/', File.separatorChar);
        String path = fileDirectoryPath + File.separator + fileExists;
        File f = new File(path);
        if (f.exists()) {
            return fileExists;
        } else {
            return noFile;
        }
    }

    public Set<String> getFileNames(File folder) {
        File[] listOfFiles = folder.listFiles();
        Set<String> fileNames = new HashSet<String>();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                fileNames.add(listOfFiles[i].getName());
            }
        }
        return fileNames;
    }
}
