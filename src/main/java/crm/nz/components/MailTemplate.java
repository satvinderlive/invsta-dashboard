/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.components;

import org.springframework.stereotype.Component;

/**
 *
 * @author innovative002
 */
@Component
public class MailTemplate {

    public String pendingRegistrationSubject = "Your Mint account application";

    public String FinalRegistrationSubject = "Verify your email";

    public String emailVerificationSubject = "Welcome to Mint Asset Management";

    public String PasswordChangeSubject = "Reset your Password";

    public String SellFundsSubject = "We’re processing your request";

    public String WithdrawalToBankSubject = "Your withdrawal has been processed";

    public String InvestmentPaymentMailSubject = "We’re processing your request";
    
    public String  ConfirmingInvestment = "Your investment has allocated";

}
