
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.components;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;
import javax.imageio.ImageIO;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Administrator
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class FileMethods {

    @Autowired
    private PathDirector pathDirector;

    public S3Object storeTos3Pics(MultipartFile s3file,String userId,String fullname,String extension) {
        String fileName=fullname+"_"+userId+"."+extension;
        String folderName="beta-pics-folder";
        return storeTos3(s3file, fileName,folderName);
    }
    public S3Object storeTos3Pdf(MultipartFile s3file,String userId,String fullname,String extension) {
        String fileName=fullname+"_"+userId+"."+extension;
        String folderName="beta-pdf-folder";
        return storeTos3(s3file, fileName,folderName);
    }
    public S3Object storeTos3UserFiles(MultipartFile s3file,String userId,String fullname,String extension) {
        String fileName=fullname+"_"+userId+"."+extension;
        String folderName="beta-user-files-folder";
        return storeTos3(s3file, fileName,folderName);
    }
    
    public S3Object storeTos3(MultipartFile s3file, String fileName,String folderName) {
        String awsAccessKey = "AKIAI5GUSB5HL2LJPIVA";
        String awsSecretKey = "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";
        AmazonS3 s3client = new AmazonS3Client(new BasicAWSCredentials(awsAccessKey, awsSecretKey));
        String bucketName = "invsta.com/"+folderName;
        try {
            InputStream istream = s3file.getInputStream();
            s3client.putObject(new PutObjectRequest(bucketName, fileName, istream, new ObjectMetadata()).withCannedAcl(CannedAccessControlList.PublicRead));
            S3Object s3Object = s3client.getObject(new GetObjectRequest(bucketName, fileName));
            return s3Object;
        } catch (SdkClientException | IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }


    public File storeImage(long createdBy, String imageSource, String imageName) throws IOException {
        byte[] imageByte = Base64.decodeBase64(imageSource);
        return storeImage(createdBy, imageByte, imageName);
    }

    public File storeImage(long createdBy, MultipartFile file, String imageName) throws IOException {
        if (!file.isEmpty()) {
            byte[] imageByte = file.getBytes();
            return storeImage(createdBy, imageByte, imageName);
        }
        return null;
    }
    public File storeImage(MultipartFile file, String fileName) throws IOException {
        if (!file.isEmpty()) {
            byte[] imageByte = file.getBytes();
            return storeImage(15, imageByte, fileName);
        }
        return null;
    }
    public File storePdf(MultipartFile file, String fileName) throws IOException {
        if (!file.isEmpty()) {
            byte[] imageByte = file.getBytes();
            return storePdf(15, imageByte, fileName);
        }
        return null;
    }

    public File storeImage(long createdBy, byte[] imageByte, String imageName) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
        BufferedImage image = ImageIO.read(bis);
        bis.close();
        String dirPath = pathDirector.imagesDirectoryPath(createdBy);
        File outputfile = new File(dirPath);
        if (!outputfile.exists()) {
            outputfile.mkdirs();
        }
        outputfile = new File(dirPath + File.separator + imageName);
        int lastIndexOf = imageName.lastIndexOf(".") + 1;
        String extension = imageName.substring(lastIndexOf);
        ImageIO.write(image, extension, outputfile);
        return outputfile;
    }
    public File storePdf(long createdBy, byte[] byteArray, String fileName) throws IOException {
        String dirPath = pathDirector.pdfDirectoryPath(createdBy);
        File outputfile = new File(dirPath);
        if (!outputfile.exists()) {
            outputfile.mkdirs();
        }
        outputfile = new File(dirPath + File.separator + fileName);
//        int lastIndexOf = imageName.lastIndexOf(".") + 1;
//        String extension = imageName.substring(lastIndexOf);
        OutputStream os = new FileOutputStream(outputfile); 
         // Starts writing the bytes in it 
            os.write(byteArray); 
//        Im(image, extension, outputfile);
        return outputfile;
    }

    public String getImage(long userid, String imageName) throws IOException {
        File returnfile = new File(pathDirector.imagesDirectoryPath(userid) + File.separator + imageName);
        if (returnfile.exists()) {
            FileInputStream fis = new FileInputStream(returnfile);
            BufferedInputStream inputStream = new BufferedInputStream(fis);
            byte[] fileBytes = new byte[(int) returnfile.length()];
            inputStream.read(fileBytes);
            inputStream.close();
            String imageSource = Base64.encodeBase64String(fileBytes);
            return imageSource;
        } else {
            return null;
        }
    }

    public String getFileExists(String fileDirectoryPath, String filePath, String noFile) {
        String fileExists = "";
        fileExists = filePath.replace('/', File.separatorChar);
        String path = fileDirectoryPath + File.separator + fileExists;
        File f = new File(path);
        if (f.exists()) {
            return fileExists;
        } else {
            return noFile;
        }
    }

    public Set<String> getFileNames(long userid) {
        return getFileNames(pathDirector.imagesDirectory(userid));
    }

    public Set<String> getFileNames(File folder) {
        File[] listOfFiles = folder.listFiles();
        Set<String> fileNames = new HashSet<String>();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                fileNames.add(listOfFiles[i].getName());
            }
        }
        return fileNames;
    }
}
