/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.components;

import crm.nz.services.DateTimeServiceImpl;
import java.util.Calendar;
import java.util.Date;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Administrator
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class TimeManager {

    private String projectName = "POC-Mint";
    private Reminders reminders = null;

    public TimeManager() {
        reminders = new Reminders();
        reminders.start();
    }

    class Reminders extends Thread {

        private Date targetDate;

        public Reminders() {
            Date now = DateTimeServiceImpl.current();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.set(Calendar.HOUR, 11);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            cal.set(Calendar.MILLISECOND, 0);
            cal.set(Calendar.AM_PM, Calendar.PM);
            targetDate = cal.getTime();
        }

        @Override
        public void run() {
            while (true) {
                Date now = DateTimeServiceImpl.current();
                Calendar cal = Calendar.getInstance();
                cal.setTime(now);
                cal.set(Calendar.SECOND, 59);
                cal.set(Calendar.MILLISECOND, 0);
                now = cal.getTime();
                LoggerFactory.getLogger(TimeManager.class).debug(projectName + " Right Now:-" + now + ", Target DateTime:-" + targetDate);
                System.err.println(projectName + " Right Now:-" + now + ", Target DateTime:-" + targetDate);
                try {
                    if (now.after(targetDate)) {
                        LoggerFactory.getLogger(TimeManager.class).debug(projectName + " (In the now after targetDate)" + " Right Now:-" + now + ", Target DateTime:-" + targetDate);
                        System.err.println(projectName + " (In the now after targetDate)" + " Right Now:-" + now + ", Target DateTime:-" + targetDate);
//                        service.addNewNotifications(now);
//                        taskService.addRepeatTask(targetDate);
                        cal.setTime(targetDate);
                        cal.add(Calendar.DAY_OF_YEAR, 1);
                        targetDate = cal.getTime();
                    }
                    Thread.sleep(CommonMethods.ONE_MINUTE);
                } catch (InterruptedException ex) {
                    LoggerFactory.getLogger(TimeManager.class).debug(" Right Now:-" + now, ex);
                }
            }
        }
    }

}
