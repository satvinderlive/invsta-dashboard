/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.components;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.net.ssl.HttpsURLConnection;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Administrator
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class HTTPRequestManager {

    private final static String USER_AGENT = "Mozilla/5.0";
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(HTTPRequestManager.class);

    public String sendHTTPGetRequest(String urlPath) {
        return sendHTTPGetRequest(urlPath, null);
    }

    public String sendHTTPGetRequest(String urlPath, Map<String, String> params) {
        return sendHTTPGetRequest(urlPath, params, null);
    }

    public String sendHTTPGetRequest(String urlPath, Map<String, String> params, Map<String, String> headers) {
        try {
            String urlParameters = "";
            if (params != null && !params.isEmpty()) {
                Set<Map.Entry<String, String>> paramSet = params.entrySet();
                int i = 0;
                for (Map.Entry<String, String> entry : paramSet) {
                    urlParameters += entry.getKey() + "=" + entry.getValue() + (i < paramSet.size() - 1 ? "&" : "");
                }
            }
            if (!urlParameters.isEmpty()) {
                urlPath = urlPath + "?" + urlParameters;
            }
            URL url = new URL(urlPath);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);
            if (headers != null && !headers.isEmpty()) {
                Set<Map.Entry<String, String>> headerSet = headers.entrySet();
                for (Map.Entry<String, String> header : headerSet) {
                    con.setRequestProperty(header.getKey(), header.getValue());
                }
            }
            int responseCode = con.getResponseCode();
            logger.debug("\nSending 'GET' request to URL : " + urlPath);
            logger.debug("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            String responseBody = response.toString();
            return responseBody;
        } catch (IOException ex) {
            logger.debug("", ex);
        }
        return null;
    }

    // HTTP POST request
    public String sendHTTPPostRequest(String urlPath, String body) {
        return sendHTTPPostRequest(urlPath, body, null);
    }

    // HTTP POST request
    public String sendHTTPPostRequest(String urlPath, String body, Map<String, String> headers) {
        try {
            URL url = new URL(urlPath);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");
            if (body != null) {
                con.setRequestProperty("Content-Length", Integer.toString(body.length()));
            }
            if (headers != null && !headers.isEmpty()) {
                Set<Map.Entry<String, String>> headerSet = headers.entrySet();
                for (Map.Entry<String, String> header : headerSet) {
                    con.setRequestProperty(header.getKey(), header.getValue());
                }
            }
            if (body != null) {
                con.getOutputStream().write(body.getBytes("UTF8"));
            }
            try (DataOutputStream dataOutputStream = new DataOutputStream(con.getOutputStream())) {
                dataOutputStream.flush();
            }
            int responseCode = con.getResponseCode();
            logger.debug("\nSending 'POST' request to URL : " + urlPath);
            logger.debug("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        } catch (IOException ex) {
            logger.debug("", ex);
            return null;
        }
    }

    public String sendHTTPPostRequest(String urlPath, String body, Map<String, String> headers, Map<String, String> params) {
        try {
            String urlParameters = "";
            if (params != null && !params.isEmpty()) {
                Set<Map.Entry<String, String>> paramSet = params.entrySet();
                int i = 0;
                for (Map.Entry<String, String> entry : paramSet) {
                    urlParameters += entry.getKey() + "=" + entry.getValue() + (i < paramSet.size() - 1 ? "&" : "");
                }
            }
            if (!urlParameters.isEmpty()) {
                urlPath = urlPath + "?" + urlParameters;
            }
            URL url = new URL(urlPath);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");
            if (body != null) {
                con.setRequestProperty("Content-Length", Integer.toString(body.length()));
            }
            if (headers != null && !headers.isEmpty()) {
                Set<Map.Entry<String, String>> headerSet = headers.entrySet();
                for (Map.Entry<String, String> header : headerSet) {
                    con.setRequestProperty(header.getKey(), header.getValue());
                }
            }
            if (body != null) {
                con.getOutputStream().write(body.getBytes("UTF8"));
            }
            try (DataOutputStream dataOutputStream = new DataOutputStream(con.getOutputStream())) {
                dataOutputStream.flush();
            }
            int responseCode = con.getResponseCode();
            logger.debug("\nSending 'POST' request to URL : " + urlPath);
            logger.debug("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        } catch (IOException ex) {
            logger.debug("", ex);
            return null;
        }
    }

    public String sendHTTPPostRequest(String urlPath, Map<String, File> files, Map<String, String> headers) {
        try {
            MultipartRequest request = new MultipartRequest(urlPath, "");
            if (headers != null) {
                Set<Map.Entry<String, String>> entrySet = headers.entrySet();
                for (Iterator<Map.Entry<String, String>> iterator = entrySet.iterator(); iterator.hasNext();) {
                    Map.Entry<String, String> next = iterator.next();
                    request.addHeaderField(next.getKey(), next.getValue());
                }
            }
            if (files != null) {
                Set<Map.Entry<String, File>> entrySet = files.entrySet();
                for (Iterator<Map.Entry<String, File>> iterator = entrySet.iterator(); iterator.hasNext();) {
                    Map.Entry<String, File> next = iterator.next();
                    request.addFilePart(next.getKey(), next.getValue());
                }
            }
            String response = request.finish();
            return response;
        } catch (Exception ex) {
            return null;
        }
    }

    public String postHttp(String url, List<NameValuePair> params, List<NameValuePair> headers) throws IOException {
        HttpPost post = new HttpPost(url);
        post.setEntity(new UrlEncodedFormEntity(params, Consts.UTF_8));
        post.getEntity().toString();
        if (headers != null) {
            for (NameValuePair header : headers) {
                post.addHeader(header.getName(), header.getValue());
            }
        }
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpResponse response = httpClient.execute(post);
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            return EntityUtils.toString(entity);
        }
        return null;
    }

    public String postHttp(String url) throws IOException {

        return null;
    }

    public String getHttp(String url, List<NameValuePair> headers) throws IOException {
        HttpRequestBase request = new HttpGet(url);
        if (headers != null) {
            for (NameValuePair header : headers) {
                request.addHeader(header.getName(), header.getValue());
            }
        }
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpResponse response = httpClient.execute(request);
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            return EntityUtils.toString(entity);
        }
        return null;
    }

    class MultipartRequest {

        private static final String LINE_FEED = "\r\n";
        private final HttpURLConnection httpConn;
        private final OutputStream outputStream;
        private final PrintWriter writer;
        private final String boundary;
        private final String charset;

        public MultipartRequest(String requestURL, String charset)
                throws IOException {
            this.charset = charset;
            this.boundary = "===" + System.currentTimeMillis() + "===";
            URL url = new URL(requestURL);
            this.httpConn = (HttpURLConnection) url.openConnection();
            httpConn.setUseCaches(false);
            httpConn.setDoOutput(true); // indicates POST method
            httpConn.setDoInput(true);
            httpConn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            httpConn.setRequestProperty("User-Agent", USER_AGENT);
            this.outputStream = httpConn.getOutputStream();
            this.writer = new PrintWriter(new OutputStreamWriter(outputStream), true);
        }

        /**
         * Adds a form field to the request
         *
         * @param name field name
         * @param value field value
         */
        public void addFormField(String name, String value) {
            writer.append("--" + boundary)
                    .append(LINE_FEED)
                    .append("Content-Disposition:form-data;")
                    .append("name=\"" + name + "\";")
                    .append(LINE_FEED)
                    .append("Content-Type: text/plain; charset=" + charset)
                    .append(LINE_FEED)
                    .append(LINE_FEED)
                    .append(value)
                    .append(LINE_FEED);
            writer.flush();
        }

        /**
         * Adds a upload file section to the request
         *
         * @param fieldName name attribute in <input type="file" name="..." />
         * @param uploadFile a File to be uploaded
         * @throws IOException
         */
        public void addFilePart(String fieldName, File uploadFile)
                throws IOException {
            String fileName = uploadFile.getName();
            writer.append("--" + boundary)
                    .append(LINE_FEED)
                    .append("Content-Disposition: form-data;")
                    .append("name=\"" + fieldName + "\";")
                    .append("filename=\"" + fileName + "\"")
                    .append(LINE_FEED)
                    .append("Content-Type: " + URLConnection.guessContentTypeFromName(fileName))
                    .append(LINE_FEED)
                    .append("Content-Transfer-Encoding: binary")
                    .append(LINE_FEED)
                    .append(LINE_FEED);
            writer.flush();
            FileInputStream inputStream = new FileInputStream(uploadFile);
            byte[] buffer = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            outputStream.flush();
            inputStream.close();
            writer.append(LINE_FEED);
            writer.flush();
        }

        /**
         * Adds a header field to the request.
         *
         * @param name - name of the header field
         * @param value - value of the header field
         */
        public void addHeaderField(String name, String value) {
            writer.append(name + ": " + value).append(LINE_FEED);
            writer.flush();
        }

        /**
         * Completes the request and receives response from the server.
         *
         * @return a list of Strings as response in case the server returned
         * status OK, otherwise an exception is thrown.
         * @throws IOException
         */
        public String finish() throws IOException {
            StringBuffer response = new StringBuffer();
            writer.append(LINE_FEED).flush();
            writer.append("--" + boundary + "--").append(LINE_FEED);
            writer.close();
            int responseCode = httpConn.getResponseCode();
            logger.debug("Response Code->" + responseCode);
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();
            httpConn.disconnect();
            logger.debug("Response->" + response);
            return response.toString();
        }
    }
}
