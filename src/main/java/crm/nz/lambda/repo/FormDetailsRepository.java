package crm.nz.lambda.repo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import crm.nz.beans.acc.api.Advisor;
import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.beans.acc.api.CountryTINBean;
import crm.nz.beans.acc.api.JointDetailBean;
import crm.nz.beans.acc.api.PersonDetailsBean;
import crm.nz.beans.acc.api.Register;
import crm.nz.components.ObjectCastManager;
import crm.nz.beans.acc.api.Occupation;
import crm.nz.components.CommonMethods;
//import static crm.nz.security.CurrentUserService.encoder;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author innovative002
 */
@Repository
public class FormDetailsRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    BCryptPasswordEncoder encoder;
    @Autowired
    CommonMethods commonMethods;

    public List<Register> getPendingRegisterDetails() {
        String sql = " select A.* from (\n"
                + "select R.*,fullName name, date_of_Birth dob from register R join individual_person_detail IPD on(R.id = IPD.reg_id)\n"
                + "union all\n"
                + "select R.*,fullName name, date_of_Birth dob from register R join joint_account_details JAD on(R.id = JAD.reg_id)\n"
                + "union all\n"
                + "select R.*,fname name,dateOfBirth dob from register R join company_trust_details CD on(R.id = CD.reg_id)) A\n"
                + "where status IN( 'PENDING', 'SUBMISSION') and approved = 'N'\n"
                + "group by A.username\n"
                + "order by created_ts desc; ";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper(Register.class));
    }

    public List<Register> getPendingRegisterDetails(String AdvisorId) {
        String sql = "  select A.* from (\n"
                + " select R.*,fullName name, date_of_Birth dob from register R "
                + " join individual_person_detail IPD on(R.id = IPD.reg_id) where advisor =" + AdvisorId
                + " union all\n"
                + " select R.*,fullName name, date_of_Birth dob from register R "
                + " join joint_account_details JAD on(R.id = JAD.reg_id) where advisor =" + AdvisorId
                + " ) A\n"
                + " where status IN( 'PENDING', 'SUBMISSION') and approved = 'N'\n"
                + " group by A.username\n"
                + " order by created_ts desc; ";
        System.out.println(sql);
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper(Register.class));
    }

    public Register getRegisterDetailsByToken(String token) {
        String sql = "select * from `register` where `token`='" + token + "';";
        List<Register> register = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Register.class));
        return !register.isEmpty() ? register.get(register.size() - 1) : null;
    }

    public Register getRegisterDetailsByEmail(String reg_id) {
        String sql = "select * from `register` where `id`='" + reg_id + "';";
        List<Register> register = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Register.class));
        return !register.isEmpty() ? register.get(register.size() - 1) : null;
    }

    public List<Occupation> getOccupation() {
        String sql = "SELECT * FROM occupation order by mmc_occu_name ;";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper(Occupation.class));
    }

    public List<Advisor> getAdvisories() {
        String sql = "SELECT A.*, lm.id as login_id FROM advisors A \n"
                + " inner join login_master lm on (lm.username = A.username AND lm.role = 'ADVISOR') ;";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper(Advisor.class));
    }

    public PersonDetailsBean getPersonDetailByToken(String token) {
        String sql = " select IPD.* , R.* from `individual_person_detail` IPD  \n"
                + " inner join  `register` R ON (IPD.reg_id = R.id and R.reg_type IN ( 'INDIVIDUAL_ACCOUNT' ,'MINOR_ACCOUNT')) \n"
                + " where R.token = '" + token + "'";
        List<PersonDetailsBean> personDetail = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PersonDetailsBean.class));
        PersonDetailsBean bean = !personDetail.isEmpty() ? personDetail.get(personDetail.size() - 1) : null;
        if (bean != null) {
            String tinSQl = " select ITC.id, ITC.country, ITC.tin, ITC.reason, IPD.reg_id\n"
                    + " from CountryTIN ITC inner join individual_person_detail IPD on (ITC.reg_id = IPD.reg_id)\n"
                    + " where IPD.reg_id = " + bean.getReg_id();
            List<CountryTINBean> tinBeanList = jdbcTemplate.query(tinSQl, new BeanPropertyRowMapper(CountryTINBean.class));
            bean.setCountryTINList(tinBeanList);
        }
        return bean;
    }

    public CompanyDetailBean getCompanyDetailByToken(String token) {
        String sql = " select CD.* , R.username email, R.password, R.token, R.status, R.step, R.created_ts, R.reg_type,R.raw_password from `company_trust_details` CD  \n"
                + "  inner join  `register` R ON (CD.reg_id = R.id and R.reg_type IN ('COMPANY_ACCOUNT', 'TRUST_ACCOUNT')) \n"
                + "  where  CD.active = 'Y' and  R.token = '" + token + "'";
        List<CompanyDetailBean> companyDetail = jdbcTemplate.query(sql, new BeanPropertyRowMapper(CompanyDetailBean.class));
        List<CompanyDetailBean> moreInvestorList = new LinkedList<>();
        if (!companyDetail.isEmpty()) {
            for (int i = 0; i < companyDetail.size(); i++) {
                if (i != 0) {
                    CompanyDetailBean moreInvestor = companyDetail.get(i);
                    moreInvestorList.add(moreInvestor);
                }
            }
            CompanyDetailBean firstBean = companyDetail.get(0);
            firstBean.setMoreInvestorList(moreInvestorList);
            return firstBean;
        }
        return null;
    }

    public JointDetailBean getJointDetailByToken(String token) {
        String sql = " select JAD.*, R.username, R.password, R.token, R.status, R.step, R.created_ts, R.reg_type,R.raw_password from `joint_account_details` JAD\n"
                + "inner join  `register` R ON (JAD.reg_id = R.id and R.reg_type = 'JOINT_ACCOUNT')\n"
                + "where JAD.active='Y' and  R.token = '" + token + "'";
        List<JointDetailBean> jointDetailBeanList = jdbcTemplate.query(sql, new BeanPropertyRowMapper(JointDetailBean.class));
        List<JointDetailBean> moreInvestorList = new LinkedList<>();
        if (!jointDetailBeanList.isEmpty()) {
            for (int i = 0; i < jointDetailBeanList.size(); i++) {
                JointDetailBean bean = jointDetailBeanList.get(i);
                String tinSQl = "select * from CountryTIN where active='Y' and investor_id = " + bean.getId();
                List<CountryTINBean> tinBeanList = jdbcTemplate.query(tinSQl, new BeanPropertyRowMapper(CountryTINBean.class));
                bean.setCountryTINList(tinBeanList);
            }
            for (int i = 0; i < jointDetailBeanList.size(); i++) {
                if (i != 0) {
                    JointDetailBean moreInvestor = jointDetailBeanList.get(i);
                    moreInvestorList.add(moreInvestor);
                }
            }
            JointDetailBean firstBean = jointDetailBeanList.get(0);
            firstBean.setMoreInvestorList(moreInvestorList);
            return firstBean;
        }
        return null;
    }

    public void saveFormDetails(PersonDetailsBean bean, HashMap response) {
        try {
            Register register = getRegisterDetailsByEmail(bean.getReg_id());

            if (register != null) {
                Connection con = jdbcTemplate.getDataSource().getConnection();
//                  bean.setEmail(register.getUsername());
//                  bean.setPassword(register.getPassword());
                try {
                    if ("SUBMISSION".equalsIgnoreCase(bean.getStatus())) {
                        CreateIndividualLogin(con, bean, response);
                    }
                    UpdateFormDetails(con, bean, response);
                } finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(FormDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            } else {
                Connection con = jdbcTemplate.getDataSource().getConnection();
                try {
                    if ("SUBMISSION".equalsIgnoreCase(bean.getStatus())) {
                        CreateIndividualLogin(con, bean, response);
                    }
                    insertFormDetails(con, bean, response);
                } finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(FormDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
            response.put("message", "You have Submited info Successfully. Now you have received Notification.");
            response.put("status", 200);
        } catch (SQLException ex) {
            Logger.getLogger(FormDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void saveCompanyDetails(CompanyDetailBean bean, HashMap response) {
        try {
            Register register = getRegisterDetailsByEmail(bean.getReg_id());
            if (register != null) {
//                bean.setEmail(register.getUsername());
//                bean.setPassword(register.getPassword());
                Connection con = jdbcTemplate.getDataSource().getConnection();
                try {
                    UpdateCompanyDetails(con, bean, response);
                } finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(FormDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            } else {
                Connection con = jdbcTemplate.getDataSource().getConnection();
                try {
                    insertCompanyDetails(con, bean, response);
                } finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(FormDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
            response.put(
                    "message", "You have Submited info Successfully. Now you have received Notification.");
            response.put(
                    "status", 200);
        } catch (SQLException ex) {
            Logger.getLogger(FormDetailsRepository.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void saveJointDetails(JointDetailBean bean, HashMap response) {
        try {
            Register register = getRegisterDetailsByEmail(bean.getReg_id());
            if (register != null) {
                Connection con = jdbcTemplate.getDataSource().getConnection();
                try {
                    bean.setEmail(register.getUsername());
                    bean.setPassword(register.getPassword());
                    if ("SUBMISSION".equalsIgnoreCase(bean.getStatus())) {
                        response.put("update", "yes");
                        CreateJointLogin(con, bean, response);
                    }
                    UpdateJointDetails(con, bean, response);
                } finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(FormDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            } else {
                Connection con = jdbcTemplate.getDataSource().getConnection();
                try {
                    if ("SUBMISSION".equalsIgnoreCase(bean.getStatus())) {
                        CreateJointLogin(con, bean, response);
                    }
                    insertJointDetails(con, bean, response);
                } finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(FormDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
            response.put("message", "You have Submited info Successfully. Now you have received Notification.");
            response.put("status", 200);

        } catch (SQLException ex) {
            Logger.getLogger(FormDetailsRepository.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void saveSingleJointDetails(JointDetailBean bean, HashMap response) throws SQLException {

//                Connection con = jdbcTemplate.getDataSource().getConnection();
        String innerSql = "UPDATE `joint_account_details`\n"
                + "  SET `ird_Number` = ?,`title` = ?,`passport_expiry` = ?,`advisor` = ?,`optional_num_type` = ?,\n"
                + " `tin` = ?,`licence_expiry_Date` = ?,`optional_num_code` = ?,`acount_holder_name` = ?,\n"
                + " `passport_number` = ?,`passport_issue_by` = ?,`reg_type` = ?,`pir` = ?,`working_with_adviser` = ?,\n"
                + " `status` = ?,`optional_num` = ?,`advisor_company` = ?,`tex_residence_Country` = ?,\n"
                + " `bank_name` = ?,`license_number` = ?,`other_id_expiry` = ?,`licence_verson_number` = ?,`step` = ?,\n"
                + " `mobile_country_code` = ?,`id_type` = ?,`mobile_number` = ?,`fullName` = ?,\n"
                + " `other_id_issueBy` = ?,`isUSCitizen` = ?,`resn_tin_unavailable` = ?,`other_id_type` = ?,\n"
                + " `date_of_Birth` = ?,`account_number` = ?,`homeAddress` = ?,`occupation` = ?,\n"
                + " `created_ts` = ?, `created_by` = ?, `preferred_name` = ?,`active`='Y', `curr_idx` = ?,\n"
                + " `investor_idverified`= ? , `country_residence`= ?  WHERE  `token` = '" + bean.getToken() + "';";
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement ps = con.prepareStatement(innerSql);
            ps.setString(1, bean.getIrd_Number());
            ps.setString(2, bean.getTitle());
            ps.setString(3, bean.getPassport_expiry());
            ps.setString(4, bean.getAdvisor());
            ps.setString(5, bean.getOptional_num_type());
            ps.setString(6, "");
            ps.setString(7, bean.getLicence_expiry_Date());
            ps.setString(8, bean.getOptional_num_code());
            ps.setString(9, bean.getAcount_holder_name());
            ps.setString(10, bean.getPassport_number());
            ps.setString(11, bean.getPassport_issue_by());
            ps.setString(12, bean.getReg_type());
            ps.setString(13, bean.getPir());
            ps.setString(14, bean.getWorking_with_adviser());
            ps.setString(15, bean.getStatus());
            ps.setString(16, bean.getOptional_num());
            ps.setString(17, bean.getAdvisor_company());
            ps.setString(18, "");
            ps.setString(19, bean.getBank_name());
            ps.setString(20, bean.getLicense_number());
            ps.setString(21, bean.getOther_id_expiry());
            ps.setString(22, bean.getLicence_verson_number());
            ps.setString(23, bean.getStep());
            ps.setString(24, bean.getMobile_country_code());
            ps.setString(25, bean.getId_type());
            ps.setString(26, bean.getMobile_number());
            ps.setString(27, bean.getFullName());
            ps.setString(28, bean.getOther_id_issueBy());
            ps.setString(29, bean.getIsUSCitizen());
            ps.setString(30, bean.getResn_tin_unavailable());
            ps.setString(31, bean.getOther_id_type());
            ps.setString(32, bean.getDate_of_Birth());
            ps.setString(33, bean.getAccount_number());
            ps.setString(34, bean.getHomeAddress());
            ps.setString(35, bean.getOccupation());
            ps.setDate(36, new Date(new java.util.Date().getTime()));
            ps.setString(37, bean.getCreated_by());
            ps.setString(38, bean.getPreferred_name());
            ps.setInt(39, 0);
            ps.setString(40, bean.getInvestor_idverified());
            ps.setString(41, bean.getCountry_residence());
            ps.executeUpdate();
            return ps;
        });

        response.put("message", "You have Submited info Successfully. Now you have received Notification.");
        response.put("status", 200);

    }

    public void insertJointDetails(Connection con, JointDetailBean bean, HashMap response) {
        try {
            String regSql = " INSERT INTO `register`\n"
                    + "(`username`,\n"
                    + "`password`,\n"
                    + "`token`,\n"
                    + "`status`,\n"
                    + "`created_ts`,\n"
                    + "`step`,\n"
                    + "`reg_type`,"
                    + "`raw_password`)\n"
                    + "VALUES\n"
                    + "(?, ?, ?, ?, ?, ?, ?, ?); ";
            PreparedStatement ps0 = con.prepareStatement(regSql, Statement.RETURN_GENERATED_KEYS);
            ps0.setString(1, bean.getEmail());
            String encryptedPassword = encoder.encode(bean.getPassword());
            ps0.setString(2, encryptedPassword);
            ps0.setString(3, bean.getToken());
            ps0.setString(4, bean.getStatus());
            ps0.setDate(5, new Date(new java.util.Date().getTime()));
            ps0.setString(6, bean.getStep());
            ps0.setString(7, bean.getReg_type());
            ps0.setString(8, bean.getPassword());
            ps0.executeUpdate();
            System.out.println("ps0 completed");
            ResultSet generatedKeys = ps0.getGeneratedKeys();
            if (generatedKeys.next()) {
                System.out.println("generatedKeys.next() start");
                String reg_id = generatedKeys.getString(1);
                List<JointDetailBean> investorList = new LinkedList<>();
                investorList.add(bean);
                if (bean.getMoreInvestorList() != null && !bean.getMoreInvestorList().isEmpty()) {
                    investorList.addAll(bean.getMoreInvestorList());
                }
                if (!investorList.isEmpty()) {
                    String investorSQL = "INSERT INTO `joint_account_details`\n"
                            + " (`IRD_Number`,`Title`,`passport_expiry`,`advisor`,`optional_num_type`,\n"
                            + " `TIN`,`licence_expiry_Date`,`optional_num_code`,`acount_holder_name`,\n"
                            + " `passport_number`,`passport_issue_by`,`reg_type`,`PIR`,`working_with_adviser`,\n"
                            + " `email`,`status`,`optional_num`,`advisor_company`,`tex_residence_Country`,\n"
                            + " `bank_name`,`License_number`,`other_id_expiry`,`licence_verson_number`,`step`,\n"
                            + " `mobile_country_code`,`id_type`,`mobile_number`,`fullName`,`password`,\n"
                            + " `other_id_issueBy`,`isUSCitizen`,`resn_tin_unavailable`,`other_id_type`,\n"
                            + " `Date_of_Birth`,`account_number`,`homeAddress`,`Occupation`,`created_ts`,`created_by`,`reg_id`,`token`,`investor_idverified`,`country_residence`)\n"
                            + "  VALUES  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";
                    PreparedStatement ps1 = con.prepareStatement(investorSQL, Statement.RETURN_GENERATED_KEYS);
                    for (JointDetailBean investor : investorList) {
                        ps1.setString(1, investor.getIrd_Number());
                        ps1.setString(2, investor.getTitle());
                        ps1.setString(3, investor.getPassport_expiry());
                        ps1.setString(4, investor.getAdvisor());
                        ps1.setString(5, investor.getOptional_num_type());
                        ps1.setString(6, null);
                        ps1.setString(7, investor.getLicence_expiry_Date());
                        ps1.setString(8, investor.getOptional_num_code());
                        ps1.setString(9, investor.getAcount_holder_name());
                        ps1.setString(10, investor.getPassport_number());
                        ps1.setString(11, investor.getPassport_issue_by());
                        ps1.setString(12, investor.getReg_type());
                        ps1.setString(13, investor.getPir());
                        ps1.setString(14, investor.getWorking_with_adviser());
                        ps1.setString(15, investor.getEmail());
                        ps1.setString(16, investor.getStatus());
                        ps1.setString(17, investor.getOptional_num());
                        ps1.setString(18, investor.getAdvisor_company());
                        ps1.setString(19, null);
                        ps1.setString(20, investor.getBank_name());
                        ps1.setString(21, investor.getLicense_number());
                        ps1.setString(22, investor.getOther_id_expiry());
                        ps1.setString(23, investor.getLicence_verson_number());
                        ps1.setString(24, investor.getStep());
                        ps1.setString(25, investor.getMobile_country_code());
                        ps1.setString(26, investor.getId_type());
                        ps1.setString(27, investor.getMobile_number());
                        ps1.setString(28, investor.getFullName());
                        ps1.setString(29, investor.getPassword());
                        ps1.setString(30, investor.getOther_id_issueBy());
                        ps1.setString(31, investor.getIsUSCitizen());
                        ps1.setString(32, investor.getResn_tin_unavailable());
                        ps1.setString(33, investor.getOther_id_type());
                        ps1.setString(34, investor.getDate_of_Birth());
                        ps1.setString(35, investor.getAccount_number());
                        ps1.setString(36, investor.getHomeAddress());
                        ps1.setString(37, investor.getOccupation());
                        ps1.setDate(38, new Date(new java.util.Date().getTime()));
                        ps1.setString(39, investor.getCreated_by());
                        ps1.setString(40, reg_id);
                        ps1.setString(41, investor.getToken());
                        ps1.setString(42, investor.getInvestor_idverified());
                        ps1.setString(43, investor.getCountry_residence());
                        ps1.addBatch();
                    }
                    System.out.println(" IN Execute Batch For Item Charge");
                    ps1.executeBatch();
                    ResultSet rs = ps1.getGeneratedKeys();
                    List<CountryTINBean> countryTINList = new LinkedList<>();
                    Integer i = 0;
                    while (rs.next()) {
                        JointDetailBean investor = investorList.get(i);
                        String investor_id = rs.getString(1);
                        investor.setId(investor_id);
                        investor.getCountryTINList().forEach((tinbean) -> {
                            tinbean.setInvestor_id(investor_id);
                        });
                        countryTINList.addAll(investor.getCountryTINList());
                        i++;
                    }
                    if (!countryTINList.isEmpty()) {
                        String tinsql = "INSERT INTO `CountryTIN`\n"
                                + "(`country`,\n"
                                + "`tin`,\n"
                                + "`reason`,\n"
                                + "`reg_id`,\n"
                                + "`investor_id`,\n"
                                + "`active`)\n"
                                + "VALUES\n"
                                + "(?, ?, ?, ?, ?, 'Y')";
                        PreparedStatement ps2 = con.prepareStatement(tinsql);
                        for (CountryTINBean tinbean : countryTINList) {
                            ps2.setString(1, tinbean.getCountry());
                            ps2.setString(2, tinbean.getTin());
                            ps2.setString(3, tinbean.getReason());
                            ps2.setString(4, reg_id);
                            ps2.setString(5, tinbean.getInvestor_id());
                            ps2.addBatch();
                        }
                        ps2.executeBatch();
                    }
                    System.out.println("batch completed");
                }
            }
            con.close();
            response.put("message", "You have added a new Joint Account Detail.");
            response.put("status", 200);
        } catch (SQLException ex) {
            ex.printStackTrace();
//            response.put("opportunityList", opportunityList);
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
//            Logger.getLogger(OpportunityRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void insertCompanyDetails(Connection con, CompanyDetailBean bean, HashMap response) {
        try {
            String innerSql = "INSERT INTO `company_trust_details`\n"
                    + " (`email`,`password`,`mobileNo`,`countryOfResidence`,`countryCode`,`bankName`,`countryOfIssue`,`countryOfTaxResidence`,`companyReasonTIN`,\n"
                    + " `myFile`,`passportExpiryDate`,`pir`,`occupation`,`registerNumber`,`dateOfBirth`,`typeOfID`,`companyIRDNumber`,`detail`,`holderCountryOfResidence`,\n"
                    + " `dateOfInco`,`fname`,`taxIdentityNumber`,`postalAddress`,`isCompanyFinancialInstitution`,`isCompanyGovernment`,`status`,`investAdviser`,\n"
                    + " `companyName`,`typeCountryOfIssue`,`sourceOfFunds`,`otherCountryCode`,`isCompanyActivePassive`,`versionNumber`,`selectAdviser`,`licenseExpiryDate`,\n"
                    + " `address`,`countryCode3`,`srcOfFund`,`companyCountryOfTaxResidence`,`positionInCompany`,`irdNumber`,`companyAddress`,`accountNumber`,`licenseNumber`,\n"
                    + " `isCompanyListed`,`nameOfAccount`,`companyTaxIdentityNumber`,`isCompanyNZPolice`,`passportNumber`,`isCompanyUSCitizen`,`step`,`otherMobileNo`,\n"
                    + " `companyAdvisor`,`otherNumber`,`citizenUS`,`token`,`created_ts`,`isInvestor_idverified`,`created_by`)\n"
                    + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";

            PreparedStatement preparedStatement = con.prepareStatement(innerSql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, bean.getEmail());
            String encryptedPassword = encoder.encode(bean.getRaw_password());
            preparedStatement.setString(2, encryptedPassword);
            preparedStatement.setString(3, bean.getMobileNo());
            preparedStatement.setString(4, bean.getCountryOfResidence());
            preparedStatement.setString(5, bean.getCountryCode());
            preparedStatement.setString(6, bean.getBankName());
            preparedStatement.setString(7, bean.getCountryOfIssue());
            preparedStatement.setString(8, bean.getCountryOfTaxResidence());
            preparedStatement.setString(9, bean.getCompanyReasonTIN());
            preparedStatement.setString(10, bean.getMyFile());
            preparedStatement.setString(11, bean.getPassportExpiryDate());
            preparedStatement.setString(12, bean.getPir());
            preparedStatement.setString(13, bean.getOccupation());
            preparedStatement.setString(14, bean.getRegisterNumber());
            preparedStatement.setString(15, bean.getDateOfBirth());
            preparedStatement.setString(16, bean.getTypeOfID());
            preparedStatement.setString(17, bean.getCompanyIRDNumber());
            preparedStatement.setString(18, bean.getDetail());
            preparedStatement.setString(19, bean.getHolderCountryOfResidence());
            preparedStatement.setString(20, bean.getDateOfInco());
            preparedStatement.setString(21, bean.getFname());
            preparedStatement.setString(22, bean.getTaxIdentityNumber());
            preparedStatement.setString(23, bean.getPostalAddress());
            preparedStatement.setString(24, bean.getIsCompanyFinancialInstitution());
            preparedStatement.setString(25, bean.getIsCompanyGovernment());
            preparedStatement.setString(26, bean.getStatus());
            preparedStatement.setString(27, bean.getInvestAdviser());
            preparedStatement.setString(28, bean.getCompanyName());
            preparedStatement.setString(29, bean.getTypeCountryOfIssue());
            preparedStatement.setString(30, bean.getSourceOfFunds());
            preparedStatement.setString(31, bean.getOtherCountryCode());
            preparedStatement.setString(32, bean.getIsCompanyActivePassive());
            preparedStatement.setString(33, bean.getVersionNumber());
            preparedStatement.setString(34, bean.getSelectAdviser());
            preparedStatement.setString(35, bean.getLicenseExpiryDate());
            preparedStatement.setString(36, bean.getAddress());
            preparedStatement.setString(37, bean.getCountryCode3());
            preparedStatement.setString(38, bean.getSrcOfFund());
            preparedStatement.setString(39, bean.getCompanyCountryOfTaxResidence());
            preparedStatement.setString(40, bean.getPositionInCompany());
            preparedStatement.setString(41, bean.getIrdNumber());
            preparedStatement.setString(42, bean.getCompanyAddress());
            preparedStatement.setString(43, bean.getAccountNumber());
            preparedStatement.setString(44, bean.getLicenseNumber());
            preparedStatement.setString(45, bean.getIsCompanyListed());
            preparedStatement.setString(46, bean.getNameOfAccount());
            preparedStatement.setString(47, bean.getCompanyTaxIdentityNumber());
            preparedStatement.setString(48, bean.getIsCompanyNZPolice());
            preparedStatement.setString(49, bean.getPassportNumber());
            preparedStatement.setString(50, bean.getIsCompanyUSCitizen());
            preparedStatement.setString(51, bean.getStep());
            preparedStatement.setString(52, bean.getOtherMobileNo());
            preparedStatement.setString(53, bean.getCompanyAdvisor());
            preparedStatement.setString(54, bean.getOtherNumber());
            preparedStatement.setString(55, bean.getCitizenUS());
            preparedStatement.setString(56, bean.getTokan());
            preparedStatement.setString(57, bean.getCrearted_ts());
            preparedStatement.setString(58, bean.getIsInvestor_idverified());
            preparedStatement.setString(59, bean.getCreated_by());
//            preparedStatement.setString(59, bean.getHolder_email());
            int i = preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
//                ActivityRepository.getInstance().newActivity(con, new ActivityBean(bean.getCreated_by(), bean.getCreated_ts(), "opportunity", generatedKeys.getString(1)), response);
            }
            generatedKeys.close();
            preparedStatement.close();
            String fetchSql = " INSERT INTO `register`\n"
                    + "(`username`,\n"
                    + "`password`,\n"
                    + "`token`,\n"
                    + "`status`,\n"
                    + "`created_ts`,\n"
                    + "`step`,\n"
                    + "`reg_type`,\n"
                    + "`raw_password`"
                    + ")\n"
                    + "VALUES\n"
                    + "(?, ?, ?, ?, ?, ?, ?, ?); ";
            PreparedStatement preparedStatement2 = con.prepareStatement(fetchSql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement2.setString(1, bean.getEmail());
            String encryptedPassword1 = encoder.encode(bean.getRaw_password());
            preparedStatement2.setString(2, encryptedPassword1);
            preparedStatement2.setString(3, bean.getTokan());
            preparedStatement2.setString(4, bean.getStatus());
            preparedStatement2.setDate(5, new Date(new java.util.Date().getTime()));
            preparedStatement2.setString(6, bean.getStep());
            preparedStatement2.setString(7, bean.getReg_type());
            preparedStatement2.setString(8, bean.getPassword());
            int x = preparedStatement2.executeUpdate();
            response.put("message", "You have added a new Conmpany Detail.");
            response.put("status", 200);
        } catch (SQLException ex) {
//            response.put("opportunityList", opportunityList);
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
//            Logger.getLogger(OpportunityRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FormDetailsRepository.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void insertFormDetails(Connection con, PersonDetailsBean bean, HashMap response) {
        try {
            String reg_id;
            if (!"MINOR_ACCOUNT".equals(bean.getReg_type())) {
                String fetchSql = " INSERT INTO `register`\n"
                        + "(`username`,\n"
                        + "`password`,\n"
                        + "`token`,\n"
                        + "`status`,\n"
                        + "`created_ts`,\n"
                        + "`step`,\n"
                        + "`reg_type`,\n"
                        + "`raw_password`"
                        + ")\n"
                        + "VALUES\n"
                        + "(?, ?, ?, ?, ?, ?, ?, ?); ";
                PreparedStatement ps0 = con.prepareStatement(fetchSql, Statement.RETURN_GENERATED_KEYS);
                ps0.setString(1, bean.getEmail());
                String encryptedPassword1 = encoder.encode(bean.getPassword());
                ps0.setString(2, encryptedPassword1);
                ps0.setString(3, bean.getTokan());
                ps0.setString(4, bean.getStatus());
                ps0.setDate(5, new Date(new java.util.Date().getTime()));
                ps0.setString(6, bean.getStep());
                ps0.setString(7, bean.getReg_type());
                ps0.setString(8, bean.getPassword());
                ps0.executeUpdate();
                ResultSet generatedKeys0 = ps0.getGeneratedKeys();
                if (generatedKeys0.next()) {
                    reg_id = generatedKeys0.getString(1);
                    bean.setReg_id(reg_id);
                }
            }
            String innerSql = " INSERT INTO `individual_person_detail`\n"
                    + "(`email`,\n"
                    + "`password`,\n"
                    + "`title`,\n"
                    + "`fullName`,\n"
                    + "`date_of_Birth`,\n"
                    + "`country_residence`,\n"
                    + "`occupation`,\n"
                    + "`working_with_adviser`,\n"
                    + "`advisor_company`,\n"
                    + "`advisor`,\n"
                    + "`homeAddress`,\n"
                    + "`mobile_country_code`,\n"
                    + "`mobile_number`,\n"
                    + "`optional_num_type`,\n"
                    + "`optional_num_code`,\n"
                    + "`id_type`,\n"
                    + "`license_number`,\n"
                    + "`licence_expiry_Date`,\n"
                    + "`licence_verson_number`,\n"
                    + "`passport_number`,\n"
                    + "`passport_expiry`,\n"
                    + "`passport_issue_by`,\n"
                    + "`other_id_type`,\n"
                    + "`other_id_expiry`,\n"
                    + "`other_id_issueBy`,\n"
                    + "`pir`,\n"
                    + "`ird_number`,\n"
                    + "`tex_residence_Country`,\n"
                    + "`tin`,\n"
                    + "`resn_tin_unavailable`,\n"
                    + "`bank_name`,\n"
                    + "`acount_holder_name`,\n"
                    + "`account_number`,\n"
                    + "`optional_num`,\n"
                    + "`preferredName`,"
                    + "`reg_id`,"
                    + "`investor_idverified`,"
                    + "`guardian_id`,"
                    + "`birth_certificate`"
                    + ")\n"
                    + "VALUES\n"
                    + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);  ";
            PreparedStatement ps1 = con.prepareStatement(innerSql, Statement.RETURN_GENERATED_KEYS);
            ps1.setString(1, bean.getEmail());
            String encryptedPassword = encoder.encode(bean.getPassword());
            ps1.setString(2, encryptedPassword);
            ps1.setString(3, bean.getTitle());
            ps1.setString(4, bean.getFullName());
            ps1.setString(5, bean.getDate_of_Birth());
            ps1.setString(6, bean.getCountry_residence());
            ps1.setString(7, bean.getOccupation());
            ps1.setString(8, bean.getWorking_with_adviser());
            ps1.setString(9, bean.getAdvisor_company());
            ps1.setString(10, bean.getAdvisor());
            ps1.setString(11, bean.getHomeAddress());
            ps1.setString(12, bean.getMobile_country_code());
            ps1.setString(13, bean.getMobile_number());
            ps1.setString(14, bean.getOptional_num_type());
            ps1.setString(15, bean.getOptional_num_code());
            ps1.setString(16, bean.getId_type());
            ps1.setString(17, bean.getLicense_number());
            ps1.setString(18, bean.getLicence_expiry_Date());
            ps1.setString(19, bean.getLicence_verson_number());
            ps1.setString(20, bean.getPassport_number());
            ps1.setString(21, bean.getPassport_expiry());
            ps1.setString(22, bean.getPassport_issue_by());
            ps1.setString(23, bean.getOther_id_type());
            ps1.setString(24, bean.getOther_id_expiry());
            ps1.setString(25, bean.getOther_id_issueBy());
            ps1.setString(26, bean.getPir());
            ps1.setString(27, bean.getIrd_number());
            ps1.setString(28, bean.getTex_residence_Country());
            ps1.setString(29, bean.getTin());
            ps1.setString(30, bean.getResn_tin_unavailable());
            ps1.setString(31, bean.getBank_name());
            ps1.setString(32, bean.getAcount_holder_name());
            ps1.setString(33, bean.getAccount_number());
            ps1.setString(34, bean.getOptional_num());
            ps1.setString(35, bean.getPreferredName());
            ps1.setString(36, bean.getReg_id());
            ps1.setString(37, bean.getInvestor_idverified());
            ps1.setString(38, bean.getGuardian_id());
            ps1.setString(39, bean.getBirth_certificate());
            ps1.executeUpdate();
            ResultSet generatedKeys1 = ps1.getGeneratedKeys();
            if (generatedKeys1.next()) {
                String individual_id = generatedKeys1.getString(1);
                String fetchSql1 = "INSERT INTO `CountryTIN`\n"
                        + " (`country`,`tin`,`reason`,`investor_id`,`reg_id`)\n"
                        + " VALUES (?,?,?,?,?); ";
                PreparedStatement ps2 = con.prepareStatement(fetchSql1, Statement.RETURN_GENERATED_KEYS);
                for (int j = 0; j < bean.getTins().length; j++) {
                    String tin = bean.getTins()[j];
                    String taxCountry = bean.getTaxCountries()[j];
                    String reasonTin = bean.getReasonTins()[j];
                    ps2.setString(1, taxCountry);
                    ps2.setString(2, tin);
                    ps2.setString(3, reasonTin);
                    ps2.setString(4, individual_id);
                    ps2.setString(5, bean.getReg_id());
                    ps2.addBatch();
                }
                ps2.executeBatch();
            }

            PersonDetailsBean minor = bean.getMinor();
            if (minor != null) {
                minor.setGuardian_id(bean.getReg_id());
                insertFormDetails(con, minor, response);
            }
            response.put("message", "You have added a new person.");
            response.put("status", 200);
        } catch (SQLException ex) {
//            response.put("opportunityList", opportunityList);
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
//            Logger.getLogger(OpportunityRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void CreateIndividualLogin(Connection con, PersonDetailsBean person, HashMap response) {
        try {
            String insertLogin = " INSERT INTO `login_master`\n"
                    + "(`username`,`password`,`created_ts`,`active`,`created_by`,`role`, `application_id`)\n"
                    + "VALUES\n"
                    + "(?,?,?,'y',?,'BENEFICIARY',?);";
            PreparedStatement ps = con.prepareStatement(insertLogin, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, person.getEmail());
            String encryptedPassword1 = encoder.encode(person.getPassword());
            ps.setString(2, encryptedPassword1);
            ps.setDate(3, new Date(new java.util.Date().getTime()));
            ps.setString(4, person.getCreated_by());
            ps.setString(5, person.getApplicationId());
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                String user_id = generatedKeys.getString(1);
                String sql1 = " INSERT INTO `user_beneficiairy_relationship`\n"
                        + "(`user_name`,`beneficiairy_id`,`user_id`, `advisor_id`)\n"
                        + "VALUES (?,?,?,?);";
                PreparedStatement ps1 = con.prepareStatement(sql1, Statement.RETURN_GENERATED_KEYS);
                ps1.setString(1, person.getEmail());
                ps1.setString(2, person.getBeneficiaryId());
                ps1.setString(3, user_id);
                ps1.setString(4, person.getAdvisor());
                ps1.executeUpdate();
//                String sql2 = "INSERT INTO `beneficiaries`\n"
//                        + "(`Id`, `Name`)\n"
//                        + "VALUES  (?, ?);";
                String sql2 = "INSERT INTO `beneficiaries`\n"
                        + "(`Id`,  `IrdNumber`, `Name`, `DateOfBirth`, `ClientAccountStartDate`, \n"
                        + "`FirstName`, `LastName`, `AccessLevel`, `AMLEntityType`,`Status`)\n"
                        + "VALUES  (?,?,?,?,?,?,?,?,?,?);";
                PreparedStatement ps2 = con.prepareStatement(sql2, Statement.RETURN_GENERATED_KEYS);
                ps2.setString(1, person.getBeneficiaryId());
                ps2.setString(2, person.getIrd_number());
                ps2.setString(3, person.getFullName());
                String dob = commonMethods.changeFormat(person.getDate_of_Birth(), CommonMethods.format8, CommonMethods.format3);
                ps2.setString(4, dob);
                ps2.setDate(5, new Date(new java.util.Date().getTime()));
                ps2.setString(6, person.getFirstName());
                ps2.setString(7, person.getLastName());
                ps2.setString(8, "ListAccess");
                ps2.setString(9, "Entity");
                ps2.setString(10, "PENDING");
                ps2.executeUpdate();
            }
            response.put("login", "You have added a new Login.");
            response.put("status", 200);
        } catch (SQLException ex) {
//            response.put("opportunityList", opportunityList);
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
//            Logger.getLogger(OpportunityRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void CreateJointLogin(Connection con, JointDetailBean joint, HashMap response) {
        try {
            String insertLogin = " INSERT INTO `login_master`\n"
                    + "(`username`,`password`,`created_ts`,`active`,`created_by`,`role`, `application_id`)\n"
                    + "VALUES\n"
                    + "(?,?,?,'y',?,'BENEFICIARY',?);";
            PreparedStatement ps = con.prepareStatement(insertLogin, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, joint.getEmail());
            Object update = response.get("update");
            if (update instanceof String && "yes".equalsIgnoreCase((String) update)) {
                String encryptedPassword1 = (joint.getPassword());
                ps.setString(2, encryptedPassword1);
            } else {
                String encryptedPassword1 = encoder.encode(joint.getPassword());
                ps.setString(2, encryptedPassword1);
            }
            ps.setDate(3, new Date(new java.util.Date().getTime()));
            ps.setString(4, joint.getCreated_by());
            ps.setString(5, joint.getApplicationId());
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                String user_id = generatedKeys.getString(1);
                String sql1 = " INSERT INTO `user_beneficiairy_relationship`\n"
                        + "(`user_name`,`beneficiairy_id`,`user_id`, `advisor_id`)\n"
                        + "VALUES (?,?,?,?);";
                PreparedStatement ps1 = con.prepareStatement(sql1, Statement.RETURN_GENERATED_KEYS);
                ps1.setString(1, joint.getEmail());
                ps1.setString(2, joint.getBeneficiaryId());
                ps1.setString(3, user_id);
                ps1.setString(4, joint.getAdvisor());
                ps1.executeUpdate();
                String sql2 = "INSERT INTO `beneficiaries`\n"
                        + "(`Id`, `Name`)\n"
                        + "VALUES  (?, ?);";
//                String sql2 = "INSERT INTO `beneficiaries`\n"
//                        + "(`Id`,  `IrdNumber`, `Name`, `DateOfBirth`, `ClientAccountStartDate`, \n"
//                        + "`FirstName`, `LastName`, `AccessLevel`, `AMLEntityType`,`Status`)\n"
//                        + "VALUES  (?,?,?,?,?,?,?,?,?,?);";
                PreparedStatement ps2 = con.prepareStatement(sql2, Statement.RETURN_GENERATED_KEYS);
                ps2.setString(1, joint.getBeneficiaryId());
                ps2.setString(2, joint.getFullName());
                ps2.executeUpdate();
            }
            response.put("login", "You have added a new Login.");
            response.put("status", 200);
        } catch (SQLException ex) {
//            response.put("opportunityList", opportunityList);
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
//            Logger.getLogger(OpportunityRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void UpdateFormDetails(Connection con, PersonDetailsBean person, HashMap response) {
        try {
            String innerSql = " update `individual_person_detail`\n"
                    + "SET\n"
                    + " `email`= ?,\n"
                    + "`password`= ?,\n"
                    + "`title`= ?,\n"
                    + "`fullName`= ?,\n"
                    + "`date_of_Birth`= ?,\n"
                    + "`country_residence`= ?,\n"
                    + "`occupation`= ?,\n"
                    + "`working_with_adviser`=?,\n"
                    + "`advisor_company`=?,\n"
                    + "`advisor`=?,\n"
                    + "`homeAddress`=?,\n"
                    + "`mobile_country_code`=?,\n"
                    + "`mobile_number`=?,\n"
                    + "`optional_num_type`=?,\n"
                    + "`optional_num_code`=?,\n"
                    + "`id_type`=?,\n"
                    + "`license_number`=?,\n"
                    + "`licence_expiry_Date`=?,\n"
                    + "`licence_verson_number`=?,\n"
                    + "`passport_number`=?,\n"
                    + "`passport_expiry`=?,\n"
                    + "`passport_issue_by`=?,\n"
                    + "`other_id_type`=?,\n"
                    + "`other_id_expiry`=?,\n"
                    + "`other_id_issueBy`=?,\n"
                    + "`pir`=?,\n"
                    + "`ird_number`=?,\n"
                    + "`tex_residence_Country`=?,\n"
                    + "`tin`=?,\n"
                    + "`resn_tin_unavailable`=?,\n"
                    + "`bank_name`=?,\n"
                    + "`acount_holder_name`=?,\n"
                    + "`account_number`=?,\n"
                    + "`optional_num`=?,\n"
                    + "`preferredName`=?,\n"
                    + "`wealth_src`=?,\n"
                    + "`investor_idverified`=? \n";
            if (person.getReg_id() != null) {
                innerSql += " WHERE `reg_id` = '" + person.getReg_id() + "';";
            } else {
                innerSql += " WHERE `email` = '" + person.getEmail() + "';";
            }

            PreparedStatement preparedStatement = con.prepareStatement(innerSql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, person.getEmail());
            String encryptedPassword3 = encoder.encode(person.getPassword());
            preparedStatement.setString(2, encryptedPassword3);
            preparedStatement.setString(3, person.getTitle());
            preparedStatement.setString(4, person.getFullName());
            preparedStatement.setString(5, person.getDate_of_Birth());
            preparedStatement.setString(6, person.getCountry_residence());
            preparedStatement.setString(7, person.getOccupation());
            preparedStatement.setString(8, person.getWorking_with_adviser());
            preparedStatement.setString(9, person.getAdvisor_company());
            preparedStatement.setString(10, person.getAdvisor());
            preparedStatement.setString(11, person.getHomeAddress());
            preparedStatement.setString(12, person.getMobile_country_code());
            preparedStatement.setString(13, person.getMobile_number());
            preparedStatement.setString(14, person.getOptional_num_type());
            preparedStatement.setString(15, person.getOptional_num_code());
            preparedStatement.setString(16, person.getId_type());
            preparedStatement.setString(17, person.getLicense_number());
            preparedStatement.setString(18, person.getLicence_expiry_Date());
            preparedStatement.setString(19, person.getLicence_verson_number());
            preparedStatement.setString(20, person.getPassport_number());
            preparedStatement.setString(21, person.getPassport_expiry());
            preparedStatement.setString(22, person.getPassport_issue_by());
            preparedStatement.setString(23, person.getOther_id_type());
            preparedStatement.setString(24, person.getOther_id_expiry());
            preparedStatement.setString(25, person.getOther_id_issueBy());
            preparedStatement.setString(26, person.getPir());
            preparedStatement.setString(27, person.getIrd_number());
            preparedStatement.setString(28, person.getTex_residence_Country());
            preparedStatement.setString(29, person.getTin());
            preparedStatement.setString(30, person.getResn_tin_unavailable());
            preparedStatement.setString(31, person.getBank_name());
            preparedStatement.setString(32, person.getAcount_holder_name());
            preparedStatement.setString(33, person.getAccount_number());
            preparedStatement.setString(34, person.getOptional_num());
            preparedStatement.setString(35, person.getPreferredName());
            preparedStatement.setString(36, person.getWealth_src());
            preparedStatement.setString(37, person.getInvestor_idverified());
            int i = preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                String fetchSql1 = " INSERT INTO `CountryTIN`\n"
                        + "(`country`,\n"
                        + "`tin`,\n"
                        + "`reason`,\n"
                        + "`reg_id`,\n"
                        + "`investor_id`,\n"
                        + "`active`)\n"
                        + "VALUES\n"
                        + "(?, ?, ?, ?, ?, 'Y')";
                PreparedStatement preparedStatement3 = con.prepareStatement(fetchSql1, Statement.RETURN_GENERATED_KEYS);
                for (int j = 0; j < person.getTins().length; j++) {
                    String tin = person.getTins()[j];
                    String taxCountry = person.getTaxCountries()[j];
                    String reasonTin = person.getReasonTins()[j];
                    preparedStatement3.setString(1, taxCountry);
                    preparedStatement3.setString(2, tin);
                    preparedStatement3.setString(3, reasonTin);
                    preparedStatement3.setString(4, person.getReg_id());
                    preparedStatement3.setString(5, person.getReg_id());
                    preparedStatement3.addBatch();
                }
                int[] x = preparedStatement3.executeBatch();
            }
            generatedKeys.close();
            preparedStatement.close();
            String fetchSql = " UPDATE `register` SET\n"
                    + " `password` =  ? ,`token` = ?,`status` = ?,`created_ts` = ?,`step` = ?,`reg_type` = ?\n"
                    + " WHERE `username`='" + person.getEmail() + "';";
            PreparedStatement preparedStatement2 = con.prepareStatement(fetchSql, Statement.RETURN_GENERATED_KEYS);
            String encryptedPassword1 = encoder.encode(person.getPassword());
            preparedStatement2.setString(1, encryptedPassword1);
            preparedStatement2.setString(2, person.getTokan());
            preparedStatement2.setString(3, person.getStatus());
            preparedStatement2.setDate(4, new Date(new java.util.Date().getTime()));
            preparedStatement2.setString(5, person.getStep());
            preparedStatement2.setString(6, person.getReg_type());
            int x = preparedStatement2.executeUpdate();
            PersonDetailsBean minor = person.getMinor();
            if (minor != null) {
                minor.setGuardian_id(person.getReg_id());
                minor.setReg_id(person.getReg_id());
                minor.setReg_type("MINOR_ACCOUNT");
                insertFormDetails(con, minor, response);
            }
            response.put("message", "You have updated details sucessfully ");
            response.put("status", 200);
        } catch (SQLException ex) {
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(FormDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void UpdateJointDetails(Connection con, JointDetailBean bean, HashMap response) {
        try {
            String sql = "UPDATE `joint_account_details` as J, `CountryTIN` as C SET C.active = 'N', J.active='N' WHERE J.reg_id = ? or C.reg_id = ?;";
            jdbcTemplate.update(sql, new Object[]{bean.getReg_id(), bean.getReg_id()});
            String sql2 = "UPDATE `register`  set `step`= ?, `status` = ? where `id` =? ;";
            jdbcTemplate.update(sql2, new Object[]{bean.getStep(), bean.getStatus(), bean.getReg_id()});
            String innerSql = "UPDATE `joint_account_details`\n"
                    + " SET `ird_Number` = ?,`title` = ?,`passport_expiry` = ?,`advisor` = ?,`optional_num_type` = ?,\n"
                    + " `tin` = ?,`licence_expiry_Date` = ?,`optional_num_code` = ?,`acount_holder_name` = ?,\n"
                    + " `passport_number` = ?,`passport_issue_by` = ?,`reg_type` = ?,`pir` = ?,`working_with_adviser` = ?,\n"
                    + " `status` = ?,`optional_num` = ?,`advisor_company` = ?,`tex_residence_Country` = ?,\n"
                    + " `bank_name` = ?,`license_number` = ?,`other_id_expiry` = ?,`licence_verson_number` = ?,`step` = ?,\n"
                    + " `mobile_country_code` = ?,`id_type` = ?,`mobile_number` = ?,`fullName` = ?,\n"
                    + " `other_id_issueBy` = ?,`isUSCitizen` = ?,`resn_tin_unavailable` = ?,`other_id_type` = ?,\n"
                    + " `date_of_Birth` = ?,`account_number` = ?,`homeAddress` = ?,`occupation` = ?,\n"
                    + " `created_ts` = ?, `created_by` = ?, `preferred_name` = ?,`active`='Y', `curr_idx` = ?,\n"
                    + " `investor_idverified`= ? , `country_residence`= ?  WHERE  `id` = '" + bean.getId() + "';";

            PreparedStatement ps = con.prepareStatement(innerSql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, bean.getIrd_Number());
            ps.setString(2, bean.getTitle());
            ps.setString(3, bean.getPassport_expiry());
            ps.setString(4, bean.getAdvisor());
            ps.setString(5, bean.getOptional_num_type());
            ps.setString(6, "");
            ps.setString(7, bean.getLicence_expiry_Date());
            ps.setString(8, bean.getOptional_num_code());
            ps.setString(9, bean.getAcount_holder_name());
            ps.setString(10, bean.getPassport_number());
            ps.setString(11, bean.getPassport_issue_by());
            ps.setString(12, bean.getReg_type());
            ps.setString(13, bean.getPir());
            ps.setString(14, bean.getWorking_with_adviser());
            ps.setString(15, bean.getStatus());
            ps.setString(16, bean.getOptional_num());
            ps.setString(17, bean.getAdvisor_company());
            ps.setString(18, "");
            ps.setString(19, bean.getBank_name());
            ps.setString(20, bean.getLicense_number());
            ps.setString(21, bean.getOther_id_expiry());
            ps.setString(22, bean.getLicence_verson_number());
            ps.setString(23, bean.getStep());
            ps.setString(24, bean.getMobile_country_code());
            ps.setString(25, bean.getId_type());
            ps.setString(26, bean.getMobile_number());
            ps.setString(27, bean.getFullName());
            ps.setString(28, bean.getOther_id_issueBy());
            ps.setString(29, bean.getIsUSCitizen());
            ps.setString(30, bean.getResn_tin_unavailable());
            ps.setString(31, bean.getOther_id_type());
            ps.setString(32, bean.getDate_of_Birth());
            ps.setString(33, bean.getAccount_number());
            ps.setString(34, bean.getHomeAddress());
            ps.setString(35, bean.getOccupation());
            ps.setDate(36, new Date(new java.util.Date().getTime()));
            ps.setString(37, bean.getCreated_by());
            ps.setString(38, bean.getPreferred_name());
            ps.setString(39, bean.getCurr_idx());
            ps.setString(40, bean.getInvestor_idverified());
            ps.setString(41, bean.getCountry_residence());
            ps.executeUpdate();
            List<CountryTINBean> countryTINList = new LinkedList<>();
            bean.getCountryTINList().forEach((tinbean) -> {
                tinbean.setInvestor_id(bean.getId());
            });
            countryTINList.addAll(bean.getCountryTINList());
            List<JointDetailBean> moreInvestorList = bean.getMoreInvestorList();
            if (moreInvestorList != null && !moreInvestorList.isEmpty()) {
                String investorSQL = "INSERT INTO `joint_account_details`\n"
                        + " (`IRD_Number`,`Title`,`passport_expiry`,`advisor`,`optional_num_type`,\n"
                        + " `TIN`,`licence_expiry_Date`,`optional_num_code`,`acount_holder_name`,\n"
                        + " `passport_number`,`passport_issue_by`,`reg_type`,`PIR`,`working_with_adviser`,\n"
                        + " `email`,`status`,`optional_num`,`advisor_company`,`tex_residence_Country`,\n"
                        + " `bank_name`,`License_number`,`other_id_expiry`,`licence_verson_number`,`step`,\n"
                        + " `mobile_country_code`,`id_type`,`mobile_number`,`fullName`,`password`,\n"
                        + " `other_id_issueBy`,`isUSCitizen`,`resn_tin_unavailable`,`other_id_type`,\n"
                        + " `Date_of_Birth`,`account_number`,`homeAddress`,`Occupation`,`created_ts`,\n"
                        + "`created_by`,`reg_id`,`active`,`preferred_name`,`token`,`investor_idverified`,`country_residence`)\n"
                        + "  VALUES  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";
                PreparedStatement ps1 = con.prepareStatement(investorSQL, Statement.RETURN_GENERATED_KEYS);
                for (JointDetailBean investor : moreInvestorList) {
                    ps1.setString(1, investor.getIrd_Number());
                    ps1.setString(2, investor.getTitle());
                    ps1.setString(3, investor.getPassport_expiry());
                    ps1.setString(4, investor.getAdvisor());
                    ps1.setString(5, investor.getOptional_num_type());
                    ps1.setString(6, null);
                    ps1.setString(7, investor.getLicence_expiry_Date());
                    ps1.setString(8, investor.getOptional_num_code());
                    ps1.setString(9, investor.getAcount_holder_name());
                    ps1.setString(10, investor.getPassport_number());
                    ps1.setString(11, investor.getPassport_issue_by());
                    ps1.setString(12, investor.getReg_type());
                    ps1.setString(13, investor.getPir());
                    ps1.setString(14, investor.getWorking_with_adviser());
                    ps1.setString(15, investor.getEmail());
                    ps1.setString(16, investor.getStatus());
                    ps1.setString(17, investor.getOptional_num());
                    ps1.setString(18, investor.getAdvisor_company());
                    ps1.setString(19, null);
                    ps1.setString(20, investor.getBank_name());
                    ps1.setString(21, investor.getLicense_number());
                    ps1.setString(22, investor.getOther_id_expiry());
                    ps1.setString(23, investor.getLicence_verson_number());
                    ps1.setString(24, investor.getStep());
                    ps1.setString(25, investor.getMobile_country_code());
                    ps1.setString(26, investor.getId_type());
                    ps1.setString(27, investor.getMobile_number());
                    ps1.setString(28, investor.getFullName());
                    ps1.setString(29, investor.getPassword());
                    ps1.setString(30, investor.getOther_id_issueBy());
                    ps1.setString(31, investor.getIsUSCitizen());
                    ps1.setString(32, investor.getResn_tin_unavailable());
                    ps1.setString(33, investor.getOther_id_type());
                    ps1.setString(34, investor.getDate_of_Birth());
                    ps1.setString(35, investor.getAccount_number());
                    ps1.setString(36, investor.getHomeAddress());
                    ps1.setString(37, investor.getOccupation());
                    ps1.setDate(38, new Date(new java.util.Date().getTime()));
                    ps1.setString(39, investor.getCreated_by());
                    ps1.setString(40, bean.getReg_id());
                    ps1.setString(41, "Y");
                    ps1.setString(42, investor.getPreferred_name());
                    ps1.setString(43, investor.getToken());
                    ps1.setString(44, investor.getInvestor_idverified());
                    ps1.setString(45, investor.getCountry_residence());
                    ps1.addBatch();
                }
                System.out.println(" IN Execute Batch For Item Charge");
                ps1.executeBatch();
                ResultSet rs = ps1.getGeneratedKeys();
                Integer i = 0;
                while (rs.next()) {
                    JointDetailBean investor = moreInvestorList.get(i);
                    String investor_id = rs.getString(1);
                    investor.setId(investor_id);
                    if (!investor.getCountryTINList().isEmpty() && investor.getCountryTINList() != null) {
                        investor.getCountryTINList().forEach((tinbean) -> {
                            tinbean.setInvestor_id(investor_id);
                        });
                    }

                    countryTINList.addAll(investor.getCountryTINList());
                    i++;
                }
            }
            if (!countryTINList.isEmpty()) {
                String tinsql = "INSERT INTO `CountryTIN`\n"
                        + "(`country`,\n"
                        + "`tin`,\n"
                        + "`reason`,\n"
                        + "`reg_id`,\n"
                        + "`investor_id`,\n"
                        + "`active`)\n"
                        + "VALUES\n"
                        + "(?, ?, ?, ?, ?, 'Y')";
                PreparedStatement ps2 = con.prepareStatement(tinsql);
                for (CountryTINBean tinbean : countryTINList) {
                    ps2.setString(1, tinbean.getCountry());
                    ps2.setString(2, tinbean.getTin());
                    ps2.setString(3, tinbean.getReason());
                    ps2.setString(4, bean.getReg_id());
                    ps2.setString(5, tinbean.getInvestor_id());
                    ps2.addBatch();
                }
                ps2.executeBatch();
            }
            System.out.println("batch completed");
            response.put("message", "You have updated details sucessfully ");
            response.put("status", 200);
        } catch (SQLException ex) {
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(FormDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void UpdateCompanyDetails(Connection con, CompanyDetailBean bean, HashMap response) {
        try {
            String sql = "UPDATE `company_trust_details` as CD SET CD.active = 'N' WHERE  CD.reg_id = ?;";
            jdbcTemplate.update(sql, new Object[]{bean.getReg_id()});
            String sql2 = "UPDATE `register`  set `step`= ?, `status` = ? where `id` =? ;";
            jdbcTemplate.update(sql2, new Object[]{bean.getStep(), bean.getStatus(), bean.getReg_id()});
            String innerSql = "UPDATE `company_trust_details`\n"
                    + " SET `mobileNo` = ?, `countryOfResidence` = ?, "
                    + " `countryCode` = ?, `bankName` = ?, `countryOfIssue` = ?,\n"
                    + " `countryOfTaxResidence` = ?,`companyReasonTIN` = ?, `myFile` = ?,"
                    + " `passportExpiryDate` = ?,`pir` = ?, `occupation` = ?,\n"
                    + " `registerNumber` = ?, `dateOfBirth` = ?,`typeOfID` = ?,`companyIRDNumber` = ?,"
                    + " `detail` = ?,`holderCountryOfResidence` = ?,\n"
                    + " `dateOfInco` = ?,`fname` = ?,`taxIdentityNumber` = ?,`postalAddress` = ?,"
                    + " `isCompanyFinancialInstitution` = ?,`isCompanyGovernment` = ?,\n"
                    + " `status` = ?,`investAdviser` = ?,`companyName` = ?,`typeCountryOfIssue` = ?,"
                    + " `sourceOfFunds` = ?,`otherCountryCode` = ?,`isCompanyActivePassive` = ?,\n"
                    + " `versionNumber` = ?,`selectAdviser` = ?,`licenseExpiryDate` = ?,`address` = ?,"
                    + " `countryCode3` = ?,`srcOfFund` = ?,\n"
                    + " `companyCountryOfTaxResidence` = ?,`positionInCompany` = ?,`irdNumber` = ?,"
                    + " `companyAddress` = ?,`accountNumber` = ?,`licenseNumber` = ?,\n"
                    + " `isCompanyListed` = ?,`nameOfAccount` = ?,`companyTaxIdentityNumber` = ?,"
                    + " `isCompanyNZPolice` = ?,`passportNumber` = ?,\n"
                    + " `isCompanyUSCitizen` = ?,`otherMobileNo` = ?,`companyAdvisor` = ?,"
                    + " `otherNumber` = ?,`citizenUS` = ?,`created_ts` = ?,`created_by` = ?,`isInvestor_idverified` = ?,`countryofincorporation`=?,`reg_type` = ?, `active`='Y' \n"
                    + " WHERE `id` = ?;";

            PreparedStatement preparedStatement = con.prepareStatement(innerSql, Statement.RETURN_GENERATED_KEYS);
//            preparedStatement.setString(1, bean.getEmail());
//            String encryptedPassword = encoder.encode(bean.getPassword());
//            preparedStatement.setString(2, encryptedPassword);
            preparedStatement.setString(1, bean.getMobileNo());
            preparedStatement.setString(2, bean.getCountryOfResidence());
            preparedStatement.setString(3, bean.getCountryCode());
            preparedStatement.setString(4, bean.getBankName());
            preparedStatement.setString(5, bean.getCountryOfIssue());
            preparedStatement.setString(6, bean.getCountryOfTaxResidence());
            preparedStatement.setString(7, bean.getCompanyReasonTIN());
            preparedStatement.setString(8, bean.getMyFile());
            preparedStatement.setString(9, bean.getPassportExpiryDate());
            preparedStatement.setString(10, bean.getPir());
            preparedStatement.setString(11, bean.getOccupation());
            preparedStatement.setString(12, bean.getRegisterNumber());
            preparedStatement.setString(13, bean.getDateOfBirth());
            preparedStatement.setString(14, bean.getTypeOfID());
            preparedStatement.setString(15, bean.getCompanyIRDNumber());
            preparedStatement.setString(16, bean.getDetail());
            preparedStatement.setString(17, bean.getHolderCountryOfResidence());
            preparedStatement.setString(18, bean.getDateOfInco());
            preparedStatement.setString(19, bean.getFname());
            preparedStatement.setString(20, bean.getTaxIdentityNumber());
            preparedStatement.setString(21, bean.getPostalAddress());
            preparedStatement.setString(22, bean.getIsCompanyFinancialInstitution());
            preparedStatement.setString(23, bean.getIsCompanyGovernment());
            preparedStatement.setString(24, bean.getStatus());
            preparedStatement.setString(25, bean.getInvestAdviser());
            preparedStatement.setString(26, bean.getCompanyName());
            preparedStatement.setString(27, bean.getTypeCountryOfIssue());
            preparedStatement.setString(28, bean.getSourceOfFunds());
            preparedStatement.setString(29, bean.getOtherCountryCode());
            preparedStatement.setString(30, bean.getIsCompanyActivePassive());
            preparedStatement.setString(31, bean.getVersionNumber());
            preparedStatement.setString(32, bean.getSelectAdviser());
            preparedStatement.setString(33, bean.getLicenseExpiryDate());
            preparedStatement.setString(34, bean.getAddress());
            preparedStatement.setString(35, bean.getCountryCode3());
            preparedStatement.setString(36, bean.getSrcOfFund());
            preparedStatement.setString(37, bean.getCompanyCountryOfTaxResidence());
            preparedStatement.setString(38, bean.getPositionInCompany());
            preparedStatement.setString(39, bean.getIrdNumber());
            preparedStatement.setString(40, bean.getCompanyAddress());
            preparedStatement.setString(41, bean.getAccountNumber());
            preparedStatement.setString(42, bean.getLicenseNumber());
            preparedStatement.setString(43, bean.getIsCompanyListed());
            preparedStatement.setString(44, bean.getNameOfAccount());
            preparedStatement.setString(45, bean.getCompanyTaxIdentityNumber());
            preparedStatement.setString(46, bean.getIsCompanyNZPolice());
            preparedStatement.setString(47, bean.getPassportNumber());
            preparedStatement.setString(48, bean.getIsCompanyUSCitizen());
            preparedStatement.setString(49, bean.getOtherMobileNo());
            preparedStatement.setString(50, bean.getCompanyAdvisor());
            preparedStatement.setString(51, bean.getOtherNumber());
            preparedStatement.setString(52, bean.getCitizenUS());
            preparedStatement.setString(53, bean.getCrearted_ts());
            preparedStatement.setString(54, bean.getCreated_by());
            preparedStatement.setString(55, bean.getIsInvestor_idverified());
            preparedStatement.setString(56, bean.getCountryofincorporation());
            preparedStatement.setString(57, bean.getReg_type());
            preparedStatement.setString(58, bean.getId());
            preparedStatement.executeUpdate();
//             preparedStatement.close();

            List<CompanyDetailBean> investorList = bean.getMoreInvestorList();
            if (!investorList.isEmpty()) {
                String investorSQL = "INSERT INTO `company_trust_details`\n"
                        + " (`mobileNo`,`countryOfResidence`,`countryCode`,`bankName`,`countryOfIssue`,`countryOfTaxResidence`,`companyReasonTIN`,\n"
                        + " `myFile`,`passportExpiryDate`,`pir`,`occupation`,`registerNumber`,`dateOfBirth`,`typeOfID`,`companyIRDNumber`,`detail`,`holderCountryOfResidence`,\n"
                        + " `dateOfInco`,`fname`,`taxIdentityNumber`,`postalAddress`,`isCompanyFinancialInstitution`,`isCompanyGovernment`,`status`,`investAdviser`,\n"
                        + " `companyName`,`typeCountryOfIssue`,`sourceOfFunds`,`otherCountryCode`,`isCompanyActivePassive`,`versionNumber`,`selectAdviser`,`licenseExpiryDate`,\n"
                        + " `address`,`countryCode3`,`srcOfFund`,`companyCountryOfTaxResidence`,`positionInCompany`,`irdNumber`,`companyAddress`,`accountNumber`,`licenseNumber`,\n"
                        + " `isCompanyListed`,`nameOfAccount`,`companyTaxIdentityNumber`,`isCompanyNZPolice`,`passportNumber`,`isCompanyUSCitizen`,`otherMobileNo`,\n"
                        + " `companyAdvisor`,`otherNumber`,`citizenUS`,`created_ts`,`created_by`,`reg_id`,`isInvestor_idverified`, `email`, `reg_type`)\n"
                        + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";

                PreparedStatement ps1 = con.prepareStatement(investorSQL, Statement.RETURN_GENERATED_KEYS);
                for (CompanyDetailBean investor : investorList) {
                    ps1.setString(1, investor.getMobileNo());
                    ps1.setString(2, investor.getCountryOfResidence());
                    ps1.setString(3, investor.getCountryCode());
                    ps1.setString(4, investor.getBankName());
                    ps1.setString(5, investor.getCountryOfIssue());
                    ps1.setString(6, investor.getCountryOfTaxResidence());
                    ps1.setString(7, investor.getCompanyReasonTIN());
                    ps1.setString(8, investor.getMyFile());
                    ps1.setString(9, investor.getPassportExpiryDate());
                    ps1.setString(10, investor.getPir());
                    ps1.setString(11, investor.getOccupation());
                    ps1.setString(12, investor.getRegisterNumber());
                    ps1.setString(13, investor.getDateOfBirth());
                    ps1.setString(14, investor.getTypeOfID());
                    ps1.setString(15, investor.getCompanyIRDNumber());
                    ps1.setString(16, investor.getDetail());
                    ps1.setString(17, investor.getHolderCountryOfResidence());
                    ps1.setString(18, investor.getDateOfInco());
                    ps1.setString(19, investor.getFname());
                    ps1.setString(20, investor.getTaxIdentityNumber());
                    ps1.setString(21, investor.getPostalAddress());
                    ps1.setString(22, investor.getIsCompanyFinancialInstitution());
                    ps1.setString(23, investor.getIsCompanyGovernment());
                    ps1.setString(24, investor.getStatus());
                    ps1.setString(25, investor.getInvestAdviser());
                    ps1.setString(26, investor.getCompanyName());
                    ps1.setString(27, investor.getTypeCountryOfIssue());
                    ps1.setString(28, investor.getSourceOfFunds());
                    ps1.setString(29, investor.getOtherCountryCode());
                    ps1.setString(30, investor.getIsCompanyActivePassive());
                    ps1.setString(31, investor.getVersionNumber());
                    ps1.setString(32, investor.getSelectAdviser());
                    ps1.setString(33, investor.getLicenseExpiryDate());
                    ps1.setString(34, investor.getAddress());
                    ps1.setString(35, investor.getCountryCode3());
                    ps1.setString(36, investor.getSrcOfFund());
                    ps1.setString(37, investor.getCompanyCountryOfTaxResidence());
                    ps1.setString(38, investor.getPositionInCompany());
                    ps1.setString(39, investor.getIrdNumber());
                    ps1.setString(40, investor.getCompanyAddress());
                    ps1.setString(41, investor.getAccountNumber());
                    ps1.setString(42, investor.getLicenseNumber());
                    ps1.setString(43, investor.getIsCompanyListed());
                    ps1.setString(44, investor.getNameOfAccount());
                    ps1.setString(45, investor.getCompanyTaxIdentityNumber());
                    ps1.setString(46, investor.getIsCompanyNZPolice());
                    ps1.setString(47, investor.getPassportNumber());
                    ps1.setString(48, investor.getIsCompanyUSCitizen());
                    ps1.setString(49, investor.getOtherMobileNo());
                    ps1.setString(50, investor.getCompanyAdvisor());
                    ps1.setString(51, investor.getOtherNumber());
                    ps1.setString(52, investor.getCitizenUS());
                    ps1.setString(53, investor.getCrearted_ts());
                    ps1.setString(54, investor.getCreated_by());
                    ps1.setString(55, bean.getReg_id());
                    ps1.setString(56, bean.getIsInvestor_idverified());
                    ps1.setString(57, investor.getEmail());
                    ps1.setString(58, investor.getReg_type());
                    ps1.addBatch();
                }
                System.out.println(" IN Execute Batch For Item Charge");
                ps1.executeBatch();
                ResultSet rs = ps1.getGeneratedKeys();
                List<CountryTINBean> countryTINList = new LinkedList<>();
                Integer i = 0;
                while (rs.next()) {
                    CompanyDetailBean investor = investorList.get(i);
                    String investor_id = rs.getString(1);
                    investor.setId(investor_id);
                    investor.getCountryTINList().forEach((tinbean) -> {
                        tinbean.setInvestor_id(investor_id);
                    });
                    countryTINList.addAll(investor.getCountryTINList());
                    i++;
                }
                if (!countryTINList.isEmpty()) {
                    String tinsql = "INSERT INTO `CountryTIN`\n"
                            + "(`country`,\n"
                            + "`tin`,\n"
                            + "`reason`,\n"
                            + "`reg_id`,\n"
                            + "`investor_id`,\n"
                            + "`active`)\n"
                            + "VALUES\n"
                            + "(?, ?, ?, ?, ?, 'Y')";
                    PreparedStatement ps2 = con.prepareStatement(tinsql);
                    for (CountryTINBean tinbean : countryTINList) {
                        ps2.setString(1, tinbean.getCountry());
                        ps2.setString(2, tinbean.getTin());
                        ps2.setString(3, tinbean.getReason());
                        ps2.setString(4, bean.getReg_id());
                        ps2.setString(5, tinbean.getInvestor_id());
                        ps2.addBatch();
                    }
                    ps2.executeBatch();
                }
                System.out.println("batch completed");
            }
            response.put("message", "You have updated details sucessfully ");
            response.put("status", 200);
        } catch (SQLException ex) {
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger
                    .getLogger(FormDetailsRepository.class
                            .getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void fetchFormDetails(String tokan, HashMap response) {
        Connection con = null;
        try {
            con = jdbcTemplate.getDataSource().getConnection();
            String innerSql = "SELECT i.* FROM individual_person_detail i inner join login l on(l.userId=i.email) where l.Tokan='" + tokan + "';";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(innerSql);
            List<Map> list = ObjectCastManager.getInstance().resultSetCasting(rs);
            response.put("formDetailsList", list);
            response.put("message", "You have requested for tasks. Now you have these tasks.");
            response.put("status", 200);
        } catch (SQLException ex) {
            response.put("formDetailsList", null);
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(FormDetailsRepository.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

}
