/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.lambda.repo;

import crm.nz.beans.acc.api.AdvisorInvestmentBean;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author TOSHIBA R830
 */
@Repository
public class AdvisorRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<AdvisorInvestmentBean> getUserInvestmentByAdvisor(String id) {
        String sql = "  SELECT pt.InvestmentCode as investmentCode,pt.EffectiveDate as effectiveDate,pt.TypeDisplayName as investmentType,\n"
                + "  pt.SubType as investmentSubType,pt.PortfolioCode as portfolioCode,pt.PortfolioName as portfolioName,\n"
                + "  sum(pt.Units) as Units, round(pt.Price,4) as price,sum(pt.Value) as value,\n"
                + "  sum(CASE when pt.SubType = 'ATT' or pt.SubType = 'INV' Then pt.Value ELSE 0 END) as att_value,\n"
                + "  sum(CASE when pt.SubType = 'WDW' Then pt.Value ELSE 0 END) as wdw_value,\n"
                + "  ubr.user_name as username,ubr.id,ubr.beneficiairy_id as beneficiairyId,ubr.user_id as userId,ubr.advisor_id as advisorId,b.Name as name \n"
                + "  FROM investment_transaction pt\n"
                + "  inner join investments i on (i.Code=pt.InvestmentCode)\n"
                + "  inner join beneficiaries b on (b.Id=i.BeneficiaryId)\n"
                + "  inner join user_beneficiairy_relationship ubr on(b.Id = ubr.beneficiairy_id)\n"
                + "  where (ubr.user_id = ? or ubr.advisor_id = ?) \n"
                + "  group by investmentCode, portfolioCode; ";
        System.out.println("sql--" + sql);
        List<AdvisorInvestmentBean> InvestmentDetail = jdbcTemplate.query(sql, new Object[]{id, id}, new BeanPropertyRowMapper(AdvisorInvestmentBean.class));
        return InvestmentDetail;
    }
}
