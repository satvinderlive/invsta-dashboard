package crm.nz.lambda.repo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.amazonaws.services.s3.internal.ObjectExpirationResult;
import crm.nz.beans.acc.api.Address;
import crm.nz.beans.acc.api.BankAccountDetail;
import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.beans.acc.api.CountryTINBean;
import crm.nz.beans.acc.api.JointDetailBean;
import crm.nz.beans.acc.api.PersonDetailsBean;
import crm.nz.beans.acc.api.PhoneNumber;
import crm.nz.beans.acc.api.Register;
import crm.nz.beans.acc.api.SettingLoginBean;
import crm.nz.components.ObjectCastManager;
import crm.nz.table.bean.Login;
//import static crm.nz.security.CurrentUserService.encoder;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author innovative002
 */
@Repository
public class SettingDetailsRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    BCryptPasswordEncoder encoder;

    public Login getMatchPasswordByEmail(String email, String oldpassword) {
        String sql = "SELECT  `password`\n"
                + " FROM `login_master` where username = ?;";
        List<Login> login = jdbcTemplate.query(sql, new Object[]{email}, new BeanPropertyRowMapper(Login.class));
        Login get = login.get(0);
//        if(oldpassword.matches(get.getPassword()));
        return get;
    }

    public PersonDetailsBean getPersonDetailByToken(String token) {
        String sql = " select IPD.* , R.* from `individual_person_detail` IPD  \n"
                + " inner join  `register` R ON (IPD.email = R.username and R.reg_type = 'INDIVIDUAL_ACCOUNT') \n"
                + " where R.token = '" + token + "'";
        List<PersonDetailsBean> personDetail = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PersonDetailsBean.class));
        PersonDetailsBean bean = !personDetail.isEmpty() ? personDetail.get(personDetail.size() - 1) : null;
        if (bean != null) {
            String tinSQl = " select ITC.id, ITC.country, ITC.tin, ITC.reason, IPD.reg_id\n"
                    + " from CountryTIN ITC inner join individual_person_detail IPD on (ITC.reg_id = IPD.reg_id)\n"
                    + " where IPD.reg_id = " + bean.getReg_id();
            List<CountryTINBean> tinBeanList = jdbcTemplate.query(tinSQl, new BeanPropertyRowMapper(CountryTINBean.class));
            bean.setCountryTINList(tinBeanList);
        }
        return bean;
    }

    public void savePasswordDetails(SettingLoginBean settingBean, HashMap response) {
        try {
            Login matchPasswordByEmail = getMatchPasswordByEmail(settingBean.getUsername(), settingBean.getOld_password());
            boolean matches = encoder.matches(settingBean.getOld_password(), matchPasswordByEmail.getPassword());
            if (matches) {
                Connection con = jdbcTemplate.getDataSource().getConnection();
                try {
                    UpdatePasswordDetails(con, settingBean, response);
                } finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(SettingDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                response.put("password", "Your password change request accept successfully.");
            } else {
                response.put("password", "You have Submited old Password did not match.");
            }
            response.put("message", "You have Submited info Successfully.");
            response.put("status", 200);
        } catch (SQLException ex) {
            Logger.getLogger(SettingDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void savePhoneDetails(PhoneNumber phone, HashMap response) {
        try {
//            getUpdByEmail(phone);
            if (phone != null) {
                Connection con = jdbcTemplate.getDataSource().getConnection();
                try {
                    UpdatePhoneDetails(con, phone, response);
                } finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(SettingDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                response.put("phone", "Your phone number change request accept successfully.");
            } else {
                response.put("phone", "Your data is incomplete.");
            }
            response.put("message", "You have Submited info Successfully.");
            response.put("status", 200);
        } catch (SQLException ex) {
            Logger.getLogger(SettingDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void saveAddressDetails(Address address, HashMap response) {
        try {
//            getUpdByEmail(phone);
            if (address != null) {
                Connection con = jdbcTemplate.getDataSource().getConnection();
                try {
                    UpdateAddressDetails(con, address, response);
                } finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(SettingDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                response.put("address", "Your address change request accept successfully.");
            } else {
                response.put("address", "You have Submited info Successfully.");
            }
            response.put("message", "You have Submited info Successfully.");
            response.put("status", 200);
        } catch (SQLException ex) {
            Logger.getLogger(SettingDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void saveBankDetails(BankAccountDetail bank, HashMap response) {
        try {
//            getUpdByEmail(phone);
            if (bank != null) {
                Connection con = jdbcTemplate.getDataSource().getConnection();
                try {
                    UpdateBankDetails(con, bank, response);
                } finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(SettingDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                response.put("bank", "Your Bank Account change request accept successfully.");
            } else {
                response.put("bank", "You have Submited info Successfully.");
            }
            response.put("message", "You have Submited info Successfully.");
            response.put("status", 200);
        } catch (SQLException ex) {
            Logger.getLogger(SettingDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void UpdatePasswordDetails(Connection con, SettingLoginBean bean, HashMap response) {
        try {
            String innerSql = " UPDATE `login_master` \n"
                    + " SET `password` = ? \n"
                    + " WHERE  `username` =  '" + bean.getUsername() + "';";

            PreparedStatement preparedStatement = con.prepareStatement(innerSql, Statement.RETURN_GENERATED_KEYS);
            String encryptedPassword3 = encoder.encode(bean.getNew_password());
            preparedStatement.setString(1, encryptedPassword3);
            int i = preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(SettingDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void UpdatePhoneDetails(Connection con, PhoneNumber phone, HashMap response) {
        try {
            String innerSql = " UPDATE `phone_numbers`\n"
                    + "SET  `IsPrimary` = '0'\n"
                    + " WHERE `BeneficiaryId` = ?;";

            PreparedStatement preparedStatement = con.prepareStatement(innerSql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, phone.getBeneficiaryId());
            int i = preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();

            String fetchSql1 = " INSERT INTO `phone_numbers`\n"
                    + "(`Country`,`CountryCode`,`Number`,`Type`,`Home`,`IsPrimary`,`BeneficiaryId`)\n"
                    + "VALUES (?,?,?,?,?,1,?);";
            PreparedStatement preparedStatement3 = con.prepareStatement(fetchSql1, Statement.RETURN_GENERATED_KEYS);
            preparedStatement3.setString(1, phone.getCountry());
            preparedStatement3.setString(2, phone.getCountryCode());
            preparedStatement3.setString(3, phone.getNumber());
            preparedStatement3.setString(4, phone.getType());
            preparedStatement3.setString(5, phone.getHome());
            preparedStatement3.setString(6, phone.getBeneficiaryId());
            int executeUpdate = preparedStatement3.executeUpdate();

            con.close();
        } catch (SQLException ex) {
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(SettingDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void UpdateAddressDetails(Connection con, Address address, HashMap response) {
        try {
            String innerSql = " UPDATE `address`\n"
                    + " SET  `IsPrimary` = '0'\n"
                    + " WHERE `BeneficiaryId` = ?;";

            PreparedStatement preparedStatement = con.prepareStatement(innerSql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, address.getBeneficiaryId());
            int i = preparedStatement.executeUpdate();
//            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            String FormatedAddress = address.getAddressLine1() + "," + address.getAddressLine2() + "," + address.getAddressLine3() + "," + address.getAddressLine4() + "," + address.getCity() + "," + address.getRegion() + "," + address.getState() + "," + address.getPostalCode();
            address.setFormattedAddress(FormatedAddress);
            String fetchSql1 = "INSERT INTO `address`\n"
                    + "(`EffectiveDate`,`InEffectiveDate`,`AddressID`,`Country`,`CountryCode`,`Type`,`AddressLine1`,\n"
                    + "`AddressLine2`,`AddressLine3`,`AddressLine4`,`City`,`Region`,`State`,`PostalCode`,`FormattedAddress`,`IsPrimary`,`BeneficiaryId`)\n"
                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'1',?);";
            PreparedStatement preparedStatement3 = con.prepareStatement(fetchSql1, Statement.RETURN_GENERATED_KEYS);
            preparedStatement3.setString(1, address.getEffectiveDate());
            preparedStatement3.setString(2, address.getInEffectiveDate());
            preparedStatement3.setString(3, address.getAddressID());
            preparedStatement3.setString(4, address.getCountry());
            preparedStatement3.setString(5, address.getCountryCode());
            preparedStatement3.setString(6, address.getType());
            preparedStatement3.setString(7, address.getAddressLine1());
            preparedStatement3.setString(8, address.getAddressLine2());
            preparedStatement3.setString(9, address.getAddressLine3());
            preparedStatement3.setString(10, address.getAddressLine4());
            preparedStatement3.setString(11, address.getCity());
            preparedStatement3.setString(12, address.getRegion());
            preparedStatement3.setString(13, address.getState());
            preparedStatement3.setString(14, address.getPostalCode());
            preparedStatement3.setString(15, address.getFormattedAddress());
            preparedStatement3.setString(16, address.getBeneficiaryId());
            int executeUpdate = preparedStatement3.executeUpdate();

//            generatedKeys.close();
            con.close();
        } catch (SQLException ex) {
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(SettingDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void UpdateBankDetails(Connection con, BankAccountDetail bank, HashMap response) {
        try {
            String innerSql = " UPDATE `Bank_account_Detail`\n"
                    + " SET  `IsPrimary` = '0' \n"
                    + " WHERE `BeneficiaryId` = ?;";

            PreparedStatement preparedStatement = con.prepareStatement(innerSql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, bank.getBeneficiaryId());
            preparedStatement.executeUpdate();

            String fetchSql1 = "INSERT INTO `Bank_account_Detail`\n"
                    + "(`AccountName`,`Bank`,`Branch`,`Account`,`Suffix`,`Currency`,`Status`,\n"
                    + "`Type`,`IsPrimary`,`BeneficiaryId`,`BankAccountDetailId`,`InvestmentCode`)\n"
                    + "VALUES (?,?,?,?,?,?,?,?,'1',?,?,?);";
            PreparedStatement preparedStatement3 = con.prepareStatement(fetchSql1, Statement.RETURN_GENERATED_KEYS);
            preparedStatement3.setString(1, bank.getAccountName());
            preparedStatement3.setString(2, bank.getBank());
            preparedStatement3.setString(3, bank.getBranch());
            preparedStatement3.setString(4, bank.getAccount());
            preparedStatement3.setString(5, bank.getSuffix());
            preparedStatement3.setString(6, bank.getCurrency());
            preparedStatement3.setString(7, bank.getStatus());
            preparedStatement3.setString(8, bank.getType());
            preparedStatement3.setString(9, bank.getBeneficiaryId());
            preparedStatement3.setString(10, bank.getBankAccountDetailId());
            preparedStatement3.setString(11, bank.getInvestmentCode());
            preparedStatement3.executeUpdate();
            
            System.out.println("fetchSql1"+ fetchSql1);
            con.close();
        } catch (SQLException ex) {
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(SettingDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void fetchFormDetails(String tokan, HashMap response) {
        Connection con = null;
        try {
            con = jdbcTemplate.getDataSource().getConnection();
            String innerSql = "SELECT i.* FROM individual_person_detail i inner join login l on(l.userId=i.email) where l.Tokan='" + tokan + "';";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(innerSql);
            List<Map> list = ObjectCastManager.getInstance().resultSetCasting(rs);
            response.put("formDetailsList", list);
            response.put("message", "You have requested for tasks. Now you have these tasks.");
            response.put("status", 200);
        } catch (SQLException ex) {
            response.put("formDetailsList", null);
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SettingDetailsRepository.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

}
