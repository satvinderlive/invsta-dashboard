/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.lambda.repo;

import crm.nz.beans.acc.api.NoteBean;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class NoteDetailsRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public NoteBean getNotesById(String noteId) {
        String Sql = "Select * from `notes` where active = 'y' AND id ='" + noteId + "' ";
        List<NoteBean> note = jdbcTemplate.query(Sql, new BeanPropertyRowMapper(NoteBean.class));
        return !note.isEmpty() ? note.get(0) : null;
    }

    public List<NoteBean> getNotes(String userId) {
        String Sql = "Select * from `notes` where active = 'y' AND created_by ='" + userId + "' ";
        List<NoteBean> notes = jdbcTemplate.query(Sql, new BeanPropertyRowMapper(NoteBean.class));
        return notes;
    }

    public void insertNoteDetails(Connection con, NoteBean note, HashMap response) throws SQLException {
        String innerSql = " INSERT INTO `notes`\n"
                + " (`note`,`related_to`,`created_ts`,`created_by`,`active`)\n"
                + " VALUES (?,?,?,?,'y');";
        PreparedStatement ps = con.prepareStatement(innerSql, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, note.getNote());
        ps.setString(2, note.getRelated_to());
        ps.setDate(3, new Date(new java.util.Date().getTime()));
        ps.setString(4, note.getCreated_by());
        int i = ps.executeUpdate();

    }

    public void updateNoteDetails(Connection con, NoteBean note, HashMap response) throws SQLException {
        String innerSql = " UPDATE `notes`\n"
                + " SET `note` = ?,`related_to` = ?,`created_ts` = ?,`created_by` = ?,`active` = 'y'\n"
                + " WHERE `id` = ?;";
        PreparedStatement ps = con.prepareStatement(innerSql, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, note.getNote());
        ps.setString(2, note.getRelated_to());
        ps.setDate(3, new Date(new java.util.Date().getTime()));
        ps.setString(4, note.getCreated_by());
        ps.setString(5, note.getId());
        int i = ps.executeUpdate();

    }

    public void deleteNoteDetails(Connection con, NoteBean note, HashMap response) throws SQLException {
        String innerSql = " DELETE FROM `notes`\n"
                + "WHERE id = ?;";
        PreparedStatement ps = con.prepareStatement(innerSql, Statement.RETURN_GENERATED_KEYS);
        String[] ids = note.getIds();
        for (String id : ids) {
            ps.setString(1, id);
            ps.addBatch();
        }
        int[] executeBatch = ps.executeBatch();

    }

    public void addNote(NoteBean note, HashMap response) throws SQLException {

        Connection con = jdbcTemplate.getDataSource().getConnection();

        try {
            insertNoteDetails(con, note, response);
            response.put("message", "You have Submited Note. Now you have received following Note(s).");
            response.put("status", 200);
        } catch (SQLException ex) {
            Logger.getLogger(NoteDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NoteDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void updateNote(NoteBean note, HashMap response) throws SQLException {
        Connection con = jdbcTemplate.getDataSource().getConnection();
        NoteBean notesById = getNotesById(note.getId());
        try {
            if (notesById != null) {
                updateNoteDetails(con, note, response);
            }
            response.put("message", "You have Updated Note. Now you have received following Note(s).");
            response.put("status", 200);
        } catch (SQLException ex) {
            Logger.getLogger(NoteDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NoteDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void deleteNote(NoteBean note, HashMap response) throws SQLException {
        Connection con = jdbcTemplate.getDataSource().getConnection();
        try {
            deleteNoteDetails(con, note, response);
            response.put("message", "You have Deleted Note. Now you have received following Note(s).");
            response.put("status", 200);
        } catch (SQLException ex) {
            Logger.getLogger(NoteDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NoteDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

}
