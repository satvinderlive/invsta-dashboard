/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.web;

import crm.nz.security.CurrentUserService;
import crm.nz.services.GetDataService;
import crm.nz.table.bean.SecuredUser;
import datadog.trace.api.Trace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController implements CurrentUserService {

    @Autowired
    private GetDataService dataService;

    @Trace
    @RequestMapping(value = {"/welcome"}, method = {RequestMethod.GET})
    public String welcome(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if (user.getSecret() != null && !user.getSecret().isEmpty() && !user.getVerification()) {
                return "use2fa";
            } else {
                modelMap.addAttribute("user", user);
                return "redirect:/home";
            }
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/home"}, method = {RequestMethod.GET})
    public String home(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("user", user);
            if ("ADMIN".equalsIgnoreCase(user.getRole())) {
                return "redirect:/admin-dashboard";
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                return "redirect:/advisor-dashboard";
            } else if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
                return "beneficiary-dashboard-db";
            } else if ("PENDING_USER".equalsIgnoreCase(user.getRole())) {
                return "redirect:/signUpPath";
            }
            return "home";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/home-{page}"}, method = {RequestMethod.GET})
    public String age(ModelMap modelMap, @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "userName", required = false) String userName,
            @PathVariable(value = "page") String page) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("id", UserId);
            modelMap.addAttribute("username", userName);

            return page;
    }
        return "redirect:/login";
    }

}
