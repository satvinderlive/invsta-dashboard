/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.web;

import crm.nz.beans.acc.api.AdvisorInvestmentBean;
import crm.nz.beans.acc.api.Portfolio;
import crm.nz.lambda.repo.AdvisorRepository;
import crm.nz.repository.PendingInvestmentRepository;
import crm.nz.repository.PendingTransactionRepository;
import crm.nz.repository.PortfolioRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import datadog.trace.api.Trace;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author TOSHIBA R830
 */
@Controller
public class AdminDashboardController implements CurrentUserService {

    @Autowired
    private PendingInvestmentRepository repository;
    @Autowired
    private PendingTransactionRepository Pendingrepository;
    @Autowired
    private PortfolioRepository portRepository;
    @Autowired
    private AdvisorRepository Advisorrepo;

    @Trace
    @RequestMapping(value = {"/admin-dashboard", "/advisor-dashboard"}, method = {RequestMethod.GET})
    public String admin(ModelMap modelMap, @RequestParam(value = "id", required = false) String id) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if ("ADMIN".equalsIgnoreCase(user.getRole())) {
                modelMap.addAttribute("user", user);
                modelMap.addAttribute("admin", "yes");
                List<Portfolio> allPortfolios = portRepository.getAllPortfolios();
                modelMap.addAttribute("allPortfolios", allPortfolios);
                return "new-advisor-dashboard";
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                if (id == null || id != null && id.isEmpty()) {
                    id = user.getUser_id();
                }
                modelMap.addAttribute("user", user);
                modelMap.addAttribute("id", id);
                modelMap.addAttribute("advisor", "no");
                List<Portfolio> allPortfolios = portRepository.getAllPortfolios();
                modelMap.addAttribute("allPortfolios", allPortfolios);
                List<AdvisorInvestmentBean> userInvestmentByAdvisor = Advisorrepo.getUserInvestmentByAdvisor(id);
                modelMap.addAttribute("userInvestments", userInvestmentByAdvisor);
                return "advisor-dashboard";
            } else if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
                return "redirect:/home-beneficiary-dashboard";
            }
            return "home";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/investment-{id}"}, method = {RequestMethod.GET})
    public String pendingInvestment(ModelMap modelMap, @PathVariable("id") String id) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            repository.acceptInvestmentRequest(id);
            return "redirect:/admin-dashboard";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/transection-{id}"}, method = {RequestMethod.GET})
    public String trnsaction(ModelMap modelMap, @PathVariable("id") String id) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            Pendingrepository.acceptTransaction(id);
            return "redirect:/admin-dashboard";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/new-advisor-dashboard"}, method = {RequestMethod.GET})
    public String NewAdvisorDashboard(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("id", user.getUser_id());
            modelMap.addAttribute("advisor", "no");
            List<Portfolio> allPortfolios = portRepository.getAllPortfolios();
            modelMap.addAttribute("allPortfolios", allPortfolios);
            List<AdvisorInvestmentBean> userInvestmentByAdvisor = Advisorrepo.getUserInvestmentByAdvisor(user.getUser_id());
            modelMap.addAttribute("userInvestments", userInvestmentByAdvisor);
            return "new-advisor-dashboard";

        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/cancelInvestment-{id}"}, method = {RequestMethod.GET})
    public String cancelInvestment(ModelMap modelMap, @PathVariable("id") String id) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            repository.cancelInvestment(id);
            return "redirect:/admin-dashboard";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/cancelTransection-{id}"}, method = {RequestMethod.GET})
    public String cancelTransection(ModelMap modelMap, @PathVariable("id") String id) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            Pendingrepository.cancelTransection(id);
            return "redirect:/admin-dashboard";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/advisor-dashboard-{id}"}, method = {RequestMethod.GET})
    public String advisor(ModelMap modelMap, @PathVariable("id") String id) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if (id == null || id != null && id.isEmpty()) {
                id = user.getUser_id();
            }
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("id", id);
            List<Portfolio> allPortfolios = portRepository.getAllPortfolios();
            modelMap.addAttribute("allPortfolios", allPortfolios);
            List<AdvisorInvestmentBean> userInvestmentByAdvisor = Advisorrepo.getUserInvestmentByAdvisor(id);
            modelMap.addAttribute("userInvestments", userInvestmentByAdvisor);
            return "advisor-dashboard";
        }

        return "redirect:/login";
    }

}
