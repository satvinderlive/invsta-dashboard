/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.web;

import crm.nz.beans.acc.api.Advisor;
import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.beans.acc.api.JointDetailBean;
import crm.nz.beans.acc.api.PersonDetailsBean;
import crm.nz.beans.acc.api.Register;
import crm.nz.components.CommonMethods;
import crm.nz.components.RandomStringGenerator;
import crm.nz.beans.acc.api.Occupation;
import crm.nz.lambda.repo.FormDetailsRepository;
import crm.nz.mail.SendMail;
import crm.nz.repository.CommonRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.security.SecurityService;
import crm.nz.services.DateTimeService;
import crm.nz.table.bean.SecuredUser;
import datadog.trace.api.Trace;
import java.io.IOException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author TOSHIBA R830
 */
@Controller
public class LoginRegisterController implements CurrentUserService {

    @Autowired
    private CommonRepository repository;
    @Autowired
    private RandomStringGenerator generator;
    @Autowired
    private FormDetailsRepository formDetailsRepository;
    @Autowired
    private SecurityService securityService;

    private static final Logger logger = LoggerFactory.getLogger(LoginRegisterController.class);

    private SecuredUser getPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof SecuredUser) {
                return ((SecuredUser) principal);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Trace
    @RequestMapping(value = {"", "/", "/haveAccount"}, method = RequestMethod.GET)
    public String empty() {
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String login(HttpServletRequest request, ModelMap modelMap,
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout) {
        logger.debug("Request URL :: " + request.getRequestURL().toString() + " :: Method Execute Start Time= " + System.currentTimeMillis());
        if (error != null) {
            modelMap.addAttribute("title", "Invalid credential");
            modelMap.addAttribute("message", "Invalid username and password!");
        }
        if (logout != null) {
            logout(repository);
            modelMap.addAttribute("title", "Logout");
            modelMap.addAttribute("message", "You have been logged out successfully.");
        }
        SecuredUser user = getPrincipal();
        if (user != null) {
            if (user.getSecret() != null && !user.getSecret().isEmpty() && !user.getVerification()) {
                return "use2fa";
            } else {
                return "redirect:/home";
            }
        }
        return "login";
    }

    @Trace
    @RequestMapping(value = {"/loggedin"}, method = RequestMethod.GET)
    public String loggedin() {
        return "redirect:/home-beneficiary-dashboard";
    }

    @Trace
    @RequestMapping(value = {"/joint-account"}, method = RequestMethod.GET)
    public String jointAccount(ModelMap modelMap){
            return "joint-account-new";
     }
    @Trace
    @RequestMapping(value = {"/minor-signup"}, method = RequestMethod.GET)
    public String minorsignup(ModelMap modelMap){
            return "minor-signup";
     }
        

    @Trace
    @RequestMapping(value = {"/company-signup"}, method = RequestMethod.GET)
    public String companySignup(ModelMap modelMap,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "password", required = false) String password) {
//        if (email != null && password != null) {
        modelMap.addAttribute("email", email);
        modelMap.addAttribute("password", password);
        return "company-signup-new";
//        }
//        return "redirect:/signUpPath";
    }

    @Trace
    @RequestMapping(value = {"/trust-signup"}, method = RequestMethod.GET)
    public String trustSignup(ModelMap modelMap) {
        
            return "trust-signup-new";
       
    }

    @Trace
    @RequestMapping(value = {"/signUpPath"}, method = RequestMethod.POST)//it is for invite code
    public String register(ModelMap modelMap, @ModelAttribute Register register) throws SQLException, IOException {
        String token = generator.generate(30);
        register.setToken(token);
        int result = repository.register(register);
        if (result == 1) {
            securityService.autologin(register.getEmail(), register.getPassword());
        }
        return "redirect:/signUpPath";
    }

    @Trace
    @RequestMapping(value = {"/signUpPath"}, method = RequestMethod.GET)//it is for invite code
    public String signUpPath(ModelMap modelMap) throws SQLException, IOException {
        SecuredUser user = getCurrentUser();
        List<Advisor> advisories = formDetailsRepository.getAdvisories();
        modelMap.addAttribute("advisories", advisories);
        List<Occupation> occupation = formDetailsRepository.getOccupation();
        modelMap.addAttribute("occupation", occupation);
        if (user != null && user.getToken() != null && user.getToken().length() == 30) {
            final String token = user.getToken();
            modelMap.addAttribute("token", token);
            PersonDetailsBean person = formDetailsRepository.getPersonDetailByToken(token);
            if (person != null && person.getReg_type() != null && "INDIVIDUAL_ACCOUNT".equalsIgnoreCase(person.getReg_type())) {
                modelMap.addAttribute("person", person);
                return "signup";
            } else if(person != null && person.getReg_type() != null && "MINOR_ACCOUNT".equalsIgnoreCase(person.getReg_type())){
                modelMap.addAttribute("person", person);
                return "minor-signup";
            }else {
                JointDetailBean joint = formDetailsRepository.getJointDetailByToken(token);
                if (joint != null && joint.getReg_type() != null && "JOINT_ACCOUNT".equalsIgnoreCase(joint.getReg_type())) {
                    modelMap.addAttribute("joint", joint);
                    return "joint-account-new";
                } else {
                    CompanyDetailBean company = formDetailsRepository.getCompanyDetailByToken(token);
                    if (company != null && company.getReg_type() != null && "COMPANY_ACCOUNT".equalsIgnoreCase(company.getReg_type())) {
                        modelMap.addAttribute("company", company);
                        return "company-signup-new";
                    } else if (company != null && company.getReg_type() != null && "TRUST_ACCOUNT".equalsIgnoreCase(company.getReg_type())) {
                        modelMap.addAttribute("company", company);
                        return "trust-signup-new";
                    }
                }
                return "signup";
            }
        } else {
            return "signup";
        }
    }

    @Trace
    @RequestMapping(value = {"/signUpPath-{token}"}, method = RequestMethod.GET)//it is from link
    public String verifyNew1(ModelMap modelMap,
            @PathVariable(value = "token") String tkn) throws SQLException, IOException {
        final String token = tkn;
        modelMap.addAttribute("token", token);
        Register register = formDetailsRepository.getRegisterDetailsByToken(token);
        securityService.autologin(register.getEmail(), register.getPassword());
        return "redirect:/signUpPath";
    }

}
