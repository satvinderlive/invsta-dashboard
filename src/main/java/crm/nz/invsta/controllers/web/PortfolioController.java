/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.web;

import com.amazonaws.services.s3.model.S3Object;
import crm.nz.beans.acc.api.BankAccountDetail;
import crm.nz.beans.acc.api.Beneficiary;
import crm.nz.beans.acc.api.FmcaInvestmentMix;
import crm.nz.beans.acc.api.FmcaTopAssets;
import crm.nz.beans.acc.api.InvestmentPdfBean;
import crm.nz.beans.acc.api.InvestmentTeamBean;
import crm.nz.beans.acc.api.PerformanceBean;
import crm.nz.beans.acc.api.Portfolio;
import crm.nz.beans.acc.api.PortfolioDetail;
import crm.nz.components.CommonMethods;
import crm.nz.components.FileMethods;
import crm.nz.repository.BeneficiaryRepository;
import crm.nz.repository.PortfolioRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.services.GetDataService;
import crm.nz.table.bean.SecuredUser;
import datadog.trace.api.Trace;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author innovative002
 */
@Controller
public class PortfolioController implements CurrentUserService {

    @Autowired
    private PortfolioRepository portRepository;
    @Autowired
    private BeneficiaryRepository beneficiaryRepository;
    @Autowired
    private GetDataService dataService;
    @Autowired
    private FileMethods fileMethods;
    @Autowired
    private CommonMethods common;

    @Trace
    @RequestMapping(value = {"/home-investment", "investment"}, method = {RequestMethod.GET})
    public String Investment(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            List<Portfolio> allPortfolios = portRepository.getAllPortfolios();
            modelMap.addAttribute("allPortfolios", allPortfolios);
            modelMap.addAttribute("user", user);
            return "investment";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/investment-fund-{pc}", "/portfolio-{pc}"}, method = {RequestMethod.GET})
    public String InvestmentFund(ModelMap modelMap,
            @PathVariable("pc") String pc,
            @RequestParam(value = "id", required = false) String UserId) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            PortfolioDetail portfolioDetail = portRepository.getPortfolioDetail(pc);
            List<PortfolioDetail> otherPortfolioDetail = portRepository.getOtherPortfolioDetail(pc);
            List<FmcaTopAssets> fmcaTopAssets = portRepository.getFmcaTopAssets(pc);
            List<FmcaInvestmentMix> totalFmcaInvestmentMix = portRepository.getTotalFmcaInvestmentMix(pc);
            List<InvestmentTeamBean> investmentTeam = portRepository.getInvestmentTeam(pc);
            List<InvestmentPdfBean> investmentPdf = portRepository.getInvestmentPdf(pc);
            List<InvestmentPdfBean> quarterlyPdf = portRepository.getQuarterlyPdf(pc);
            List<PerformanceBean> performanceSinceinception = portRepository.getPerformanceSinceinception(pc);
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("investmentTeam", investmentTeam);
            modelMap.addAttribute("performanceSinceinception", performanceSinceinception);
            modelMap.addAttribute("fmcaTopAssets", fmcaTopAssets);
            modelMap.addAttribute("totalFmcaInvestmentMix", totalFmcaInvestmentMix);
            modelMap.addAttribute("applicationId", user.getApplication_id());
            modelMap.addAttribute("id", UserId);
            modelMap.addAttribute("pc", pc);
            modelMap.addAttribute("portfolioDetail", portfolioDetail);
            modelMap.addAttribute("otherPortfolioDetail", otherPortfolioDetail);
            modelMap.addAttribute("investmentPdf", investmentPdf);
            modelMap.addAttribute("quarterlyPdf", quarterlyPdf);
            return "portfolio";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/new-portfolio-{pc}"}, method = {RequestMethod.GET})
    public String NewInvestmentFund(ModelMap modelMap,
            @PathVariable("pc") String pc,
            @RequestParam(value = "id", required = false) String UserId) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);

            PortfolioDetail portfolioDetail = portRepository.getPortfolioDetail(pc);
            List<PortfolioDetail> otherPortfolioDetail = portRepository.getOtherPortfolioDetail(pc);
            List<FmcaTopAssets> fmcaTopAssets = portRepository.getFmcaTopAssets(pc);
            List<FmcaInvestmentMix> totalFmcaInvestmentMix = portRepository.getTotalFmcaInvestmentMix(pc);
            List<InvestmentTeamBean> investmentTeam = portRepository.getInvestmentTeam(pc);
            List<InvestmentPdfBean> investmentPdf = portRepository.getInvestmentPdf(pc);
            List<PerformanceBean> performanceSinceinception = portRepository.getPerformanceSinceinception(pc);
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("investmentTeam", investmentTeam);
            modelMap.addAttribute("performanceSinceinception", performanceSinceinception);
            modelMap.addAttribute("fmcaTopAssets", fmcaTopAssets);
            modelMap.addAttribute("totalFmcaInvestmentMix", totalFmcaInvestmentMix);
            modelMap.addAttribute("applicationId", user.getApplication_id());
            modelMap.addAttribute("id", UserId);
            modelMap.addAttribute("pc", pc);
            modelMap.addAttribute("portfolioDetail", portfolioDetail);
            modelMap.addAttribute("otherPortfolioDetail", otherPortfolioDetail);
            modelMap.addAttribute("investmentPdf", investmentPdf);
            return "new-portfolio";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/make-investment-{pc}"}, method = {RequestMethod.GET})
    public String makeInvestment(ModelMap modelMap,
            @PathVariable("pc") String pc,
            @RequestParam(value = "id", required = false) String UserId) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);

            List<Beneficiary> beneficiaryDetails = beneficiaryRepository.getBeneficiaryDetails(UserId, null, null);
            List<BankAccountDetail> bankAccountDetails = beneficiaryRepository.getBankAccountDetails(UserId, null);
            modelMap.addAttribute("beneficiaryDetails", beneficiaryDetails);
            modelMap.addAttribute("bankAccountDetails", bankAccountDetails);
            modelMap.addAttribute("user", user);
            return "new-payment-changes";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/update-portfolio-{pc}"}, method = {RequestMethod.GET})
    public String InvestmentFund(ModelMap modelMap,
            @PathVariable("pc") String portfolioCode) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            PortfolioDetail portfolioDetail = portRepository.getPortfolioDetail(portfolioCode);
            List<PortfolioDetail> otherPortfolioDetail = portRepository.getOtherPortfolioDetail(portfolioCode);
            List<FmcaTopAssets> fmcaTopAssets = portRepository.getFmcaTopAssets(portfolioCode);
            List<FmcaInvestmentMix> totalFmcaInvestmentMix = portRepository.getTotalFmcaInvestmentMix(portfolioCode);
            List<InvestmentTeamBean> investmentTeam = portRepository.getInvestmentTeam(portfolioCode);
            List<InvestmentPdfBean> investmentPdf = portRepository.getInvestmentPdf(portfolioCode);
            List<PerformanceBean> performanceSinceinception = portRepository.getPerformanceSinceinception(portfolioCode);

            modelMap.addAttribute("user", user);
            modelMap.addAttribute("pc", portfolioCode);
            modelMap.addAttribute("portfolioDetail", portfolioDetail);
            modelMap.addAttribute("fmcaTopAssets", fmcaTopAssets);
            modelMap.addAttribute("totalFmcaInvestmentMix", totalFmcaInvestmentMix);
            modelMap.addAttribute("otherPortfolioDetail", otherPortfolioDetail);
            modelMap.addAttribute("investmentTeam", investmentTeam);
            modelMap.addAttribute("investmentPdf", investmentPdf);
            modelMap.addAttribute("performanceSinceinception", performanceSinceinception);
            return "portfolio-editable";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/update-PortfolioDetail"}, method = {RequestMethod.POST})
    public String UpdatePortfolioDetail(ModelMap modelMap, @ModelAttribute PortfolioDetail bean) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
//            String abc=bean;
            System.out.println("UpdatePortfolioDetail-------->" + bean);

            MultipartFile multipartFile = bean.getPortfolio_image();
            if (multipartFile.getSize() > 0) {
                System.out.println("if !multipartFile.isEmpty()");
                String ext = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
                String name = FilenameUtils.removeExtension(multipartFile.getOriginalFilename()) + common.today().getTime();
                S3Object s3Object = fileMethods.storeTos3Pics(multipartFile, bean.getPortfolio(), name, ext);
                String url = s3Object.getObjectContent().getHttpRequest().getURI().toString();
                bean.setPortfolio_pic(url);
            } else {

                System.out.println("else multipartFile.isEmpty()");
            }

            portRepository.setPortfolioDetail(bean);
            portRepository.setPortfolio(bean);
            return "redirect:/update-portfolio-" + bean.getPortfolio();
        }
        return "error";
    }

    @Trace
    @RequestMapping(value = {"/UpdatefmcaTopAsset"}, method = {RequestMethod.POST})
    public String UpdatefmcaTopAsset(ModelMap modelMap, @ModelAttribute FmcaTopAssets bean) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            System.out.println("UpdatePortfolioDetail-------->" + bean);
            portRepository.setFmcaTopAssets(bean);
            return "redirect:/update-portfolio-" + bean.getPortfolio();
        }
        return "error";
    }

    @Trace
    @RequestMapping(value = {"/UpdateTotalFmcaInvestmentMix"}, method = {RequestMethod.POST})
    public String UpdateTotalFmcaInvestmentMix(ModelMap modelMap, @ModelAttribute FmcaTopAssets bean) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            System.out.println("UpdatePortfolioDetail-------->" + bean);
            portRepository.setTotalFmcaInvestmentMix(bean);
            return "redirect:/update-portfolio-" + bean.getPortfolio();
        }
        return "error";
    }

    @Trace
    @RequestMapping(value = {"/UpdateInvestmentTeam"}, method = {RequestMethod.POST})
    public String UpdateInvestmentTeam(@ModelAttribute InvestmentTeamBean bean,
            @RequestPart("image_file") MultipartFile[] files) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if (files != null && files.length > 0) {
                System.out.println("files---->" + files);
                System.out.println("files.length---->" + files.length);
                LinkedList<InvestmentTeamBean> teamBeans = new LinkedList<>();
                String[] names = bean.getName().split(",");
                String[] places = bean.getPlace().split(",");
                String portfolio = bean.getPortfolio();
                String bio = bean.getTeam_bio();
                String[] pic_urls = bean.getPic_url().split(",");
                for (int i = 0; i < files.length; i++) {
                    InvestmentTeamBean teamBean = new InvestmentTeamBean();
                    teamBean.setName(names[i]);
                    teamBean.setPlace(places[i]);
                    teamBean.setPortfolio(portfolio);
                    teamBean.setPic_url(pic_urls[i]);
                    teamBean.setTeam_bio(bio);
                    MultipartFile multipartFile = files[i];
                    if (multipartFile.getSize() > 0) {
                        System.out.println("if !multipartFile.isEmpty()" + i);
                        String ext = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
                        String name = FilenameUtils.removeExtension(multipartFile.getOriginalFilename()) + common.today().getTime();
                        S3Object s3Object = fileMethods.storeTos3Pics(multipartFile, portfolio, name, ext);
                        String url = s3Object.getObjectContent().getHttpRequest().getURI().toString();
                        teamBean.setPic_url(url);
                    } else {
                        System.out.println("else multipartFile.isEmpty()" + i);
                    }
                    teamBeans.add(teamBean);
                }
                portRepository.saveOrUpdateInvestmentTeam(teamBeans, portfolio);
            }
            return "redirect:/update-portfolio-" + bean.getPortfolio();
        }
        return "error";
    }

    @Trace
    @RequestMapping(value = {"/UpdateInvestmentPdf"}, method = {RequestMethod.POST})
    public String UpdateInvestmentPdf(@ModelAttribute InvestmentPdfBean bean,
            @RequestPart("pdf_file") MultipartFile[] files) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if (files != null && files.length > 0) {
                System.out.println("files---->" + files);
                LinkedList<InvestmentPdfBean> pdfBeans = new LinkedList<>();
                String[] names = bean.getPdf_name().split(",");
//                String[] places = bean.getPlace().split(",");
                String portfolio = bean.getPortfolio();
                String[] pdf_urls = bean.getPdf_url().split(",");
                for (int i = 0; i < files.length; i++) {
                    InvestmentPdfBean pdfBean = new InvestmentPdfBean();
                    pdfBean.setPdf_name(names[i]);
//                    teamBean.setPlace(places[i]);
                    pdfBean.setPortfolio(portfolio);
                    pdfBean.setPdf_url(pdf_urls[i]);
                    MultipartFile multipartFile = files[i];
                    if (multipartFile.getSize() > 0) {
                        System.out.println("!multipartFile.isEmpty()" + i);
                        String ext = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
                        String name = FilenameUtils.removeExtension(multipartFile.getOriginalFilename()) + common.today().getTime();
                        S3Object s3Object = fileMethods.storeTos3Pics(multipartFile, portfolio, name, ext);
                        String url = s3Object.getObjectContent().getHttpRequest().getURI().toString();
                        pdfBean.setPdf_url(url);
                    } else {
                        System.out.println("multipartFile.isEmpty()" + i);
                    }
                    pdfBeans.add(pdfBean);
                }
                portRepository.saveOrUpdatePdfFile(pdfBeans, portfolio);
            }
            return "redirect:/update-portfolio-" + bean.getPortfolio();
        }
        return "error";
    }

}
