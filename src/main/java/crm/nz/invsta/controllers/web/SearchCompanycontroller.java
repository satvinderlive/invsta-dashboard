/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.web;

import crm.nz.authorization.GetNZBNTokenService;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import datadog.trace.api.Trace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author TOSHIBA R830
 */
@Controller
public class SearchCompanycontroller implements CurrentUserService {

    @Trace
    @RequestMapping(value = {"/search-company"}, method = {RequestMethod.GET})
    public String searchcompany(ModelMap modelMap) {
//        SecuredUser user = getCurrentUser();
//        String clientCredentials = auth2TokenService.getClientCredentials();
//        System.out.println("clientCredentials-----" + clientCredentials);
        modelMap.addAttribute("token", "3c595b269b2d0d5c382c86037de0b9d7");
        return "search-company";

    }
}
