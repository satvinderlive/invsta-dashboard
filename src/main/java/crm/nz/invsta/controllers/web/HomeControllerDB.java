/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.web;

import com.google.gson.Gson;
import crm.nz.authorization.BeneficiariesService;
import crm.nz.authorization.GetOAuth2TokenService;
import crm.nz.authorization.InvestmentsService;
import crm.nz.beans.acc.api.Beneficiary;
import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.beans.acc.api.JointDetailBean;
import crm.nz.beans.acc.api.PersonDetailsBean;
import crm.nz.lambda.repo.FormDetailsRepository;
import crm.nz.repository.BeneficiaryRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import datadog.trace.api.Trace;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeControllerDB implements CurrentUserService {

    @Autowired
    GetOAuth2TokenService auth2TokenService;
    @Autowired
    BeneficiariesService beneficiariesService;
    @Autowired
    InvestmentsService investmentsService;
    @Autowired
    BeneficiaryRepository beneficiaryRepository;
    @Autowired
    FormDetailsRepository formDetailsRepository;
    @Autowired
    Gson gson;

    @Trace
    @RequestMapping(value = {"/home-beneficiary-db"}, method = {RequestMethod.GET})
    public String beneficiaryProfileFromDB(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String id,
            @RequestParam(value = "bi", required = false) String bi) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
                id = user.getUser_id();
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                if (id == null || id != null && id.isEmpty()) {
                    id = user.getUser_id();
                }
            }
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("id", id);
            modelMap.addAttribute("bi", bi);
            return "beneficary-db";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/home-beneficiary-dashboard-db"}, method = {RequestMethod.GET})
    public String beneficiaryDashboardFromDB(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String id,
            @RequestParam(value = "name", required = false) String userName,
            @RequestParam(value = "ic", required = false) String ic
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
                if (id == null || id != null && id.isEmpty()) {
                    id = user.getUser_id();
                }
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("id", id);
            modelMap.addAttribute("userName", userName);
            modelMap.addAttribute("ic", ic);
            return "beneficiary-dashboard-db";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/show-me-more-db-{ic}"}, method = {RequestMethod.GET})
    public String showmemore(ModelMap modelMap,
            @PathVariable("ic") String InvestmentCode,
            @RequestParam("pc") String PortfolioCode) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("ic", InvestmentCode);
            modelMap.addAttribute("pc", PortfolioCode);
            return "show-me-more-db";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/profile-db"}, method = {RequestMethod.GET})
    public String getBenficaryIDs(ModelMap modelMap,
            @RequestParam("id") String id) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
                id = user.getUser_id();
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                if (id == null || id != null && id.isEmpty()) {
                    id = user.getUser_id();
                }
            }
            System.out.println("userId" + id);
            List<Beneficiary> beneficiaryDetails = beneficiaryRepository.getBeneficiaryDetails(id, null, null);
            modelMap.addAttribute("beneficiaryDetails", beneficiaryDetails);
            modelMap.addAttribute("user", user);
            return "beneficary-db";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/home-beneficiary-transactions-db"}, method = {RequestMethod.GET})
    public String beneficaryTransactions(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String id) {//bounce
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
                id = user.getUser_id();
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                if (id == null || id != null && id.isEmpty()) {
                    id = user.getUser_id();
                }
            }
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("id", id);
            return "beneficary-transactions-db";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/home-reg-details-db-{tkn}", "/reg-details-db-{tkn}"}, method = {RequestMethod.GET})
    public String details(ModelMap modelMap,
            @PathVariable("tkn") String token) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("token", token);
            PersonDetailsBean person = formDetailsRepository.getPersonDetailByToken(token);
            if (person != null) {
                modelMap.addAttribute("person", person);
            } else {
                JointDetailBean joint = formDetailsRepository.getJointDetailByToken(token);
                if (joint != null) {
                    modelMap.addAttribute("joint", joint);
                } else {
                    CompanyDetailBean company = formDetailsRepository.getCompanyDetailByToken(token);
                    if (company != null) {
                        modelMap.addAttribute("company", company);
                    }
                }
            }
            return "reg-details-db";
        }
        return "redirect:/login";
    }
    @Trace
    @RequestMapping(value = {"/payment-enterpayment-new"}, method = {RequestMethod.GET})
    public String paymentEnterpayment(ModelMap modelMap )           {
        
            return "payment-enterpayment-new";
      
    }

}
