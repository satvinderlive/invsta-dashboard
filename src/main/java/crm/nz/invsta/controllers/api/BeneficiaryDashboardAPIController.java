/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import com.google.gson.Gson;
import crm.nz.authorization.BeneficiariesService;
import crm.nz.authorization.GetOAuth2TokenService;
import crm.nz.authorization.InvestmentsService;
import crm.nz.authorization.TransactionsService;
import crm.nz.beans.acc.api.Address;
import crm.nz.beans.acc.api.BankAccountDetail;
import crm.nz.beans.acc.api.Beneficiary;
import crm.nz.beans.acc.api.Email;
import crm.nz.beans.acc.api.FmcaInvestmentMix;
import crm.nz.beans.acc.api.Identification;
import crm.nz.beans.acc.api.InvestmentHolding;
import crm.nz.beans.acc.api.InvestmentPerformance;
import crm.nz.beans.acc.api.PhoneNumber;
import crm.nz.repository.BeneficiaryRepository;
import crm.nz.repository.InvestmentRepository;
import crm.nz.repository.TransactionRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ADMIN
 */
@RestController
@RequestMapping(value = "/rest/groot/db/api")
public class BeneficiaryDashboardAPIController implements CurrentUserService {

    @Autowired
    private GetOAuth2TokenService oAuth2TokenService;
    @Autowired
    private BeneficiariesService beneficiariesService;
    @Autowired
    private InvestmentsService investmentsService;
    @Autowired
    private TransactionsService transactionsService;
    @Autowired
    private BeneficiaryRepository beneficiaryrepository;
    @Autowired
    private InvestmentRepository investrepository;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private Gson gson;

    @RequestMapping(value = {"/beneficiaries"}, method = {RequestMethod.GET})
    @ResponseBody
    public String allBeneficiariesToDB() {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if ("ADMIN".equalsIgnoreCase(user.getRole())) {
                List<Beneficiary> beneficiaries = beneficiaryrepository.getBeneficiaryDetails(null, null, null);
                return "{"
                        + "\"beneficiaries\":" + beneficiaries.toString()
                        + "}";
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                List<Beneficiary> beneficiaries = beneficiaryrepository.getBeneficiaryDetails(null, null, user.getUser_id());
                return "{"
                        + "\"beneficiaries\":" + beneficiaries.toString()
                        + "}";
            } else if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
                return "{\"message\":\"Sorry\"}";
            }

//            if (beneficiaries.size() < 5) {
//                String clientCredentials = oAuth2TokenService.getClientCredentials();
//                Type type = null;
//                boolean resultEqualsTo = true;
//                Integer page = 1, clientSize = 25;
//                type = new TypeToken<LinkedList<Beneficiary>>() {
//                }.getType();
//                LinkedList<Beneficiary> beneficiaryList = new LinkedList<>();
//                while (resultEqualsTo) {
//                    try {
//                        StringBuilder result0 = beneficiariesService.beneficiaries(clientCredentials, page, clientSize);
//                        LinkedList<Beneficiary> list = gson.fromJson(result0.toString(), type);
//                        System.out.println(page + " @ pageIndex List.size" + list.size());
//                        beneficiaryList.addAll(list);
//                        if (clientSize != list.size()) {
//                            resultEqualsTo = false;
//                        }
//                    } catch (JsonSyntaxException ex) {
//                        ex.printStackTrace();
//                    }
//                    page++;
//                }
//                beneficiaryrepository.saveBeneficiaries(beneficiaryList);
//                beneficiaries = beneficiaryList;
//            }
        }
        return "{}";
    }

    @RequestMapping(value = {"/beneficiary-{UserId}-{BeneficiaryId}"}, method = {RequestMethod.GET})
    @ResponseBody
    public String beneficiaryById(ModelMap modelMap,
            @PathVariable(value = "UserId") String UserId,
            @PathVariable(value = "BeneficiaryId") String BeneficiaryId) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
                UserId = user.getUser_id();
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                if (UserId == null || UserId != null && UserId.isEmpty()) {
                    UserId = user.getUser_id();
                }
            }
            Beneficiary beneficiary = beneficiaryrepository.getBeneficiary(BeneficiaryId);
            List<Address> addresses = beneficiaryrepository.getAddresses(UserId, BeneficiaryId);
            List<Email> emails = beneficiaryrepository.getEmails(UserId, BeneficiaryId);
            List<PhoneNumber> phoneNumber = beneficiaryrepository.getPhoneNumbers(UserId, BeneficiaryId);
            List<Identification> identifications = beneficiaryrepository.getIdentifications(UserId, BeneficiaryId);
            List<BankAccountDetail> bankAccountDetails = beneficiaryrepository.getBankAccountDetails(UserId, BeneficiaryId);
//            if (identifications == null || identifications.isEmpty()) {
//                String clientCredentials = oAuth2TokenService.getClientCredentials();
//                StringBuilder beneficiary1 = beneficiariesService.beneficiaryById(clientCredentials, id);
//                beneficiary = gson.fromJson(beneficiary1.toString(), Beneficiary.class);
//                beneficiaryrepository.saveEmails(beneficiary.getEmails(), beneficiary.getId());
//                beneficiaryrepository.savePhoneNumbers(beneficiary.getPhoneNumbers(), beneficiary.getId());
//                beneficiaryrepository.saveAddresses(beneficiary.getAddresses(), beneficiary.getId());
//                beneficiaryrepository.saveIdentifications(beneficiary.getIdentification(), beneficiary.getId());
//                addresses = beneficiary.getAddresses();
//                emails = beneficiary.getEmails();
//                identifications = beneficiary.getIdentification();
//                phoneNumber = beneficiary.getPhoneNumbers();
//            } 
            if (BeneficiaryId != null) {
                return "{"
                        + "\"addresses\":" + addresses.toString()
                        + ",\"beneficiary\":" + beneficiary.toString()
                        + ",\"emails\":" + emails.toString()
                        + ",\"bankAccountDetails\":" + bankAccountDetails.toString()
                        + ",\"phoneNumber\":" + phoneNumber.toString()
                        + ",\"identifications\":" + identifications.toString()
                        + "}";
            }
        }

        return "{}";
    }

    @RequestMapping(value = {"/beneficiary-dashboard"}, method = {RequestMethod.GET})
    @ResponseBody
    public String beneficiaryDashboardDB(
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String BeneficiaryId,
            @RequestParam(value = "ic", required = false) String InvestmentCode,
            @RequestParam(value = "pc", required = false) String PortfolioCode
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
                UserId = user.getUser_id();
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                if (UserId == null || UserId != null && UserId.isEmpty()) {
                    UserId = user.getUser_id();
                }
            } else if ("ADMIN".equalsIgnoreCase(user.getRole())) {
                if (UserId == null || UserId != null && UserId.isEmpty()) {
                    UserId = user.getUser_id();
                }
            }
            if (UserId != null) {
                List<InvestmentHolding> investmentHoldings = investrepository.getInvestmentHoldings(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
                List<InvestmentPerformance> investmentPerformances = investrepository.getInvestmentPerformances(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
                List<FmcaInvestmentMix> fmcaInvestmentMix = investrepository.getTotalFmcaInvestmentMix(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
                List<FmcaInvestmentMix> fmcaInvestmentMixByTable = investrepository.getTotalFmcaInvestmentMixByTable(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
                return "{"
                        + "\"investmentHoldings\":" + investmentHoldings.toString()
                        + ",\"investmentPerformances\":" + investmentPerformances.toString()
                        + ",\"fmcaInvestmentMixByTable\":" + fmcaInvestmentMixByTable.toString()
                        + ",\"fmcaInvestmentMix\":" + fmcaInvestmentMix.toString()
                        + "}";
            }
        }
        return null;
    }

//    @RequestMapping(value = {"/beneficiary-dashboard"}, method = {RequestMethod.GET})
//    @ResponseBody
//    public String beneficiaryDashboardDB(
//            @RequestParam(value = "id", required = false) String id) {
//        SecuredUser user = getCurrentUser();
//        if (user != null) {
//            if (id == null || id != null && id.isEmpty()) {
//                id = user.getUser_id();
//            }
//            if (id != null) {
//                Beneficiary beneficiary = beneficiaryrepository.getBeneficiary(id);
//                Type type = null;
//                List<InvestmentHolding> investmentHoldings = null;
//                List<InvestmentPerformance> investmentPerformances = null;
//                List<FmcaInvestmentMix> fmcaInvestmentMix = null;
//                if (beneficiary != null) {
//                    investmentHoldings = investrepository.getInvestmentHoldings(id, null);
//                    investmentPerformances = investrepository.getInvestmentPerformances(id, null);
//                    fmcaInvestmentMix = investrepository.getTotalFmcaInvestmentMix(id, null, null);
//                } else {
//                    final String BeneficiaryId = id;
//                    String clientCredentials = oAuth2TokenService.getClientCredentials();
//                    List<Investment> investments = investrepository.getInvestments(BeneficiaryId);
//                    if (investments == null || investments.isEmpty()) {
//                        StringBuilder result0 = beneficiariesService.beneficiaryById(clientCredentials, id);
//                        beneficiary = gson.fromJson(result0.toString(), Beneficiary.class);
////                    beneficiaryrepository.saveBeneficiary(beneficiary);
//                        investments = beneficiary.getInvestments();
//                        investments.forEach((Investment inv) -> {
//                            inv.setBeneficiaryId(BeneficiaryId);
//                        });
//                        investrepository.saveInvestment(investments);
//                    }
//                    for (Investment investment : investments) {
//                        final String InvestmentCode = investment.getCode();
//                        StringBuilder result2 = investmentsService.investmentHoldingSummaryValues(clientCredentials, InvestmentCode);//we need request for this investment holding summary
//                        type = new TypeToken<LinkedList<InvestmentHolding>>() {
//                        }.getType();
//                        investmentHoldings = gson.fromJson(result2.toString(), type);
//                        investmentHoldings.forEach((InvestmentHolding inv) -> {
//                            inv.setBeneficiaryId(BeneficiaryId);
//                        });
//                        investrepository.saveInvestmentHolding(investmentHoldings);
//                        StringBuilder result1 = investmentsService.investmentPerformance(clientCredentials, InvestmentCode);
//                        type = new TypeToken<LinkedList<InvestmentPerformance>>() {
//                        }.getType();
//                        investmentPerformances = gson.fromJson(result1.toString(), type);
//                        investmentPerformances.forEach((InvestmentPerformance inv) -> {
//                            inv.setBeneficiaryId(BeneficiaryId);
//                            inv.setInvestmentCode(InvestmentCode);
//                        });
//                        investrepository.saveInvestmentPerformance(investmentPerformances);
//                        type = new TypeToken<LinkedList<FmcaInvestmentMix>>() {
//                        }.getType();
//                        for (InvestmentHolding holding : investmentHoldings) {
//                            StringBuilder result3 = investmentsService.fmcaInvestmentMix(clientCredentials, holding.getInvestmentCode(), holding.getPortfolioCode());
//                            fmcaInvestmentMix = gson.fromJson(result3.toString(), type);
//                            fmcaInvestmentMix.forEach((FmcaInvestmentMix inv) -> {
//                                inv.setBeneficiaryId(BeneficiaryId);
//                                inv.setInvestmentCode(InvestmentCode);
//                            });
//                            investrepository.saveFmcaInvestmentMix((List<FmcaInvestmentMix>) fmcaInvestmentMix);
//                        }
//                    }
//                    fmcaInvestmentMix = investrepository.getTotalFmcaInvestmentMix(id, null, null);
//                }
//                if (beneficiary != null) {
//                    return "{"
//                            + "\"investmentHoldings\":" + investmentHoldings.toString()
//                            + ",\"investmentPerformances\":" + investmentPerformances.toString()
//                            + ",\"fmcaInvestmentMix\":" + fmcaInvestmentMix.toString()
//                            + "}";
//                }
//            }
//        }
//        return null;
//    }    
//    @RequestMapping(value = {"/investments"}, method = {RequestMethod.GET})
//    @ResponseBody
//    public String investmentsDB(@RequestParam(value = "id", required = false) String beneficiary_id) {
//        SecuredUser user = getCurrentUser();
//        if (user != null) {
//            if (beneficiary_id == null || beneficiary_id != null && beneficiary_id.isEmpty()) {
//                beneficiary_id = user.getUser_id();
//            }
//            if (beneficiary_id != null) {
//                List<Investment> investments = investrepository.getInvestments(beneficiary_id);
//                if (investments.isEmpty()) {
//                    final String BeneficiaryId = beneficiary_id;
//                    String clientCredentials = oAuth2TokenService.getClientCredentials();
//                    StringBuilder result0 = beneficiariesService.beneficiaryById(clientCredentials, BeneficiaryId);
//                    Beneficiary beneficiary = gson.fromJson(result0.toString(), Beneficiary.class);
////                    beneficiaryrepository.saveBeneficiary(beneficiary);
//                    investments = beneficiary.getInvestments();
//                    investments.forEach((Investment inv) -> {
//                        inv.setBeneficiaryId(BeneficiaryId);
//                    });
//                    investrepository.saveInvestment(investments);
//                }
//                return "{"
//                        + "\"investments\":" + investments.toString()
//                        + "}";
//            }
//        }
//        return "{}";
//    }
}
