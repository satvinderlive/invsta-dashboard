/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import crm.nz.authorization.GetOAuth2TokenService;
import crm.nz.authorization.OnboardingsServices;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author TOSHIBA R830
 */
@Controller
public class ApplicationController implements CurrentUserService {
    

    @Autowired
    private GetOAuth2TokenService oAuth2TokenService;
  
    @Autowired
    private OnboardingsServices onboardingsServices;
    
    
      @RequestMapping(value = {"/delete-application-{id}"}, method = {RequestMethod.GET})
    public String deleteApplicaion(ModelMap modelMap,@PathVariable(value = "id") String applicationId) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            String clientCredentials = oAuth2TokenService.getClientCredentials();
             onboardingsServices.deleteApplicationById(clientCredentials,applicationId);
            return "redirect:/admin-dashboard";
        }
        return "redirect:/login";
    }
}
