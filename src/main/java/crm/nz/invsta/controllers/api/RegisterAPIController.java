package crm.nz.invsta.controllers.api;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.google.gson.Gson;
import crm.nz.authorization.DataZooService;
import crm.nz.lambda.repo.FormDetailsRepository;
import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.beans.acc.api.JointDetailBean;
import crm.nz.beans.acc.api.PersonDetailsBean;
import crm.nz.beans.acc.api.Register;
import crm.nz.beans.acc.api.ResponseBean;
import crm.nz.components.CommonMethods;
import crm.nz.components.ObjectCastManager;
import crm.nz.components.RandomStringGenerator;
import crm.nz.mail.SendMail;
import crm.nz.security.CurrentUserService;
import crm.nz.services.ThirdAPIRegisterService;
import crm.nz.table.bean.SecuredUser;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ADMIN
 */
@RestController
@RequestMapping(value = "/rest/groot/db/api")
public class RegisterAPIController implements CurrentUserService {

    @Autowired
    private SendMail sendMail;
    @Autowired
    private RandomStringGenerator generator;
    @Autowired
    private ThirdAPIRegisterService registerService;
    @Autowired
    private FormDetailsRepository formDetailsRepository;
    @Autowired
    private DataZooService datazooservice;
    @Autowired
    CommonMethods common;

    Gson gson = new Gson();

    @RequestMapping(value = {"/get-pending-registration"}, method = RequestMethod.GET)
    @ResponseBody
    public String getPendingRegisterDetails(HttpServletRequest request, ModelMap model) throws IOException {
        SecuredUser user = getCurrentUser();
        if ("ADMIN".equalsIgnoreCase(user.getRole())) {
            List<Register> pendingRegisterDetails = formDetailsRepository.getPendingRegisterDetails();
            return pendingRegisterDetails.toString();
        } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
            List<Register> pendingRegisterDetails = formDetailsRepository.getPendingRegisterDetails(user.getUser_id());
            return pendingRegisterDetails.toString();
        }
        return "[]";
    }

    @RequestMapping(value = {"/get-pending-verification-{token}"}, method = RequestMethod.GET)
    @ResponseBody
    public String getPendingRegisterVerificationDetails(HttpServletRequest request,
            ModelMap modelMap, @PathVariable(value = "token") String token) throws IOException {
        SecuredUser user = getCurrentUser();
        if ("ADMIN".equalsIgnoreCase(user.getRole())) {
            PersonDetailsBean person = formDetailsRepository.getPersonDetailByToken(token);
            if (person != null) {
                return person.toString();
            } else {
                JointDetailBean joint = formDetailsRepository.getJointDetailByToken(token);
                if (joint != null) {
                    return joint.toString();
                } else {
                    CompanyDetailBean company = formDetailsRepository.getCompanyDetailByToken(token);
                    if (company != null) {
                        return company.toString();
                    }
                }
            }

        } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
            List<Register> pendingRegisterDetails = formDetailsRepository.getPendingRegisterDetails(user.getUser_id());
            return pendingRegisterDetails.toString();
        }
        return "[]";
    }

    @RequestMapping(value = {"/dl-verification"}, method = RequestMethod.POST)
    @ResponseBody
    public String verifyDlDetail(HttpServletRequest request, ModelMap model, @RequestBody PersonDetailsBean dlPerson) throws IOException {
        String sessiontokenObj = datazooservice.sessionToken();
        HashMap<String, Object> sessionToken = gson.fromJson(sessiontokenObj, HashMap.class);
        String token = (String) sessionToken.get("sessionToken");
        String format3 = common.changeFormat(dlPerson.getDate_of_Birth(), CommonMethods.format8, CommonMethods.format3);
        dlPerson.setDate_of_Birth(format3);
        StringBuilder verifyNZTADL = datazooservice.verifyNZTADL(token, dlPerson);
        System.out.println("verifyNZTADL--->" + verifyNZTADL);
        return verifyNZTADL.toString();
    }

    @RequestMapping(value = {"/pp-verification"}, method = RequestMethod.POST)
    @ResponseBody
    public String verifyPPDetail(HttpServletRequest request, ModelMap model, @RequestBody PersonDetailsBean dlPerson) throws IOException {
        String sessiontokenObj = datazooservice.sessionToken();
        HashMap<String, Object> sessionToken = gson.fromJson(sessiontokenObj, HashMap.class);
        String token = (String) sessionToken.get("sessionToken");
        String Dob = common.changeFormat(dlPerson.getDate_of_Birth(), CommonMethods.format8, CommonMethods.format3);
        dlPerson.setDate_of_Birth(Dob);
        String exp = common.changeFormat(dlPerson.getPassport_expiry(), CommonMethods.format8, CommonMethods.format3);
        dlPerson.setPassport_expiry(exp);
        StringBuilder verifyNZDIAPP = datazooservice.verifyNZDIAPP(token, dlPerson);
        System.out.println("verifyNZDIAPP--->" + verifyNZDIAPP);
        return verifyNZDIAPP.toString();
    }

    @RequestMapping(value = {"/person-registeration"}, method = RequestMethod.POST)
    @ResponseBody
    public String savePersonDetail(HttpServletRequest request, ModelMap model,
            @RequestBody PersonDetailsBean person) throws IOException {
        String token = generator.generate(30);
        person.setTokan(token);
        HashMap response = new HashMap<>();
        System.out.println("here");
        if ("SUBMISSION".equalsIgnoreCase(person.getStatus())) {
            person = registerService.createIndividual(person);
        }
        formDetailsRepository.saveFormDetails(person, response);
        
        sendMail.sendVerifyNewPersonToEmail(person, token, requestedDomain(request));
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }

    @RequestMapping(value = {"/company-registeration"}, method = RequestMethod.POST)
    @ResponseBody
    public String saveCompanyTrustDetail(HttpServletRequest request, ModelMap model,
            @RequestBody CompanyDetailBean company) throws IOException {
        String token = generator.generate(30);
        company.setTokan(token);
        HashMap response = new HashMap<>();
        formDetailsRepository.saveCompanyDetails(company, response);
        sendMail.sendVerifyNewCompanyToEmail(company, token, requestedDomain(request));
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }

    @RequestMapping(value = {"/joint-registeration"}, method = RequestMethod.POST)
    @ResponseBody
    public String saveJointDetail(HttpServletRequest request, ModelMap model,
            @RequestBody JointDetailBean joint) throws IOException {
        String token = generator.generate(30);
        joint.setToken(token);
        if (joint.getMoreInvestorList() != null) {
            joint.getMoreInvestorList().forEach((inv) -> {
                String generate = generator.generate(30);
                inv.setToken(generate);
            });
        }
        HashMap response = new HashMap<>();
        if ("SUBMISSION".equalsIgnoreCase(joint.getStatus())) {
            System.out.println("1");
           
//            joint = registerService.createJoint(joint);
            System.out.println("2");
        }
        formDetailsRepository.saveJointDetails(joint, response);
        sendMail.sendVerifyNewJointToEmail(joint, token, requestedDomain(request));
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }
    
    @RequestMapping(value = {"/joint-single-registeration"}, method = RequestMethod.POST)
    @ResponseBody
    public String saveSingleJointDetail(HttpServletRequest request, ModelMap model,
            @RequestBody JointDetailBean joint) throws IOException, SQLException {
        
        HashMap response = new HashMap<>();
//        if ("SUBMISSION".equalsIgnoreCase(joint.getStatus())) {
//            System.out.println("1");
//           
////            joint = registerService.createJoint(joint);
//            System.out.println("2");
//        }
        formDetailsRepository.saveSingleJointDetails(joint, response);
//        sendMail.sendVerifyNewJointToEmail(joint, joint.getToken(), requestedDomain(request));
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }

}
