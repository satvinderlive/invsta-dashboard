/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import crm.nz.beans.acc.api.Address;
import crm.nz.beans.acc.api.BankAccountDetail;
import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.beans.acc.api.PhoneNumber;
import crm.nz.beans.acc.api.ResponseBean;
import crm.nz.beans.acc.api.SettingLoginBean;
import crm.nz.components.ObjectCastManager;
import crm.nz.components.RandomStringGenerator;
import crm.nz.lambda.repo.SettingDetailsRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.Login;
import crm.nz.table.bean.SecuredUser;
import java.io.IOException;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ADMIN
 */
@RestController
@RequestMapping(value = "/rest/groot/db/api")
public class SettingAPIController implements CurrentUserService {

    @Autowired
    private RandomStringGenerator generator;
    
    @Autowired
    private SettingDetailsRepository settingDetailsRepository;

    @RequestMapping(value = {"/password-registeration"}, method = RequestMethod.POST)
    @ResponseBody
    public String saveChangePassword(HttpServletRequest request, ModelMap model, @RequestBody SettingLoginBean login) throws IOException {
         SecuredUser user = getCurrentUser();
        if (user != null) {
            login.setUsername(user.getUsername());
        }
//        String token = generator.generate(30);
//        login.setTokan(token);
        HashMap response = new HashMap<>();
       
        settingDetailsRepository.savePasswordDetails(login, response);
//        sendMail.sendVerifyNewCompanyToEmail(login, token, requestedDomain(request));
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }
    
    @RequestMapping(value = {"/phoneNumber-registeration"}, method = RequestMethod.POST)
    @ResponseBody
    public String savePhoneDetail(HttpServletRequest request, ModelMap model, @RequestBody PhoneNumber phone) throws IOException {
         SecuredUser user = getCurrentUser();
        if (user != null) {
//            phone.setUsername(user.get());
        }
        String token = generator.generate(30);
//        login.setTokan(token);
        HashMap response = new HashMap<>();
        settingDetailsRepository.savePhoneDetails(phone, response);
//        sendMail.sendVerifyNewCompanyToEmail(login, token, requestedDomain(request));
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }
    
    @RequestMapping(value = {"/address-registeration"}, method = RequestMethod.POST)
    @ResponseBody
    public String saveAddressDetail(ModelMap model, @RequestBody Address address) throws IOException {
         SecuredUser user = getCurrentUser();
        if (user != null) {
//            phone.setUsername(user.get());
        }
        String token = generator.generate(30);
//        login.setTokan(token);
        HashMap response = new HashMap<>();
        settingDetailsRepository.saveAddressDetails(address, response);
//        sendMail.sendVerifyNewCompanyToEmail(login, token, requestedDomain(request));
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }
    
    @RequestMapping(value = {"/bankAccount-registeration"}, method = RequestMethod.POST)
    @ResponseBody
    public String saveBankAccountDetail(HttpServletRequest request, ModelMap model, @RequestBody BankAccountDetail bank) throws IOException {
         SecuredUser user = getCurrentUser();
        if (user != null) {
//            phone.setUsername(user.get());
        }
        String token = generator.generate(30);
//        login.setTokan(token);
        HashMap response = new HashMap<>();
        settingDetailsRepository.saveBankDetails(bank, response);
//        sendMail.sendVerifyNewCompanyToEmail(login, token, requestedDomain(request));
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }
    
}
