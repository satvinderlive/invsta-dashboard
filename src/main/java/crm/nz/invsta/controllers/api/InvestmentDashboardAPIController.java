/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import crm.nz.authorization.GetOAuth2TokenService;
import crm.nz.authorization.InvestmentsService;
import crm.nz.authorization.PricePortfoliosService;
import crm.nz.authorization.TransactionsService;
import crm.nz.beans.acc.api.FmcaInvestmentMix;
import crm.nz.beans.acc.api.FmcaTopAssets;
import crm.nz.beans.acc.api.Investment;
import crm.nz.beans.acc.api.InvestmentHolding;
import crm.nz.beans.acc.api.InvestmentPerformance;
import crm.nz.beans.acc.api.InvestmentTransection;
import crm.nz.beans.acc.api.PortfolioDetail;
import crm.nz.beans.acc.api.Transaction;
import crm.nz.beans.acc.api.TransactionItem;
import crm.nz.repository.InvestmentRepository;
import crm.nz.repository.PortfolioRepository;
import crm.nz.repository.TransactionRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ADMIN
 */
@RestController
@RequestMapping(value = "/rest/groot/db/api")
public class InvestmentDashboardAPIController implements CurrentUserService {

    @Autowired
    private GetOAuth2TokenService oAuth2TokenService;
    @Autowired
    private PricePortfoliosService pricePortfoliosService;
    @Autowired
    private InvestmentsService investmentsService;
    @Autowired
    private TransactionsService transactionsService;
    @Autowired
    private InvestmentRepository investmentsRepository;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private PortfolioRepository portRepository;
    @Autowired
    private Gson gson;

    @RequestMapping(value = {"/investment-holdings"}, method = {RequestMethod.GET})
    @ResponseBody
    public String investmentHoldingsDB(
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String BeneficiaryId,
            @RequestParam(value = "ic", required = false) String InvestmentCode,
            @RequestParam(value = "pc", required = false) String PortfolioCode
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
             if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
                UserId = user.getUser_id();
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                if (UserId == null || UserId != null && UserId.isEmpty()) {
                    UserId = user.getUser_id();
                }
            }else if ("ADMIN".equalsIgnoreCase(user.getRole())) {
                if (UserId == null || UserId != null && UserId.isEmpty()) {
                    UserId = user.getUser_id();
                }
            }
            List<InvestmentHolding> investmentHoldings = investmentsRepository.getInvestmentHoldings(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
            if (investmentHoldings.isEmpty()) {
                List<Investment> investments = investmentsRepository.getInvestments(UserId, BeneficiaryId);
                if (!investments.isEmpty()) {
                    String clientCredentials = oAuth2TokenService.getClientCredentials();
                    for (Investment investment : investments) {
                        StringBuilder result2 = investmentsService.investmentHoldingSummaryValues(clientCredentials, investment.getCode());//we need request for this investment holding summary
                        Type type = new TypeToken<LinkedList<InvestmentHolding>>() {
                        }.getType();
                        List<InvestmentHolding> list = gson.fromJson(result2.toString(), type);
                        list.forEach((InvestmentHolding inv) -> {
                            inv.setBeneficiaryId(investment.getBeneficiaryId());
                        });
                        investmentHoldings.addAll(list);
                    }
                    investmentsRepository.saveInvestmentHolding(investmentHoldings);
                }
                investmentHoldings = investmentsRepository.getInvestmentHoldings(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
            }
            return "{"
                    + "\"investmentHoldings\":" + investmentHoldings.toString()
                    + "}";
        }
        return "{}";
    }

    @RequestMapping(value = {"/investment-performances"}, method = {RequestMethod.GET})
    @ResponseBody
    public String investmentPerformancesDB(
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String BeneficiaryId,
            @RequestParam(value = "ic", required = false) String InvestmentCode,
            @RequestParam(value = "pc", required = false) String PortfolioCode
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
                UserId = user.getUser_id();
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                if (UserId == null || UserId != null && UserId.isEmpty()) {
                    UserId = user.getUser_id();
                }
            } else if ("ADMIN".equalsIgnoreCase(user.getRole())) {
                if (UserId == null || UserId != null && UserId.isEmpty()) {
                    UserId = user.getUser_id();
                }
            }
            List<InvestmentPerformance> investmentPerformances = investmentsRepository.getInvestmentPerformances(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
            if (investmentPerformances.isEmpty()) {
                List<Investment> investments = investmentsRepository.getInvestments(UserId, BeneficiaryId);
                String clientCredentials = oAuth2TokenService.getClientCredentials();
                for (Investment investment : investments) {
                    StringBuilder result1 = investmentsService.investmentPerformance(clientCredentials, investment.getCode());
                    Type type = new TypeToken<LinkedList<InvestmentPerformance>>() {
                    }.getType();
                    List<InvestmentPerformance> list = gson.fromJson(result1.toString(), type);
                    list.forEach((InvestmentPerformance inv) -> {
                        inv.setBeneficiaryId(investment.getBeneficiaryId());
                        inv.setInvestmentCode(investment.getCode());
                    });
                    investmentPerformances.addAll(list);
                }
                investmentsRepository.saveInvestmentPerformance(investmentPerformances);
                investmentPerformances = investmentsRepository.getInvestmentPerformances(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
            }
            return "{"
                    + "\"investmentPerformances\":" + investmentPerformances.toString()
                    + "}";
        }
        return "{}";
    }

    @RequestMapping(value = {"/fmca-investment-mix"}, method = {RequestMethod.GET})
    @ResponseBody
    public String fmcaInvestmentMixDB(
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String BeneficiaryId,
            @RequestParam(value = "ic", required = false) String InvestmentCode,
            @RequestParam(value = "pc", required = false) String PortfolioCode
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
             if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
                UserId = user.getUser_id();
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                if (UserId == null || UserId != null && UserId.isEmpty()) {
                    UserId = user.getUser_id();
                }
            }else if ("ADMIN".equalsIgnoreCase(user.getRole())) {
                if (UserId == null || UserId != null && UserId.isEmpty()) {
                    UserId = user.getUser_id();
                }
            }
            List<FmcaInvestmentMix> fmcaInvestmentMix = investmentsRepository.getTotalFmcaInvestmentMixByTable(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
//            List<FmcaInvestmentMix> fmcaInvestmentMix = investmentsRepository.getTotalFmcaInvestmentMix(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
//            if (fmcaInvestmentMix.isEmpty()) {
//                String clientCredentials = oAuth2TokenService.getClientCredentials();
//                Type type = new TypeToken<LinkedList<FmcaInvestmentMix>>() {
//                }.getType();
//                List<InvestmentHolding> investmentHoldings = investmentsRepository.getInvestmentHoldings(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
//                for (InvestmentHolding investment : investmentHoldings) {
//                    StringBuilder result3 = investmentsService.fmcaInvestmentMix(clientCredentials, investment.getInvestmentCode(), investment.getPortfolioCode());
//                    fmcaInvestmentMix = gson.fromJson(result3.toString(), type);
//                    fmcaInvestmentMix.forEach((FmcaInvestmentMix inv) -> {
//                        inv.setBeneficiaryId(investment.getBeneficiaryId());
//                        inv.setInvestmentCode(investment.getInvestmentCode());
//                    });
//                    investmentsRepository.saveFmcaInvestmentMix((List<FmcaInvestmentMix>) fmcaInvestmentMix);
//                }
//                fmcaInvestmentMix = investmentsRepository.getTotalFmcaInvestmentMixByTable(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
//            }
            return "{"
                    + "\"fmcaInvestmentMix\":" + fmcaInvestmentMix.toString()
                    + "}";
        }
        return "{}";
    }

    @RequestMapping(value = {"/show-me-more"}, method = {RequestMethod.GET})
    @ResponseBody
    public String portfolioDashboard(ModelMap modelMap,
            @RequestParam(value = "ic") String ic,
            @RequestParam(value = "pc") String pc
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            List<InvestmentHolding> investmentHoldings = investmentsRepository.getInvestmentHoldings(null, null, ic, pc);
//            List<InvestmentTransection> investmnetTransection = investmentsRepository.getInvestmnetTransection(null, null, ic, pc);
//            List<FmcaInvestmentMix> fmcaInvestmentMix = investrepository.getTotalFmcaInvestmentMix(null, ic, pc);
            List<FmcaInvestmentMix> fmcaInvestmentMixByTable = portRepository.getTotalFmcaInvestmentMix(pc);
//            List<FmcaTopAssets> fmcaTopAssetses = investrepository.getFmcaTopAssets(null, ic, pc);
            List<FmcaTopAssets> fmcaTopAssetsesByTable = portRepository.getFmcaTopAssets(pc);

            String clientCredentials = null;
            clientCredentials = oAuth2TokenService.getClientCredentials();
//            if (fmcaTopAssetses != null) {
//                fmcaTopAssets = investmentsService.fmcaTopAssets(clientCredentials, ic, pc).toString();
//            }  else {
//                fmcaTopAssets = fmcaTopAssetses.toString();
//            }
            StringBuilder pricePortfolioUnitPrices = pricePortfoliosService.pricePortfolioUnitPrices(clientCredentials, pc);
            return "{"
                    //                    + "\"fmcaTopAssets\":" + fmcaTopAssets
                    + "\"fmcaTopAssetsesByTable\":" + fmcaTopAssetsesByTable.toString()
                    //                    + ",\"fmcaInvestmentMix\":" + fmcaInvestmentMix.toString()
                    + ",\"fmcaInvestmentMixByTable\":" + fmcaInvestmentMixByTable.toString()
                    + ",\"investmentHoldings\":" + investmentHoldings.toString()
                    + ",\"pricePortfolioUnitPrices\":" + pricePortfolioUnitPrices.toString()
                    + "}";
        }
        return "{}";
    }

    @RequestMapping(value = {"/investment-transactions"}, method = {RequestMethod.GET})
    @ResponseBody
    public String BeneTransactionHistory(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String BeneficiaryId,
            @RequestParam(value = "ic", required = false) String InvestmentCode,
            @RequestParam(value = "pc", required = false) String PortfolioCode
    ) {
        SecuredUser user = getCurrentUser();
        List<TransactionItem> transactions;
        if (user != null) {
             if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
                UserId = user.getUser_id();
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                if (UserId == null || UserId != null && UserId.isEmpty()) {
                    UserId = user.getUser_id();
                }
            }
            transactions = transactionRepository.getTransactions(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
            if (transactions == null || transactions != null && transactions.isEmpty()) {
                String clientCredentials = oAuth2TokenService.getClientCredentials();
                List<Investment> investments = investmentsRepository.getInvestments(UserId, BeneficiaryId);
                List<TransactionItem> transactionList = new LinkedList<>();
                for (Investment investment : investments) {
                    boolean resultEqualsTo = true;
                    Integer page = 1;
                    while (resultEqualsTo) {
                        StringBuilder result4 = transactionsService.investmentTransactions(clientCredentials, investment.getCode(), page);
                        Transaction transaction = gson.fromJson(result4.toString(), Transaction.class);
                        transactionList.addAll(transaction.getItems());
                        if (transaction.getItems().size() == 50) {
                            resultEqualsTo = true;
                        } else {
                            resultEqualsTo = false;
                        }
                        page++;
                    }
                }
                transactionRepository.saveTransactions(transactionList);
                transactions = transactionRepository.getTransactions(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
                System.out.println(" txns " + transactions.toString());
            }
            return "{"
                    + "\"investmentTransactions\":" + transactions.toString()
                    + "}";
        }
        return "{}";
    }
}
