/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import crm.nz.repository.CommonRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.jboss.aerogear.security.otp.Totp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.ModelMap;

/**
 *
 * @author Jyotika Narin
 */
@Controller
public class QRController implements CurrentUserService {

    @Autowired
    private CommonRepository repository;

    public static String QR_PREFIX = "https://chart.googleapis.com/chart?chs=200x200&chld=M%%7C0&cht=qr&chl=";
    public static String APP_NAME = "Poc-as-roduct";
//    public static String secret = "VPJNUUCXHSY4NA5X";

    @RequestMapping(value = "/qrcode", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> getQRUrl(@RequestParam("username") final String username) throws UnsupportedEncodingException {
        final Map<String, String> result = new HashMap<>();
        SecuredUser user = getCurrentUser();
        if (user != null) {
//            String base32Secret = secret;
            String base32Secret = generateBase32Secret(16);
            user.setSecret(base32Secret);
            System.out.println("base32Secret--------" + base32Secret);

            repository.updateSecretCode(user);
//            qrCodeMailToUser(user.getEmail());
            result.put("url", generateQRUrl(base32Secret, user.getUsername()));
            result.put("key", base32Secret);
        } else {
            result.put("url", "");
            result.put("key", "");
        }
        return result;
    }
    @RequestMapping(value = "/disbale2fa", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> disbale2fa(@RequestParam("username") final String username) throws UnsupportedEncodingException {
        final Map<String, String> result = new HashMap<>();
        SecuredUser user = getCurrentUser();
        if (user != null) {
            repository.removeSecretCode(user);
            user.setSecret("");
        } else {
            result.put("url","");
            result.put("key", "");
        }
        return result;
    }
    @RequestMapping(value = "/genrateCode", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> genrateCode() throws UnsupportedEncodingException {
        final Map<String, String> result = new HashMap<>();
        SecuredUser user = getCurrentUser();
        if (user != null) {
            result.put("url", generateQRUrl(user.getSecret(),user.getUsername()));
             result.put("key", user.getSecret());
        } else {
            result.put("url","");
            result.put("key", "");
        }
        return result;
    }

    private String generateQRUrl(String secret, String username) throws UnsupportedEncodingException {
        return QR_PREFIX + URLEncoder.encode(String.format("otpauth://totp/%s:%s?secret=%s&issuer=%s", APP_NAME, username, secret, APP_NAME), "UTF-8");
    }

//    private void qrCodeMailToUser(final String email) throws UnsupportedEncodingException {
//        final UserInfo user = repository.findByUsername(email);
//        if (user != null) {
//            mailManager.sendQRcodeEmail(user.getFullName(), email, generateQRUrl(user.getSecret(), user.getEmail()));
//        }
//    }
    public static String generateBase32Secret(int length) {
        StringBuilder sb = new StringBuilder(length);
        Random random = new SecureRandom();
        for (int i = 0; i < length; i++) {
            int val = random.nextInt(32);
            if (val < 26) {
                sb.append((char) ('A' + val));
            } else {
                sb.append((char) ('2' + (val - 26)));
            }
        }
        return sb.toString();
    }

    @RequestMapping(value = {"/use2fa"}, method = RequestMethod.GET)
    public String use2fa(ModelMap modelMap) {
        return "redirect:/home-use2fa";
    }

    @RequestMapping(value = {"/use2fa"}, method = RequestMethod.POST)
    public String use2fa(ModelMap modelMap, @RequestParam(value = "code", required = true) String code) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            Totp totp = new Totp(user.getSecret());
            if (totp.verify(code)) {
                user.setVerification(true);
               return "redirect:/home";
            } else {
                modelMap.addAttribute("message", "Invalid Authentication Code!");
                return "use2fa";
            }
        }
        return "redirect:/login";
    }

}
