/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import crm.nz.beans.acc.api.Advisor;
import crm.nz.beans.acc.api.PhoneNumber;
import crm.nz.beans.acc.api.ResponseBean;
import crm.nz.components.ObjectCastManager;
import crm.nz.lambda.repo.AdminDetailsRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import java.io.IOException;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ADMIN
 */
@RestController
@RequestMapping(value = "/rest/groot/db/admin/api")
public class adminPostApiController implements CurrentUserService {
    
    @Autowired
    AdminDetailsRepository adminDetailsRepository;
    
     @RequestMapping(value = {"/add-advisor"}, method = RequestMethod.POST)
    @ResponseBody
    public String saveAdvisorDetail(HttpServletRequest request, ModelMap model, @RequestBody Advisor advisor) throws IOException {
         SecuredUser user = getCurrentUser();
        if (user != null) {
//            phone.setUsername(user.get());
        }
        HashMap response = new HashMap<>();
        adminDetailsRepository.saveAdvisorDetails(advisor, response);
//        sendMail.sendVerifyNewCompanyToEmail(login, token, requestedDomain(request));
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }
}
