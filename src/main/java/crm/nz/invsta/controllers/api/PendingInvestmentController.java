/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import crm.nz.authorization.GetOAuth2TokenService;
import crm.nz.authorization.OnboardingsServices;
import crm.nz.beans.acc.api.PendingInvestmentBean;
import crm.nz.components.CommonMethods;
import crm.nz.mail.SendMail;
import crm.nz.repository.PendingInvestmentRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author TOSHIBA R830
 */
@RestController
@RequestMapping(value = "/rest/groot/db/api/admin")
public class PendingInvestmentController implements CurrentUserService {

    @Autowired
    private PendingInvestmentRepository repository;
    @Autowired
    private GetOAuth2TokenService oAuth2TokenService;
    @Autowired
    private OnboardingsServices onboardingsServices;
    @Autowired
    private CommonMethods common;
    @Autowired
    private SendMail sendMail;
    

    @RequestMapping(value = {"/pending-investment"}, method = {RequestMethod.POST})
    @ResponseBody
    public String pendingInvestment(ModelMap modelMap, @RequestBody PendingInvestmentBean bean) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            bean.setUserId(user.getUser_id());
            bean.setStatus("Pending");
            bean.setCreated_ts(common.format2(common.today()));
            String clientCredentials = oAuth2TokenService.getClientCredentials();
            String createBeneficiaryInvestmentBody = "{\n"
                    + "	\"ApplicationId\": " + bean.getApplicationId() + ",\n"
                    + "	\"BeneficiaryId\": " + bean.getBeneficiaryId() + ",\n"
                    + "	\"InvestmentName\": \"" + bean.getInvestmentName() + "\",\n"
                    + "	\"InvestmentType\": \"Unit_Registry\",\n"
                    + "	\"InvestmentFunds\": [\n"
                    + "    	{\n"
                    + "		\"ApplicationId\": " + bean.getApplicationId() + ",\n"
                    + "         \"BeneficiaryId\": " + bean.getBeneficiaryId() + ",\n"
                    + "    	\"FundCode\": \"" + bean.getPortfolioCode() + "\",\n"
                    + "    	\"FundName\": \"" + bean.getPortfolioName() + "\",\n"
                    + "    	\"Amount\": " + bean.getInvestedAmount() + "\n"
                    + "    	}"
                    + "    	\n"
                    + "	],\n"
                    + "	\"ReinvestPercentage\": 0,\n"
                    + "	\"AdvisorCode\": \"BR011\",\n"
                    + "	\"AmountToInvest\": " + bean.getInvestedAmount() + ",\n"
                    + "	\"InvestmentContribution\": \"" + bean.getInvestedAmount() + "\"\n"
                    + "}";
            System.out.println("--->" + createBeneficiaryInvestmentBody);
            StringBuilder investmentId = onboardingsServices.createNewInvestment(clientCredentials, bean.getApplicationId(), createBeneficiaryInvestmentBody);
            bean.setInvestmentId(investmentId.toString());
            repository.setPendingInvestment(bean);
//            sendMail.sendInvestmentPaymentMail(bean, user,);
            sendMail.sendInvestmentPaymentPayNowMail(bean, user);
            return "data";
        }
        return "error";
    }

    @RequestMapping(value = {"/get-pending-investment"}, method = {RequestMethod.GET})
     @ResponseBody
    public String pendingInvestment(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if ("ADMIN".equalsIgnoreCase(user.getRole())) {
                List<PendingInvestmentBean> pendingInvestment = repository.getPendingInvestment();
                return pendingInvestment.toString();
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                List<PendingInvestmentBean> pendingInvestment = repository.getAdvisorPendingInvestment(user.getUser_id());
                return pendingInvestment.toString();
            }
        }
        return "error";
    }
}
