/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import crm.nz.beans.acc.api.NoteBean;
import crm.nz.beans.acc.api.ResponseBean;
import crm.nz.components.ObjectCastManager;
import crm.nz.lambda.repo.NoteDetailsRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ADMIN
 */
@RestController
@RequestMapping(value = "/rest/groot/db/api")
public class NoteAPIController implements CurrentUserService {

    @Autowired
    NoteDetailsRepository noteDetailsRepository;

    @RequestMapping(value = {"/fetchNote"}, method = RequestMethod.GET)
    @ResponseBody
    public String getNote(ModelMap map) throws IOException {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            List<NoteBean> notes = noteDetailsRepository.getNotes(user.getUser_id());
            return notes.toString();
               
        }
        return "{}";
    }

    @RequestMapping(value = {"/addNote"}, method = RequestMethod.POST)
    @ResponseBody
    public String addNote(ModelMap map, @RequestBody NoteBean note) throws IOException, SQLException {
        SecuredUser user = getCurrentUser();
        note.setCreated_by(user.getUser_id());
        HashMap response = new HashMap();
        noteDetailsRepository.addNote(note, response);
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }

    @RequestMapping(value = {"/updateNote"}, method = RequestMethod.POST)
    @ResponseBody
    public String updateNote(ModelMap map, @RequestBody NoteBean note) throws IOException, SQLException {
        SecuredUser user = getCurrentUser();
        note.setCreated_by(user.getUser_id());
        HashMap response = new HashMap();
        noteDetailsRepository.updateNote(note, response);
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }
    
    @RequestMapping(value = {"/deleteNote"}, method = RequestMethod.POST)
    @ResponseBody
    public String deleteNote(ModelMap map, @RequestBody NoteBean note) throws IOException, SQLException {
        SecuredUser user = getCurrentUser();
        System.out.println("scsfsfsrhararesrehtsderaser");
        note.setCreated_by(user.getUser_id());
        HashMap response = new HashMap();
        noteDetailsRepository.deleteNote(note, response);
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }

}
