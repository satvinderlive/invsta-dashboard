/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import com.google.gson.Gson;
import crm.nz.authorization.GetOAuth2TokenService;
import crm.nz.authorization.TransactionsService;
import crm.nz.repository.InvestmentRepository;
import crm.nz.repository.TransactionRepository;
import crm.nz.security.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author IESL
 */
@RestController
@RequestMapping(value = "/rest/groot/db/api")
public class DatabasaeAPIController implements CurrentUserService {

    @Autowired
    private GetOAuth2TokenService oAuth2TokenService;
    @Autowired
    private TransactionsService transactionsService;
    @Autowired
    private InvestmentRepository investrepository;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private Gson gson;
    
}
