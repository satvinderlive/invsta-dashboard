/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.authorization;

import crm.nz.beans.acc.api.NZBNApiBean;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StringWriter;
import java.net.URL;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.HttpsURLConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class GetNZBNTokenService {

    private static final Pattern pattern = Pattern.compile(".*\"access_token\"\\s*:\\s*\"([^\"]+)\".*");
//    private String tokenUrl = "https://api.business.govt.nz/services/token";
//    private String clientId = "dBcLy0PQVjUfER0smWkv_sJkgoka";//clientId
//    private String clientSecret = "_f2q8udPLt8Ehl_oZIJGWOLQsfUa";//client secret
    @Autowired
    private NZBNApiBean nZBNApiBean;

    public String getClientCredentials() {
        String auth = nZBNApiBean.getNzbnClientId() + ":" + nZBNApiBean.getNzbnClientSecret();
        String authentication = Base64.getEncoder().encodeToString(auth.getBytes());
        String content = "grant_type=client_credentials&validity_period=7200";
        BufferedReader reader = null;
        HttpsURLConnection connection = null;
        String access_token = "";
        try {
            URL url = new URL(nZBNApiBean.getNzbnTokenUrl());
            connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Authorization", "Basic " + authentication);
//            connection.setRequestProperty("Authorization", "Basic " + token);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Accept", "application/json");
            connection.setDoOutput(true);

//             connection.addRequestProperty("User-Agent", "Mozilla/5.0");
            PrintStream os = new PrintStream(connection.getOutputStream());
            os.print(content);
            os.close();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = null;
            StringWriter out = new StringWriter(connection.getContentLength() > 0 ? connection.getContentLength() : 2048);
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            String response = out.toString();
            Matcher matcher = pattern.matcher(response);
            if (matcher.matches() && matcher.groupCount() > 0) {
                access_token = matcher.group(1);
            }
            System.out.println("clientCredentials : " + access_token);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
            connection.disconnect();
        }
        return access_token;
    }
}
