/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.authorization;

import crm.nz.beans.acc.api.OAuth2TokenServiceBean;
import crm.nz.services.HttpService;
import java.io.IOException;
import java.net.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class MessageService extends HttpService{

    
//    @Autowired
//     private  String oAuth2TokenApiUrl;
// url/advisor/{page}
    @Autowired  
 OAuth2TokenServiceBean oAuth2TokenServiceBean;
    public StringBuilder getAllMessages(String bearerToken) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/message/getAllMessages"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("getAllMessages : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
