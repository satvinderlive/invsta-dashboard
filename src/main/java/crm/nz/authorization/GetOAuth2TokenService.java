/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.authorization;

import crm.nz.beans.acc.api.OAuth2TokenServiceBean;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StringWriter;
import java.net.URL;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.HttpsURLConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class GetOAuth2TokenService {

    {
        System.out.println("GetOAuth2TokenService");
    }
    @Autowired
    private OAuth2TokenServiceBean oAuth2TokenServiceBean;

//    final public static String oAuth2TokenApiUrl = "https://mintnztest2api.azurewebsites.net";
//    final public static String oAuth2TokenApiUrl = "https://mintnztestapi.mmcnz.co.nz";
////    final public static String apiUrl = "https://mintnztest2api.azurewebsites.net";
////    private static final String tokenUrl = oAuth2TokenApiUrl.concat("/connect/token");
//
    private static final Pattern pattern = Pattern.compile(".*\"access_token\"\\s*:\\s*\"([^\"]+)\".*");
//     private static final String oAuth2TokenClientId = "Mint_Invsta";//clientId
//    private static final String oAuth2TokenClientSecret = "M5nJrY6ErrUwmMvYXqm8cwpNQzHh9KMB";//client secret
//    @Autowired
//    private String oAuth2TokenApiUrl;
//    @Autowired
//    private String oAuth2TokenClientId;//clientId
//    @Autowired
//    private String oAuth2TokenClientSecret;//client secret

//    private Pattern pattern = Pattern.compile(".*\"access_token\"\\s*:\\s*\"([^\"]+)\".*");
    public String getClientCredentials() {
        String auth = oAuth2TokenServiceBean.getoAuth2TokenClientId() + ":" + oAuth2TokenServiceBean.getoAuth2TokenClientSecret();
        String authentication = Base64.getEncoder().encodeToString(auth.getBytes());
        String tokenUrl = oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/connect/token");
        String content = "grant_type=client_credentials";
        BufferedReader reader = null;
        HttpsURLConnection connection = null;
        String access_token = "";
        try {
            URL url = new URL(tokenUrl);
            connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Authorization", "Basic " + authentication);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Accept", "application/json");
            PrintStream os = new PrintStream(connection.getOutputStream());
            os.print(content);
            os.close();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = null;
            StringWriter out = new StringWriter(connection.getContentLength() > 0 ? connection.getContentLength() : 2048);
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            String response = out.toString();
            Matcher matcher = pattern.matcher(response);
            if (matcher.matches() && matcher.groupCount() > 0) {
                access_token = matcher.group(1);
            }
            System.out.println("clientCredentials : " + access_token);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
            connection.disconnect();
        }
        return access_token;
    }
}
