/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.authorization;

import crm.nz.beans.acc.api.Beneficiary;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class TestMintNZAPI {

//    public static void insertBeneficiaryList(List<Beneficiary> beneficiaryList) {
//        Connection con = null;
//        String url = "jdbc:mysql://invsta-staging.cdiah0tmq8hx.ap-southeast-2.rds.amazonaws.com/groot",
//                username = "admin", password = "11$O7PDjAcsstm$";
//        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(11);
//        try {
//            con = DriverManager.getConnection(url, username, password);
//            String sql = " INSERT INTO `login_master` (`username`,  `password`, `name`, `active`, `created_ts`)\n"
//                    + "VALUES (?, ?, ?,'Y', now()) ";
//            PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
//            for (Beneficiary beneficiary : beneficiaryList) {
//                preparedStatement.setString(1, beneficiary.getIrdNumber());
//                String irdNumber = beneficiary.getIrdNumber();
//                preparedStatement.setString(2, encoder.encode(irdNumber));
//                preparedStatement.setString(3, beneficiary.getName());
//                preparedStatement.setLong(4, beneficiary.getId());
//                preparedStatement.addBatch();
//            }
//            preparedStatement.executeUpdate();
//            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
//            if (generatedKeys.next()) {
//                String user_id = generatedKeys.getString(1);
//                String sql1 = " INSERT INTO `user_beneficiairy_relationship`\n"
//                        + "(`user_name`,`beneficiairy_id`,`user_id`)\n"
//                        + "VALUES (?,?,?);";
//                PreparedStatement preparedStatement1 = con.prepareStatement(sql1, Statement.RETURN_GENERATED_KEYS);
//                for (Beneficiary beneficiary : beneficiaryList) {
//                    preparedStatement1.setString(1, beneficiary.getName());
//                    preparedStatement1.setLong(2, beneficiary.getId());
//                    preparedStatement1.setString(3, user_id);
//                    preparedStatement1.addBatch();
//                }
//
//            }
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//        } finally {
//            if (con != null) {
//                try {
//                    con.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(TestMintNZAPI.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//    }

    public static void main(String args[]) {
//    DataZooService abc =new DataZooService();
//    String ver =abc.sessionToken();
//        GetOAuth2TokenService oAuth2TokenService = new GetOAuth2TokenService();
//        String clientCredentials = oAuth2TokenService.getClientCredentials();
//        MessageService messageService = new MessageService();
//        messageService.getAllMessages(clientCredentials);
//        AdvisorService advisorService = new AdvisorService();
//        advisorService.advisor(clientCredentials);
//        BeneficiariesService beneficiariesService = new BeneficiariesService();
//        boolean resultEqualsTo = true;
//        Integer page = 1, clientSize = 200;//max Client Size
//        Type type = new TypeToken<LinkedList<Beneficiary>>() {
//        }.getType();
//        List<Beneficiary> beneficiaryList = new LinkedList<>();
//        Gson gson = new Gson();
//        while (resultEqualsTo) {
//            StringBuilder beneficiaries = beneficiariesService.beneficiaries(clientCredentials, page, clientSize);
//            try {
//                LinkedList<Beneficiary> list = gson.fromJson(beneficiaries.toString(), type);
//                System.out.println(page + " @ pageIndex List.size" + list.size());
//                beneficiaryList.addAll(list);
//                if (clientSize != list.size()) {
//                    resultEqualsTo = false;
//                }
//            } catch (JsonSyntaxException ex) {
//                ex.printStackTrace();
//            }
//            page++;
//        }
//        for (Beneficiary beneficiary : beneficiaryList) {
//        beneficiariesService.beneficiaryById(clientCredentials, "4721");
//            beneficiariesService.beneficiaryById(clientCredentials, beneficiary.getBeneficiaryId());
//        }
//        insertBeneficiaryList(beneficiaryList);
//        beneficiariesService.beneficiaryByCustomerNumber(clientCredentials, "MIN626");

//        PortfoliosService portfoliosService = new PortfoliosService();
//        portfoliosService.allPortfolios(clientCredentials);
//        portfoliosService.portfolio(clientCredentials, "290002");
//        portfoliosService.portfolioSectorValuations(clientCredentials, "290004");
//        portfoliosService.portfolioTopAssets(clientCredentials, "290004");
//        portfoliosService.portfolioAssets(clientCredentials, "290004");

//        PricePortfoliosService pricePortfoliosService = new PricePortfoliosService();
//        pricePortfoliosService.pricePortfolioLatest(clientCredentials, "290002");
//        pricePortfoliosService.pricePortfolioLatest(clientCredentials, "290002", "2019-06-25");
//        pricePortfoliosService.pricePortfolioUnitPrices(clientCredentials, "290003");

//        TransactionsService transactionsService = new TransactionsService();
//        transactionsService.investmentTransactionsSummary(clientCredentials, "MIN793");
//        transactionsService.investmentTransactions(clientCredentials, "MIN793");

//        DocumentsService documentsService = new DocumentsService();
//        documentsService.listOfDocuments(clientCredentials);

//        InvestmentsService investmentsService = new InvestmentsService();
//        investmentsService.investment(clientCredentials, "MIN793");//already
//        investmentsService.investmentAssets(clientCredentials, "MIN793", "290004");// wrong 
//        investmentsService.investmentTopAssets(clientCredentials, "MIN793", "290004");// wrong
//        investmentsService.investments(clientCredentials);// bad request exception from api server
//        investmentsService.investmentHoldings(clientCredentials, "MIN793");
//        investmentsService.investmentHoldingSummaryValues(clientCredentials, "MIN793");//we need request for this investment holding summary
//        investmentsService.investmentHoldingTransactionSummary(clientCredentials, "MIN793");
//        investmentsService.investmentHoldingTransactionSummaryByPortfolio(clientCredentials, "MIN793", "290002");
//        investmentsService.investmentSummary(clientCredentials, "MIN793");//it is very good investmnet summary with portfolio break down
//        investmentsService.investmentPerformance(clientCredentials, "MIN793");
//        investmentsService.kiwisaverReasonTotals(clientCredentials, "MIN793");
//        investmentsService.marketvaluetimeseries(clientCredentials);// bad request exception from api server
//        investmentsService.fmcaInvestmentMix(clientCredentials, "MIN793");
//        investmentsService.fmcaInvestmentMix(clientCredentials, "MIN793", "290004");
//        investmentsService.fmcaTopAssets(clientCredentials, "MIN793");
//        investmentsService.fmcaTopAssets(clientCredentials, "MIN793", "290002");

//        investmentsService.investment(clientCredentials, "MIN876");//already
//        AllConnectionService.credentialsProvider();
//        AllConnectionService.getSecret();
//        AllConnectionService allConnectionService = new AllConnectionService();
//        investmentsService.fmcaTopAssets(clientCredentials, "MIN793");
//        DataZooService datazoo= new DataZooService();
//        String tokan= datazoo.sessionToken();
//        GetOAuth2TokenService oAuth2TokenService = new GetOAuth2TokenService();
//        String clientCredentials = oAuth2TokenService.getClientCredentials();
//        DataZooService datazoo = new DataZooService();
//        datazoo.sessionToken();
//        GetOAuth2TokenService oAuth2TokenService = new GetOAuth2TokenService();
//        String clientCredentials = oAuth2TokenService.getClientCredentials();
////        String app = "{ \"ApplicationId\": 0, \"BeneficiaryId\": 0, \"Title\": \"string\", \"FirstName\": \"string\", \"SecondName\": \"string\", \"LastName\": \"string\", \"MiddleName\": \"string\", \"PreferredName\": \"string\", \"Gender\": \"Male\", \"DateOfBirth\": \"string\", \"PlaceOfBirth\": \"string\", \"Nation\": \"string\", \"Occupation\": \"string\", \"OccupationStatus\": \"string\", \"IRDNo\": \"string\", \"IsUSTaxResident\": true, \"PIRRate\": 0, \"ConsentToCheckId\": true, \"Email\": \"string\", \"SecondaryEmail\": \"string\", \"PreferredComm\": \"string\", \"TINNumber\": \"string\", \"TINNumber2\": \"string\", \"TINNumber3\": \"string\", \"TINCountry\": \"string\", \"TINCountry2\": \"string\", \"TINCountry3\": \"string\", \"CountryOfResidency\": \"string\", \"ExternalReference\": \"string\", \"UserDefined1\": \"string\", \"UserDefined2\": \"string\", \"UserDefined3\": \"string\", \"UserDefined4\": \"string\", \"UserDefined5\": \"string\", \"UserDefined6\": \"string\", \"UserDefined7\": \"string\", \"UserDefined8\": \"string\", \"UserDefined9\": \"string\", \"UserDefined10\": \"string\"}";
//        OnboardingsServices onboardingsServices = new OnboardingsServices();
//        onboardingsServices.currentApplications(clientCredentials);
//        StringBuilder createApplication = onboardingsServices.createApplication(clientCredentials);
//        System.out.println("createApplication" + createApplication);
//        onboardingsServices.currentApplicationById(clientCredentials, "16356");
//        onboardingsServices.deleteApplicationById(clientCredentials, "16327");
//      "Type": "Unavailable", "Message": "Application Deletion is not enabled"
//        onboardingsServices.currentApplicationByRef(clientCredentials, "15067");
//        String updateBeneficiayBody = "{\n"
//                + "  \"ApplicationId\": 15068,\n"
//                + "  \"BeneficiaryId\": 14193,\n"
//                + "  \"Title\": \"Neeraj\",\n"
//                + "  \"FirstName\": \"Neerja\",\n"
//                + "  \"SecondName\": \"Mamu\",\n"
//                + "  \"LastName\": \"Khan\",\n"
//                + "  \"MiddleName\": \"Pooja\",\n"
//                + "  \"PreferredName\": \"Neerja Khan\",\n"
//                + "  \"Gender\": \"Female\",\n"
//                + "  \"DateOfBirth\": \"2005-01-19\",\n"
//                + "  \"PlaceOfBirth\": \"Home\",\n"
//                + "  \"Nation\": \"Ind\",\n"
//                + "  \"Occupation\": 21,\n"
//                + "  \"OccupationStatus\": \"A-Level\",\n"
//                + "  \"IRDNo\": \"1089006541\",\n"
//                + "  \"IsUSTaxResident\": false,\n"
//                + "  \"PIRRate\": 0,\n"
//                + "  \"ConsentToCheckId\": true,\n"
//                + "  \"Email\": \"neerja@test.co\",\n"
//                + "  \"SecondaryEmail\": \"neerja@test.co\",\n"
//                + "  \"PreferredComm\": \"\",\n"
//                + "  \"TINNumber\": \"10213241343545645\",\n"
//                + "  \"CountryOfResidency\": \"New Zealand\",\n"
//                + "  \"ExternalReference\": \"IAS\"\n"
//                + "}";
//        System.out.println("---->"+updateBeneficiayBody);
//        StringBuilder updateBeneficiary = onboardingsServices.updateBeneficiary(clientCredentials, "15068", "14193", updateBeneficiayBody);
//        String createBeneficiayBody = "{\n"
//                + "  \"ApplicationId\": 16356,\n"
//                + "  \"BeneficiaryId\": 0,\n"
//                + "  \"Title\": \"Mr\",\n"
//                + "  \"FirstName\": \"Rony\",\n"
//                + "  \"SecondName\": \"Honey\",\n"
//                + "  \"LastName\": \"Singh\",\n"
//                + "  \"MiddleName\": \"\",\n"
//                + "  \"PreferredName\": \"Rony Singh\",\n"
//                + "  \"Gender\": \"Male\",\n"
//                + "  \"DateOfBirth\": \"2000-12-19\",\n"
//                + "  \"PlaceOfBirth\": \"Home\",\n"
//                + "  \"Nation\": \"Ind\",\n"
//                + "  \"Occupation\": 6,\n"
//                + "  \"OccupationStatus\": \"A-Level\",\n"
//                + "  \"IRDNo\": \"1089006541\",\n"
//                + "  \"IsUSTaxResident\": false,\n"
//                + "  \"PIRRate\": 0,\n"
//                + "  \"ConsentToCheckId\": true,\n"
//                + "  \"Email\": \"maninderjit@iesl.co\",\n"
//                + "  \"SecondaryEmail\": \"maninderjit@test.com\",\n"
//                + "  \"PreferredComm\": \"\",\n"
//                + "  \"TINNumber\": \"1234567890\",\n"
//                + "  \"CountryOfResidency\": \"New Zealand\",\n"
//                + "  \"ExternalReference\": \"IAS\"\n"
//                + "}";
//        System.out.println("--->" + createBeneficiayBody);
//        onboardingsServices.createBeneficiary(clientCredentials, "16356", createBeneficiayBody);
//        onboardingsServices.currentApplicationById(clientCredentials, "16356");
//        onboardingsServices.currentApplicationById(clientCredentials, "15067");
//         "Errors": ""
//        onboardingsServices.submitApplication(clientCredentials, "15067");//Sorry, there seems to be a problem - Nexus Registry API
//        String createBeneficiayRelationshipBody = "{\n"
//                + "    \"ApplicationId\": 15068,\n"
//                + "    \"BeneficiaryRelationshipId\": 0,\n"
//                + "    \"BeneficiaryId\": 16597,\n"
//                + "    \"SecondaryBeneficiaryId\": 14193,\n"
//                + "    \"RelationshipType\": \"Guardian\"\n"
//                + "}";
//        onboardingsServices.createBeneficiaryRelationship(clientCredentials, "15067", createBeneficiayRelationshipBody);
//        String response = "{\n"
//                + "    \"ApplicationId\": 15068,\n"
//                + "    \"BeneficiaryRelationshipId\": 1207,\n"
//                + "    \"BeneficiaryId\": 16597,\n"
//                + "    \"SecondaryBeneficiaryId\": 14193,\n"
//                + "    \"RelationshipType\": \"Guardian\"\n"
//                + "}";
//        onboardingsServices.fetchBeneficiaryRelationshipByIds(clientCredentials, "15067", "45");
//        String createBeneficiayBankAccountBody = "{\n"
//                + "  \"ApplicationId\": 15067,\n"
//                + "  \"BeneficiaryId\": 14194,\n"
//                + "  \"BankCode\": 01,\n"
//                + "  \"BranchCode\": 0001,\n"
//                + "  \"Account\": 3089724,\n"
//                + "  \"Suffix\": 30,\n"
//                + "  \"BankAccountName\": \"Abhy Singla\",\n"
//                + "  \"BankAccountCurrency\": \"NZD\",\n"
//                + "  \"IsPrimary\": false\n"
//                + "}";
//        System.out.println("--->" + createBeneficiayBankAccountBody);
//        onboardingsServices.createBeneficiaryBankAccount(clientCredentials, "15067", createBeneficiayBankAccountBody);
//        String response = "{\n"
//                + "    \"Errors\": {\n"
//                + "        \"BankAccount: BankAccount\": [\n"
//                + "            {\n"
//                + "                \"Type\": \"Other\",\n"
//                + "                \"Message\": \"Invalid BankAccount: The bank account number is not valid.\"\n"
//                + "            }\n"
//                + "        ]\n"
//                + "    },\n"
//                + "    \"Type\": \"about:blank\",\n"
//                + "    \"Title\": null,\n"
//                + "    \"Status\": 422,\n"
//                + "    \"Detail\": null\n"
//                + "}";
//        System.out.println("--->" + response);
// after execution Bank Account record not supplied, or Bank Account object identifiers do not match the create parameters
//      Primary key field BeneficiaryAddressId cannot have a value for method POST
//        String BeneficiaryAddressBody = "{\n"
//                + "  \"ApplicationId\": 15068,\n"
//                + "  \"BeneficiaryId\": 14193,\n"
//                + "  \"AddressType\": \"Residential\",\n"
//                + "  \"AddressLine1\": \"445 Mount Eden Road, Mount Eden, Auckland\",\n"
//                + "  \"AddressLine2\": \"\",\n"
//                + "  \"AddressLine3\": \"\",\n"
//                + "  \"AddressLine4\": \"\",\n"
//                + "  \"City\": \"Auckland\",\n"
//                + "  \"Region\": \"Auckland 1010\",\n"
//                + "  \"State\": \"Auckland\",\n"
//                + "  \"Nation\": \"New Zealand\",\n"
//                + "  \"PostCode\": \"5022\",\n"
//                + "  \"IsPrimary\": true,\n"
//                + "  \"SameAsResidential\": true\n"
//                + "}";
//        System.out.println("--->" + BeneficiaryAddressBody);
//        onboardingsServices.createBeneficiaryAddress(address, "15068", "14193", BeneficiaryAddressBody);
//                + " { \"BeneficiaryAddressId\": 16111 }\n"
//        String citizenShip = "";
//        onboardingsServices.createBeneficiaryAddress(address, "15067", "14192", citizenShip);
//                + " { \"BeneficiaryCitizenshipId\": 3044 }\n"
//        String createBeneficiaryInvestmentBody = "{\n"
//                + "	\"ApplicationId\": 15067,\n"
//                + "	\"BeneficiaryId\": 14192,\n"
//                + "	\"InvestmentName\": \" Investment\",\n"
//                + "	\"InvestmentType\": \"Unit_Registry\",\n"
//                + "	\"InvestmentFunds\": [\n"
//                + "    	{\n"
//                + "		\"ApplicationId\": 15067,\n"
//                + "        \"BeneficiaryId\": 14192,\n"
//                + "    	\"FundCode\": \"290002\",\n"
//                + "    	\"FundName\": \"Australasian Equity Fund\",\n"
//                + "    	\"Amount\": 1500\n"
//                + "    	},    	\n"
//                + "    	{\n"
//                + "		\"ApplicationId\": 15067,\n"
//                + "        \"BeneficiaryId\": 14192,\n"
//                + "    	\"FundCode\": \"290004\",\n"
//                + "    	\"FundName\": \"Australasian Property Fund\",\n"
//                + "    	\"Amount\": 1500\n"
//                + "    	}\n"
//                + "	],\n"
//                + "	\"ReinvestPercentage\": 0,\n"
//                + "	\"AdvisorCode\": \"BR011\",\n"
//                + "	\"AmountToInvest\": 3000,\n"
//                + "	\"InvestmentContribution\": \"3000\"\n"
//                + "}";
//        System.out.println("--->" + createBeneficiaryInvestmentBody);
//        POST /onboarding/applications/{applicationId}/investments        
//        after execution 18919
//        String body = "{\n"
//                        + "    \"BeneficiaryId\": 4721, \n"
//                        + "    \"Name\": \"New Investment 4721\", \n"
//                        + "    \"InvestmentType\": \"Unit_Registry\", \n"
//                        + "    \"InvestmentProfiles\": [ { \"FundCode\": \"290002\", \"Percentage\": 50, \"ReinvestPercentage\": 50 },{ \"FundCode\": \"290007\", \"Percentage\": 50, \"ReinvestPercentage\": 50 } ],\n"
//                        + "    \"BankAccounts\": [ { \"BankCode\": 01, \"BranchCode\": 888, \"Account\": 7841210, \"Suffix\": 0, \"BankAccountName\": \"New Investment\", \"IsPrimary\": true } ],\n"
//                        + "    \"StandingInstructions\": [ { \"BankAccountName\": \"string\", \"Frequency\": \"Weekly\", \"Amount\": 0, \"Comments\": \"string\", \"StartDate\": \"2019-09-27T06:23:03.806Z\", \"EndDate\": \"2021-09-29T06:23:03.806Z\" } ],\n"
//                        + "}";
//                onboardingsServices.addNewInvestment(clientCredentials, "4721", body);
//                String body = "{\n"
//                        + "        \"GrossAmount\": 10, \n"
//                        + "        \"Portfolio\": \"290002\"\n"
//                        + "      }";
//                onboardingsServices.createInvestmentTransaction(clientCredentials, "MIN793", body);
        //All, Percentage, Dollars, Units
        //        String body = "{\n"
        //                + "     \"AmountType\": \"All\",\n"
        //                + "     \"Amount\": 10, \n"
        //                + "     \"Portfolio\": \"290002\" \n"
        //                + "}";
        //        onboardingsServices.createInvestmentTransactionRedemption(clientCredentials, "MIN793", body);
//POST        https://mintnztestapi.mmcnz.co.nz/onboarding/registeruser?emailAddress=maninderjit%40iesl.co&resendExisting=true
//User Registered Successfully
    }
}
