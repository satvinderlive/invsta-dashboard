/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.authorization;

import crm.nz.beans.acc.api.OAuth2TokenServiceBean;
import crm.nz.services.HttpService;
import java.net.MalformedURLException;
import java.net.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class BeneficiariesService extends HttpService {

// url/beneficiaries/{page}
    public StringBuilder beneficiaries(String bearerToken) {
        return beneficiaries(bearerToken, 1);
    }
    @Autowired
    OAuth2TokenServiceBean oAuth2TokenServiceBean;

//    @Autowired
//    private String oAuth2TokenApiUrl;
// url/beneficiaries/{page}
    public StringBuilder beneficiaries(String bearerToken, int pageIndex) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/beneficiaries/" + pageIndex + "?page=1&pageSize=200"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("beneficiaries : " + builder);
            return builder;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/beneficiaries/{page}
    public StringBuilder beneficiaries(String bearerToken, int pageIndex, int pageSize) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/beneficiaries/" + pageIndex + "?page=1&pageSize=" + pageSize));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("beneficiaries : " + builder);
            return builder;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/beneficiary/{id}
    public StringBuilder beneficiaryById(String bearerToken, String id) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/beneficiary/" + id));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("beneficiaryById : " + builder);
            return builder;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/beneficiary/external/{customerNumber}
    public StringBuilder beneficiaryByCustomerNumber(String bearerToken, String customerNumber) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/beneficiary/external/" + customerNumber));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("beneficiary : " + builder);
            return builder;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
