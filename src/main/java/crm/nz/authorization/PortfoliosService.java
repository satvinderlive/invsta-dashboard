/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.authorization;

import crm.nz.beans.acc.api.OAuth2TokenServiceBean;
import crm.nz.services.HttpService;
import java.io.IOException;
import java.net.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class PortfoliosService extends HttpService {

//    @Autowired
//     private  String oAuth2TokenApiUrl;
// url/portfolio/allPortfolios
      @Autowired
    OAuth2TokenServiceBean oAuth2TokenServiceBean;

    public StringBuilder allPortfolios(String bearerToken) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/portfolio/allPortfolios"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("allPortfolios : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/portfolio/{portfolioCode}
    public StringBuilder portfolio(String bearerToken, String portfolioCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/portfolio/" + portfolioCode));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("portfolio : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/portfolio/{portfolioCode}/sectorValuations
    public StringBuilder portfolioSectorValuations(String bearerToken, String portfolioCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/portfolio/" + portfolioCode + "/sectorValuations"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("portfolio : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/portfolio/{portfolioCode}/TopAssets
    public StringBuilder portfolioTopAssets(String bearerToken, String portfolioCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/portfolio/" + portfolioCode + "/TopAssets"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("portfolio : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
// url/portfolio/{portfolioCode}/assets/{page}

    public StringBuilder portfolioAssets(String bearerToken, String portfolioCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/portfolio/" + portfolioCode + "/assets/1"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("portfolio : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/portfolio/{code}/volatilityRisk
//    public StringBuilder portfolio(String bearerToken, String portfolioCode) {
//        try {
//            URL url = new URL(apiUrl.concat("/portfolio/" + portfolioCode));
//            StringBuilder builder = getResponse(url, bearerToken);
//            System.out.println("portfolio : " + builder);
//            return builder;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
}
