/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.authorization;

import crm.nz.beans.acc.api.OAuth2TokenServiceBean;
import crm.nz.services.HttpService;
import java.io.IOException;
import java.net.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class InvestmentsService extends HttpService{
    
//    @Autowired
//     private  String oAuth2TokenApiUrl;

          @Autowired
 OAuth2TokenServiceBean oAuth2TokenServiceBean;
// url/investment/{investmentCode}/topAssets/{portfolioCode}
    public StringBuilder investmentTopAssets(String bearerToken, String investmentCode, String portfolioCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/topAssets/" + portfolioCode));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("investment : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url /investment/{investmentCode}/assets/{page}5?page=1&pageSize=25"    
    public StringBuilder investmentAssets(String bearerToken, String investmentCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/assets/1?page=1&pageSize=25"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("investmentAssets : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url /investment/marketvaluetimeseries
    public StringBuilder marketvaluetimeseries(String bearerToken) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/marketvaluetimeseries?interval=1"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("marketvaluetimeseries : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url /investment/{investmentCode}/fmcaInvestmentMix
    public StringBuilder fmcaInvestmentMix(String bearerToken, String investmentCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/fmcaInvestmentMix"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("fmcaInvestmentMix : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url /investment/{investmentCode}/fmcaInvestmentMix?portfolioCode={portfolioCode}
    public StringBuilder fmcaInvestmentMix(String bearerToken, String investmentCode, String portfolioCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/fmcaInvestmentMix?portfolioCode=" + portfolioCode));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("fmcaInvestmentMix : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url /investment/{investmentCode}/fmcaTopAssets
    public StringBuilder fmcaTopAssets(String bearerToken, String investmentCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/fmcaTopAssets"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("fmcaTopAssets : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url /investment/{investmentCode}/fmcaTopAssets?portfolioCode={portfolioCode}
    public StringBuilder fmcaTopAssets(String bearerToken, String investmentCode, String portfolioCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/fmcaTopAssets?portfolioCode=" + portfolioCode));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("fmcaTopAssets : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/investment/{investmentCode}
    public StringBuilder investment(String bearerToken, String investmentCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("investment : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/investment/{investmentCode}
    public StringBuilder investmentSummary(String bearerToken, String investmentCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/summary"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("investmentSummary : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/investment/{investmentCode}/kiwisaverreasontotals
    public StringBuilder kiwisaverReasonTotals(String bearerToken, String investmentCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/kiwisaverreasontotals"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("kiwisaverReasonTotals : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/investment/{investmentCode}/performance
    public StringBuilder investmentPerformance(String bearerToken, String investmentCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/performance?asAt=2019-08-29"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("investmentPerformance : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/investment/{investmentCode}/holdings
    public StringBuilder investmentHoldings(String bearerToken, String investmentCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/holdings"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("investmentHoldings : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/investment/{investmentCode}/portfolioPercentages/{page}
    public StringBuilder portfolioPercentages(String bearerToken, String investmentCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/portfolioPercentages/1"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("portfolioPercentages : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/investment/{investmentCode}/holdingSummaryValues
//    asAt Date of the holdings, leave blank for the latest holdings
    public StringBuilder investmentHoldingSummaryValues(String bearerToken, String investmentCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/holdingSummaryValues"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("investmentHoldingSummaryValues : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/investment/{investmentCode}/holdingSummaryValues/{asAt}
//    asAt Date of the holdings, leave blank for the latest holdings
    public StringBuilder investmentHoldingSummaryValues(String bearerToken, String investmentCode, String asAt) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/holdingSummaryValues" + "/" + asAt));
            StringBuilder builder = getResponse(url, bearerToken);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/investment/{investmentCode}/holdingTransactionSummary
//    asAt Date of the holdings, leave blank for the latest holdings
    public StringBuilder investmentHoldingTransactionSummary(String bearerToken, String investmentCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/holdingTransactionSummary"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("investmentHoldingTransactionSummary : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/investment/{investmentCode}/holdingSummaryValues/asAt
//    asAt Date of the holdings, leave blank for the latest holdings
    public StringBuilder investmentHoldingTransactionSummary(String bearerToken, String investmentCode, String asAt) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/holdingTransactionSummary" + "/" + asAt));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("investmentHoldingTransactionSummaryAsAt : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/investment/{investmentCode}/holdingTransactionSummary
//    asAt Date of the holdings, leave blank for the latest holdings
    public StringBuilder investmentHoldingTransactionSummaryByPortfolio(String bearerToken, String investmentCode, String portfolioCode) {

        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/holdingTransactionSummary/" + portfolioCode));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("investmentHoldingTransactionSummaryByPortfolio : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/investment/{investmentCode}/holdingSummaryValues/asAt
//    asAt Date of the holdings, leave blank for the latest holdings
    public StringBuilder investmentHoldingTransactionSummaryByPortfolio(String bearerToken, String investmentCode, String portfolio, String asAt) {

        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/holdingTransactionSummary" + "/AsAt/" + asAt));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("investmentHoldingTransactionSummaryAsAt : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/investments/{page}
    public StringBuilder investments(String bearerToken) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investments/1?page=1&pageSize=25"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("investments : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
