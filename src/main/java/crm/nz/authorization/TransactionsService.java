/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.authorization;

import crm.nz.beans.acc.api.OAuth2TokenServiceBean;
import crm.nz.services.HttpService;
import java.io.IOException;
import java.net.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class TransactionsService extends HttpService {
    
//    @Autowired
//     private  String oAuth2TokenApiUrl;
      @Autowired
      OAuth2TokenServiceBean oAuth2TokenServiceBean;
// url/investment/{investmentCode}/transactions/summary/{page}
    public StringBuilder investmentTransactionsSummary(String bearerToken, String investmentCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/transactions/summary/1"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("investmentTransactionsSummary : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/investment/{investmentCode}/transactions
    public StringBuilder investmentTransactions(String bearerToken, String investmentCode, Integer page) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/" + investmentCode + "/transactions/" + page+"?page=1&pageSize=50"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("investmentTransactions : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
