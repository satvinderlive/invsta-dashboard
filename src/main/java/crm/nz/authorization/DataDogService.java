/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.authorization;

import crm.nz.services.HttpService;
import java.io.IOException;
import java.net.URL;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class DataDogService extends HttpService {

    final static String apiKey = "a315c8e21731f674069c581435da6b6a";
    final static String appKeyName = "temp_app_key";
    final static String appKey = "5388123b841ae919e9717e84fa34b987b492580e";
    final static String apiUrl = "https://api.datadoghq.com/api/v1";

    public StringBuilder checkAppRun() {
        try {
            URL url = new URL(apiUrl.concat("/check_run"));
            StringBuilder response = postResponse(url, null, null,null, null);
            System.out.println("response : " + response);
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
