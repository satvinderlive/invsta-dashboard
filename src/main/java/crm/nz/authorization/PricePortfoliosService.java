/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.authorization;

import crm.nz.beans.acc.api.OAuth2TokenServiceBean;
import crm.nz.services.HttpService;
import java.io.IOException;
import java.net.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class PricePortfoliosService extends HttpService{

    
//    @Autowired
//     private  String oAuth2TokenApiUrl;
// url /price/portfolio/{portfolioCode}/latest
      @Autowired
      OAuth2TokenServiceBean oAuth2TokenServiceBean;
    public StringBuilder pricePortfolioLatest(String bearerToken, String portfolioCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/price/portfolio/" + portfolioCode + "/latest"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("portfolio : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url /price/portfolio/{portfolioCode}/latest/{onOrBeforeDate}
    public StringBuilder pricePortfolioLatest(String bearerToken, String portfolioCode, String onOrBeforeDate) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/price/portfolio/" + portfolioCode + "/latest/" + onOrBeforeDate));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("portfolio : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    
// url /price/portfolio/{portfolioCode}/UnitPrices
    public StringBuilder pricePortfolioUnitPrices(String bearerToken, String portfolioCode) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/price/portfolio/" + portfolioCode + "/UnitPrices"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("portfolio : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
