/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.authorization;

import crm.nz.beans.acc.api.OAuth2TokenServiceBean;
import crm.nz.services.HttpService;
import java.io.IOException;
import java.net.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class OnboardingsServices extends HttpService {

    
      @Autowired
 OAuth2TokenServiceBean oAuth2TokenServiceBean;
//    @Autowired
//     private  String oAuth2TokenApiUrl;
// url/onboarding/applications GET
    //Pending and done application by current user.
    public StringBuilder currentApplications(String bearerToken) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/onboarding/applications"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("currentApplications : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// /onboarding/applications/{applicationId} GET
    //Pending and done application by current user.
    public StringBuilder currentApplicationById(String bearerToken, String applicationId) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/onboarding/applications/").concat(applicationId));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("currentApplicationById : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// /onboarding/applications/externalReference/{externalReference} GET
    //Pending and done application by current user.
    public StringBuilder currentApplicationByRef(String bearerToken, String externalReference) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/onboarding/applications/externalReference/").concat(externalReference));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("currentApplicationByRef : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// /onboarding/applications/{applicationId}/beneficiaries/{beneficiaryId} GET
    //Pending and done application by current user.
    public StringBuilder fetchBeneficiaryByIds(String bearerToken, String applicationId, String beneficiaryId) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/onboarding/applications/").concat(applicationId).concat("/beneficiaries/").concat(beneficiaryId));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("fetchBeneficiaryByIds :" + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// /onboarding/applications/{applicationId}/beneficiaryrelationship/{beneficiaryRelationshipId}
    //Retrieves Beneficiary Relationship.
    public StringBuilder fetchBeneficiaryRelationshipByIds(String bearerToken, String applicationId, String beneficiaryRelationshipId) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/onboarding/applications/").concat(applicationId).concat("/beneficiaryrelationship/").concat(beneficiaryRelationshipId));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("fetchBeneficiaryRelationshipByIds : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/onboarding/applications POST
    //create a new application by current user
    public StringBuilder createApplication(String bearerToken) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/onboarding/applications"));
            StringBuilder builder = postResponse(url, bearerToken, null);
            System.out.println("createApplication : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // url/onboarding/applications DELETE
    //Delete an application by Id
    public StringBuilder deleteApplicationById(String bearerToken, String applicationId) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/onboarding/applications/").concat(applicationId));
            StringBuilder builder = deleteResponse(url, bearerToken);
            System.out.println("currentApplicationById : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url /onboarding/applications/{applicationId}/beneficiaries/{beneficiaryId} PUT
    //Update details of a Beneficiary
    public void updateBeneficiary(String bearerToken, String applicationId, String beneficiaryId, String body) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/onboarding/applications/").concat(applicationId).concat("/beneficiaries/").concat(beneficiaryId));
            putRequest(url, bearerToken, body);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

// url /onboarding/applications/{applicationId}/beneficiaries POST
    //Create Beneficiary in current application by id
    public StringBuilder createBeneficiary(String bearerToken, String applicationId, String body) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/onboarding/applications/").concat(applicationId).concat("/beneficiaries"));
            System.out.println(url);
            StringBuilder builder = postResponse(url, bearerToken, body);
            System.out.println("createBeneficiary : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/onboarding/applications/{applicationId}/submit POST
    //Completes the Onboarding process and turns the current applicant using current user
    public StringBuilder submitApplication(String bearerToken, String applicationId) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/onboarding/applications/").concat(applicationId).concat("/submit"));
            StringBuilder builder = postResponse(url, bearerToken, null);
            System.out.println("createApplication : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // url /onboarding/applications/{applicationId}/beneficiaryrelationship POST
    //Create Beneficiary Relationship in current application by id
    public StringBuilder createBeneficiaryRelationship(String bearerToken, String applicationId, String body) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/onboarding/applications/").concat(applicationId).concat("/beneficiaryrelationship"));
            StringBuilder builder = postResponse(url, bearerToken, body);
            System.out.println("createBeneficiaryRelationship : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // url /onboarding/applications/{applicationId}/bankaccounts POST
    //Create Bank Account in current application by id
    public StringBuilder createBeneficiaryBankAccount(String bearerToken, String applicationId, String body) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/onboarding/applications/").concat(applicationId).concat("/bankaccounts"));
            StringBuilder builder = postResponse(url, bearerToken, body);
            System.out.println("createBeneficiaryRelationship : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // url /onboarding/applications/{applicationId}/beneficiaries/{beneficiaryId}/citizenShip POST
    /**
     * "Message": "Primary key field BeneficiaryAddressId cannot have a value
     * for method POST"
     */
    public StringBuilder createBeneficiaryAddress(String bearerToken, String applicationId, String beneficiaryId, String body) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/onboarding/applications/").concat(applicationId).concat("/beneficiaries/").concat(beneficiaryId).concat("/citizenShip"));
            StringBuilder builder = postResponse(url, bearerToken, body);
            System.out.println("createBeneficiaryRelationship : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // url /onboarding/applications/{applicationId}/beneficiaries/{beneficiaryId}/citizenShip POST
    public StringBuilder createBeneficiaryCitizenship(String bearerToken, String applicationId, String beneficiaryId, String body) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/onboarding/applications/").concat(applicationId).concat("/beneficiaries/").concat(beneficiaryId).concat("/citizenShip"));
            StringBuilder builder = postResponse(url, bearerToken, body);
            System.out.println("createBeneficiaryCitizenship : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

//        POST /onboarding/applications/{applicationId}/investments     
    public StringBuilder createNewInvestment(String bearerToken, String applicationId, String body) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/onboarding/applications/").concat(applicationId).concat("/investments"));
            StringBuilder builder = postResponse(url, bearerToken, body);
            System.out.println("addNewInvestment : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * {
     * "BeneficiaryId": 0, "BeneficiaryIRDNumber": "string",
     * "BeneficiaryCustomerNumber": "string", "Name": "string", "Greeting":
     * "string", "InvestmentType": "Unit_Registry", "InvestmentProfiles": [ {
     * "FundCode": "string", "Percentage": 0, "ReinvestPercentage": 0 } ],
     * "BankAccounts": [ { "BankCode": 0, "BranchCode": 0, "Account": 0,
     * "Suffix": 0, "BankAccountName": "string", "IsPrimary": true } ],
     * "StandingInstructions": [ { "BankAccountName": "string", "Frequency":
     * "Weekly", "Amount": 0, "Comments": "string", "StartDate":
     * "2019-09-27T06:23:03.806Z", "EndDate": "2019-09-27T06:23:03.806Z" } ],
     * "InvestmentStrategy": "string", "InvestmentStrategyBand":
     * 0,"ExternalReference": "string", "DefaultReinvestPercentage": 0,
     * "AdvisorCode": "string" }
     */
    // url /beneficiary/{beneficiaryId}/investment POST
//  Adds a new investment to an existing beneficiary. This can only be used once a beneficiary has already been onboarded. 
    public StringBuilder addNewInvestment(String bearerToken, String beneficiaryId, String body) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/beneficiary/").concat(beneficiaryId).concat("/investment"));
            StringBuilder builder = postResponse(url, bearerToken, body);
            System.out.println("addNewInvestment : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * {
     * "ExternalReference": "string", "GrossAmount": 0, required "Portfolio":
     * "string" required }
     */
    // url /investment/{investmentCode}/action/application POST
//  Requests an application/subsequent investment transaction for the given investment 
    public StringBuilder createInvestmentTransaction(String bearerToken, String investmentCode, String body) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/").concat(investmentCode).concat("/action/application"));
            StringBuilder builder = postResponse(url, bearerToken, body);
            System.out.println("createInvestmentTransaction : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * {
     * "AmountType": "All",//All, Percentage, Dollars, Units
     * "ExternalReference": "string", "Amount": 0, "Portfolio": "string" }
     */
    // url /investment/{investmentCode}/action/redemption POST
//  Requests a application transaction for the given investment 
    public StringBuilder createInvestmentTransactionRedemption(String bearerToken, String investmentCode, String body) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/investment/").concat(investmentCode).concat("/action/redemption"));
            StringBuilder builder = postResponse(url, bearerToken, body);
            System.out.println("createInvestmentTransaction : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * [{ "FromPortfolio": "string", "AmountType": "All", //All, Percentage,
     * Dollars, Units "Amount": 0, "ToPortfolio": "string" }]
     */
// url/beneficiary/{beneficiaryId}/investment/{investmentCode}/action/switch POST
    //Requests a switch transaction for the given beneficiary and investment
    public StringBuilder swithTransaction(String bearerToken, String beneficiaryId, String investmentCode, String body) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/beneficiary/").concat(beneficiaryId).concat("/investment/").concat(investmentCode).concat("/action/switch"));
            StringBuilder builder = postResponse(url, bearerToken, body);
            System.out.println("currentApplicationById : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
