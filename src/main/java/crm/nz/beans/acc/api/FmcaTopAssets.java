/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ADMIN
 */
public class FmcaTopAssets implements ToObjectConverter {

    String AssetName, AssetType, AssetClass, AsAtDate, id, portfolio, investmentName,prices,colorCode;

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }
    List <String> Portfolios;
    Double Percentage, InvestorAssetValue, price;

    public String getPrices() {
        return prices;
    }

    public void setPrices(String prices) {
        this.prices = prices;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(String portfolio) {
        this.portfolio = portfolio;
    }

    public String getInvestmentName() {
        return investmentName;
    }

    public void setInvestmentName(String investmentName) {
        this.investmentName = investmentName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(FmcaTopAssets.class);

    public String getAssetName() {
        return AssetName;
    }

    public void setAssetName(String AssetName) {
        this.AssetName = AssetName;
    }

    public String getAssetType() {
        return AssetType;
    }

    public void setAssetType(String AssetType) {
        this.AssetType = AssetType;
    }

    public String getAssetClass() {
        return AssetClass;
    }

    public void setAssetClass(String AssetClass) {
        this.AssetClass = AssetClass;
    }

    public String getAsAtDate() {
        return AsAtDate;
    }

    public void setAsAtDate(String AsAtDate) {
        this.AsAtDate = AsAtDate;
    }

    public List<String> getPortfolios() {
        return Portfolios;
    }

    public void setPortfolios(List<String> Portfolios) {
        this.Portfolios = Portfolios;
    }

    public Double getPercentage() {
        return Percentage;
    }

    public void setPercentage(Double Percentage) {
        this.Percentage = Percentage;
    }

    public Double getInvestorAssetValue() {
        return InvestorAssetValue;
    }

    public void setInvestorAssetValue(Double InvestorAssetValue) {
        this.InvestorAssetValue = InvestorAssetValue;
    }

    @Override

    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

}
