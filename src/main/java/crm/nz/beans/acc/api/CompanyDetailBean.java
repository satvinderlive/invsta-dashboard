/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ADMIN
 */
public class CompanyDetailBean implements ToObjectConverter {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CompanyDetailBean.class);
    private String id;

    String holder_email, email, password, tokan, companyName, countryCode, dateOfInco, registerNumber, companyAddress, postalAddress, investAdviser, companyAdvisor, selectAdviser, fname, dateOfBirth, occupation,
            holderCountryOfResidence, positionInCompany, address, countryCode3, mobileNo, otherNumber, otherCountryCode, otherMobileNo,
            srcOfFund, licenseNumber, licenseExpiryDate, versionNumber, passportNumber, passportExpiryDate, countryOfIssue,
            typeOfID, typeCountryOfIssue, myFile, countryOfResidence, irdNumber, citizenUS, countryOfTaxResidence, taxIdentityNumber,
            pir, companyIRDNumber, sourceOfFunds, detail, isCompanyListed, isCompanyGovernment, isCompanyNZPolice, isCompanyFinancialInstitution,
            isCompanyActivePassive, isCompanyUSCitizen, companyCountryOfTaxResidence, companyTaxIdentityNumber, companyReasonTIN, bankName, nameOfAccount,
            reg_type, accountNumber, step, status, created_by, crearted_ts,isInvestor_idverified;
    private String countryofincorporation;
    private String raw_password;
    private String reg_id;
    private List<CountryTINBean> countryTINList;
    private List<CompanyDetailBean> moreInvestorList;

    public List<CountryTINBean> getCountryTINList() {
        return countryTINList;
    }

    public void setCountryTINList(List<CountryTINBean> countryTINList) {
        this.countryTINList = countryTINList;
    }

    public String getHolder_email() {
        return holder_email;
    }

    public void setHolder_email(String holder_email) {
        this.holder_email = holder_email;
    }

    public String getIsInvestor_idverified() {
        return isInvestor_idverified;
    }

    public void setIsInvestor_idverified(String isInvestor_idverified) {
        this.isInvestor_idverified = isInvestor_idverified;
    }
    
    

    public String getReg_type() {
        return reg_type;
    }

    public void setReg_type(String reg_type) {
        this.reg_type = reg_type;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCrearted_ts() {
        return crearted_ts;
    }

    public void setCrearted_ts(String crearted_ts) {
        this.crearted_ts = crearted_ts;
    }

    public String getTokan() {
        return tokan;
    }

    public void setTokan(String tokan) {
        this.tokan = tokan;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDateOfInco() {
        return dateOfInco;
    }

    public void setDateOfInco(String dateOfInco) {
        this.dateOfInco = dateOfInco;
    }

    public String getRegisterNumber() {
        return registerNumber;
    }

    public void setRegisterNumber(String registerNumber) {
        this.registerNumber = registerNumber;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getInvestAdviser() {
        return investAdviser;
    }

    public void setInvestAdviser(String investAdviser) {
        this.investAdviser = investAdviser;
    }

    public String getCompanyAdvisor() {
        return companyAdvisor;
    }

    public void setCompanyAdvisor(String companyAdvisor) {
        this.companyAdvisor = companyAdvisor;
    }

    public String getSelectAdviser() {
        return selectAdviser;
    }

    public void setSelectAdviser(String selectAdviser) {
        this.selectAdviser = selectAdviser;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getHolderCountryOfResidence() {
        return holderCountryOfResidence;
    }

    public void setHolderCountryOfResidence(String holderCountryOfResidence) {
        this.holderCountryOfResidence = holderCountryOfResidence;
    }

    public String getPositionInCompany() {
        return positionInCompany;
    }

    public void setPositionInCompany(String positionInCompany) {
        this.positionInCompany = positionInCompany;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountryCode3() {
        return countryCode3;
    }

    public void setCountryCode3(String countryCode3) {
        this.countryCode3 = countryCode3;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getOtherNumber() {
        return otherNumber;
    }

    public void setOtherNumber(String otherNumber) {
        this.otherNumber = otherNumber;
    }

    public String getOtherCountryCode() {
        return otherCountryCode;
    }

    public void setOtherCountryCode(String otherCountryCode) {
        this.otherCountryCode = otherCountryCode;
    }

    public String getOtherMobileNo() {
        return otherMobileNo;
    }

    public void setOtherMobileNo(String otherMobileNo) {
        this.otherMobileNo = otherMobileNo;
    }

    public String getSrcOfFund() {
        return srcOfFund;
    }

    public void setSrcOfFund(String srcOfFund) {
        this.srcOfFund = srcOfFund;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getLicenseExpiryDate() {
        return licenseExpiryDate;
    }

    public void setLicenseExpiryDate(String licenseExpiryDate) {
        this.licenseExpiryDate = licenseExpiryDate;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getPassportExpiryDate() {
        return passportExpiryDate;
    }

    public void setPassportExpiryDate(String passportExpiryDate) {
        this.passportExpiryDate = passportExpiryDate;
    }

    public String getCountryOfIssue() {
        return countryOfIssue;
    }

    public void setCountryOfIssue(String countryOfIssue) {
        this.countryOfIssue = countryOfIssue;
    }

    public String getTypeOfID() {
        return typeOfID;
    }

    public void setTypeOfID(String typeOfID) {
        this.typeOfID = typeOfID;
    }

    public String getTypeCountryOfIssue() {
        return typeCountryOfIssue;
    }

    public void setTypeCountryOfIssue(String typeCountryOfIssue) {
        this.typeCountryOfIssue = typeCountryOfIssue;
    }

    public String getMyFile() {
        return myFile;
    }

    public void setMyFile(String myFile) {
        this.myFile = myFile;
    }

    public String getCountryOfResidence() {
        return countryOfResidence;
    }

    public void setCountryOfResidence(String countryOfResidence) {
        this.countryOfResidence = countryOfResidence;
    }

    public String getIrdNumber() {
        return irdNumber;
    }

    public void setIrdNumber(String irdNumber) {
        this.irdNumber = irdNumber;
    }

    public String getCitizenUS() {
        return citizenUS;
    }

    public void setCitizenUS(String citizenUS) {
        this.citizenUS = citizenUS;
    }

    public String getCountryOfTaxResidence() {
        return countryOfTaxResidence;
    }

    public void setCountryOfTaxResidence(String countryOfTaxResidence) {
        this.countryOfTaxResidence = countryOfTaxResidence;
    }

    public String getTaxIdentityNumber() {
        return taxIdentityNumber;
    }

    public void setTaxIdentityNumber(String taxIdentityNumber) {
        this.taxIdentityNumber = taxIdentityNumber;
    }

    public String getPir() {
        return pir;
    }

    public void setPir(String pir) {
        this.pir = pir;
    }

    public String getCompanyIRDNumber() {
        return companyIRDNumber;
    }

    public void setCompanyIRDNumber(String companyIRDNumber) {
        this.companyIRDNumber = companyIRDNumber;
    }

    public String getSourceOfFunds() {
        return sourceOfFunds;
    }

    public void setSourceOfFunds(String sourceOfFunds) {
        this.sourceOfFunds = sourceOfFunds;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getIsCompanyListed() {
        return isCompanyListed;
    }

    public void setIsCompanyListed(String isCompanyListed) {
        this.isCompanyListed = isCompanyListed;
    }

    public String getIsCompanyGovernment() {
        return isCompanyGovernment;
    }

    public void setIsCompanyGovernment(String isCompanyGovernment) {
        this.isCompanyGovernment = isCompanyGovernment;
    }

    public String getIsCompanyNZPolice() {
        return isCompanyNZPolice;
    }

    public void setIsCompanyNZPolice(String isCompanyNZPolice) {
        this.isCompanyNZPolice = isCompanyNZPolice;
    }

    public String getIsCompanyFinancialInstitution() {
        return isCompanyFinancialInstitution;
    }

    public void setIsCompanyFinancialInstitution(String isCompanyFinancialInstitution) {
        this.isCompanyFinancialInstitution = isCompanyFinancialInstitution;
    }

    public String getIsCompanyActivePassive() {
        return isCompanyActivePassive;
    }

    public void setIsCompanyActivePassive(String isCompanyActivePassive) {
        this.isCompanyActivePassive = isCompanyActivePassive;
    }

    public String getIsCompanyUSCitizen() {
        return isCompanyUSCitizen;
    }

    public void setIsCompanyUSCitizen(String isCompanyUSCitizen) {
        this.isCompanyUSCitizen = isCompanyUSCitizen;
    }

    public String getCompanyCountryOfTaxResidence() {
        return companyCountryOfTaxResidence;
    }

    public void setCompanyCountryOfTaxResidence(String companyCountryOfTaxResidence) {
        this.companyCountryOfTaxResidence = companyCountryOfTaxResidence;
    }

    public String getCompanyTaxIdentityNumber() {
        return companyTaxIdentityNumber;
    }

    public void setCompanyTaxIdentityNumber(String companyTaxIdentityNumber) {
        this.companyTaxIdentityNumber = companyTaxIdentityNumber;
    }

    public String getCompanyReasonTIN() {
        return companyReasonTIN;
    }

    public void setCompanyReasonTIN(String companyReasonTIN) {
        this.companyReasonTIN = companyReasonTIN;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getNameOfAccount() {
        return nameOfAccount;
    }

    public void setNameOfAccount(String nameOfAccount) {
        this.nameOfAccount = nameOfAccount;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String getReg_id() {
        return reg_id;
    }

    public void setReg_id(String reg_id) {
        this.reg_id = reg_id;
    }

    public List<CompanyDetailBean> getMoreInvestorList() {
        return moreInvestorList;
    }

    public void setMoreInvestorList(List<CompanyDetailBean> moreInvestorList) {
        this.moreInvestorList = moreInvestorList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRaw_password() {
        return raw_password;
    }

    public void setRaw_password(String raw_password) {
        this.raw_password = raw_password;
    }

    public String getCountryofincorporation() {
        return countryofincorporation;
    }

    public void setCountryofincorporation(String countryofincorporation) {
        this.countryofincorporation = countryofincorporation;
    }

  
    }
