/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

/**
 *
 * @author IESL
 */
public class NZTADriverLicense {

    private String reportingReference;
    private String firstName;
    private String middleName;
    private String lastName;
    private String dateOfBirth;
    private String driversLicenceNo;
    private String driversLicenceVersion;

    /**
     * @return the reportingReference
     */
    public String getReportingReference() {
        return reportingReference;
    }

    /**
     * @param reportingReference the reportingReference to set
     */
    public void setReportingReference(String reportingReference) {
        this.reportingReference = reportingReference;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the dateOfBirth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth the dateOfBirth to set
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return the driversLicenceNo
     */
    public String getDriversLicenceNo() {
        return driversLicenceNo;
    }

    /**
     * @param driversLicenceNo the driversLicenceNo to set
     */
    public void setDriversLicenceNo(String driversLicenceNo) {
        this.driversLicenceNo = driversLicenceNo;
    }

    /**
     * @return the driversLicenceVersion
     */
    public String getDriversLicenceVersion() {
        return driversLicenceVersion;
    }

    /**
     * @param driversLicenceVersion the driversLicenceVersion to set
     */
    public void setDriversLicenceVersion(String driversLicenceVersion) {
        this.driversLicenceVersion = driversLicenceVersion;
    }

    public String requestBody() {
        return "{\n"
                + "    \"dataSources\": [\n"
                + "        \"NZTA Drivers License\"\n"
                + "    ],\n"
                + "    \"reportingReference\": " + (reportingReference != null ? "\"" + reportingReference + "\"" : null) + ",\n"
                + "    \"firstName\": " + (firstName != null ? "\"" + firstName + "\"" : null) + ",\n"
                + "    \"middleName\": " + (middleName != null ? "\"" + middleName + "\"" : null) + ",\n"
                + "    \"lastName\": " + (lastName != null ? "\"" + lastName + "\"" : null) + ",\n"
                + "    \"dateOfBirth\": " + (dateOfBirth != null ? "\"" + dateOfBirth + "\"" : null) + ",\n"
                + "    \"driversLicenceNo\": " + (driversLicenceNo != null ? "\"" + driversLicenceNo + "\"" : null) + ",\n"
                + "    \"driversLicenceVersion\": " + (driversLicenceVersion != null ? "\"" + driversLicenceVersion + "\"" : null) + ",\n"
                + "    \"driversLicenceConsentObtained\": true\n"
                + "}";
    }

}
