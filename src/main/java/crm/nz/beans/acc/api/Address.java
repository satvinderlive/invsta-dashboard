/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;
/**
 *
 * @author IESL
 */
public class Address implements ToObjectConverter{

    String EffectiveDate, InEffectiveDate, AddressID, Country, CountryCode, Type, AddressLine1, AddressLine2,
    AddressLine3, AddressLine4, City, Region, State, PostalCode, FormattedAddress, BeneficiaryId;

    public String getBeneficiaryId() {
        return BeneficiaryId;
    }

    public void setBeneficiaryId(String BeneficiaryId) {
        this.BeneficiaryId = BeneficiaryId;
    }
    boolean IsPrimary;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Address.class);

    
    
    public String getEffectiveDate() {
        return EffectiveDate;
    }

    public void setEffectiveDate(String EffectiveDate) {
        this.EffectiveDate = EffectiveDate;
    }

    public String getInEffectiveDate() {
        return InEffectiveDate;
    }

    public void setInEffectiveDate(String InEffectiveDate) {
        this.InEffectiveDate = InEffectiveDate;
    }

    public String getAddressID() {
        return AddressID;
    }

    public void setAddressID(String AddressID) {
        this.AddressID = AddressID;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String CountryCode) {
        this.CountryCode = CountryCode;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getAddressLine1() {
        return AddressLine1;
    }

    public void setAddressLine1(String AddressLine1) {
        this.AddressLine1 = AddressLine1;
    }

    public String getAddressLine2() {
        return AddressLine2;
    }

    public void setAddressLine2(String AddressLine2) {
        this.AddressLine2 = AddressLine2;
    }

    public String getAddressLine3() {
        return AddressLine3;
    }

    public void setAddressLine3(String AddressLine3) {
        this.AddressLine3 = AddressLine3;
    }

    public String getAddressLine4() {
        return AddressLine4;
    }

    public void setAddressLine4(String AddressLine4) {
        this.AddressLine4 = AddressLine4;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String Region) {
        this.Region = Region;
    }

    public String getState() {
        return State;
    }

    public void setState(String State) {
        this.State = State;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String PostalCode) {
        this.PostalCode = PostalCode;
    }

    public String getFormattedAddress() {
        return FormattedAddress;
    }

    public void setFormattedAddress(String FormattedAddress) {
        this.FormattedAddress = FormattedAddress;
    }

    public boolean isIsPrimary() {
        return IsPrimary;
    }

    public void setIsPrimary(boolean IsPrimary) {
        this.IsPrimary = IsPrimary;
    }
    
   

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }
}
