/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;


import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author innovative002
 */
public class PerformanceBean implements ToObjectConverter {
    
    
      private  String date,data,onemonth,onemonthTrending,threemonth,threemonthTrending,oneyear, oneyeartrending,fiveyear,portfolio,unit_price,fund_size,one_year_return,riskIndicator;
private  static Logger logger = LoggerFactory.getLogger(PerformanceBean.class);
    public String getPortfolio() {
        return portfolio;
    }

    public String getRiskIndicator() {
        return riskIndicator;
    }

    public void setRiskIndicator(String riskIndicator) {
        this.riskIndicator = riskIndicator;
    }

    public String getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(String unit_price) {
        this.unit_price = unit_price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getFund_size() {
        return fund_size;
    }

    public void setFund_size(String fund_size) {
        this.fund_size = fund_size;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        PerformanceBean.logger = logger;
    }

    public String getOne_year_return() {
        return one_year_return;
    }

    public void setOne_year_return(String one_year_return) {
        this.one_year_return = one_year_return;
    }

    public void setPortfolio(String portfolio) {
        this.portfolio = portfolio;
    }

    public String getOnemonth() {
        return onemonth;
    }

    public void setOnemonth(String onemonth) {
        this.onemonth = onemonth;
    }

    public String getOnemonthTrending() {
        return onemonthTrending;
    }

    public void setOnemonthTrending(String onemonthTrending) {
        this.onemonthTrending = onemonthTrending;
    }

    public String getThreemonth() {
        return threemonth;
    }

    public void setThreemonth(String threemonth) {
        this.threemonth = threemonth;
    }

    public String getThreemonthTrending() {
        return threemonthTrending;
    }

    public void setThreemonthTrending(String threemonthTrending) {
        this.threemonthTrending = threemonthTrending;
    }

    public String getOneyear() {
        return oneyear;
    }

    public void setOneyear(String oneyear) {
        this.oneyear = oneyear;
    }

    public String getOneyeartrending() {
        return oneyeartrending;
    }

    public void setOneyeartrending(String oneyeartrending) {
        this.oneyeartrending = oneyeartrending;
    }

    public String getFiveyear() {
        return fiveyear;
    }

    public void setFiveyear(String fiveyear) {
        this.fiveyear = fiveyear;
    }
 
    
    
        @Override

    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }
    
    
    
    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
              
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

}
