/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

/**
 *
 * @author IESL
 */
public class Status {

    Boolean AllAMLBeneficaryChecksPassed;
    String LastReason;
    Boolean IsComplete;
    String ApplicationId;
    Integer AMLStatusId;

    public Boolean getAllAMLBeneficaryChecksPassed() {
        return AllAMLBeneficaryChecksPassed;
    }

    public void setAllAMLBeneficaryChecksPassed(Boolean AllAMLBeneficaryChecksPassed) {
        this.AllAMLBeneficaryChecksPassed = AllAMLBeneficaryChecksPassed;
    }

    public String getLastReason() {
        return LastReason;
    }

    public void setLastReason(String LastReason) {
        this.LastReason = LastReason;
    }

    public Boolean getIsComplete() {
        return IsComplete;
    }

    public void setIsComplete(Boolean IsComplete) {
        this.IsComplete = IsComplete;
    }

    public String getApplicationId() {
        return ApplicationId;
    }

    public void setApplicationId(String ApplicationId) {
        this.ApplicationId = ApplicationId;
    }

    public Integer getAMLStatusId() {
        return AMLStatusId;
    }

    public void setAMLStatusId(Integer AMLStatusId) {
        this.AMLStatusId = AMLStatusId;
    }

}
