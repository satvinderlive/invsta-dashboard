/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

/**
 *
 * @author IESL
 */
public class NZTADLResponse {

    private String reportingReference;
    private Boolean safeHarbour;
    private DriverLicence driversLicence;

    /**
     * @return the reportingReference
     */
    public String getReportingReference() {
        return reportingReference;
    }

    /**
     * @param reportingReference the reportingReference to set
     */
    public void setReportingReference(String reportingReference) {
        this.reportingReference = reportingReference;
    }

    /**
     * @return the safeHarbour
     */
    public Boolean getSafeHarbour() {
        return safeHarbour;
    }

    /**
     * @param safeHarbour the safeHarbour to set
     */
    public void setSafeHarbour(Boolean safeHarbour) {
        this.safeHarbour = safeHarbour;
    }

    /**
     * @return the driversLicence
     */
    public DriverLicence getDriversLicence() {
        return driversLicence;
    }

    /**
     * @param driversLicence the driversLicence to set
     */
    public void setDriversLicence(DriverLicence driversLicence) {
        this.driversLicence = driversLicence;
    }

}
