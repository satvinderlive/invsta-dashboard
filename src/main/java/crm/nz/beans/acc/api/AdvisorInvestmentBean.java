/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;

/**
 *
 * @author innovative002
 */
public class AdvisorInvestmentBean {

    private String investmentCode, effectiveDate, investmentType, investmentSubType, portfolioCode, portfolioName, Units,
            price, value, att_value, wdw_value, username, id, beneficiairyId, userId, advisorId, name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(AdvisorInvestmentBean.class);

    public String getInvestmentCode() {
        return investmentCode;
    }

    public void setInvestmentCode(String investmentCode) {
        this.investmentCode = investmentCode;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getInvestmentType() {
        return investmentType;
    }

    public void setInvestmentType(String investmentType) {
        this.investmentType = investmentType;
    }

    public String getInvestmentSubType() {
        return investmentSubType;
    }

    public void setInvestmentSubType(String investmentSubType) {
        this.investmentSubType = investmentSubType;
    }

    public String getPortfolioCode() {
        return portfolioCode;
    }

    public void setPortfolioCode(String portfolioCode) {
        this.portfolioCode = portfolioCode;
    }

    public String getPortfolioName() {
        return portfolioName;
    }

    public void setPortfolioName(String portfolioName) {
        this.portfolioName = portfolioName;
    }

    public String getUnits() {
        return Units;
    }

    public void setUnits(String Units) {
        this.Units = Units;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBeneficiairyId() {
        return beneficiairyId;
    }

    public void setBeneficiairyId(String beneficiairyId) {
        this.beneficiairyId = beneficiairyId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAdvisorId() {
        return advisorId;
    }

    public void setAdvisorId(String advisorId) {
        this.advisorId = advisorId;
    }

    @Override
    public String toString() {
        return toString(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    /**
     * @return the att_value
     */
    public String getAtt_value() {
        return att_value;
    }

    /**
     * @param att_value the att_value to set
     */
    public void setAtt_value(String att_value) {
        this.att_value = att_value;
    }

    /**
     * @return the wdw_value
     */
    public String getWdw_value() {
        return wdw_value;
    }

    /**
     * @param wdw_value the wdw_value to set
     */
    public void setWdw_value(String wdw_value) {
        this.wdw_value = wdw_value;
    }
}
