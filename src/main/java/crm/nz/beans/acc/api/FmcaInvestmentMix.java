/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ADMIN
 */
public class FmcaInvestmentMix implements ToObjectConverter {

    String id, Portfolio, FMCAAssetClass, AsAtDate, InvestmentCode, BeneficiaryId, InvestmentName;
    private String colorCode;
    Double SectorValueBase, Percentage ,Price;

    public String getInvestmentName() {
        return InvestmentName;
    }

    public void setInvestmentName(String InvestmentName) {
        this.InvestmentName = InvestmentName;
    }

    public Double getPrice() {
        return Price;
    }

    public void setPrice(Double Price) {
        this.Price = Price;
    }
  
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(FmcaTopAssets.class);

    public String getInvestmentCode() {
        return InvestmentCode;
    }

    public void setInvestmentCode(String InvestmentCode) {
        this.InvestmentCode = InvestmentCode;
    }

    public String getBeneficiaryId() {
        return BeneficiaryId;
    }

    public void setBeneficiaryId(String BeneficiaryId) {
        this.BeneficiaryId = BeneficiaryId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPortfolio() {
        return Portfolio;
    }

    public void setPortfolio(String Portfolio) {
        this.Portfolio = Portfolio;
    }

    public String getFMCAAssetClass() {
        return FMCAAssetClass;
    }

    public void setFMCAAssetClass(String FMCAAssetClass) {
        this.FMCAAssetClass = FMCAAssetClass;
    }

    public String getAsAtDate() {
        return AsAtDate;
    }

    public void setAsAtDate(String AsAtDate) {
        this.AsAtDate = AsAtDate;
    }

    public Double getSectorValueBase() {
        return SectorValueBase;
    }

    public void setSectorValueBase(Double SectorValueBase) {
        this.SectorValueBase = SectorValueBase;
    }

    public Double getPercentage() {
        return Percentage;
    }

    public void setPercentage(Double Percentage) {
        this.Percentage = Percentage;
    }

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }
}
