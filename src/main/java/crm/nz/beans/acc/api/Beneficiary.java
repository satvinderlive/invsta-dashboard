/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 *
 * @author IESL
 */
public class Beneficiary implements ToObjectConverter {

    private BigDecimal CurrentPirRate;
    private Long Id;
    private String IrdNumber, CustomerNumber, AccountOf, Name, DateOfBirth, ClientAccountStartDate,
            FirstName, LastName, AccessLevel, AMLEntityType, Status, PreferredFirstName, PreferredName, BeneficiaryId, UserId,
            ApplicationId, Title, SecondName, MiddleName, Gender, PlaceOfBirth, Nation, Occupation, OccupationStatus, IRDNo,
            Email, SecondaryEmail, PreferredComm, TINNumber, TINNumber2, TINNumber3, TINCountry, TINCountry2, TINCountry3, CountryOfResidency, ExternalReference,
            UserDefined1, UserDefined2, UserDefined3, UserDefined4, UserDefined5, UserDefined6, UserDefined7, UserDefined8, UserDefined9, UserDefined10,total;
    private List<PIRRate> PIRRates;
    private List<PhoneNumber> PhoneNumbers;
    private List<Address> Addresses;
    private List<BankAccountDetail> BankAccountDetail;
    private List<Email> Emails;
    private List<Identification> Identification;
    private List<Investment> Investments;
    private List<InvestmentHolding> InvestmentHoldings;
    private Integer PIRRate;
    private Boolean IsUSTaxResident, ConsentToCheckId;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Beneficiary.class);

    public String getApplicationId() {
        return ApplicationId;
    }

    public void setApplicationId(String ApplicationId) {
        this.ApplicationId = ApplicationId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getSecondName() {
        return SecondName;
    }

    public void setSecondName(String SecondName) {
        this.SecondName = SecondName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String MiddleName) {
        this.MiddleName = MiddleName;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public String getPlaceOfBirth() {
        return PlaceOfBirth;
    }

    public void setPlaceOfBirth(String PlaceOfBirth) {
        this.PlaceOfBirth = PlaceOfBirth;
    }

    public String getNation() {
        return Nation;
    }

    public void setNation(String Nation) {
        this.Nation = Nation;
    }

    public String getOccupation() {
        return Occupation;
    }

    public void setOccupation(String Occupation) {
        this.Occupation = Occupation;
    }

    public String getOccupationStatus() {
        return OccupationStatus;
    }

    public void setOccupationStatus(String OccupationStatus) {
        this.OccupationStatus = OccupationStatus;
    }

    public String getIRDNo() {
        return IRDNo;
    }

    public void setIRDNo(String IRDNo) {
        this.IRDNo = IRDNo;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getSecondaryEmail() {
        return SecondaryEmail;
    }

    public void setSecondaryEmail(String SecondaryEmail) {
        this.SecondaryEmail = SecondaryEmail;
    }

    public String getPreferredComm() {
        return PreferredComm;
    }

    public void setPreferredComm(String PreferredComm) {
        this.PreferredComm = PreferredComm;
    }

    public String getTINNumber() {
        return TINNumber;
    }

    public void setTINNumber(String TINNumber) {
        this.TINNumber = TINNumber;
    }

    public String getTINNumber2() {
        return TINNumber2;
    }

    public void setTINNumber2(String TINNumber2) {
        this.TINNumber2 = TINNumber2;
    }

    public String getTINNumber3() {
        return TINNumber3;
    }

    public void setTINNumber3(String TINNumber3) {
        this.TINNumber3 = TINNumber3;
    }

    public String getTINCountry() {
        return TINCountry;
    }

    public void setTINCountry(String TINCountry) {
        this.TINCountry = TINCountry;
    }

    public String getTINCountry2() {
        return TINCountry2;
    }

    public void setTINCountry2(String TINCountry2) {
        this.TINCountry2 = TINCountry2;
    }

    public String getTINCountry3() {
        return TINCountry3;
    }

    public void setTINCountry3(String TINCountry3) {
        this.TINCountry3 = TINCountry3;
    }

    public String getCountryOfResidency() {
        return CountryOfResidency;
    }

    public void setCountryOfResidency(String CountryOfResidency) {
        this.CountryOfResidency = CountryOfResidency;
    }

    public String getExternalReference() {
        return ExternalReference;
    }

    public void setExternalReference(String ExternalReference) {
        this.ExternalReference = ExternalReference;
    }

    public String getUserDefined1() {
        return UserDefined1;
    }

    public void setUserDefined1(String UserDefined1) {
        this.UserDefined1 = UserDefined1;
    }

    public String getUserDefined2() {
        return UserDefined2;
    }

    public void setUserDefined2(String UserDefined2) {
        this.UserDefined2 = UserDefined2;
    }

    public String getUserDefined3() {
        return UserDefined3;
    }

    public void setUserDefined3(String UserDefined3) {
        this.UserDefined3 = UserDefined3;
    }

    public String getUserDefined4() {
        return UserDefined4;
    }

    public void setUserDefined4(String UserDefined4) {
        this.UserDefined4 = UserDefined4;
    }

    public String getUserDefined5() {
        return UserDefined5;
    }

    public void setUserDefined5(String UserDefined5) {
        this.UserDefined5 = UserDefined5;
    }

    public String getUserDefined6() {
        return UserDefined6;
    }

    public void setUserDefined6(String UserDefined6) {
        this.UserDefined6 = UserDefined6;
    }

    public String getUserDefined7() {
        return UserDefined7;
    }

    public void setUserDefined7(String UserDefined7) {
        this.UserDefined7 = UserDefined7;
    }

    public String getUserDefined8() {
        return UserDefined8;
    }

    public void setUserDefined8(String UserDefined8) {
        this.UserDefined8 = UserDefined8;
    }

    public String getUserDefined9() {
        return UserDefined9;
    }

    public void setUserDefined9(String UserDefined9) {
        this.UserDefined9 = UserDefined9;
    }

    public String getUserDefined10() {
        return UserDefined10;
    }

    public void setUserDefined10(String UserDefined10) {
        this.UserDefined10 = UserDefined10;
    }

    public Integer getPIRRate() {
        return PIRRate;
    }

    public void setPIRRate(Integer PIRRate) {
        this.PIRRate = PIRRate;
    }

    public Boolean getIsUSTaxResident() {
        return IsUSTaxResident;
    }

    public void setIsUSTaxResident(Boolean IsUSTaxResident) {
        this.IsUSTaxResident = IsUSTaxResident;
    }

    public Boolean getConsentToCheckId() {
        return ConsentToCheckId;
    }

    public void setConsentToCheckId(Boolean ConsentToCheckId) {
        this.ConsentToCheckId = ConsentToCheckId;
    }

    public List<Investment> getInvestments() {
        return Investments;
    }

    public void setInvestments(List<Investment> Investments) {
        this.Investments = Investments;
    }

    public BigDecimal getCurrentPirRate() {
        return CurrentPirRate;
    }

    public void setCurrentPirRate(BigDecimal CurrentPirRate) {
        this.CurrentPirRate = CurrentPirRate;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
        this.BeneficiaryId = String.valueOf(Id);
    }

    public String getIrdNumber() {
        return IrdNumber;
    }

    public void setIrdNumber(String IrdNumber) {
        this.IrdNumber = IrdNumber;
    }

    public String getCustomerNumber() {
        return CustomerNumber;
    }

    public void setCustomerNumber(String CustomerNumber) {
        this.CustomerNumber = CustomerNumber;
    }

    public String getAccountOf() {
        return AccountOf;
    }

    public void setAccountOf(String AccountOf) {
        this.AccountOf = AccountOf;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String DateOfBirth) {
        this.DateOfBirth = DateOfBirth;
    }

    public String getClientAccountStartDate() {
        return ClientAccountStartDate;
    }

    public void setClientAccountStartDate(String ClientAccountStartDate) {
        this.ClientAccountStartDate = ClientAccountStartDate;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getAccessLevel() {
        return AccessLevel;
    }

    public void setAccessLevel(String AccessLevel) {
        this.AccessLevel = AccessLevel;
    }

    public String getAMLEntityType() {
        return AMLEntityType;
    }

    public void setAMLEntityType(String AMLEntityType) {
        this.AMLEntityType = AMLEntityType;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getPreferredFirstName() {
        return PreferredFirstName;
    }

    public void setPreferredFirstName(String PreferredFirstName) {
        this.PreferredFirstName = PreferredFirstName;
    }

    public String getPreferredName() {
        return PreferredName;
    }

    public void setPreferredName(String PreferredName) {
        this.PreferredName = PreferredName;
    }

    public List<PhoneNumber> getPhoneNumbers() {
        return PhoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneNumber> PhoneNumbers) {
        this.PhoneNumbers = PhoneNumbers;
    }

    public List<Address> getAddresses() {
        return Addresses;
    }

    public void setAddresses(List<Address> Addresses) {
        this.Addresses = Addresses;
    }

    public List<Email> getEmails() {
        return Emails;
    }

    public void setEmails(List<Email> Emails) {
        this.Emails = Emails;
    }

    public List<Identification> getIdentification() {
        return Identification;
    }

    public void setIdentification(List<Identification> Identification) {
        this.Identification = Identification;
    }

    public String getBeneficiaryId() {
        return BeneficiaryId;
    }

    public void setBeneficiaryId(String BeneficiaryId) {
        this.BeneficiaryId = BeneficiaryId;
    }

    /**
     * @return the PIRRates
     */
    public List<PIRRate> getPIRRates() {
        return PIRRates;
    }

    /**
     * @param PIRRates the PIRRates to set
     */
    public void setPIRRates(List<PIRRate> PIRRates) {
        this.PIRRates = PIRRates;
    }

    /**
     * @return the InvestmentHoldings
     */
    public List<InvestmentHolding> getInvestmentHoldings() {
        return InvestmentHoldings;
    }

    /**
     * @param InvestmentHoldings the InvestmentHoldings to set
     */
    public void setInvestmentHoldings(List<InvestmentHolding> InvestmentHoldings) {
        this.InvestmentHoldings = InvestmentHoldings;
    }

    /**
     * @return the BankAccountDetail
     */
    public List<BankAccountDetail> getBankAccountDetail() {
        return BankAccountDetail;
    }

    /**
     * @param BankAccountDetail the BankAccountDetail to set
     */
    public void setBankAccountDetail(List<BankAccountDetail> BankAccountDetail) {
        this.BankAccountDetail = BankAccountDetail;
    }

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    /**
     * @return the UserId
     */
    public String getUserId() {
        return UserId;
    }

    /**
     * @param UserId the UserId to set
     */
    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

//{
//    "email":"king@king.co.in",
//    "password":"King@0000",
//    "title":"Mr",
//    "fullName":"King",
//    "firstName":"King",
//    "lastName":"Q",
//    "date_of_Birth":"31-10-2000",
//    "country_residence":" India ",
//    "occupation":"21",
//    "working_with_adviser":"Yes",
//    "advisor_company":"1",
//    "advisor":"1",
//    "homeAddress":"Settlement Road, Opaheke, Papakura, New Zealand",
//    "mobile_country_code":"+64",
//    "mobile_number":"785765464",
//    "optional_num_type":"Home",
//    "optional_num_code":"+64",
//    "optional_num":"695595959",
//    "id_type":"NZ Driver Licence",
//    "license_number":"7897896544",
//    "licence_expiry_Date":"31-10-2029",
//    "licence_verson_number":"999",
//    "passport_number":"",
//    "passport_expiry":"",
//    "passport_issue_by":" New Zealand",
//    "other_id_type":"",
//    "other_id_expiry":"",
//    "other_id_issueBy":" New Zealand",
//    "pir":"28%",
//    "ird_number":"753-112-498",
//    "tex_residence_Country":null,
//    "tin":null,
//    "wealth_src":" employment",
//    "resn_tin_unavailable":null,
//    "bank_name":"ANZ Bank New Zealand",
//    "acount_holder_name":"Maninderjit",
//    "account_number":"78-9745-4464546-466",
//    "preferredName":"King Co",
//    "reg_id":null,
//    "tokan":null,
//    "link":null,
//    "step":"12",
//    "status":"SUBMISSION",
//    "reg_type":"INDIVIDUAL_ACCOUNT",
//    "created_ts":null,
//    "created_by":null,
//    "tins":null,
//    "reasonTins":null,
//    "taxCountries":null,
//    "countryTINList":null,
//    "logger":null
//}
    public String getThirdAPIUpdateDetails(PersonDetailsBean person) {
        return "{\n"
                + "  \"ApplicationId\": " + ApplicationId + ",\n"
                + "  \"BeneficiaryId\": " + BeneficiaryId + ",\n"
                + "  \"Title\": \"" + person.getTitle() + "\",\n"
                + "  \"FirstName\": \"" + person.getFirstName() + "\",\n"
                + "  \"SecondName\": " + (SecondName != null ? "\"" + SecondName + "\"" : null) + ",\n"
                + "  \"LastName\": " + (person.getLastName() != null ? "\"" + person.getLastName() + "\"" : null) + ",\n"
                + "  \"MiddleName\": " + (person.getMiddleName() != null ? "\"" + person.getMiddleName() + "\"" : null) + ",\n"
                + "  \"PreferredName\": \"" + person.getPreferredName() + "\",\n"
                + "  \"Gender\": " + (person.getGender()  != null ? "\"" + person.getGender() + "\"" : null) + ",\n"
                + "  \"DateOfBirth\": \"" + person.getDate_of_Birth() + "\",\n"
                + "  \"PlaceOfBirth\": \"Home\",\n"
                + "  \"Nation\": \"" + person.getCountry_residence() + "\",\n"
                + "  \"Occupation\": " + person.getOccupation() + ",\n"
                + "  \"OccupationStatus\": \"" + person.getOccupationStatus() + "\",\n"
                + "  \"IRDNo\": \"" + person.getIrd_number() + "\",\n"
                + "  \"IsUSTaxResident\": " + person.getIsUSTaxResident() + ",\n"
                + "  \"PIRRate\": " + person.getPir() + ",\n"
                + "  \"ConsentToCheckId\": " + ConsentToCheckId + ",\n"
                + "  \"Email\": \"" + person.getEmail() + "\",\n"
                + "  \"SecondaryEmail\": \"" + person.getEmail() + "\",\n"
                + "  \"PreferredComm\": " + (PreferredComm != null ? "\"" + PreferredComm + "\"" : null) + ",\n"
                + "  \"TINNumber\": " + (person.getTin() != null ? "\"" + person.getTin() + "\"" : null) + ",\n"
                + "  \"CountryOfResidency\": \"" + person.getCountry_residence() + "\",\n"
                + "  \"ExternalReference\": \"INV#" + ApplicationId + "\"\n"
                + "}";
    }
    
    public String getThirdAPIUpdateDetails(JointDetailBean person) {
        return "{\n"
                + "  \"ApplicationId\": " + ApplicationId + ",\n"
                + "  \"BeneficiaryId\": " + BeneficiaryId + ",\n"
                + "  \"Title\": \"" + person.getTitle() + "\",\n"
                + "  \"FirstName\": \"" + person.getFirstName() + "\",\n"
                + "  \"SecondName\": " + (SecondName != null ? "\"" + SecondName + "\"" : null) + ",\n"
                + "  \"LastName\": " + (person.getLastName() != null ? "\"" + person.getLastName() + "\"" : null) + ",\n"
                + "  \"MiddleName\": " + (MiddleName != null ? "\"" + MiddleName + "\"" : null) + ",\n"
                + "  \"PreferredName\": \"" + person.getPreferred_name() + "\",\n"
                + "  \"Gender\": " + (Gender != null ? "\"" + Gender + "\"" : null) + ",\n"
                + "  \"DateOfBirth\": \"" + person.getDate_of_Birth() + "\",\n"
                + "  \"PlaceOfBirth\": \"Home\",\n"
                + "  \"Nation\": \"" + person.getCountry_residence() + "\",\n"
                + "  \"Occupation\": " + person.getOccupation() + ",\n"
                + "  \"OccupationStatus\": \"" + person.getOccupationStatus() + "\",\n"
                + "  \"IRDNo\": \"" + person.getIrd_Number()+ "\",\n"
                + "  \"IsUSTaxResident\": " + IsUSTaxResident + ",\n"
                + "  \"PIRRate\": " + person.getPir() + ",\n"
                + "  \"ConsentToCheckId\": " + ConsentToCheckId + ",\n"
                + "  \"Email\": \"" + person.getEmail() + "\",\n"
                + "  \"SecondaryEmail\": \"" + person.getEmail() + "\",\n"
                + "  \"PreferredComm\": " + (PreferredComm != null ? "\"" + PreferredComm + "\"" : null) + ",\n"
                + "  \"TINNumber\": " + (person.getTin() != null ? "\"" + person.getTin() + "\"" : null) + ",\n"
                + "  \"CountryOfResidency\": \"" + person.getCountry_residence() + "\",\n"
                + "  \"ExternalReference\": \"INV#" + ApplicationId + "\"\n"
                + "}";
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

}
