/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;

/**
 *
 * @author IESL
 */
public class PhoneNumber implements ToObjectConverter {

    Long PhoneId;
    String Country, CountryCode, Number, Type, Home, BeneficiaryId;

    public String getBeneficiaryId() {
        return BeneficiaryId;
    }

    public void setBeneficiaryId(String BeneficiaryId) {
        this.BeneficiaryId = BeneficiaryId;
    };
    boolean IsPrimary;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PhoneNumber.class);

    public Long getPhoneId() {
        return PhoneId;
    }

    public void setPhoneId(Long PhoneId) {
        this.PhoneId = PhoneId;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String CountryCode) {
        this.CountryCode = CountryCode;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String Number) {
        this.Number = Number;
    }

  

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getHome() {
        return Home;
    }

    public void setHome(String Home) {
        this.Home = Home;
    }

    public boolean isIsPrimary() {
        return IsPrimary;
    }

    public void setIsPrimary(boolean IsPrimary) {
        this.IsPrimary = IsPrimary;
    }

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }
}
