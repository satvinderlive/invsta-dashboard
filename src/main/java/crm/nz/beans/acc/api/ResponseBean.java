/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import crm.nz.constants.Constants;
import static crm.nz.table.bean.ToObjectConverter.checkNull;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 * @author IESL
 */
public class ResponseBean {

    /**
     * @return the contactList
     */
   
    public Map getRequest() {
        return request;
    }

    /**
     * @param request the request to set
     */
    public void setRequest(Map request) {
        this.request = request;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the eventList
     */


    private Map request;
    private String message;
    private int status;
    private List<PersonDetailsBean> person;

    public List<PersonDetailsBean> getPerson() {
        return person;
    }

    public void setPerson(List<PersonDetailsBean> person) {
        this.person = person;
    }
 
    @Override
    public String toString() {
        return toString(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuilder buffer = new StringBuilder();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                if (fieldValue instanceof String) {
                    String checkNull = checkNull(fieldValue);
                    if (checkNull != null) {
                        buffer.append(checkNull);
                    } else {
                        buffer.append(checkNull);
                    }
                } else {
                    buffer.append(checkNull(fieldValue));
                }
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(ResponseBean.class.getName()).throwing(ResponseBean.class.getName(), "toString(Object THIS)", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }


}
