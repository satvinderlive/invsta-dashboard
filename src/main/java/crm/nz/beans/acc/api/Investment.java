/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 *
 * @author IESL
 */
public class Investment implements ToObjectConverter {

    private List<BankAccountDetail> BankAccountDetail;
    private List<Email> Emails;
    private int id;
    private Long RegistryAccountId;
    private String BeneficiaryId, Code, ContractDate, InvestmentType, InvestmentName, Status, ExternalReference, FeeGroup, AdvisorRate, Greeting, InvestmentStrategyName;
    private Double Contributions, Withdrawals, Earnings, TotalValue, ReinvestedDistributions, CashDistributions, Tax, TaxPaid, InvestmentReturnRate, InvestmentStrategyBand;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Investment.class);

    public List<BankAccountDetail> getBankAccountDetail() {
        return BankAccountDetail;
    }

    public void setBankAccountDetail(List<BankAccountDetail> BankAccountDetail) {
        this.BankAccountDetail = BankAccountDetail;
    }

    /**
     * @return the BeneficiaryId
     */
    public String getBeneficiaryId() {
        return BeneficiaryId;
    }

    /**
     * @param BeneficiaryId the BeneficiaryId to set
     */
    public void setBeneficiaryId(String BeneficiaryId) {
        this.BeneficiaryId = BeneficiaryId;
    }

    public List<Email> getEmails() {
        return Emails;
    }

    public void setEmails(List<Email> Emails) {
        this.Emails = Emails;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getRegistryAccountId() {
        return RegistryAccountId;
    }

    public void setRegistryAccountId(Long RegistryAccountId) {
        this.RegistryAccountId = RegistryAccountId;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public String getContractDate() {
        return ContractDate;
    }

    public void setContractDate(String ContractDate) {
        this.ContractDate = ContractDate;
    }

    public String getInvestmentType() {
        return InvestmentType;
    }

    public void setInvestmentType(String InvestmentType) {
        this.InvestmentType = InvestmentType;
    }

    public String getInvestmentName() {
        return InvestmentName;
    }

    public void setInvestmentName(String InvestmentName) {
        this.InvestmentName = InvestmentName;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getExternalReference() {
        return ExternalReference;
    }

    public void setExternalReference(String ExternalReference) {
        this.ExternalReference = ExternalReference;
    }

    public String getFeeGroup() {
        return FeeGroup;
    }

    public void setFeeGroup(String FeeGroup) {
        this.FeeGroup = FeeGroup;
    }

    public String getAdvisorRate() {
        return AdvisorRate;
    }

    public void setAdvisorRate(String AdvisorRate) {
        this.AdvisorRate = AdvisorRate;
    }

    public String getGreeting() {
        return Greeting;
    }

    public void setGreeting(String Greeting) {
        this.Greeting = Greeting;
    }

    public String getInvestmentStrategyName() {
        return InvestmentStrategyName;
    }

    public void setInvestmentStrategyName(String InvestmentStrategyName) {
        this.InvestmentStrategyName = InvestmentStrategyName;
    }

    public Double getContributions() {
        return Contributions;
    }

    public void setContributions(Double Contributions) {
        this.Contributions = Contributions;
    }

    public Double getWithdrawals() {
        return Withdrawals;
    }

    public void setWithdrawals(Double Withdrawals) {
        this.Withdrawals = Withdrawals;
    }

    public Double getEarnings() {
        return Earnings;
    }

    public void setEarnings(Double Earnings) {
        this.Earnings = Earnings;
    }

    public Double getTotalValue() {
        return TotalValue;
    }

    public void setTotalValue(Double TotalValue) {
        this.TotalValue = TotalValue;
    }

    public Double getReinvestedDistributions() {
        return ReinvestedDistributions;
    }

    public void setReinvestedDistributions(Double ReinvestedDistributions) {
        this.ReinvestedDistributions = ReinvestedDistributions;
    }

    public Double getCashDistributions() {
        return CashDistributions;
    }

    public void setCashDistributions(Double CashDistributions) {
        this.CashDistributions = CashDistributions;
    }

    public Double getTax() {
        return Tax;
    }

    public void setTax(Double Tax) {
        this.Tax = Tax;
    }

    public Double getTaxPaid() {
        return TaxPaid;
    }

    public void setTaxPaid(Double TaxPaid) {
        this.TaxPaid = TaxPaid;
    }

    public Double getInvestmentReturnRate() {
        return InvestmentReturnRate;
    }

    public void setInvestmentReturnRate(Double InvestmentReturnRate) {
        this.InvestmentReturnRate = InvestmentReturnRate;
    }

    public Double getInvestmentStrategyBand() {
        return InvestmentStrategyBand;
    }

    public void setInvestmentStrategyBand(Double InvestmentStrategyBand) {
        this.InvestmentStrategyBand = InvestmentStrategyBand;
    }

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

}
