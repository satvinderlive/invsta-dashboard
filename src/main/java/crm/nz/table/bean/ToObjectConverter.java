/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.table.bean;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Administrator
 */
public interface ToObjectConverter {

    public static String toString(Object THIS) throws IllegalArgumentException, IllegalAccessException {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];

            String fieldName = field.getName();
            Object fieldValue = field.get(THIS);
            buffer.append("\"").append(fieldName).append("\"");
            buffer.append(":");
            buffer.append(checkNull(fieldValue));
            if (i != declaredFields.length - 1) {
                buffer.append(", \n");
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public static String toJSONObject(Object THIS) {
        try {
            return toString(THIS).replaceAll("\"", "");
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            return null;
        }
    }

    public static String checkNull(Object... oArr) {
        if (oArr != null) {
            Object o = oArr[0];
            Boolean toObject = false;
            if (oArr.length > 1 && (Boolean) oArr[1]) {
                toObject = true;
            }
            if (o instanceof List) {
                List list = (List) o;
                String objects = "";
                for (int i = 0; i < list.size(); i++) {
                    Object obj = list.get(i);
                    String toString = null;
                    if (toObject && obj instanceof ToObjectConverter) {
                        toString = ((ToObjectConverter) obj).toJSONObject();
                    } else {
                        toString = obj.toString();
                    }
                    objects = objects + ((i > 0 && i != list.size()) ? "," : "") + toString;
                }
                return "[" + objects + "]";
            } else if (o instanceof Map) {
                Map map = (Map) o;
                String objects = "";
                List<Map.Entry> list = new ArrayList<>(map.entrySet());
                for (int i = 0; i < list.size(); i++) {
                    Map.Entry entry = list.get(i);
                    Object key = entry.getKey();
                    Object value = entry.getValue();
                    String toString = null;
                    if (toObject && value instanceof ToObjectConverter) {
                        toString = key + ":" + ((ToObjectConverter) value).toJSONObject();
                    } else {
                        toString = "\"" + key + "\"" + ":" + value.toString();
                    }
                    objects = objects + ((i > 0 && i != list.size()) ? "," : "") + toString;
                }
                return "{" + objects + "}";
            } else if (o instanceof Double) {
                return "" + o + "";
            } else if (o instanceof Long) {
                return "" + o + "";
            } else if (o instanceof Boolean) {
                return "" + o + "";
            } else if (o instanceof Date) {
                return "'" + o.toString() + "'";
            } else if (o instanceof String) {
                String val = (String) o;
                if (val.equalsIgnoreCase("null")) {
                    return null;
                }
                return ("\"" + val + "\"").trim();
            }
            return null;
        } else {
            return null;
        }
    }

    public String toJSONObject();
}
