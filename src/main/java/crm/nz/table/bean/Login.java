 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.table.bean;

import crm.nz.constants.Constants;
import static crm.nz.table.bean.ToObjectConverter.checkNull;
import java.lang.reflect.Field;
import java.util.logging.Logger;

/**
 *
 * @author TOSHIBA R830
 */
public class Login {

    /**
     * @return the step
     */
    public String getStep() {
        return step;
    }

    /**
     * @param step the step to set
     */
    public void setStep(String step) {
        this.step = step;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the created_ts
     */
    public String getCreated_ts() {
        return created_ts;
    }

    /**
     * @param created_ts the created_ts to set
     */
    public void setCreated_ts(String created_ts) {
        this.created_ts = created_ts;
    }

    /**
     * @return the created_by
     */
    public String getCreated_by() {
        return created_by;
    }

    /**
     * @param created_by the created_by to set
     */
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return the active
     */
    public String getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(String active) {
        this.active = active;
    }

    /**
     * @return the client_id
     */
    public String getClient_id() {
        return client_id;
    }

    /**
     * @param client_id the client_id to set
     */
    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    /**
     * @return the client_secret
     */
    public String getClient_secret() {
        return client_secret;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    /**
     * @param client_secret the client_secret to set
     */
    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }

    public String getApplication_id() {
        return application_id;
    }

    public void setApplication_id(String application_id) {
        this.application_id = application_id;
    }

    private String username;
    private String password;
    private String created_ts;
    private String created_by;
    private String role;
    private String active;
    private String client_id;
    private String user_id;
    private String application_id;
    private String client_secret;
    private String token;
    private String step;
    private String secret;
    private String name;
    

    @Override
    public String toString() {
        return toString(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                if (fieldValue instanceof String) {
                    String checkNull = checkNull(fieldValue);
                    if (checkNull != null) {
                        buffer.append(checkNull);
                    } else {
                        buffer.append(checkNull);
                    }
                } else {
                    buffer.append(checkNull(fieldValue));
                }
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(SecuredUser.class.getName()).throwing(SecuredUser.class.getName(), "toString(Object THIS)", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
