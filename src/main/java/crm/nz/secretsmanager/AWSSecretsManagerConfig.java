/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.secretsmanager;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.DecryptionFailureException;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.amazonaws.services.secretsmanager.model.InternalServiceErrorException;
import com.amazonaws.services.secretsmanager.model.InvalidParameterException;
import com.amazonaws.services.secretsmanager.model.InvalidRequestException;
import com.amazonaws.services.secretsmanager.model.ResourceNotFoundException;
import com.google.gson.Gson;
import crm.nz.beans.acc.api.DataZooServicebean;
import crm.nz.beans.acc.api.NZBNApiBean;
import crm.nz.beans.acc.api.OAuth2TokenServiceBean;
import java.util.Base64;
import java.util.HashMap;
import org.apache.commons.dbcp.BasicDataSource;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author innovative002
 */
@Configuration
public class AWSSecretsManagerConfig {

    {
        System.out.println("AWSSecretsManagerConfig");
    }

    private Gson gson = new Gson();
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(AWSSecretsManagerConfig.class);
    private String region = "ap-southeast-2";
    private String awsAccessKey = "AKIA53JJUB3CYMYM45WD";
    private String awsSecretKey = "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";
    private AWSStaticCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey));
    // Create a Secrets Manager client
    private AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard()
            .withRegion(region)
            .withCredentials(credentialsProvider)
            .build();

// Use this code snippet in your app.
// If you need more information about configurations or implementing the sample code, visit the AWS docs:
// https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/java-dg-samples.html#prerequisites
//    public AWSSecretsManagerConfig(){} 
    private HashMap<String, String> getRetrievedSecretMap(String secretName) {
        String secret;
        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                .withSecretId(secretName);
        GetSecretValueResult getSecretValueResult = null;
        try {
            getSecretValueResult = client.getSecretValue(getSecretValueRequest);
        } catch (DecryptionFailureException e) {
            // Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InternalServiceErrorException e) {
            // An error occurred on the server side.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InvalidParameterException e) {
            // You provided an invalid value for a parameter.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InvalidRequestException e) {
            // You provided a parameter value that is not valid for the current state of the resource.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (ResourceNotFoundException e) {
            // We can't find the resource that you asked for.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        }
        // Decrypts secret using the associated KMS CMK.
        // Depending on whether the secret is a string or binary, one of these fields will be populated.
        if (getSecretValueResult.getSecretString() != null) {
            secret = getSecretValueResult.getSecretString();
        } else {
            secret = new String(Base64.getDecoder().decode(getSecretValueResult.getSecretBinary()).array());
        }
        final HashMap<String, String> secretMap = gson.fromJson(secret, HashMap.class);
        return secretMap;
    }

    @Bean(name = "dataSource")
    public BasicDataSource dataSource() {
        String secretName0 = "mysql/testing";
//            String secretName0 = "mysql/staging";
        HashMap<String, String> secretMap0 = getRetrievedSecretMap(secretName0);
        final String url = secretMap0.get("url").concat(secretMap0.get("westpacdb"));
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(secretMap0.get("driverClassName"));
        dataSource.setUrl(url);
        dataSource.setUsername(secretMap0.get("username"));
        dataSource.setPassword(secretMap0.get("password"));
        dataSource.setInitialSize(10);
        dataSource.setMaxActive(25);
        dataSource.setMaxIdle(20);
        dataSource.setMinIdle(10);
        dataSource.setTestOnBorrow(true);
        dataSource.setValidationQuery("SELECT 1");
        System.out.println(dataSource);
        return dataSource;
    }

    @Bean(name = "oAuth2TokenServiceBean")
    public OAuth2TokenServiceBean getOAuth2Token() {
//        String secretName3 = "apikey/MintOAuth2TokenService1";
//        HashMap<String, String> secretMap3 = getRetrievedSecretMap(secretName3);
//        OAuth2TokenServiceBean oAuth2TokenServiceBean = new OAuth2TokenServiceBean(secretMap3.get("apiUrl"), secretMap3.get("clientId"), secretMap3.get("clientSecret"));
//        return oAuth2TokenServiceBean;
        return new OAuth2TokenServiceBean();
    }

    @Bean(name = "nZBNApiBean")
    public NZBNApiBean getNZBN() {
        String secretName1 = "apikey/auth/nzbn";
        HashMap<String, String> secretMap1 = getRetrievedSecretMap(secretName1);
        NZBNApiBean nZBNApiBean = new NZBNApiBean(secretMap1.get("tokenUrl"), secretMap1.get("clientId"), secretMap1.get("clientSecret"));
        return nZBNApiBean;
    }

    @Bean(name = "dataZooServicebean")
    public DataZooServicebean getDataZooService() {
        String secretName2 = "apikey/DataZooService";
        HashMap<String, String> secretMap2 = getRetrievedSecretMap(secretName2);
        DataZooServicebean dataZooServicebean = new DataZooServicebean(secretMap2.get("username"), secretMap2.get("password"), secretMap2.get("apiUrl"));
        return dataZooServicebean;
    }

}
