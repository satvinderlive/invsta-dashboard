/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.Beneficiary;
import crm.nz.beans.acc.api.PendingTransactionBean;
import crm.nz.lambda.repo.FormDetailsRepository;
import static crm.nz.repository.PendingInvestmentRepository.getPrice;
import static crm.nz.repository.PendingInvestmentRepository.invTranSql;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author TOSHIBA R830
 */
@Repository
public class PendingTransactionRepositoryImpl implements PendingTransactionRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void setPendingTransaction(PendingTransactionBean bean) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement ps = con.prepareStatement(insertSql);
            ps.setString(1, bean.getAmount());
            ps.setString(2, bean.getInvestmentcode());
            ps.setString(3, bean.getPortfolioCode());
            ps.setString(4, bean.getApplicationId());
            ps.setString(5, bean.getUserId());
            ps.setString(6, bean.getBeneficiaryId());
            ps.setString(7, bean.getBankAccountId());
            ps.setString(8, bean.getType());
            ps.setString(9, bean.getCreated_ts());
            return ps;
        });

    }

    @Override
    public List<PendingTransactionBean> getPendingTransaction() {
        String sql = "SELECT pt.*,lm.username as name,b.Name as beneficiaryName ,p.Name as portfolioName FROM pending_transaction pt inner join beneficiaries b on\n"
                + " (b.Id=pt.beneficiaryId) inner join portfolio p on(p.Code=pt.portfolioCode and p.status='Y')\n"
                + " inner join login_master lm on(lm.id=pt.userId) where pt.status='Pending' ";
        List<PendingTransactionBean> beneficiaryDetail = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PendingTransactionBean.class));
        return beneficiaryDetail;

    }

    @Override
    public void cancelTransection(String id) {
        String setstatus = "UPDATE pending_transaction SET status = 'Cancel' WHERE id =?";
        jdbcTemplate.update(setstatus, new Object[]{id});

    }

    @Override
    public void acceptTransaction(String id) {
        Connection con = null;
        try {
            con = jdbcTemplate.getDataSource().getConnection();
            String sql = "SELECT pt.*,p.Name as name  FROM pending_transaction pt inner join portfolio p  on (p.code=pt.portfolioCode and p.status='Y') where pt.id= ?";
            List<PendingTransactionBean> pedndingTransection = jdbcTemplate.query(sql, new Object[]{id}, new BeanPropertyRowMapper(PendingTransactionBean.class));
            PendingTransactionBean bean = pedndingTransection.get(0);

            List<PendingTransactionBean> pricebeans = jdbcTemplate.query(getPrice, new Object[]{bean.getPortfolioCode()}, new BeanPropertyRowMapper(PendingTransactionBean.class));
            PendingTransactionBean pricebean = pricebeans.get(0);
            BigDecimal nav = new BigDecimal(pricebean.getAmount());
            BigDecimal amount = new BigDecimal(bean.getAmount());

            BigDecimal units = amount.divide(nav, 2, RoundingMode.HALF_UP);
            String displayType = "";
            if (bean.getType().equalsIgnoreCase("WDR")) {
                displayType = "withdrawal";
            } else {
                displayType = "Contribution";
            }

            PreparedStatement ps3 = con.prepareStatement(invTranSql, Statement.RETURN_GENERATED_KEYS);
            ps3.setString(1, bean.getInvestmentcode());
            ps3.setDate(2, new Date(new java.util.Date().getTime()));
            ps3.setString(3, "APP");
            ps3.setString(4, displayType);
            ps3.setString(5, bean.getType());
            ps3.setString(6, bean.getType());
            ps3.setString(7, bean.getPortfolioCode());
            ps3.setString(8, bean.getName());
            ps3.setString(9, units.toPlainString());
            ps3.setString(10, nav.toPlainString());
            ps3.setString(11, bean.getAmount());
            ps3.setDouble(12, 0.0);
            ps3.setDouble(13, 0.0);
            ps3.setDouble(14, 0.0);
            ps3.setDouble(15, 0.0);
            ps3.setDouble(16, 0.0);
            ps3.setDouble(17, 0.0);
            ps3.setDouble(18, 0.0);
            ps3.setDouble(19, 0.0);
            ps3.setString(20, "0");
            ps3.setString(21, "Provider");
            ps3.setString(22, "Other");
            ps3.setString(23, "Contribution");
            ps3.setString(24, "Application");
            ps3.executeUpdate();
//            ps3.close();
            String setstatus = "UPDATE pending_transaction SET status = 'Done' WHERE id =?";
            jdbcTemplate.update(setstatus, new Object[]{bean.getId()});

        } catch (SQLException ex) {
            System.out.println("" + ex);
        } finally {
            if (con != null) {
                try {
                    con.close();

                } catch (SQLException ex) {
                    Logger.getLogger(FormDetailsRepository.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
