/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.PendingInvestmentBean;
import java.util.List;

public interface PendingInvestmentRepository {

    String insertSql = "INSERT INTO `pending_investment`\n"
            + "(`investmentName`,\n"
            + "`investedAmount`,\n"
            + "`investmentCode`,\n"
            + "`portfolioCode`,\n"
            + "`applicationId`,\n"
            + "`userId`,\n"
            + "`beneficiaryId`,\n"
            + "`investmentId`,\n"
            + "`bankAccountId`,\n"
            + "`created_ts`,\n"
            + "`status`)\n"
            + "VALUES\n"
            + "(?,?,?,?,?,?,?,?,?,?,'Pending')";

    String insertcode = "INSERT INTO investments\n"
            + "(Contributions,\n"
            + "Withdrawals,\n"
            + "Earnings,\n"
            + "TotalValue,\n"
            + "ReinvestedDistributions,\n"
            + "CashDistributions,\n"
            + "Tax,\n"
            + "TaxPaid,\n"
            + "InvestmentReturnRate,\n"
            + "InvestmentStrategyBand,\n"
            + "Code,\n"
            + "InvestmentType,\n"
            + "BeneficiaryId,\n"
            + "bankAccountId)\n"
            + "VALUES\n"
            + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

    String getPrice = "SELECT max(id) id,LastPrice amount FROM `portfolio_price` where PortfolioCode=?;";
    String insertInvestmentHolding = "INSERT INTO `investment_holding`\n"
            + "(\n"
            + "`InvestmentCode`,\n"
            + "`AsAt`,\n"
            + "`PortfolioCode`,\n"
            + "`PortfolioName`,\n"
            + "`DistMethod`,\n"
            + "`Units`,\n"
            + "`TaxOwed`,\n"
            + "`Price`,\n"
            + "`MarketValue`,\n"
            + "`PortfolioScale`,\n"
            + "`AverageFundReturn`,\n"
            + "`Contributions`,\n"
            + "`CashDistributions`,\n"
            + "`Earnings`,\n"
            + "`BeneficiaryId`)\n"
            + "VALUES\n"
            + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
    String invTranSql = "INSERT INTO investment_transaction\n"
            + "(InvestmentCode,\n"
            + "EffectiveDate,\n"
            + "Type,\n"
            + "TypeDisplayName,\n"
            + "SubType,\n"
            + "SubTypeDisplayName,\n"
            + "PortfolioCode,\n"
            + "PortfolioName,\n"
            + "Units,\n"
            + "Price,\n"
            + "Value,\n"
            + "Cash,\n"
            + "Tax,\n"
            + "Fee,\n"
            + "AccruedIncome,\n"
            + "CashClearing,\n"
            + "PaymentClearing,\n"
            + "PortfolioAccount,\n"
            + "TaxRebate,\n"
            + "isPending,\n"
            + "TransactionSourceType,\n"
            + "TransactionMethodType,\n"
            + "TransactionDisplayName,\n"
            + "TransactionTypeDescription)\n"
            + "VALUES\n"
            + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

    public void setPendingInvestment(PendingInvestmentBean bean);

    public List<PendingInvestmentBean> getPendingInvestment();
    
    public List<PendingInvestmentBean> getAdvisorPendingInvestment(String id);

    public void acceptInvestmentRequest(String id);
    
    public void cancelInvestment(String id);

    public int codeGenerator();

}
