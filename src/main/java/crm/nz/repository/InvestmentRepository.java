/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.FmcaInvestmentMix;
import crm.nz.beans.acc.api.FmcaTopAssets;
import crm.nz.beans.acc.api.Investment;
import crm.nz.beans.acc.api.InvestmentHolding;
import crm.nz.beans.acc.api.InvestmentPerformance;
import crm.nz.beans.acc.api.InvestmentTransection;
import java.util.List;

/**
 *
 * @author IESL
 */
public interface InvestmentRepository {

    public void saveInvestments(List<Investment> investment);

    public void saveFmcaInvestmentMix(List<FmcaInvestmentMix> fmcaInvestmentMix);

    public void saveFmcaTopAssets(List<FmcaTopAssets> fmcaTopAssets);

    public void saveInvestmentHolding(List<InvestmentHolding> bean);

    public void saveInvestmentPerformance(List<InvestmentPerformance> bean);

    public List<Investment> getInvestments(String UserId, String BeneficiaryId);

    public List<InvestmentHolding> getInvestmentHoldings(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode);

    public List<InvestmentPerformance> getInvestmentPerformances(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode);

    public List<FmcaInvestmentMix> getTotalFmcaInvestmentMix(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode);

    public List<FmcaInvestmentMix> getTotalFmcaInvestmentMixByTable(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode);

    public List<FmcaTopAssets> getFmcaTopAssets(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode);

    public List<InvestmentTransection> getInvestmnetTransection(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode);

}
