/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.FmcaInvestmentMix;
import crm.nz.beans.acc.api.FmcaTopAssets;
import crm.nz.beans.acc.api.InvestmentPdfBean;
import crm.nz.beans.acc.api.InvestmentTeamBean;
import crm.nz.beans.acc.api.PerformanceBean;
import crm.nz.beans.acc.api.Portfolio;
import crm.nz.beans.acc.api.PortfolioDetail;
import crm.nz.beans.acc.api.PortfolioPrice;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public interface PortfolioRepository {

    public List<FmcaInvestmentMix> getTotalFmcaInvestmentMix(String PortfolioCode);

    public List<FmcaTopAssets> getFmcaTopAssets(String PortfolioCode);

    public PortfolioDetail getPortfolioDetail(String PortfolioCode);

    public void setPortfolioDetail(PortfolioDetail bean);

    public void savePortfolios(List<Portfolio> list);

    public void setFmcaTopAssets(FmcaTopAssets bean);

    public void setTotalFmcaInvestmentMix(FmcaTopAssets bean);

    public void setFundPerformance(PerformanceBean bean);

    public List<PerformanceBean> getFundPerformance(String PortfolioCode);
    
    public List<PerformanceBean> getPerformanceSinceinception(String PortfolioCode);

    public void savePortfolioPrices(List<PortfolioPrice> list);

    public List<Portfolio> getAllPortfolios();

    public List<PortfolioDetail> getOtherPortfolioDetail(String PortfolioCode);

    public void setPortfolio(PortfolioDetail bean);

    public void saveOrUpdateInvestmentTeam(List<InvestmentTeamBean> teamBeans, String portfolio);
    
    public void saveOrUpdatePdfFile(List<InvestmentPdfBean> teamBeans, String portfolio);

    public List<InvestmentTeamBean> getInvestmentTeam(String PortfolioCode);
    
    public List<InvestmentPdfBean> getInvestmentPdf(String PortfolioCode);
    
    public List<InvestmentPdfBean> getQuarterlyPdf(String PortfolioCode);

}
