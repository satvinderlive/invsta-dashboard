/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.PendingTransactionBean;
import java.util.List;

/**
 *
 * @author TOSHIBA R830
 */
public interface PendingTransactionRepository {
    
       
       String insertSql = "INSERT INTO `pending_transaction`\n"
                + "(`amount`,\n"
                + "`investmentCode`,\n"
                + "`portfolioCode`,\n"
                + "`applicationId`,\n"
                + "`userId`,\n"
                + "`beneficiaryId`,\n"
                + "`bankAccountId`,\n"
                + "`type`,\n"
                + "`created_ts`,\n"
                + "`status`)\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?,?,'Pending')";
       
        String invTranSql = "INSERT INTO investment_transaction\n"
            + "(InvestmentCode,\n"
            + "EffectiveDate,\n"
            + "Type,\n"
            + "TypeDisplayName,\n"
            + "SubType,\n"
            + "SubTypeDisplayName,\n"
            + "PortfolioCode,\n"
            + "PortfolioName,\n"
            + "Units,\n"
            + "Price,\n"
            + "Value,\n"
            + "Cash,\n"
            + "Tax,\n"
            + "Fee,\n"
            + "AccruedIncome,\n"
            + "CashClearing,\n"
            + "PaymentClearing,\n"
            + "PortfolioAccount,\n"
            + "TaxRebate,\n"
            + "isPending,\n"
            + "TransactionSourceType,\n"
            + "TransactionMethodType,\n"
            + "TransactionDisplayName,\n"
            + "TransactionTypeDescription)\n"
            + "VALUES\n"
            + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
      
    public void setPendingTransaction(PendingTransactionBean bean);
    
    public void acceptTransaction(String id);
    
    public List<PendingTransactionBean> getPendingTransaction();
    
    public void cancelTransection(String id);
}
