/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.PendingInvestmentBean;
import crm.nz.beans.acc.api.PendingTransactionBean;
import crm.nz.beans.acc.api.PortfolioDetail;
import crm.nz.lambda.repo.FormDetailsRepository;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author TOSHIBA R830
 */
@Repository
public class PendingInvestmentRepositoryImpl implements PendingInvestmentRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void setPendingInvestment(PendingInvestmentBean bean) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement ps = con.prepareStatement(insertSql);
            ps.setString(1, bean.getInvestmentName());
            ps.setString(2, bean.getInvestedAmount());
            ps.setString(3, bean.getInvestmentCode());
            ps.setString(4, bean.getPortfolioCode());
            ps.setString(5, bean.getApplicationId());
            ps.setString(6, bean.getUserId());
            ps.setString(7, bean.getBeneficiaryId());
            ps.setString(8, bean.getInvestmentId());
            ps.setString(9, bean.getBankAccountId());
            ps.setString(10, bean.getCreated_ts());
            return ps;
        });
    }

    @Override
    public List<PendingInvestmentBean> getPendingInvestment() {
        String sql = "SELECT pt.*,lm.username as name,b.Name as beneficiaryName,p.Name as portfolioName FROM pending_investment pt inner join beneficiaries b on\n"
                + " (b.Id=pt.beneficiaryId)\n"
                + "  inner join portfolio p on(p.Code=pt.portfolioCode and p.status='Y')\n"
                + "  inner join login_master lm on(lm.id=pt.userId) where pt.status='Pending'  ";
        List<PendingInvestmentBean> beneficiaryDetail = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PendingInvestmentBean.class));
        return beneficiaryDetail;

    }
    @Override
     public void cancelInvestment(String id){
         String setstatus = "UPDATE pending_investment SET status = 'Cancel' WHERE id =?";
            jdbcTemplate.update(setstatus, new Object[]{id});
        
    }

    @Override
    public List<PendingInvestmentBean> getAdvisorPendingInvestment(String id) {
        String sql = " SELECT pt.*,lm.username as name,b.Name as beneficiaryName FROM pending_investment pt\n"
                + "inner join beneficiaries b on (b.Id=pt.beneficiaryId)\n"
                + "inner join login_master lm on(lm.id=pt.userId)\n"
                + "inner join user_beneficiairy_relationship ubr on(lm.id = ubr.user_id)\n"
                + "where pt.status='Pending'\n"
                + "and ubr.advisor_id = " + id;
        List<PendingInvestmentBean> beneficiaryDetail = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PendingInvestmentBean.class));
        return beneficiaryDetail;

    }

    @Override
    public int codeGenerator() {

        String sql = "INSERT INTO `codeGenerator` (`code`) VALUES (?);";

        KeyHolder keyholder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps0 = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps0.setString(1, "A");

                return ps0;
            }
        }, keyholder);
        int key = keyholder.getKey().intValue();
        return key;
    }

    @Override
    public void acceptInvestmentRequest(String id) {
        Connection con = null;
        try {
            String sql = "SELECT * FROM pending_investment where id= ?";
            con = jdbcTemplate.getDataSource().getConnection();

            List<PendingInvestmentBean> beneficiaryDetail = jdbcTemplate.query(sql, new Object[]{id}, new BeanPropertyRowMapper(PendingInvestmentBean.class));
            PendingInvestmentBean bean = beneficiaryDetail.get(0);
            int codeGenerator = codeGenerator();
            String code = "INV" + codeGenerator;

            String sql1 = "SELECT p.Name fundName FROM portfolio p where code= ? and `status`='Y'";
            List<PortfolioDetail> fundDetails = jdbcTemplate.query(sql1, new Object[]{bean.getPortfolioCode()}, new BeanPropertyRowMapper(PortfolioDetail.class));
            PortfolioDetail fundDetail = fundDetails.get(0);
            String fundName = fundDetail.getFundName();
            PreparedStatement ps = con.prepareStatement(insertcode, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, "0");
            ps.setString(2, "0");
            ps.setString(3, "0");
            ps.setString(4, "0");
            ps.setString(5, "0");
            ps.setString(6, "0");
            ps.setString(7, "0");
            ps.setString(8, "0");
            ps.setString(9, "0");
            ps.setString(10, "0");
            ps.setString(11, code);
            ps.setString(12, "Unit_Registry");
            ps.setString(13, bean.getBeneficiaryId());
            ps.setString(14, bean.getBankAccountId());
            ps.executeUpdate();
            ps.close();

            List<PendingTransactionBean> pricebeans = jdbcTemplate.query(getPrice, new Object[]{bean.getPortfolioCode()}, new BeanPropertyRowMapper(PendingTransactionBean.class));
            PendingTransactionBean pricebean = pricebeans.get(0);
            BigDecimal nav = new BigDecimal(pricebean.getAmount());
            BigDecimal amount = new BigDecimal(bean.getInvestedAmount());

            BigDecimal units = amount.divide(nav, 2, RoundingMode.HALF_UP);
            PreparedStatement ps2 = con.prepareStatement(insertInvestmentHolding, Statement.RETURN_GENERATED_KEYS);
            ps2.setString(1, code);
            ps2.setDate(2, new Date(new java.util.Date().getTime()));
            ps2.setString(3, bean.getPortfolioCode());
            ps2.setString(4, fundName);
            ps2.setString(5, "INVESTED");
            ps2.setString(6, units.toPlainString());
            ps2.setString(7, "0");
            ps2.setString(8, nav.toPlainString());
            ps2.setString(9, bean.getInvestedAmount());
            ps2.setDouble(10, 0.0);
            ps2.setDouble(11, 0.0);
            ps2.setString(12, bean.getInvestedAmount());
            ps2.setDouble(13, 0.0);
            ps2.setDouble(14, 0.0);
            ps2.setString(15, bean.getBeneficiaryId());
            ps2.executeUpdate();
            ps2.close();

            PreparedStatement ps3 = con.prepareStatement(invTranSql, Statement.RETURN_GENERATED_KEYS);
            ps3.setString(1, code);
            ps3.setDate(2, new Date(new java.util.Date().getTime()));
            ps3.setString(3, "APP");
            ps3.setString(4, "Contribution");
            ps3.setString(5, "INV");
            ps3.setString(6, "Contribution");
            ps3.setString(7, bean.getPortfolioCode());
            ps3.setString(8, fundName);
            ps3.setString(9, units.toPlainString());
            ps3.setString(10, nav.toPlainString());
            ps3.setString(11, bean.getInvestedAmount());
            ps3.setDouble(12, 0.0);
            ps3.setDouble(13, 0.0);
            ps3.setDouble(14, 0.0);
            ps3.setDouble(15, 0.0);
            ps3.setDouble(16, 0.0);
            ps3.setDouble(17, 0.0);
            ps3.setDouble(18, 0.0);
            ps3.setDouble(19, 0.0);
            ps3.setString(20, "0");
            ps3.setString(21, "Provider");
            ps3.setString(22, "Other");
            ps3.setString(23, "Contribution");
            ps3.setString(24, "Application");
            ps3.executeUpdate();
//            ps3.close();
            String setstatus = "UPDATE pending_investment SET status = 'Done' WHERE id =?";
            jdbcTemplate.update(setstatus, new Object[]{bean.getId()});

        } catch (SQLException ex) {
            System.out.print("sqlexcp------------------" + ex);
        } catch (NullPointerException ex) {
            System.out.print("sqlexcp------------------" + ex);
        } finally {
            if (con != null) {
                try {
                    con.close();

                } catch (SQLException ex) {
                    Logger.getLogger(FormDetailsRepository.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

}
