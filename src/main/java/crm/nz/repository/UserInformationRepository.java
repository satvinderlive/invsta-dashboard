/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.userInfobean;

/**
 *
 * @author TOSHIBA R830
 */
public interface UserInformationRepository {

    String sqladmin = "SELECT (SELECT COUNT(*) FROM login_master where role='BENEFICIARY')+\n"
            + "  (SELECT COUNT(*) from register) AS user,\n"
            + "  (select COUNT(*) as investment from investment_transaction ) as investment\n"
            + "	, round((SELECT sum(g1.Price) as invp FROM investment_transaction g1 where g1.SubType = 'INV')\n"
            + "	- (SELECT sum(g2.Price) as wdwp from investment_transaction g2 where g2.SubType = 'WDW'),4)\n"
            + "	as amount";

    String sqladvisor = "SELECT (SELECT COUNT(*) FROM login_master lm where lm.role='ADVISOR' AND lm.id= ?)\n"
            + "          + (SELECT COUNT(*) from register R\n"
            + "             inner join individual_person_detail ipd on (ipd.reg_id= R.id )\n"
            + "              inner join login_master lma on ( lma.id = ipd.advisor AND ipd.advisor= ? )\n"
            + "             )\n"
            + "           + (SELECT COUNT(*) from register R\n"
            + "             inner join joint_account_details jad on (jad.reg_id= R.id )\n"
            + "              inner join login_master lma on ( lma.id = jad.advisor AND jad.advisor= ? )\n"
            + "             ) AS user,\n"
            + "          (select COUNT(*) as investment from investment_transaction  ) as investment\n"
            + "		, round((SELECT sum(g1.Price) as invp FROM investment_transaction g1 where g1.SubType = 'INV')\n"
            + "		- (SELECT sum(g2.Price) as wdwp from investment_transaction g2 where g2.SubType = 'WDW'),4)\n"
            + "		as amount";

    public userInfobean getUserdetailsByAdmin();

    public userInfobean getUserdetailsByAdvisor(String id);

}
