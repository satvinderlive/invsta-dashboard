/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.FmcaInvestmentMix;
import crm.nz.beans.acc.api.FmcaTopAssets;
import crm.nz.beans.acc.api.Investment;
import crm.nz.beans.acc.api.InvestmentHolding;
import crm.nz.beans.acc.api.InvestmentPerformance;
import crm.nz.beans.acc.api.InvestmentTransection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author IESL
 */
@Repository
public class InvestmentRepositoryImpl implements InvestmentRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void saveInvestments(List<Investment> investments) {
        String insertInvestment = "INSERT INTO `investments`\n"
                + "(`InvestmentName`,`Status`,`ExternalReference`,`FeeGroup`,`AdvisorRate`,`Greeting`,`InvestmentStrategyName`,\n"
                + "`Contributions`,`Withdrawals`,`Earnings`,`TotalValue`,`ReinvestedDistributions`,`CashDistributions`,`Tax`,`TaxPaid`,\n"
                + "`InvestmentReturnRate`,`InvestmentStrategyBand`,`RegistryAccountId`,`ContractDate`,`Code`,`InvestmentType`, `BeneficiaryId`)\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        jdbcTemplate.batchUpdate(insertInvestment, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Investment Inv = investments.get(i);
                ps.setString(1, Inv.getInvestmentName());
                ps.setString(2, Inv.getStatus());
                ps.setString(3, Inv.getExternalReference());
                ps.setString(4, Inv.getFeeGroup());
                ps.setString(5, Inv.getAdvisorRate());
                ps.setString(6, Inv.getGreeting());
                ps.setString(7, Inv.getInvestmentStrategyName());
                ps.setDouble(8, Inv.getContributions());
                ps.setDouble(9, Inv.getWithdrawals());
                ps.setDouble(10, Inv.getEarnings());
                ps.setDouble(11, Inv.getTotalValue());
                ps.setDouble(12, Inv.getReinvestedDistributions());
                ps.setDouble(13, Inv.getCashDistributions());
                ps.setDouble(14, Inv.getTax());
                ps.setDouble(15, Inv.getTaxPaid());
                double rate = Inv.getInvestmentReturnRate() != null ? Inv.getInvestmentReturnRate() : 0.0;
                ps.setDouble(16, rate);
                ps.setDouble(17, Inv.getInvestmentStrategyBand());
                ps.setLong(18, Inv.getRegistryAccountId());
                ps.setString(19, Inv.getContractDate());
                ps.setString(20, Inv.getCode());
                ps.setString(21, Inv.getInvestmentType());
                ps.setString(22, Inv.getBeneficiaryId());
            }

            @Override
            public int getBatchSize() {
                return investments.size();
            }
        });

    }

    @Override
    public void saveFmcaTopAssets(List<FmcaTopAssets> fmcaTopAssets) {
        String insertFmcaTopAsset = "INSERT INTO `fmca_top_assest`\n"
                + "(`AssetName`,`AssetType`,`AssetClass`,`Portfolios`,`AsAtDate`,`Percentage`,`InvestorAssetValue`)\n"
                + "VALUES (?,?,?,?,?,?,?);";
        System.out.println("insertFmcaTopAsset----->" + insertFmcaTopAsset);
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            PreparedStatement ps = connection.prepareStatement(insertFmcaTopAsset, Statement.RETURN_GENERATED_KEYS);
            for (int i = 0; i < fmcaTopAssets.size(); i++) {
                FmcaTopAssets fmcaTopAsset = fmcaTopAssets.get(i);
                ps.setString(1, fmcaTopAsset.getAssetName());
                ps.setString(2, fmcaTopAsset.getAssetType());
                ps.setString(3, fmcaTopAsset.getAssetClass());
                ps.setString(5, fmcaTopAsset.getAsAtDate());
                ps.setDouble(5, fmcaTopAsset.getPercentage());
                ps.setDouble(5, fmcaTopAsset.getInvestorAssetValue());
                ps.addBatch();
            }
            int[] i = ps.executeBatch();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            while (generatedKeys.next()) {
                String AssetId = generatedKeys.getString(1);
                String Sql = "INSERT INTO `AssetPortfolioRelation`\n"
                        + "(`AssetId`,`PortfolioCode`)\n"
                        + "VALUES (?,?);";
                PreparedStatement ps1 = connection.prepareStatement(Sql, Statement.RETURN_GENERATED_KEYS);
                ps1.setString(1, AssetId);
                ps1.setString(1, AssetId);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CommonRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        jdbcTemplate.batchUpdate(insertFmcaTopAsset, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {

            }

            @Override
            public int getBatchSize() {
                return fmcaTopAssets.size();
            }
        });

    }

    @Override
    public void saveFmcaInvestmentMix(List<FmcaInvestmentMix> fmcaInvestmentMixs) {
        String insertFmcaInvestmentMix = "INSERT INTO `fmca_investment_mix`\n"
                + "(`Portfolio`,`FMCAAssetClass`,`SectorValueBase`,`Percentage`,`AsAtDate`, `InvestmentCode`, `BeneficiaryId`)\n"
                + "VALUES (?,?,?,?,?,?,?);";
        System.out.println("insertFmcaInvestmentMix----->" + insertFmcaInvestmentMix);
        jdbcTemplate.batchUpdate(insertFmcaInvestmentMix, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                FmcaInvestmentMix fmcaInvestmentMix = fmcaInvestmentMixs.get(i);
                ps.setString(1, fmcaInvestmentMix.getPortfolio());
                ps.setString(2, fmcaInvestmentMix.getFMCAAssetClass());
                ps.setDouble(3, fmcaInvestmentMix.getSectorValueBase());
                ps.setDouble(4, fmcaInvestmentMix.getPercentage());
                ps.setString(5, fmcaInvestmentMix.getAsAtDate());
                ps.setString(6, fmcaInvestmentMix.getInvestmentCode());
                ps.setString(7, fmcaInvestmentMix.getBeneficiaryId());
            }

            @Override
            public int getBatchSize() {
                return fmcaInvestmentMixs.size();
            }
        });

    }

    @Override
    public void saveInvestmentHolding(List<InvestmentHolding> bean) {
        String insertInvestmentHolding = "INSERT INTO `investment_holding`\n"
                + "(`InvestmentCode`,`AsAt`,`PortfolioCode`,`PortfolioName`,`DistMethod`,`Units`,`TaxOwed`,\n"
                + "`Price`,`MarketValue`,`PortfolioScale`,`Volatility`,`AverageFundReturn`,`Contributions`,\n"
                + "`Withdrawals`,`Fees`,`CashDistributions`,`ReinvestedDistributions`,`Earnings`,`ReturnRate`,`BeneficiaryId`)\n"
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        System.out.println("insertInvestmentHolding----->" + insertInvestmentHolding);
        jdbcTemplate.batchUpdate(insertInvestmentHolding, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                InvestmentHolding InvHolding = bean.get(i);
                ps.setString(1, InvHolding.getInvestmentCode());
                ps.setString(2, InvHolding.getAsAt());
                ps.setString(3, InvHolding.getPortfolioCode());
                ps.setString(4, InvHolding.getPortfolioName());
                ps.setString(5, InvHolding.getDistMethod());
                ps.setDouble(6, InvHolding.getUnits());
                ps.setDouble(7, InvHolding.getTaxOwed());
                ps.setDouble(8, InvHolding.getPrice());
                ps.setDouble(9, InvHolding.getMarketValue());
                ps.setDouble(10, InvHolding.getPortfolioScale());
                ps.setDouble(11, InvHolding.getVolatility());
                ps.setDouble(12, InvHolding.getAverageFundReturn());
                ps.setDouble(13, InvHolding.getContributions());
                ps.setDouble(14, InvHolding.getWithdrawals());
                ps.setDouble(15, InvHolding.getFees());
                ps.setDouble(16, InvHolding.getCashDistributions());
                ps.setDouble(17, InvHolding.getReinvestedDistributions());
                ps.setDouble(18, InvHolding.getEarnings());
                ps.setDouble(19, InvHolding.getReturnRate());
                ps.setString(20, InvHolding.getBeneficiaryId());
            }

            @Override
            public int getBatchSize() {
                return bean.size();
            }
        });
    }

    @Override
    public void saveInvestmentPerformance(List<InvestmentPerformance> bean) {
        String insertInvestmentPerformance = "INSERT INTO `investment_performance`\n"
                + "(`Portfolio`,`PeriodStartDate`,`PeriodEndDate`,`XIRRReturnRate`,`InsufficientData`,`PeriodsCovered`,`IsTotalPeriod`,`BeneficiaryId`, `InvestmentCode`)\n"
                + "VALUES (?,?,?,?,?,?,?,?,?);";
        System.out.println("insertInvestmentPerformance----->" + insertInvestmentPerformance);
        jdbcTemplate.batchUpdate(insertInvestmentPerformance, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                InvestmentPerformance InvPerformance = bean.get(i);
                ps.setString(1, InvPerformance.getPortfolio());
                ps.setString(2, InvPerformance.getPeriodStartDate());
                ps.setString(3, InvPerformance.getPeriodEndDate());
                ps.setDouble(4, InvPerformance.getXIRRReturnRate());
                ps.setBoolean(5, InvPerformance.isInsufficientData());
                ps.setDouble(6, InvPerformance.getPeriodsCovered());
                ps.setBoolean(7, InvPerformance.isIsTotalPeriod());
                ps.setString(8, InvPerformance.getBeneficiaryId());
                ps.setString(9, InvPerformance.getInvestmentCode());
            }

            @Override
            public int getBatchSize() {
                return bean.size();
            }
        });
    }

    @Override
    public List<Investment> getInvestments(String UserId, String BeneficiaryId) {
        String sql = " SELECT * FROM investments IM "
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " WHERE IM.BeneficiaryId = UBR.beneficiairy_id ";
        if (UserId != null) {
            sql += " and UBR.user_id = " + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and IM.BeneficiaryId = " + BeneficiaryId;
        }
        List<Investment> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Investment.class));
        return list;
    }

    @Override
    public List<InvestmentHolding> getInvestmentHoldings(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String sql = "SELECT UUH.ClientName, UUH.Id id, Unitholder InvestmentCode, now() AsAt, Portfolio PortfolioCode, FundName PortfolioName, 'Reinvested' DistMethod, Units, 0 TaxOwed, Red Price, LastPrice * Units MarketValue, '' PortfolioScale, '' Volatility, '' AverageFundReturn, Units * Red Contributions, 0 Withdrawals, 0 Fees, 0 CashDistributions, 0 ReinvestedDistributions, 0 Earnings, 0 ReturnRate, UUH.BeneficiaryId  FROM investments IM \n"
                + "INNER JOIN UpdateUnitsHolding UUH ON(IM.Code = UUH.Unitholder)\n"
                + "INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + "INNER JOIN (SELECT * FROM portfolio_price WHERE CreatedDate = (SELECT max(CreatedDate) FROM portfolio_price) GROUP BY PortfolioCode) PP ON(UUH.Portfolio = PP.PortfolioCode)\n"
                + "WHERE IM.BeneficiaryId = UUH.BeneficiaryId\n";
//        String sql = "SELECT IH.* FROM investments IM \n"
//                + "INNER JOIN investment_holding IH ON(IM.Code = IH.InvestmentCode)\n"
//                + "INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
//                + "WHERE IM.BeneficiaryId = IH.BeneficiaryId\n";
        if (UserId != null) {
            sql += " and UBR.user_id = " + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and UBR.beneficiairy_id = " + BeneficiaryId;
        }
        if (InvestmentCode != null) {
            sql += " and IM.Code = '" + InvestmentCode + "'";
        }
        if (PortfolioCode != null) {
            sql += " and UUH.Portfolio = " + PortfolioCode;
//            sql += " and IH.PortfolioCode = " + PortfolioCode;
        }
        sql+=" group by Unitholder, PortfolioCode order by  `Date` desc; ";
        System.out.println("sql---->" + sql);
        List<InvestmentHolding> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(InvestmentHolding.class));
        return list;
    }

    @Override
    public List<InvestmentPerformance> getInvestmentPerformances(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String sql = "SELECT IP.* FROM investments IM\n"
                + " INNER JOIN investment_performance IP on(IM.Code = IP.InvestmentCode)\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " WHERE IM.BeneficiaryId = IP.BeneficiaryId ";
        if (UserId != null) {
            sql += " and UBR.user_id = " + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and IM.BeneficiaryId = " + BeneficiaryId;
        }
        System.out.println("sql---->" + sql);
        List<InvestmentPerformance> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(InvestmentPerformance.class));
        return list;
    }

    @Override
    public List<FmcaInvestmentMix> getTotalFmcaInvestmentMix(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String sql = " select FMCAAssetClass, sum(SectorValueBase) SectorValueBase from investments IM\n"
                + " inner join fmca_investment_mix FIM on(IM.Code = FIM.InvestmentCode)\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " where FIM.BeneficiaryId = IM.BeneficiaryId ";
        if (UserId != null) {
            sql += " and UBR.user_id =" + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and FIM.BeneficiaryId =" + BeneficiaryId;
        }
        if (InvestmentCode != null) {
            sql += " and FIM.InvestmentCode ='" + InvestmentCode + "'";
        }
        if (PortfolioCode != null) {
            sql += " and FIM.Portfolio =" + PortfolioCode;
        }
        sql += " group by InvestmentCode, FMCAAssetClass;";
        System.out.println("sql---->" + sql);
        List<FmcaInvestmentMix> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(FmcaInvestmentMix.class));
        return list;
    }

    @Override
    public List<FmcaInvestmentMix> getTotalFmcaInvestmentMixByTable(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String sql = " SELECT  TFA.InvestmentName FMCAAssetClass, SUM(TFA.Price) SectorValueBase\n"
                + " FROM investments IM INNER JOIN investment_holding IH ON (IM.Code = IH.InvestmentCode)\n"
                + " INNER JOIN target_fund_allocation TFA ON (IH.PortfolioCode = TFA.Portfolio)\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " where IH.PortfolioCode = TFA.Portfolio ";
        if (UserId != null) {
            sql += " and UBR.user_id =" + UserId;
        }
        sql += " GROUP BY FMCAAssetClass;";
        System.out.println("sql---->" + sql);
        List<FmcaInvestmentMix> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(FmcaInvestmentMix.class));
        return list;
    }

    @Override
    public List<FmcaTopAssets> getFmcaTopAssets(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return null;
    }

    @Override
    public List<InvestmentTransection> getInvestmnetTransection(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String sql = "SELECT \n"
                + "  sum(CASE when pt.SubType = 'ATT' or pt.SubType = 'INV' Then pt.Value ELSE 0 END) as att_value,\n"
                + "  sum(CASE when pt.SubType = 'WDW' Then pt.Value ELSE 0 END) as wdw_value\n"
                + "  FROM investment_transaction pt\n"
                + "  inner join investments i on (i.Code=pt.InvestmentCode)\n"
                + "  INNER JOIN user_beneficiairy_relationship UBR ON (i.BeneficiaryId = UBR.beneficiairy_id)\n"
                + "  WHERE i.BeneficiaryId = UBR.beneficiairy_id\n";
        if (UserId != null) {
            sql += " and UBR.user_id = " + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and IH.BeneficiaryId = " + BeneficiaryId;
        }
        if (InvestmentCode != null) {
            sql += " and IH.InvestmentCode = '" + InvestmentCode + "'\n";
        }
        if (PortfolioCode != null) {
            sql += " and IH.PortfolioCode = " + PortfolioCode;
        }
        List<InvestmentTransection> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(InvestmentTransection.class));
        return list;
    }

}
