/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.beans.acc.api.JointDetailBean;
import crm.nz.table.bean.DocumentBean;
import java.sql.Date;
//import crm.nz.table.bean.DocumentBean;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author innovative002
 */
@Repository
public class OutsideRepositoryImp implements OutsideRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    BCryptPasswordEncoder encoder;

    @Override
    public JointDetailBean getJointHolderDetailsByToken(String token) {
        String Sql = "select * from `joint_account_details` where `token` ='" + token + "';";
        List<JointDetailBean> joint = jdbcTemplate.query(Sql, new BeanPropertyRowMapper(JointDetailBean.class));
        return !joint.isEmpty() ? joint.get(joint.size() - 1) : null;

    }
    
    @Override
    public CompanyDetailBean getCompanyHolderDetailsByToken(String token) {
        String Sql = "select * from `company_trust_details` where `token` ='" + token + "';";
        List<CompanyDetailBean> company = jdbcTemplate.query(Sql, new BeanPropertyRowMapper(CompanyDetailBean.class));
        return !company.isEmpty() ? company.get(company.size() - 1) : null;

    }


    @Override
    public void saveSingleJointHolderDetailsByToken(JointDetailBean bean) {
        String innerSql = "UPDATE `joint_account_details`\n"
                + "  SET `ird_Number` = ?,`title` = ?,`passport_expiry` = ?,`advisor` = ?,`optional_num_type` = ?,\n"
                + " `tin` = ?,`licence_expiry_Date` = ?,`optional_num_code` = ?,`acount_holder_name` = ?,\n"
                + " `passport_number` = ?,`passport_issue_by` = ?,`reg_type` = ?,`pir` = ?,`working_with_adviser` = ?,\n"
                + " `status` = ?,`optional_num` = ?,`advisor_company` = ?,`tex_residence_Country` = ?,\n"
                + " `bank_name` = ?,`license_number` = ?,`other_id_expiry` = ?,`licence_verson_number` = ?,`step` = ?,\n"
                + " `mobile_country_code` = ?,`id_type` = ?,`mobile_number` = ?,`fullName` = ?,\n"
                + " `other_id_issueBy` = ?,`isUSCitizen` = ?,`resn_tin_unavailable` = ?,`other_id_type` = ?,\n"
                + " `date_of_Birth` = ?,`account_number` = ?,`homeAddress` = ?,`occupation` = ?,\n"
                + " `created_ts` = ?, `created_by` = ?, `preferred_name` = ?,`active`='Y', `curr_idx` = ?,\n"
                + " `investor_idverified`= ? , `country_residence`= ?  WHERE  `id` = '" + bean.getId() + "';";
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement ps = con.prepareStatement(innerSql);
            ps.setString(1, bean.getIrd_Number());
            ps.setString(2, bean.getTitle());
            ps.setString(3, bean.getPassport_expiry());
            ps.setString(4, bean.getAdvisor());
            ps.setString(5, bean.getOptional_num_type());
            ps.setString(6, "");
            ps.setString(7, bean.getLicence_expiry_Date());
            ps.setString(8, bean.getOptional_num_code());
            ps.setString(9, bean.getAcount_holder_name());
            ps.setString(10, bean.getPassport_number());
            ps.setString(11, bean.getPassport_issue_by());
            ps.setString(12, bean.getReg_type());
            ps.setString(13, bean.getPir());
            ps.setString(14, bean.getWorking_with_adviser());
            ps.setString(15, bean.getStatus());
            ps.setString(16, bean.getOptional_num());
            ps.setString(17, bean.getAdvisor_company());
            ps.setString(18, "");
            ps.setString(19, bean.getBank_name());
            ps.setString(20, bean.getLicense_number());
            ps.setString(21, bean.getOther_id_expiry());
            ps.setString(22, bean.getLicence_verson_number());
            ps.setString(23, bean.getStep());
            ps.setString(24, bean.getMobile_country_code());
            ps.setString(25, bean.getId_type());
            ps.setString(26, bean.getMobile_number());
            ps.setString(27, bean.getFullName());
            ps.setString(28, bean.getOther_id_issueBy());
            ps.setString(29, bean.getIsUSCitizen());
            ps.setString(30, bean.getResn_tin_unavailable());
            ps.setString(31, bean.getOther_id_type());
            ps.setString(32, bean.getDate_of_Birth());
            ps.setString(33, bean.getAccount_number());
            ps.setString(34, bean.getHomeAddress());
            ps.setString(35, bean.getOccupation());
            ps.setDate(36, new Date(new java.util.Date().getTime()));
            ps.setString(37, bean.getCreated_by());
            ps.setString(38, bean.getPreferred_name());
            ps.setString(39, bean.getCurr_idx());
            ps.setString(40, bean.getInvestor_idverified());
            ps.setString(41, bean.getCountry_residence());
            ps.executeUpdate();
            return ps;
        });
        
        

    }

    public void saveDocumentDetails(DocumentBean user) {
        String sql = "INSERT INTO `document` \n"
                + " (`name`,`document_file`,`upload_path`)\n"
                + " VALUES (?,?,'yes')\n";
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, user.getName());
            pstmnt.setString(2, user.getUpload_path());
//            pstmnt.setString(3, bea n.getUpload_path());
//            pstmnt.setString(4, bean.getCreated_by());
//            pstmnt.setString(5, bean.getCreated_ts());
//            pstmnt.setString(6, bean.getReport_id());
            return pstmnt;
        }, holder);

//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<DocumentBean> getDocument() {
        String sql = " SELECT * FROM `document`";// where  `email` = ? \n";
        List<DocumentBean> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(DocumentBean.class));
        return list;
    }
}
//@Repository
//public class uploadRepImpl implements uploadRepository {
//
//    @Autowired
//    private JdbcTemplate jdbcTemplate;
//
//    @Override
//    public void upload(uploadbean bean) {
//        String sql = "INSERT INTO `upload` \n"
//                + " (`id`,`name`,`description`,`upload_path`)\n"
//                + " VALUES (?,?,?,?)\n";
//        KeyHolder holder = new GeneratedKeyHolder();
//        jdbcTemplate.update((java.sql.Connection con) -> {
//            PreparedStatement pstmnt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
//            pstmnt.setString(1, bean.getId());
//            pstmnt.setString(2, bean.getName());
//            pstmnt.setString(3, bean.getDescription());
//            pstmnt.setString(4, bean.getUpload_path());
//
//
//            return pstmnt;
//        }, holder);
//
//    }
//
//    @Override
//    public List<uploadbean> getfile() {
//        String sql = " SELECT * FROM `upload`  ";
//        List<uploadbean> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(uploadbean.class));
//        return list;
//    }
//
//    @Override
//    public void updatuploaded(uploadbean bean) {
//String sql = " UPDATE `upload`\n"
//                + "SET `name` = ?, `description`= ? \n"
//                + "WHERE `id` = ?;";
//        jdbcTemplate.update(sql, new Object[]{bean.getName(), bean.getDescription(), bean.getId()});
//    }    
//}
