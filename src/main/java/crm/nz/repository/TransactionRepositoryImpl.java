/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.TransactionItem;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class TransactionRepositoryImpl implements TransactionRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void saveTransactions(List<TransactionItem> transactionDetails) {

        String loginSQL = "INSERT INTO `investment_transaction`\n"
                + "(`InvestmentCode`,`EffectiveDate`,`Type`,`TypeDisplayName`,`SubType`,`SubTypeDisplayName`,`PortfolioCode`,`PortfolioName`,\n"
                + "`Units`,`Price`,`Value`,`Cash`,`Tax`,`Fee`,`AccruedIncome`,`CashClearing`,`PaymentClearing`,`PortfolioAccount`,`TaxRebate`,\n"
                + "`isPending`,`TransactionReasonType`,`TransactionSourceType`,`TransactionMethodType`,`TransactionDescription`,\n"
                + "`TransactionDisplayName`,`TransactionTypeDescription`,`PaymentReference`,`ExternalReference`)\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

        jdbcTemplate.batchUpdate(loginSQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                TransactionItem transactionDetail = transactionDetails.get(i);
                ps.setString(1, transactionDetail.getInvestmentCode());
                ps.setString(2, transactionDetail.getEffectiveDate());
                ps.setString(3, transactionDetail.getType());
                ps.setString(4, transactionDetail.getTypeDisplayName());
                ps.setString(5, transactionDetail.getSubType());
                ps.setString(6, transactionDetail.getSubTypeDisplayName());
                ps.setString(7, transactionDetail.getPortfolioCode());
                ps.setString(8, transactionDetail.getPortfolioName());
                ps.setDouble(9, transactionDetail.getUnits());
                double Price = transactionDetail.getPrice() != null ? transactionDetail.getPrice() : 0.0;
                ps.setDouble(10, Price);
                ps.setDouble(11, transactionDetail.getValue());
                ps.setDouble(12, transactionDetail.getCash());
                ps.setDouble(13, transactionDetail.getTax());
                ps.setDouble(14, transactionDetail.getFee());
                ps.setDouble(15, transactionDetail.getAccruedIncome());
                ps.setDouble(16, transactionDetail.getCashClearing());
                ps.setDouble(17, transactionDetail.getPaymentClearing());
                ps.setDouble(18, transactionDetail.getPortfolioAccount());
                ps.setDouble(19, transactionDetail.getTaxRebate());
                ps.setBoolean(20, transactionDetail.getIsPending());
                ps.setString(21, transactionDetail.getTransactionReasonType());
                ps.setString(22, transactionDetail.getTransactionSourceType());
                ps.setString(23, transactionDetail.getTransactionMethodType());
                ps.setString(24, transactionDetail.getTransactionDescription());
                ps.setString(25, transactionDetail.getTransactionDisplayName());
                ps.setString(26, transactionDetail.getTransactionTypeDescription());
                ps.setString(27, transactionDetail.getPaymentReference());
                ps.setString(28, transactionDetail.getExternalReference());
            }

            @Override
            public int getBatchSize() {
                return transactionDetails.size();
            }

        });
    }

    @Override
    public List<TransactionItem> getTransactions(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {

        String Sql = "SELECT IT.* FROM investment_transaction IT\n"
                + " INNER JOIN investments IM ON(IT.InvestmentCode = IM.Code)\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " WHERE IT.id > 0 ";
        if (UserId != null) {
            Sql += " and UBR.user_id= " + UserId;
        }
        if (BeneficiaryId != null) {
            Sql += " and IM.BeneficiaryId = " + BeneficiaryId;
        }
        if (BeneficiaryId != null) {
            Sql += " and IM.BeneficiaryId = " + BeneficiaryId;
        }
        if (InvestmentCode != null) {
            Sql += " and IM.Code = " + InvestmentCode;
        }
        Sql += " order by EffectiveDate asc";
        List<TransactionItem> transactions = jdbcTemplate.query(Sql, new BeanPropertyRowMapper(TransactionItem.class));
        return transactions;
    }

}
