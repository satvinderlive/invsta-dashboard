/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.Address;
import crm.nz.beans.acc.api.BankAccountDetail;
import crm.nz.beans.acc.api.Beneficiary;
import crm.nz.beans.acc.api.Email;
import crm.nz.beans.acc.api.Identification;
import crm.nz.beans.acc.api.PIRRate;
import crm.nz.beans.acc.api.PhoneNumber;
import java.util.List;

/**
 *
 * @author IESL
 */
public interface BeneficiaryRepository {

    public void saveBeneficiary(Beneficiary bean);

    public void saveBeneficiaries(List<Beneficiary> beneficiaryList);

    public void savePhoneNumbers(List<PhoneNumber> phoneNumbers);

    public List<PhoneNumber> getPhoneNumbers(String UserId, String BeneficiaryId);

    public void saveEmails(List<Email> emails);

    public List<Email> getEmails(String UserId, String BeneficiaryId);

    public void saveBankAccountDetails(List<BankAccountDetail> details);

    public List<BankAccountDetail> getBankAccountDetails(String UserId, String BeneficiaryId);

    public List<BankAccountDetail> getBankAccountsByInvestmentCode(String InvestmentCode);

    public void saveAddresses(List<Address> addresses);

    public List<Address> getAddresses(String UserId, String BeneficiaryId);

    public void saveIdentifications(List<Identification> BeneficiaryIdentification);

    public List<Identification> getIdentifications(String UserId, String BeneficiaryId);

    public void savePIRRates(List<PIRRate> PIRRates);
    
    public Beneficiary getBeneficiary(String BeneficiaryId);

    public List<Beneficiary> getBeneficiaryDetails(String UserId, String BeneficiaryId, String AdvisorId);

    public List<Beneficiary> getBeneficiaryDetailsByInvestmentCode(String InvestmentCode);

    public void saveUsernames(List<Email> emailList);

}
